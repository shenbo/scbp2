#!/usr/bin/python
#coding:utf-8

from libtest import *
import sys
import random

# previous define
trader = long(sys.argv[0])
instrAddr = long(sys.argv[1])
instrID = sys.argv[2]
bookAddr = long(sys.argv[3])
historyLowPrice = float(sys.argv[4])
historyHighPrice = float(sys.argv[5])

# direction
BUY = 0;
SELL = 1;

BID = 0;
ASK = 1;

# book: bid, ask
bookSize = [[0 for col in range(10)] for row in range(2)] 
bookPrice = [[0 for col in range(10)] for row in range(2)] 

book = getBook(bookAddr);

def initialize():
    for i in range(0, 40) :
        if (i <= 9) :
            bookSize[BID][i] = book[i];
        elif (i <= 19) :
            bookSize[ASK][i - 10] = book[i];
        elif (i <= 29) :
            bookPrice[BID][i - 20] = book[i];
        else :
            bookPrice[ASK][i - 30] = book[i];

initialize()

def openOrder(vol, price, direction) :
    sendOrder(trader, instrAddr, vol, price, direction)

# strategy
def bookChanged():
    # 行情档次
    depth = 8;

    # 下单信号, 价格窗口
    SignalWindow = 0.03;
    
    mktBidPrice = bookPrice[BID][depth];
    mktAskPrice = bookPrice[ASK][depth];
    
    if (mktAskPrice < (historyLowPrice * (1 + SignalWindow))):
        # 根据对手盘设置下单尺寸
        buyOrderSize = min(100, bookSize[ASK][depth]);
        if (buyOrderSize > 0):
            # 股票买入
            openOrder(buyOrderSize, mktAskPrice, BUY); 

    if (mktBidPrice > (historyHighPrice * (1 - SignalWindow))):
        sellOrderSize = min(100, bookSize[BID][depth]);
        if (sellOrderSize > 0):
            # 股票卖出
            openOrder(sellOrderSize, mktBidPrice, SELL);      


bookChanged();
