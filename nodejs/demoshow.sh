#!/bin/sh

# prepare raw result
tradeListFile="trade.list"
rm -rf $tradeListFile

day1Summary="day1.summary"

rm -rf $day1Summary

/home/boshen/bin/ipnl $1 > tmp.o
cat tmp.o | sed 's/\s\s*/ /g' > $day1Summary

#echo "buy:" >> $tradeListFile
cat $day1Summary | cut -d ' ' -f 1-7,8,9 | grep -i 'SHSE_' >> $tradeListFile

echo "" >> $tradeListFile
#echo "Day2(sell):" >> $tradeListFile

resultFileName="result.details"
rm -rf $resultFileName

echo "Summary:" >> $resultFileName
echo "===================================================" >> $resultFileName

./parse $tradeListFile >> $resultFileName

# cancel orders number
echo "\norder cancell info:" >> $resultFileName
cat $day1Summary | grep -i 'Cancels Attempts:' >> $resultFileName
cat $day1Summary | grep -i 'Cancels Confirms:' >> $resultFileName

echo "\nTrade info list:" >> $resultFileName
echo "===================================================" >> $resultFileName
cat $tradeListFile >> $resultFileName

# try to plot the profit
day1ProfitData="day1Profit.dat"
rm -rf $day1ProfitData

cat $day1Summary | grep -i 'SHSE_' | sed "s/$/ $2/" | cut -d ' ' -f 12,3,8 >> $day1ProfitData 
