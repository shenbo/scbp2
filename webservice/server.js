
var http = require("http");
// var rawBodyParser = require("body-parser");

var getRawBody = require('raw-body')
var bodyContent = 'the initial value';
var server = http.createServer(function(request, response) {
  getBody(request, response);
});

function getBody(request, response) {
  getRawBody(request, {
    length: this.length,
    limit: '16mb',
    encoding: this.charset//typer.parse(request.headers['content-type']).parameters.charset
  }, function (err, string) {
    if (err) {
      response.statusCode = 500;
      response.end(err.message);
      return;
    }

    response.statusCode = 200;
    bodyContent = string;
    doRequest(request, response);
  })
}

var mysql = require('mysql');

var db_config = {
  host     : '192.168.0.164',
  user     : 'root',
  password : '123',
  database : 'sse'
};

var connection;

function handleDisconnect() {
    connection = mysql.createConnection(db_config);

    connection.connect(function(err) {
        if (err) {
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000);
        }
    });

    connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } else {                                      // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}

/*
var connection = mysql.createConnection({
  host     : '192.168.0.164',
  user     : 'root',
  password : '123',
  database : 'sse'
});
*/

function authValidate(user, password, callback, response) {
    var command = 'select * from accounts where nickname=' + connection.escape(user);
    // console.log("command=", command);
    connection.query(command, function(err, rows, fields) {
        if (!err) {
            if (rows.length > 0) {
                if (rows[0].password == password) {
                    callback("OK", response);
                    return;
                }
            }
        }

        callback("FALSE", response);
    });
}

function showResult(ret, response) {
    if (ret == "OK") {
        responseDone(response, '', 200);
    } else {
        responseDone(response, '', 400);
    }
}

function doRequest(request, response) {
  var res = "test";
  console.log("start to process", request.method, "request");
  switch (request.method) {
    case 'POST': {
        switch (request.url) {
            case '/auth': {
                var body = new Buffer(bodyContent, 'base64').toString();
                var arr = body.split(",");
                authValidate(arr[0], arr[1], showResult, response);
            } break;
        }
    }break;
    case 'GET': {
      switch (request.url) {
        case '/result': {
          var cmd = 'cat result.details';
          shellRun(cmd, function(result) {
            responseDone(response, result, 200);
          });
        } break;
        case '/profit': {
          var cmd = 'cat profitAll.o';
          shellRun(cmd, function(result) {
            responseDone(response, result, 200);
          });
        } break;
        case '/positions': {
          var cmd = 'cat positions';
          shellRun(cmd, function(result) {
            responseDone(response, result, 200);
          });
        } break;
        case '/baseline': {
          var cmd = 'cat baseline';
          shellRun(cmd, function(result) {
            responseDone(response, result, 200);
          });
        } break;
        case '/netvalue': {
          var cmd = 'cat netvalue';
          shellRun(cmd, function(result) {
            responseDone(response, result, 200);
          });
        } break;
        case '/fills': {
          var cmd = 'cat fills';
          shellRun(cmd, function(result) {
            responseDone(response, result, 200);
          });
        } break;
        case '/result/png': {
          doGetPng(response);
        } break;
        default:
          //console.log("default url");
      } break;
    }
    case 'PUT': {
      switch (request.url) {
        case '/run': {
          var body = new Buffer(bodyContent, 'base64').toString();
          doPut(response, body);
        } break;
      }
    } break;
    default: {
      responseDone(response, '', 200);
    }break;
  }
}

function responseDone(response, result, stat) {
  response.setHeader("Access-Control-Allow-Origin", "*");
  response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT");
  response.writeHead(stat, {"Content-Type": "text/html"});
  response.end(result);
}

var exec = require('child_process').exec, child;
function shellRun(cmd, callback) {
    var result = '';
    
    try {
      var runcmd = exec(cmd, {maxBuffer: 1024 * 1024 * 8}, function (err, stdout, stderr) {
      result += stdout.toString();
      console.log("shell script output, stdout=*, err=", err, ", stderr=", stderr);
      return callback(result);
      })
    } catch (e) {
      console.log("catch exception; e=", e);
    }
}

var fs = require('fs');

function doGetPng(response) {
  var img = fs.readFileSync('./res.png');
  response.setHeader("Access-Control-Allow-Origin", "*");
  response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT");
  response.writeHead(200, {'Content-Type': 'image/png' });
  btoa(img);
  response.end(img, 'binary');
}

function doPut(response, strategy) {
  //console.log("debug 001");
  fs.writeFile("strategy/strategy.tmp.o", strategy, function(err) {
    if (err) {
      console.log("err msg=", err.message);
      // TODO
      return;
    }
  });
      responseDone(response, '', 200);
      return;
  try {
    var cmd = './demo2run.sh';
    shellRun(cmd, function(result) {
      responseDone(response, '', 200);
    });
  } catch (e) {
    console.log("catch an exception, e=", e);
  }

  {
      return;
  }
  
  try {
    var cmd = './demorun.sh';
    shellRun(cmd, function(result) {
      responseDone(response, '', 200);
    });
  } catch (e) {
    console.log("catch an exception, e=", e);
  }
}

// connection.connect();
handleDisconnect();
server.listen(7709);
console.log("Server is listening");
