#!/usr/bin/python
#coding:utf-8

from libtest import *
import sys
import random

# previous define
trader = long(sys.argv[0])
instrAddr = long(sys.argv[1])
instrID = sys.argv[2]
bookAddr = long(sys.argv[3])
historyLowPrice = float(sys.argv[4])
historyHighPrice = float(sys.argv[5])

# direction
BUY = 0;
SELL = 1;

BID = 0;
ASK = 1;

# book: bid, ask
bookSize = [[0 for col in range(10)] for row in range(2)] 
bookPrice = [[0 for col in range(10)] for row in range(2)] 

book = getBook(bookAddr);

def initialize():
    for i in range(0, 40) :
        if (i <= 9) :
            bookSize[BID][i] = book[i];
        elif (i <= 19) :
            bookSize[ASK][i - 10] = book[i];
        elif (i <= 29) :
            bookPrice[BID][i - 20] = book[i];
        else :
            bookPrice[ASK][i - 30] = book[i];

initialize()

def openOrder(vol, price, direction) :
    sendOrder(trader, instrAddr, vol, price, direction)

# strategy
