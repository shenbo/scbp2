#!/bin/sh

bookChangedBase="bookChangedBase.py"
bookChangedBody="bookChangedBody.py"
tickReceivedBase="tickReceivedBase.py"
tickReceivedHandler="tickReceivedBody.py"

bookChangedTag="def bookChanged():"
tickReceivedTag="def tickReceived(askSize, askPrice, bidSize, bidPrice):"

# clear anyway
rm -rf $bookChangedBody
rm -rf $tickReceivedHandler

touch $bookChangedBody
touch $tickReceivedHandler

des='N' # 'B' for bookChanged; 'T' for tickReceived

IFS=''
cat $1 |
while read line
do
    if [ "$line" = "$bookChangedTag" ]; then
        des='B'
    elif [ "$line" = "$tickReceivedTag" ]; then
        des='T'
    fi

    if [ "$des" = "N" ]; then
        continue
    elif [ "$des" = "B" ]; then
        echo $line >> $bookChangedBody
    elif [ "$des" = "T" ]; then
        echo $line >> $tickReceivedHandler
    fi

    #echo $line
done

bookChangedHandlerScript="bookChanged.py"
rm -rf $bookChangedHandlerScript
touch $bookChangedHandlerScript

# merge base and body
cat $bookChangedBase >> $bookChangedHandlerScript
cat $bookChangedBody >> $bookChangedHandlerScript
echo "\n" >> $bookChangedHandlerScript
echo "bookChanged();" >> $bookChangedHandlerScript

echo "generating done ..."
#echo $des
