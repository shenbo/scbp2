#ifndef BB_DB_QUERYBUILDER_H
#define BB_DB_QUERYBUILDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bitset>
#include <deque>
#include <boost/compressed_pair.hpp>
#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>
#include <boost/io/ios_state.hpp>
#include <bb/core/Error.h>
#include <bb/db/Connection.h>


/* Contains utility classes which should make building SQL query strings less error-prone.
 * Right now it supports two types of "column list followed by N rows of values" types of queries
 * using db::InsertBuilder and db::ReplaceBuilder
 *
 * Example:
 *      db::ReplaceBuilder m_pos_updater(db_profile_name, db_schema_and_table);
 *      // the first row must contain both column names and values
 *      m_pos_updater.reset()
 *              ( "symid",        100 )
 *              ( "acctid",       50 )
 *              ( "date",         std::string("20090622") )
 *              ( "pos",          100 )
 *              ( "last_tick_px", 12.34 )
 *              ( "update_time",  std::string("10:30:00"))
 *          ;
 *      // mark the row completed the row
 *      m_pos_updater.valueRowComplete();
 *
 *      // additional values may be included in the query
 *      m_pos_updater.addValueToRow(101)
 *          .addValueToRow(50)
 *          .addValueToRow(std::string("20090622"))
 *          .addValueToRow(100)
 *          .addValueToRow(56.78)
 *          .addValueToRow(std::string("10:30:00"))
 *          .valueRowComplete()
 *      // ...
 *      // ...
 *      // ...
 *      mysqlpp::SimpleResult result = m_pos_updater.execute();
 */

namespace bb {
namespace db {

/// Functor class for converting a value to a string which can be parsed as SQL.
/// This must be specialized for every type for which conversion is supported.
/// The use of this class is wrapped up by sql_value_thunk.
template<typename T> struct sql_value_printer
{
    /// If you get errors about unknown_type_for_sql_value_printer, that means you're trying to
    /// print a SQL value for which conversion-to-string isn't supported
    struct unknown_type_for_sql_value_printer {};
    void operator()(mysqlpp::Query &query, const unknown_type_for_sql_value_printer &t);
};

template<> struct sql_value_printer<std::string>
    { void operator()(mysqlpp::Query &query, const std::string &t) const { query << mysqlpp::quote << t; } };

template<> struct sql_value_printer<bool>
    { void operator()(mysqlpp::Query &query, bool t) const
        { query << (t ? "TRUE" : "FALSE" ); } };

template<> struct sql_value_printer<char>
    { void operator()(mysqlpp::Query &query, char t) const { query << '\'' << t << '\''; } };

template<> struct sql_value_printer<short>
    { void operator()(mysqlpp::Query &query, short t) const { query << t; } };

template<> struct sql_value_printer<unsigned short>
    { void operator()(mysqlpp::Query &query, unsigned short t) const { query << t; } };

template<> struct sql_value_printer<int>
    { void operator()(mysqlpp::Query &query, int t) const { query << t; } };

template<> struct sql_value_printer<unsigned int>
    { void operator()(mysqlpp::Query &query, unsigned int t) const { query << t; } };

template<> struct sql_value_printer<long unsigned int>
    { void operator()(mysqlpp::Query &query, long unsigned int t) const { query << t; } };

template<> struct sql_value_printer<long long>
    { void operator()(mysqlpp::Query &query, long long t) const { query << t; } };

template<> struct sql_value_printer<unsigned long long>
    { void operator()(mysqlpp::Query &query, unsigned long long t) const { query << t; } };

template<> struct sql_value_printer<double>
    { void operator()(mysqlpp::Query &query, double t) const
        { boost::io::ios_precision_saver ias(query); query << std::setprecision(std::numeric_limits<double>::digits10) << t; } };

template<> struct sql_value_printer<float>
    { void operator()(mysqlpp::Query &query, float t) const
        { boost::io::ios_precision_saver ias(query); query << std::setprecision(std::numeric_limits<float>::digits10) << t; } };

template<size_t N> struct sql_value_printer<std::bitset<N> >
    { void operator()(mysqlpp::Query &query, const std::bitset<N>& t) const { query << 'b' << '\'' << t << '\''; } };

template<typename T> struct sql_value_printer<mysqlpp::Null<T> >
    { void operator()(mysqlpp::Query &query, const mysqlpp::Null<T> &t) const
        { if(t.is_null) query << "(NULL)";
          else sql_value_printer<T>()(query, t.data); } };

template<typename T> struct sql_value_printer<boost::optional<T> >
    { void operator()(mysqlpp::Query &query, const boost::optional<T>& t) const
        { if(!t) query << "(NULL)";
          else sql_value_printer<T>()(query, *t); } };


struct sql_value_thunk_base
{
    virtual ~sql_value_thunk_base() {}
    virtual void printValue(mysqlpp::Query &query) const = 0;
};

/// This wraps a SQL value so that it can be printed later.
/// This makes a copy of the value type! (otherwise there could be problems with refs to temporaries)
/// If this is a problem for your type efficiency-wise, you might want to investigate adding
/// boost::ref support to sql_value_thunk.
template<typename T> class sql_value_thunk : public sql_value_thunk_base
{
public:
    /// if sql_value_printer<T> is an empty class, apply the empty base class optimization
    typedef boost::compressed_pair<T, sql_value_printer<T> > TPair;

    sql_value_thunk(const T& v) : m_val(TPair(v, sql_value_printer<T>())) {}

    virtual void printValue(mysqlpp::Query &query) const { m_val.second()(query, m_val.first()); }
    TPair m_val;
};

enum UpdateOper { ASSIGN, INCREMENT };

class QueryBuilder : public boost::noncopyable
{
public:
    virtual ~QueryBuilder() { clear(); }

    /// Adds a name/value pair to the insert query
    template<typename T>
    QueryBuilder &operator()(const char *fieldName, const T &x)
    {
        if(m_fields_finalized)
            BB_THROW_ERROR_SS("db::" << m_builder_id << ": Cannot add fields after 1st row of values");

        m_fields.push_back(fieldName);

        // when we define the fields, store the associated values in the first row
        m_values.front().push_back(new sql_value_thunk<T>(x));
        return *this;
    }

    /// adds a field to the query
    void addField(const char* fieldName)
    {
        if(m_fields_finalized)
            BB_THROW_ERROR_SS("db::" << m_builder_id << ": Cannot add fields after 1st row of values");

        m_fields.push_back(fieldName);
    }

    template<typename T>
    QueryBuilder& addValueToRow(const T &x)
    {
        if(m_row_completed)
        {
            m_values.push_back(DequeSqlValThunk_t());
            m_row_completed=false;
        }
        // Error if m_fields is empty
        if(m_fields.empty() || m_values.empty())
            BB_THROW_ERROR_SS("db::" << m_builder_id << ": empty query ");

        // disable operator()
        m_fields_finalized = true;

        // if previous values deque is full, start a new one
        if ( m_values.back().size() == m_fields.size())
            BB_THROW_ERROR_SS("db::" << m_builder_id << ": too many values for number of fields");

        m_values.back().push_back(new sql_value_thunk<T>(x));

        return *this;
    }

    QueryBuilder& valueRowComplete()
    {
        // Error if m_fields is empty
        if(m_fields.empty() || m_values.empty())
            BB_THROW_ERROR_SS("db::" << m_builder_id << ": empty query ");
        if ( m_values.back().size() != m_fields.size())
            BB_THROW_ERROR_SS("db::" << m_builder_id << ": Incomplete value row in query");

        m_row_completed=true;
        return *this;
    }

    template<typename T>
    QueryBuilder &operator()(const std::string &fieldName, const T &x)
        { return operator()(fieldName.c_str(), x); }

    /// Returns the completed query string. You can only call this once.
    virtual mysqlpp::Query &finishQuery()
    {
        if(m_values.empty())
            BB_THROW_ERROR_SS("db::" << m_builder_id << ": empty query");
        if(m_values.back().size() != m_fields.size())
            BB_THROW_ERROR_SS("db::" << m_builder_id << ": values list doesn't match field list");

        // add the fields
        bool first_field = true;
        for(std::deque<std::string>::const_iterator i=m_fields.begin(), e=m_fields.end(); i!=e; ++i)
        {
            if (!first_field)
                m_query << ',';
            m_query << '`' << *i << '`';
            first_field = false;
        }

        // add the values
        m_query << ") VALUES ";
        bool first = true;
        while(!m_values.empty())
        {
            if(m_values.front().empty())
            {
                m_values.pop_front();
            }
            if(!first)
                m_query << ',';
            m_query << '(';
            bool first_val = true;
            while(!m_values.front().empty())
            {
                if (!first_val)
                    m_query << ',';
                sql_value_thunk_base *thunk = m_values.front().front();
                thunk->printValue(m_query);
                delete thunk;
                m_values.front().pop_front();
                first_val = false;
            }
            m_query << ')';
            m_values.pop_front();

            first = false;
        }

        return m_query;
    }

    /// Finishes and executes the insert.
    //mysqlpp::SimpleResult execute() { finishQuery(); return m_query.execute(); }

    mysqlpp::SimpleResult execute(bool verbose=false) {
        finishQuery();
        if (verbose){
            std::cout << "m_query: " << m_query.str() << std::endl;
        }
        return m_query.execute();
    }
    /// Reset to the initial state, ready for another call to finishQuery() or execute()
    virtual QueryBuilder &reset() = 0;

protected:
    QueryBuilder(db::ConnectionPtr conn, const std::string &table_name, const std::string &builder_id)
        : m_query(conn->query())
        , m_table_name(table_name)
        , m_builder_id(builder_id)
        , m_values(1)
        , m_fields_finalized(false)
        , m_row_completed(false)

    {
        m_values.resize(1);
    }
    void clear()
    {
        for(std::deque< DequeSqlValThunk_t >::iterator i=m_values.begin(), e=m_values.end(); i!=e; ++i)
        {
            std::for_each( i->begin(), i->end(), boost::checked_deleter<sql_value_thunk_base>() );
            i->clear();

        }
        m_values.clear();
        m_values.resize(1);
        m_fields.clear();
        m_row_completed = false;
        m_fields_finalized = false;
    }

    mysqlpp::Query m_query;
    std::string m_table_name;
    std::string m_builder_id;

    std::deque< std::string > m_fields;

    typedef std::deque<sql_value_thunk_base*> DequeSqlValThunk_t;
    std::deque< DequeSqlValThunk_t >m_values;

    bool m_fields_finalized;
    bool m_row_completed;

};

class InsertBuilder : public QueryBuilder
{
public:
    InsertBuilder(db::ConnectionPtr conn, const std::string &table_name)
        : QueryBuilder(conn, table_name, "InsertBuilder")
    {
        reset();
    }
    InsertBuilder &reset()
    {
        clear();
        m_query.reset();
        m_query << "INSERT INTO " << m_table_name << " (";
        return *this;
    }
};

BB_DECLARE_SHARED_PTR( InsertBuilder );

// INSERT BUT THERE'S NO PROBLEM IF WE HAVE DUPLICATES
class InsertIgnoreBuilder : public QueryBuilder
{
public:
    InsertIgnoreBuilder(db::ConnectionPtr conn, const std::string &table_name)
        : QueryBuilder(conn, table_name, "InsertIgnoreBuilder")
    {
        reset();
    }
    InsertIgnoreBuilder &reset()
    {
        clear();
        m_query.reset();
        m_query << "INSERT IGNORE INTO " << m_table_name << " (";
        return *this;
    }
};

BB_DECLARE_SHARED_PTR( InsertIgnoreBuilder );

// INSERT ... ON DUPLICATE KEY UPDATE ... col1=col1+VALUES(col1) ...
class InsertODKUBuilder : public QueryBuilder
{
public:
    InsertODKUBuilder(db::ConnectionPtr conn, const std::string &table_name)
        : QueryBuilder(conn, table_name, "InsertODKUBuilder")
    {
        reset();
    }
    InsertODKUBuilder& reset()
    {
        clear();
        m_query.reset();
        m_query << "INSERT INTO " << m_table_name << " (";
        return *this;
    }

    InsertODKUBuilder& addODKUStatement(const std::string &fieldName, const UpdateOper oper=ASSIGN)
    {
        std::map< std::string , std::string >::const_iterator i=m_odku_statements.find(fieldName);
        if (i!= m_odku_statements.end())
            BB_THROW_ERROR_SS("db::" << m_builder_id << ": Would change update from " << i->second << " to " <<  "=VALUES(" << fieldName << ")");

        switch(oper)
        {
            case ASSIGN:
                m_odku_statements[fieldName] = fieldName+"=VALUES("+fieldName+")";
                break;
            case INCREMENT:
                m_odku_statements[fieldName] = fieldName+"="+fieldName+"+VALUES("+fieldName+")";
                break;
            default:
                BB_THROW_ERROR_SS("db::" << m_builder_id << ": oper" << oper << " is not a valid UpdateOper");
                break;
        }
        return *this;
    }

    mysqlpp::Query& finishQuery()
    {
        // first, do the normal finishing
        QueryBuilder::finishQuery();

        // now, append the ON DUPLICATE KEY UPDATE statement
        bool first_statement= true;
        if(! m_odku_statements.empty())
        {
            m_query << "ON DUPLICATE KEY UPDATE ";
            for(std::map<std::string,std::string>::const_iterator i=m_odku_statements.begin(), e=m_odku_statements.end(); i!=e; ++i)
            {
                if (!first_statement)
                    m_query << ',';
                m_query << i->second;
                first_statement = false;
            }
        }
        //std::cout << "Query: " << m_query.str() << std::endl;
        return m_query;
    }
protected:
    InsertODKUBuilder& clear()
    {
        QueryBuilder::clear();
        m_odku_statements.clear();
        return *this;
    }
private:
    std::map< std::string, std::string > m_odku_statements;
};

BB_DECLARE_SHARED_PTR( InsertODKUBuilder );

class ReplaceBuilder : public QueryBuilder
{
public:
    ReplaceBuilder(db::ConnectionPtr conn, const std::string &table_name)
        : QueryBuilder(conn, table_name, "ReplaceBuilder")
    {
        reset();
    }

    ReplaceBuilder &reset()
    {
        clear();
        m_query.reset();
        m_query << "REPLACE INTO " << m_table_name << " (";
        return *this;
    }
};

BB_DECLARE_SHARED_PTR( ReplaceBuilder );

} // namespace db
} // namespace bb


#endif // BB_DB_QUERYBUILDER_H
