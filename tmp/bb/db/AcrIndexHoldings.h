#ifndef BB_DB_ACRINDEXHOLDINGS_H
#define BB_DB_ACRINDEXHOLDINGS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// Dependent includes
#include <vector>
#include <iosfwd>
#include <bb/core/smart_ptr.h>
#include <bb/core/instrument.h>
#include <string>
#include <iosfwd>
#include <bb/core/symbol.h>
#include <bb/core/safevalue.h>
#include <bb/core/safeenum.h>
#include <boost/static_assert.hpp>
#include <bb/db/AcrIndexInfo.h>
struct lua_State;
namespace bb {
namespace AcrIndex{
BB_FWD_DECLARE_SHARED_PTR(Holdings);
BB_FWD_DECLARE_SHARED_PTR(ISpec);

}

namespace db {

/// Retrieves the AcrIndexHoldingsPtr corresponding to holdings of the specified AcrIndex symbol
/// on the day of the specified timeval.
///
/// The search checks the dates to the trade date.  If none are found,
/// it is considered failed
///
/// If the corresponding AcrIndexHoldings do not exist, it throws a bb::Error.
AcrIndex::HoldingsCPtr getAcrIndexHoldings(symbol_t sym_adx, timeval_t tv,std::string const& profile );
AcrIndex::HoldingsCPtr getAcrIndexHoldings( bb::symbol_t const& sym_adx, bb::timeval_t const& tv,AcrIndex::ISpecCPtr const& , std::string const& profile );
bool exists( bb::symbol_t const& sym_adx, bb::timeval_t const& tv,AcrIndex::ISpecCPtr const& , std::string const& profile );
bool registerDBAcrIndexEntities( lua_State&  L );

} // namespace db


/**
**/


namespace AcrIndex{


class Holdings {
public:
    struct Component {
        typedef double WeightType;
        enum CoeffType {
            CoeffType_Shares, CoeffType_Constant
        };
        Component() ;
        Component(instrument_t const&symbol_, WeightType const&weight_,CoeffType wt);
        Component(instrument_t const&symbol_, WeightType const&weight_);//default CoeffType_Shares
        WeightType getWeight() const;
        instrument_t const& getInstrument() const ;
        friend std::ostream& operator<<(std::ostream&, Component const&);
        friend std::ostream& operator<<(std::ostream&, Component::CoeffType);
        CoeffType getType()const;
    private:
        instrument_t m_instr;
        WeightType m_weight;
        CoeffType m_coefftype;
    };
    typedef std::vector<Component> ComponentVec;
    typedef std::vector<const Component> ComponentCVec;

public:
    bb::symbol_t getSymbol() const;
    bb::date_t getTradeDate() const;
    unsigned size()const;
    ComponentVec const& getComponents() const;
    void push_back(Component const&);

    ISpecCPtr getSpec()const;

    template<typename T > bb::shared_ptr<T const> getInfo()const{
        bb::shared_ptr<T const> ret=m_infos.get<T>();
        return ret;
    }

    template<typename T >
    void addInfo(bb::shared_ptr<T const> const&v){
        m_infos.add(v);
    }

    typedef ComponentVec::value_type value_type;
    typedef ComponentVec::const_iterator const_iterator;
    typedef Component const& const_reference;

    const_iterator begin()const;
    const_iterator end()const;
    Holdings(bb::symbol_t adx_sym, bb::date_t const& tradedate,ISpecCPtr const&);
    Holdings(bb::instrument_t const & adx_sym, bb::date_t const& tradedate,ComponentVec const&,ISpecCPtr const&);
    Holdings(bb::symbol_t const & adx_sym, bb::date_t const& tradedate,ComponentVec const&,ISpecCPtr const&);
    virtual ~Holdings();
private:
    ComponentVec& getComponents();
    bb::symbol_t m_adx_sym;
    bb::date_t m_tradedate;
    ComponentVec m_comps;
    ISpecCPtr m_spec;
    InfoContainer m_infos;


    friend HoldingsCPtr db::getAcrIndexHoldings(symbol_t sym_adx,timeval_t tv, const std::string& profile);
    friend std::ostream& operator<<(std::ostream&, Holdings const&);
};

}//AcrIndex
} // namespace bb


#endif // BB_DB_ACRINDEXHOLDINGS_H
