#ifndef BB_DB_COMMODITIESSPECIFICATIONS_H
#define BB_DB_COMMODITIESSPECIFICATIONS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/optional.hpp>

#include <bb/core/CommoditiesSpecifications.h>
#include <bb/core/smart_ptr.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Environment );


namespace db {

ICommoditiesSpecificationsMapCPtr getCommoditiesSpecsMap( const EnvironmentPtr& env, const std::string& db_profile );


class CommoditiesSpecificationsMap : public ICommoditiesSpecificationsMap
{
public:
    typedef container_type::mapped_type mapped_type;
    typedef container_type::value_type value_type;
    typedef container_type::iterator iterator;

public:
    CommoditiesSpecificationsMap( const bb::EnvironmentPtr& env, const std::string& db_profile );

    iterator begin()
    {
        return m_spec_map.begin();
    }

    const_iterator begin() const
    {
        return m_spec_map.begin();
    }

    iterator end()
    {
        return m_spec_map.end();
    }

    const_iterator end() const
    {
        return m_spec_map.end();
    }

    mapped_type& operator[]( const key_type& key )
    {
        return m_spec_map[key];
    }

    iterator find( const key_type& key )
    {
        return m_spec_map.find( key );
    }

    const_iterator find( const key_type& key ) const
    {
        return m_spec_map.find( key );
    }

    boost::optional<CommoditySpecification> findBySymbolExpiry( const symbol_t& symbol, const expiry_t& expiry ) const
    {
        const_iterator map_it = m_spec_map.find( symbol );
        if( map_it != m_spec_map.end() )
        {
            CommoditySpecificationsList::const_iterator list_it = map_it->second.find( expiry );
            if( list_it != map_it->second.end() )
                return boost::optional<CommoditySpecification>( *list_it );
        }
        return boost::optional<CommoditySpecification>();
    }

    boost::optional<CommoditySpecification> findByInstrument( const instrument_t& instrument ) const
    {
        return findBySymbolExpiry( instrument.sym, instrument.exp );
    }

    // Throws on error
    CommoditySpecification findLua( const symbol_t& symbol, const expiry_t& expiry ) const;

    /// Returns the DB profile this CommoditiesSpecificationsMap was constructed
    /// out of
    const std::string& getDBProfile() const
    {
        return m_db_profile;
    }

private:
    /// Contract specifications can change over time, so we only construct one
    /// for a given date
    date_t m_date;

    /// DB profile that this map was constructed out of
    std::string m_db_profile;

    /// Container for the mappings
    container_type m_spec_map;
};

BB_DECLARE_SHARED_PTR( CommoditiesSpecificationsMap );

} // namespace db
} // namespace bb

#endif // BB_DB_COMMODITIESSPECIFICATIONS_H
