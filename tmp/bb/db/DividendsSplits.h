#ifndef BB_DB_DIVIDENDSSPLITS_H
#define BB_DB_DIVIDENDSSPLITS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <string>
#include <vector>
#include <bb/core/date.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/symboldateidx.h>
#include <boost/range/iterator_range.hpp>
#include <bb/core/Scripting.h>


namespace bb {
namespace db {
class DividendsSplits
{
public:

    BB_DECLARE_SCRIPTING();

    struct Data:bb::SymbolDateIdx {
         typedef bb::SymbolDateIdx base_type;
         Data(base_type const&_idx, double _cash, double _stock);
         friend std::ostream& operator<<(std::ostream&os,Data const& y);
         double operator()(double const&did)const{
                 return (did/m_stock)-m_cash;
         }
     private:
         double m_cash;
         double m_stock;
    };
    typedef std::vector<Data> cont_type;
    typedef std::vector<bb::symbol_t> symbols_type;
    typedef cont_type::const_iterator const_iterator;
    typedef cont_type::value_type value_type;
    DividendsSplits(symbols_type const&,  bb::date_t const& begindate,bb::date_t const& enddate, const std::string& db_profile );

    typedef boost::iterator_range<const_iterator> range_type;

    const_iterator begin() const;
    const_iterator end() const;

   // void print( std::ostream& os ) const;

private:
    std::string m_db_profile;
    cont_type m_vals;
};

BB_DECLARE_SHARED_PTR( DividendsSplits );

} // namespace db
} // namespace bb

#endif // BB_DB_DIVIDENDSSPLITS_H
