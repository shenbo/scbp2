#ifndef BB_DB_ACRINDEXSPEC_H
#define BB_DB_ACRINDEXSPEC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// Dependent includes

#include <iosfwd>
#include <bb/core/smart_ptr.h>
#include <bb/core/instrument.h>
#include <bb/core/symbol.h>
#include <bb/core/safevalue.h>
#include <bb/core/safeenum.h>
#include <boost/static_assert.hpp>
struct lua_State;
namespace bb {
namespace AcrIndex{

/** OVERVIEW
 * The components of an Index can be determined in several different ways
 * The original way is by the published weights. Also we calculate component weight in a number of different ways
 *
 *
 *
 *
 */


namespace detail{

struct SpecTypePolicy{
    typedef unsigned  storage_type;
    enum _EnumVals{
        SpecType_Published=0,SpecType_Alternate=1,SpecType_Manual=2
    };
    typedef _EnumVals enum_type;
    static const _EnumVals largest_enum_val=SpecType_Alternate;
    static storage_type narrow(storage_type _val);
    friend std::ostream&operator<<(std::ostream&os,enum_type val);
private:
    BOOST_STATIC_ASSERT(sizeof(storage_type) <=sizeof(unsigned) );
    BOOST_STATIC_ASSERT(unsigned(1<<((sizeof(storage_type)*8)-1 ) ) >=largest_enum_val);
};

//values match the values in etf/alternate_selection_type
struct SelectionTypePolicy{
    typedef unsigned char storage_type;
    enum _EnumVals{
        SelectionType_Manual=0,ETF_AllComponents=1,ETF_WildComponents=2,ETF_Test=3,ETF_LargestVolume=4,ETF_CorrelationMagic=5,ETF_Correlation2=6
    };
    typedef _EnumVals enum_type;
    static const _EnumVals largest_enum_val=ETF_Correlation2;
    static storage_type narrow(storage_type _val);
    friend std::ostream&operator<<(std::ostream&os,enum_type val);
private:
    BOOST_STATIC_ASSERT(sizeof(storage_type) <=sizeof(unsigned) );
    BOOST_STATIC_ASSERT(unsigned(1<<((sizeof(storage_type)*8)-1 ) ) >=largest_enum_val);

};
struct SelectionCodePolicy:SafeFundamentalDefaultPolicy<unsigned>{};
struct SelectionSeriesPolicy:SafeFundamentalDefaultPolicy<unsigned>{};
struct HoldingAlternateIdPolicy:SafeFundamentalDefaultPolicy<unsigned>{};
}//detail



typedef SafeEnum<detail::SelectionTypePolicy> SelectionType;
typedef SafeValue<detail::SelectionCodePolicy> SelectionCode;
typedef SafeValue<detail::SelectionSeriesPolicy> SelectionSeries;

typedef SafeEnum<detail::SpecTypePolicy> SpecType;


BB_FWD_DECLARE_SHARED_PTR( ISpec );
//this is the spec for choosing which basket of components we will use
class ISpec
{
public:
    virtual ~ISpec();
    virtual SpecType getSpecType()const=0;
    friend std::ostream& operator<<(std::ostream&,ISpec const&);
    virtual std::ostream& print(std::ostream&)const=0;
    int compare(ISpec const&)const;
protected:
    virtual int compare_i(ISpec const&)const=0;
};
BB_DECLARE_SHARED_PTR( ISpec );
bool operator<(ISpecCPtr const&,ISpecCPtr const&);
bool operator==(ISpecCPtr const&,ISpecCPtr const&);
namespace detail{
template<typename SpecType::enum_type t>struct SpecTemplate:ISpec{
    SpecType getSpecType()const{return t;}
    std::ostream& print(std::ostream&os)const{
        os<<t;
        return os;
    }
private:
    int compare_i(ISpec const&)const{return 0;}
};
}//detail
//use the actual published components
typedef detail::SpecTemplate<SpecType::SpecType_Published> SpecPublished;
BB_DECLARE_SHARED_PTR( SpecPublished );
//manually set the components
typedef detail::SpecTemplate<SpecType::SpecType_Manual> SpecManual;
BB_DECLARE_SHARED_PTR( SpecManual );

//alternate spec
//the meaning of Code and Series is dependent on the SelectionType

struct ISpecAlternate:ISpec{
    virtual SelectionType           getSelectionType()const=0;
    virtual SelectionCode           getCode()const=0;
    virtual SelectionSeries         getSeries()const=0;

};
BB_DECLARE_SHARED_PTR( ISpecAlternate );


//concrete implementation
struct SpecAlternate:ISpecAlternate{
     SpecType           getSpecType()const;
     SelectionType      getSelectionType()const;
     SelectionCode      getCode()const;
     SelectionSeries    getSeries()const;
     std::ostream& print(std::ostream&)const;
     SpecAlternate(SelectionType const&,SelectionCode const&,SelectionSeries const& );
private:
    SelectionType      m_type;
    SelectionCode      m_code;
    SelectionSeries    m_series;
private:
    int compare_i(ISpec const&)const;
};
BB_DECLARE_SHARED_PTR(SpecAlternate );


bool registerSpecEntities( lua_State&  L );


} // namespace AcrIndex
} // namespace bb


#endif // BB_DB_ACRINDEXSPEC_H
