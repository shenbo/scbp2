// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include <bb/db/IndexSpecificationDB.h>

#include <bb/core/env.h>
#include <bb/db/Connection.h>

#include <iostream>


namespace bb {
namespace db {

IndexSpecificationDB::IndexSpecificationDB( const EnvironmentPtr& env
    , const date_t date
    , const std::string& db_profile
    , const std::string& db_table
    , const bb::instrument_t& index_instr )
    : m_db_profile(db_profile)
    , m_db_table(db_table)
    , m_index_instr(index_instr)
{


   // query db for index components
    size_t cnt = 5; //max number of retries. Enhance resilience to network connection issue to database
    while ( !m_connection && cnt > 0 )
    {
        m_connection=db::getConnection( env, m_db_profile, true );
        cnt--;
    }
    if( cnt <= 0 )
        BB_THROW_ERROR("MySql: could not connect to index weights DB");

    mysqlpp::Query query = m_connection->query();

    /// we may need to change this query after we improve our db table
    query << "SELECT ticker,weight FROM " << m_db_table << " WHERE date='" << date <<
             "' AND index_symid=" << m_index_instr.sym.id();

    // std::cout << query << std::endl ;
    m_results = query.use();
} // bb::db::IndexSpecificationDB::IndexSpecificationDB

boost::optional< std::pair<std::string, double> > IndexSpecificationDB::next()
{
    if( mysqlpp::Row row = m_results.fetch_row() )
    {
        return std::make_pair( std::string(row.at(0).c_str()), (double) row.at(1) );
    }
    return boost::none;
}

} // namespace db
} // namespace bb
