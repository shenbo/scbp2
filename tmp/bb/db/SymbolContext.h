#ifndef BB_DB_SYMBOLCONTEXT_H
#define BB_DB_SYMBOLCONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <vector>

#include <bb/core/smart_ptr.h>

namespace bb {
class date_t;
BB_FWD_DECLARE_SHARED_PTR( BasicSymbolContext );
BB_FWD_DECLARE_SHARED_PTR( Environment );

namespace db {

/// Creates a BasicSymbolContext from the DB
BasicSymbolContextPtr createSymbolContext( const EnvironmentPtr& env, const std::string& db_profile,
        const date_t& date, const std::vector<unsigned int>& sources );

} // namespace db
} // namespace bb

#endif // BB_DB_SYMBOLCONTEXT_H
