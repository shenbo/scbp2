#ifndef BB_DB_DATAFILEDB_H
#define BB_DB_DATAFILEDB_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <string>

#include <bb/core/gdate.h>
#include <bb/core/source.h>
#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Environment );

namespace db {

/// Returns a list of unsplit datafiles matching the given
/// source&date range from the worfdata.source_datafiles catalog.
std::vector<std::string> getUnsplitDatafiles( const bb::EnvironmentPtr& env
    , source_t source
    , const timeval_t& starttv
    , const timeval_t& endtv );

} // namespace db
} // namespace bb

#endif // BB_DB_DATAFILEDB_H
