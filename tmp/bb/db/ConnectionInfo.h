#ifndef BB_DB_CONNECTIONINFO_H
#define BB_DB_CONNECTIONINFO_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <bb/core/smart_ptr.h>

namespace luabind { namespace adl { class object; } using adl::object; }

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( Environment );
namespace db {

namespace detail {
class ConnectionInfoFactory;
} // namespace detail


/// Database Connection Info
class IConnectionInfo
{
public:
    virtual ~IConnectionInfo() {}
    virtual const std::string& getAdapter() const = 0;
};
BB_DECLARE_SHARED_PTR( IConnectionInfo );


/// MySQL ConnectionInfo
BB_FWD_DECLARE_SHARED_PTR( MySQLConnectionInfo );
class MySQLConnectionInfo
    : public IConnectionInfo
{
    friend class detail::ConnectionInfoFactory;
public:
    /// Creates a MySQLConnectionInfo.
    static MySQLConnectionInfoPtr create(
        const std::string& host,
        const std::string& username,
        const std::string& password,
        const std::string& database,
        const unsigned int port = 3306 // default mysql port
    );

    // Returns the name of the MySQLConnectionInfo's adapter.
    static const std::string getAdapterName()
    {   return "mysql"; }

    /// Returns "mysql".
    virtual const std::string&  getAdapter() const
    {   return m_adapter; }

    const std::string&  getHost() const     { return m_host; }
    const std::string&  getUsername() const { return m_username; }
    const std::string&  getPassword() const { return m_password; }
    const std::string&  getDatabase() const { return m_database; }
    const unsigned int  getPort() const     { return m_port; }

private:
    // zero-initialized, meant for implementors
    MySQLConnectionInfo() {}

private:
    std::string m_adapter;
    std::string m_host;
    std::string m_username;
    std::string m_password;
    std::string m_database;
    unsigned int m_port;
};


/// PostgreSQL ConnectionInfo
BB_FWD_DECLARE_SHARED_PTR( PostgreSQLConnectionInfo );
class PostgreSQLConnectionInfo
    : public IConnectionInfo
{
    friend class detail::ConnectionInfoFactory;
public:
    /// Creates a PostgreSQLConnectionInfo.
    static PostgreSQLConnectionInfoPtr create(
        const std::string& host,
        const std::string& username,
        const std::string& database
    );

    // Returns the name of the MySQLConnectionInfo's adapter.
    static const std::string getAdapterName()
    {   return "postgresql"; }

    /// Returns "postgresql".
    virtual const std::string&  getAdapter() const
    {   return m_adapter; }

    const std::string&  getHost() const     { return m_host; }
    const std::string&  getUsername() const { return m_username; }
    const std::string&  getDatabase() const { return m_database; }

private:
    // zero-initialized, meant for implementors
    PostgreSQLConnectionInfo() {}

private:
    std::string m_adapter;
    std::string m_host;
    std::string m_username;
    std::string m_database;
};


/// MongoDB ConnectionInfo
BB_FWD_DECLARE_SHARED_PTR( MongoDBConnectionInfo );
class MongoDBConnectionInfo
    : public IConnectionInfo
{
    friend class detail::ConnectionInfoFactory;
public:
    /// Creates a MongoDBConnectionInfo.
    static MongoDBConnectionInfoPtr create(
        const std::string& host
    );

    // Returns the name of the MongoDBConnectionInfo's adapter.
    static const std::string getAdapterName()
    {   return "mongodb"; }

    /// Returns "mongodb".
    virtual const std::string&  getAdapter() const
    {   return m_adapter; }

    const std::string&  getHost() const     { return m_host; }

private:
    // zero-initialized, meant for implementors
    MongoDBConnectionInfo() {}

private:
    std::string m_adapter;
    std::string m_host;
};


/// Sqlite3ConnectionInfo
BB_FWD_DECLARE_SHARED_PTR( Sqlite3ConnectionInfo );
class Sqlite3ConnectionInfo
    : public IConnectionInfo
{
    friend class detail::ConnectionInfoFactory;
public:
    /// Creates a Sqlite3ConnectionInfo.
    static Sqlite3ConnectionInfoPtr create( const std::string& dbfile );

    // Returns the name of the Sqlite3ConnectionInfo's adapter.
    static const std::string getAdapterName()
    {   return "sqlite3"; }

    /// Returns "sqlite3".
    virtual const std::string&  getAdapter() const
    {   return m_adapter; }

    const std::string&  getDBFile() const   { return m_dbfile; }

private:
    // zero-initialized, meant for implementors
    Sqlite3ConnectionInfo() {}

private:
    std::string m_adapter;
    std::string m_dbfile;
};


/// Returns an IConnectionInfoPtr for the given profile.
/// Returns a null IConnectionInfoPtr if the profile doesn't exist.
/// Throws an exception if the profile cannot be parsed.
IConnectionInfoPtr getConnectionInfo( const EnvironmentPtr& env, const std::string& profile );
IConnectionInfoPtr getConnectionInfo( const luabind::object &luaprofile );

} // namespace db
} // namespace bb

#endif // BB_DB_CONNECTIONINFO_H
