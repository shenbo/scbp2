#ifndef BB_DB_DBSCRIPTING_H
#define BB_DB_DBSCRIPTING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

struct lua_State;
namespace bb {
namespace db {

/// Adds the DB functionality to the Luabind scripting environment.
bool registerScripting();
bool registerDBEntities( lua_State& rL );

} // namespace db
} // namespace bb

#endif // BB_DB_DBSCRIPTING_H
