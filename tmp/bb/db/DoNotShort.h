#ifndef BB_DB_DONOTSHORT_H
#define BB_DB_DONOTSHORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/acct.h>
#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/hash_map.h>

#include <bb/db/Connection.h>

namespace bb {

class symbol_t;

namespace db {

// Forward declarations
BB_FWD_DECLARE_SHARED_PTR( DoNotShort );

///
/// The search checks the dates from tv back to the next earlier published date.  If none are found,
/// it is considered failed, unless search_forward_on_failure in which case it will search forward.
///
class DoNotShort
{
public:
    static const char* const default_connection_profile;
    /// Creates a DoNotShortPtr, loading information from the database.
    /// Returns an invalid shared_ptr if the do-not-short list cannot be loaded.
    static DoNotShortPtr create( const timeval_t& tv_date_requested,
                                 bool search_forward_on_failure = false,
                                 const std::string& connection_profile = default_connection_profile );


    /// Same as above, but only load information for one account ( above loads all accounts )
    /// Use if you know you will only be trading in one account ( which is usually the case )
    /// Has different name because of lua bindings ( luabindings only used by Century. rename
    /// this method if Century ever updated )
    static DoNotShortPtr create_for_account( const timeval_t& tv_date_requested,
                                             const bb::acct_t account,
                                             bool search_forward_on_failure = false,
                                             const std::string& connection_profile = default_connection_profile );

    /// Returns true if the symbol is shortable, or false otherwise.
    inline bool is_shortable( const acct_t& acct, const symbol_t& sym ) const
    {
        return getMaxShortPosition( acct, sym ) != 0;
    }

    /// Returns the maximum short position for the symbol in the
    /// account. Returns zero if the symbol may not be shorted, or a
    /// positive number representing the maximum number of shares that
    /// may be shorted. For 'easy to borrow' symbols which have no
    /// specified limit, it returns numeric_limits<size_t>::max().
    size_t getMaxShortPosition( const acct_t& acct, const symbol_t& sym ) const;

    /// Returns the date that was requested in the constructor.
    const timeval_t& getDateRequested() const  { return m_requested_tv; }

    /// Returns the published date of the do-not-short table that was already.
    /// Otherwise returns timeval_t() if the table was not loaded.
    timeval_t getDatePublished( const acct_t& acct ) const;

private:
    // Use the factory method.
    DoNotShort( const timeval_t& tv_date_requested );

    /// Loads short information from database
    /// The search checks the dates from tv back to the next earlier published date.  If none are found,
    /// it is considered failed, unless search_forward_on_failure in which case it will search forward.
    bool load_from_database( bool search_forward_on_failure, const ConnectionPtr& spConn );
    bool load_clearing_firm_from_database( int clearing_firm_id,
                                           bool search_forward_on_failure,
                                           const ConnectionPtr& spConn );

    // Populates one clearing-firm's shorting info
    bool load( const ConnectionPtr& spConn,
               const std::string& table_name,
               const std::string& date_colname,
               const std::string& clearing_firm_colname,
               const mysqlpp::Date& published_date,
               int clearing );

    // Returns the best date to use for setting shortability.
    mysqlpp::Date get_published_date( const ConnectionPtr& spConn,
                                      const std::string& table_name,
                                      const std::string& date_colname,
                                      const std::string& clearing_firm_colname,
                                      int clearing,
                                      bool search_forward_on_failure ) const;


    // Loads the clearing firms
    bool load_clearing_firms( const ConnectionPtr& spConn );

    // Loads the accounts
    bool load_accounts( const ConnectionPtr& spConn );

private:
    const timeval_t m_requested_tv;
    std::vector<int> m_clearingFirms;

    std::map<acct_t, int> m_mapAccountClearing;
    typedef std::map<acct_t, int>::const_iterator CIterAC;

    typedef bbext::hash_map< symbol_t, size_t > SizeForSymbolMap;

    std::map<int, SizeForSymbolMap > m_mapClearingShortableTable;
    typedef std::map<int, SizeForSymbolMap >::const_iterator CIterCST;

    std::map<int, timeval_t > m_mapClearingPublishedTV;
    typedef std::map<int, timeval_t >::const_iterator CIterCPTV;

};

} // namespace db
} // namespace bb

#endif // BB_DB_DONOTSHORT_H
