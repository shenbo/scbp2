#ifndef BB_DB_DAYINFOSTATS_H
#define BB_DB_DAYINFOSTATS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// Dependent includes
#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>
#include <string>
#include <bb/core/instrument.h>
#include <map>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Environment );

class DayInfoStatsFactory;
class DayInfoStats ;
/**
 DayInfoStats
 Represents summary information for a particular symbol/date pair.

 **/
class DayInfoStats {
public:

    friend class DayInfoStatsFactory;
    struct Data {
        friend class DayInfoStats;
        double getAverage() const;
        double getStddev_p() const;
        double getStddev_s() const;
        //if you really care, then increase the number of samples until you don't
        double getStddev() const{
            return m_stddev_p;
        }
        double getMax() const;
        double getMin() const;
        friend std::ostream& operator<<(std::ostream&,Data const&);
    private:
        Data();
        Data(double avg, double stddev_s, double stddev_p,double mmax,double mmin);
        double m_avg;
        double m_stddev_p;
        double m_stddev_s;
        double m_max;
        double m_min;
    };
    /// Returns the symbol this DayInfo refers to.
    bb::symbol_t getSymbol() const ;
    /// Returns the date of the most recent sample this DayInfoStats refers to.

    bb::date_t getDate() const;
    /// Returns the number of samples used
    unsigned getNumSamples() const;
    /// Returns the  opening stats.
    Data const& getOpen() const ;
    /// Returns the closing price.
    Data const& getClose() const;
    /// Returns the lowest price.
    Data const& getLow() const;
    /// Returns the highest price.
    Data const& getHigh() const;
    /// Returns the volume
    Data  const& getVolume() const;

private:
    /// Constructs a DayInfoStats with the given sym and date_range,i.e.
    ///start with start_tv-N trading days to  start_tv
    DayInfoStats(const bb::EnvironmentPtr& env, symbol_t sym, bb::date_t const& start_tv, unsigned N,std::string const& _dbprof);

    bb::symbol_t m_sym;
    bb::date_t m_tv;
    unsigned m_numsamples;
    Data m_open;
    Data m_close;
    Data m_low;
    Data m_high;
    Data m_vol;
    friend std::ostream& operator<<(std::ostream&,DayInfoStats const&);
};
BB_DECLARE_SHARED_PTR( DayInfoStats );

class DayInfoStatsFactory
{
public:
    DayInfoStatsCPtr get(bb::symbol_t const&)const;

    bb::date_t getDate() const;
    /// Returns the number of samples used
    unsigned getNumSamples() const;

    DayInfoStatsFactory( const EnvironmentPtr& env, bb::date_t const& start_dt, unsigned N, std::string const& _dbprof );
private:
    EnvironmentPtr m_env;
    typedef std::map<bb::symbol_t,DayInfoStatsCPtr> symmap_t;
    mutable symmap_t m_symmap;
    bb::date_t m_date;
    unsigned m_numsamples;
    std::string m_dbprofile;
    friend std::ostream& operator<<(std::ostream&,DayInfoStatsFactory const&);
};


BB_DECLARE_SHARED_PTR( DayInfoStatsFactory );
} //namespace bb


#endif // BB_DB_DAYINFOSTATS_H
