#ifndef BB_DB_SYMBOLTABLE_H
#define BB_DB_SYMBOLTABLE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/mapdb.h>

namespace bb {
namespace db {

class SymbolTable : public mapdb
{
public:
    /// Loads symbol mappings from the database into this mapdb.
    /// @return a code equivalent to 0 on success, non-zero code on error
    mapdb_return_t load_from_db(const EnvironmentPtr& env, const std::string& db_profile,
            const date_t& date, const std::vector<unsigned int>& sources);

private:
    using mapdb::load_from_file; // hidden, use load_from_db
};

} // namespace db
} // namespace bb

#endif // BB_DB_SYMBOLTABLE_H
