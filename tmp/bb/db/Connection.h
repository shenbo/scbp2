#ifndef BB_DB_CONNECTION_H
#define BB_DB_CONNECTION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <mysql++/mysql++.h>

#include <bb/core/smart_ptr.h>

namespace bb {

class timeval_t;
class date_t;

BB_FWD_DECLARE_SHARED_PTR( Environment );

namespace db {

typedef boost::shared_ptr<mysqlpp::Connection> ConnectionPtr;

/// Returns the ConnectionPtr corresponding to the given profile., or an invalid sp if it fails.
/// This is created once and cached (unless do_not_cache is true).
/// Profiles are stored in /with/bb/root/conf/database.lua
/// NOT THREADSAFE
ConnectionPtr getConnection( const bb::EnvironmentPtr& env, const std::string& profile
    , bool do_not_cache = false );

/// Resets the database Connection for the specified profile.
/// Profiles are stored in /with/bb/root/conf/database.lua
///
/// Note that this does not invalidate any existing ConnectionPtr held from a previous
/// call to getConnection(profile).  It only changes what future calls to getConnection return.
/// NOT THREADSAFE
void resetConnection( const std::string& profile );


/// When inserting text, any embedded "'" need to be escaped, which this does.
std::string sql_escape( const std::string& str, bool add_enclosing_quotes = false );

// convert to mysqlpp date
mysqlpp::Date date2mysqldate( const bb::date_t& date );
mysqlpp::Date tv2mysqldate( const bb::timeval_t& tv );

} // namespace db
} // namespace bb

#endif // BB_DB_CONNECTION_H
