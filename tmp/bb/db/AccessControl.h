#ifndef BB_DB_ACCESSCONTROL_H
#define BB_DB_ACCESSCONTROL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/**
 * @file AccessControl.h
 *
 * Provides helper functions for interacting with our authentication API.
 * Generally, users will drill down to the available features level and grab the
 * values for those features.  We assume the user knows what app it is, and how
 * to interpret the values.
 */

#include <list>
#include <string>

#include <bb/core/smart_ptr.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR(Environment);
namespace db {

BB_FWD_DECLARE_SHARED_PTR(MongoDBConnectionInfo);


namespace acl {

/// @return the apps that the given user has permissions for
std::list<std::string> getAppsForUser(const std::string& user, const MongoDBConnectionInfoPtr& conn);

std::list<std::string> getAppsForUser(const std::string& user,
                                      const bb::EnvironmentPtr& env, const std::string& profile );

/// @return the features in the app that the given user has permissions for
std::list<std::string> getFeaturesForUserApp(const std::string& user, const std::string& app,
                                             const MongoDBConnectionInfoPtr& conn);

std::list<std::string> getFeaturesForUserApp(const std::string& user, const std::string& app,
                                             const bb::EnvironmentPtr& env, const std::string& profile );

/// @return the value associated with the user, app, feature permission when the
///         value is a single string
std::string getValueString(const std::string& user, const std::string& app, const std::string& feature,
    const MongoDBConnectionInfoPtr& conn);

std::string getValueString(const std::string& user, const std::string& app, const std::string& feature, const bb::EnvironmentPtr& env, const std::string& profile );


/// @return the value associated with the user, app, feature permission when the
///         value is a list
std::list<std::string> getValueList(const std::string& user, const std::string& app, const std::string& feature,
    const MongoDBConnectionInfoPtr& conn);

std::list<std::string> getValueList(const std::string& user, const std::string& app, const std::string& feature, const bb::EnvironmentPtr& env, const std::string& profile );


} // namespace acl
} // namespace db
} // namespace bb

#endif // BB_DB_ACCESSCONTROL_H
