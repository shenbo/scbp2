#ifndef BB_MESSAGING_ECHELON_CLIENT_HISTNOTIFIER_H
#define BB_MESSAGING_ECHELON_CLIENT_HISTNOTIFIER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/TimeProvider.h>

#include <bb/messaging/echelon/client/INotifier.h>

namespace bb {
namespace messaging {

class HistNotifier
    : public INotifier
{
public:
    HistNotifier( const std::string& id, const bb::ITimeProviderPtr& tp )
        : m_id( id )
        , m_spTimeProvider( tp ) {}

    HistNotifier( const bb::ITimeProviderPtr& tp )
        : m_spTimeProvider( tp ) {}

    virtual bool sendRaw( const std::string& prefix, const std::string& notify_str )
    {
        if( m_id.size() )
            std::cout << m_spTimeProvider->getTime() << " (" << m_id << ") ";
        else
            std::cout << m_spTimeProvider->getTime() << ' ';

        if( prefix == "trade" )
            std::cout << "[TRADE] ";
        std::cout << notify_str << std::endl;
        return true;
    }

protected:
    std::string m_id;
    bb::ITimeProviderPtr m_spTimeProvider;
};

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_HISTNOTIFIER_H