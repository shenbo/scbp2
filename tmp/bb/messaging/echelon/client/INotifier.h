#ifndef BB_MESSAGING_ECHELON_CLIENT_INOTIFIER_H
#define BB_MESSAGING_ECHELON_CLIENT_INOTIFIER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <sstream>
#include <iostream>

#include <boost/bind.hpp>
#include <boost/function.hpp>

#include <bb/core/smart_ptr.h>

namespace bb {
namespace messaging {

template<typename T, bool check_name>
class notifiable;

class INotifier
{
public:
    virtual ~INotifier() {}

    template<typename T> bool notifyChange( const std::string& name, const T& newval )
    {
        return sendChange( name, boost::bind( &INotifier::format_value<T>, newval ) );
    }

    typedef boost::function< const std::string() > to_string_t;

    virtual bool sendRaw( const std::string& prefix, const std::string& notify_str ) = 0;

    template<typename T> static const std::string format_value( const T& val )
    {
        std::stringstream ss;
        ss << val;
        return ss.str();
    }

    class refresher
    {
    public:
        refresher( INotifier* notif )
            : m_notifier( notif )
        {}

        template<typename U, bool b> refresher& item( const notifiable<U, b>& obj )
        {
            m_ss << obj.key() << ": "
                 << INotifier::format_value( obj.get() ) << std::endl;
            return *this;
        }
        void end()
        {
            m_notifier->sendRaw( "status-refresh", m_ss.str() );
        }

    protected:
        friend class INotifier;
        refresher( const refresher& r ) : m_notifier( r.m_notifier ) {}

    protected:
        std::stringstream m_ss;
        INotifier* m_notifier;
    };

    refresher refresh() { return refresher( this ); }

    class builder
    {
    public:
        template<typename U> builder& operator<<( const U& u ) { m_ss << u; return *this; }
        builder& operator<<( std::ostream& ( *f )( std::ostream & ) ) { return end(); }
        builder& end()
        {
            m_notifier->sendRaw( m_prefix, m_ss.str() );
            return *this;
        }

    protected:
        friend class INotifier;
        builder( INotifier* notif, const std::string& prefix )
            : m_notifier( notif )
            , m_prefix( prefix ) {}
        builder( const builder& b ) : m_notifier( b.m_notifier ) {}

    protected:
        INotifier* m_notifier;
        std::string m_prefix;
        std::stringstream m_ss;
    };
    builder info() { return builder( this, "info" ); }
    builder warn() { return builder( this, "warn" ); }
    builder trade() { return builder( this, "trade" ); }

protected:
    virtual bool sendChange( const std::string& name, const std::string& newval )
    {
        return sendRaw( "status-update", name + ": " + newval );
    }

    virtual bool sendChange( const std::string& name,
                             const to_string_t& format_func )
    {
        return sendChange( name, format_func() );
    }
};

BB_DECLARE_SHARED_PTR( INotifier );

template<> inline const std::string INotifier::format_value<bool>( const bool& val )
{
    static const std::string true_s( "true" );
    static const std::string false_s( "false" );
    return val ? true_s : false_s;
}

template<> inline const std::string INotifier::format_value<std::string>( const std::string& val )
{
    return '\'' + val + '\'';
}

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_INOTIFIER_H