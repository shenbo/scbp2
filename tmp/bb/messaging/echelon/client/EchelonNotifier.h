#ifndef BB_MESSAGING_ECHELON_CLIENT_ECHELONNOTIFIER_H
#define BB_MESSAGING_ECHELON_CLIENT_ECHELONNOTIFIER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iostream>
#include <string>
#include <boost/shared_ptr.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/TimeProvider.h>

#include <bb/messaging/echelon/client/StrategyClient.h>
#include <bb/messaging/echelon/client/INotifier.h>

namespace bb {
namespace messaging {

class EchelonNotifier
    : public INotifier
{
public:
    EchelonNotifier( const bb::ITimeProviderPtr& tp, const boost::shared_ptr<StrategyClient>& sc )
        : m_spTimeProvider( tp )
        , m_spStrategyClient( sc ) {}

    boost::shared_ptr<StrategyClient> getStrategyClient() const { return m_spStrategyClient; }

protected:
    virtual bool sendRaw( const std::string& prefix, const std::string& msg )
    {
        std::string outputmsg = prefix + " " + msg;
        bool rval = m_spStrategyClient->send( outputmsg );
        std::cout << m_spTimeProvider->getTime() << " " << outputmsg << std::endl;
        return rval;
    }

    using INotifier::sendChange;

    virtual bool sendChange( const std::string& name,
                             const to_string_t& format_func )
    {
        bool rval = m_spStrategyClient->send( std::string( "status-update " ) + name + ": ", format_func );
        return rval;
    }

    bb::ITimeProviderPtr m_spTimeProvider;
    boost::shared_ptr<StrategyClient> m_spStrategyClient;
};

BB_DECLARE_SHARED_PTR( EchelonNotifier );

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_ECHELONNOTIFIER_H