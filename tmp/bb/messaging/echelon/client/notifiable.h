#ifndef BB_MESSAGING_ECHELON_CLIENT_NOTIFIABLE_H
#define BB_MESSAGING_ECHELON_CLIENT_NOTIFIABLE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <boost/shared_ptr.hpp>

#include <bb/messaging/echelon/INotifier.h>

namespace bb {
namespace messaging {

class INotifier;

template<typename T, bool check_change = false>
class notifiable
{
public:
    typedef T value_type;

    notifiable( const std::string& keyname, boost::shared_ptr<INotifier> notifier )
        : m_key( keyname )
        , m_spNotifier( notifier ) {}
    notifiable( const T& v, const std::string& keyname, boost::shared_ptr<INotifier> notifier )
        : m_val( v )
        , m_key( keyname )
        , m_spNotifier( notifier ) {}
    notifiable<T, check_change>& operator=( const T& v );
    operator T&() { return m_val; }
    operator const T&() const { return m_val; }

    const std::string& key() const { return m_key; }
    const T& get() const { return m_val; }

    void refresh()
    {
        if( m_spNotifier )
            m_spNotifier->refresh().item( *this ).end();
    }

protected:
    T m_val;
    std::string m_key;
    boost::shared_ptr<INotifier> m_spNotifier;
};

template<typename T, bool check_change> inline notifiable<T, check_change>&
notifiable<T, check_change>::operator=( const T& v )
{
    if( check_change && ( m_val == v ) )
        return *this;

    m_val = v;
    if( m_spNotifier )
        m_spNotifier->notifyChange( m_key, m_val );
    return *this;
}

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_NOTIFIABLE_H