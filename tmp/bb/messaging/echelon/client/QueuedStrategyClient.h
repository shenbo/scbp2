#ifndef BB_MESSAGING_ECHELON_CLIENT_QUEUEDSTRATEGYCLIENT_H
#define BB_MESSAGING_ECHELON_CLIENT_QUEUEDSTRATEGYCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/ListenerList.h>
#include <bb/core/SelectDispatcher.h>
#include <bb/core/queue/ThreadPipeNotifier.h>
#include <bb/core/queue/waitable_queue.h>

#include <bb/messaging/echelon/client/IStringMsgListener.h>
#include <bb/messaging/echelon/client/StrategyClient.h>

namespace bb {
namespace messaging {

class QueuedStrategyClient
    : public StrategyClient
    , public bb::ListenerList<IStringMsgListener*>
{
public:
    QueuedStrategyClient( const std::string& host, int16_t port );
    ~QueuedStrategyClient();

    void registerReadDispatch( bb::FDSet& sd );
    void registerSendDispatch( bb::SelectDispatcher& sd );
    void registerAsyncSendDispatch( bb::SelectDispatcher& sd );

    virtual bool send( const std::string& s );
    virtual bool send( const std::string& prefix, const format_func_t& format_func );

protected:
    typedef boost::function< void ( ) > action_t;
    typedef boost::function< void ( ) > async_send_t;

    typedef bb::queue::waitable_concurrent_queue< action_t, bb::queue::ThreadPipeNotifier > queue_t;
    typedef bb::queue::waitable_concurrent_queue< std::string, bb::queue::ThreadPipeNotifier > sendqueue_t;
    typedef bb::queue::waitable_concurrent_queue< async_send_t, bb::queue::ThreadPipeNotifier > async_sendqueue_t;

protected:
    virtual void onConnected();
    virtual void onDisconnected();
    virtual void onMessage( const std::string& msg );
    virtual void processMessage( const std::string& msg );

    void postAction( const action_t& action );
    void postAsyncSend( const async_send_t& async_send );

    void doAction();
    void processQueuedSend();
    void processQueuedAsyncSend();

private:
    void formatAndSend(
        const std::string& prefix,
        const format_func_t& format_func );

protected:
    bb::Subscription m_subSend;
    bb::Subscription m_subRead;
    bb::Subscription m_subAsyncSend;

    queue_t m_actionQueue;
    sendqueue_t m_sendQueue;
    async_sendqueue_t m_asyncSendQueue;
};

BB_DECLARE_SHARED_PTR( QueuedStrategyClient );

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_QUEUEDSTRATEGYCLIENT_H