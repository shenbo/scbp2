#ifndef BB_MESSAGING_ECHELON_CLIENT_STRATEGYCLIENTRUNNER_H
#define BB_MESSAGING_ECHELON_CLIENT_STRATEGYCLIENTRUNNER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include "QueuedTdClient.h"
#include "Quitter.h"

namespace bb {
namespace messaging {

class StrategyClient;

class StrategyClientRunner
{
public:
    StrategyClientRunner( const boost::shared_ptr<StrategyClient>& sc );
    virtual ~StrategyClientRunner();

    void start();
    void stop();

protected:
    virtual void run();

protected:
    Quitter m_quit;
    boost::shared_ptr< boost::thread > m_spRunThread;
    boost::shared_ptr< StrategyClient > m_spStrategyClient;
};

BB_DECLARE_SHARED_PTR( StrategyClientRunner );

namespace echelon {

class QueuedClientRunner
{
public:
    QueuedClientRunner( const boost::shared_ptr<QueuedClient>& qc );
    virtual ~QueuedClientRunner();

    void start();
    void stop();

protected:
    virtual void run();

protected:
    Quitter m_quit;
    boost::shared_ptr< boost::thread > m_spRunThread;
    boost::shared_ptr< QueuedClient > m_spQueuedClient;
};

} // namespace echelon

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_STRATEGYCLIENTRUNNER_H