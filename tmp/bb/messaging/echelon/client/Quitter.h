#ifndef BB_MESSAGING_ECHELON_CLIENT_QUITTER_H
#define BB_MESSAGING_ECHELON_CLIENT_QUITTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/bind.hpp>

#include <bb/core/ThreadPipe.h>
#include <bb/core/SelectDispatcher.h>

namespace bb {
namespace messaging {

class Quitter
{
public:
    Quitter() : bquit( false )
    {
        makeThreadPipe( m_reader, m_writer );
    }
    void signalQuit() { bquit = true; m_writer.sendWakeup( 'Q' ); }
    void registerSelect( bb::Subscription& sub, bb::SelectDispatcher& sd )
    {
        sd.createReadCB( sub, m_reader.getFD()
                         , boost::bind( &Quitter::onQuit, &bquit ) );
    }

    operator bool() { return bquit; }
    void reset()
    {
        bquit = false;
        makeThreadPipe( m_reader, m_writer );
    }

protected:
    static void onQuit( bool* bquit ) { ( *bquit ) = true; }

protected:
    bb::ThreadPipeWriter m_writer;
    bb::ThreadPipeReader m_reader;
    bool bquit;
};

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_QUITTER_H