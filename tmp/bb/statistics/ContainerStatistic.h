#ifndef BB_STATISTICS_CONTAINERSTATISTIC_H
#define BB_STATISTICS_CONTAINERSTATISTIC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <limits>
#include <numeric>

namespace bb {
namespace statistics {

template<class RandAccessIter>
double mean( RandAccessIter begin, RandAccessIter end )
{
    if( begin == end )
    {
        return std::numeric_limits<double>::quiet_NaN();
    }

    return std::accumulate( begin, end , 0.0 ) / std::distance( begin, end );
}

template<class RandAccessIter>
double median( RandAccessIter begin, RandAccessIter end )
{
    if( begin == end )
    {
        return std::numeric_limits<double>::quiet_NaN();
    }

    std::size_t size = end - begin;
    std::size_t middleIdx = size / 2;

    RandAccessIter target = begin + middleIdx;

    std::nth_element( begin, target, end );

    // Odd number of elements
    if( size % 2 != 0 )
    {
        return *target;
    }
    // Even number of elements
    else
    {
        double t = *target;
        RandAccessIter targetNeighbor = target - 1;

        std::nth_element( begin, targetNeighbor, end );

        return ( t + *targetNeighbor ) / 2.0;
    }
}

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_CONTAINERSTATISTIC_H
