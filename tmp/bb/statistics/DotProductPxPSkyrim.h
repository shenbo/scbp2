#ifndef BB_STATISTICS_DOTPRODUCTPXPSKYRIM_H
#define BB_STATISTICS_DOTPRODUCTPXPSKYRIM_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/DotProductPxP.h>
#include <bb/db/DayInfo.h>
#include <boost/unordered_map.hpp>

namespace bb {

namespace statistics{

/**
 * An DotProductPxPSkyrim Adds a few functionalities used by SkyrimBaseline1.
 **/
class DotProductPxPSkyrim : public DotProductPxP
{
public:
	DotProductPxPSkyrim( const instrument_t& instr,bool updatealways=false );

	/// Adds a component to the DotProductPxP. The same PriceProvider can be added multiple times.
	void addComponent( IPriceProviderCPtr const& spPP, double weight );

	double getRefPrice(bool *pSuccess = NULL) const;

	bool isPriceOK() const;

	void onComponentPriceChanged( const IPriceProvider& pp, int comp_index );

	void setDebugMode( bool debugMode );

	void _update() const {};

	void _update( int comp_index ) const;

	mutable bool m_debug;
	mutable int debugCounter;
	int m_comp_index;
	mutable std::string debugTitle;
	mutable std::string debugWeightedString;
	mutable std::string debugString;

private:
	typedef boost::unordered_map<int,double> int2double_map;
	typedef boost::unordered_map<int,bool> int2bool_map;

	instrument_t m_instr;
	mutable bool m_ok;
	mutable bool m_dirty;
	mutable double m_value;
	mutable double m_percentage_fail;
	mutable double m_old_value;
	timeval_t m_last_change_tv;
	ComponentList m_comps;
	bool m_updatealways;
	mutable int2double_map m_lastPx_input;
	mutable int2bool_map m_isOk_input;
	mutable int m_isOk_count;

};
BB_DECLARE_SHARED_PTR( DotProductPxPSkyrim );

/// PxPSpec for building a DotProductPxPSkyrim
class DotProductPxPSkyrimSpec : public DotProductPxPSpec
{
public:
	BB_DECLARE_SCRIPTING();

	DotProductPxPSkyrimSpec();
	DotProductPxPSkyrimSpec( const DotProductPxPSkyrimSpec& a, const boost::optional<InstrSubst> &instrSubst );

	virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
	virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;

	instrument_t m_instrument;
};
BB_DECLARE_SHARED_PTR( DotProductPxPSkyrimSpec );

}

} // namespace bb

#endif // BB_STATISTICS_DOTPRODUCTPXPSKYRIM_H
