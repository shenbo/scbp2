#ifndef BB_STATISTICS_ROLLINGWINDOW_H
#define BB_STATISTICS_ROLLINGWINDOW_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <deque>
#include <utility>

#include <bb/core/EventPublisher.h>
#include <bb/core/PriceSize.h>
#include <bb/core/Scripting.h>
#include <bb/core/ptime.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>

namespace bb {

class IDataRequirements;
class Subscription;
struct LuaPrintSettings;

BB_FWD_DECLARE_SHARED_PTR( IPriceSizeProvider );
BB_FWD_DECLARE_SHARED_PTR( IPxSzProviderSpec );

namespace statistics {

class IStatisticBuilder;

class DataPoint : private std::pair<bb::timeval_t, bb::PriceSize>
{
public:
    DataPoint()
        : std::pair<bb::timeval_t, bb::PriceSize>( bb::timeval_t(), bb::PriceSize() )
    {}
    DataPoint( const bb::timeval_t& timeval )
        : std::pair<bb::timeval_t, bb::PriceSize>( timeval, bb::PriceSize() )
    {}
    DataPoint( const bb::timeval_t& timeval, const bb::PriceSize& priceSize )
        : std::pair<bb::timeval_t, bb::PriceSize>( timeval, priceSize )
    {}

    const bb::timeval_t& getTimeval() const { return this->first; }
    const bb::PriceSize& getPriceSize() const { return this->second; }
    double getPrice() const { return getPriceSize().getPrice(); }
    int32_t getSize() const { return getPriceSize().getSize(); }
};

class IRollingWindow;

class IRollingWindowListener : public bb::IEventSubscriber
{
public:
    virtual ~IRollingWindowListener() {}

    virtual void onRollingWindowChanged( const IRollingWindow* rollingWindow ) = 0;
};
BB_DECLARE_SHARED_PTR( IRollingWindowListener );

class IRollingWindow : public std::deque<DataPoint>
{
public:
    typedef EventPublisher<IRollingWindowListener> Listeners_t;

    static const bb::timeval_t& getTimeval( const DataPoint& dataPoint );
    static const bb::PriceSize& getPriceSize( const DataPoint& dataPoint );
    static double getPrice( const DataPoint& dataPoint );
    static int32_t getSize( const DataPoint& dataPoint );
    static DataPoint add( const DataPoint& lhs, const DataPoint& rhs );
    static DataPoint addNotional( const DataPoint& lhs, const DataPoint& rhs );

    virtual ~IRollingWindow() {}

    virtual bool isOK() const = 0;
    virtual bb::timeval_t getLastChangeTime() const = 0;
    virtual bb::ptime_duration_t getInterval() const = 0;

    virtual bool addRollingWindowListener( IRollingWindowListener* listener ) = 0;
    virtual bool removeRollingWindowListener( IRollingWindowListener* listener ) = 0;
};
BB_DECLARE_SHARED_PTR( IRollingWindow );

BB_FWD_DECLARE_SHARED_PTR( IRollingWindowSpec );
class IRollingWindowSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~IRollingWindowSpec() {}

   /// Instantiates a concrete IRollingWindow according to the Spec
    virtual IRollingWindowPtr build( IStatisticBuilder* builder ) const = 0;

    /// deep copy this IRollingWindowSpec
    virtual IRollingWindowSpec* clone() const = 0;
    inline static IRollingWindowSpec* clone( const IRollingWindowSpec* e ) { return e ? e->clone() : NULL; }
    inline static IRollingWindowSpec* clone( IRollingWindowSpecCPtr e ) { return clone( e.get() ); }

    /// Combines this IRollingWindowSpec into the given hash value
    virtual void hashCombine( size_t& result ) const = 0;

    /// Compares two data sampler specs.
    virtual bool compare( const IRollingWindowSpec* other ) const = 0;

    /// Prints the lua representation of this RollingWindowSpec.
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const = 0;

    /// ensures sure that all fields have been set correctly, or throws runtime_error
    virtual void checkValid() const {}

    /// Propagates all the required data (instr/src pairs) to the IDataRequirements.
    virtual void getDataRequirements( IDataRequirements* rqs ) const = 0;
};

std::size_t hash_value( const IRollingWindowSpec& a );

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG

bool operator==( const IRollingWindowSpec& a, const IRollingWindowSpec& b );
inline bool operator!=( const IRollingWindowSpec& a, const IRollingWindowSpec& b ) { return !( a == b ); }

std::ostream& operator<<( std::ostream& out, const IRollingWindowSpec& spec );
void luaprint( std::ostream& out, IRollingWindowSpec const& ds, LuaPrintSettings const& ps );

#endif // !SWIG

// helpers for hash_map
struct RollingWindowSpecHasher : public std::unary_function<size_t, IRollingWindowSpecCPtr>
{
    size_t operator()( const IRollingWindowSpecCPtr& a ) const { return hash_value( *a ); }
};

struct RollingWindowSpecComparator : public std::binary_function<bool, IRollingWindowSpecCPtr, IRollingWindowSpecCPtr>
{
    bool operator()( const IRollingWindowSpecCPtr& a, const IRollingWindowSpecCPtr& b ) const { return *a == *b; }
};

class RollingWindowListenerImpl : public IRollingWindow
{
public:
    virtual bool addRollingWindowListener( IRollingWindowListener* listener );
    virtual bool removeRollingWindowListener( IRollingWindowListener* listener );

protected:
    void notifyRollingWindowListeners();

    Listeners_t m_listeners;
};

class RollingWindow : public RollingWindowListenerImpl
{
public:
    RollingWindow( const bb::IPriceSizeProviderPtr& pxszp, const bb::ptime_duration_t& interval );

    virtual bool isOK() const { return size() > 0; }
    virtual bb::timeval_t getLastChangeTime() const { return m_lastChangeTv; }
    virtual bb::ptime_duration_t getInterval() const { return m_interval; }

protected:
    void onUpdate( const bb::IPriceSizeProvider& pxszp );
    void notifyRollingWindowListeners( const bb::timeval_t& changeTv );

private:
    bb::IPriceSizeProviderPtr m_pxszp;
    const bb::ptime_duration_t m_interval;
    bb::Subscription m_sub;
    bb::timeval_t m_lastChangeTv;
};
BB_DECLARE_SHARED_PTR( RollingWindow );

class RollingWindowSpec : public IRollingWindowSpec
{
public:
    BB_DECLARE_SCRIPTING();

    RollingWindowSpec();
    RollingWindowSpec( const RollingWindowSpec& e );

    virtual IRollingWindowPtr build( IStatisticBuilder* builder ) const;

    virtual RollingWindowSpec* clone() const;
    virtual void checkValid() const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IRollingWindowSpec* other ) const;
    virtual void getDataRequirements( IDataRequirements* rqs ) const;

    bb::IPxSzProviderSpecPtr m_pxszp;
    bb::ptime_duration_t m_interval;
};
BB_DECLARE_SHARED_PTR( RollingWindowSpec );

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_ROLLINGWINDOW_H

