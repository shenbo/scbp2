#ifndef BB_STATISTICS_STATISTICBUILDER_H
#define BB_STATISTICS_STATISTICBUILDER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <map>

#include <bb/clientcore/PriceProviderBuilder.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/statistics/DataSampler.h>
#include <bb/statistics/RollingStatistic.h>
#include <bb/statistics/RollingWindow.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( IBookBuilder );
BB_FWD_DECLARE_SHARED_PTR( SourceTickFactory );
BB_FWD_DECLARE_SHARED_PTR( PriceProviderBuilder );

namespace statistics {

class IStatisticBuilder
{
public:
    virtual ~IStatisticBuilder() {}

    virtual IDataSamplerPtr buildDataSampler( IDataSamplerSpecCPtr spec ) = 0;
    virtual IRollingStatisticPtr buildRollingStatistic( IRollingStatisticSpecCPtr spec ) = 0;

    virtual bb::IBookBuilderPtr getBookBuilder() const = 0;
    virtual bb::PriceProviderBuilderPtr getPxPBuilder() const = 0;
    virtual bb::SourceTickFactoryPtr getSourceTickFactory() const = 0;
    virtual bb::ClockMonitorPtr getClockMonitor() const = 0;

    virtual void setVerboseLevel( int verboseLevel ) = 0;
    virtual int getVerboseLevel() const = 0;
};
BB_DECLARE_SHARED_PTR( IStatisticBuilder );

/// StatisticBuilder takes a IDataSamplerSpec structure (i.e. a description of a
/// signal that needs to be built) and builds it.
class StatisticBuilder : public IStatisticBuilder
{
public:
    StatisticBuilder( bb::ClientContextPtr clientContext
                        , bb::IBookBuilderPtr bookBuilder
                        , int verboseLevel
        );

    // IStatisticBuilder implementation
    virtual IDataSamplerPtr buildDataSampler( IDataSamplerSpecCPtr spec );
    virtual IRollingStatisticPtr buildRollingStatistic( IRollingStatisticSpecCPtr spec );
    virtual IRollingWindowPtr buildRollingWindow( IRollingWindowSpecCPtr spec );

    virtual bb::IBookBuilderPtr getBookBuilder() const { return m_bookBuilder; }
    virtual bb::PriceProviderBuilderPtr getPxPBuilder() const { return m_pxpBuilder; }
    virtual bb::SourceTickFactoryPtr getSourceTickFactory() const { return m_pxpBuilder->getSourceTickFactory(); }
    virtual bb::ClockMonitorPtr getClockMonitor() const { return getClientContext()->getClockMonitor(); }

    virtual void setVerboseLevel( int verboseLevel ) { m_verboseLevel = verboseLevel; }
    virtual int getVerboseLevel() const { return m_verboseLevel; }

    // Functions used by Spec subclasses in building their concrete PriceProviders
    ClientContextPtr getClientContext() const { return m_clientContext.lock(); }
    IMStreamManagerPtr getMStreamManager() const { return getClientContext()->getMStreamManager(); }
    EventDistributorPtr getEventDistributor() const { return getClientContext()->getEventDistributor(); }

protected:
    ClientContextWeakPtr m_clientContext;
    IBookBuilderPtr m_bookBuilder;
    PriceProviderBuilderPtr m_pxpBuilder;

    int m_verboseLevel;

    // a list of the data samplers that already exist
    typedef bbext::hash_map<IDataSamplerSpecCPtr, IDataSamplerWeakPtr, DataSamplerSpecHasher, DataSamplerSpecComparator> DataSamplers_t;
    DataSamplers_t m_alreadyBuiltDataSamplers;

    SpamFilter m_harvestedDataSamplerSpam;

    // a list of the rolling statistics that already exist
    typedef bbext::hash_map<IRollingStatisticSpecCPtr, IRollingStatisticWeakPtr, RollingStatisticSpecHasher, RollingStatisticSpecComparator> RollingStatistics_t;
    RollingStatistics_t m_alreadyBuiltRollingStatistics;

    SpamFilter m_harvestedRollingStatisticSpam;

    // a list of the rolling windows that already exist
    typedef bbext::hash_map<IRollingWindowSpecCPtr, IRollingWindowWeakPtr, RollingWindowSpecHasher, RollingWindowSpecComparator> RollingWindows_t;
    RollingWindows_t m_alreadyBuiltRollingWindows;

    SpamFilter m_harvestedRollingWindowSpam;
};
BB_DECLARE_SHARED_PTR( StatisticBuilder );

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_STATISTICBUILDER_H
