#ifndef BB_STATISTICS_ROLLINGSTATISTIC_H
#define BB_STATISTICS_ROLLINGSTATISTIC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <ostream>
#include <string>

#include <bb/core/EventPublisher.h>
#include <bb/core/Scripting.h>
#include <bb/core/smart_ptr.h>

#include <bb/statistics/RollingWindow.h>

namespace bb {

class timeval_t;
class IDataRequirements;
class PriceSize;
struct LuaPrintSettings;

BB_FWD_DECLARE_SHARED_PTR( IPxSzProviderSpec );

namespace statistics {

class IStatisticBuilder;
class IRollingStatistic;

class IRollingStatisticListener : public bb::IEventSubscriber
{
public:
    virtual ~IRollingStatisticListener() {}

    virtual void onRollingStatisticChanged( const IRollingStatistic* rollingStatistic ) = 0;
};

class IRollingStatistic : public IRollingWindowListener
{
public:
    typedef EventPublisher<IRollingStatisticListener> Listeners_t;

    virtual ~IRollingStatistic() {}

    virtual const std::string& getDesc() const = 0;
    virtual bb::timeval_t getLastChangeTime() const = 0;
    virtual const bb::PriceSize& getValue() const = 0;
    virtual bool isOK() const = 0;

    virtual void onRollingWindowChanged( const IRollingWindow* rollingWindow ) = 0;

    virtual bool addRollingStatisticListener( IRollingStatisticListener* listener ) = 0;
    virtual bool removeRollingStatisticListener( IRollingStatisticListener* listener ) = 0;
};
BB_DECLARE_SHARED_PTR( IRollingStatistic );

BB_FWD_DECLARE_SHARED_PTR( IRollingStatisticSpec );
class IRollingStatisticSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~IRollingStatisticSpec() {}

    virtual std::string getDescription() const = 0;

   /// Instantiates a concrete IRollingStatistic according to the Spec
    virtual IRollingStatisticPtr build( IStatisticBuilder* builder ) const = 0;

    /// deep copy this IRollingStatisticSpec
    virtual IRollingStatisticSpec* clone() const = 0;
    inline static IRollingStatisticSpec* clone( const IRollingStatisticSpec* e ) { return e ? e->clone() : NULL; }
    inline static IRollingStatisticSpec* clone( IRollingStatisticSpecCPtr e ) { return clone( e.get() ); }

    /// Combines this IRollingStatisticSpec into the given hash value
    virtual void hashCombine( size_t& result ) const = 0;

    /// Compares two data sampler specs.
    virtual bool compare( const IRollingStatisticSpec* other ) const = 0;

    /// Prints the lua representation of this RollingStatisticSpec.
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const = 0;

    /// ensures sure that all fields have been set correctly, or throws runtime_error
    virtual void checkValid() const {}

    /// Propagates all the required data (instr/src pairs) to the IDataRequirements.
    virtual void getDataRequirements( IDataRequirements* rqs ) const = 0;
};

class RollingStatisticListenerImpl : public IRollingStatistic
{
public:
    virtual bool addRollingStatisticListener( IRollingStatisticListener* listener );
    virtual bool removeRollingStatisticListener( IRollingStatisticListener* listener );

protected:
    void notifyRollingStatisticListeners();

    Listeners_t m_listeners;
};

class RollingStatisticImpl : public RollingStatisticListenerImpl
{
public:
    virtual ~RollingStatisticImpl();

    virtual const std::string& getDesc() const { return m_desc; }
    virtual bb::timeval_t getLastChangeTime() const { return m_lastChangeTv; }
    virtual const bb::PriceSize& getValue() const { return m_pxsz; }
    virtual bool isOK() const { return m_isOK; }

    virtual void onRollingWindowChanged( const IRollingWindow* rollingWindow ) = 0;

protected:
    RollingStatisticImpl( const IRollingWindowPtr& rollingWindow, const std::string& desc );

    void notifyRollingStatisticListeners( const bb::timeval_t& changeTv );

protected:
    const IRollingWindowPtr m_rollingWindow;
    const std::string m_desc;
    bb::PriceSize m_pxsz;
    bool m_isOK;
    bb::timeval_t m_lastChangeTv;
};
BB_DECLARE_SHARED_PTR( RollingStatisticImpl );

class RollingStatisticSpec : public IRollingStatisticSpec
{
public:
    BB_DECLARE_SCRIPTING();

    RollingStatisticSpec();
    RollingStatisticSpec( const RollingStatisticSpec& e );

    virtual std::string getDescription() const { return m_description; }

    virtual void checkValid() const;
    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IRollingStatisticSpec* other ) const;
    virtual void getDataRequirements( IDataRequirements* rqs ) const;

    std::string m_description;
    bb::IPxSzProviderSpecPtr m_pxszp;
    bb::ptime_duration_t m_interval;
};
BB_DECLARE_SHARED_PTR( RollingStatisticSpec );

std::size_t hash_value( const IRollingStatisticSpec& a );

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG

bool operator==( const IRollingStatisticSpec& a, const IRollingStatisticSpec& b );
inline bool operator!=( const IRollingStatisticSpec& a, const IRollingStatisticSpec& b ) { return !( a == b ); }

std::ostream& operator<<( std::ostream& out, const IRollingStatisticSpec& spec );
void luaprint( std::ostream& out, IRollingStatisticSpec const& ds, LuaPrintSettings const& ps );

#endif // !SWIG

// helpers for hash_map
struct RollingStatisticSpecHasher : public std::unary_function<size_t, IRollingStatisticSpecCPtr>
{
    size_t operator()( const IRollingStatisticSpecCPtr& a ) const { return hash_value( *a ); }
};

struct RollingStatisticSpecComparator : public std::binary_function<bool, IRollingStatisticSpecCPtr, IRollingStatisticSpecCPtr>
{
    bool operator()( const IRollingStatisticSpecCPtr& a, const IRollingStatisticSpecCPtr& b ) const { return *a == *b; }
};

class RollingMean : public RollingStatisticImpl
{
public:
    RollingMean( const IRollingWindowPtr& rollingWindow, const std::string& desc )
        : RollingStatisticImpl( rollingWindow, desc )
    {}

protected:
    virtual void onRollingWindowChanged( const IRollingWindow* rollingWindow );
};
BB_DECLARE_SHARED_PTR( RollingMean );

class RollingMeanSpec : public RollingStatisticSpec
{
public:
    BB_DECLARE_SCRIPTING();

    RollingMeanSpec();
    RollingMeanSpec( const RollingMeanSpec& e );

    virtual IRollingStatisticPtr build( IStatisticBuilder* builder ) const;

    virtual RollingMeanSpec* clone() const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
};
BB_DECLARE_SHARED_PTR( RollingMeanSpec );

class RollingVwap : public RollingStatisticImpl
{
public:
    RollingVwap( const IRollingWindowPtr& rollingWindow, const std::string& desc )
        : RollingStatisticImpl( rollingWindow, desc )
    {}

protected:
    virtual void onRollingWindowChanged( const IRollingWindow* rollingWindow );
};
BB_DECLARE_SHARED_PTR( RollingVwap );

class RollingVwapSpec : public RollingStatisticSpec
{
public:
    BB_DECLARE_SCRIPTING();

    RollingVwapSpec();
    RollingVwapSpec( const RollingVwapSpec& e );

    virtual IRollingStatisticPtr build( IStatisticBuilder* builder ) const;

    virtual RollingVwapSpec* clone() const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
};
BB_DECLARE_SHARED_PTR( RollingVwapSpec );

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_ROLLINGSTATISTIC_H
