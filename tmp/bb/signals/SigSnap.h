#ifndef BB_SIGNALS_SIGSNAP_H
#define BB_SIGNALS_SIGSNAP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/circular_buffer.hpp>
#include <boost/type_traits.hpp>
#include <vector>
#include <algorithm>

#include <bb/clientcore/ClockMonitor.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

template<typename T>
class SnapshotBase {
public:
    class error { };

    typedef std::vector<double> weight_set;
    typedef std::vector<unsigned int> interval_set;

    void setSize(unsigned int size);
    void setInterval(unsigned int i, unsigned int interval);

    /// Snapshot expects to be called per beat, which is the
    /// granularity of time.
    void onBeat(const T &t);
    unsigned int getIntervalAt(unsigned int i) const;

    unsigned int dataSize() const { return data.size(); }
    unsigned int intSize() const { return intervals.size(); }
    void reset();

protected:
    SnapshotBase(const T &default_value);
    /// pushes variable T in as the most recent datapoint.
    void push_dp(const T &t);
    void resize_data(unsigned int s);


    T def_val;

    // note -- most recent datapoint at front of data and weights

    typedef boost::circular_buffer<T> T_dq;
    T_dq data; // size is max of intervals

    interval_set intervals;
};

template<typename T, class Enable=void>
class Snapshot {};

template<typename T>
class Snapshot<T, typename boost::enable_if < boost::is_class<T> >::type > : public SnapshotBase<T>
{
    public:
        // Classes ( such as vectors etc ) are expensive to copy. We return those by const ref.
        Snapshot(T const & val): SnapshotBase<T>(val){};
        T const & getValAt(unsigned int i) const
        {
            return likely ( i < this->intervals.size() ) ? getDataAt(this->intervals[i]) : this->def_val;
        }
        T const & getDataAt(unsigned int i) const
        {
            return likely ( i < this->data.size() ) ? this->data[i] : this->def_val;
        }
};

template<typename T>
class Snapshot<T, typename boost::disable_if < boost::is_class<T> >::type > : public SnapshotBase<T>
{
    public:
        // Typically, non-class types are 64 bits at most and cheaper to just copy rather than return a reference to.
        // Just return these by value. ( todo: add exceptions for say __uint128_t should the need arise )
        Snapshot(T const val): SnapshotBase<T>(val){};
        T getValAt(unsigned int i) const
        {
            return likely ( i < this->intervals.size() ) ? getDataAt(this->intervals[i]) : this->def_val;
        }
        T getDataAt(unsigned int i) const
        {
            return likely ( i < this->data.size() ) ? this->data[i] : this->def_val;
        }
};

template<typename T>
SnapshotBase<T>::SnapshotBase(const T &default_value)
    : def_val(default_value)
{ }

template<typename T>
void SnapshotBase<T>::setSize(unsigned int s)
{
    intervals.resize(s);

    for (unsigned int i = 0; i < s; i++)
        intervals[i] = 0;
}

template<typename T>
void SnapshotBase<T>::setInterval(unsigned int i, unsigned int interval)
{
    BB_THROW_EXASSERT_SSX ( intervals.size() > i, "chosen interval is out of range" );
    intervals[i] = interval;
    resize_data(*std::max_element(intervals.begin(), intervals.end()) + 1);
}


template<typename T>
void SnapshotBase<T>::onBeat(const T &t)
{
    push_dp(t);
}

template<typename T>
unsigned int SnapshotBase<T>::getIntervalAt(unsigned int i) const
{
    return intervals[i];
}

template<typename T>
void SnapshotBase<T>::reset()
{
    data.clear();
}


template<typename T>
void SnapshotBase<T>::push_dp(const T &t)
{
    data.push_front(t);
}

template<typename T>
void SnapshotBase<T>::resize_data(unsigned int s)
{
    if ( s > data.capacity() )
    {
        data.set_capacity(s);
    }
}



// Some useful implementations.


/// Provides a function (check) that can be called with a symbol and
/// price, but only pays attention to it if it's a specified interval
/// after the last call. the value by which the weighted snapshot
/// should be adjusted to produce a signal in the correct range.
/// Offset needs to be defined in the implementing class, which must
/// also call check at least once every interval.
class SigSnap
    : public SignalSmonImpl
{
public:
    typedef std::vector<unsigned int> intervals;

    /// N is the number of weights, which must be equal to the number of intervals.
    SigSnap( const instrument_t& instr, const std::string &desc,
             ClockMonitorPtr cm,
             IPriceProviderPtr spRefPP, IPriceProviderPtr spInputPP,
             ptime_duration_t _interval,
             const intervals &interval_list, int vbose,
             ReturnMode returnMode,
             bool include_stddev );

    virtual void reset();

    void setInterval(unsigned int i, unsigned int j); // J is in multiples of INTERVAL

protected:
    // from ClockListener interface
    virtual void onWakeupCall(const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

    virtual void check(timeval_t curtime, double px);
    virtual void recomputeState() const;

    //    typedef bb::auto_vector< Snapshot<double> > snapshot_set;
    //    snapshot_set snapshots;
    Snapshot<double> m_snapshot;

    unsigned int m_num;
    timeval_t m_last_check;
    ptime_duration_t m_interval;
    IPriceProviderPtr m_spRefPP, m_spInputPP;
    Subscription m_spRefppSub, m_spInputPPSub;
    ReturnMode m_returnMode;
    bool m_includeStddev;

    static const int R_CHECK = cm::USER_REASON + 1;
};
BB_DECLARE_SHARED_PTR(SigSnap);


/// SignalSpec for SigSnap
class SigSnapSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigSnapSpec()
        : m_returnMode(DIFF)
        , m_includeStddev( false )
    {
    }

    SigSnapSpec(const SigSnapSpec &e);

    virtual instrument_t getInstrument() const { return m_inputPxP->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigSnapSpec *clone() const;

    IPxProviderSpecPtr m_inputPxP;
    ptime_duration_t m_interval;
    std::vector<unsigned int> m_intervals;
    ReturnMode m_returnMode;
    bool m_includeStddev;
};
BB_DECLARE_SHARED_PTR(SigSnapSpec);

class SigSnapSignal
    : public SignalSmonImpl
//    , private ISignalListener
{
public:
    typedef std::vector<unsigned int> intervals;

    /// N is the number of weights, which must be equal to the number of intervals.
    SigSnapSignal( const instrument_t& instr, const std::string &desc,
                   ClockMonitorPtr cm,
                   IPriceProviderPtr spRefPP,
                   ISignalPtr spInputSignal,
                   uint32_t inputIndex,
                   ptime_duration_t _interval,
                   const intervals &interval_list, int vbose,
                   ReturnMode returnMode,
                   bool include_stddev
        );

    SigSnapSignal( const instrument_t& instr, const std::string &desc,
                   ClockMonitorPtr cm,
                   ISignalPtr spInputSignal,
                   uint32_t inputIndex,
                   ptime_duration_t _interval,
                   const intervals &interval_list, int vbose,
                   ReturnMode returnMode,
                   bool include_stddev
        );

    /// Destructor
    virtual ~SigSnapSignal();

    virtual void reset();

    void setInterval(unsigned int i, unsigned int j); // J is in multiples of INTERVAL

protected:
    // from ClockListener interface
    virtual void onWakeupCall(const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

    virtual void check(timeval_t curtime, double px);
    virtual void recomputeState() const;

    Snapshot<double> m_snapshot;

    unsigned int m_num;
    timeval_t m_last_check;
    ptime_duration_t m_interval;
    IPriceProviderPtr m_spRefPP;
    ISignalPtr m_spInputSignal;
    uint32_t m_inputIndex;
    ReturnMode m_returnMode;
    bool m_includeStddev;

    static const int R_CHECK = cm::USER_REASON + 1;
};
BB_DECLARE_SHARED_PTR(SigSnapSignal);


/// SignalSpec for SigSnapSignal
class SigSnapSignalSpec : public NoRefPxSignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigSnapSignalSpec()
        : m_returnMode(DIFF)
        , m_includeStddev( false )
    {
    }

    SigSnapSignalSpec(const SigSnapSignalSpec &e);

    virtual instrument_t getInstrument() const { return  m_inputSignal->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigSnapSignalSpec *clone() const;

    IPxProviderSpecPtr m_refPxP;
    ISignalSpecPtr m_inputSignal;
    uint32_t m_inputIndex;
    ptime_duration_t m_interval;
    std::vector<unsigned int> m_intervals;
    ReturnMode m_returnMode;
    bool m_includeStddev;
};
BB_DECLARE_SHARED_PTR(SigSnapSpec);



} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGSNAP_H
