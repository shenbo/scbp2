#ifndef BB_SIGNALS_OVARGENCOMMON_H
#define BB_SIGNALS_OVARGENCOMMON_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/signals/SignalBuilder.h>

/// Some random crap that's shared between libsignals & ovargen.

namespace bb {
namespace signals {

BB_FWD_DECLARE_SHARED_PTR(ISignal);

/// Appends the date to the fileBase, unless the path already contains the date.
/// This is how signalgen/ovargen chooses its output file name.
std::string filenameAddDate(std::string fileBase, int ymdDate);

enum CacheMode
{
    READ,
    READ_WRITE,
    CACHE_ONLY // No data will be generated except what's in the cache.
};

CacheMode parseCacheWriteModeArg(std::string arg);

/// Creates a SignalBuilder with the given cache modes.
ISignalBuilderPtr createSignalBuilder(ClientContextPtr cc, IBookBuilderPtr bookBuilder, CacheMode mode,
        std::string cacheURL, bool runLive, int vbose);

namespace ovargen
{
    // This is used by signal setup to cache the dependent variable if --ovargen-cache is specified...
    // Though signalgen doesn't need that data directly, it helps to throw them in
    // the cache so that ovargen doesn't need to rebuild the books from scratch.
    class DependentVarCacher
    {
    public:
        DependentVarCacher(ISignalBuilderPtr signalBuilder);
        void addDependentVariable(ISignalSpecPtr targetVar);

    protected:
        ISignalBuilderPtr m_signalBuilder;
        std::list<ISignalPtr> m_signals;
    };
}

}
}

#endif // BB_SIGNALS_OVARGENCOMMON_H
