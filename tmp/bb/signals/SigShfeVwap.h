#ifndef BB_SIGNALS_SIGSHFEVWAP_H
#define BB_SIGNALS_SIGSHFEVWAP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/ClockMonitor.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

#include <bb/signals/SigSnap.h>
#include <bb/clientcore/BookPriceProvider.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/ShfeTickProvider.h>



namespace bb {
namespace signals {


class SigShfeVwap
    : public SignalStateImpl
    , private ITickListener
    , private IClockListener
{
public:
    typedef std::vector<unsigned int> intervals;

    /// N is the number of weights, which must be equal to the number of intervals.
    SigShfeVwap( const instrument_t& instr, const std::string &desc,
             ClockMonitorPtr cm,
             IPriceProviderPtr spRefPP, ITickProviderPtr spTickProvider,
             ptime_duration_t _interval,
             const std::vector<double> &interval_list, const uint32_t lotSize, int vbose);

    virtual ~SigShfeVwap();

    void setInterval(unsigned int i, unsigned int j); // J is in multiples of INTERVAL

protected:
    void onRefPriceChanged( const IPriceProvider& pp );
    virtual void onWakeupCall(const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );
    // we don't care about this
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume ) {};

    void check(timeval_t curtime);
    void recomputeState() const;

    void _reset();
    ClockMonitorPtr             m_spCM;
    IPriceProviderPtr           m_spRefPP;
    Subscription                m_spRefPPSub;
    ShfeTickProviderPtr         m_shfeTickProvider;

    Snapshot<double>            m_snapshot;   // record history of refpx.
    Snapshot<double>            m_snapTurnover; // record total turnover
    Snapshot<uint32_t>          m_snapTtlvlm;   // record total volume.
    Snapshot<timeval_t>         m_snapMsgTime;  // record Msg times.

    unsigned int                m_num;          // number of lags
    ptime_duration_t            m_interval;
    uint32_t                    m_lotSize;
    
    timeval_t                   m_last_check;
    std::vector<double>			m_durations;
    static const int R_CHECK = cm::USER_REASON + 1;
};
BB_DECLARE_SHARED_PTR(SigShfeVwap);



/// SignalSpec for SigShfeVwap
class SigShfeVwapSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigShfeVwapSpec() {}
    SigShfeVwapSpec(const SigShfeVwapSpec &e);

    virtual instrument_t getInstrument() const { return m_refPxP->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigShfeVwapSpec *clone() const;


    ptime_duration_t 					m_interval;
    std::vector<double> 				m_intervals;
    source_t 							m_tickSource;
    uint32_t							m_lotSize;
};
BB_DECLARE_SHARED_PTR(SigShfeVwapSpec);



} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGSHFEVWAP_H
