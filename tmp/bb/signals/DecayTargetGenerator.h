#ifndef BB_SIGNALS_DECAYTARGETGENERATOR_H
#define BB_SIGNALS_DECAYTARGETGENERATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */
#include <boost/format.hpp>
#include <bb/core/smart_ptr.h>
#include <bb/signals/FlexOvarGenerator.h>

namespace bb {
namespace signals {

class DecayTargetGenerator
    : public FlexOvarGenerator::IGenerator
{
public:
    DecayTargetGenerator( const double& decay_rate
                          , const ptime_duration_t &sampleInterval
                          , const ptime_duration_t &sampleOffset
                          , int numSamplesOmit, int numSamplesMax);
    virtual ~DecayTargetGenerator();

    virtual void update(const FlexOvarGenerator::DataSample& slice);

    virtual bool isReady() { return m_ready; }

    virtual bool valueChanged() { return m_dirty; }

    virtual double get() { m_dirty = false; return m_currentValue; }
    virtual double getRefpx() { return m_currentRefpx; }
    virtual timeval_t getScheduledTv() { return m_currentScheduledTv; }
    virtual timeval_t getActualTv() { return m_currentActualTv; }

    virtual const std::string& getDescription() { return m_desc; }

    virtual void reset();

    virtual size_t getNumSamplesMax() const { return m_numSamplesMax; }

    virtual ptime_duration_t getTvMax() const { return m_tvMax; }

    virtual void initPrinting( const bb::ClockMonitor* cm );

    class SmoothedValue {
    public:
        SmoothedValue(timeval_t start_window, timeval_t end_window, std::vector<double> decays )
            : m_start( start_window )
            , m_end( end_window )
            , m_decays( decays )
            , m_refpx( 0.0 )
            , m_scheduledTv( timeval_t::earliest )
            , m_actualTv( timeval_t::earliest )
            , m_value( 0.0 )
            , m_ready( false )
            , m_weightIndex( 0 )
            { 
                std::cout << "Constructed Smoove: with start:" << m_start << " end:" << m_end << std::endl;
                // std::cout << "Constructed Smoove: with start:" << m_start << "  end:"
                //           << m_end << " decays:" << m_decays << std::endl;

            }
        ~SmoothedValue() { }

        void update(const FlexOvarGenerator::DataSample& slice);

        bool isReady() {return m_ready;}

        timeval_t m_start;
        timeval_t m_end;
        std::vector<double> m_decays;
        double m_refpx;
        timeval_t m_scheduledTv;
        timeval_t m_actualTv;
        double m_value;
        bool m_ready;
        uint32_t m_weightIndex;
    };
protected:
    double m_decayRate;
    ptime_duration_t m_sampleInterval;
    ptime_duration_t m_sampleOffset;
    ptime_duration_t m_tvOmit;
    ptime_duration_t m_tvMax;

    int m_numSamplesOmit;
    int m_numSamplesMax;
    int m_numValidSlices;

    std::string m_desc;

    FILE* m_outf;

    double m_currentValue;
    double m_currentRefpx;
    timeval_t m_currentScheduledTv;
    timeval_t m_currentActualTv;

    std::vector<double> m_decays;

    std::deque<SmoothedValue> m_smoothedValues;
    typedef std::deque<SmoothedValue>::iterator deqiter;

    bool m_dirty;
    bool m_ready;
};
BB_DECLARE_SHARED_PTR( DecayTargetGenerator );

/// Spec corresponding to DecayTargetGenerator
class DecayTargetGeneratorSpec
    : public FlexOvarGenerator::IGeneratorSpec
{
public:
    BB_DECLARE_SCRIPTING();

    DecayTargetGeneratorSpec() 
        : m_decayRate( 0.0 )
        , m_sampleInterval( boost::posix_time::seconds(0) )
        , m_sampleOffset( boost::posix_time::seconds(0) )
        , m_numSamplesOmit( 0 )
        , m_numSamplesMax( 0 )
    {}

    DecayTargetGeneratorSpec(const DecayTargetGeneratorSpec &e);

    virtual void hashCombine(size_t &result) const;
    virtual DecayTargetGeneratorSpec *clone() const;
    virtual bool compare(const IGeneratorSpec *other) const;

    virtual FlexOvarGenerator::IGeneratorPtr build() const;

    double m_decayRate;
    ptime_duration_t m_sampleInterval;
    ptime_duration_t m_sampleOffset;
    int m_numSamplesOmit;
    int m_numSamplesMax;
};
BB_DECLARE_SHARED_PTR(DecayTargetGeneratorSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_DECAYTARGETGENERATOR_H
