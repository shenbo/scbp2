#ifndef BB_SIGNALS_SIGNALSCONFIGFILE_H
#define BB_SIGNALS_SIGNALSCONFIGFILE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/shared_ptr.hpp>
#include <bb/core/ptime.h>
#include <bb/clientcore/SessionParams.h>
#include <bb/clientcore/SymbolsConfigFile.h>
#include <bb/signals/SignalSpec.h>
#include <bb/signals/IControlSignal.h>

namespace bb {

namespace statistics {
BB_FWD_DECLARE_SHARED_PTR( IDataSamplerSpec );
}

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( IModelDataWriterSpec );
BB_FWD_DECLARE_SHARED_PTR( ISignalPrinterSpec );

class SignalProductEntry : public ConfigFileProductEntry
{
public:
    SignalProductEntry( const instrument_t& instr, const luabind::object& fact )
        : ConfigFileProductEntry( instr, fact )
    {
    }
    void initPrintSettings( const luabind::table<>& sigConfig );
    ISignalSpecPtr getDependentVariable() const;
    statistics::IDataSamplerSpecPtr getDataSampler() const;
    IModelDataWriterSpecPtr getModelDataWriter() const;

    ptime_duration_t m_printInterval, m_printOffset;
    std::string m_printFormat;
};

/// we extend bb::ConfigFile (the clientcore version) with some additional
/// signals stuff.
class ConfigFile : public bb::SymbolsConfigFile
{
public:
    typedef bb::SymbolsConfigFile Super;
    // These two typedefs are just to make swig happy.
    typedef ConfigFileProductEntry ProductEntry;
    typedef bb::ConfigFile::ProductVec_t ProductVec_t;

    ConfigFile(bool runLive, const date_t &startDate, const date_t &endDate, LuaStatePtr config);

    virtual void loadSettings( const luabind::object& root );

    // returns a list of signals which should be created for this product
    void getSignals( std::vector<ISignalSpecCPtr>* signals, const ProductEntry& prod, bool isTarget ) const;
    virtual void getAllSignals( std::vector<ISignalSpecCPtr>* signals ) const;
    ISignalSpecPtr getDependentVariable( const ProductEntry& prod ) const;
    statistics::IDataSamplerSpecPtr getDataSampler( const ProductEntry& prod ) const;
    IModelDataWriterSpecPtr getModelDataWriter( const ProductEntry& prod ) const;

    virtual const ProductVec_t& getSignalProducts() const;

    /// Returns how often we should print offset
    virtual ptime_duration_t getPrintInterval() const;
    virtual ptime_duration_t getPrintOffset() const;
    virtual std::string getPrintFormat() const;

    const std::vector<InstrSource>& getTriggerList() const;
    const ptime_duration_t& getTriggerTimeout() const;
    bool getTriggerSmartFire() const;
    bool getTriggerOnMsg() const;

    IControlSignal::SpecPtr getControlSignalSpec() const { return m_spControlSignalSpec; }
    const SessionParams& getSessionParams() const { return m_sessionParams; }

    virtual bool isTriggerListSignalPrinterConfigured() const;
    virtual bool isSignalPrinterConfigured() const;

protected:
    virtual ProductEntryPtr createProductEntry( const instrument_t& instr, const luabind::object& factory, bool is_target );

    boost::shared_ptr<SignalProductEntry> m_spSignalTargetProduct;
    ProductVec_t m_signalProducts;
    std::vector<InstrSource> m_triggerList;
    ptime_duration_t m_triggerTimeout;
    bool m_triggerSmartFire, m_triggerOnMsg;
    IControlSignal::SpecPtr m_spControlSignalSpec;
    SessionParams m_sessionParams;
    mutable bb::statistics::IDataSamplerSpecPtr m_dataSamplerSpec;
};
BB_DECLARE_SHARED_PTR(ConfigFile);

} // namespace signals
} // namespace bb

inline const bb::ConfigFile::ProductVec_t &bb::signals::ConfigFile::getSignalProducts() const
{
    if(!m_fileLoaded)
        throw std::runtime_error("bb::ConfigFile::getSignalProducts called before a file was loaded");
    return m_signalProducts;
}

#endif // BB_SIGNALS_SIGNALSCONFIGFILE_H
