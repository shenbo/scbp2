#ifndef BB_SIGNALS_TARGETINFO_H
#define BB_SIGNALS_TARGETINFO_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/// This contains data structures with target related info that the SignalManager stores.

#include <bb/core/bbassert.h>
#include <bb/clientcore/BookSpec.h>
#include <bb/signals/SignalSpec.h>
#include <bb/signals/Signal.h>
#include <bb/signals/TargetWeights.h>

namespace bb {
namespace signals {

BB_FWD_DECLARE_SHARED_PTR(TargetWeights);
BB_FWD_DECLARE_SHARED_PTR(LuaTargetWeights);

/// per-target-symbol information which is required by the SignalManager.
class TargetInfo
{
public:
    TargetInfo(const instrument_t &instr, IBookSpecCPtr refBook, ISignalSpecListCPtr signal_specs
               , ISignalSpecPtr depvar_spec
               , int verbose
               , ptime_duration_t printInterval
               , ptime_duration_t printOffset )
        : m_instrument(instr)
        , m_refBook(refBook)
        , m_signalSpecs(signal_specs)
        , m_depvarSpec(depvar_spec)
        , m_verbose(verbose)
        , m_printInterval(printInterval)
        , m_printOffset(printOffset) {}

    const instrument_t &getInstrument() const { return m_instrument; }
    IBookSpecCPtr getRefBook() const { return m_refBook; } // used for printing.

    ISignalSpecCPtr getDependentVarSpec() const { return m_depvarSpec;}
    int getVerboseLevel() const { return m_verbose;}
    ptime_duration_t getPrintInterval() const { return m_printInterval;}
    ptime_duration_t getPrintOffset() const { return m_printOffset;}

    /// returns the weights for this symbol. Can be null if no weights were loaded.
    TargetWeightsCPtr getWeightsCalc() const { return m_weightsCalc; }
    void setWeights(TargetWeightsPtr w) { m_weightsCalc = w; }

    const ISignalSpecListCPtr getSignalSpecs() const { return m_signalSpecs; }
    WeightsCPtr getWeights() const { return m_weightsCalc->getWeights(); }

protected:
    instrument_t m_instrument;
    IBookSpecCPtr m_refBook;

    TargetWeightsPtr m_weightsCalc;
    ISignalSpecListCPtr m_signalSpecs;

    ISignalSpecPtr m_depvarSpec;
    int m_verbose;
    ptime_duration_t m_printInterval;
    ptime_duration_t m_printOffset;
};
BB_DECLARE_SHARED_PTR(TargetInfo);

} // namespace signals
} // namespace bb


#endif // BB_SIGNALS_TARGETINFO_H
