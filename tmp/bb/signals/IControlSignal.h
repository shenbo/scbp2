#ifndef BB_SIGNALS_ICONTROLSIGNAL_H
#define BB_SIGNALS_ICONTROLSIGNAL_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>
#include <bb/core/Scripting.h>

namespace bb {
namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );

// Control Signals are single value integral values
// non-zero is ON, zero is OFF

BB_FWD_DECLARE_SHARED_PTR( IControlSignal );

class IControlSignal
{
public:
    class Spec
    {
    public:
        BB_DECLARE_SCRIPTING();

        virtual ~Spec() {}
        virtual IControlSignalPtr create( const ISignalBuilderPtr& signalBuilder )
        {
            return IControlSignalPtr();
        };
    };
    BB_DECLARE_SHARED_PTR( Spec );

public:
    virtual ~IControlSignal() {}

    virtual int32_t value() const = 0;
    virtual bool isValid() const = 0;
};

BB_DECLARE_SHARED_PTR( IControlSignal );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_ICONTROLSIGNAL_H
