#ifndef BB_SIGNALS_IFILLSTATPROVIDER_H
#define BB_SIGNALS_IFILLSTATPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>

namespace bb {
namespace signals {

// The IFillStatProvider
// Provides stats regarding recent fills.
// "recent" is implementation dependent.


class IFillStatProvider {

public:

    virtual ~IFillStatProvider() { }

    virtual double getPriceStdev()=0;
    virtual double getLatestPrice()=0;

    virtual double getVolume()=0;
    virtual double getLastFillSize()=0;
};

BB_DECLARE_SHARED_PTR( IFillStatProvider );


// The IFillBaselineStatProvider
// Provides stats regarding recent fills, including how the recent
// fills compare to the product's normal behavior
// "recent" and "normal behavior" are implementation dependent.

class IFillBaselineStatProvider : public IFillStatProvider {

public:

    virtual ~IFillBaselineStatProvider() { }

    virtual double getPriceStdevBaselineZscore()=0;
    virtual double getVolumeBaselineZscore()=0;

};

BB_DECLARE_SHARED_PTR( IFillBaselineStatProvider );



} // signals
} // bb

#endif // BB_SIGNALS_IFILLSTATPROVIDER_H
