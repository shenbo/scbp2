#ifndef BB_SIGNALS_SIGPRICEDEFENSE_H
#define BB_SIGNALS_SIGPRICEDEFENSE_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/optional.hpp>
#include <bb/core/ptime.h>

#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>
#include <bb/clientcore/TickProvider.h>

#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>




namespace bb {
namespace signals {

class DefendedPrice
{
public:   
    DefendedPrice( const bb::dir_t& direction
                   , const TradeTick& trade
                   , const bb::ptime_duration_t& min_defense_spacing
                   , const bool& include_defeats
                   , const double& fuzzy_defense_range );

    DefendedPrice( const bb::dir_t& direction
                   , double price
                   , const bb::ptime_duration_t& min_defense_spacing
                   , const bool& include_defeats
                   , const double& fuzzy_defense_range );
    // Default constructor for pre-trading initialization
    DefendedPrice();

    // Decide if same or new defense. 
    void processTrade(const TradeTick& trade_tick);
    int32_t getTotalDefenseVolume() const;
    uint32_t getNumberOfAssaults() const;
    double getPrice() const {return m_price;}
    dir_t getDirection() const {return m_direction;}
    bb::timeval_t getLastAssaultTime() const {return m_lastTradeTime;}
    void setDirection( dir_t direction ) {m_direction = direction;}
    uint32_t getNumberOfDefeats() const { return m_defeats;}
    
private:
    dir_t m_direction;
    double m_price;
    bb::timeval_t m_lastTradeTime;
    bb::ptime_duration_t m_minDefenseSpacing;
    bool m_includeDefeats;
    double m_fuzzyDefenseRange;
    bb::timeval_t m_lastDefeatTime;

    std::vector<int32_t> m_defenseVolumes;
    uint32_t m_defeats;
};

class SigPriceDefense
    : public SignalStateImpl
    , private IClockListener
    , private ITickListener
    , private IBookListener
{
public:
    SigPriceDefense( const instrument_t& instr, const std::string &desc, ClockMonitorPtr spCM,
                     IBookPtr spBook, ITickProviderCPtr spTickProvider,
                     const ptime_duration_t& min_defense_spacing,
                     const ptime_duration_t& min_add_spacing,
                     const uint32_t& max_removal_defense,
                     const double& tick_size,
                     const double& nonlinearity,
                     const bool& include_defeats,
                     const bool& output_broken_only,
                     const double& fuzzy_defense_range
        );

    /// Destructor
    virtual ~SigPriceDefense();

    class MarketCmp
    {
    public:
        MarketCmp(bb::dir_t direction) : m_direction(direction) {};
        
        bool operator ()( uint32_t a, uint32_t b ) const
        {
            if( m_direction == bb::BUY )
                return b < a;
            else
                return a < b;
        }
        
        bb::dir_t m_direction;
    };

    class ExactMult
    {
    public:
        ExactMult( double d ) : m_mult( d ) {}
        
        uint32_t get() const { return m_mult; }
        friend int operator *( const ExactMult& m, double val )
        { return bb::round_to_nearest_integer( val * m.m_mult ); }
        friend int operator *( double val, const ExactMult& m )
        { return bb::round_to_nearest_integer( val * m.m_mult ); }
        
    protected:
        uint32_t m_mult;
    };

private:
    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

    // IBookListener interface
    /// Invoked when the subscribed Book changes.
    /// The levelChanged entries are negative if there is no change, or a 0-based depth.
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );

    /// Invoked when the subscribed Book is flushed.
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg )
        {};

        /// This will be called when the ITickProvider's receives a new TradeTick.
    /// That last trade tick is passed.
    /// Spurious calls are allowed.  tp will never be NULL.
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );

    // we don't care about this
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume ) {};

    void reset();
    virtual void recomputeState() const;
    void log(timeval_t time, double price, bool is_resistance, bool was_added);
    void updateBidHistory(double bid_price);
    void updateAskHistory(double ask_price);
    double getMeanDefenderDistance() const;
    double getMeanDefenseSize() const;
private:
    ClockMonitorPtr                 m_spCM;
    IBookPtr                        m_spBook;
    ITickProviderCPtr               m_spTickProvider;

    ptime_duration_t m_minDefenseSpacing;
    ptime_duration_t m_minAddSpacing;
    uint32_t m_maxRemovalDefense;
    double m_tickSize;
    double m_nonlinearity;
    bool m_includeDefeats;
    bool m_outputBrokenOnly;
    double m_fuzzyDefenseRange;
    double m_distanceSafetyFactor;

    // Default sort:  inner(lower) ask price comes first.
    typedef std::pair<uint32_t, DefendedPrice> defender_pair_t;
    std::map<uint32_t, DefendedPrice, MarketCmp> m_askDefenders;
    // Sorted so inner (higher) bids come first
    std::map<uint32_t, DefendedPrice, MarketCmp> m_bidDefenders;

    ExactMult m_toIntMultiplier;

    boost::optional<DefendedPrice> m_recentHighInflection;
    boost::optional<DefendedPrice> m_recentLowInflection;

    boost::optional<DefendedPrice> m_currentBid;
    boost::optional<DefendedPrice> m_lastBid;
    boost::optional<DefendedPrice> m_nextLastBid;

    boost::optional<DefendedPrice> m_currentAsk;
    boost::optional<DefendedPrice> m_lastAsk;
    boost::optional<DefendedPrice> m_nextLastAsk;

    timeval_t m_lastResistanceAdded;
    timeval_t m_lastSupportAdded;
};
BB_DECLARE_SHARED_PTR( SigPriceDefense );


/// SignalSpec for SigPriceDefense
class SigPriceDefenseSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigPriceDefenseSpec()
        : m_minDefenseSpacing( boost::posix_time::hours(25) )
        , m_minAddSpacing( boost::posix_time::hours(25) )
        , m_maxRemovalDefense( 0 )
        , m_tickSize( 0.0 )
        , m_nonlinearity( 2.0 )
        , m_includeDefeats( false )
        , m_outputBrokenOnly( false )
        , m_fuzzyDefenseRange( 0.0 )
        {};

    SigPriceDefenseSpec(const SigPriceDefenseSpec &other);

    virtual instrument_t getInstrument() const { return m_inputBook->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigPriceDefenseSpec *clone() const;

    IBookSpecPtr  m_inputBook;
    source_t m_tickSource;
    
    bb::ptime_duration_t m_minDefenseSpacing;
    bb::ptime_duration_t m_minAddSpacing;
    uint32_t m_maxRemovalDefense;
    double m_tickSize;
    double m_nonlinearity;
    bool m_includeDefeats;
    bool m_outputBrokenOnly;
    double m_fuzzyDefenseRange;
};
BB_DECLARE_SHARED_PTR( SigPriceDefenseSpec );


} // namespace signals
} // namespace bb


#endif // BB_SIGNALS_SIGPRICEDEFENSE_H
