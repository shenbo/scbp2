#ifndef BB_SIGNALS_SIGSMON_H
#define BB_SIGNALS_SIGSMON_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/ISourceMonitorListener.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

/// Exports the current sourcemonitor status as a signal.
/// Hacky, but useful for data investigation.
class SigSMon : public SignalStateImpl, public SourceMonitorListener
{
public:
    SigSMon(const std::string &desc, ITimeProviderPtr timeProv, SourceMonitorPtr smon, bool verbose);
    virtual ~SigSMon();

protected:
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv);
    virtual void recomputeState() const {}

    SourceMonitorPtr m_sourceMon;
    ITimeProviderPtr m_timeProvider;
    bool m_verbose;
};
BB_DECLARE_SHARED_PTR(SigSMon);

/// SigSMonSpec
class SigSMonSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigSMonSpec() : m_verbose(false) {}
    SigSMonSpec(const SigSMonSpec &e);

    virtual instrument_t getInstrument() const { return instrument_t::unknown(); }
    virtual std::string getDescription() const { return m_description; }

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const {}
    virtual SigSMonSpec *clone() const;

    std::string m_description;
    source_t m_source;
    bool m_verbose;
};
BB_DECLARE_SHARED_PTR(SigSMonSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGSMON_H
