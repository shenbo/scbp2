#ifndef BB_SIGNALS_MOVINGAVERAGE_H
#define BB_SIGNALS_MOVINGAVERAGE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <cstddef>
#include <vector>

namespace bb {
namespace signals {

/// A MovingAverageGenerator implements an Exponential-weighted Moving Average
/// (http://en.wikipedia.org/wiki/Weighted_moving_average).
/// Multiple EMAs at varying decay rates can be tracked.
///
/// A decay weight of 0.9 means that each new impulse will count towards 10% of
/// the output signal, i.e.
/// s(t) = w*s(t-1) + (1-w)*v(t)
/// Where v is the input signal.
///
/// Boundary conditions: the first value will be trusted more than it should be,
/// until the moving average initializes.
class MovingAverageGenerator
{
public:
    /// Constructs a MovingAverageGenerator with the specified decay weights.
    /// Although initializing with empty decay weights is allowed, it is not useful.
    MovingAverageGenerator( const std::vector<double>& vDecayWeights );

    /// Returns the size of the state vector.
    size_t getStateSize() const                         { return m_vdecays.size(); }

    /// Returns the state vector.
    const std::vector<double>& getState() const         { return m_vstate; }

    /// Returns the decay weight vector that the MovingAverageGenerator was initialized with.
    const std::vector<double>& getDecayWeights() const  { return m_vdecays; }

    /// Resets this instance, including setting the state vector to 0.
    void reset();

    /// Updates the state vector according to the passed value.
    void update( double val );

    /// Returns true if this MovingAverageGenerator has been updated at least once.
    bool isReady() const                                { return !m_first_update; }

private:
    const std::vector<double>   m_vdecays;
    std::vector<double>         m_vstate;
    bool                        m_first_update;
};

/// Exponentially-weighted moving average, similar to MovingAverageGenerator,
/// with 3 differences:
/// 1) Only one decay rate at a time. If you need multiple, instantiate multiple
///    EMAGenerators.
/// 2) The decay rate is specified as halflife. A halflife of 5 means that an impulse
///    which occurs at time t1 will have twice the influence at t1 than it does at t1+5.
/// 3) Different startup procedure: hopefully more robust, but more costly
///    (1 divide per iteration).
class EMAGenerator
{
public:
    EMAGenerator( double halfLife );

    double getState() const         { return m_state; }

    void reset();
    void update( double val );

private:
    double m_alpha, m_rawState, m_startupWeight, m_state;
};

} // namespace signals
} // namespace bb


#endif // BB_SIGNALS_MOVINGAVERAGE_H
