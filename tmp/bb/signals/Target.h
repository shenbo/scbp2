#ifndef BB_SIGNALS_TARGET_H
#define BB_SIGNALS_TARGET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <bb/core/smart_ptr.h>

namespace bb {
namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignal );
BB_FWD_DECLARE_SHARED_PTR( Weights );
BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );

class TargetSpec;

class Target
{
public:
    Target( ISignalBuilderPtr builder, const TargetSpec& spec );

    const std::vector<ISignalPtr>& getSignals() const { return m_signals; }
    WeightsCPtr getWeights() const { return m_spWeights; }

protected:
    std::vector<ISignalPtr> m_signals;
    WeightsCPtr m_spWeights;
};

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_TARGET_H
