#ifndef BB_SIGNALS_SIGBOOKBIAS_H
#define BB_SIGNALS_SIGBOOKBIAS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/IBook.h>
#include <bb/clientcore/BookPriceProvider.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {


/// A Signal that given a reference PriceProvider and an input Book,
/// generates a signal where the signal state is:
///
///    v[i] = px(book_level_i) - refpx
///
/// px() depends on the EBookPriceView (default is BPV_BEST_VWMP).
/// i => [0, numLevels]
///
class SigBookBias
    : public SignalStateImpl
    , private IBookListener
    , private IClockListener
{
public:
    /// Constructor
    SigBookBias( const instrument_t& instr, const std::string &desc,
                 ClockMonitorPtr spCM, IPriceProviderPtr spRefPP, IBookPtr spBook,
                 const uint32_t numLevels, ReturnMode returnMode, EBookPriceView view = BPV_BEST_VWMP);

    /// Destructor
    virtual ~SigBookBias();

    /// Returns the timeval this signal last changed.
    virtual timeval_t getLastChangeTv() const
    {   return m_lastChangeTv; }

private:
    void onRefPriceChanged( const IPriceProvider& pp );

    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

    void _reset();
    virtual void recomputeState() const;

private:
    ClockMonitorPtr       m_spCM;
    IPriceProviderPtr     m_spRefPP;
    Subscription          m_spRefPPSub;
    IBookPtr              m_spBook;
    const uint32_t        m_numLevels;
    const EBookPriceView  m_view;
    double                m_refpx;
    bool                  m_refpx_ready;
    ReturnMode m_returnMode;
};
BB_DECLARE_SHARED_PTR( SigBookBias );


/// SignalSpec for SigBookBias
class SigBookBiasSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigBookBiasSpec()
        : m_view( BPV_BEST_VWMP )
        , m_returnMode(DIFF)
    {}
    SigBookBiasSpec(const SigBookBiasSpec &e);

    virtual instrument_t getInstrument() const { return m_book->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigBookBiasSpec *clone() const;

    IBookSpecCPtr   m_book;
    uint32_t        m_numLevels;
    EBookPriceView  m_view;
    ReturnMode m_returnMode;
};
BB_DECLARE_SHARED_PTR(SigBookBiasSpec);



} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGBOOKBIAS_H
