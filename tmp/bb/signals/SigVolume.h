#ifndef BB_SIGNALS_SIGVOLUME_H
#define BB_SIGNALS_SIGVOLUME_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/ClockMonitor.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

#include <bb/signals/SigSnap.h>
#include <bb/clientcore/BookPriceProvider.h>
#include <bb/clientcore/TickProvider.h>


namespace bb {
namespace signals {


class SigVolume
    : public SignalSmonImpl
    , private ITickListener
{
public:
    typedef std::vector<unsigned int> intervals;
    enum SigVolType { Vol, AbsVol };

    /// N is the number of weights, which must be equal to the number of intervals.
    SigVolume( const instrument_t& instr, const std::string &desc,
               ClockMonitorPtr cm,
               IPriceProviderPtr spRefPP, ITickProviderPtr spTickProvider,
               ptime_duration_t _interval,
               const intervals &interval_list, int vbose,
               SigVolType sigVolType);

    virtual ~SigVolume();

    virtual void reset();
    void setInterval(unsigned int i, unsigned int j); // J is in multiples of INTERVAL

protected:
    // IClockListener
    virtual void onWakeupCall(const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    // ITickListener
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume ) {};

    void check(timeval_t curtime);
    void recomputeState() const;

    Snapshot<uint32_t>          m_snapVol; // history of volume

    IPriceProviderPtr           m_spRefPP;
    Subscription                m_spRefPPSub;
    ITickProviderPtr            m_spTickProvider;
    unsigned int                m_num; // number of intervals
    ptime_duration_t            m_interval;
    timeval_t                   m_last_check;
    SigVolType                  m_sigVolType;

    static const int R_CHECK = cm::USER_REASON + 1;
};
BB_DECLARE_SHARED_PTR(SigVolume);


/// SignalSpec for SigVolume
class SigVolumeSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigVolumeSpec()
        : m_sigVolType(SigVolume::Vol)
    {
    }

    SigVolumeSpec(const SigVolumeSpec &e);

    virtual instrument_t getInstrument() const { return m_refPxP->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigVolumeSpec *clone() const;

    ptime_duration_t m_interval;
    std::vector<unsigned int> m_intervals;
    source_t m_tickSource;
    SigVolume::SigVolType m_sigVolType;
};
BB_DECLARE_SHARED_PTR(SigVolumeSpec);



} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGVOLUME_H
