#ifndef BB_SIGNALS_MESSAGESIGNAL_H
#define BB_SIGNALS_MESSAGESIGNAL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/integer_traits.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/Uuid.h>

#include <bb/io/SendTransport.h>

#include <bb/clientcore/ClockMonitor.h>

#include <bb/signals/Signal.h>

/// This contains SignalSerializer and MessageSignal, which are responsible for serializing/deserializing
/// a signal to messages, for remote signals (sigserver) and cached signals.

namespace bb {

class DependentVariableUpdate16Msg;
BB_FWD_DECLARE_SHARED_PTR(MsgHandler);

/// SourceMonitor specialized for signal/book messages.
class RemoteMessageMonitor : public SourceMonitor
{
public:
    typedef SourceMonitor Super;
    RemoteMessageMonitor(const CInfo &_cinfo, Uuid sigUuid);

    virtual void onEvent(const Msg& msg);
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
protected:
    NullEventDistListener m_heartbeatHandler;
    EventSubPtr m_heartbeatEventSub;

    Uuid m_uuid;
};
BB_DECLARE_SHARED_PTR(RemoteMessageMonitor);



namespace signals {

/// MessageSignal is a signal which takes its value from msgs.
/// These messages might be read off disk (for a cached signal),
/// or off the network (for the signal server).
class MessageSignal
    : public SignalListenerImpl
    , protected SourceMonitorListener
{
public:
    typedef uint32_t SignalID_t;
    static const SignalID_t INVALID_SIGNAL_ID = boost::integer_traits<SignalID_t>::const_max;

    MessageSignal(
        source_t src,
        bool passive,
        instrument_t instr,
        std::string description,
        EventDistributorPtr eventDist,
        ClockMonitorPtr cm,
        int verboseLevel,
        const Uuid &serverUuid,
        SignalID_t signalID,
        const std::vector<std::string> &stateNames,
        const Priority &priority);
    virtual ~MessageSignal();

    void setSourceMonitor(bb::RemoteMessageMonitorPtr srcMon);

    // ISignal implementation
    virtual const instrument_t &getInstrument() const { return m_instr; }
    virtual bool isOK() const;
    virtual const std::string &getDesc() const { return m_desc; }
    virtual size_t getStateSize() const;
    virtual const std::vector<std::string>& getStateNames() const { return m_vStateNames; }
    virtual const std::vector<double>& getSignalState() const;
    virtual timeval_t getLastChangeTv() const { return m_lastChangeTv; }
    virtual boost::optional<timeval_t> getLastScheduledChangeTv() const { return m_lastScheduledChangeTv; }

    template<typename SignalMsg_t> void onUpdateMessage(const SignalMsg_t &msg);
    void onDependentVarUpdate(const DependentVariableUpdate16Msg &msg);

    // SourceMonitorListener implementation
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv);

protected:
    void exposeUpdate(const timeval_t &ctv, const timeval_t &swtv);

    Uuid m_serverUuid;
    SignalID_t m_signalID;
    RemoteMessageMonitorPtr m_sourceMonitor;
    EventDistributorPtr m_eventDist;
    ClockMonitorPtr m_clockMonitor;
    Priority m_priority;
    int m_verboseLevel;
    MsgHandlerPtr m_update4Handler, m_update8Handler, m_update16Handler, m_ovargen16Handler;

    bool m_newIsOK;
    instrument_t m_instr;
    std::string m_desc;
    std::vector<std::string> m_vStateNames;
    std::vector<double> m_newState;
    bb::timeval_t m_newMsgTv;
    Subscription m_exposeCBSub;

    /// this reflects the isOK flag from the last message we recieved
    bool m_msgIsOK;
    std::vector<double> m_state;
    bb::timeval_t m_lastChangeTv;
    boost::optional<bb::timeval_t> m_lastScheduledChangeTv;
    smon_status_t m_smonStatus;
};
BB_DECLARE_SHARED_PTR(MessageSignal);


/// This saves the output of an ISignal to a message stream, so that
/// it can be read back by MessageSignal.
class SignalSerializer : public ISignalListener
{
public:
    SignalSerializer(ClientContextPtr clientContext, ISignalPtr signal,
        bb::ISendTransportPtr sendTransport, const Uuid &serverID, unsigned int signalID,
        source_t source, uint32_t *outgoingSeq, bool isDependentVariable);

    ISignalPtr getSignal() { return m_signal; }
    const Uuid &getUuid() const { return m_serverID; }
    unsigned int getSignalId() const { return m_signalID; }
    uint64_t getMessagesSent() const { return m_messagesSent; }
    uint64_t getBytesSent() const { return m_bytesSent; }

    // forces a write of the current signal value, even if one isn't pending
    void queueSignalUpdate();

    // ISignalListener implementation
    virtual void onSignalChanged(const ISignal& signal);

    void onDependentVariableChanged();
protected:
    template<typename UpdateMessage_t> void writeUpdateImpl();
    void writeUpdate();

    EventDistributorPtr m_eventDist;
    ClockMonitorPtr m_clockMonitor;
    ISendTransportPtr m_sendTransport;
    symbol_t m_symbol;
    ISignalPtr m_signal;
    Uuid m_serverID;
    unsigned int m_signalID;
    source_t m_source;
    uint32_t *m_outgoingSeq;
    bool m_writePending;
    uint64_t m_messagesSent, m_bytesSent;
    bool m_lastIsOK;
    bool m_isDependentVar;
    bool m_hasScheduledWakeupTv;
    std::vector<double> m_lastState;
};
BB_DECLARE_SHARED_PTR(SignalSerializer);



} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_MESSAGESIGNAL_H
