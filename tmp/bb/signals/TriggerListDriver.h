#ifndef BB_SIGNALS_TRIGGERLISTDRIVER_H
#define BB_SIGNALS_TRIGGERLISTDRIVER_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <set>
#include <vector>
#include <boost/optional.hpp>
#include <bb/core/InstrSource.h>
#include <bb/core/ListenerList.h>
#include <bb/core/Priority.h>
#include <bb/core/ptime.h>
#include <bb/core/smart_ptr.h>

namespace bb {

class timeval_t;
class Msg;
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( IBookBuilder );
BB_FWD_DECLARE_SHARED_PTR( ITickProviderFactory );
BB_FWD_DECLARE_SHARED_PTR( IClientTimer );

namespace signals {
BB_FWD_DECLARE_SHARED_PTR( IControlSignal );

/** Drives the execution of a process by waiting on a list of triggers
 * fire_priority - must be higher than Book priority, i.e. only makes sense to
 *                 fire after the triggers are detected.  Otherwise will lag
 *                 by 1 message
 */
class TriggerListDriver
{
public:
    class Listener
    {
    public:
        virtual ~Listener() {}

        virtual void onFire( const timeval_t& ctv, const timeval_t& swtv ) = 0;

        // this method is for debugging purposes
        virtual void onInterestingMsg( const Msg& m ) = 0;
    };

public:
    TriggerListDriver( const std::vector<InstrSource>& trigger_list
        , const ptime_duration_t& trigger_timeout
        , bool trigger_smart_fire
        , bool trigger_on_msg
        , const Priority& fire_priority
        , const ClientContextPtr& cc
        , const IBookBuilderPtr& book_builder
        , const ITickProviderFactoryPtr& tf
        , int32_t verbose = 0 );

    void setControlSignal( const IControlSignalPtr& ctrl );
    const IControlSignalCPtr getControlSignal() const;

    void subscribe( bb::Subscription& s, Listener* v )
    { m_listeners.subscribe( s, v ); }

    void print() const;

private:
    class Register;
    BB_DECLARE_SHARED_PTR( Register );
    class Trigger;
    BB_DECLARE_SHARED_PTR( Trigger );
    friend class Trigger;

private:
    void onAllEvents( const Msg& msg );
    void onAllSymbols( const Msg& msg );
    void onTriggerFire( const timeval_t& tv, uint32_t index );
    void onTriggerTimeout( const timeval_t& ctv, const timeval_t& swtv );
    void fire( const timeval_t& ctv, const timeval_t& swtv, const Msg* msg );

private:
    int32_t m_verbose;
    std::vector<InstrSource> m_triggerList;
    ptime_duration_t m_triggerTimeout;
    bool m_triggerSmartFire, m_triggerOnMsg;
    Priority m_firePriority;
    ClockMonitorPtr m_spClockMonitor;
    IClientTimerPtr m_spClientTimer;
    bb::Subscription m_subEvents;
    bb::Subscription m_subTimeout;
    bb::Subscription m_subAllSymbols;
    RegisterPtr m_spRegister;
    std::vector<TriggerPtr> m_triggers;
    ListenerList<Listener*> m_listeners;
    IControlSignalPtr m_spControlSignal;

    timeval_t m_currentExchangeTime;
    bool m_beyondLastTriggerInstr;
    boost::optional<bb::instrument_t> m_optLastTriggerInMarketData;
    std::set<bb::instrument_t> m_instrSetEndofBatch;
    uint32_t m_lastMsgSeq;
};

BB_DECLARE_SHARED_PTR( TriggerListDriver );

} // signals
} // bb

#endif // BB_SIGNALS_TRIGGERLISTDRIVER_H
