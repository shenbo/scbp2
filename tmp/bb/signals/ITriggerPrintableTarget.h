#ifndef BB_SIGNALS_ITRIGGERPRINTABLETARGET_H
#define BB_SIGNALS_ITRIGGERPRINTABLETARGET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/signals/Signal.h>

namespace bb {
namespace signals {

/// 2014-01-23 TriggerListSignalPrinter currently only has two requirements of target
/// variables.

/// A refprice should be available.
/// 1) getRefPrice()
/// 2) firstVarAsRefpx()

/// One needs to indicate how far in the future the target is shifted, relative to the input signals
/// which will be used to predict it.
/// 3) getPrintDelay()

// Don't really need the listeners part of ISignal in this case, but since ISignal is currently
// the root of the signal lib inheritance and some implementation is currently required we will use SignalListenerImpl
// which is the simplest.
class ITriggerPrintableTarget
    : virtual public SignalListenerImpl
{
public:

    virtual ~ITriggerPrintableTarget() {}

    virtual double getPrintRefPrice() const = 0;
    // Clunky, but needed for backwards compatibility.
    // Traditionally target vars kept the refpx at index 0 of the "signal-state"
    virtual bool firstVarAsPrintRefPrice() const = 0;

    virtual ptime_duration_t getPrintDelay() const = 0;
};

BB_DECLARE_SHARED_PTR( ITriggerPrintableTarget );

class TriggerPrintableTargetList : public std::vector<ITriggerPrintableTargetPtr>{};
BB_DECLARE_SHARED_PTR( TriggerPrintableTargetList );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_ITRIGGERPRINTABLETARGET_H

