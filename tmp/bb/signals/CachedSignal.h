#ifndef BB_SIGNALS_CACHEDSIGNAL_H
#define BB_SIGNALS_CACHEDSIGNAL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/integer_traits.hpp>

#include <bb/core/hash_map.h>
#include <bb/core/LuaState.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Uuid.h>

#include <bb/io/Socket.h>
#include <bb/io/SendTransport.h>

#include <bb/clientcore/ClockMonitor.h>

#include <bb/signals/MessageSignal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

class CacheFile
{
public:
    CacheFile(std::string descFile);

    const Uuid &getServerUuid() const { return m_serverUuid; }

    /// returns the signal ID and state names for the given signal spec.
    /// throws bb::Error if the signal isn't cached in this file.
    void lookupSignal(MessageSignal::SignalID_t *out_sigID,
            std::vector<std::string> *out_stateNames,
            timeval_t startTime,
            timeval_t endTime,
            ISignalSpecCPtr spec);

protected:
    // config methods (called from the lua .desc file)
    void setDate(timeval_t startTime, timeval_t endTime);
    void setServerId(Uuid id);
    void addSignal(MessageSignal::SignalID_t id, ISignalSpecPtr sig, luabind::object stateNamesObj);

    std::string m_filename;
    LuaState m_config;

    Uuid m_serverUuid;
    timeval_t m_startTime, m_endTime;
    typedef std::pair<MessageSignal::SignalID_t, std::vector<std::string> > Entry_t;
    typedef bbext::hash_map<ISignalSpecCPtr, Entry_t, SignalSpecHasher, SignalSpecComparator> CacheMap_t;
    CacheMap_t m_entries;
};
BB_DECLARE_SHARED_PTR(CacheFile);


/// CachedSignalSpec (a MessageSignal which loads the signal messages from a file)
class CachedSignalSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    CachedSignalSpec() {}
    CachedSignalSpec(const CachedSignalSpec &e);

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual CachedSignalSpec *clone() const;

    virtual instrument_t getInstrument() const { return m_subSignal->getInstrument(); }
    virtual std::string getDescription() const { return m_subSignal->getDescription(); }

    ISignalSpecCPtr m_subSignal;
    source_t m_signalSource;
    std::string m_filename;
};
BB_DECLARE_SHARED_PTR(CachedSignalSpec);



} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_CACHEDSIGNAL_H
