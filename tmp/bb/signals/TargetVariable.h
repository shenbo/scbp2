#ifndef BB_SIGNALS_TARGETVARIABLE_H
#define BB_SIGNALS_TARGETVARIABLE_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <string>
#include <vector>

#include <boost/bind.hpp>
#include <boost/optional.hpp>

#include <bb/core/Subscription.h>
#include <bb/core/instrument.h>
#include <bb/core/timeval.h>
#include <bb/statistics/DataSampler.h>
#include <bb/signals/Signal.h>

namespace bb {

namespace statistics {

BB_FWD_DECLARE_SHARED_PTR( IDataSampler );

} // namespace statistics

namespace signals {

class TargetVariable
    : virtual public SignalListenerImpl
    , public statistics::IDataSamplerListener
{
};
BB_DECLARE_SHARED_PTR( TargetVariable );

class SamplerTargetVariable
    : public TargetVariable
{
public:
    SamplerTargetVariable( const ISignalPtr& signal, const statistics::IDataSamplerPtr& dataSampler );

    virtual void onDataSample( const statistics::IDataSampler* dataSampler );

    virtual bool isOK() const;
    virtual const bb::instrument_t& getInstrument() const { return m_instr; }
    virtual const std::string& getDesc() const { return m_desc; }
    virtual const std::vector<double>& getSignalState() const;
    virtual size_t getStateSize() const { return m_stateNames.size(); }
    virtual const std::vector<std::string>& getStateNames() const { return m_stateNames; }
    virtual timeval_t getLastChangeTv() const { return m_lastChangeTv; }
    virtual boost::optional<bb::timeval_t> getLastScheduledChangeTv() const { return boost::none; }

protected:
    /// This is called when the signal is dirty, and someone wants to know isOK or getSignalState.
    /// Should update m_state and m_isOK to update the state to reflect reality.
    void recomputeState() const;

    /// Resets the state to !ok, state == all zeros, and notifies listeners
    void reset( const bb::timeval_t& changeTv );

    /// Updates the last change time and sets the dirty bit.
    void notifySignalListeners( const bb::timeval_t& changeTv );

    bb::instrument_t                m_instr;
    std::string                     m_desc;
    std::vector<std::string>        m_stateNames;
    bb::timeval_t                   m_lastChangeTv;
    mutable bool                    m_dirty;
    mutable bool                    m_isOK;
    mutable std::vector<double>     m_state;

    const ISignalPtr m_signal;
    statistics::IDataSamplerPtr m_dataSampler;
    Subscription m_sub;
    std::vector<double> m_lastValues;
};
BB_DECLARE_SHARED_PTR( SamplerTargetVariable );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_TARGETVARIABLE_H
