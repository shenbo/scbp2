#ifndef BB_SIGNALS_SIGPREDICTIONEVALUATOR_H
#define BB_SIGNALS_SIGPREDICTIONEVALUATOR_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/scoped_array.hpp>
#include <bb/core/Stats.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>
#include <bb/signals/IMetaPredictionSignal.h>
#include <bb/signals/PredictionReport.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalManager.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {


// Wrap PredictionHistory, calculate the rsq for a "recent" window and output that rsq as the signal.
class SigPredictionEvaluator : public SignalStateImpl, public IMetaPredictionSignal, public IPredictionListener
{
public:
    SigPredictionEvaluator(const instrument_t& instr, const std::string &desc,
        const uint32_t prediction_samples);

    virtual ~SigPredictionEvaluator() {};

    // IMetaPrediction
    virtual void setPredictionHistory( PredictionHistoryPtr pred_history );
    // IPredictionListener impl
    virtual void onStartPrediction(const timeval_t &tv, int32_t ymdDate);
    virtual void onEndPrediction(const timeval_t &tv, int32_t ymdDate);
    virtual void onPricePrediction(const timeval_t &tv, double predictedPx, double refpx,
            const double_range &ovars);

protected:
    // void onPriceChanged( const IPriceProvider& pp );
    virtual void recomputeState() const;

    PredictionHistoryPtr m_predHistory;
    Subscription m_listenerSub;

    uint32_t m_predictionSamples;

    typedef std::pair<double, double> PredHistoryEntry_t;
    std::deque<PredHistoryEntry_t> m_predictionHistoryWindow;

    RSquaredCalculator m_rSquared;
};
BB_DECLARE_SHARED_PTR(SigPredictionEvaluator);


/// SigPredictionEvaluatorSpec
class SigPredictionEvaluatorSpec : public NoRefPxSignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    // Using default target var, this default should be ~5 mins
    SigPredictionEvaluatorSpec() 
        : m_predictionSamples( 150 )
    {}

    SigPredictionEvaluatorSpec(const SigPredictionEvaluatorSpec &e);

    virtual instrument_t getInstrument() const { return m_instrument; }

    virtual ISignalPtr build(SignalBuilder *builder) const;
//    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
//    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigPredictionEvaluatorSpec *clone() const;

    instrument_t m_instrument;
    uint32_t m_predictionSamples;

};
BB_DECLARE_SHARED_PTR(SigPredictionEvaluatorSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGPREDICTIONEVALUATOR_H
