#ifndef BB_SERVERS_TDCORE_TDSERVER_H
#define BB_SERVERS_TDCORE_TDSERVER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/side.h>
#include <bb/core/symbol.h>

#include <bb/servers/tdcore/TdConfig.h>

namespace bb {
namespace tdcore {

class TdOrderManager;

class TdServer
{
protected:
    virtual ~TdServer() {}

public:
    virtual void login() = 0;
    virtual void logout() = 0;
    virtual void halt() = 0;
    virtual void halt( const instrument_t& ) = 0;
    virtual void resume() = 0;
    virtual void resume( const instrument_t& ) = 0;
    virtual bool isHalted() const = 0;

    virtual void sendTestRequest( const std::string& text ) = 0;

    virtual void setNextOutgoingSeqNum( long seq_num );
    virtual void setNextIncomingSeqNum( long seq_num );
    virtual void sendSequenceResetReset( long seq_num );

    virtual void setNextOrderSeqNum( unsigned int ) = 0;

    virtual void cancelOrder( unsigned int tid ) = 0;
    virtual void cancelOrders( side_t, symbol_t ) = 0;
    virtual void cancelOrdersForClient( uint32_t ) = 0;

    virtual void forceDoneOrder( unsigned int tid ) = 0;
    virtual void forceDoneOrders( side_t, symbol_t ) = 0;
    virtual void forceDoneOrdersForClient( uint32_t ) = 0;
    virtual void forceDoneOrdersForClientIdent( const std::string& ) = 0;

    virtual void printStatus() const = 0;
    virtual void savePositions( const std::string& filename ) const = 0;

    virtual void printRiskLimits() const = 0;
    virtual void reloadRiskLimits() = 0;
    virtual void reloadRiskLimits( const std::string& filename ) = 0;
    virtual void recheckRiskLimits() const = 0;
    virtual void reapplyRiskLimits() const = 0;

    virtual void printAllowedUsers() const = 0;
    virtual void reloadAllowedUsers() = 0;

    // Commands will be used in MultiSession Server only
    virtual void setNextOutgoingSeqNumBySession( const std::string& sessionId, long seqNum );
    virtual void setNextIncomingSeqNumBySession( const std::string& sessionId, long seqNum );
    virtual void sendSequenceResetResetBySession( const std::string& sessionId, long seqNum );

    virtual void loginSession( const std::string& sessionId );
    virtual void logoutSession( const std::string& sessionId );

    virtual void setAccountLimits(int ordersLimits);
    virtual void setAccountInstrumentLimits(const instrument_t&, int maxOrderQty, double maxPosition, double maxExposure);
};

/// Common implementation of TdServer
class TdServerImpl : public TdServer
{
public:
    virtual void printAllowedUsers() const;
    virtual void reloadAllowedUsers();

    virtual void forceDoneOrder( unsigned int tid );
    virtual void forceDoneOrders( side_t, symbol_t );
    virtual void forceDoneOrdersForClient( uint32_t );
    virtual void forceDoneOrdersForClientIdent( const std::string& );

protected:
    TdServerImpl( const std::string& configFile, TdConfig& config );

    virtual TdConfig loadConfig( const std::string& configFile );

    virtual TdOrderManager& getOrderManager() = 0;
    virtual const TdOrderManager& getOrderManager() const = 0;

    virtual void cancelOrders( side_t, symbol_t );
    virtual void cancelOrders( side_t, const instrument_t & );

    const TdConfig& getConfig() const { return m_config; }
    TdConfig& getConfig() { return m_config; }

private:
    virtual void setNextOrderSeqNum( unsigned int order_seq_num );

    virtual void cancelOrdersForClient( uint32_t clientId );

    virtual void printStatus() const;

    const std::string& m_configFile;
    TdConfig& m_config;
};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDSERVER_H
