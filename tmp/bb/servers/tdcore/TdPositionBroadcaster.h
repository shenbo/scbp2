#ifndef BB_SERVERS_TDCORE_TDPOSITIONBROADCASTER_H
#define BB_SERVERS_TDCORE_TDPOSITIONBROADCASTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>
#include <boost/random.hpp>

#include <bb/core/acct.h>
#include <bb/core/instrument.h>
#include <bb/core/symbol.h>

#include <bb/servers/tdcore/TdPositionBroadcasterCore.h>

namespace bb {

namespace tdcore {

/// Broadcasts position updates periodically, using TdRiskControl to provide the position data
class TdPositionBroadcaster
    : public TdPositionBroadcasterCore
{
public:

    template<class RiskControlType>
    TdPositionBroadcaster( RiskControlType& riskControl, acct_t account )
    {
        setPlanckSleep( false );
        addAccount( &riskControl, account );
        activateBroadcast();
    }

private:

    //unsigned BROADCAST_CYCLE;
    static const unsigned BROADCAST_CHANGES_INTERVAL = 2;
    static const unsigned BROADCAST_MAX_DELAY = 5;

    virtual void broadcastPositions() const; // m_broadcastThread entry function

};
BB_DECLARE_SHARED_PTR( TdPositionBroadcaster );

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDPOSITIONBROADCASTER_H
