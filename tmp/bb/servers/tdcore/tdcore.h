#ifndef BB_SERVERS_TDCORE_TDCORE_H
#define BB_SERVERS_TDCORE_TDCORE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <boost/program_options/options_description.hpp>

#include <bb/core/acct.h>

namespace bb {
namespace tdcore {

namespace detail {
extern int verbose;
extern std::string appname;
extern bool disable_authentication;
extern bool disable_database;
extern bool enable_spinning;
extern int pin_core;
namespace po = boost::program_options;
}

/// assumes single account
std::pair<acct_t, std::string> init( const std::string& program_name, int argc, char* argv[]
    , const detail::po::options_description& others = detail::po::options_description() );

/// assumes accounts configured in the config file
std::string init_multiacct( const std::string& program_name, int argc, char* argv[]
    , const detail::po::options_description& others = detail::po::options_description() );

/// Set/get the global verbosity of the td logs
inline void setVerbose( int verbose ) { detail::verbose = verbose; }
inline int getVerbose() { return detail::verbose; }

/// Get the name of the application that is used for producing logs
inline const std::string& getAppName() { return detail::appname; }

/// Authentication may be disabled for testing (and only for testing)
/// SASL authentication on the server side requires a private Kerberos keytab
inline bool requireAuthentication() { return !detail::disable_authentication; }

/// if the database is unavailable then this flag can be used to tell BB to bypass it
inline bool requireDatabase() { return !detail::disable_database; }

} // tdcore
} // bb

#endif // BB_SERVERS_TDCORE_TDCORE_H
