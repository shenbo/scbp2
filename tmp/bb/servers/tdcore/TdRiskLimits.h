#ifndef BB_SERVERS_TDCORE_TDRISKLIMITS_H
#define BB_SERVERS_TDCORE_TDRISKLIMITS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bitset>
#include <iosfwd>


#include <bb/core/Scripting.h>
#include <bb/core/symbol.h>
#include <bb/core/instrument.h>
#include <bb/core/tif.h>
#include <bb/core/hash_map.h>
#include <bb/core/hash_set.h>

namespace bb {
namespace tdcore {

class TdRiskLimits;

struct InstrumentLimits
{
    InstrumentLimits();
    InstrumentLimits( const TdRiskLimits& riskLimits );

    int32_t order_size; // maximum order size(fat finger limit)
    double order_value; // maximum order value(fat finger limit)
    int32_t potential_side_size; // maximum number of shares (potential) in a given direction
    int32_t side_size;
    double  loss;            // maximum realized loss in dollars
    double  exposure;        // maximum dollar value of actual position plus open orders in either direction
    double  position;        // maximum absolute dollar value of actual position
};

class TdRiskLimits
{
public:
    BB_DECLARE_SCRIPTING();

    TdRiskLimits( double _default_price ); // derived classes specify a sensible default price (e.g. 100 for stocks in USD)
    static TdRiskLimits unlimited( double _default_price );

    typedef bbext::hash_map<instrument_t, InstrumentLimits, instrument_t::hash_no_mkt_no_currency, instrument_t::equals_no_mkt_no_currency> InstrumentLimitTable;

    typedef bbext::hash_set<instrument_t, instrument_t::hash_no_mkt_no_currency, instrument_t::equals_no_mkt_no_currency> InstrumentList;


    bool enable_reject;
    bool enable_cancel;

    double  symbol_exposure;        // maximum dollar value of actual position plus open orders in either direction
    double  symbol_position;        // maximum absolute dollar value of actual position
    int32_t symbol_shares;          // maximum number of shares (potential) in either direction
    double  symbol_loss;            // maximum realized loss in dollars
    int32_t symbol_order_size;      // maximum order size(fat finger limit)
    double symbol_order_value;      // maximum order value(fat finger limit)

    double  account_exposure;       // maximum dollar value of actual position plus open orders in either direction
    double  account_position_gross; // maximum dollar value of long positions + short positions
    double  account_position_net;   // maximum dollar value of long positions - short positions
    int32_t account_open_orders;    // maximum number of open orders
    uint32_t account_total_orders;  // maximum number of orders that can be sent regardless of if they are valid or not
    uint32_t account_total_rejects; // maximum number of rejects you can have before all orders get blocked
    double  account_loss;           // maximum realized loss in dollars
    uint32_t account_avg_orders_per_sec; // average open order number per second
    uint32_t account_orders_in_buffer_window; // open order count that will trigger TD to halt for this account in case it's opening too many orders in a short period of time.
    double  clearly_erroneous_percent; // maximum percentage away from price

    double option_price_limit_parameter;
    double option_price_limit_range;

    InstrumentLimitTable per_instrument_limits; // map of per instrument limits

    double account_gamma_position;  // maximum gamma dollar value allowed (only applicable to options)
    double account_gamma_exposure;  // maximum gamma dollar value allowed, aka actual position + plus open orders in either direction (only applicable to options)
    double account_vega_position;   // maximum vega dollar value allowed (only applicable to options)
    double account_vega_exposure;   // maximum vega dollar value allowed, aka actual position + plus open orders in either direction (only applicable to options)

    double  default_price;    // share price (pessimistic) to use when otherwise unknown
    bool    validate_limits;  // if set, the risk limits are validated after being loaded from lua

    std::bitset<TIF_MAX> allowed_tifs; // tif_ts not set will always be rejected
    std::bitset<SYM_MAX> allowed_syms; // symbols not set will always be rejected
    std::bitset<MKT_MAX> relevant_markets; // if not empty, we will ignore all markets that aren't in here

    InstrumentList allowed_derivatives;

    const InstrumentLimitTable& getAllInstrumentLimits() const { return per_instrument_limits; }
    bool getInstrumentLimit( const instrument_t& instr, InstrumentLimits& result ) const;
    bool getInstrumentLimitByIgnoringExpiry( const instrument_t& instr, InstrumentLimits& result ) const;
    void setInstrumentLimit(const instrument_t& instr, InstrumentLimits& newLimit );
    void validateLimits();
};

std::ostream& operator <<( std::ostream& out, const TdRiskLimits& limits );
std::ostream& operator <<( std::ostream& out, const InstrumentLimits& limits );

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDRISKLIMITS_H
