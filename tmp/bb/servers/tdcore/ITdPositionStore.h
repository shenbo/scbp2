#ifndef BB_SERVERS_TDCORE_ITDPOSITIONSTORE_H
#define BB_SERVERS_TDCORE_ITDPOSITIONSTORE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <stdint.h>

#include <boost/function.hpp>

#include <bb/core/smart_ptr.h>

namespace bb {
class acct_t;
class instrument_t;

namespace tdcore {
class SymbolData;

class ITdPositionStore
{
public:
    virtual ~ITdPositionStore() {}

    virtual const SymbolData& getSymbolData( const instrument_t& ) const= 0;
    virtual SymbolData& setSymbolPriceIfUnset( const instrument_t& ) = 0; // use m_limits.default_price (supports stocks and options)
    virtual SymbolData& setSymbolPriceIfUnset( const instrument_t&, double price ) = 0;
    virtual void setSymbolPrice( const instrument_t&, double price ) = 0; // set price and adjust account values
    virtual void setSymbolSize( const instrument_t&, int32_t dir_size ) = 0; // set position size and adjust account values
    virtual void setSymbolBaseline( const instrument_t&, int32_t baseline ) = 0;

    virtual void clear( bool keep_baselines = false ) = 0;

    // these can be called from a separate thread, e.g. for position broadcasting
    virtual void forEach( boost::function<void ( const instrument_t&, const SymbolData& )> callback ) const = 0;
    virtual void forInstrument( const instrument_t&, boost::function<void ( const SymbolData& )> callback ) const = 0;
};
BB_DECLARE_SHARED_PTR( ITdPositionStore );

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_ITDPOSITIONSTORE_H
