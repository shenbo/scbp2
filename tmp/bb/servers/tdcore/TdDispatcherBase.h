#ifndef BB_SERVERS_TDCORE_TDDISPATCHERBASE_H
#define BB_SERVERS_TDCORE_TDDISPATCHERBASE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/acct.h>
#include <bb/core/Msg.h>
#include <bb/core/oreason.h>
#include <bb/core/source.h>
#include <bb/core/FDSetFwd.h>
#include <bb/core/Subscription.h>

#include <bb/io/ByteSinkFactory.h>

#include <bb/servers/tdcore/TdConfig.h>

namespace bb {
class UserMessageMsg;
BB_FWD_DECLARE_SHARED_PTR( MQServer );
BB_FWD_DECLARE_SHARED_PTR( IDNSResolver );
BB_FWD_DECLARE_SHARED_PTR( ISendTransport );
BB_FWD_DECLARE_SHARED_PTR( SCTPSocket );
BB_FWD_DECLARE_SHARED_PTR( TCPSocket );
BB_FWD_DECLARE_SHARED_PTR( UnixSocket );
BB_FWD_DECLARE_SHARED_PTR( InProcessTransport );
BB_FWD_DECLARE_SHARED_PTR( InProcessTransportServer );
BB_FWD_DECLARE_SHARED_PTR( ByteSinkFactory );

namespace tdcore {
class TdPositionBroadcaster;
class TdPositionSender;
class TdRiskControl;
BB_FWD_DECLARE_SHARED_PTR( TdClient );
BB_FWD_DECLARE_SHARED_PTR( TdClientFactory );

/// for programs which do not enter orders, such as fill daemons
class TdDispatcherBase
{
    typedef boost::function<void( const TdClientPtr& )> ClientConnectionEndCB;

public:
    void setClientConnectionEndCB( const ClientConnectionEndCB& );
    void setAllowedUsers( const TdConfig::AllowedUsers& allowedUsers );

    void run();

protected:
    // The first constructor is for the normal mode of operation for
    // TdDispatcherBase. Use the second constructor to inhibit the normal
    // construction of an internal SelectDispatcher, for cases where
    // the TdDispatcherBase will be driven by an external event loop.
    TdDispatcherBase( const acct_t& account, EFeedType sourceType );
    TdDispatcherBase( const acct_t& account, EFeedType sourceType, FDSet& fdSet);

    virtual ~TdDispatcherBase() {}

    void stop();

    // support for derived class
    source_t              getSource() const     { return m_source; }
    ISendTransportPtr&    getRebroadcaster()    { return m_rebroadcaster; }
    FDSet&                getDispatcher()       { return m_dispatcher; }

    // support from derived class
    virtual TdRiskControl&         getRiskControl()         = 0;
    virtual TdPositionBroadcaster& getPositionBroadcaster() = 0;
    virtual TdPositionSender&      getPositionSender()      = 0;
    virtual void onRunningOrPreviousCrashDetected() {};

    void initializeFactories( const TdConfig& config );
    ByteSinkFactoryPtr getLogSinkFactory();

    const acct_t m_account;



private:
    void initialize();
    void handleConnectionTCP();
    void handleConnectionMQ();
    void handleConnectionUnix();
    void handleConnectionInProc( const InProcessTransportPtr& );
    void handleConnectionSCTP();

    void handleClientMsg( const TdClientPtr& bbc, const Msg& new_msg );
    void handleUserMessage( const UserMessageMsg& user, const TdClientPtr& bbc );

    void assertUserIsAdmin( const TdClientPtr&, const Msg& ) const;

    // derived classes may implement this to return true if they handled the given Msg
    virtual bool handleClientMsgHook( const TdClientPtr&, const Msg& ) { return false; }

    bool m_stop;

    const source_t m_source;

    ClientConnectionEndCB m_clientConnectionEndCB;

    ISendTransportPtr m_rebroadcaster;

    // client connection acceptors
    TCPSocketPtr m_serverTCP;
    MQServerPtr  m_serverMQ;
    UnixSocketPtr m_serverUnix;
    InProcessTransportServerPtr m_serverInProc;
    SCTPSocketPtr m_serverSCTP;

    Subscription m_callbackTCP;
    Subscription m_callbackMQ;
    Subscription m_callbackUnix;
    Subscription m_callbackSCTP;

    // m_optinalInternalDispatcher is set if TdDispatcher owns its own SelectDispatcher.
    FDSetPtr m_optionalInternalDispatcher;
    FDSet&   m_dispatcher;

    TdClientFactoryPtr            m_clientFactory;
    threading::ThreadPoolPtr      m_loggingThreadPool;
    ByteSinkFactoryPtr            m_logSinkFactory;

    IDNSResolverPtr               m_resolver;
    const TdConfig::AllowedUsers* m_allowedUsers; // if NULL, all users are allowed
};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDDISPATCHERBASE_H
