#ifndef BB_SERVERS_TDCORE_TDORDERACTIONPROCESSOR_H
#define BB_SERVERS_TDCORE_TDORDERACTIONPROCESSOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/bind.hpp>
#include <boost/make_shared.hpp>

#include <bb/core/queue/ThreadPipeNotifier.h>
#include <bb/core/queue/waitable_queue.h>

#include <bb/servers/tdcore/ITdOrderRouter.h>
#include <bb/servers/tdcore/TdOrderManager.h>

namespace bb {
namespace tdcore {

/// This holds the cross thread message queuing and processing functionality of OrderActions
class TdOrderActionProcessor
{
    typedef boost::function0<void> TdAction;
    typedef boost::shared_ptr<TdAction> TdActionPtr;
    typedef queue::waitable_concurrent_queue<TdActionPtr, queue::ThreadPipeNotifier> TdActionQueue;

public:
    TdOrderActionProcessor( FDSet& );
    void initOrderRouter( ITdOrderRouter* );

    template<typename Action>
    void postAction( const Action* );

    // raw interface (used by sfittd & knightfd) to post any old function, not limited to TdOrderAction types
    void post( const boost::function0<void>& );

    // for testing (used by test_SfitTdTraderInterface)
    size_t size() const;
    void clear();

private:
    void handleAction();

    template<typename Action>
    class OrderActionHolder
    {
    public:
        OrderActionHolder( const Action* );
        void operator() ( TdOrderManager* );

    private:
        typedef void (TdOrderManager::*handler_type)( const Action& );
        handler_type m_handler;
        boost::shared_ptr<const Action> m_action;
    };

private:
    ITdOrderRouter* m_orderRouter; // not a shared pointer because that could cause a circular reference
    TdActionQueue m_queue;
    Subscription m_queueSub;
};

template<typename Action>
void TdOrderActionProcessor::postAction( const Action* action )
{
    BB_ASSERT( m_orderRouter );
    BB_ASSERT( m_orderRouter->getOrderManager() );

    m_queue.push( boost::make_shared<boost::function0<void> >(
            boost::bind<void>( OrderActionHolder<Action>( action ), m_orderRouter->getOrderManager() ) ) );
}

inline void TdOrderActionProcessor::post( const boost::function0<void>& func )
{
    m_queue.push( boost::make_shared<boost::function0<void> >( func ) );
}

inline void TdOrderActionProcessor::handleAction()
{
    // there used to be a while loop here to read all the available messages from the queue,
    // but this caused a burst of messages in the queue (e.g. from FIX) to prevent timely processing of client messages.
    // if we used nonblocking sockets for the client connection and read from them until empty, maybe we'd loop here again.

    TdActionQueue::value_type result;
    m_queue.pop( &result ); // should never be here when the queue is empty, so don't check return
    (*result)();
}

template<typename Action>
TdOrderActionProcessor::OrderActionHolder<Action>::OrderActionHolder( const Action* action )
    : m_handler( &TdOrderManager::handle ), m_action( action )
{
}

template<typename Action>
void TdOrderActionProcessor::OrderActionHolder<Action>::operator()( TdOrderManager* orderManager )
{
    try
    {
        (orderManager->*m_handler)( *m_action );
    }
    catch( const TdOrderManager::ErrOrderNotFound& ex )
    {
        LOG_WARN << "order not found: " << ex.what() << bb::endl;
    }
}

} // namespace bb
} // namespace tdcore

#endif // BB_SERVERS_TDCORE_TDORDERACTIONPROCESSOR_H
