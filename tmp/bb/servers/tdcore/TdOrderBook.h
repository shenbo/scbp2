#ifndef BB_SERVERS_TDCORE_TDORDERBOOK_H
#define BB_SERVERS_TDCORE_TDORDERBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <cxxabi.h>

#include <iosfwd>
#include <utility>

#include <boost/none.hpp>
#include <boost/mpl/map.hpp>
#include <boost/pool/object_pool.hpp>
#include <boost/utility/enable_if.hpp>
#define BOOST_MULTI_INDEX_DISABLE_SERIALIZATION // eliminate unnecessary dependency
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/ordered_index.hpp>
#undef BOOST_MULTI_INDEX_DISABLE_SERIALIZATION

#include <bb/core/tid.h>
#include <bb/core/PersistentNumber.h>
#include <bb/servers/tdcore/TdOrder.h>
#include <bb/servers/tdcore/TdClient.h>

namespace bb {
namespace tdcore {
BB_FWD_DECLARE_SHARED_PTR( TdClient );

class orderpair : public std::pair<uint32_t, uint32_t>
{
public:
    inline orderpair( first_type _clientid, second_type _orderid )
        : std::pair<uint32_t, uint32_t>( _clientid, _orderid ) {}
};

std::ostream &operator <<( std::ostream &out, const orderpair &p );

namespace TdOrderBook_detail { // private namespace which we are free to pollute with "using namespace"

using namespace boost::multi_index;

typedef uint32_t seqnum_t;

// this container provides several ways of finding an order
// its declaration is hideous, yes, but its functionality is many-splendored
typedef multi_index_container<TdOrder*, indexed_by<
    // index by clientid + orderid
    ordered_unique<tag<orderpair>, composite_key<TdOrder,
            member<TdOrder, orderpair::first_type, &TdOrder::clientid>,
            member<TdOrder, orderpair::second_type, &TdOrder::orderid> > >,
    // index by tradeid
    hashed_unique<tag<bb::tid_t>, member<TdOrder, bb::tid_t, &TdOrder::tradeid> >
    > > orders_t;

#if __GNUC__ == 4 && __GNUC_MINOR__ > 5 || (__GNUC_MINOR__ == 5 && __GNUC_PATCHLEVEL__ >= 2)
namespace {
// gcc 4.5.2 has a bug that causes the following error unless this useless function is defined here
// boost::multi_index::detail::hashed_non_unique_tag>::node_type' is protected
inline void dummy_forEachOrderWithClientID( const orders_t& orders )
{
    throw std::logic_error("dummy_forEachOrderWithClientID called erroneously");
    orders.get<orderpair>().equal_range( uint32_t() );
}
}
#endif

} // namespace TdOrderBook_detail


// These mappings help us convert between TdOrder(New|Future|)Msg and
// the corresponding Order type, such as StockOrder, FutureOrder or Order.
// If no such mapping exists, it will return to boost::none_type, which will
// cause a nice big compiler error to let the user know this won't work.
namespace {
    template<typename OrderMsgT>
    struct TdOrderMapping
    {
        typedef boost::none_t OrderT;
    };

    template<>
    struct TdOrderMapping<TdOrderMsg>
    {
        typedef StockOrder OrderT;
    };

    template<>
    struct TdOrderMapping<TdOrderFutureMsg>
    {
        typedef FutureOrder OrderT;
    };

    template<>
    struct TdOrderMapping<TdOrderNewMsg>
    {
        typedef Order OrderT;
    };

    template<>
    struct TdOrderMapping<TdOrderReserveMsg>
    {
        typedef ReserveOrder OrderT;
    };

    template<>
    struct TdOrderMapping<TdOrderModifyMsg>
    {
        typedef ModifyOrder OrderT;
    };
}

///
/// Maintains a local list of orders
///
class TdOrderBook
    : public bb::Primer::ITimePrimeable
{
public:
    friend class TdOrderBookTester;

    // Release memory back to the pool.
    // This of course only works if the pool itself was responsible of allocating the order.
    template<typename PoolTp, typename OrderTp>
    static void deallocateOrder ( PoolTp& pool, TdOrder* order )
    {
        pool.destroy( reinterpret_cast<OrderTp*>(order) );
    }

    TdOrderBook( const std::string& seqfile_prefix, PersistentNumberBase<uint32_t>& persistent_number);
    ~TdOrderBook();

    class error :  public std::exception { public: const char* what() const throw() { return "TdOrderBook::error"; } };
    class invalid_status :  public error { public: const char* what() const throw() { return "TdOrderBook::invalid_status"; } };
    class invalid_orderid : public error { public: const char* what() const throw() { return "TdOrderBook::invalid_orderid"; } };
    class send_error :      public error { public: const char* what() const throw() { return "TdOrderBook::send_error"; } };

    bool prime( const bb::timeval_t & time) const override;

    template<typename OrderMsgT>
    TdOrder* addOrder( uint32_t clientid, const TdClientPtr& bbc, const OrderMsgT& order, TdTimingInfo& info );

    template<typename ExtraDataT, typename OrderMsgT>
    TdOrder* addOrder( uint32_t clientid, const TdClientPtr& bbc, const OrderMsgT& order, TdTimingInfo& info );

    void moveOrder( bb::ostatus_t oldstat, bb::ostatus_t newstat, TdOrder* order );
    void removeOrder( TdOrder* order );
    void removeOrderNoDelete( TdOrder* order );
    bool exists( uint32_t clientid, uint32_t orderid ) const;

    TdOrder* get( uint32_t clientid, uint32_t orderid );
    TdOrder* get( uint32_t seqnum );
    TdOrder* get( const bb::tid_t &tradeid );

    uint32_t getNumOrders( uint32_t clientid ) const;
    uint32_t getNumOrders() const { return orders.size(); }

    // returns NULL on success, error message otherwise
    const char* approve( uint32_t clientid, uint32_t orderid );
    void assignSeq( TdOrder* order, uint32_t seq ) const;

    uint32_t getNextOrderSeqNum() const { return next_seq; }
    void setNextOrderSeqNum( uint32_t order_seq_num ) { next_seq.set( order_seq_num ); }
    uint32_t useNextOrderSeqNum() { return ++next_seq; }

    // We allow storing 'extra data' within an order. That way, a Td could assign information
    // that is only useful to itself whilst the order can be processed unharmed by other parts
    // of the system. This 'extra data' is stored as a pointer.
    // CompactTdOrder helps us keep memory close by - we allocate the ExtraData at the end of
    // the struct.
    template<typename OrderT,
        typename OrderMsgT,
        typename ExtraDataT>
    class CompactTdOrder : public OrderT
    {
        public:
            CompactTdOrder( TdClient* client,
                    const OrderMsgT& order,
                    const TdOrder::DeallocFunction& deallocFunc = boost::bind(&CompactTdOrder<OrderT, OrderMsgT, ExtraDataT>::deleteOrder, _1) )
                : OrderT ( client, order, deallocFunc )
            {
                OrderT::setOrderExtraData( &m_extra );
            }

            virtual ~CompactTdOrder()
            {
                // We make sure to set the pointer back to nullptr, otherwise '~OrderT()' will
                // try and delete it again.
                OrderT::setOrderExtraData ( nullptr );
            }

            static void deleteOrder ( void * order )
            {
                delete reinterpret_cast< CompactTdOrder<OrderT, OrderMsgT, ExtraDataT>* > ( order );
            }

            friend class TdOrderBook;
        private:
            ExtraDataT m_extra;
    };

protected:

    // If we know how to turn this 'OrderMsgT' into an order, then:
    // - Define a new 'CompactOrder' type, which contains the order itself plus 'ExtraDataT' extra data.
    // - Define an object_pool that knows how to allocate/deallocate such objects.
    // - Set up a static instance of such a pool, and a deallocator ready.
    // - Have the pool create a 'CompactOrder' but return an TdOrder*
    template<typename ExtraDataT, typename OrderMsgT>
    typename boost::disable_if< boost::is_same<ExtraDataT, boost::none_t>, TdOrder* >::type
    allocateOrder( TdClient* bbc, const OrderMsgT& order  )
    {
        typedef typename TdOrderMapping<OrderMsgT>::OrderT OrderTp;
        typedef CompactTdOrder < OrderTp , OrderMsgT, ExtraDataT > CompactOrderTp;
        typedef ObjectPool< CompactOrderTp, OrderTp, ExtraDataT > CompactOrderPool;
        static CompactOrderPool pool;
        static TdOrder::DeallocFunction destructor = boost::bind( &TdOrderBook::deallocateOrder<CompactOrderPool, CompactOrderTp>, boost::ref(pool), _1 );
        CompactOrderTp * o = pool.construct(bbc, order, destructor );
        return o;
    }

    // If we know how to turn this 'OrderMsgT' into an order, then:
    // - Define an object_pool that knows how to allocate/deallocate the order.
    // - Set up a static instance of such a pool, and a deallocator ready.
    // - Have the pool create a new order of the right type but return an TdOrder*
    template<typename ExtraDataT, typename OrderMsgT>
    typename boost::enable_if< boost::is_same<ExtraDataT, boost::none_t>, TdOrder* >::type
    allocateOrder( TdClient* bbc, const OrderMsgT& order  )
    {
        typedef typename TdOrderMapping<OrderMsgT>::OrderT OrderTp;
        typedef ObjectPool< OrderTp, OrderMsgT, boost::none_t > OrderPool;
        static OrderPool pool;
        static TdOrder::DeallocFunction destructor = boost::bind( &TdOrderBook::deallocateOrder<OrderPool, OrderTp>, boost::ref(pool), _1 );
        OrderTp * o = pool.construct(bbc, order, destructor );
        return o;
    }

    // The default case if you're not going to supply extra data.
    // In this case, we will use boost::none_t for ExtraData, which will
    // make sure we call the appropriate overload above.
    template<typename OrderMsgT>
    TdOrder* allocateOrder( TdClient* bbc, const OrderMsgT& order  )
    {
        return allocateOrder<boost::none_t, OrderMsgT>(bbc, order);
    }

private:

    //TODO ( after DEV-3417 ):
    // - This ObjectPool should be a PreallocatedObjectFactory
    // - TdOrder should be a 'FactoryObject'
    // But yeah, only after DEV-3417 because we don't want to *introduce* a dependency
    // on shared pointers. In orders of all places.
    template<typename ObjectType, typename BaseType, typename ExtraType>
    class ObjectPool : public boost::object_pool<ObjectType>
    {
        public:
        ObjectPool() : boost::object_pool<ObjectType>(256) {}
        ~ObjectPool()
        {
            if ( boost::object_pool<ObjectType>::get_next_size() != 256 )
            {
                std::string object_name, extra_name;
                // demangle object name
                {
                    int rv;
                    char * demangled =  abi::__cxa_demangle(typeid(BaseType).name(), 0, 0, &rv);
                    object_name = demangled;
                    free(demangled);
                }
                // if set, demangle extra name
                if ( !boost::is_same < boost::none_t, ExtraType >::value )
                {
                    int rv;
                    char * demangled =  abi::__cxa_demangle(typeid(ExtraType).name(), 0, 0, &rv);
                    extra_name = demangled;
                    free(demangled);
                }
                LOG_WARN <<
                    "Order pool for " <<
                    object_name;
                if ( !extra_name.empty() )
                {
                    LOG_WARN << "<" << extra_name << "> ";
                }
                LOG_WARN <<
                    " was resized. cur size: " <<
                    boost::object_pool<ObjectType>::get_next_size()/2 << bb::endl;
            }
        }
    };

    PersistentNumberBase<uint32_t>& next_seq;

    typedef TdOrderBook_detail::seqnum_t seqnum_t;
    typedef TdOrderBook_detail::orders_t orders_t;

    orders_t orders;

public:
    template<typename Action>
    void forEachOrder( Action action )
    {
        std::for_each( orders.begin(), orders.end(), action );
    }

    template<typename Action>
    void forEachOrder( Action action ) const
    {
        std::for_each( orders.begin(), orders.end(), action );
    }

    template<typename Action>
    void forEachOrderWithClientID( Action action, uint32_t clientid )
    {
        typedef orders_t::index<orderpair>::type::iterator iterator;
        std::pair<iterator, iterator> range = orders.get<orderpair>().equal_range( clientid );
        std::for_each( range.first, range.second, action );
    }
};
BB_DECLARE_SHARED_PTR(TdOrderBook);

template<typename OrderMsgT>
TdOrder* TdOrderBook::addOrder( uint32_t clientid, const TdClientPtr& bbc, const OrderMsgT& order, TdTimingInfo& info )
{
    return addOrder<boost::none_t>( clientid, bbc, order, info );
}

template<typename ExtraDataT, typename OrderMsgT>
TdOrder* TdOrderBook::addOrder( uint32_t clientid, const TdClientPtr& bbc, const OrderMsgT& order, TdTimingInfo& info )
{
    // allocate the correct order type
    TdOrder * o = allocateOrder<ExtraDataT, OrderMsgT>( bbc.get(), order );

    // Debug mode sanity checks - an error here means an implementation failure, not something wrong with the order.
    BB_THROW_EXASSERT_SSX_DEBUG ( ( !boost::is_same<OrderMsgT, TdOrderFutureMsg>::value || dynamic_cast<FutureOrder*>(o) != nullptr ), "Expected a FutureOrder to be built!" );
    BB_THROW_EXASSERT_SSX_DEBUG ( ( !boost::is_same<OrderMsgT, TdOrderMsg>::value || dynamic_cast<StockOrder*>(o) != nullptr ), "Expected a StockOrder to be built!" );
    BB_THROW_EXASSERT_SSX_DEBUG ( ( !boost::is_same<OrderMsgT, TdOrderNewMsg>::value || dynamic_cast<TdOrder*>(o) != nullptr ), "Expected a TdOrder to be built!" );

    o->clientid = clientid;
    o->setSeq( useNextOrderSeqNum() );
    o->orderTimings[bb::STAT_NEW] = bb::timeval_t::now;
    o->setTdTimingInfo( &info );

    if ( unlikely( !orders.insert( o ).second ) )
    {
        throw std::runtime_error( "failed to add order into TdOrderBook (duplicate ID?)" );
    }

    if( likely((bool) bbc) )
    {
        bbc->increaseOpenOrderCount();
    }

    return o;
}

} // namespace bb
} // namespace tdcore

#endif // BB_SERVERS_TDCORE_TDORDERBOOK_H
