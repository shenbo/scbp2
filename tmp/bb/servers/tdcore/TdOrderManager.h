#ifndef BB_SERVERS_TDCORE_TDORDERMANAGER_H
#define BB_SERVERS_TDCORE_TDORDERMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */
#include <boost/type_traits.hpp>
#include <boost/utility/enable_if.hpp>

#include <bb/core/Error.h>
#include <bb/core/mpl.h>

#include <bb/servers/tdcore/ITdOrderRouter.h>
#include <bb/servers/tdcore/TdOrderBook.h>
#include <bb/servers/tdcore/TdClient.h>
#include <bb/servers/tdcore/TdRetiredOrderBook.h>
#include <bb/servers/tdcore/TdTimingInfo.h>

namespace bb {
namespace tdcore {
class OrderConfirm;
class OrderReject;
class OrderExecution;
class TradeBust;
class OrderClose;
class OrderCancelReject;
class OrderReprice;
class OrderRoute;
class OrderModified;
class OrderModifyReject;
BB_FWD_DECLARE_SHARED_PTR( TdRetiredOrderBook );



///
/// Order Manager, takes in order submission from BB
///
class TdOrderManager
{
public:

    struct ErrOrderNotFound : public std::runtime_error
    {
        ErrOrderNotFound( const std::string& _context, bool _suppressCancelReject = false )
            : std::runtime_error( _context )
            , suppressCancelReject( _suppressCancelReject ){}
        bool suppressCancelReject;
    };
    struct WarnInvalidRequest : public std::runtime_error
    {
        WarnInvalidRequest( const std::string& _context ) : std::runtime_error( _context ) {}
    };

    TdOrderManager( ITdOrderRouter* _orderRouter,
                    const std::string& seqfile_prefix,
                    const TdOrderManagerConfig& config = TdOrderManagerConfig());

    void setRememberDoneOrders( bool b ) { m_properties.setRememberDoneOrders( b ); }
    ///
    /// Check an order for sanity
    ///
    /// @param order message
    ///
    /// Templatized to handle all OrderMsg types except TdOrderNewMsg and TdOrderModifyMsg
    template<typename OrderMsgT>
    static typename boost::enable_if<
        bb::both_false <
            typename boost::is_same<OrderMsgT, TdOrderNewMsg>::type,
            typename boost::is_same<OrderMsgT, TdOrderModifyMsg>::type >
        , bool>::type
    checkOrderBasic( const OrderMsgT& order, acct_t account );

    /// check order sanity for TdOrderNewMsg and TdOrderModifyMsg, since those
    /// will not be handled by the templatized checkOrderBasic
    static bool checkOrderBasic( const TdOrderNewMsg& order, acct_t account );
    static bool checkOrderBasic( const TdOrderModifyMsg& modifyMsg, acct_t account );

    ///
    /// Process a new order message
    ///
    /// @param order message
    /// @param session client to sent the order
    ///
    template<typename OrderMsgT>
    TdOrder* processOrder( const OrderMsgT& order
            , const TdClientPtr& bbc
            , bb::tdcore::TdTimingInfo & info);

    template<typename ExtraDataT, typename OrderMsgT>
    TdOrder* processOrder( const OrderMsgT& order
            , const TdClientPtr& bbc
            , bb::tdcore::TdTimingInfo & info);

    ///
    /// Handle a cancel message
    ///
    /// @param cancel message
    /// @param bbc session client to sent the cancel
    ///
    /// \exception ErrOrderNotFound
    /// \exception WarnInvalidRequest
    ///
    void processCancel( const bb::TdCancelMsg& cancel, const TdClientPtr& bbc );


    ///
    /// Process a modify order message
    ///
    /// @param modify message
    /// @param session client to sent the order
    ///
    void processModify( const TdOrderModifyMsg& modifyMsg
                        , const TdClientPtr& bbc
                        , bb::tdcore::TdTimingInfo & info);


    ///
    /// Cancel all known orders
    ///
    void processCancelAll();
    void processCancelAll( acct_t acct );
    void processCancelAll( side_t side, symbol_t symbol );
    void processCancelAll( side_t side, instrument_t instr );
    void processCancelAll( acct_t acct, side_t side, instrument_t symbol );
    void processCancelAll( uint32_t clientId, side_t side, instrument_t symbol );
    void processCancelAll( uint32_t clientId );

    void cancelOrder( const bb::tid_t& tid );

    ///
    /// Force Done orders
    ///
    void forceDoneOrder( const bb::tid_t& tid);
    void processForceDoneAll( side_t side, symbol_t symbol );
    void processForceDoneAll( acct_t acct, side_t side, const bb::instrument_t& instr );
    void processForceDoneAll( uint32_t clientId );

    // TODO: fixme - we have to make something that does a find by exchange ID
    // temporarily use the orderid as a sequence number
    void handle( const OrderConfirm& conf );
    void handle( const OrderReject& reject );
    void handle( const OrderExecution& exec );
    void handle( const TradeBust& exec );
    void handle( const OrderClose& close );
    void handle( const OrderCancelReject& creject );
    void handle( const OrderReprice& reprice );
    void handle( const OrderRoute& route );
    void handle( const OrderModified& modify );
    void handle( const OrderModifyReject& modifyReject );

    TdOrder* getOrder( const bb::tid_t& tradeid, const char* errmsg ) throw (ErrOrderNotFound);
    TdOrder* getOrder( uint32_t clientid, uint32_t orderid );

    void moveOrder( bb::ostatus_t oldstat, bb::ostatus_t newstat, TdOrder *order )
    {
        try
        {
            m_orderBook.moveOrder( oldstat, newstat, order );
        }
        catch( TdOrderBook::send_error& e )
        {
            LOG_WARN << "oid: " << order->getOrderId() << ' ' << order->getTradeId()
                     << " failed to send order status change to client" << bb::endl;
        }
        catch( TdOrderBook::error& e )
        {
            LOG_PANIC << "oid: " << order->getOrderId() << ' ' << order->getTradeId()
                      << " failed to change order status: " << e.what() << bb::endl;
        }
    }

    void logStatus() const;
    void onClientDisconnect( const TdClientPtr& bbc );

    uint32_t getNextOrderSeqNum() const { return m_orderBook.getNextOrderSeqNum(); }
    void setNextOrderSeqNum( uint32_t order_seq_num ) { m_orderBook.setNextOrderSeqNum( order_seq_num ); }

    template<typename Action>
    void forEachOrder( Action action )
    {
        m_orderBook.forEachOrder( action );
    }

    template<typename Action>
    void forEachOrder( Action action ) const
    {
        m_orderBook.forEachOrder( action );
    }

    template<typename Action>
    void forEachRetiredOrder( Action action ) const
    {
        m_spRetiredOrderBook->forEachOrder( action );
    }

    bool cancelOrder( TdOrder* order ); // returns false if cancel was throttled (throws on failure)

    const TdOrderManagerConfig& getTdOrderManagerConfig() const {
        return m_config;
    }

    TdOrderBook & getOrderBook()
    {
        return m_orderBook;
    }

private:
    PersistentNumberBase<uint32_t>* getPersistentNumber();

    void cancelOrder( TdOrder* order, size_t* count ); // catches common exceptions (for use in loops)
    void cancelOrder( TdOrder* order, acct_t account, side_t side, instrument_t instr, size_t* count );

    void forceDoneOrder( TdOrder* );
    void forceDoneOrder( TdOrder*, size_t*);
    void forceDoneOrder( TdOrder*, acct_t, side_t, const bb::instrument_t&, size_t* count );

    /// Create a new TdOrder for an incoming message
    /// @param order message
    /// @param client pointer
    /// @param timing info
    template<typename OrderMsgT>
    TdOrder* createNewTdOrder( const OrderMsgT& orderMsg, const TdClientPtr& bbc,
                            bb::tdcore::TdTimingInfo & info );

    /// Create a new TdOrder for an incoming message
    /// Pre-allocate and set the 'ExtraData' field as well.
    /// @param order message
    /// @param client pointer
    /// @param timing info
    template<typename ExtraDataT, typename OrderMsgT>
    TdOrder* createNewTdOrder( const OrderMsgT& orderMsg, const TdClientPtr& bbc,
                        bb::tdcore::TdTimingInfo & info );

    /// Get TdOrder associated with orderid mentioned in the message
    /// @param order message
    /// @param client
    template<typename MsgT>
    TdOrder* getTdOrder( const MsgT& msg, const TdClientPtr& bbc );

    /// Checks common parts of all order messages
    template<typename OrderMsgT>
    static bool checkOrderBasicCommon( const OrderMsgT& order, acct_t account );

    void logOrderTimings( const TdOrder& o );
    void processOrderImpl( TdOrder* o, bb::tdcore::TdTimingInfo& info );

    const TdOrderManagerConfig m_config;

    ITdOrderRouter* m_orderRouter;
    bb::shared_ptr<PersistentNumberBase<uint32_t> > m_persistentNumberPtr;
    TdOrderBook m_orderBook;
    TdRetiredOrderBookPtr m_spRetiredOrderBook;

    class Properties
    {
    public:
        Properties() : m_bRememberDoneOrders( false ) {}
        bool getRememberDoneOrders() const { return m_bRememberDoneOrders; }
        void setRememberDoneOrders( bool b ) { m_bRememberDoneOrders = b; }
    protected:
        bool m_bRememberDoneOrders;
    } m_properties;
};
BB_DECLARE_SHARED_PTR(TdOrderManager);

template<typename MsgT>
TdOrder* TdOrderManager::getTdOrder( const MsgT& msg, const TdClientPtr& bbc )
{
    TdOrder* order = m_orderBook.get( bbc->getId(), msg.getOrderid() );
    if( unlikely( !order ) )
    {
        std::ostringstream errMsg;
        errMsg << "TdOrderManager::getOrder oid: " << msg.getOrderid();
        throw ErrOrderNotFound( errMsg.str(), m_config.m_bSuppressCancelRejects );
    }

    return order;
}

template<typename OrderMsgT>
TdOrder* TdOrderManager::createNewTdOrder( const OrderMsgT& orderMsg, const TdClientPtr& bbc,
                        bb::tdcore::TdTimingInfo & info )
{
    return createNewTdOrder<boost::none_t>(orderMsg, bbc, info);
}

template<typename ExtraDataT, typename OrderMsgT>
TdOrder* TdOrderManager::createNewTdOrder( const OrderMsgT& orderMsg, const TdClientPtr& bbc,
                        bb::tdcore::TdTimingInfo & info )
{
    if ( const char* error = m_orderBook.approve( bbc->getId(), orderMsg.getOrderid() ) )
    {
        throw WarnInvalidRequest( error );
    }

    if ( unlikely( !m_orderRouter->isActive() ) )
    {
        throw ITdOrderRouter::WarnSendFailedNotLoggedOn();
    }

    return m_orderBook.addOrder<ExtraDataT>( bbc->getId(), bbc, orderMsg, info );
}

template<typename OrderMsgT>
TdOrder* TdOrderManager::processOrder( const OrderMsgT& order, const TdClientPtr& bbc,
                                   bb::tdcore::TdTimingInfo & info )
{
    return processOrder<boost::none_t, OrderMsgT>(order, bbc, info);
}

template<typename ExtraDataT, typename OrderMsgT>
TdOrder* TdOrderManager::processOrder( const OrderMsgT& order, const TdClientPtr& bbc,
                                   bb::tdcore::TdTimingInfo & info )
{
    TdOrder* tdOrder = createNewTdOrder<ExtraDataT, OrderMsgT>( order, bbc, info );
    BB_THROW_EXASSERT_SSX_DEBUG ( ( !boost::is_same<OrderMsgT, TdOrderFutureMsg>::value || dynamic_cast<FutureOrder*>(tdOrder) != nullptr ), "Expected a FutureOrder to be built!" );
    BB_THROW_EXASSERT_SSX_DEBUG ( ( !boost::is_same<OrderMsgT, TdOrderMsg>::value || dynamic_cast<StockOrder*>(tdOrder) != nullptr ), "Expected a StockOrder to be built!" );
    BB_THROW_EXASSERT_SSX_DEBUG ( ( !boost::is_same<OrderMsgT, TdOrderNewMsg>::value || dynamic_cast<TdOrder*>(tdOrder) != nullptr ), "Expected a TdOrder to be built!" );
    processOrderImpl( tdOrder, info );

    // Debug mode sanity checks - this should have failed in a much lower level already
    return tdOrder;
}

template<typename OrderMsgT>
typename boost::enable_if<
    bb::both_false <
        typename boost::is_same<OrderMsgT, TdOrderNewMsg>::type,
        typename boost::is_same<OrderMsgT, TdOrderModifyMsg>::type >
    , bool>::type
TdOrderManager::checkOrderBasic( const OrderMsgT& order, acct_t account )
{
    // Adding checks to make sure we don't hit this code for TdOrderNewMsg and TdOrderModifyMsg
    BOOST_STATIC_ASSERT( ( !boost::is_same<OrderMsgT, TdOrderNewMsg>::value ) );
    BOOST_STATIC_ASSERT( ( !boost::is_same<OrderMsgT, TdOrderModifyMsg>::value ) );

    // We keep this check out of the checkOrderBasicCommon due to special case of spreads.
    // Since we have template specialized for TdOrderNewMsg which is used for spread orders
    // we can safely do this check here.
    if( unlikely( order.getPx() < 0 ) )
    {
        LOG_WARN << "oid: " << order.getOrderid() << " has invalid price: " << order.getPx() << bb::endl;
        return false;
    }

    return checkOrderBasicCommon( order, account );
}

template<typename OrderMsgT>
bool TdOrderManager::checkOrderBasicCommon( const OrderMsgT& order, acct_t account )
{
    if( unlikely( order.getOrderid() <= 0 ) )
    {
        LOG_WARN << "oid: " << order.getOrderid() << " is invalid" << bb::endl;
        return false;
    }

    if( unlikely( order.getAccount() != account ) )
    {
        LOG_WARN << "oid: " << order.getOrderid() << " has wrong account: " << order.getAccount() << bb::endl;
        return false;
    }

    if( unlikely( order.getDir() != bb::BUY && order.getDir() != bb::SELL && order.getDir() != bb::SHORT ) )
    {
        LOG_WARN << "oid: " << order.getOrderid() << " has invalid direction: " << static_cast<int>( order.getDir() ) << bb::endl;
        return false;
    }

    if( unlikely( order.getSize() <= 0 || order.getSize() > 999999 ) )
    {
        LOG_WARN << "oid: " << order.getOrderid() << " has invalid size: " << order.getSize() << bb::endl;
        return false;
    }

    if( unlikely( !order.hdr->symbol.isValid() ) )
    {
        LOG_WARN << "oid: " << order.getOrderid() << " has invalid symbol ID: " << order.hdr->symbol.id() << bb::endl;
        return false;
    }

    if( unlikely( order.getDest() == bb::MKT_UNKNOWN || order.getDest() == bb::MKT_MAX || !mktdestIsValid( order.getDest() ) ) )
    {
        LOG_WARN << "oid: " << order.getOrderid() << " has invalid mktdest: " << static_cast<int>( order.getDest() ) << bb::endl;
        return false;
    }

    if( unlikely( order.getTimeout() < 0 ) )
    {
        LOG_WARN << "oid: " << order.getOrderid() << " has invalid timeout: " << order.getTimeout() << bb::endl;
        return false;
    }

    if( unlikely( order.getTif() != bb::TIF_SPECIFIED && order.getTif() != bb::TIF_ABSOLUTE && order.getTimeout() != 0 ) )
    {
        LOG_WARN << "oid: " << order.getOrderid() << " has timeout with inappropriate TIF: " << order.getTif() << bb::endl;
        return false;
    }

    return true;
}

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDORDERMANAGER_H
