#ifndef BB_SERVERS_TDCORE_MULTIACCOUNTRISKCONTROL_H
#define BB_SERVERS_TDCORE_MULTIACCOUNTRISKCONTROL_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/shared_ptr.hpp>
#include <bb/servers/tdcore/TdRiskControl.h>
#include <bb/core/instr_hashmap.h>
#include <bb/core/Primer.h>

class MultiAccountRiskControlTest;

namespace bb {
namespace tdcore {

BB_FWD_DECLARE_SHARED_PTR( TdRiskControl );
BB_FWD_DECLARE_SHARED_PTR( MultiAccountTdPositionBroadcaster );

class IPositionBroadcaster
{
public:
    virtual ~IPositionBroadcaster() {}

    virtual void setupPositionBroadcast( acct_t acct, const instrument_t& instr ) = 0;
};

///
/// The CombinedTdRiskControl object is used to group together the Risk Control object
/// of a startegy account and an exchange account.  All risk operations ( i.e. open
/// close/approve/fill ) are executed on both objects.  Operations are executed on the
/// account object first and then on the exchange object.  Use this class to avoid the
/// map look-ups inherent in the MultiAccountRiskControl.
///
/// Note, that this object operates directly on the underlying risk control objects and
/// all the other operations performed by the MultiAccountRiskControl, such as updating
/// position and updating the broadcaster are skipped.
///
class CombinedTdRiskControl
    : public ITdRiskControl
{
public:
    ///
    /// Create a new Combined Risk control object. params:
    ///   account_risk   - a reference to the BB account risk control object
    ///   exchange_risk  - a reference to the exchange account risk control
    CombinedTdRiskControl(ITdRiskControl& account_risk, ITdRiskControl& exchange_risk)
        : m_accountRiskControl( account_risk )
        , m_exchangeRiskControl( exchange_risk )
    {};

    virtual ~CombinedTdRiskControl(){};

    /// handles a fill that does not correspond to an approved order
    /// this can be used for margin_create_fill messages, or for fill daemons processing out-of-band order flow
    virtual int32_t fill_without_order( dir_t dir, const instrument_t& instr, double price, int32_t size )
    {
        const int32_t pos = m_accountRiskControl.fill_without_order( dir, instr, price, size );
        m_exchangeRiskControl.fill_without_order( dir, instr, price, size );
        return pos;
    }

    // public methods are to be called only from the thread which constructed this
    virtual bool approve( const TdOrderMsg& order )        { return approveImpl( order ); } // true if order is approved
    virtual bool approve( const TdOrderFutureMsg& order )  { return approveImpl( order ); } // true if order is approved
    virtual bool approve( const TdOrderNewMsg& order )     { return approveImpl( order ); } // true if order is approved
    virtual bool approve( const TdOrderModifyMsg& modifyMsg ) { return approveImpl( modifyMsg ); } // true if order is approved

    // submit order (must have been approved previously)
    virtual void open ( const TdOrder& order )
    {
        m_accountRiskControl.open( order );
        m_exchangeRiskControl.open( order );
    }

    // end order (filled or not)
    virtual void close( const TdOrder& order )
    {
        m_accountRiskControl.close( order );
        m_exchangeRiskControl.close( order );
    }

    // update position for an open order and return shares of the instrument

    virtual int32_t fill( dir_t dir, const instrument_t& instr, double price, int32_t size, fillflag_t fillflag = FILLFLAG_NONE ) // for unknown executions
    {
        const int32_t pos = m_accountRiskControl.fill( dir, instr, price, size, fillflag );
        m_exchangeRiskControl.fill( dir, instr, price, size, fillflag );
        return pos;
    }

    virtual int32_t fill( const TdOrder& order, double price, int32_t size ) // partial or complete fill received
    {
        const int32_t pos = m_accountRiskControl.fill( order, price, size );
        m_exchangeRiskControl.fill( order, price, size );
        return pos;
    }
    virtual int32_t late_fill( const TdOrder& order, double price, int32_t size )
    {
        const int32_t pos = m_accountRiskControl.late_fill( order, price, size );
        m_exchangeRiskControl.late_fill( order, price, size );
        return pos;
    }

    virtual int32_t bust( const TdOrder& order, double price, int32_t size ) // automated trade bust "undoes" the fill
    {
        const int32_t pos = m_accountRiskControl.bust( order, price, size );
        m_exchangeRiskControl.bust( order, price, size );
        return pos;
    }

    virtual void setSymbolPrice( const instrument_t& instr, double price ) // set price and adjust account values
    {
        m_accountRiskControl.setSymbolPrice( instr, price );
        m_exchangeRiskControl.setSymbolPrice( instr, price );
    }
    void setSymbolNotionalValueFromPrice( const instrument_t& instr, double price )
    {
        m_accountRiskControl.setSymbolNotionalValueFromPrice( instr, price );
        m_exchangeRiskControl.setSymbolNotionalValueFromPrice( instr, price );
    }

    // these functions are not supported because it may be ambiguous if they should be applied to the
    // account or exchange risk control.  To avoid confusion get the base object of the desired account
    // and execute these functions on that object
    virtual void halt()                                       { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual void halt( const instrument_t& instr )            { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual void resume()                                     { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual void resume( const instrument_t& instr )          { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual bool isHalted() const                             { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual void populateMessage( MarginLimitsMsg& msg )      { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual void populateMessage( MarginAccountInfoMsg& msg ) { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }

protected:
    template< typename MsgT >
    bool approveImpl( const MsgT& order )
    {
        return m_accountRiskControl.approve( order ) && m_exchangeRiskControl.approve( order );
    }

    ITdRiskControl&       m_accountRiskControl;
    ITdRiskControl&       m_exchangeRiskControl;
};
BB_DECLARE_SHARED_PTR( CombinedTdRiskControl );

class MultiAccountRiskControl
    : public ITdRiskControl
    , public bb::Primer::IInstrumentPrimeable
    , public bb::Primer::ITimePrimeable
{
public:
    BB_FWD_DECLARE_SHARED_PTR( AccountData );

    class InstrumentData : public SymbolData
    {
    public:
        InstrumentData( const instrument_t instr = instrument_t::unknown() );
        void increment( const tdcore::SymbolData& sd );
        void updatePosition( dir_t dir, int32_t size );
        const instrument_t m_instr;
    };

    typedef bbext::hash_map<bb::acct_t, AccountDataPtr >           AccountTable;
    typedef bbext::hash_map<bb::acct_t, CombinedTdRiskControlPtr > CombinedRiskControlMap;
    typedef bb::instr_hashmap<InstrumentData>                      InstrumentDataTable;

    typedef InstrumentDataTable::iterator iterator;

public:
    MultiAccountRiskControl();
    virtual ~MultiAccountRiskControl();

    void addAccount( const acct_t& acct,
                     const TdRiskControlPtr& risk_control,
                     const acct_t& exch_acct,
                     const TdRiskControlPtr& exchange_account_risk_control );

    // backward compatible for TDs that don't use exchange-account-level risk control
    void addAccount( const acct_t& acct,
                     const TdRiskControlPtr& risk_control );

    TdRiskControlPtr & getRiskControl( const acct_t& acct ) const;

    // Returns a CombinedTdRiskControl object
    // Note that when using the combinedTdRiskControl object the price
    // will not be updated on a fill and the global positions will not
    // be updated.
    CombinedTdRiskControl& getCombinedRiskControl( const acct_t& acct );
    CombinedTdRiskControlPtr getCombinedRiskControlPtr(const acct_t& acct );

    void setDefaultAccount( const acct_t& acct)
    {
        m_defaultAccount = acct;
    }

    const acct_t& getDefaultAccount() const
    {
        return m_defaultAccount;
    }

    virtual bool approve( const TdOrderMsg& order ); // true if order is approved
    virtual bool approve( const TdOrderFutureMsg& order ); // true if order is approved
    virtual bool approve( const TdOrderNewMsg& order ); // true if order is approved
    virtual bool approve( const TdOrderModifyMsg& order ); // true if order is approved

    virtual void open( const TdOrder& order ); // submit order (must have been approved previously)
    virtual void close( const TdOrder& order ); // end order (filled or not)
    virtual int32_t fill( dir_t dir, const instrument_t& instr, double price, int32_t size, fillflag_t fillflag ); // for unknown executions
    virtual int32_t fill( const TdOrder& order, double price, int32_t size ); // partial or complete fill received
    virtual int32_t late_fill( const TdOrder& order, double price, int32_t size );
    int32_t fill_on_account( acct_t acct, dir_t dir, const instrument_t& instr, double price, int32_t size );
    virtual int32_t bust( const TdOrder& order, double price, int32_t size ); // automated trade bust "undoes" the fill

    virtual void setupPositionBroadcast( acct_t acct, const instrument_t& instr );

    /// returns <price,actual,baseline>
    virtual const SymbolData& getSymbolData( const acct_t& acct, const instrument_t& instr ) const;
    virtual void setSymbolPrice( const instrument_t& instr, double price );
    virtual void setSymbolNotionalValueFromPrice( const instrument_t& instr, double price );
    const SymbolData& getInstrumentData( const instrument_t& instr ) const;

    void savePositions( acct_t acct, const std::string& filename ) const;
    void saveAllPositions();
    void broadcastRiskLimits() const;

    void setInstrumentPriceIfUnset( const instrument_t& instr );
    MultiAccountTdPositionBroadcasterPtr getPositionBroadcaster( acct_t acct );
    void setSymbolSize( acct_t acct, const instrument_t& instr, int32_t pos, bool bsend_immed = false );

    virtual int32_t getMaxOpenOrders() const;

    void synchronizePositions( const instrument_t& instr, int32_t pos, acct_t default_account );

    void updateAggregate();

    iterator begin() { return m_instrumentDataTable.begin(); }
    iterator end() { return m_instrumentDataTable.end(); }

    void printPositions();

    void populateMessage( bb::MarginLimitsMsg& msg )
    {
        throw std::logic_error( "MultiAccountRiskControl::populateMessage: not implemented" );
    }

    void populateMessage( bb::MarginAccountInfoMsg& msg )
    {
        throw std::logic_error( "MultiAccountRiskControl::populateMessage: not implemented" );
    }

    // avoid halt hiding errors
    using ITdRiskControl::halt;
    virtual void halt();

    // avoid resume hiding errors
    using ITdRiskControl::resume;
    virtual void resume();

    void haltAccount( const bb::acct_t& acct );
    void resumeAccount( const bb::acct_t& acct );

protected:
    bool prime(bb::instrument_t const & instr) const override;
    bool prime(bb::timeval_t const & time) const override;
    friend class ::MultiAccountRiskControlTest;

    InstrumentData& getInstrumentData_int( const instrument_t& instr );
    const InstrumentData& getInstrumentData_int( const instrument_t& instr ) const;
    void updatePosition( dir_t dir, const instrument_t& instr, int32_t size );
    void flush( const instrument_t& instr );

protected:
    CombinedRiskControlMap    m_combinedRiskControlMap;

    AccountTable m_accountTable;

    // mapping from internal account to exchange-account level risk control
    AccountTable m_exchangeAccountRiskControlTable;

    // mapping from exchange account to risk control
    AccountTable m_createdExchangeAccounts;

    mutable InstrumentDataTable m_instrumentDataTable;
    bb::acct_t m_defaultAccount;

private:
    template<typename OrderMsgT>
    bool approveImpl( const OrderMsgT& order );

};

BB_DECLARE_SHARED_PTR( MultiAccountRiskControl );

} // tdcore
} // bb

#endif // BB_SERVERS_TDCORE_MULTIACCOUNTRISKCONTROL_H
