#ifndef BB_SERVERS_TDCORE_MULTIACCOUNTTDPOSITIONBROADCASTER_H
#define BB_SERVERS_TDCORE_MULTIACCOUNTTDPOSITIONBROADCASTER_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>
#include <boost/random.hpp>

#include <bb/core/acct.h>
#include <bb/core/instrument.h>
#include <bb/core/symbol.h>

#include <bb/servers/tdcore/TdPositionBroadcasterCore.h>

namespace bb {

namespace tdcore {

BB_FWD_DECLARE_SHARED_PTR( MultiAccountTdPositionBroadcaster );

/// Broadcasts position updates periodically, using TdRiskControl to provide the position data
/// This class is different from TdPositionBroadcaster because it is a singleton and supports multi account
class MultiAccountTdPositionBroadcaster
    : public TdPositionBroadcasterCore
{
public:

    MultiAccountTdPositionBroadcasterPtr addAccount( const TdRiskControl* riskControl, acct_t& account );

    static MultiAccountTdPositionBroadcasterPtr& getInstance()
    {
        static MultiAccountTdPositionBroadcasterPtr singleton(new MultiAccountTdPositionBroadcaster);
        if( !m_singleton )
            m_singleton = singleton;

        return m_singleton;
    }

private:

    // singleton instance variable
    static MultiAccountTdPositionBroadcasterPtr m_singleton;

    // prevent construction of this class as it is a singleton
    MultiAccountTdPositionBroadcaster() {};
    // prevent accidental copy of this class as it is a singleton
    MultiAccountTdPositionBroadcaster( MultiAccountTdPositionBroadcaster const& );
    void operator=( MultiAccountTdPositionBroadcaster const & );

    virtual void broadcastPositions() const; // m_broadcastThread entry function

};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_MULTIACCOUNTTDPOSITIONBROADCASTER_H