#ifndef BB_SERVERS_TDCORE_TDORDERSTATE_H
#define BB_SERVERS_TDCORE_TDORDERSTATE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/mktdest.h>
#include <bb/core/oreason.h>
#include <bb/core/ostatus.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>

#include <bb/servers/tdcore/TdTimingInfo.h>

namespace bb {
namespace tdcore {
BB_FWD_DECLARE_SHARED_PTR( ITdClient );
class TdOrder;
class ModifyOrder;
class TdClient;
class TdOrderCancelState;
class OrderCancelReject;
class OrderClose;
class OrderConfirm;
class OrderReject;
class OrderReprice;
class OrderRoute;
class ITdOrderRouter;
class TradeBust;
class OrderModified;
class OrderModifyReject;

///
/// Represents the current state of an order.
/// Delegates cancel events to the cancelState of an order.
///
class TdOrderState
{
public:
    virtual ~TdOrderState() {}
    virtual ostatus_t getStatus() const = 0;
    virtual void onNewRequest( TdOrder* o, ITdOrderRouter* r,
            const bb::tdcore::TdTimingInfo & info = bb::tdcore::TdTimingInfo::empty() ) const { BadEvent( "NewRequest" ); }
    virtual void onOrderClose( TdOrder* o, ITdOrderRouter* r
        , const OrderClose& close ) const { BadEvent( "OrderClose" ); }
    virtual void onOrderReject( TdOrder* o, ITdOrderRouter* r
        , const OrderReject& creject ) const { BadEvent( "OrderReject" ); }
    virtual void onOrderConfirm( TdOrder* o, ITdOrderRouter* r
        , const OrderConfirm& confirm ) const { BadEvent( "OrderConfirm" ); }
    virtual void onOrderReprice( TdOrder* o, ITdOrderRouter* r
        , const OrderReprice& b ) const { BadEvent( "OrderReprice" ); }
    virtual void onOrderRoute( TdOrder* o, ITdOrderRouter* r
        , const OrderRoute& b ) const { BadEvent( "OrderRoute" ); }
    virtual void onTradeBust( TdOrder* o, ITdOrderRouter* r
        , const TradeBust& b ) const { BadEvent( "TradeBust" ); }

    virtual void onCancelRequest( TdOrder* o, ITdOrderRouter* r ) const { BadEvent( "CancelRequest" ); }
    virtual void onCancelReject( TdOrder* o, ITdOrderRouter* r
        , const OrderCancelReject& creject ) const;

    virtual void onModifyRequest( TdOrder* origOrder, ModifyOrder* newOrder, ITdOrderRouter* r,
                               const bb::tdcore::TdTimingInfo & info = bb::tdcore::TdTimingInfo::empty() ) const
    { BadEvent( "ModifyRequest" ); }

    virtual void onOrderModified( TdOrder* origOrder, TdOrder* newOrder, ITdOrderRouter* orderRouter
                                  , const OrderModified& modify ) const { BadEvent( "OrderModified" ); }
    virtual void onModifyReject( TdOrder* origOrder, TdOrder* newOrder
                                 , ITdOrderRouter* orderRouter, const OrderModifyReject& modifyReject ) const
    { BadEvent( "ModifyReject" ); };

protected:
    const TdOrderState* nextState( TdOrder* order
        , const TdOrderState* s
        , const timeval_t& exch_time = timeval_t()
        , const oreason_t reason = R_NONE
        , const bool bsend_change = true
        , const bb::tdcore::TdTimingInfo & info = bb::tdcore::TdTimingInfo::empty() ) const;
    void BadEvent( const std::string& func ) const;

    static void sendStatusChange( TdClient* bbc
        , TdOrder* order
        , const TdOrderState* prev_state
        , const TdOrderCancelState* prev_cxlstate
        , oreason_t reason
        , const timeval_t& exch_time = timeval_t()
        , const timeval_t& recv_time = timeval_t()
        , const timeval_t& nic_time = timeval_t()
        , TdTimingInfo const & timing_info = TdTimingInfo::empty() );

    static void sendModifyStatusChange(
        TdClient* bbc,
        TdOrder* origOrder,
        TdOrder* newOrder,
        const TdOrderState* prev_state_new,
        const TdOrderState* prev_state_orig,
        const TdOrderCancelState* prev_cxlstate_new,
        const TdOrderCancelState* prev_cxlstate_orig,
        oreason_t new_reason,
        oreason_t orig_reason,
        const timeval_t& exch_time = timeval_t(),
        const timeval_t& recv_time = timeval_t(),
        const timeval_t& nic_time = timeval_t(),
        TdTimingInfo const & timing_info = TdTimingInfo::empty()
        );

    static void sendReprice( TdClient* bbc
        , TdOrder* order
        , double price
        , const timeval_t& exch_time = timeval_t() );

    static void sendRoute( TdClient* bbc
        , TdOrder* order
        , mktdest_t via_mkt );

    void sendModify( TdOrder* origOrder, ModifyOrder* newOrder
                     , ITdOrderRouter* orderRouter, const bb::tdcore::TdTimingInfo& info ) const;
};

template<typename T, ostatus_t s> class TdOrderStateImpl : public TdOrderState
{
public:
    static const TdOrderState* instance()
    {
        static T obj;
        return &obj;
    }
    virtual ostatus_t getStatus() const { return s; }
};

class TdOrderStateNew : public TdOrderStateImpl<TdOrderStateNew, STAT_NEW>
{
public:
    virtual void onNewRequest( TdOrder* o
            , ITdOrderRouter* r
            , const bb::tdcore::TdTimingInfo & info ) const;
    virtual void onCancelRequest( TdOrder* o, ITdOrderRouter* r ) const;
    virtual void onModifyRequest( TdOrder* origOrder, ModifyOrder* newOrder
                                  , ITdOrderRouter* orderRouter, const bb::tdcore::TdTimingInfo& info ) const;
};

class TdOrderStateTransit : public TdOrderStateImpl<TdOrderStateTransit, STAT_TRANSIT>
{
public:
    virtual void onOrderRoute( TdOrder* o, ITdOrderRouter* r, const OrderRoute& route ) const;
    virtual void onOrderConfirm( TdOrder* o, ITdOrderRouter* r, const OrderConfirm& confirm ) const;
    virtual void onOrderReprice( TdOrder* o, ITdOrderRouter* r, const OrderReprice& reprice ) const;
    virtual void onOrderReject( TdOrder* o, ITdOrderRouter* r, const OrderReject& creject ) const;
    virtual void onOrderClose( TdOrder* o, ITdOrderRouter* r, const OrderClose& close ) const;
    virtual void onCancelRequest( TdOrder* o, ITdOrderRouter* r ) const;
    virtual void onModifyRequest( TdOrder* origOrder, ModifyOrder* newOrder
                                  , ITdOrderRouter* orderRouter, const bb::tdcore::TdTimingInfo& info ) const;
    virtual void onOrderModified( TdOrder* origOrder, TdOrder* newOrder, ITdOrderRouter* orderRouter
                                  , const OrderModified& modify ) const ;
    virtual void onModifyReject( TdOrder* origOrder, TdOrder* newOrder
                                 , ITdOrderRouter* orderRouter, const OrderModifyReject& modifyReject ) const;
};

class TdOrderStateOpen : public TdOrderStateImpl<TdOrderStateOpen, STAT_OPEN>
{
public:
    virtual void onOrderReprice( TdOrder* o, ITdOrderRouter* r, const OrderReprice& reprice ) const;
    virtual void onOrderReject( TdOrder* o, ITdOrderRouter* r, const OrderReject& creject ) const;
    virtual void onOrderClose( TdOrder* o, ITdOrderRouter* r, const OrderClose& close ) const;
    virtual void onTradeBust( TdOrder* o, ITdOrderRouter* r, const TradeBust& b ) const;
    virtual void onCancelRequest( TdOrder* o, ITdOrderRouter* r ) const;
    virtual void onModifyRequest( TdOrder* origOrder, ModifyOrder* newOrder
                                  , ITdOrderRouter* orderRouter, const bb::tdcore::TdTimingInfo& info ) const;
    virtual void onModifyReject( TdOrder* origOrder, TdOrder* newOrder
                                 , ITdOrderRouter* orderRouter, const OrderModifyReject& modifyReject ) const;
};

class TdOrderStateDone : public TdOrderStateImpl<TdOrderStateDone, STAT_DONE>
{
public:
    virtual void onTradeBust( TdOrder* o, ITdOrderRouter* r, const TradeBust& b ) const;
    virtual void onCancelReject( TdOrder* o, ITdOrderRouter* r
        , const OrderCancelReject& creject ) const;
    virtual void onModifyReject( TdOrder* origOrder, TdOrder* newOrder
                                 , ITdOrderRouter* orderRouter, const OrderModifyReject& modifyReject ) const;
};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDORDERSTATE_H
