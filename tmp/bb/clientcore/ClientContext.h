#ifndef BB_CLIENTCORE_CLIENTCONTEXT_H
#define BB_CLIENTCORE_CLIENTCONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <string>
#include <vector>
#include <bb/core/mktdest.h>
#include <bb/core/timeval.h>
#include <bb/core/Context.h>
#include <bb/core/InstrSource.h>
#include <bb/core/host_port_pair.h>

#include <bb/clientcore/EventDist.h>
#include <bb/clientcore/SessionParams.h>
#include <bb/clientcore/HistMStreamManager.h>
#include <bb/clientcore/LiveMStreamManager.h>
#include <bb/clientcore/DataMonitor.h>
#include <bb/core/LuaConfig.h>
#include <bb/core/TimeProvider.h>
#include <bb/core/Dispose.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Alert );
BB_FWD_DECLARE_SHARED_PTR( AcrPingHandler );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( DataMonitor );
BB_FWD_DECLARE_SHARED_PTR( HistClientContext );
BB_FWD_DECLARE_SHARED_PTR( SimHistClientContext );
BB_FWD_DECLARE_SHARED_PTR( ClientContextAdvancer );
BB_FWD_DECLARE_SHARED_PTR( IndeterminateHistClientContext );
BB_FWD_DECLARE_SHARED_PTR( DelayedHistClientContext );
BB_FWD_DECLARE_SHARED_PTR( LookAheadHistClientContext );
BB_FWD_DECLARE_SHARED_PTR( HistMStreamManager );
BB_FWD_DECLARE_SHARED_PTR( IClientTimer );
BB_FWD_DECLARE_SHARED_PTR( LiveClientContext );
BB_FWD_DECLARE_SHARED_PTR( LiveMStreamManager );
BB_FWD_DECLARE_SHARED_PTR( MsgPrinter );
BB_FWD_DECLARE_SHARED_PTR( RandomSource );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
BB_FWD_DECLARE_SHARED_PTR( ClientIdleManager );
BB_FWD_DECLARE_SHARED_PTR( IEquitySecurityInfoMap );
BB_FWD_DECLARE_SHARED_PTR( ICommoditiesSpecificationsMap );
BB_FWD_DECLARE_SHARED_PTR( MessageStatCollector );
BB_FWD_DECLARE_SHARED_PTR( TickStatsServerClient );
BB_FWD_DECLARE_SHARED_PTR( ISpinServerConnection );
BB_FWD_DECLARE_SHARED_PTR( MemAcrBookSharedMap );

/// Abstract Context for ClientCore.
/// Provides basic services such as EventDistributor, ClockMonitor, MStreamManager, alerts, and verbosity.
class ClientContext
    : public ITimeProvider
    , public Context
    , public IntrusiveWeakPtrHookable
{
public:
    virtual ~ClientContext();

    EnvironmentPtr getEnvironment() const { return m_env; }

    /// Returns the MStreamManager maintained by this context.
    virtual IMStreamManagerPtr getMStreamManager() = 0;
    /// Are we in Live or Simulated mode? (Default is false)
    virtual bool isLive()const;

    /// Main Loop
    void run();

    /// Link all streams to the event distributor but do not start the event loop
    /// The select on the underlying streams must still be driven. Do not call
    /// this function and run.
    void startDispatch();

    /// Stop all streams
    void endDispatch();

    /// Returns the EventDistributorPtr of this context.
    EventDistributorPtr const & getEventDistributor() const;

    /// Returns the ClockMonitor of this context.
    ClockMonitorPtr getClockMonitor() const;

    /// Returns the IClientTimer of this context.
    /// In a LiveClientContext this is based on the system (wall) time.
    /// In a HistClientContext this is based on message times (like ClockMonitor).
    IClientTimerPtr getClientTimer();

    /// Implement ITimeProvider of this context.
    /// In a LiveClientContext this is based on the system (wall) time.
    /// Defaults to Clockmonitor is based on message times (like ClockMonitor).
    timeval_t getTime()const;

    /// Returns the RandomSource maintained by this context.
    RandomSourcePtr getRandomSource();

    /// Returns the Alert used to store information by this ClientContext.
    AlertPtr getAlert();

    /// Returns the verbosity of this context.
    int getVerbose() const;

    /// Returns the specified start timeval of this context.
    timeval_t getStartTimeval() const;

    /// Returns the specified end timeval of this context.
    timeval_t getEndTimeval() const;

    // Support for alpha entry/exit time so we can measure how long strategists' processing takes.
    static timeval_t const & getAlphaEntryTime();
    static timeval_t const & getAlphaExitTime();
    static void resetAlphaEntryTime();
    static void resetAlphaExitTime();
    static void setAlphaEntryTime();
    static void setAlphaExitTime();

    /// Returns the client ID string of this context.
    const std::string& getIDString() const;

    /// Returns the current db profile string.
    const std::string& getDBProfileString() const;

    /// Returns the current equities security info map for this
    /// context. Note that you should not cache this value since the
    /// returned object may change across day boundaries.
    const IEquitySecurityInfoMapCPtr& getEquitySecurityInfoMap( );

    /// Returns the current commodities specifications imap for this
    /// context. Note that you should not cache this value since the
    /// returned object may change across day boundaries.
    const ICommoditiesSpecificationsMapCPtr& getCommoditiesSpecificationsMap();

    // Source Monitors:

    /// Returns a SourceMonitorPtr for a given source.
    /// If the source monitor already exists, then the manager retrieves that one.
    /// Otherwise, if the create flag is true, it creates the SourceMonitor and returns it.
    /// Throws if the proper settings do not exist in the manager.
    SourceMonitorPtr getSourceMonitor( source_t source, bool create = false, mktdest_t mktdest = MKT_UNKNOWN );

    DataMonitorPtr getDataMonitor( const DataMonitorID& id,
            DataMonitor::MonitorScope_t scope = DataMonitor::MONITOR_DURING_TRADING_HOURS_ONLY);

#ifndef SWIG
    virtual TickStatsServerClientPtr getTickStatsServerClient( EFeedType feed );
    virtual TickStatsServerClientPtr getTickStatsServerClient( const HostPortPair& hpp );

    virtual ISpinServerConnectionPtr getSpinServerConnection( const HostPortPair& hpp );
#endif // SWIG
    /// Workaround for source monitors -- add in symbols so that there's enough data coming
    /// through for the smons to do what they do.
    void setupSmonBaselineData( const std::vector<InstrSource> &smonBaselineData );

    /// Registers a function to be called any time a signal arrives. Signal handlers cannot be unregistered.
    void sigAction(int sig, const UnixSigCallback &cb);
    /// registers a signal handler to call MStreamManager::exit() on a signal.
    void sigExit(int sig);

    const bb::SessionParams& getSessionParams() const;
    void setSessionParams( const bb::SessionParams& params );


    /// Sets the timeout that will be used on the next call to select
    /// (and all subsequent ones until the next setSelectTimeout). If no data
    /// is received in this interval, an OnIdleMsg will be injected into the
    /// event distributor.
    /// boost::date_time::not_a_date_time means no timeout whatsoever.
    void setIdleTimeout(const bb::ptime_duration_t &timeout) { getMStreamManager()->setNextTimeout(timeout); }

    ClientIdleManagerPtr getIdleManager();

    /// Enable Message Statistics. This will enable keeping timing statistics for each message
    /// processed by the EventDistributor.
    /// Data will be saved into an indexed file with the name <ident>.stats.YYYYMMDD.<idx>.log.gz
    /// WARNING  This may impact performance and slow down simulations!!
    void enableMessageStatistics();

    /// Get a sharedMemoryVector for a given EFeedType
    MemAcrBookSharedMapPtr& getFeedSharedMem( const EFeedType );
protected:
    ClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv, const std::string& id,
                   int verbose, const std::string& dbProfileString );

    // default implementation is to do nothing.
    virtual ByteSinkPtr wrapWithThreadedByteSink( ByteSinkPtr byteSink ) { return byteSink; }
    std::string getFeedSharedMemName( const EFeedType type) const ;

    EnvironmentPtr          m_env;
    timeval_t               m_starttv, m_endtv;
    std::string             m_idString;
    int                     m_verbose;
    AlertPtr                m_spAlert;
    EventDistributorPtr     m_spED;
    ClockMonitorPtr         m_spCM;
    IClientTimerPtr         m_spClientTimer;
    RandomSourcePtr         m_spRandomSource;
    MsgPrinterPtr           m_spMsgPrinter;
    bb::SessionParamsPtr    m_spSessionParams;
    static timeval_t        m_alphaEntryTime, m_alphaExitTime;
    ClientIdleManagerPtr    m_idleManager;
    MessageStatCollectorPtr m_spStatsCollector;

    typedef std::map<source_t, std::vector<instrument_t> > SmonBaselineMap_t;
    SmonBaselineMap_t m_smonBaselineData;

    typedef std::map<source_t, SourceMonitorPtr> SmonMap_t;
    SmonMap_t m_sourceMonMap;


    typedef std::map<DataMonitorID, DataMonitorPtr> DataMonitorMap_t;
    DataMonitorMap_t m_dataMonitorMap;

    const std::string m_dbProfileString;
    IEquitySecurityInfoMapCPtr m_equitySecurityInfoMap;
    ICommoditiesSpecificationsMapCPtr m_commoditiesSpecificationsMap;

    typedef std::map<EFeedType, MemAcrBookSharedMapPtr>  SourceSharedMemMap;
    SourceSharedMemMap                m_sourceSharedMemMap;

    typedef std::map<HostPortPair, ISpinServerConnectionPtr> SpinServerConnectionMap;

    SpinServerConnectionMap m_spinServerConnections;
};
BB_DECLARE_SHARED_PTR( ClientContext );


/// ClientContext for running Live
class LiveClientContext
    : public ClientContext
{
public:
    typedef boost::function<void()> OnIdleCallback;

    /// Creates a LiveClientContext.
    static LiveClientContextPtr create( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                                        const std::string& id, int verbose = 0,
                                        const std::string& dbProfileString = "production", const FDSetPtr& fd = FDSetPtr() );

    /// Returns the LiveMStreamManager used by this LiveClientContext.
    LiveMStreamManagerPtr getLiveMStreamManager();

    // ClientContext
    virtual IMStreamManagerPtr getMStreamManager();

#ifndef SWIG
    virtual TickStatsServerClientPtr getTickStatsServerClient( EFeedType feed );
    virtual TickStatsServerClientPtr getTickStatsServerClient( const HostPortPair& hpp );

    virtual ISpinServerConnectionPtr getSpinServerConnection( const HostPortPair& hpp );
#endif //SWIG

    //TimeProvider
    timeval_t getTime() const;

    bool isLive() const;

protected:
    // clients use the create method
    LiveClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                       const std::string& id, int verbose, const std::string& dbProfileString, const FDSetPtr& fd);

    ByteSinkPtr wrapWithThreadedByteSink( ByteSinkPtr byteSink );

    LiveMStreamManagerPtr   m_spMStreamMgr;
    AcrPingHandlerPtr       m_spPingHandler;

    typedef std::map<HostPortPair, TickStatsServerClientPtr> TickStatsServerClientMap_t;
    TickStatsServerClientMap_t m_tickStatsServerClientMap;

private:
    // prevent accidentally calling create with id=0 (without this, it compiles and std::string's ctor throws)
    static void create( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv, int );
};

/// ClientContext for running historically
class HistClientContext
    : public ClientContext
{
#if !defined( SWIG )
public:
    typedef boost::function<bool(source_t)> UseSplitCallback;

    enum SplitMode{
        kSplit,
        kUnsplit,
        kConfigurable
    };

    /// Creates a HistClientContext.
    static HistClientContextPtr create( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                                        const std::string& id, int verbose = 0,
                                        const std::string& dbProfileString = "dbstage", ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

    static HistClientContextPtr create( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                                        const std::string& id,
                                        HistMStreamManagerPtr hist_mmgr,
                                        int verbose = 0,
                                        const std::string& dbProfileString = "dbstage" );


    /// Variant that uses an UnsplitMStreamManager.
    static HistClientContextPtr createUnsplit( const EnvironmentPtr& env, const timeval_t& starttv,
                                               const timeval_t& endtv, const std::string& id,
                                               int verbose = 0, const std::string& dbProfileString = "dbstage",
                                               ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

    /// Variant that uses an ConfigurableMStreamManager.
    static HistClientContextPtr createConfigurable( const EnvironmentPtr& env, const timeval_t& starttv,
                                                    const timeval_t& endtv, const std::string& id,
                                                    int verbose = 0, const std::string& dbProfileString = "dbstage",
                                                    ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr());

    /// Returns the HistMStreamManager used by this HistClientContext.
    HistMStreamManagerPtr getHistMStreamManager();

    // ClientContext
    virtual IMStreamManagerPtr getMStreamManager();

    // set the callback for the configurableMStreamManager
    void setUseSplitCallback( UseSplitCallback cb);

    virtual ISpinServerConnectionPtr getSpinServerConnection( const HostPortPair& hpp );

protected:
    // clients use the create method
    HistClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv, SplitMode mode,
                       const std::string& id, ByteSourceFactoryPtr byteSourceFactory, int verbose, const std::string& dbProfileString );
    HistClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                       const std::string& id, ByteSourceFactoryPtr byteSourceFactory, int verbose, const std::string& dbProfileString );
    HistClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                       const std::string& id, HistMStreamManagerPtr hist_mmgr,
                       int verbose, const std::string& dbProfileString );
    void init( const timeval_t& starttv, const timeval_t& endtv, const std::string& id, int verbose);
    void initHistMStreamMgr( const timeval_t& starttv, const timeval_t& endtv, const SplitMode mode );

    HistMStreamManagerPtr   m_spMStreamMgr;

private:
    // prevent accidentally calling create with id=0 (without this, it compiles and std::string's ctor throws)
    static void create( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv, int );

    // call back used for configurableMStreamManager
    bool useSplitCallback(source_t src);

    boost::optional<UseSplitCallback>      m_useSplitCallback;

    ByteSourceFactoryPtr m_byteSourceFactory;

#endif // defined( SWIG )
};

class SimHistClientContext
    : public HistClientContext
{
#if !defined( SWIG )
public:
    virtual void advance( const timeval_t& timeval );

protected:
    SimHistClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                              const std::string& id, HistMStreamManagerPtr hist_mmgr,
                              int verbose, const std::string& dbProfileString );
#endif // defined( SWIG )
};


#if !defined( SWIG )
class ClientContextAdvancer
{
public:
    virtual void setClientContext( const ClientContextPtr& clientContext ){ m_context = clientContext; }
    virtual bool getVerbose(){ return unlikely( m_context && m_context->getVerbose() ); }
    virtual ~ClientContextAdvancer();
    virtual void advance( const timeval_t& );
protected:
    ClientContextAdvancer( const ptime_duration_t max_advance_duration );
    virtual void runUntil( const timeval_t& );
    virtual void stop();
private:
    ClientContextPtr m_context;
    ptime_duration_t m_maxAdvanceInterval;
    timeval_t m_advanceToTime;
};
#endif // defined( SWIG )

#if !defined( SWIG )
class IndeterminateHistClientContext
    : public SimHistClientContext
{
public:

    static IndeterminateHistClientContextPtr create( const EnvironmentPtr& env,
                                                     const timeval_t& starttv, const timeval_t& endtv,
                                                     const std::string& id = "IndeterminateHistClientContext",
                                                     int verbose = 0,
                                                     const std::string& dbProfileString = "dbstage" );

    virtual void setAdvancer( const ClientContextAdvancerPtr& advancer ){ m_advancer = advancer; }
    virtual void advance( const timeval_t& timeval );

protected:
    IndeterminateHistClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                                    const std::string& id, HistMStreamManagerPtr hist_mmgr,
                                    int verbose, const std::string& dbProfileString );

    ClientContextAdvancerPtr m_advancer;
};
#endif // defined( SWIG )

class DelayedHistClientContext
    : public SimHistClientContext
{
#if !defined( SWIG )
public:
    static DelayedHistClientContextPtr create( const EnvironmentPtr& env,
                                                   const timeval_t& starttv, const timeval_t& endtv,
                                                   const ptime_duration_t& delay,
                                                   const std::string& id = "DelayedHistClientContext",
                                                   int verbose = 0,
                                                   const std::string& dbProfileString = "dbstage" );



protected:
    DelayedHistClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                              const std::string& id, HistMStreamManagerPtr hist_mmgr,
                              int verbose, const std::string& dbProfileString );
#endif // defined( SWIG )
};



class LookAheadHistClientContext
    : public SimHistClientContext
{
#if !defined( SWIG )
public:

    static LookAheadHistClientContextPtr create( const EnvironmentPtr& env,
                                                 const timeval_t& starttv, const timeval_t& endtv,
                                                 const ptime_duration_t& look_ahead_time,
                                                 const std::string& id = "LookAheadHistClientContext",
                                                 int verbose = 0,
                                                 const std::string& dbProfileString = "dbstage" );

    virtual void advance( const timeval_t& timeval );
protected:
    LookAheadHistClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv,
                                const std::string& id, HistMStreamManagerPtr hist_mmgr,
                                int verbose, const std::string& dbProfileString,
                                const ptime_duration_t& look_ahead_time );

    ptime_duration_t m_lookAheadTime;
#endif // defined( SWIG )
};




class ClientContextFactory {
public:

#if !defined( SWIG )

    enum Verbosity {
        kQuiet,
        kVerbose
    };

    enum DataMode {
        kLive,
        kHistoricalSplit,
        kHistoricalUnsplit,
        kHistoricalConfigurable
    };

    struct Config : public LuaConfig<Config> {
        friend class ClientContextFactory;

    public:
        typedef boost::function<bool(source_t)> UseSplitCallback;

        Config();

        Config( const std::string& id,
                const timeval_t& starttv,
                const timeval_t& endtv,
                DataMode dataMode,
                ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

        Config( const std::string& id,
                const timeval_t& starttv,
                const timeval_t& endtv,
                DataMode dataMode,
                Verbosity verbose,
                ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

        Config( const std::string& id,
                const timeval_t& starttv,
                const timeval_t& endtv,
                DataMode dataMode,
                const std::string& dbprofile,
                ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

        Config( const std::string& id,
                const timeval_t& starttv,
                const timeval_t& endtv,
                DataMode dataMode,
                Verbosity verbose,
                const std::string& dbprofile,
                ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

        Config( const std::string& id,
                const timeval_t& starttv,
                const timeval_t& endtv,
                DataMode dataMode,
                Verbosity verbose,
                UseSplitCallback callback,
                const std::string& dbprofile,
                ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

        static void describe();

        void setDispatcher( const FDSetPtr& fd ){ m_fd = fd; }

    private:
        std::string m_id;
        timeval_t m_starttv;
        timeval_t m_endtv;
        DataMode m_dataMode;
        Verbosity m_verbose;
        std::string m_dbprofile;
        boost::optional<UseSplitCallback> m_useSplitCallback;
        ByteSourceFactoryPtr m_byteSourceFactory;
        FDSetPtr m_fd;
    };

    static ClientContextPtr create( const EnvironmentPtr& env, const Config& );
    static ClientContextPtr create( const luabind::object& );

#endif // defined( SWIG )

    static ClientContextPtr create(
        const EnvironmentPtr& env,
        bool live,
        const timeval_t& starttv,
        const timeval_t& endtv,
        const std::string& id,
        int verbose = 0 );
};

typedef ClientContextFactory CCFactory;


///////////////////////////////////////////////////////////////////////////////////////////////////
//
// Inline implementation
//
///////////////////////////////////////////////////////////////////////////////////////////////////
inline bool                ClientContext::isLive() const                 { return false; }
inline void                ClientContext::run()                          { getEventDistributor()->run(); }
inline void                ClientContext::startDispatch()                { getEventDistributor()->startDispatch(); }
inline void                ClientContext::endDispatch()                  { getEventDistributor()->endDispatch(); }
inline int                 ClientContext::getVerbose()      const        { return m_verbose; }
inline timeval_t           ClientContext::getStartTimeval() const        { return m_starttv; }
inline timeval_t           ClientContext::getEndTimeval()   const        { return m_endtv; }
inline const std::string&  ClientContext::getIDString()     const        { return m_idString; }
inline AlertPtr            ClientContext::getAlert()                     { return m_spAlert; }
inline EventDistributorPtr const & ClientContext::getEventDistributor() const    { return m_spED; }
inline ClockMonitorPtr     ClientContext::getClockMonitor() const        { return m_spCM; }
inline IClientTimerPtr     ClientContext::getClientTimer()               { return m_spClientTimer; }
inline RandomSourcePtr     ClientContext::getRandomSource()              { return m_spRandomSource; }
inline const std::string&  ClientContext::getDBProfileString() const     { return m_dbProfileString; }
inline void ClientContext::sigAction(int sig, const UnixSigCallback &cb) { getMStreamManager()->sigAction(sig, cb); }

inline IMStreamManagerPtr    LiveClientContext::getMStreamManager()      { return m_spMStreamMgr; }
inline IMStreamManagerPtr    HistClientContext::getMStreamManager()      { return m_spMStreamMgr; }
inline LiveMStreamManagerPtr LiveClientContext::getLiveMStreamManager()  { return m_spMStreamMgr; }
inline HistMStreamManagerPtr HistClientContext::getHistMStreamManager()  { return m_spMStreamMgr; }
inline timeval_t             LiveClientContext::getTime() const          { return timeval_t::now; }
inline bool                  LiveClientContext::isLive() const           { return true; }

} // namespace bb

#endif // BB_CLIENTCORE_CLIENTCONTEXT_H
