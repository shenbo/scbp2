#ifndef BB_CLIENTCORE_ARCABOOK_H
#define BB_CLIENTCORE_ARCABOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/consts/arca_consts.h>
#include <bb/clientcore/ArcaUtils.h>
#include <bb/core/hash_map.h>

#include <bb/clientcore/L2Book.h>
namespace bb {

class ClientContext;

/*****************************************************************************/
//
// ARCA Book
//
/*****************************************************************************/

class ArcaBook
    : public L2Book
{
public:
    ArcaBook( const ClientContextPtr& context, const instrument_t& instr, source_t src,
              SourceMonitorPtr sm, const char* desc, L2BookConfig config = L2BookConfig() );
    virtual ~ArcaBook();

    virtual void flushBook();
    virtual void dropOrder( BookOrder* o );

    void createOrder( const timeval_t& _tv_msg, const timeval_t& _tv_exch,
                      uint64_t _idnum, side_t _side, double _px, int _sz, uint8_t priceScale );

    /// ClockListener interface
    virtual void onWakeupCall( const timeval_t &ctv, const timeval_t &swtv, int reason, void* pData );

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    typedef bbext::hash_map<uint32_t, BookOrder*> bo_map;

    virtual bool handleCrossedOrder( BookOrder* o );
    virtual std::pair<long, long> getBookCheckOffset();
    void dropOrder( bo_map::iterator mel );
    PriceScaler<ArcaPriceScalerTraits> m_price_converter;

    bo_map  arca_orders;
    EventSubPtr m_eventSub;
    uint32_t m_expectedSeqNum;
    uint32_t m_droppedMessagesCount;
};


} // bb

#endif // BB_CLIENTCORE_ARCABOOK_H
