#ifndef BB_CLIENTCORE_LIFFEBOOK_H
#define BB_CLIENTCORE_LIFFEBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/consts/liffe_consts.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>

namespace bb {


/*****************************************************************************/
//
// LIFFE Book
//
// LiffeBooks are aggregate, meaning that each level bundles all the
// orders together.
//
/*****************************************************************************/

class LiffeBook
    : public BookImpl
    , private IEventDistListener
    , private SourceMonitorListener
{
public:
    LiffeBook( const instrument_t& instr, ClientContextPtr spCC, source_t src, mktdest_t mkt,
               const std::string& strDesc, SourceMonitorPtr spSMon );
    virtual ~LiffeBook();

    /// Returns the LIFFE market mode.
    liffe::market_mode_t getMarketMode() const    { return m_market_mode; }

    /// Returns the true if the exchange considers the instrument available.
    bool isAvailable() const                      { return m_available; }

    /// Returns the last sequence number issued by liffeqd (or cbotqd).
    uint32_t getLastSeqnum() const                  { return m_last_msg_seqnum; }

// IBook interface
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    /// Note that empty levels above the "bottom" level are counted.
    virtual size_t getNumLevels( side_t side ) const;

private:
    static const int  NO_SEQNUM   = -1;

    /// EventListener interface
    virtual void onEvent( const Msg& msg );

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );

    /// Flushes one side of the book.
    /// Does NOT call listeners.
    void flushSide( side_t side );

    /// Finds the specified order in the book and updates its value.
    /// If size <= 0, then it is removed from the book.
    /// If it is not already in the book, then it is added.
    /// Returns true if the book is modified, false otherwise.
    /// Notifies listeners properly.
    bool applyOrderToBook( const Msg* pMsg,
                           side_t side, const timeval_t& tv_msg, const timeval_t& tv_exch,
                           double px, int32_t sz, uint32_t seqnum );

protected:
    typedef std::list<BookLevelPtr>   BookLevelList;

    ClientContextPtr     m_spCC;
    SourceMonitorPtr     m_spSMon;
    uint32_t             m_verbose;
    uint32_t             m_eventCount;

    uint32_t             m_next_bo_seqnum;
    uint32_t             m_last_msg_seqnum;

    bool                 m_batch_modify;
    mutable bool         m_ok, m_ok_dirty;
    bool                 m_source_ok;

    mktdest_t            m_mkt;
    liffe::market_mode_t m_market_mode;
    bool                 m_available;

    // book levels are kept in a std::vector.  most Liffe books are at most 10 levels deep
    // an empty level has a null BookLevelPtr.  level 0 is top of book
    BookLevelList        m_levels[2];    // BID = 0, ASK = 1
    EventSubPtr          m_eventSub, m_xchgEventSub;
};


} // namespace bb


#endif // BB_CLIENTCORE_LIFFEBOOK_H
