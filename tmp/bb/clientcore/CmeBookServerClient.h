#ifndef BB_CLIENTCORE_CMEBOOKSERVERCLIENT_H
#define BB_CLIENTCORE_CMEBOOKSERVERCLIENT_H

#include <map>

#include <bb/core/instrument.h>
#include <bb/core/MStreamCallback.h>
#include <bb/clientcore/IEventDistListener.h>

#include <bb/clientcore/CmeBookServerConfig.h>
/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

namespace bb {

class sockaddr_ipv4_t;
class FDSet;

BB_FWD_DECLARE_SHARED_PTR( LiveClientContext );
BB_FWD_DECLARE_SHARED_PTR( TCPSocket );
BB_FWD_DECLARE_SHARED_PTR( ISendTransport );
BB_FWD_DECLARE_SHARED_PTR( IRecvTransport );

class CmeBook;

}

namespace cme {



BB_FWD_DECLARE_SCOPED_PTR( RecoveryManager );

class BookServerClient : public bb::IMStreamCallback
{
public:
    BookServerClient( bb::LiveClientContextPtr& pContext,
                      const CmeBookServerConfig& conf,
                      bb::CmeBook& target );

    virtual ~BookServerClient();

    virtual void onMessage( const bb::Msg& msg );

    bool sendRequest();

private:
    bb::FDSet& getReactor() const;

    void scheduleNextHostAttempt();

    void onConnect( const bb::TCPSocketPtr& pSock, int error );

    void onDNSResolved( const bb::sockaddr_ipv4_t& addr );
    void onDNSError();

    bool onCommError( const std::exception& ex );
    void onStreamEnd();

    void onRecoverySuccess();
    void onRecoveryFailure();

    void recoveryCleanup();

private:
    bb::LiveClientContextPtr m_pContext;
    const CmeBookServerConfig m_conf;

    RecoveryManagerScopedPtr m_recoveryManager;

    bb::Subscription m_connectSub; // Used for both the connection as well as DNS lookup;
    bb::Subscription m_eventSub;

    bb::TCPSocketPtr m_socket;
    bb::ISendTransportPtr m_sender;
    bb::IRecvTransportPtr m_recv;

    std::vector<std::string> m_hosts;

    bool m_verbose;

};

}

#endif // BB_CLIENTCORE_CMEBOOKSERVERCLIENT_H
