#ifndef BB_CLIENTCORE_MESSAGESTATCOLLECTOR_H
#define BB_CLIENTCORE_MESSAGESTATCOLLECTOR_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/core/CompactMsg.h>

#include <bb/clientcore/ClientContext.h>
namespace bb{

BB_FWD_DECLARE_SHARED_PTR( ISendTransport );
BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );

class MessageStatCollector
    : public EventDistStateListener
{
public:
    MessageStatCollector( const ClientContext& cc, ISendTransportPtr transport );
    virtual bool onEventDistributorStartProcessing( const Msg& msg );
    virtual bool onEventDistributorFinishProcessing( const Msg& msg );


protected:
    EventDistributorPtr            m_spED;
    ClockMonitorPtr                m_spClockMon;
    CompactMsg<StatCollectorMsg>   m_statMsg;
    ISendTransportPtr              m_transport;
    uint32_t                       m_msgCount;
};

} // end namespace bb

#endif // BB_CLIENTCORE_MESSAGESTATCOLLECTOR_H
