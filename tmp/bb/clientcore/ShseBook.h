#ifndef BB_CLIENTCORE_SHSEBOOK_H
#define BB_CLIENTCORE_SHSEBOOK_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <set>

#include <bb/core/messages.h>
#include <bb/core/protobuf/ProtoBufMsg.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/SourceBooks.h>

#include <boost/array.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );

// SHSE sends market data updates in both full snapshots and delta snapshots (specified in field image_status)
// The usual pattern is a full snapshot every 1 minute with delta snapshots in between
// Besides the normal price x size information it provides a wide array of other fields including the order queue for best bid/offer
// The order queue is limited to 50 elements therefore we cannot calculate the price level size from summing all orders

class ShseBook
    : public BookImpl
    , private IEventDistListener
    , private SourceMonitorListener
{
public:
    ShseBook( const instrument_t& instr, ClientContextPtr spCC, source_t src,
             const std::string& strDesc, SourceMonitorPtr spSMon );
    virtual ~ShseBook();

// IBook interface
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, ShseBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( side_t side ) const;
    boost::optional<double> getTurnover() const { return m_opTurnover; }
    boost::optional<int64_t> getTotalVolume() const { return m_opTotalVolume; }

    double getLimitUpPrice() const
    { return m_limitUpPrice ? m_limitUpPrice.get() : 0.0; }
    double getLimitDownPrice() const
    { return m_limitDownPrice ? m_limitDownPrice.get() : 0.0; }

private:

    typedef std::vector<BookLevelPtr>   BookLevelVec;
    typedef std::bitset<32> SeenLevelsSet;
    typedef std::set<long> PriceSet;

    /// EventListener interface
    virtual void onEvent( const Msg& msg );

    void onEvent( const ProtoBufMsgBase& msg );


    void onBadState();

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );

    template<side_t side>
    std::pair<size_t,bool> tryAddLevel( const double price, const int32_t size );

    template<side_t side>
    size_t tryDropLevelsByIndex( SeenLevelsSet& seen_levels );

    template<side_t side>
    size_t tryDropLevelsByPrice( PriceSet& marked_prices );

    template <side_t side>
    int32_t updateBookLevelsFullSnapshot( const ::google::protobuf::RepeatedPtrField< acr::ShseMarketData::PriceLevels >& shse_book_side );

    template <side_t side>
    int32_t updateBookLevelsDeltaSnapshot( const ::google::protobuf::RepeatedPtrField< acr::ShseMarketData::PriceLevels >& shse_book_side );

    long toLong( double price ) { return (long)(price * 1000); }  // transform price to avoid comparing doubles

    ClientContextPtr    m_spCC;
    SourceMonitorPtr    m_spSMon;
    uint32_t            m_verbose;

    mutable bool        m_ok;
    mutable bool        m_ok_dirty;
    bool                m_source_ok;

    bool                m_full_snapshot_received;
    BookLevelVec        m_bookLevels[2];    // BID = 0, ASK = 1

    EventSubPtr         m_eventSub;

    boost::optional<double>    m_opTurnover;
    boost::optional<int64_t>   m_opTotalVolume;

    bool m_inLimitDown;
    boost::optional<double>    m_limitDownPrice;
    bool m_inLimitUp;
    boost::optional<double>    m_limitUpPrice;

    const uint32_t m_kMktDataCompleteBodyTypeMask;
    typedef boost::unordered_map<uint32_t, acr::ShseMarketData> SplitSeq2MktDataCache;
    SplitSeq2MktDataCache m_splitSeq2MktDataMap;

};
BB_DECLARE_SHARED_PTR( ShseBook );

/// BookSpec corresponding to an Wind book
class ShseBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    ShseBookSpec() : SourceBookSpec() {}
    ShseBookSpec(const bb::ShseBookSpec&, const boost::optional<bb::InstrSubst>&);
    ShseBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    //virtual void checkValid() const;
    //virtual void hashCombine(size_t &result) const;
    //virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual ShseBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    //virtual void getDataRequirements(IDataRequirements *rqs) const;
};
BB_DECLARE_SHARED_PTR(ShseBookSpec);

}

#endif // BB_CLIENTCORE_SHSEBOOK_H
