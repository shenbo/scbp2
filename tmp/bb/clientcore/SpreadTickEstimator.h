#ifndef BB_CLIENTCORE_SPREADTICKESTIMATOR_H
#define BB_CLIENTCORE_SPREADTICKESTIMATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>
#include <bb/core/ListenerList.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/PriceProvider.h>

namespace bb
{

class SpreadTickEstimator
    : public IPriceProvider
    , protected bb::ITickListener
    , public bb::ListenerList< boost::function< void (SpreadTickEstimator*) > >
{
public:
    SpreadTickEstimator( bb::ITickProviderPtr tick_b, bb::ITickProviderPtr tick_a );
    SpreadTickEstimator( double mb, const bb::ITickProviderCPtr& tick_b
        , double ma, const bb::ITickProviderCPtr& tick_a );

    virtual ~SpreadTickEstimator();

    virtual void onTickReceived( const bb::ITickProvider* tp, const bb::TradeTick& tick );

    bool isLastTickOK() const
    {
        return m_spTickA->isLastTickOK() && m_spTickB->isLastTickOK();
    }

    double getEstimate() const;

    // IPriceProvider interface
    virtual double getRefPrice( bool* pSuccess = NULL ) const;

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const { return isLastTickOK(); }

    /// Returns the instrument_t associated with this PriceProvider
    virtual instrument_t getInstrument() const { return m_spTickA->getInstrument(); }

    /// Returns the timeval at which this PriceProvider last changed.
    virtual timeval_t getLastChangeTime() const;

    /// Adds a listener to be notified whenever the price might have changed
    /// Subscription is an output parameter. When the returned Subscription
    /// is released, the listener is unsubscribed.
    virtual void addPriceListener( Subscription& sub, const Listener& listener ) const
    {
        m_listeners.subscribe( sub, listener );
    }

protected:
    double m_multB;
    bb::ITickProviderCPtr m_spTickB;
    double m_multA;
    bb::ITickProviderCPtr m_spTickA;
    mutable bb::ListenerList<Listener> m_listeners;
    int32_t m_updateFlags;
};

BB_DECLARE_SHARED_PTR( SpreadTickEstimator );

} // namespace bb

#endif // BB_CLIENTCORE_SPREADTICKESTIMATOR_H
