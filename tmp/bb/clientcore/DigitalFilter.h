#ifndef BB_CLIENTCORE_DIGITALFILTER_H
#define BB_CLIENTCORE_DIGITALFILTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <numeric>
#include <boost/circular_buffer.hpp>

namespace bb {

/** standard n-sample digital filter
 * computes something like:
 * y = b[i] * x[n-i] + b[i+1] * x[n-i-1] + ... + b[n-1]*x[0]
 * 
 * @code
 *     vector<double> b = list_of(1)(1)(1);
 *     DigitalFilter f( b );
 *     f.push_back( 3 );
 *     f.push_back( 3 );
 *     f.push_back( 3 );
 *     
 *     double y = f.compute();
 * @endcode
 * 
 */
class DigitalFilter
{
    typedef boost::circular_buffer<double> cbuf;
public:
    DigitalFilter( const std::vector<double>& beta )
        : m_beta( beta )
        , m_sampleBuffer( beta.size() ) 
        , m_norm( std::accumulate( beta.begin(), beta.end(), 0.0 ) )
    {}

    double compute() const
    {
        double result = 0;
        int32_t i = 0;
        for( cbuf::const_reverse_iterator it = m_sampleBuffer.rbegin();
             it != m_sampleBuffer.rend(); ++it, ++i )
        {
            result += m_beta[i] * (*it);
        }
        return result / m_norm;
    }
    
    void push_back( double d ) { m_sampleBuffer.push_back( d ); }
    double back() const { return m_sampleBuffer.back(); }
    size_t size() const { return m_sampleBuffer.size(); }
    size_t max_size() const { return m_sampleBuffer.max_size(); }
    const std::vector<double>& getWeights() const { return m_beta; }
    double getNorm() const { return m_norm; }

    const cbuf& samples() const { return m_sampleBuffer; }

protected:
    std::vector<double> m_beta;
    cbuf m_sampleBuffer;
    double m_norm;
};

} // namespace bb

#endif // BB_CLIENTCORE_DIGITALFILTER_H
