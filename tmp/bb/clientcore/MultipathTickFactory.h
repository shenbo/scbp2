#ifndef BB_CLIENTCORE_MULTIPATHTICKFACTORY_H
#define BB_CLIENTCORE_MULTIPATHTICKFACTORY_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */


#include <set>
#include <bb/core/hash_map.h>
#include <bb/core/sourceset.h>
#include <bb/clientcore/MultipathTick.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( SourceTickFactory );

/** Creates MultipathTicks
 */
class MultipathTickFactory
{
public:
    /// Constructs a MultipathTickFactory for the given SourceTickFactory.
    MultipathTickFactory( SourceTickFactoryPtr const& src_factory );

    MultipathTickProviderPtr getMultipathTickProvider( const instrument_t& instr, const sourceset_t& srcs, bool create );

    MultipathTickProviderPtr getTickProvider( const instrument_t& instr, const source_t& src, bool create );

private:
    SourceTickFactoryPtr m_spSourceTickFactory;

    typedef bbext::hash_map<size_t, MultipathTickProviderPtr> Hash2MultipathTickMap;
    Hash2MultipathTickMap m_alreadyBuiltMultipathTicks;
};

BB_DECLARE_SHARED_PTR( MultipathTickFactory );

}

#endif // BB_CLIENTCORE_MULTIPATHTICKFACTORY_H
