#ifndef BB_CLIENTCORE_SMOOTHPRICEPROVIDER_H
#define BB_CLIENTCORE_SMOOTHPRICEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <deque>

#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( SmoothPriceProvider );

class SmoothPriceProvider
    : public PriceProviderImpl
{
public:
    SmoothPriceProvider(IPriceProviderCPtr underlying);

    virtual ~SmoothPriceProvider() {}

    /// Returns a "reference price", which generally means
    /// an as-current-as-possible price
    /// Returns 0.0 if no price is available, and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual double getRefPrice(bool* pSuccess = NULL) const { if (pSuccess) *pSuccess = isPriceOK(); return m_px; }

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const { return m_ok; }

    /// Returns the instrument_t associated with this PriceProvider
    virtual instrument_t getInstrument() const { return m_underlying->getInstrument(); }

    /// Returns the timeval at which this PriceProvider last changed.
    virtual timeval_t getLastChangeTime() const { return m_tv; }

private:
    void onPriceChanged(const IPriceProvider& sender);

    IPriceProviderCPtr m_underlying;
    Subscription m_sub;
    bool m_ok;
    double m_px;
    timeval_t m_tv;

    struct data_t {
        data_t(double px_, timeval_t tv_): px(px_), tv(tv_) {}
        double px;
        timeval_t tv;
    };

    typedef std::deque<data_t> history_t;
    history_t m_history;
};


/// PxPSpec for building a SmoothPxP
class SmoothPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SmoothPxPSpec() {}
    SmoothPxPSpec( const SmoothPxPSpec& a, const boost::optional<InstrSubst> &instrSubst );

    virtual instrument_t getInstrument() const { return m_underlying->getInstrument(); }
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual SmoothPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    IPxProviderSpecPtr m_underlying;
};
BB_DECLARE_SHARED_PTR( SmoothPxPSpec );


} // namespace bb

#endif // BB_CLIENTCORE_SMOOTHPRICEPROVIDER_H
