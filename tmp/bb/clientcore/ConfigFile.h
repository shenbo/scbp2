#ifndef BB_CLIENTCORE_CONFIGFILE_H
#define BB_CLIENTCORE_CONFIGFILE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <luabind/object.hpp>

#include <bb/core/instrument.h>
#include <bb/core/InstrSource.h>
#include <bb/clientcore/BookSpec.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(LuaState);

/// Each product in the stocks file contains an instrument and a lua factory class.
class ConfigFileProductEntry
{
public:
    ConfigFileProductEntry() {}
    ConfigFileProductEntry(const instrument_t &instr, const luabind::object &fact)
        : m_instrument(instr), m_factory(fact) {}
    virtual ~ConfigFileProductEntry() {}

    IBookSpecPtr getRefBookSpec() const;
    const instrument_t& getInstrument() const { return m_instrument; }

    instrument_t m_instrument;
    luabind::object m_factory;
};

/// This abstracts out reading and extracting information from the lua "stocks" file.
/// This only has the common ClientCore level information, you can subclass off this and
/// add in additional info of your own.
class ConfigFile
{
public:
    typedef ConfigFileProductEntry ProductEntry;
    BB_DECLARE_SHARED_PTR( ProductEntry );
    typedef std::vector<ProductEntryCPtr> ProductVec_t;

    ConfigFile(bool runLive, const date_t &startDate, const date_t &endDate, LuaStatePtr config);
    virtual ~ConfigFile() {}

    /// Loads the given stocks file. You should only do this once for each ConfigFile instance.
    virtual void load(const std::string& filename);
    virtual void loadSettings( const luabind::object& root );

    bool isLoaded() const { return m_fileLoaded; }

    virtual ProductEntryCPtr getTargetProduct() const { return ProductEntryCPtr(); }
    virtual const ProductVec_t& getSignalProducts() const { static ProductVec_t dummy; return dummy; }
    const std::vector<InstrSource> &getSmonBaselineData() const;

    source_t feedToSource(EFeedType feedtype) const;
    bool getRunLive() const { return m_runLive; }
    date_t getStartDate() const { return m_startDate; }
    date_t getEndDate() const { return m_endDate; }
    bool getUseSrcMonitors() const { return m_useSrcMonitors; }

    LuaState &getLuaConfig() { return *m_config; }

protected:
    /// parses the data for a product entry out of the lua table
    ProductEntryPtr productUnpack( const luabind::object& productTable, bool is_target );

    /// returns a newly allocated product entry object.
    virtual ProductEntryPtr createProductEntry( const instrument_t& instr, const luabind::object& root, bool is_target );

    bool m_runLive;
    date_t m_startDate, m_endDate;

    LuaStatePtr m_config;

    bool m_fileLoaded, m_useSrcMonitors;
    std::vector<InstrSource> m_smonBaselineData;
};
BB_DECLARE_SHARED_PTR(ConfigFile);


} // namespace bb

inline const std::vector<bb::InstrSource> &bb::ConfigFile::getSmonBaselineData() const
{
    if(!m_fileLoaded)
        throw std::runtime_error("bb::ConfigFile::getSmonBaselineData called before a file was loaded");
    return m_smonBaselineData;
}


#endif // BB_CLIENTCORE_CONFIGFILE_H
