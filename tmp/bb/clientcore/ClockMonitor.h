#ifndef BB_CLIENTCORE_CLOCKMONITOR_H
#define BB_CLIENTCORE_CLOCKMONITOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <list>
#include <stdexcept>
#include <string>
#include <vector>
#include <map>

#include <boost/function.hpp>
#include <boost/intrusive_ptr.hpp>

#include <bb/core/instrument.h>
#include <bb/core/timezone.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <bb/core/Priority.h>
#include <bb/core/Subscription.h>
#include <bb/core/TimeProvider.h>

#include <bb/clientcore/EventDist.h>
#include <bb/clientcore/IEventDistListener.h>
#include <bb/clientcore/ClockNotice.h>
#include <bb/clientcore/SessionParams.h>
#include <bb/clientcore/IClockListener.h>

namespace bb {

// forward declaration
BB_FWD_DECLARE_SHARED_PTR(IAlert);
class IClockListener;
struct LuaPrintSettings;
class timezone_t;


/**
   A key component of ClientCore, the ClockMonitor is the main timekeeper of
   the library.  It receives events from EventDistributor and uses their
   timestamps to maintain a base time for the system.

   ClockListeners can have "wakeup calls" scheduled with ClockMonitor. When a
   wakeup call's time has been reached, ClockMonitor will invoke the
   ClockListener's onWakeupCall method, passing along information attached to
   the wakeup call. Wakeup calls can be scheduled by the Listener for itself
   (this is the most typical case), but also other objects can schedule
   wakeup calls for a Listener (eg SimTrader schedules wakeup calls for
   OrderManager).

   Multiple identical wakeup calls are not allowed by default, but can be
   scheduled with an optional parameter. When identical wakeup calls are
   scheduled, they are called in a FIFO order when the wakeup time comes.

   Wakeup calls can be scheduled with a priority.  This has the same semantics as
   EventDistributor priorities, so use the PRIORITY_ constants.  The "HIGHEST"
   priority listeners are called first.

   ClockMonitor also verifies that the incoming message-based time frame is
   free from serious defects (time running massively backwards, etc.).

   A ClockMonitor is useless in isolation. It must be pumped with events.
   A common pattern is to subscribe to all events of an EventDistributor:

@code
      EventDistributorPtr spED( new EventDistributor() );
      ClockMonitorPtr spCM( new ClockMonitor( spED, 20040502 );
@endcode

   Another common pattern is to immediately schedule for a new day:
@code
      cm->scheduleClockNotice( this, 0, cm::ENDOFDAY );
@endcode

**/
class ClockMonitor
    : public ITimeProvider
    , public EventDistStateListener
{
public:
    /// Handler for a wakeup call.  Matches the signature:
    ///    void func( const timeval_t& ctv, const timeval_t& swtv )
    /// where:
    ///     ctv    "current timeval", according ClockMonitor's last message
    ///     swtv   "scheduled wakeup timeval", the time at which the client wished to be awaken
    typedef boost::function<void(const timeval_t&, const timeval_t&)> WakeupHandler;

public:
    /// Constructs a ClockMonitor starting at midnight of starttv.
    ClockMonitor( EventDistributorPtr spED
                  , const SessionParamsCPtr& session
                  , const timeval_t& starttv
                  , int32_t verbose = 0 );
    /// Destructor
    virtual ~ClockMonitor();

    /// Schedules a wakeup call for a user-defined reason.
    /// Note that handler is copied, so be weary of values (versus pointers or references) bound to it.
    ///
    /// The wakeupSub is a subscription object, for canceling the wakeup call.
    /// It becomes invalidated (i.e. isValid() returns false) after the wakeup fires.
    ///
    /// @param wakeupSub  will unsubscribe the wakeup call upon destruction
    /// @param handler    the WakeupHandler to invoke at wtv
    /// @param wtv        the time to be woken up
    /// @param priority   the priority of this handler
    void scheduleWakeupCall( Subscription &wakeupSub, const WakeupHandler& handler, const timeval_t& wtv, Priority priority );

// Classic interface
    /// Schedules a wakeup call for a user-defined reason.
    /// @param l    the IClockListener to wake up
    /// @param wtv  the time to be woken up
    /// @param reason the reason code for the wakeup.
    ///               Allows clients to match specific behavior to a wakeup call.
    ///               Can not be a cm::clock_notice reason.
    /// @param pData  user-assigned data for the wakeup
    /// @param priority  the priority of this listener
    /// @param allow_identical  Control whether multiple identical wakeup calls can be scheduled.
    ///                    Default is to not allow multiple identical calls. Wakeup calls
    ///                    scheduled for identical timevals will be called in a FIFO order,
    ///                    but all user-defined wakeup calls have priority over all clock
    ///                    notice calls with equal timevals.
    /// @return true if scheduled
    bool scheduleWakeupCall( IClockListener* pCL, const timeval_t& wtv, int32_t reason,
                             Priority priority, void* pData = NULL, bool allow_identical = false );

    /// Schedules a wakeup call. Only reserved clock_notices are allowed. Use scheduleWakeupCall
    /// for a wakeup call for a user-defined reason.
    /// @param l        the ClockListener to wake up
    /// @param wtv      the time to be woken up
    /// @param reason   the cm::clock_notice reason code for the wakeup.
    /// @param priority the priority of this listener
    /// @param pData    user-assigned data for the wakeup
    /// @param allow_identical  Control whether multiple identical wakeup calls can be scheduled.
    ///                    Default is to not allow multiple identical calls. Wakeup calls
    ///                    scheduled for identical timevals will be called in a FIFO order
    /// @return true if scheduled
    bool scheduleClockNotice( IClockListener* pCL, const timeval_t& wtv, int32_t reason,
                              Priority priority, void* pData = NULL, bool allow_identical = false );

    /// Schedules a wakeup call. Only reserved clock_notices are allowed. Use scheduleWakeupCall
    /// for a wakeup call for a user-defined reason.
    /// @param l    the ClockListener to wake up
    /// @param cn     the clock_notice to schedule
    /// @param priority  the priority of this listener
    /// @param pData  user-assigned data for the wakeup
    /// @param allow_identical  Control whether multiple identical wakeup calls can be scheduled.
    ///                    Default is to not allow multiple identical calls. Wakeup calls
    ///                    scheduled for identical timevals will be called in a FIFO order
    /// @return true if scheduled
    bool scheduleClockNotice( IClockListener* pCL, const cm::clock_notice& cn,
                              Priority priority, void* pData = NULL, bool allow_identical = false );

    /// Schedules a series of wakeup calls from a vector of clock notices.
    /// Schedules one wakeup call per entry in notices, associating the same pData
    /// with all notices.
    /// @param l       the ClockListener to wake up
    /// @param notices the vector of timeval, reason pairs to schedule for wakeups.
    /// @param pData   user-assigned data for the wakeup
    /// @param priority  the priority of this listener
    /// @param allow_identical  Control whether multiple identical wakeup calls can be scheduled.
    ///                    Default is to not allow multiple identical calls. Wakeup calls
    ///                    scheduled for identical timevals will be called in a FIFO order
    /// @return true if all are scheduled, false if any scheduling failed
    bool scheduleClockNotices( IClockListener* pCL, const std::vector<cm::clock_notice>& notices,
                               Priority priority, void* pData = NULL, bool allow_identical = false );

    /// Unschedules all wakeup calls for a listener, time, and reason.
    /// The reason can be a clock_notice or user-defined.
    /// @return True if the wakeup call was unscheduled.
    bool unscheduleWakeupCall( IClockListener* pCL, const timeval_t& wtv, int32_t reason );

    /// Unschedules all wakeup calls for a listener for any specified reason.
    void unscheduleWakeupCalls( IClockListener* pCL, int32_t reason);

    /// The EventDistributor will tell us when it starts and finishes processing a message.
    bool onEventDistributorStartProcessing( const Msg& msg );
    bool onEventDistributorFinishProcessing( const Msg& msg );

private:
    /// Unschedule all wakeup calls for a listener.
    /// Is being used by unscheduleUserWakeupCalls and unscheduleClockNotices
    inline bool unscheduleWakeupCallsWithoutReason ( IClockListener* pCL, bool user_defined );

public:
    /// Unschedules all user defined wakeup calls for a listener.
    /// (does not unschedule clock_notice wakeups, if any exist)
    void unscheduleUserWakeupCalls( IClockListener* pCL );

    /// Unschedules all clock_notice wakeups for a listener
    void unscheduleClockNotices( IClockListener* pCL );

    /// Unschedule all wakeup calls, both user defined and clock_notices
    void unscheduleAll( IClockListener* pCL );


    /// Sets alert object used for broadcasting alarms, sounding alarm msg.
    void setAlert( const IAlertPtr &alert );

    /// Returns the current time in string form.
    const std::string& getTimeStr() const;

    /// Returns the current time as a timeval
    /// If no valid message has been received yet, returns midnight of starttv.
    virtual timeval_t getTime() const   {
        return m_currtv;
    }

    /// Returns the current date as YYYYMMDD
    /// If no valid message has been received yet, returns 0.
    int32_t getYMDDate() const            {
        return m_currdate;
    }

    /// @return The timeval of midnight for the current day.
    /// If no valid message has been received yet, returns midnight of starttv.
    timeval_t getMidnighttv() const     {
        return m_midnight_tv;
    }

    /// Returns the time at which the next scheduled wakeup will be fired.
    /// Mostly useful for regression testing. Returns timeval_t() if none.
    timeval_t getNextWakeupTime() const;

    /// Returns the number of wakeups which triggered since the construction of the ClockMonitor.
    /// Wakeups whose wakeup time is crossed but are cancelled in that instant before
    /// their handler is called are counted.
    int64_t getNumEventsFiringWakeups() const {
        return m_num_wakeup_firings;
    }

    /// @return True if the passed reason is user-defined (== NOREASON or >= USER_REASON)
    static bool is_user_defined( const int32_t reason )
    {
        return (reason == cm::NOREASON || reason >= cm::USER_REASON);
    }

    /// @return True if the passed reason is a reserved clock_notice (!= NOREASON && < USER_REASON)
    static bool is_clock_notice( const int reason )
    {
        return (reason != cm::NOREASON && reason < cm::USER_REASON);
    }

    const SessionParams& getSessionParams() const;

    // Returns the number of timers that were triggered, upto this point, by the current message.
    uint16_t getTimerPopCount() const { return m_timerPopCnt;}
    // Returns the number of timers that have been added, upto this point, by the current message.
    uint16_t getTimerAddedCount() const { return m_timerAddedCnt;}

protected:
    // Start to schedule all work items that should've run by now
    void startProcessing( const Msg& msg );
    // Cleaning up afterwards
    void stopProcessing( const Msg& msg );

    /// Schedules wakeup calls without any restrictions on reason
    /// See specs on scheduleWakeupCall for details on scheduling order and parameters
    bool _scheduleWakeupCall( IClockListener* pCL, timeval_t wtv, int32_t reason,
                              Priority priority, void* pData, bool allow_identical);

    /// Does validity checking of the submitted time, relative to previous time.
    void checkTime(MsgHdr const& msgHdr);

protected:
    class WakeupSubImpl;
    /*****************************************************************************/
    //
    // ClockMonitor::WakeupCall definition
    //
    /*****************************************************************************/

    class WakeupCall : public EventDistributor::MsgWorker
    {
    public:
        WakeupCall( const ClockMonitor *cm, const WakeupHandler& wakeup_handler, const timeval_t& wakeup_tv, Priority priority )
            : EventDistributor::MsgWorker( priority, this )
            , m_clockMonitor(cm)
            , m_wakeup_handler( wakeup_handler )
            , m_wakeup_tv( wakeup_tv )
            , m_valid( true )
            , m_holder( NULL )
            , m_pCL( NULL )
            , m_reason( 0 )
        {}

        // Legacy support
        WakeupCall( const ClockMonitor *cm, IClockListener* pCL, const int32_t reason, void* pData,
                    const timeval_t& wakeup_tv, Priority priority )
            : EventDistributor::MsgWorker( priority, this )
            , m_clockMonitor(cm)
            , m_wakeup_tv( wakeup_tv )
            , m_valid( true )
            , m_holder( NULL )
            , m_pCL( pCL )
            , m_reason( reason )
            , m_pData( pData )
        {}

        virtual ~WakeupCall();

        bool isValid() const;
        void invalidate(); // Sets m_valid to false and notifies our WakeupSubImpl, if we've got one.

        const WakeupHandler& getWakeupHandler() const   {
            return m_wakeup_handler;
        }
        const timeval_t& getWakeupTime() const          {
            return m_wakeup_tv;
        }

        bool isLegacy() const                           {
            return m_pCL != NULL;
        }
        IClockListener* getClockListener() const        {
            return m_pCL;
        }
        int32_t getReason() const                         {
            return m_reason;
        }
        void* getData() const                           {
            return m_pData;
        }
        void setHolder(WakeupSubImpl *holder)           {
            BB_ASSERT(m_holder == NULL);
            m_holder = holder;
        }

        void onEvent( const Msg &msg )
        {
            if ( !m_valid )
                return;
            // note: we can add trace code here -- something like:
            // m_clockMonitor->trace(this)
            // so we can dynamically trace clock notice messages
            if ( isLegacy() )
                m_pCL->onWakeupCall( m_clockMonitor->m_currtv, m_wakeup_tv, m_reason, m_pData );
            else
                m_wakeup_handler( m_clockMonitor->m_currtv, m_wakeup_tv );
            invalidate();
        }

    private:
        const ClockMonitor *m_clockMonitor;
        WakeupHandler       m_wakeup_handler;
        timeval_t           m_wakeup_tv;
        bool                m_valid;
        WakeupSubImpl      *m_holder;

        // legacy support
        IClockListener*  m_pCL;
        uint32_t         m_reason;
        void*            m_pData;
    };
    typedef boost::intrusive_ptr<WakeupCall> WakeupCallPtr;
    typedef boost::intrusive_ptr<const WakeupCall> WakeupCallCPtr;

protected:
    timeval_t           m_midnight_tv;
    int32_t             m_currdate;
    int32_t             m_last_alert_sec;
    timeval_t           m_last_checktm_tv;
    source_t            m_last_checktm_src;
    uint32_t            m_last_checktm_sequence;

    timeval_t           m_currtv;       // most recently seen timeval, return value for getTime()
    mutable std::string m_timeStr;      // cached value of string representation of currtv
    mutable int64_t     m_timeStr_sec;  // seconds at which timeStr was last calculated

    EventDistributorPtr m_spED;
    EventSubPtr         m_spOnEventHighestSub, m_spOnEventLowestSub;

    IAlertPtr           m_alert;
    int32_t             m_verbose;
    uint16_t            m_timerPopCnt;
    uint16_t            m_timerAddedCnt;

    typedef std::vector<WakeupCallPtr>  WakeupCallVec;
    struct WakeupCallSetItem
    {
        WakeupCallSetItem ( WakeupCallPtr & pp ) : listener ( pp->getClockListener() ),
            reason ( pp->getReason() ),
            prio ( pp->getPriority() ),
            ptr ( pp ) {}
        const IClockListener * listener;
        const int32_t   reason;
        const Priority  prio;
        WakeupCallPtr   ptr;
    };
    struct sortByPriorityAndReason
    {
        bool operator() ( WakeupCallSetItem const & one, WakeupCallSetItem const & two ) const;
    };
    typedef std::multiset<WakeupCallSetItem, sortByPriorityAndReason>  WakeupCallSet;
    typedef std::map< bb::timeval_t, WakeupCallSet > WakeupCallMap;

    WakeupCallMap     m_wakeup_map;
    WakeupCallVec     m_pending_wakeups;
    int64_t           m_num_wakeup_firings;
    SessionParamsCPtr m_spSessionParams;
};

BB_DECLARE_SHARED_PTR( ClockMonitor );

// Legacy name
typedef Subscription WakeupSubPtr;

struct ReasonPrinter { //funky stuff needed to avoid RubySwig Landmines
    explicit ReasonPrinter(cm::reason_t c):m_val(c) {}
    explicit ReasonPrinter(int32_t c):m_val(static_cast<cm::reason_t >(c)) {}
private:
    cm::reason_t m_val;
    friend std::ostream& operator<<(std::ostream&,ReasonPrinter);
};



} // namespace bb


#endif // BB_CLIENTCORE_CLOCKMONITOR_H
