#ifndef BB_CLIENTCORE_ISPECIALORDERPOSTPROCESSOR_H
#define BB_CLIENTCORE_ISPECIALORDERPOSTPROCESSOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {

class TdOrderNewMsg;

class ISpecialOrderPostProcessor
{
public:
    virtual ~ISpecialOrderPostProcessor();

    virtual const TdOrderNewMsg& postProcessMessage( const TdOrderNewMsg& ) const = 0;
};

}

#endif // BB_CLIENTCORE_ISPECIALORDERPOSTPROCESSOR_H
