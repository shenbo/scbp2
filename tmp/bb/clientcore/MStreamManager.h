#ifndef BB_CLIENTCORE_MSTREAMMANAGER_H
#define BB_CLIENTCORE_MSTREAMMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/mtype.h>
#include <bb/core/Error.h>
#include <bb/core/ptime.h>
#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/UnixSig.h>
#include <bb/core/network.h>

namespace bb {

class IMStreamCallback;
class instrument_t;
class source_t;

/// A stream of messages, including automatic management of market data topic filtering.
///
/// This is a low-level abstraction -- consumers of messages should look at the
/// EventDistributor for message feed management.
class IMStreamManager
{
public:
    virtual ~IMStreamManager();

    /// Runs the main loop, passing all messages to the given IMStreamCallback.
    virtual void run(IMStreamCallback *c)                                         = 0;

    /// Registers all the streams but does not start the event loop. This assume that the
    /// event loop is being driven by some other entity
    virtual void startDispatch(IMStreamCallback *c) { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); };

    /// Removes all streams. This assume that the event loop is being driven by some other entity
    virtual void endDispatch()                      { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); };

    /// Cleanly exits the main loop prematurely. Message processing will stop
    /// after the current message.
    virtual void exit()                                                           = 0;

    /// Registers a function to be called any time a signal arrives. Signal handlers cannot be unregistered.
    virtual void sigAction(int sig, const UnixSigCallback &cb)                    = 0;

    // Feed management
    // Clients: don't use addData* directly -- use the EventDistributor instead.
    virtual void addFeed( const source_t& source ){}

    /// Adds in market data for the given source/instr.
    virtual void addDataByMTypeInstr( const source_t& source, mtype_t mtype, const instrument_t& instr ) = 0;

    /// Adds in market data for the given source/mtype.
    /// This should include all messages of the given mtype.
    virtual void addDataByMType( const source_t& source, mtype_t mtype )          = 0;

    /// Request a removal of market data for the given source/mtype/instr.
    /// Note that a call to removeDataByMTypeInstr should be made for each
    /// call to addDataByMTypeInstr.
    virtual void removeDataByMTypeInstr( const source_t& source, const mtype_t mtype, const instrument_t& instr ) {};

    /// Remove market data for the given source/mtype.
    /// Note that a call to removeDataByMType should be made for each
    /// call to addDataByMType.
    virtual void removeDataByMType( const source_t& source, const mtype_t mtype ) {};

    /// Return the NIC time for the last message received
    virtual const timeval_t& getLastMsgReceiveNicTimestamp() const { return timeval_t::earliest; };

    /// Returns the source address ( typically udp://<qd_server_host> ) of the last received message.
    virtual const bb::sockaddr_ipv4_t & getLastMsgSourceAddress() const { static bb::sockaddr_ipv4_t empty; return empty; };

    /// Sets the timeout that will be used on the next blocking operation
    /// (and all subsequent ones until the next setNextTimeout). If no data
    /// is received in this interval.
    virtual void setNextTimeout(const bb::ptime_duration_t &timeout) {};


};
BB_DECLARE_SHARED_PTR( IMStreamManager );

} // namespace bb

#endif // BB_CLIENTCORE_MSTREAMMANAGER_H
