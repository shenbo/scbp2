#ifndef BB_CLIENTCORE_DATACOLLECTORCONFIGFILE_H
#define BB_CLIENTCORE_DATACOLLECTORCONFIGFILE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/SymbolsConfigFile.h>

namespace bb {

///
/// This is a small extension to the config file, so that you can get a list of tick sources
/// and a list of all books for each product. It is used by secstat among other things.
///
/// This is how DataCollectorSetup figures out which books and ticks to listen to for a
/// given instrument: it's an extension to the ConfigFile, which requires 2 new methods
/// in your lua factory:
/// getAllTickSources, which should return a table of source_t, and
/// getAllBooks, which should return a table of {name, bookSpec} pairs, where name is a
///              descriptive name for the book.
/// These aren't defined in the default SignalsLib because we're lazy; ExampleSecStatLib.lua
/// does define them.
///
/// Reads the following:
/// symbol_config = {
///     target_product = { product, factory },
///     tick_size = 0.05 -- optional
/// }
///
class DataCollectorConfigFile : public bb::SymbolsConfigFile
{
public:
    typedef bb::SymbolsConfigFile Super;
    DataCollectorConfigFile(bool runLive, const bb::date_t &startDate, const bb::date_t &endDate);

    void getAllTickSources(std::vector<bb::source_t> *sources, const ProductEntry &prod);

    typedef std::vector<std::pair<std::string, bb::IBookSpecPtr> > SourceBooksTable_t;
    void getAllBooks(SourceBooksTable_t *books, const ProductEntry &prod);

    virtual void loadSettings( const luabind::object& root );

    // This is a temporary facility to get tick size information into a data collecting
    // application.  The correct solution would be to have the tick sizes in a database
    boost::optional<double> getTickSize() const { return m_tickSize; }

protected:
    // minimum price increment, if specified.
    boost::optional<double> m_tickSize;
};
BB_DECLARE_SHARED_PTR(DataCollectorConfigFile);

} // namespace bb

#endif // BB_CLIENTCORE_DATACOLLECTORCONFIGFILE_H
