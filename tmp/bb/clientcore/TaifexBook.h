#ifndef BB_CLIENTCORE_TAIFEXBOOK_H
#define BB_CLIENTCORE_TAIFEXBOOK_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <set>

#include <bb/core/messages.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/SourceBooks.h>
#include <bb/clientcore/SnapshotTcpClient.h>

#include <boost/array.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
class TaifexBookMsg;
struct order_depth_level;

class TaifexBook
    : public BookImpl
    , private IEventDistListener
    , private SourceMonitorListener
    , private SnapshotTcpClientListener
{
public:
    TaifexBook( const instrument_t& instr, ClientContextPtr spCC, source_t src,
             const std::string& strDesc, SourceMonitorPtr spSMon, bool requestSnapshot );
    virtual ~TaifexBook();

// IBook interface
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, WindBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( side_t side ) const;

private:
    typedef std::vector<BookLevelPtr>   BookLevelVec;
    typedef std::map<double,BookLevelPtr,bb::FPComp> BookLevelMap;

    // SnapshotTcpClientListener interface
    virtual void onSnapshot( const BookSnapshotMsg& msg );

    /// EventListener interface
    virtual void onEvent( const Msg& msg );

    void onEvent( const TaifexBookMsg& msg );

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );

    void updateBookLevels(
        side_t side,
        const TaifexBookMsg& msg,
        std::set<int32_t>& levelsChanged );

    bool isBookLevelChanged( const BookLevelPtr& bookLevel, const order_depth_level& newLevel );


    ClientContextPtr    m_spCC;
    SourceMonitorPtr    m_spSMon;
    uint32_t            m_verbose;

    mutable bool        m_ok;
    mutable bool        m_ok_dirty;
    bool                m_source_ok;

    BookLevelVec        m_bookLevels[2];    // BID = 0, ASK = 1
    BookLevelMap        m_bookLevelsMap[2];

    EventSubPtr         m_eventSub;

    size_t m_maxNumLevels;

    static const size_t STOCK_MAX_LEVEL = 5;

    order_depth_level msg_taifex_book::*m_orderDepthOffsets[2][STOCK_MAX_LEVEL];

    boost::optional<double>    m_opTurnover;
    boost::optional<int32_t>   m_opTotalVolume;

    SnapshotTcpClientPtr m_tcpClient;
};

BB_DECLARE_SHARED_PTR( TaifexBook );

/// BookSpec corresponding to a Taifex book
class TaifexBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    TaifexBookSpec() : SourceBookSpec() {}
    TaifexBookSpec(const bb::TaifexBookSpec&, const boost::optional<bb::InstrSubst>&);
    TaifexBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual TaifexBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
};
BB_DECLARE_SHARED_PTR(TaifexBookSpec);

}

#endif // BB_CLIENTCORE_TAIFEXBOOK_H
