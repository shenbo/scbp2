#ifndef BB_CLIENTCORE_HKFEPRSBOOK_H
#define BB_CLIENTCORE_HKFEPRSBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <map>

#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>

#include <boost/array.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(ClientContext);
BB_FWD_DECLARE_SHARED_PTR(SourceMonitor);

class HkfeQuoteMsg;
class HkfeTradeInfoMsg;

class HkfePrsBookSnapshot {
public:
    double m_price[5];
    int m_size[5];

    HkfePrsBookSnapshot(double depth0, double depth1, double depth2, double depth3, double depth4,
                        int size0, int size1, int size2, int size3, int size4);

    HkfePrsBookSnapshot(const HkfeQuoteMsg& msg);

    bool hasNewLevel(HkfePrsBookSnapshot& prevSnapshot);

    /* Return the number of levels; stops at first zero size it sees */
    int getDepth();
};

class HkfePrsBook
    : public BookImpl
    , private IEventDistListener
    , private SourceMonitorListener
{
public:
    enum EView { QUOTE_ONLY = 0, QUOTE_AND_TICK = 1 };

    HkfePrsBook(const instrument_t& instr, ClientContextPtr spCC, source_t src,
                const std::string& strDesc, SourceMonitorPtr spSMon, EView view = QUOTE_ONLY);
    virtual ~HkfePrsBook();

    EView getView() const { return m_view; }
    void setView( EView view ) { m_view = view; }

// IBook interface
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, OrcBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide(size_t depth, side_t side) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter(side_t side) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels(side_t side) const;

private:

    typedef std::vector<BookLevelPtr>   BookLevelVec;

    /// EventListener interface
    virtual void onEvent(const Msg& msg);

    void onEvent(const HkfeQuoteMsg& msg);
    void onEvent(const HkfeTradeInfoMsg& msg);

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );

private:
    void insert( side_t s, int32_t index, double px, int32_t sz );
    void modify( const BookLevelPtr& p, int32_t sz );
    void drop( side_t s, int32_t level );

protected:
    ClientContextPtr    m_spCC;
    SourceMonitorPtr    m_spSMon;
    uint32_t            m_verbose;
    bool                m_source_ok;
    BookLevelVec        m_bookLevels[2];    // BID = 0, ASK = 1
    EventSubPtr         m_eventSub;
    EView               m_view;
};

BB_DECLARE_SHARED_PTR(HkfePrsBook);

} // namespace bb

#endif // BB_CLIENTCORE_HKFEPRSBOOK_H
