#ifndef BB_CLIENTCORE_ITRADEDEMONCLIENT_H
#define BB_CLIENTCORE_ITRADEDEMONCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hc.h>
#include <bb/core/side.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/tif.h>

#include <bb/clientcore/IPositionBroadcastRequester.h>

namespace bb
{

class ISpecialOrderPostProcessor;

/**
   So, this is the brand new scheme for viewing orders and their
   locations on the way to fill heaven (or cancel hell as it may
   be).  There are two types of "orders".  Plain orders, and
   cancels.  They both have a status, ie a location corresponding
   to where they are on the path.

   new - brand spankin' new and might not have reached the trade server quite yet
   transit - on the way from the trade server to market.  Most likely on Andover's machine
   open - sitting on the market, available for a fill
   done - off the market, whether from a fill or a cancel.

   Cancel orders have only new, transit, and done states.

   We also need to have a notion of a status change, which includes
   the old state, new state.

   Also, we need a fill message.
*/

/// Interface for interacting with Trade Demons.
class ITradeDemonClient : public IPositionBroadcastRequester
{
public:
    enum TransportType { NONE, TCP, MQUEUE, UNIX, SCTP, INPROCESS };

    virtual ~ITradeDemonClient() {}

    virtual TransportType getTransportType() const
    {
        return NONE;
    }

    /// Returns true if the ITradeDemonClient is connected.
    virtual bool is_valid() const = 0;

    // timeout/visible only apply to equity orders
    virtual hc_return_t send_order( acct_t acct
                                    , const instrument_t& instr
                                    , dir_t dir
                                    , mktdest_t dest
                                    , double px
                                    , unsigned int size
                                    , unsigned int orderid
                                    , int timeout
                                    , bool visible
                                    , tif_t tif
                                    , uint32_t oflags
                                    , timeval_t* send_time = NULL
                                    , const ISpecialOrderPostProcessor* specialOrder = NULL ) = 0;

    /// Send cancel to server
    virtual hc_return_t send_cancel( acct_t acct, const instrument_t& instr,
            unsigned int orderid, timeval_t* send_time = NULL ) = 0;

    virtual hc_return_t send_cancels_for_side( acct_t acct, const instrument_t& instr,
            side_t side, timeval_t* send_time = NULL ) = 0;

    // timeout/visible only apply to equity orders
    // TODO:: Can be made pure virtual once we have implemented the function in SimTraderDeamonClient
    virtual hc_return_t modify_order( acct_t acct
                                      , unsigned int orig_orderid
                                      , const instrument_t& instr
                                      , dir_t dir
                                      , mktdest_t dest
                                      , double px
                                      , unsigned int size
                                      , unsigned int orderid
                                      , int timeout
                                      , bool visible
                                      , tif_t tif
                                      , uint32_t oflags
                                      , timeval_t* send_time = NULL
                                      , const ISpecialOrderPostProcessor* specialOrder = NULL )
    {
        BB_THROW_ERROR( "ModifyOrders are not supported by this TradeDaemon" );
    }

    /// sends an identify message to the server
    /// returns true if successful
    virtual bool identify( const char* id, timeval_t* send_time = NULL ) = 0;

    /// Once connected, this will contain the fqdn of the server on the order side.
    std::string const & td_fqdn() const { return m_tdFqdn; }
protected:
    std::string m_tdFqdn;
};
BB_DECLARE_SHARED_PTR( ITradeDemonClient );

} // namespace bb


#endif // BB_CLIENTCORE_ITRADEDEMONCLIENT_H
