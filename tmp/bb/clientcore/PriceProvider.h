#ifndef BB_CLIENTCORE_PRICEPROVIDER_H
#define BB_CLIENTCORE_PRICEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/**
   @file PriceProvider.h

   PriceProvider interface and several implementations.

**/

#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/PriceSize.h>
#include <bb/core/ListenNotify.h>
#include <bb/core/Subscription.h>

namespace bb {

// Forward declarations
BB_FWD_DECLARE_SHARED_PTR( IPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( IPriceSizeProvider );
BB_FWD_DECLARE_SHARED_PTR( UTDFPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( ACRIndexPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
BB_FWD_DECLARE_SHARED_PTR( PriceProviderBuilder );


/// Interface for classes which provide prices for a given symbol.
class IPriceProvider
{
    BB_DECLARE_LISTENER( IPriceProvider );

public:
    virtual ~IPriceProvider() {}

    /// Returns a "reference price", which generally means
    /// an as-current-as-possible price
    /// Returns 0.0 if no price is available, and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual double getRefPrice(bool* pSuccess = NULL) const = 0;

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const = 0;

    /// Returns the instrument_t associated with this PriceProvider
    virtual instrument_t getInstrument() const = 0;

    /// Returns the timeval at which this PriceProvider last changed.
    virtual timeval_t getLastChangeTime() const = 0;

    /// Adds a listener to be notified whenever the price might have changed
    /// Subscription is an output parameter. When the returned Subscription
    /// is released, the listener is unsubscribed.
    virtual void addPriceListener( Subscription &sub, const Listener& listener ) const = 0;
};


/// Implements the common addListener support.
class PriceProviderImpl
    : public IPriceProvider
{
public:
    /// Adds a listener to be notified whenever the price might have changed
    /// Subscription will be initialized with a new subscription to the object.
    /// When the Subscription is released, the listener is unsubscribed.
    virtual void addPriceListener( Subscription &outListenerSub, const Listener& listener ) const
    {   m_notifier.subscribeListener( outListenerSub, listener ); }

protected:
    void notifyPriceChanged()
    {   m_notifier.notifyListeners(*this); }

    mutable TNotifier<IPriceProvider> m_notifier;
};


/// Interface for classes which provide prices/sizes.
class IPriceSizeProvider
{
    BB_DECLARE_LISTENER( IPriceSizeProvider );

public:
    virtual ~IPriceSizeProvider() {}

    /// Returns a "reference PriceSize", which generally means
    /// an as-current-as-possible price
    /// Returns PriceSize() if no price is available, and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual PriceSize getRefPriceSize(bool* pSuccess = NULL) const = 0;

    /// Returns true if getRefPriceSize would return a valid price if asked at current moment.
    virtual bool isPriceSizeOK() const = 0;

    /// Returns the instrument_t associated with this IPriceSizeProvider
    virtual instrument_t getInstrument() const = 0;

    /// Returns the timeval at which this PriceSizeProvider last changed.
    virtual timeval_t getLastChangeTime() const = 0;

    /// Adds a listener to be notified whenever the PriceSize might have changed
    /// Returns a Subscription.  This will be invalid if !listener.
    /// When the returned Subscription is released, the listener is unsubscribed.
    virtual void addPriceSizeListener( Subscription &outListenerSub, const Listener& listener ) const = 0;
};


/// Implements the common addListener support
class PriceSizeProviderImpl
    : public IPriceSizeProvider
{
public:
    /// Adds a listener to be notified whenever the price might have changed
    /// Returns a Subscription.  This will be invalid if !listener.
    /// When the returned Subscription is released, the listener is unsubscribed.
    virtual void addPriceSizeListener( Subscription &outListenerSub, const Listener& listener ) const
    {   m_notifier.subscribeListener( outListenerSub, listener ); }

protected:
    void notifyPriceSizeChanged()
    {   m_notifier.notifyListeners( *this ); }

    mutable TNotifier<IPriceSizeProvider> m_notifier;
};

} // namespace bb

#endif // BB_CLIENTCORE_PRICEPROVIDER_H
