#ifndef BB_CLIENTCORE_SPECIALORDERPOSTPROCESSORS_H
#define BB_CLIENTCORE_SPECIALORDERPOSTPROCESSORS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>

#include <bb/core/bbint.h>
#include <bb/core/messages.h>
#include <bb/clientcore/ISpecialOrderPostProcessor.h>


namespace bb {

BB_DECLARE_SCOPED_PTR(TdOrderReserveMsg);

class ReserveOrderPostProcessor : public ISpecialOrderPostProcessor
{
public:
    ReserveOrderPostProcessor( uint32_t max_floor );
    virtual ~ReserveOrderPostProcessor();

    virtual const TdOrderNewMsg& postProcessMessage( const TdOrderNewMsg& oldOrder) const;

private:
    ReserveOrderPostProcessor() {}

    TdOrderReserveMsgScopedPtr m_msg;
};



}


#endif // BB_CLIENTCORE_SPECIALORDERPOSTPROCESSORS_H
