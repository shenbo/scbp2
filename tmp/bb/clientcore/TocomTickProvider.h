#ifndef BB_CLIENTCORE_TOCOMTICKPROVIDER_H
#define BB_CLIENTCORE_TOCOMTICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <boost/optional.hpp>

#include <bb/core/messages.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/ListenerList.h>

#include <bb/clientcore/TickProvider.h>

namespace bb {

class TocomTradeMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

struct TocomOrdersInTick
{
    TocomOrdersInTick() : remainingQty( 0 ) {}

    void reset()
    {
        remainingQty = 0;

        bidOrders.clear();
        askOrders.clear();
    }

    struct Order
    {
        double px;
        int32_t qty;
        uint16_t block_sz;
        uint16_t order_type;
    };

    int32_t remainingQty;

    std::vector<Order> bidOrders;
    std::vector<Order> askOrders;
};

class ITocomTickWithOrdersListener
{
public:
    virtual ~ITocomTickWithOrdersListener() {}

    virtual void onTickWithOrders( const ITickProvider *tp, const TradeTick& tick,
                                   const TocomOrdersInTick& orders ) = 0;
};

class TocomTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an TocomTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    TocomTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                       const std::string& desc );

    /// Destructor.
    virtual ~TocomTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const { return m_bTickReceived; }

    void addTickWithOrdersListener( ITocomTickWithOrdersListener *l ) const
    { m_tickWithOrdersListeners.add( l ); }

    void removeTickWithOrdersListener( ITocomTickWithOrdersListener *l ) const
    { m_tickWithOrdersListeners.remove( l ); }

    const TocomOrdersInTick& getOrdersInTick() { return m_orders_in_tick; }

private:
    // Event handlers
    void onTocomTradeMsg( const TocomTradeMsg& msg );

    void notifyTickWithOrders();
    void addOrderToTick( const TocomTradeMsg& msg );

private:
    bool m_bInitialized;
    bool m_bTickReceived;
    MsgHandlerPtr m_subTocomTradeMsg;
    boost::optional<uint32_t> m_currentTradeSequence;

    mutable ListenerList<ITocomTickWithOrdersListener*> m_tickWithOrdersListeners;
    TocomOrdersInTick m_orders_in_tick;
};

BB_DECLARE_SHARED_PTR( TocomTickProvider );

}

#endif // BB_CLIENTCORE_TOCOMTICKPROVIDER_H
