#ifndef BB_CLIENTCORE_INDEXSPECIFICATION_H
#define BB_CLIENTCORE_INDEXSPECIFICATION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Error.h>
#include <iostream>
#include <vector>
#include <bb/core/date.h>
#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>



namespace bb{


class IndexComponent
{
public:
    IndexComponent(const instrument_t& instr, double weight)
        :m_instr(instr)
        ,m_weight(weight)
    {
    }

    const instrument_t& getInstr() const
    {
        return m_instr;
    }

    const double getWeight() const
    {
        return m_weight;
    }

private:
    instrument_t m_instr;
    double m_weight;
}; // class IndexComponent


class IIndexSpecification
{
public:
    typedef std::vector<IndexComponent>::iterator iterator;
    typedef std::vector<IndexComponent>::const_iterator const_iterator;

public:

   /// Destructor
    virtual ~IIndexSpecification() { }

    virtual const instrument_t& getIndexInstr() const = 0;
    virtual const date_t& getDate() const = 0;

    /// Returns an iterator to the beginning of the container
    virtual const_iterator begin() const = 0;

    /// Returns an iterator to the end of the container
    virtual const_iterator end() const = 0;


}; // class IIndexSpecification





class IndexSpecificationImp : public IIndexSpecification
{

public:
    typedef std::vector<IndexComponent>::iterator iterator;
    typedef std::vector<IndexComponent>::const_iterator const_iterator;

    IndexSpecificationImp(const instrument_t& index_instr,const bb::date_t date)
    :m_index_instr(index_instr)
    ,m_date(date)
    {
    }

    void add_component(instrument_t instr,double weight)
    {
        IndexComponent component_instr(instr,weight);
        m_components.push_back(component_instr);
    }


    IndexSpecificationImp(const instrument_t& index_instr,const bb::date_t date ,const std::vector<instrument_t>& members,const std::vector<double>& weights)
        :m_index_instr(index_instr)
        ,m_date(date)
    {
        if(members.size()!=weights.size())
            BB_THROW_ERROR_SS("Index Members size differs from Weights size ");

        //interate through components and add them to my IndexSpecification class

        for (size_t i=0 ; i!=members.size(); ++i)
        {
            add_component(members[i],weights[i]);
        }

    }

    const bb::instrument_t& getIndexInstr() const
    {
        return m_index_instr;
    }

    const date_t& getDate() const
    {
        return m_date;
    }

    const std::vector<IndexComponent>& getComponents() const
    {
        return m_components;
    }


    iterator begin()
    {
        return m_components.begin();
    }

    const_iterator begin() const
    {
        return m_components.begin();
    }

    iterator end()
    {
        return m_components.end();
    }

    const_iterator end() const
    {
        return m_components.end();
    }

    iterator findInstr( const instrument_t& instr )
    {
        for( iterator it = m_components.begin(), end = m_components.end(); it != end; ++it )
        {
            if(instr == it->getInstr() )
                return it;
        }
        return m_components.end();
    }

    const_iterator findInstr( const instrument_t& instr ) const
    {
        for( const_iterator it = m_components.begin(), end = m_components.end(); it != end; ++it )
        {
            if(instr == it->getInstr() )
                return it;
        }
        return m_components.end();
    }

protected:
    bb::instrument_t m_index_instr;
    bb::date_t m_date; //index components can change over time
    std::vector<IndexComponent> m_components;
}; // class IndexSpecificationImp




std::ostream& operator<<(std::ostream&, const IIndexSpecification&);


BB_DECLARE_SHARED_PTR(IIndexSpecification);


} // namespace bb

#endif // BB_CLIENTCORE_INDEXSPECIFICATION_H
