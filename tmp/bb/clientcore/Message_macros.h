#ifndef BB_CLIENTCORE_MESSAGE_MACROS_H
#define BB_CLIENTCORE_MESSAGE_MACROS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/MessageMacros.h>
#include <bb/core/ColorMacros.h>

#pragma message YELLOW( "\nYou are #including deprecated header <bb/clientcore/Message_macros.h>. Your code is fine, but please migrate to <bb/core/MessageMacros.h> as soon as possible.")

#endif // BB_CLIENTCORE_MESSAGE_MACROS_H
