#ifndef BB_CLIENTCORE_ICANDLESTICKSERIES_H
#define BB_CLIENTCORE_ICANDLESTICKSERIES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/InfoObjectHolder.h>

namespace bb {

class Candlestick : public InfoObjectHolder
{
public:
    Candlestick( const bb::timeval_t& ts, double open, double high, double low, double close )
        : m_timestamp( ts )
        , m_open( open )
        , m_high( high )
        , m_low( low )
        , m_close( close )
        , m_volume( 0 ) {}

    Candlestick( const bb::timeval_t& ts
        , double open, double high, double low, double close, size_t volume )
        : m_timestamp( ts )
        , m_open( open )
        , m_high( high )
        , m_low( low )
        , m_close( close )
        , m_volume( volume ) {}

    const bb::timeval_t& getTime() const { return m_timestamp; }
    double getClose() const { return m_close; }
    double getOpen() const { return m_open; }
    double getHigh() const { return m_high; }
    double getLow() const { return m_low; }
    size_t getVolume() const { return m_volume; }

    enum FIELD { OPEN = 1, HIGH, LOW, CLOSE, VOLUME };

protected:
    bb::timeval_t m_timestamp;
    double m_open;
    double m_high;
    double m_low;
    double m_close;
    size_t m_volume;
};

class ICandlestickSeries
{
protected:
    class iterator_imp
    {
    public:
        virtual ~iterator_imp() {}
        virtual Candlestick* get() = 0;
        virtual void next() = 0;
        virtual bool is_valid() = 0;
        virtual iterator_imp* clone() const = 0;
    };

    class citerator_imp
    {
    public:
        virtual ~citerator_imp() {}
        virtual const Candlestick* get() = 0;
        virtual void next() = 0;
        virtual bool is_valid() = 0;
        virtual citerator_imp* clone() const = 0;
    };

public:
    class iterator
    {
    public:
        iterator( iterator_imp* i ) : m_ptr( i ) {}
        iterator( const iterator& i ) : m_ptr( i.m_ptr->clone() ) {}

        Candlestick* operator->() { return m_ptr->get(); }
        Candlestick* operator->() const { return m_ptr->get(); }
        Candlestick& operator*() { return *(m_ptr->get()); }

        iterator& next() { m_ptr->next(); return *this; }
        bool isValid() const { return m_ptr && m_ptr->is_valid(); }
        iterator& operator=( const iterator& src )
        {
            m_ptr.reset( src.m_ptr->clone() );
            return *this;
        }
        iterator& operator++() { return next(); }

    protected:
        boost::scoped_ptr<iterator_imp> m_ptr;
    };

    class citerator
    {
    public:
        citerator( citerator_imp* i ) : m_ptr( i ) {}
        citerator( const citerator& i ) : m_ptr( i.m_ptr->clone() ) {}

        const Candlestick* operator->() { return m_ptr->get(); }
        const Candlestick* operator->() const { return m_ptr->get(); }
        const Candlestick& operator*() { return *(m_ptr->get()); }

        citerator& next() { m_ptr->next(); return *this; }
        bool isValid() const { return m_ptr && m_ptr->is_valid(); }
        citerator& operator=( const citerator& src )
        {
            m_ptr.reset( src.m_ptr->clone() );
            return *this;
        }
        citerator& operator++() { return next(); }

    protected:
        boost::scoped_ptr<citerator_imp> m_ptr;
    };
    typedef citerator const_iterator;
    typedef citerator const_reverse_iterator;

    virtual ~ICandlestickSeries() {}

    virtual iterator begin() { return iterator( 0 ); }
    virtual iterator end() { return iterator( 0 ); }
    virtual citerator rbegin() const { return citerator( 0 ); }
    virtual citerator rend() const { return citerator( 0 ); }
    virtual iterator rbegin() { return iterator( 0 ); }
    virtual iterator rend() { return iterator( 0 ); }

    virtual size_t size() const = 0;
    virtual void push_back( const Candlestick& c ) = 0;

    Candlestick& back() { return *rbegin(); }
};

BB_DECLARE_SHARED_PTR( ICandlestickSeries );

} // namespace bb

#endif // BB_CLIENTCORE_ICANDLESTICKSERIES_H
