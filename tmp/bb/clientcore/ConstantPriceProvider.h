#ifndef BB_CLIENTCORE_CONSTANTPRICEPROVIDER_H
#define BB_CLIENTCORE_CONSTANTPRICEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>

#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ConstantPriceProvider );


/********************************************************************/
//
// A PriceProvider that returns a manually set price
//
/********************************************************************/

class ConstantPriceProvider
    : public PriceProviderImpl
{
public:
    static ConstantPriceProviderPtr create( const instrument_t& instr, double px, const timeval_t& tv = timeval_t() );
    static ConstantPriceProviderPtr create( const instrument_t& instr, const timeval_t& tv = timeval_t()  );

    //Set the provider to a valid state
    ConstantPriceProvider( const instrument_t& instr, double px, const timeval_t& tv = timeval_t() );
    ConstantPriceProvider( const instrument_t& instr , const timeval_t& tv = timeval_t());

    /// Setting a price
    /// Notifies listeners.
    void setPrice( double px,  timeval_t const& tv = timeval_t() );
    //to invalidate the provider, call this
    void invalidate( timeval_t const& tv = timeval_t() );

    /// Returns true if the price is greater than or equal to zero.
    virtual bool isPriceOK() const;

    virtual double getRefPrice( bool* pSuccess = NULL ) const;

    virtual instrument_t getInstrument() const;
    virtual timeval_t    getLastChangeTime() const;

private:
    static double invalid();

    instrument_t    m_instr;
    double          m_px;
    timeval_t       m_lastChangeTime;
};
BB_DECLARE_SHARED_PTR( ConstantPriceProvider );

/// PxPSpec corresponding to a ConstantPriceProvider
class ConstantPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    ConstantPxPSpec()
        : m_px( 0 )
        , m_timeval()
    {}
    ConstantPxPSpec(const ConstantPxPSpec &a, const boost::optional<InstrSubst> &instrSubst);

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual ConstantPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const {}

    instrument_t    m_instrument;
    double          m_px;
    timeval_t       m_timeval;   // the starting last timeval
};
BB_DECLARE_SHARED_PTR( ConstantPxPSpec );


} // namespace bb

#endif // BB_CLIENTCORE_CONSTANTPRICEPROVIDER_H
