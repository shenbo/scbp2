#ifndef BB_CLIENTCORE_ACRINDEX_H
#define BB_CLIENTCORE_ACRINDEX_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/bind.hpp>
#include <boost/static_assert.hpp>

#include <bb/core/Error.h>
#include <bb/core/ListenNotify.h>
#include <bb/core/Scripting.h>

#include <bb/clientcore/IBook.h>
#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( IFilteredBookFactory );


/// Returns a price provider which will calculate the value of the ACRIndex from the underlyings.
/// The priceSpec should be a PxProvider with getInstrument() == the acrindex symbol you want.
/// This instrument will be looked up in the database, and a DotProductPxPSpec will be returned
/// with the appropriate weights.
///
/// Each priceSpec in the DotProductPxPSpec will be constructed by replacing
/// priceSpec->getInstrument() with the actual priceSpec.
IPxProviderSpecPtr getAcrIndexSpec( const timeval_t &tv, IPxProviderSpecCPtr const&priceSpec ,std::string const&dbprofile="production");

/// Returns a price provider which will calculate the value of the ACRIndex from the underlyings.
/// the acrindex symbol you want.
/// This instrument will be looked up in the database, and a DotProductPxPSpec will be returned
/// with the appropriate weights.
/// the prices will be all the prices found in DayInfo for Open, Close, AdjClose, etc.

enum AcrIndexDayInfoRequestType{

    AcrIndexDayInfo_open,AcrIndexDayInfo_close
};
IPxProviderSpecPtr getAcrIndexDayInfo( const timeval_t &tv,instrument_t const& inst, AcrIndexDayInfoRequestType
        ,std::string const&dbprofile="production");


/**
 * An BookBasedAcrIndex holds a set of components and their respective weights.
 * These components are IBookPtrs.
 *
 * People looking for a more flexible pricing approach should check out
 * getAcrIndexSpec/DotProductPxP.
 *
 * The reported price of a BookBasedAcrIndex is weighted sum of all its components' prices.
 * This is reported in various ways, according to different views of the books
 * (best bid, best ask, midpoint, pressure-weighted midpoint, volume-weighted midpoint).
 *
 * The BookBasedAcrIndex implementation itself is very simple, negotiating only
 * with interfaces.  The composition of an BookBasedAcrIndex is left to factories.
 *
 * TODO: ERROR HANDLING?
 * Implementation detail:  When a component is added, AcrIndex subscribes to
 * it and updates itself whenever the price update comes.
 **/
class BookBasedAcrIndex
    : protected IBookListener
{
    BB_DECLARE_LISTENER( BookBasedAcrIndex );
public:
    class Component {
    public:
        Component()
            : m_weight( 0 )
        {}
        Component( IBookCPtr spBook, double weight )
            : m_spBook( spBook ), m_weight( weight )
        {}

        IBookCPtr getBook() const       { return m_spBook; }
        double    getWeight() const     { return m_weight; }
        void      setWeight( double w ) { m_weight = w; }

        instrument_t getInstrument() const
        {
            if ( !m_spBook )
                return instrument_t::unknown();
            return m_spBook->getInstrument();
        }

        bool isEmpty() const { return !m_spBook && bb::EQ(m_weight,0); }

        bool operator<(Component const&r)const{
            return getInstrument()<r.getInstrument();
        }
        bool operator==(Component const&r)const{
            return getInstrument()==r.getInstrument();
        }
        bool operator==(instrument_t const&r)const{
            return getInstrument()==r;
        }
        friend std::ostream& operator<<(std::ostream &,Component const&);
    private:
        IBookCPtr   m_spBook;
        double      m_weight;
    };
    typedef std::vector<Component>        ComponentList;

public:
    /// Creates an BookBasedAcrIndex with the specified identifying instrument and the date.
    /// The BookBasedAcrIndex is constructed with no components. begins empty.
    BookBasedAcrIndex( const instrument_t& instr ,bb::date_t const&);

    /// Destructor
    virtual ~BookBasedAcrIndex();

    /// Adds a component to the BookBasedAcrIndex.
    /// If the given IBookr already exists, its weight is updated.
    /// Returns false if this weight update occurs (or invalid sp is passed), true otherwise.
    bool addComponent( IBookCPtr spBook, double weight );

    /// Removes a component to the BookBasedAcrIndex, by specifying the added IBook.
    /// Returns false if the removal fails (IBook not found).
    bool removeComponent( IBookCPtr spBook );

    /// Removes a component to the BookBasedAcrIndex, by specifying its instrument.
    /// Since components are added as IBook, it is possible to add multiple
    /// IBook with the same instrument.  In this case, the first one added is removed (FIFO).
    /// Returns the Component removed.  Returns an empty component if it is not found.
    Component removeComponent( const instrument_t& instr );

    /// Returns this BookBasedAcrIndex's list of components.
    const ComponentList& getComponents() const
    {   return m_comps; }

    /// Returns the index value, looking only at the best bid prices of its components.
    /// Returns NaN if the value cannot be calculated for some reason.
    double getBestBidPrice() const;
    /// Returns the index value, looking only at the best ask prices of its components.
    /// Returns NaN if the value cannot be calculated for some reason.
    double getBestAskPrice() const;
    /// Returns the index value, looking only at the simple midpoint prices of its components.
    /// Returns NaN if the value cannot be calculated for some reason.
    double getMidPrice() const;
    /// Returns true if getIndexPrice would return a valid price, if asked at current moment
    /// Note that having no components is considered a valid state; getIndexPrice returns 0 in this case.
    bool isOK() const;

    /// Returns instrument associated with this PriceProviderBasedAcrIndex.
    instrument_t getInstrument() const
    {   return m_instr; }

    /// Returns the last timeval that this AcrIndex changed.
    timeval_t getLastChangeTime() const
    {   return m_last_change_tv; }

    /// Adds the given BookBasedAcrIndex::Listener to this AcrIndex.
    /// Subscription will be initialized with a new subscription to the object.
    /// When the Subscription is released, the listener is unsubscribed.
    void subscribeListener( Subscription &outSub, const Listener& listener ) const
    {   m_notifier.subscribeListener( outSub, listener ); }

    /// Adds a constant cash amount to the total price
    /// intended to be set once upon construction
    /// no notifications are done on update
    double setCash(double const&c);
    double getCash()const;
    bb::date_t getTradeDate()const;
protected:
    /// updates all the index's internal values
    /// TODO: eventually we'll do smarter caching... for now it's just a dirty bit
    /// we need to look at performance and usage first
    void _update() const;

    /// Invoked when the subscribed Book changes.
    /// The levelChanged entries are negative if there is no change, or a 0-based depth.
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );
    /// Invoked when the subscribed Book is flushed.
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

private:
    instrument_t    m_instr;
    timeval_t       m_last_change_tv;

    mutable bool    m_ok;
    mutable bool    m_dirty;
    ComponentList   m_comps;

    mutable double  m_best_bid_px, m_best_ask_px;
    mutable double  m_mid_px;
    mutable  TNotifier<BookBasedAcrIndex> m_notifier;
    double m_cash;
    bb::date_t m_date;  //the date this is for
};

BB_DECLARE_SHARED_PTR( BookBasedAcrIndex );



// Adapts a BookBasedAcrIndex to a PriceProvider
class BookBasedAcrIndexPriceProviderAdapter
    : public PriceProviderImpl
{
public:
    BookBasedAcrIndexPriceProviderAdapter( BookBasedAcrIndexPtr const&spADX )
        : m_spADX( spADX )
    {
        if ( !m_spADX )
            BB_THROW_ERROR( "BookBasedAcrIndexPriceProviderAdapter: Bad ADX" );

        m_spADX->subscribeListener( m_spADXSub, boost::bind(
                &BookBasedAcrIndexPriceProviderAdapter::onIndexChanged, this) );
    }

    /// Returns a "reference price", which generally means
    /// an as-current-as-possible price
    /// Returns NaN if no price is available, and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual double getRefPrice(bool* pSuccess = NULL) const
    {
        if ( !m_spADX->isOK() ) {
            if ( pSuccess ) *pSuccess = false;
            return std::numeric_limits<double>::quiet_NaN();
        }
        double px = m_spADX->getMidPrice();
        if ( std::isnan( px ) ) {
            if ( pSuccess ) *pSuccess = false;
            return std::numeric_limits<double>::quiet_NaN();
        }
        if ( pSuccess ) *pSuccess = true;
        return px;
    }

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const
    {   return m_spADX->isOK(); }

    /// Returns the instrument_t associated with this PriceProvider
    virtual instrument_t getInstrument() const
    {   return m_spADX->getInstrument(); }

    /// Returns the timeval at which this PriceProvider last changed.
    virtual timeval_t getLastChangeTime() const
    {   return m_spADX->getLastChangeTime(); }

private:
    void onIndexChanged()
    {   notifyPriceChanged(); }

private:
    BookBasedAcrIndexPtr   m_spADX;
    Subscription           m_spADXSub;
};
BB_DECLARE_SHARED_PTR( BookBasedAcrIndexPriceProviderAdapter );


/// PxPSpec corresponding to a BookBasedAcrIndexPriceProviderAdapter
/// The supplied BookSpec is used as a prototype for all instruments.
class BookAcrIndexPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    BookAcrIndexPxPSpec();
    BookAcrIndexPxPSpec( const BookAcrIndexPxPSpec& other, const boost::optional<InstrSubst> &instrSubst,std::string const&_dbprofile="production" );

    virtual instrument_t getInstrument() const { return m_bookSpec->getInstrument(); }
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual BookAcrIndexPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    timeval_t       m_timeval;   // the time to start searching from
    IBookSpecCPtr   m_bookSpec;
    std::string     m_dbprofile;
};
BB_DECLARE_SHARED_PTR( BookAcrIndexPxPSpec );




} // namespace bb

#endif // BB_CLIENTCORE_ACRINDEX_H
