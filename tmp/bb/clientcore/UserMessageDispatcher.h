#ifndef BB_CLIENTCORE_USERMESSAGEDISPATCHER_H
#define BB_CLIENTCORE_USERMESSAGEDISPATCHER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>
#include <list>

#include <bb/core/acct.h>
#include <bb/core/TimeProvider.h>
#include <bb/clientcore/EventDist.h>
#include <bb/core/usermsg.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(MsgHandler);
class UserMessageMsg;

struct UMsgFilter
{
    UMsgFilter() {}

    UMsgFilter sym_equals(bb::symbol_t sym) const;
    // matches if umsg.symbol == sym || umsg.sym == SYM_ALL.
    UMsgFilter sym_matches(bb::symbol_t sym) const;
    UMsgFilter sym_is_all() const { return sym_equals(SYM_ALL); }
    UMsgFilter sym_is_not_all() const;
    UMsgFilter target(usermsg_target_t target) const;
    UMsgFilter command(usermsg_command_t cmd) const;

    bool matches(UserMessageMsg const &msg) const;

protected:
    enum MatchMode { MATCH_ALL, NOT_EQUALS, EQUALS };
    boost::optional<std::pair<symbol_t, MatchMode> > m_sym;
    boost::optional<usermsg_target_t> m_target;
    boost::optional<usermsg_command_t> m_command;
};

class UserMessageDispatcher
{
public:
    UserMessageDispatcher( const ITimeProviderCPtr& timeProv, EventDistributorPtr ed,
                           source_t umsgSource, acct_t acct, Priority priority, bool runLive = true);

    typedef boost::function<void (timeval_t const &ctv, symbol_t sym, const UserMessageMsg&)> Handler;

    // Calls handler on all usermessages that match the given filter
    void handle(UMsgFilter const &filter, Handler const &handler);

    // Handle the given user message. This is public primarily to make
    // it easier to test components that listen for user messages.
    void onUserMessage(const UserMessageMsg &user_msg);

protected:
    ITimeProviderCPtr m_timeProvider;
    bb::acct_t m_acct;
    typedef std::list<std::pair<UMsgFilter, Handler> > Handlers_t;
    Handlers_t m_handlers;
    MsgHandlerPtr m_umsgHandlerRef;
};
BB_DECLARE_SHARED_PTR(UserMessageDispatcher);

} // bb

#endif // BB_CLIENTCORE_USERMESSAGEDISPATCHER_H
