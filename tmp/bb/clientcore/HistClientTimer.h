#ifndef BB_CLIENTCORE_HISTCLIENTTIMER_H
#define BB_CLIENTCORE_HISTCLIENTTIMER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Dispose.h>
#include <bb/core/MStream.h>
#include <bb/core/Subscription.h>
#include <bb/core/timeval.h>

#include <bb/clientcore/EventDist.h> // for PRIORITY_CC_Misc (which probably should be in a separate file)
#include <bb/clientcore/IClientTimer.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( ITimeProvider );
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );
BB_FWD_DECLARE_SHARED_PTR( HistMStreamManager );
BB_FWD_DECLARE_SHARED_PTR( DFStreamMplex );
BB_FWD_DECLARE_SCOPED_PTR( TimeoutMsg );

class HistClientTimer
    : public IClientTimer
{
public:
    HistClientTimer( const HistMStreamManagerPtr& streamManager,
            const EventDistributorPtr& distributor, const ITimeProviderPtr& timeProvider );

private:
    virtual void scheduleImpl( Subscription& sub, const Callback& callback, const timeval_t& wtv );
    virtual void scheduleFromNowImpl( Subscription& sub, const Callback& callback, const ptime_duration_t& duration );
    virtual void schedulePeriodicImpl( Subscription& sub, const Callback& callback, const timeval_t& starttime, const timeval_t& endtime,
                                       const ptime_duration_t& interval, const Subscription::Callback& done_callback );

    HistMStreamManagerPtr                   m_streamManager;
    ITimeProviderPtr                        m_timeProvider;
    timeval_t                               m_starttv;
    timeval_t                               m_endtv;
    EventDistributorPtr                     m_evdist;
};
BB_DECLARE_SHARED_PTR( HistClientTimer );


// PeriodicTimerMStream
// This is what HistClientTimer uses, but other apps find it useful as well, esp when sampling historical data
struct PeriodicTimerMStream
    : HistMStream
    , protected IEventDistListener
    , DeadlyEmbraceManager<>
{
    //Behavior of how time is advanced
    // This is to make non uniform intervals easier to program
    struct IPeriodicTimerSpec
    {
        virtual timeval_t getTime() const = 0;
        virtual void advance() = 0;
        virtual bool expired() const = 0;
        virtual Priority getPriority() const = 0;
        virtual ~IPeriodicTimerSpec(){}
    };
    BB_DECLARE_SHARED_PTR( IPeriodicTimerSpec );

    //the default is equal spaced intervals from begin to end
    struct UniformPeriodicIntervals : IPeriodicTimerSpec
    {
        timeval_t getTime() const;
        void advance();
        bool expired() const;
        Priority getPriority() const;

        UniformPeriodicIntervals( const timeval_t& start, const timeval_t& end,
                const ptime_duration_t& interval, Priority = PRIORITY_CC_Misc );

    private:
        timeval_t        m_current;
        timeval_t        m_end;
        ptime_duration_t m_interval;
        Priority         m_priority;
    };
    BB_DECLARE_SHARED_PTR( UniformPeriodicIntervals );

    typedef boost::function<void()> Callback;

    virtual ~PeriodicTimerMStream();
    const Msg* next(); // override HistMStream
    void dispose(); // override IDispose
    PeriodicTimerMStream( const IPeriodicTimerSpecPtr&, const Callback&, const EventDistributorPtr&, const instrument_t& instr );

    // create will do all the work for you of tying all this together
    static void create( Subscription& sub, const Callback& callback, const instrument_t& subInstr, const IPeriodicTimerSpecPtr&,
            const DFStreamMplexPtr& dfstream, const EventDistributorPtr& evdist,
            const Subscription::Callback& done_callback = Subscription::Callback() );
private:
    void invalidate_i();
    void onEvent( const Msg& );

    IPeriodicTimerSpecPtr m_spec;
    const TimeoutMsgScopedPtr m_msg;
    Subscription m_eventSubs;
    Callback m_cb;
};
BB_DECLARE_SHARED_PTR( PeriodicTimerMStream );

} // namespace bb

#endif // BB_CLIENTCORE_HISTCLIENTTIMER_H
