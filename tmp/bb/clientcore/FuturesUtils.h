#ifndef BB_CLIENTCORE_FUTURESUTILS_H
#define BB_CLIENTCORE_FUTURESUTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>
#include <bb/core/Scripting.h>
#include <bb/db/DayInfo.h>

namespace bb
{

/*
 * FuturesStrip
 *
 *
 */
struct FuturesStrip{
    typedef DayInfoFuturesMap::cont_type cont_type;
    typedef cont_type::value_type value_type;
    typedef cont_type::const_reference  const_reference;
    typedef cont_type::const_iterator const_iterator;
    typedef cont_type::iterator iterator;
    typedef cont_type::size_type size_type;


    struct Options{
        Options& setMinVolume(unsigned);
         unsigned getMinVolume()const;
        Options&  setMaxEntries(unsigned);
        size_type  getMaxEntries()const;
        enum OrderingOption{CALENDAR,VOLUME};
        Options& setOrdering(OrderingOption);
        OrderingOption getOrdering()const;
        Options();
        friend std::ostream& operator<<(std::ostream&,Options const&);
        friend std::ostream& operator<<(std::ostream&,OrderingOption const&);
    private:
        unsigned m_vol;
        size_type m_size;
        OrderingOption m_opt;

    };
    FuturesStrip(bb::symbol_t const&,DayInfoFuturesMapCPtr const&,Options const&);
    bb::symbol_t            getSymbol() const;
    bb::date_t              getDate()   const;
    friend std::ostream&    operator<<(std::ostream&, FuturesStrip const&);

    const_iterator begin()const;
    const_iterator end()const;

    size_type size() const;
    bool empty() const;

    const_reference at(size_type i)const;
    Options getOptions()const;
    BB_DECLARE_SCRIPTING();
 private:
    bb::symbol_t m_symbol;
    DayInfoFuturesMapCPtr m_map;
    cont_type m_dif;
    Options m_opts;
};
BB_DECLARE_SHARED_PTR(FuturesStrip);

} // end namespace bb
#endif // BB_CLIENTCORE_FUTURESUTILS_H
