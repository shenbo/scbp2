#ifndef BB_CLIENTCORE_SOURCETRADINGDATE_H
#define BB_CLIENTCORE_SOURCETRADINGDATE_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include <string>
#include <map>
#include <vector>
#include <bb/core/gdate.h>
#include <bb/core/smart_ptr.h>
#include <bb/db/TradingCalendar.h>

namespace bb {

class SourceTradingDate
{
public:
    SourceTradingDate( const std::string& db_profile );

    CalendarCPtr getCalendar( const db::TradingCalendarFactory::Source& source );
    bool isTradingDate( symbol_t sym, const gdate_t& date );

    void addExcludeDate( gdate_t date ){ m_excludeDates.push_back( date ); } //todo: add sym/date pair support

protected:
    std::string m_dbProfile;
    std::vector<gdate_t> m_excludeDates;

    typedef std::map<db::TradingCalendarFactory::Source, CalendarCPtr> CalendarMap;
    mutable CalendarMap m_factories; //cache
};

BB_DECLARE_SHARED_PTR( SourceTradingDate );

} // namespace bb

#endif // BB_CLIENTCORE_SOURCETRADINGDATE_H
