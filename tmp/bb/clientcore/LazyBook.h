#ifndef BB_CLIENTCORE_LAZYBOOK_H
#define BB_CLIENTCORE_LAZYBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/MasterBook.h>
#include <boost/container/flat_map.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );

class LazyBook
    : public Book
    , public CompositeBook
{
public:
    LazyBook( const ClientContextPtr& cc, const instrument_t& instr, const char* desc, int _vbose );
    virtual ~LazyBook(){};

    /// Adds spSubBook to MB's list as the book for spSubBook->getSource()
    /// and stores spSMon as the SourceMonitor for spSubBook->getSource()
    virtual void addSubBook( IBookPtr spSubBook, SourceMonitorPtr spSMon );

    // return true if the book had to be updated.  False if no update was required
    bool updateBook( uint32_t levels );

    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Flushes the book. Listeners are notified with onBookFlush.  Otherwise, listeners are notified onBookChange.
    virtual void flushBook();

    virtual double getMidPrice() const;
    virtual bb::IBookLevelCIterPtr getBookLevelIter(bb::side_t) const;
    virtual size_t getNumLevels(bb::side_t) const;

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

    void checkIsOk();

protected:
    // BookListener interface
    /// Invoked when the subscribed Book is flushed.
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

    /// Invoked when the subscribed Book changes.
    /// The levelChanged entries are negative if there is no change, or a 0-based depth.
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );

    typedef boost::container::flat_map< double,
                                        BookLevel,
                                        BookLevelComparer<BID> >     BookLevelBidMap;
    typedef boost::container::flat_map< double,
                                        BookLevel,
                                        BookLevelComparer<ASK> >     BookLevelAskMap;



    BookLevelBidMap  m_bidMap;
    BookLevelAskMap  m_askMap;
    ClientContextPtr m_clientContext;
    uint32_t         m_lowestLevelUpdated[2];

    static const uint32_t         s_checkOkInterval = 20;

};
BB_DECLARE_SHARED_PTR( LazyBook );

} // namespace bb

#endif // BB_CLIENTCORE_LAZYBOOK_H
