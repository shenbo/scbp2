#ifndef BB_CLIENTCORE_TICKSTATS_H
#define BB_CLIENTCORE_TICKSTATS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/TickProvider.h>

namespace bb {

class TickStats
{
public:
    TickStats( const instrument_t& instr )
        : m_instr( instr )
        , m_total_volume( 0 )
        , m_total_hidden_volume( 0 )
        , m_total_extended_volume( 0 )
        , m_lastPrice( 0 )
    {}

    const instrument_t& getInstrument() const   { return m_instr; }
    uint32_t getTotalVolume() const               { return m_total_volume; }
    uint32_t getTotalHiddenVolume() const         { return m_total_hidden_volume; }
    uint32_t getTotalExtendedVolume() const       { return m_total_extended_volume; }
    double getLastPrice() const                 { return m_lastPrice; }
    double getHighPrice() const                 { return m_highPrice ? m_highPrice.get() : 0.0; }
    double getLowPrice() const                  { return m_lowPrice ? m_lowPrice.get() : 0.0; }
    double getOpenPrice() const                 { return m_openPrice ? m_openPrice.get() : 0.0; }
    double getClosePrice() const                { return m_closePrice ? m_closePrice.get() : 0.0; }

    void update( const ITickProvider& tp )
    {
        m_lastPrice = tp.getLastPrice();
        const TradeTick& tick = *tp.getLastTradeTick();

        if ( tick.getHours() != TradeTick::CROSS_TRADE )
        {
            if ( tick.getHours() != TradeTick::HOURS_EXTENDED )
            {
                if ( !std::isnan( tp.getHighPrice() ) && NEZ( tp.getHighPrice() ) )
                    m_highPrice = m_highPrice ? std::max( m_highPrice.get(), tp.getHighPrice() ) : tp.getHighPrice();
                if ( !std::isnan( tp.getLowPrice() ) && NEZ( tp.getLowPrice() ) )
                    m_lowPrice  = m_lowPrice ? std::min( m_lowPrice.get(),  tp.getLowPrice() ) : tp.getLowPrice();
            }

            if( !std::isnan( tp.getOpenPrice() ) && NEZ( tp.getOpenPrice() ) )
                m_openPrice = tp.getOpenPrice();
            if( !std::isnan( tp.getClosePrice() ) && NEZ( tp.getClosePrice() ) )
                m_closePrice = tp.getClosePrice();

            int32_t sz = tick.getSize();

            if( tp.getTotalVolume() != 0 )
                m_total_volume = tp.getTotalVolume();
            else if( tick.getHours() != TradeTick::HOURS_EXTENDED && tick.getHours() != TradeTick::CROSS_TRADE )
                m_total_volume += sz;

            if( tp.getTotalHiddenVolume() != 0 )
                m_total_hidden_volume = tp.getTotalHiddenVolume();
            else if( tick.isHidden() )
                m_total_hidden_volume += sz;

            if( tp.getTotalExtendedVolume() != 0 )
                m_total_extended_volume = tp.getTotalExtendedVolume();
            else
                m_total_extended_volume += sz;
        }
    }

private:
    instrument_t    m_instr;
    uint32_t        m_total_volume;
    uint32_t        m_total_hidden_volume;
    uint32_t        m_total_extended_volume;
    double          m_lastPrice;
    boost::optional<double> m_highPrice;
    boost::optional<double> m_lowPrice;
    boost::optional<double> m_openPrice;
    boost::optional<double> m_closePrice;
};

} // namespace bb

#endif // BB_CLIENTCORE_TICKSTATS_H
