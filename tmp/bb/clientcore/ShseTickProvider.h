#ifndef BB_CLIENTCORE_SHSETICKPROVIDER_H
#define BB_CLIENTCORE_SHSETICKPROVIDER_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/core/protobuf/ProtoBufMsg.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/MsgHandler.h>


namespace bb {

class ShseTickProvider : public TickProviderImpl
{
public:
    ShseTickProvider(ClientContext &ctx, const instrument_t &instr, source_t source, const std::string &desc);
    ~ShseTickProvider() {}

    virtual bool isLastTickOK() const { return m_bTickReceived; }

    uint64_t getNotionalTurnover() const { return m_notionalTurnover; }

private:
    void onTradeMsg( const ProtoBufMsgBase &msg );
    void onIndexMsg( const ShseIndexDataMsg &msg );

    bool m_bTickReceived;

    uint64_t m_notionalTurnover;

    MsgHandlerPtr m_subTradeMsg;
    MsgHandlerPtr m_subIndexMsg;
};

BB_DECLARE_SHARED_PTR(ShseTickProvider);

}

#endif // BB_CLIENTCORE_SHSETICKPROVIDER_H
