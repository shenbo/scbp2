#ifndef BB_CLIENTCORE_SOURCEMONITORPARAMINFOLOADER_H
#define BB_CLIENTCORE_SOURCEMONITORPARAMINFOLOADER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/LuaTableLoader.h>
#include <bb/clientcore/SourceMonitor.h>

namespace bb {

/** Loads parameters for configuring a SourceMonitor.
 *
 * Knows the following keys:
 * @arg @c min_interval_tv
 * @arg @c min_avg_interval_tv
 * @arg @c long_interval_mult
 * @arg @c max_interval_mul
 * @arg @c check_status_interval
 * @arg @c typical_lag
 * @arg @c check_seqnum
 * @arg @c low_precision
 *
 * Used like:
 * @code
 *
 * shared_ptr<SourceMonitor::ParamInfo> param( ... );
 * luabind::object o = DefaultCoreContext::getEnvironment()->luaState().root()["SourceMonitorConfig"];
 *
 * SourceMonitorParamInfoLoader::load( param.get(), lua_object );
 *
 * m_spClientContext->set( "SourceMonitor_SRC_ISLD", param );
 *
 * @endcode
 *
 */

class SourceMonitorParamInfoLoader
{
public:
    template <typename T> static void apply( T* dest, const luabind::object& config )
    {
        (*dest) = luabind::object_cast<T>( config );
    }

    template<typename T> static LuaTableLoader0::apply_func_t apply_func( T* dest )
    {
        return boost::bind( (void (*)( T*, const luabind::object&) ) &apply, dest, _1 );
    }

    static void load( SourceMonitor::ParamInfo* info, const luabind::table<> & obj )
    {
        const LuaTableLoader0::Entry p[] = {
            { "min_interval_tv", apply_func( &info->minInterval ) },
            { "min_avg_interval_tv", apply_func( &info->minAvgInterval ) },
            { "long_interval_mult", apply_func( &info->longIntervalMult ) },
            { "max_interval_mul", apply_func( &info->maxIntervalMult ) },
            { "check_status_interval", apply_func( &info->checkStatusInterval ) },
            { "typical_lag", apply_func( &info->typicalLag ) },
            { "check_seqnum", apply_func( &info->bCheckSeqNum ) },
            { "low_precision", apply_func( &info->bLowPrecision ) },
        };
        LuaTableLoader0 loader( p, DIM( p ) );
        loader.load( obj );
    }
};

template <> void SourceMonitorParamInfoLoader::apply<timeval_t>( timeval_t* dest, const luabind::object& config )
{
    (*dest) = timeval_t( luabind::object_cast<double>( config ) );
}

} // bb

#endif // BB_CLIENTCORE_SOURCEMONITORPARAMINFOLOADER_H
