#ifndef BB_CLIENTCORE_TDORDER_H
#define BB_CLIENTCORE_TDORDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/BookOrder.h>
#include <bb/core/acct.h>
#include <bb/core/oreason.h>
#include <bb/core/ostatus.h>
#include <bb/core/cxlstatus.h>

namespace bb {

/// A BookOrder with Trade Daemon information.
class TDOrder
    : public BookOrder
{
public:
    acct_t          account;
    uint32_t        orderid;
    ostatus_t       status;
    cxlstatus_t     cxlstatus;
    oreason_t       reason;
    timeval_t       timeout;
    bool            visible;
    uint32_t        fileid;

    virtual std::ostream& print( std::ostream &out ) const;
};
BB_DECLARE_SHARED_PTR( TDOrder );

} // namespace bb

#endif // BB_CLIENTCORE_TDORDER_H
