#ifndef BB_CLIENTCORE_MASTERBOOKLEVEL_H
#define BB_CLIENTCORE_MASTERBOOKLEVEL_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */
#include <bb/clientcore/BookLevel.h>
#include <bb/clientcore/MasterBookLevel.h>

namespace bb{

class MasterBookLevel
    : public BookLevel
{
public:
    MasterBookLevel();
    MasterBookLevel( source_t _src, side_t _side, double _px );


    /// adds the sub-books BookLevel pointer to this MasterBookLevel
    void updateMasterBookLevel( const BookLevel* bLevel, source_t src );

    /// Return the numbers of orders for the given source
    virtual int32_t getNumOrders( source_t src ) const;

    /// Call parent insertOrder to add order to book.
    /// Also updates the MasterBookLevel to have a pointer to the sub-book BookLevel
    virtual void insertOrder( BookOrder* bOrder );

    /// Call parent function to delete order from the book. The function is only called
    /// in MasterBook when the corresponding BookLevel in sub-book has been dropped by the
    /// So we remove sub-book's bookLevel pointer from m_bookLevelMap
    virtual bool removeAndDeleteOrder( BookOrder* bOrder );
private:
    typedef std::map<const source_t, const BookLevel*> SourceBookLevelMap;
    SourceBookLevelMap m_bookLevelMap;
};

typedef PreallocatedObjectFactory<MasterBookLevel>  MasterBookLevelFactory;
typedef boost::intrusive_ptr<MasterBookLevelFactory>   MasterBookLevelFactoryPtr;

} // namespace bb


#endif // BB_CLIENTCORE_MASTERBOOKLEVEL_H
