#ifndef BB_CLIENTCORE_GTASTOCKTICKPROVIDER_H
#define BB_CLIENTCORE_GTASTOCKTICKPROVIDER_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class GtaStockTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an GtaStockTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    GtaStockTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                          const std::string& desc );

    /// Destructor.
    virtual ~GtaStockTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const { return m_bTickReceived; }

private:
    // Event handlers
    void onGtaStockSnapshotMsg( const GtaStockSnapshotMsg& msg );
    void onGtaStockTransactionMsg( const GtaStockTransactionMsg& msg );
    void onGtaIndexSnapshotMsg( const GtaIndexSnapshotMsg& msg );

    void onStartOfDay( const timeval_t& ctv, const timeval_t& wtv );
    void scheduleNextStartOfDay( const timeval_t& now );

private:
    ClockMonitorPtr m_spClockMonitor;
    bool m_bInitialized;
    bool m_bTickReceived;
    uint32_t m_lastSerial;
    MsgHandlerPtr m_subGtaStockSnapshotMsg;
    MsgHandlerPtr m_subGtaStockTransactionMsg;
    MsgHandlerPtr m_subGtaIndexSnapshotMsg;
    Subscription m_subWakeup;
};

BB_DECLARE_SHARED_PTR( GtaStockTickProvider );

}

#endif // BB_CLIENTCORE_GTASTOCKTICKPROVIDER_H
