#ifndef BB_CLIENTCORE_CFFEXTICKPROVIDER_H
#define BB_CLIENTCORE_CFFEXTICKPROVIDER_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>

#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

class CffexQdMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class CffexTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an CffexTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    CffexTickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~CffexTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_bTickReceived; }

    double getNotionalTurnover() const { return m_notionalTurnover; }
    double getNotionalAvgPx() const { return m_notionalAvgPx; }
    boost::optional<double> getAvgPxInLastTick( double lot_size ) const;

private:
    // Event handlers
    void onCffexQdMsg( const CffexQdMsg& msg );

private:
    bool m_bInitialized;
    bool m_bTickReceived;
    MsgHandlerPtr m_subCffexQdMsg;

    double m_notionalTurnover;
    double m_notionalAvgPx;

    double m_lastNotionalTurnover;
    uint32_t m_lastTotalVolume;
};

BB_DECLARE_SHARED_PTR( CffexTickProvider );

}

#endif // BB_CLIENTCORE_CFFEXTICKPROVIDER_H
