#ifndef BB_CLIENTCORE_TICKSTATSSERVERCLIENT_H
#define BB_CLIENTCORE_TICKSTATSSERVERCLIENT_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

//#include <bb/core/Log.h>
#include <bb/core/instrument.h>
#include <bb/core/messages.h>
#include <bb/core/EventPublisher.h>
#include <bb/core/host_port_pair.h>

#include <bb/clientcore/ClientContext.h>

#include <bb/io/SendTransport.h>
#include <bb/io/feeds.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

using namespace std;

class ITickStatsServerListener : public bb::IEventSubscriber
{
public:
    virtual ~ITickStatsServerListener() {};
    virtual bool onTickStatisticsMsg( const HostPortPair& hpp, const instrument_t& instr, const TickStatisticsMsg& msg ){ return true; }
    virtual bool onTickStatsServerRequestEnd( const HostPortPair& hpp ){ return true; }
    virtual bool onTickStatsServerDisconnect( const HostPortPair& hpp ){ return true; }
};

typedef EventPublisher<ITickStatsServerListener> TickStatsServerPublisher;

struct TickStatisticsMsgNotifier : public TickStatsServerPublisher::INotifier
{
    TickStatisticsMsgNotifier( ITickStatsServerListener* callback )
        : TickStatsServerPublisher::INotifier ( callback ) { }

    bool notify( const HostPortPair& hpp, const instrument_t& instr, const TickStatisticsMsg& msg )
    {
        return !m_callback->onTickStatisticsMsg( hpp, instr, msg );
    }
};

struct TickStatsServerRequestEndNotifier : public TickStatsServerPublisher::INotifier
{
    TickStatsServerRequestEndNotifier( ITickStatsServerListener* callback )
        : TickStatsServerPublisher::INotifier ( callback ) { }

    bool notify( const HostPortPair& hpp )
    {
        return !m_callback->onTickStatsServerRequestEnd( hpp );
    }
};

struct TickStatsServerDisconnectNotifier : public TickStatsServerPublisher::INotifier
{
    TickStatsServerDisconnectNotifier( ITickStatsServerListener* callback )
        : TickStatsServerPublisher::INotifier ( callback ) { }

    bool notify( const HostPortPair& hpp )
    {
        return !m_callback->onTickStatsServerDisconnect( hpp );
    }
};

class TickStatsServerClient
{
public:
    virtual ~TickStatsServerClient(){}

    TickStatsServerClient( const EventDistributorPtr& ed, const LiveMStreamManagerPtr& lmsm, const HostPortPair& hpp );

    const HostPortPair& getHostPortPair(){ return m_hostPortPair; }

    void addTickStatsServerListener (ITickStatsServerListener* listener);
    void subscribe( const std::vector<instrument_t>& instrs );
    void request( uint32_t request_id );
    void request() { request( m_currentRequestId + 1 ); }
    void setRequestId( uint32_t request_id ){ m_currentRequestId = request_id; }

private:
    virtual void onTickStatisticsMsg( const TickStatisticsMsg& msg );
    virtual void onRequestEndMsg( const SpinServerStreamEndMsg& msg );

    void onConnectionEnd(const TCPSocketSendTransportPtr& send_transport,
                         const HostPortPair& tick_server);
    bool onConnectionError(const TCPSocketSendTransportPtr& send_transport,
                           const HostPortPair& tick_server, const std::exception& ex);

    /// Multiplexing incoming multicast data message streams
    HostPortPair                m_hostPortPair;
    EventDistributorPtr         m_spEventDist;

    TickStatsServerPublisher    m_publisher;
    TCPSocketSendTransportPtr   m_sendTransport;

    typedef std::map< bb::instrument_t, MsgHandlerPtr , bb::instrument_t::less_no_mkt_no_currency> MsgHandlerMap;
    MsgHandlerMap               m_TickStatsMsgHandlers;
    MsgHandlerPtr               m_spRequestEndMsgHandler;

    uint32_t m_currentRequestId;
    bool m_requestPending;
};
BB_DECLARE_SHARED_PTR( TickStatsServerClient );

} // namespace bb

#endif // BB_CLIENTCORE_TICKSTATSSERVERCLIENT_H
