#ifndef BB_CLIENTCORE_BOOKORDER_H
#define BB_CLIENTCORE_BOOKORDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/side.h>
#include <bb/core/source.h>
#include <bb/core/PreallocatedObjectFactory.h>

namespace bb {

class BookLevel;

/// Represents an order that resides in a Book
class BookOrder
    : public FactoryObject<BookOrder>
{
public:
    instrument_t instr;     /// instrument for the order
    timeval_t    tv_msg;    /// timeval of last change, posted by the quote daemon
    timeval_t    tv_exch;   /// timeval of last change, posted by the exchange
    double       px;        /// price of the order
    int32_t      sz;        /// size of the order
    side_t       side;      /// whether this is a BUY or SELL order
    uint64_t     idnum;     /// the ID of the order
    mktdest_t    mkt;       /// market destination of the order
    source_t     src;       /// source of the order
    BookLevel*   lvl;       /// book level to which this order belongs

    BookOrder();
    BookOrder( side_t _side, double _px, int32_t _sz, source_t _src );
    BookOrder( const instrument_t& _instr,
               const timeval_t& _tv_msg, const timeval_t& _tv_exch,
               uint64_t _idnum, side_t _side, double _px, int32_t _sz,
               mktdest_t _mkt, source_t _src );

    // copy constructor must be implemented because Factory objects
    // are non-copyable to ensure that any manually created objects
    // are initialized with the correct deallocator
    BookOrder( const BookOrder& o);
    BookOrder& operator= ( const BookOrder& o );

    bool isOK() const { return ( !std::isnan( px ) && sz > 0 ); }
    mktdest_t getMktDest() const { return mkt; }

    virtual ~BookOrder() {}
    /// reset keeps the side information and sets other fields to 0
    virtual void reset();
    virtual std::ostream& print( std::ostream &out ) const;
};

typedef PreallocatedObjectFactory<BookOrder>  BookOrderFactory;
BB_DECLARE_INTRUSIVE_PTR( BookOrderFactory );

inline std::ostream& operator <<( std::ostream &out, const BookOrder& bo )
{
    return bo.print( out );
}

} // namespace bb

#endif // BB_CLIENTCORE_BOOKORDER_H
