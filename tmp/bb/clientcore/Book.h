#ifndef BB_CLIENTCORE_BOOK_H
#define BB_CLIENTCORE_BOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#define BOOST_MULTI_INDEX_DISABLE_SERIALIZATION // eliminate unnecessary dependency
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/global_fun.hpp>
#include <boost/multi_index/random_access_index.hpp>
#undef BOOST_MULTI_INDEX_DISABLE_SERIALIZATION

#include <boost/foreach.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include <boost/math/special_functions/round.hpp>
#include <vector>
#include <list>
#include <bb/core/compat.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Msg.h>
#include <bb/core/EventPublisher.h>
#include <bb/clientcore/IEventDistListener.h>
#include <bb/clientcore/ISourceMonitorListener.h>
#include <bb/clientcore/SpamFilter.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/BookLevel.h>
#include <bb/clientcore/BookSpec.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/IClockListener.h>
#include <bb/clientcore/SessionParams.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );

class SetAlphaEntryTimeListener
    : public IBookListener
    , public IBookLevelListener
{
    public:
        // IBookListener callbacks
        void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                    int32_t bidLevelChanged, int32_t askLevelChanged ) override
        {
            setAlphaEntryTime();
        }
        void onBookFlushed( const IBook* pBook, const Msg* pMsg ) override
        {
            setAlphaEntryTime();
        }

        //IBookLevelListener callbacks
        bool onBookLevelAdded( const IBook* pBook, const BookLevel* pLevel ) override
        {
            setAlphaEntryTime();
            return true;
        }
        void onBookLevelDropped( const IBook* pBook, const BookLevel* pLevel ) override
        {
            setAlphaEntryTime();
        }
        void onBookLevelModified( const IBook* pBook, const BookLevel* pLevel ) override
        {
            setAlphaEntryTime();
        }

    private:
        inline void setAlphaEntryTime()
        {
            ClientContext::setAlphaEntryTime();
        }
};

/// Returns the default number of levels of a Book to print.
/// Initial default is 4.
int32_t getDefaultMaxPrintLevels();

/// Sets the default number of levels of a Book to print.
void setDefaultMaxPrintLevels( int32_t numLevels );

// A common implementation of IBook
// Handles the basic fields and listeners
class BookImpl
    : public IBook
{
public:
    virtual ~BookImpl();

    /// Returns the instrument this book is maintained for.
    virtual instrument_t getInstrument() const      { return m_instr; }

    /// Returns a descriptive name for the book.
    virtual std::string  getDesc() const            { return m_desc; }

    /// Returns the source of this book.
    virtual source_t     getSource() const          { return m_src; }

    /// Returns the timeval this book last changed.
    virtual timeval_t    getLastChangeTime() const  { return m_lastChangeTv; }

    /// Returns true if the book is an aggregate book.
    /// An aggregate book is one that does not keep individual orders,
    /// but rather each BookLevel is an aggregate of orders.
    virtual bool isAggregate() const                { return m_aggregate; }

    /// Tells the Book to drop the specified level.
    /// Returns true if the Book actually does it, false otherwise.
    /// This default implementation constantly refuses to drop a level.
    virtual bool dropBookLevel( side_t side, double px )    { return false; }

    /// Adds a book listener to this Book.
    /// Returns true if successful, or false if the listener is already added.
    virtual bool addBookListener( IBookListener* pListener ) const;

    /// Removes a book listener from this Book.
    /// Returns true if successful, or false if the listener was never in the Book.
    virtual bool removeBookListener( IBookListener* pListener ) const;

    /// Adds a book level listener to this Book.
    /// Returns true if successful, or false if the listener is already added.
    virtual bool addBookLevelListener( IBookLevelListener* pListener ) const;

    /// Removes a book level listener from this Book.
    /// Returns true if successful, or false if the listener was never in the Book.
    virtual bool removeBookLevelListener( IBookLevelListener* pListener ) const;

    /// Default impl: don't allow adding to MasterBook.
    virtual void addToMasterBook();

protected:
    // BookImpl must be derived from
    BookImpl( const instrument_t& instr, const std::string& desc, source_t src, bool aggregate );

    /// Children should call this when they want to notify their listeners that they have changed.
    virtual void notifyBookChanged( const Msg* pMsg, int32_t bidLevelChanged, int32_t askLevelChanged ) const;
    /// Children should call this when they want to notify their listeners that they have flushed.
    virtual void notifyBookFlushed( const Msg* pMsg ) const;
    /// Children should call this when they want to notify their listeners that a BookLevel is about to be added.
    /// Returns 0 if no listeners returned false.  Otherwise, returns the number of listeners that returned false.
    virtual size_t notifyBookLevelAdded( const BookLevel* pLevel ) const;
    /// Children should call this when they want to notify their listeners that a BookLevel was dropped.
    virtual void notifyBookLevelDropped( const BookLevel* pLevel ) const;
    /// Children should call this when they want to notify their listeners that a BookLevel was modified.
    virtual void notifyBookLevelModified( const BookLevel* pLevel ) const;

    /// Returns the number of IBookListeners we have.
    size_t numBookListeners() const      { return m_bookListeners.size(); }
    /// Returns the number of IBookLevelListeners we have.
    size_t numBookLevelListeners() const { return m_bookLevelListeners.size(); }

    /// Print the first optional_nlevels of the book to an ostream.
    /// If optional_nlevels isn't specified, either getDefaultMaxPrintLevels() or print_level_chg + 2 is used.
    virtual std::ostream& print( std::ostream& out, int optional_nlvls = -2 ) const;

    // the maximum levels to print, to be overridden by derived classes where needed
    virtual int32_t defaultMaxPrintLevels() const { return bb::getDefaultMaxPrintLevels(); }

protected:
    typedef bb::EventPublisher<IBookListener> IBookPub;
    typedef bb::EventPublisher<IBookLevelListener> IBookLevelPub;

    // All bb::EventPublisher knows how to call back to is a bool notify(Var.. arguments) function.
    // We have to do the actual work here of making it sure we actually call the appropriate function on the
    // listener with the correct arguments.
    //
    // It would have been nicer to have these custom structs defined as local structs within the
    // 'notifyBook(Level)(Added/Dropped/Modified)' functions, but the 2003 C++ Standard 14.3.1/2 forbids this.
    // I hate the 2003 C++ Standard section 14.3.1.
    struct notifyBookFlushedImpl : public IBookPub::INotifier
    {
        notifyBookFlushedImpl(IBookListener* callback) : IBookPub::INotifier ( callback ){}
        bool notify( const IBook* pBook, const Msg* pMsg )
        {
            m_callback->onBookFlushed ( pBook, pMsg ); return true;
        }
    };

    struct notifyBookChangedImpl : public IBookPub::INotifier
    {
        notifyBookChangedImpl(IBookListener* callback) : IBookPub::INotifier ( callback ){}
        bool notify( const IBook* pBook, const Msg* pMsg, const int32_t bidLevelChanged, const int32_t askLevelChanged )
        {
            if( m_callback->shouldNotify(bidLevelChanged,askLevelChanged) )
            {
                m_callback->onBookChanged( pBook, pMsg, bidLevelChanged, askLevelChanged );
            }
            return true;
        }
    };

    struct notifyBookLevelAddedImpl : public IBookLevelPub::INotifier
    {
        notifyBookLevelAddedImpl(IBookLevelListener* callback) : IBookLevelPub::INotifier ( callback ){}
        // Note that we're counting *failures* here. This is on purpose.
        bool notify( const IBook* pBook, const BookLevel* pLevel )
        {
            return !m_callback->onBookLevelAdded( pBook, pLevel );
        }
    };

    struct notifyBookLevelDroppedImpl : public IBookLevelPub::INotifier
    {
        notifyBookLevelDroppedImpl(IBookLevelListener* callback) : IBookLevelPub::INotifier ( callback ){}
        bool notify( const IBook* pBook, const BookLevel* pLevel )
        {
            m_callback->onBookLevelDropped( pBook, pLevel ); return true;
        }
    };

    struct notifyBookLevelModifiedImpl : public IBookLevelPub::INotifier
    {
        notifyBookLevelModifiedImpl(IBookLevelListener* callback) : IBookLevelPub::INotifier ( callback ){}
        bool notify( const IBook* pBook, const BookLevel* pLevel )
        {
            m_callback->onBookLevelModified( pBook, pLevel ); return true;
        }
    };

    instrument_t             m_instr;
    std::string              m_desc;
    source_t                 m_src;
    timeval_t                m_lastChangeTv;
    bool                     m_aggregate;
    mutable IBookPub         m_bookListeners;
    mutable IBookLevelPub    m_bookLevelListeners;
private:
    SetAlphaEntryTimeListener m_alphaEntryTimeListener;
};



/// A common Book implementation is to have two halves of a sorted container.
/// This template class is an IBookLevelCIter implementation for that scheme.
/// There is no ownership of the BookLevels. The BookLevelCPtr is given a
/// deleter that does nothing on destruction.
/// There is no lifetime management so be careful.
template <class TContainer>
class TSortedBookLevelCIterImpl
    : public IBookLevelRawIter
{
public:
    TSortedBookLevelCIterImpl( const TContainer& cont )
        : m_cont( cont)
    {
        initialize();
    }

    virtual ~TSortedBookLevelCIterImpl()
    {}

    ///  Initialize the iterator
    virtual void initialize()
    {
        m_curr_iter = m_cont.begin();
        m_end_iter  = m_cont.end();
    }

    /// Returns true if there is another item in the sequence.
    /// IBookLevelCIter::next will return a valid value if this is true.
    /// IBookLevelCIter::next will return an invalid value if this is false.
    virtual bool hasNext() const
    {
        // we can't be past the end of the vector, and the shared_ptr must be valid.
        return (m_curr_iter != m_end_iter) && (*m_curr_iter);
    }

    /// Returns the next BookLevel from the iterator and increments the iterator.
    /// Returns an empty smart-pointer if the iteration is finished.
    virtual BookLevelCPtr next()
    {
        BookLevel * ptr = nextRaw();
        if ( nullptr == ptr )
            return BookLevelCPtr();
        // return a shared_ptr with no deleter
        return makeNoopSharedCPtr( ptr );
    }

    /// Returns the next BookLevel from the iterator and increments the iterator.
    /// Returns a nullptr if the iteration is finished.
    virtual BookLevel* nextRaw()
    {
        if ( !hasNext() )
        {
            return nullptr;
        }
        // return BookLevel*
        return *(m_curr_iter++);
    }

private:
    typename TContainer::const_iterator m_curr_iter;
    typename TContainer::const_iterator m_end_iter;
    const TContainer&                   m_cont;
};



/// A common Book implementation is to have two halves of a sorted container.
/// This template class is an IBookLevelCIter implementation for that scheme.
/// There is no ownership of the BookLevels. The BookLevelCPtr is given a
/// deleter that does nothing on destruction.
/// Holds a boost::shared_ptr<TShared> to help manage lifetime.
template <class TContainer, class TShared>
class TLockedSortedBookLevelCIterImpl
    : public IBookLevelCIter
{
public:
    // TODO: lifetime management
    TLockedSortedBookLevelCIterImpl( const TContainer& cont, boost::shared_ptr<TShared> spSharedItem )
        : m_spSharedItem( spSharedItem )
        , m_curr_iter( cont.begin() )
        , m_end_iter( cont.end() )
    {}

    virtual ~TLockedSortedBookLevelCIterImpl()
    {}

    /// Returns true if there is another item in the sequence.
    /// IBookLevelCIter::next will return a valid value if this is true.
    /// IBookLevelCIter::next will return an invalid value if this is false.
    virtual bool hasNext() const
    {
        return m_curr_iter != m_end_iter;
    }

    /// Returns the next BookLevel from the iterator and increments the iterator.
    /// Returns an empty smart-pointer if the iteration is finished.
    virtual BookLevelCPtr next()
    {
        if ( m_curr_iter == m_end_iter )
            return BookLevelCPtr();
        // return a shared_ptr with no deleter
        return makeNoopSharedCPtr( *(m_curr_iter++) );
    }

private:
    shared_ptr<TShared>                 m_spSharedItem;
    typename TContainer::const_iterator m_curr_iter;
    typename TContainer::const_iterator m_end_iter;
};

// We're using a boost multi index container to provide access to BookLevel *'s.
// This gives us access to BookLevels by:
// - std::vector-like iterator
// - keyed by identify ( hash of the booklevel )
// - keyed by price ( x multiplier, and rounded ). This will *still iterator over all items*, and
//      we will only use this in an assert below, not for performance.
typedef boost::multi_index_container<BookLevel*, boost::multi_index::indexed_by<
        boost::multi_index::random_access<>, // like vector
        boost::multi_index::hashed_unique<boost::multi_index::identity<BookLevel*> > // like hash_set
        > > bookhalf_base_t;

class bookhalf_t
    : public bookhalf_base_t
{
public:
    using bookhalf_base_t::insert;
    bookhalf_t() {}

    void remove( const value_type& value )
    {
        const std::pair<iterator, iterator> range = std::equal_range(begin(), end(), value, BookLevelCmp());
        erase(range.first, range.second);
    }
    difference_type insert( const value_type& value )
    {
        const iterator it = std::lower_bound(begin(), end(), value, BookLevelCmp());
        insert(it, value);
        return it - begin();
    }
    const_iterator find( const value_type& value ) const
    {
        return project<0>(get<1>().find(value)); // lookup by hash but return by primary index
    }

public:
    // Look for a book level by price.
    // Returns end() if we can't find it, or if we don't have any book levels at all.
    const_iterator find( const double& value ) const
    {
        if ( unlikely ( empty() ) ) { return end(); }
        else
        {
            // TODO: store the side in the book level so we don't have to look it up
            //       within the orders inside.
            const bb::side_t side ( (*begin())->getSide() );
            BB_ASSERT ( side == bb::BID || side == bb::ASK );
            return side == bb::BID ? find<bb::BID>( value ) : find<bb::ASK> ( value );
        }
    }

private:
    // Look for the given price by iterating over all price levels on our side.
    // We're iterating instead of binary searching because we prefer cache locality over
    // algorithmic efficiency. This will help us as long as the number of book levels stays
    // relatively small, less than a few hundred.
    //
    // Because we supply the side and we keep our levels sorted in order, we know when to stop
    // looking.
    template<bb::side_t side>
    const_iterator find( const double& value ) const
    {
        for(const_iterator iter = begin();
                iter != end() && (
                    ( side == BID && bb::GE( (*iter)->getPrice(), value ) ) ||
                    ( side == ASK && bb::LE( (*iter)->getPrice(), value ) ) );
                iter++ )
        {
            if ( bb::EQ ( (*iter)->getPrice(), value ) )
                return iter;
        }
        return end();
    }
};

/// Represents a Book for a given stock.
/// A Book is tied to a specific symbol/source pair.
/// It notifies listeners of changes and keeps various aggregate information.
/// Book MUST be subclassed.  You cannot directly create a Book instance.
class Book
    : public BookImpl
    , protected IEventDistListener
    , protected IClockListener
    , protected SourceMonitorListener
{
public:
    virtual ~Book();

    symbol_t getSymbol() const       { return m_instr.sym; }
    bb::mktdest_t getMktDest() const { return mkt; }

    /// Returns value of book_rdy_fg, which is a status variable that needs to be
    /// kept consistent whenever book changes
    virtual bool isOK() const { return(book_rdy_fg); }

    void reset();

    // set functions for some internal vars
    void setVbose(int v) { vbose = v; }
    void setWarnDelayTv(timeval_t tv) { warn_delay_tv = tv; }

protected:
    // EventListener interface
    virtual void onEvent( const Msg& msg ) {}

    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int32_t reason, void* pData );

    /// SourceMonitorListener handler
    virtual void onSMonStatusChange(source_t src, smon_status_t status,
                                    smon_status_t old_status, const ptime_duration_t& avg_lag_tv,
                                    const ptime_duration_t& last_interval_tv) { }
    void setMktDest( bb::mktdest_t m ) { mkt = m; }

protected:
    // Only subclasses of Book may be used.
    Book( const instrument_t& instr, const ClockMonitorPtr& cm, const SessionParams& sparams
        , const char* desc, int _vbose=0 );

    mktdest_t mkt;
    std::vector<mtype_t> mtypes;

    mutable bool book_rdy_fg;
    bool open_hours_fg;
    bool market_hours_fg;

    ClockMonitorPtr m_spCM;
    SessionParams m_sessionParams;
    int vbose;
    int eventcnt;
    timeval_t warn_delay_tv;
    int lvl_chg[2];
    bool bSourceOK;

    SpamFilter m_spam_filter;
};

BB_DECLARE_SHARED_PTR( Book );


/*****************************************************************************/
//
// Book "Algorithms"
//
/*****************************************************************************/

/// Returns the BookLevel for the specified side and price.
/// If pDepth is passed, fills it with the depth at which the price was found.
/// If it is not found, returns an empty BookLevelCPtr and sets *pDepth to -1.
template <class T>
BookLevelCPtr getBookLevelAtPrice( const T& tbook, side_t side, double px, int32_t* pDepth = NULL )
{
    BB_ASSERT( side == BID || side == ASK );
    const int s = side2sign( side );
    int32_t depth = 0;

    const double spx = s * px;

    IBookLevelCIterPtr iter = tbook.getBookLevelIter( side );
    while ( iter->hasNext() )
    {
        BookLevelCPtr spBL = iter->next();
        double sblpx = s * spBL->getPrice();
        if ( bb::EQ(spx, sblpx) )
        {
            if ( pDepth ) *pDepth = depth;
            return spBL;
        }
        if ( bb::GT(spx, sblpx) )
            break;
        depth++;
    }
    // if we got here, we didn't find anything
    if ( pDepth ) *pDepth = -1;
    return BookLevelCPtr();
}

double getVWAP(  IBook const& ml, bb::side_t side,int32_t arg_sz, bool* pSuccess = NULL );

} // namespace bb

#endif // BB_CLIENTCORE_BOOK_H
