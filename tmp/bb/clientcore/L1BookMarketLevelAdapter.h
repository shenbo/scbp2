#ifndef BB_CLIENTCORE_L1BOOKMARKETLEVELADAPTER_H
#define BB_CLIENTCORE_L1BOOKMARKETLEVELADAPTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/L1Book.h>
#include <bb/clientcore/L1MarketLevelProvider.h>

namespace bb {

class L1BookMarketLevelAdapter : public L1MarketLevelProvider
{
public:
    L1BookMarketLevelAdapter( const ClientContextPtr& cc, const L1BookPtr& book )
        : L1MarketLevelProvider( cc, book->getInstrument(), book->getMktDest() )
        , m_spBook( book ) {}

    virtual const MarketLevel& getMarketLevel() const
    {
        m_mktLevelTmp.set( m_spBook->getSide( BID ), m_spBook->getSide( ASK ) );
        return m_mktLevelTmp;
    }

    virtual const timeval_t& getLastUpdateTime() const
    {
        m_tvTmp = m_spBook->getLastChangeTime();
        return m_tvTmp;
    }

    virtual bool isMarketLevelOk() const
    {
        return m_spBook->isOK();
    }

protected:
    L1BookPtr m_spBook;
    mutable bb::timeval_t m_tvTmp;
    mutable bb::MarketLevel m_mktLevelTmp;
};

BB_DECLARE_SHARED_PTR( L1BookMarketLevelAdapter );

}  // namespace bb

#endif // BB_CLIENTCORE_L1BOOKMARKETLEVELADAPTER_H
