#ifndef BB_CLIENTCORE_L1MARKETLEVELPROVIDER_H
#define BB_CLIENTCORE_L1MARKETLEVELPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Error.h>
#include <bb/core/MarketLevel.h>
#include <bb/core/Subscription.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/MarketLevelProvider.h>
#include <bb/core/smart_ptr.h>
#include <utility>
namespace bb {

BB_FWD_DECLARE_SHARED_PTR(L1MarketLevelProvider);
BB_FWD_DECLARE_SHARED_PTR(SxL1MarketLevelProvider);
BB_FWD_DECLARE_SHARED_PTR(BbL1MarketLevelProvider);

class L1MarketLevelProvider
    : public MarketLevelProviderImpl
{
public:
    /// Destructor
    virtual ~L1MarketLevelProvider() { }

    // Accessors

    virtual mktdest_t getMktDest() const
    {
        return m_mktdest;
    }

    virtual const MarketLevel& getMarketLevel() const
    {
        return m_level;
    }

    virtual symbol_t getSymbol() const
    {
        return m_instr.sym;
    }

    virtual const instrument_t& getInstrument() const
    {
        return m_instr;
    }

    virtual const timeval_t& getLastUpdateTime() const
    {
        return m_last_update_tv;
    }

    virtual bool isMarketLevelOk() const
    {
        return (m_last_update_tv != 0) && ((m_level.getSize(BID) != 0 && NEZ(m_level.getPrice(BID))) ||
            (m_level.getSize(ASK) != 0 && NEZ(m_level.getPrice(ASK))));
    }

    virtual mktdest_t getNBBOMktDest(const side_t side) const
    {
        return side == ASK ? m_ask_nbbo_mkt : m_bid_nbbo_mkt;
    }
    std::pair<mktdest_t,mktdest_t>  getNBBOMktDests() const
    {
        return std::make_pair(m_bid_nbbo_mkt,m_ask_nbbo_mkt);
    }
    // setters
    virtual const MarketLevel& updateMarketLevel(const MarketLevel &ml, const bb::mktdest_t bid_mkt,
                                                 const bb::mktdest_t ask_mkt, const timeval_t update_tv)
    {
        if (update_tv > m_last_update_tv)
        {
            m_level = ml;
            m_last_update_tv = update_tv;
            m_bid_nbbo_mkt = bid_mkt;
            m_ask_nbbo_mkt = ask_mkt;
        }

        return m_level;
    }

protected:
    /// Constructor
    L1MarketLevelProvider(const ClientContextPtr&, const instrument_t&, mktdest_t);

    /// Instrument that this MarketLevelProvider is tracking
    instrument_t m_instr;

    /// The current market destination "mask" of this LevelProvider
    mktdest_t m_mktdest;

    /// Last update time
    timeval_t m_last_update_tv;

    /// Most recently updated market level
    MarketLevel m_level;

    // save the NBBO mktdest for each side
    mktdest_t m_bid_nbbo_mkt;
    mktdest_t m_ask_nbbo_mkt;
};

class SxL1MarketLevelProvider
    : public L1MarketLevelProvider
{
public:
    /// MKT_UNKNOWN implies that you're not tracking a specific market
    /// destination, but the overall NBBO (national best bid and offer).
    SxL1MarketLevelProvider(const ClientContextPtr&, const instrument_t&, mktdest_t = MKT_UNKNOWN);

    void onSxL1Msg(const Msg& msg);

private:
    // Subscription tokens
    Subscription m_sub_quote, m_sub_bbo_quote, m_sub_bbo_app;
};

class BbL1MarketLevelProvider
    : public L1MarketLevelProvider
{
public:
    /// MKT_UNKNOWN implies that you're not tracking a specific market
    /// destination, but the overall NBBO (national best bid and offer).
    BbL1MarketLevelProvider( ClientContextPtr const& client_context,  instrument_t const& instr); //mkt=MKT_UNKNOWN
    BbL1MarketLevelProvider( ClientContextPtr const& client_context,  instrument_t const& instr, mktdest_t mkt );

    void onBbL1Msg( Msg const& msg);

private:
    void init(ClientContextPtr const& client_context,  instrument_t const& instr, mktdest_t mkt);
    // Subscription tokens
    Subscription m_sub_quote, m_sub_bbo_quote, m_sub_bbo_app;
};

} // namespace bb

#endif // BB_CLIENTCORE_L1MARKETLEVELPROVIDER_H
