#ifndef BB_CLIENTCORE_PRICEPROVIDERBUILDER_H
#define BB_CLIENTCORE_PRICEPROVIDERBUILDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash_map.h>
#include <bb/core/sourceset.h>
#include <bb/clientcore/PriceProviderSpec.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>
#include <bb/clientcore/SpamFilter.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( MasterTickProvider );
BB_FWD_DECLARE_SHARED_PTR( MasterTickFactory );
BB_FWD_DECLARE_SHARED_PTR( MultipathTickProvider );
BB_FWD_DECLARE_SHARED_PTR( MultipathTickFactory );
BB_FWD_DECLARE_SHARED_PTR( SourceTickFactory );
BB_FWD_DECLARE_SHARED_PTR( BookBasedAcrIndexFactory );
BB_FWD_DECLARE_SHARED_PTR( IBookBuilder );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( IPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( IPriceSizeProvider );

/// PriceProviderBuilder takes either a PxProviderSpec or a PxSzProviderSpec structure
/// and builds it.
class PriceProviderBuilder
{
public:
    PriceProviderBuilder(
            ClientContextPtr spCContext, bool useSrcMonitors,
            IBookBuilderPtr spBookBuilder );

    IPriceProviderPtr      buildPxProvider( IPxProviderSpecPtr );
    IPriceSizeProviderPtr  buildPxSzProvider( IPxSzProviderSpecPtr );

    // Functions used by Spec subclasses in building their concrete PriceProviders
    IBookBuilderPtr getBookBuilder() { return m_spBookBuilder; }
    MasterTickProviderPtr getMasterTickProvider( const instrument_t& instr, const sourceset_t& sources );
    MultipathTickProviderPtr getMultipathTickProvider( const instrument_t& instr, const sourceset_t& sources );
    SourceTickFactoryPtr getSourceTickFactory() { return m_spTickFactory; }
    BookBasedAcrIndexFactoryPtr getBookAdxFactory() { return m_spBookAdxFactory; }

protected:
    ClientContextWeakPtr        m_spCContext;

    IBookBuilderPtr             m_spBookBuilder;
    SourceTickFactoryPtr        m_spTickFactory;  // SourceTickFactory for now
    MasterTickFactoryPtr        m_spMasterTickFactory;
    MultipathTickFactoryPtr     m_spMultipathTickFactory;
    BookBasedAcrIndexFactoryPtr m_spBookAdxFactory;

    IPriceSizeProviderPtr       m_lastPxSzP;

    // already existing PriceProviders
    typedef bbext::hash_map<IPxProviderSpecCPtr, IPriceProviderWeakPtr, PxPSpecHasher, PxPSpecComparator> PxPSpec2InstanceMap;
    PxPSpec2InstanceMap         m_alreadyBuiltPxPs;
    typedef bbext::hash_map<IPxSzProviderSpecCPtr, IPriceSizeProviderWeakPtr, PxSzPSpecHasher, PxSzPSpecComparator> PxSzPSpec2InstanceMap;
    PxSzPSpec2InstanceMap       m_alreadyBuiltPxSzPs;

    SpamFilter                  m_harvestedPxPSpam;
};
BB_DECLARE_SHARED_PTR( PriceProviderBuilder );


} // namespace bb

#endif // BB_CLIENTCORE_PRICEPROVIDERBUILDER_H
