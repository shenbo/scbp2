#ifndef BB_CLIENTCORE_ACRPING_H
#define BB_CLIENTCORE_ACRPING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/ptr_container/ptr_vector.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/Uuid.h>
#include <bb/core/source.h>

namespace bb {

/// This is a "client" for acr_ping, which is a latency measurement program.
/// This lives in a client program, and answers ping messages.
/// The server side is in clientcore/utils/acr_ping.cc

class AcrPingMsg;
class AcrPingConnectTcpMsg;
class AcrPingConnectMqMsg;
class AcrPingConnectUdpMsg;
class AcrPingConnectUnixMsg;

class FDSet;
class LiveClientContext;
BB_FWD_DECLARE_SHARED_PTR(MsgHandler);

class AcrPingHandler
{
public:
    typedef uint32_t ConnID_t;
    class Connection;
    typedef boost::ptr_vector<Connection> ConnVec_t;

    AcrPingHandler(LiveClientContext *cc);
    ~AcrPingHandler();

    void onConnectTCP (const AcrPingConnectTcpMsg  &connMsg);
    void onConnectMQ  (const AcrPingConnectMqMsg   &connMsg);
    void onConnectUDP (const AcrPingConnectUdpMsg  &connMsg);
    void onConnectUnix(const AcrPingConnectUnixMsg &connMsg);

    void destroyConnection(Connection *conn);
    void onPing(const AcrPingMsg &ping);

protected:
    source_t m_src;
    std::string m_alertPrefix;
    FDSet *m_fdSet;
    MsgHandlerPtr m_connectTCPMsgHandler, m_connectMQMsgHandler, m_connectUDPMsgHandler, m_connectUnixMsgHandler;
    MsgHandlerPtr m_pingMsgHandler;
    ConnVec_t m_connections;
};

} // namespace bb

#endif // BB_CLIENTCORE_ACRPING_H
