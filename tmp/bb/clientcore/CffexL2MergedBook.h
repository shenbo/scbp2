#ifndef BB_CLIENTCORE_CFFEXL2MERGEDBOOK_H
#define BB_CLIENTCORE_CFFEXL2MERGEDBOOK_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <map>
#include <set>

#include <bb/core/messages.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/SourceBooks.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/TickProvider.h>
#include <boost/array.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );

class CffexL2MergedBook
    : public BookImpl
    , private IEventDistListener
    , private SourceMonitorListener
    , public IBookListener
    , public ITickListener
{
public:
    CffexL2MergedBook( const ClientContextPtr& spCC, const instrument_t& instr, source_t src,
                       const std::string& strDesc, SourceMonitorPtr spSMon );
    virtual ~CffexL2MergedBook();

// IBook interface
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, CffexL2MergedBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( side_t side ) const;

protected:
    virtual void onBookChanged( const IBook *book, const Msg *msg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );

    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );

private:
    typedef std::vector<BookLevelPtr>   BookLevelVec;
    typedef std::map<double,BookLevelPtr> BookLevelMap;

    /// EventListener interface
    virtual void onEvent( const Msg& msg );

    void onEvent( const WindFuturesMarketDataMsg& msg );
    void onEvent( const CffexInfoOrderMsg& msg );

    void handleEvent( const WindFuturesMarketDataMsg& msg );

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );

    void updateBookLevels(
        side_t side,
        const WindFuturesMarketDataMsg& msg,
        std::set<int32_t>& levelsChanged );

    ClientContextPtr    m_spCC;
    SourceMonitorPtr    m_spSMon;
    uint32_t            m_verbose;

    bool                m_source_ok;

    BookLevelVec        m_bookLevels[2];    // BID = 0, ASK = 1
    BookLevelMap        m_bookLevelsMap[2];

    EventSubPtr         m_eventSub1;
    EventSubPtr         m_eventSub2;

    IBookPtr            m_spCffexComboBook;
    IBookPtr            m_spShfeL2Book;
    ITickProviderPtr    m_spCffexInfoTick;

    static constexpr size_t MAX_LEVEL = 5;
    static double EMPTY_SIDE_BOOK_OFFSET_PERCENT;
};

BB_DECLARE_SHARED_PTR( CffexL2MergedBook );

/// BookSpec corresponding to an CffexL2Merged book
class CffexL2MergedBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    CffexL2MergedBookSpec() : SourceBookSpec() {}
    CffexL2MergedBookSpec(const bb::CffexL2MergedBookSpec&, const boost::optional<bb::InstrSubst>&);
    CffexL2MergedBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    //virtual void checkValid() const;
    //virtual void hashCombine(size_t &result) const;
    //virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual CffexL2MergedBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    //virtual void getDataRequirements(IDataRequirements *rqs) const;
};
BB_DECLARE_SHARED_PTR(CffexL2MergedBookSpec);

}

#endif // BB_CLIENTCORE_CFFEXL2MERGEDBOOK_H
