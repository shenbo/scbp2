#ifndef BB_CLIENTCORE_CZCEL1TICKPROVIDER_H
#define BB_CLIENTCORE_CZCEL1TICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class CzceL1TickProvider
    : public TickProviderImpl
{
public:
    CzceL1TickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~CzceL1TickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const { return m_bTickReceived; }
    double getTurnover() const { return m_turnover;}

private:
   void onCzceL1QdMsg( const CzceL1QdMsg& msg );


private:
   bool m_bInitialized;
   bool m_bTickReceived;
   MsgHandlerPtr m_subCzceL1QdMsg;

   double m_turnover;
};

BB_DECLARE_SHARED_PTR( CzceL1TickProvider);

}

#endif // BB_CLIENTCORE_CZCEL1TICKPROVIDER_H
