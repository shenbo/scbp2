#ifndef BB_CLIENTCORE_VALIDATE_H
#define BB_CLIENTCORE_VALIDATE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>

namespace bb {

/// A mixin to make validating already constructed objects idiomatic
/// Example
///
///     if (m_ptr && IValidateCPtr v=bb::dynamic_pointer_cast<IValidate const>(m_ptr))
///         v->validate();
/// Where m_ptr may or may not implement IValidate
/// Useful for validating configurations
struct IValidate{
    virtual void validate()const=0;//throw if invalid
    virtual ~IValidate(){}

};
BB_DECLARE_SHARED_PTR( IValidate );
}

#endif // BB_CLIENTCORE_VALIDATE_H
