#ifndef BB_CLIENTCORE_SXL1TICKPROVIDER_H
#define BB_CLIENTCORE_SXL1TICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/EquitySecurityInfo.h>
#include <bb/core/messages.h>

#include <bb/clientcore/IClockListener.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/MsgHandler.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

BB_FWD_DECLARE_SCOPED_PTR(SxL1TradeMsg);

/// Ticks from the SXL1 datafeed
class SxL1TickProvider
    : public TickProviderImpl
    , private IClockListener
{
public:
    /// Constructs a SxL1TickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    SxL1TickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~SxL1TickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_tick_ok; }

    /// Returns the last SxL1TradeMsg handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK()
    const SxL1TradeMsg& getSxL1TickMsg() const  { return *m_tradeMsg; }

private:
    // Event handlers
    void onSxL1Tick( const SxL1TradeMsg& msg );

    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

private:
    const static int R_ONESECAFTERCLOSE = cm::USER_REASON + 1;

    ClockMonitorPtr       m_spCM;
    MsgHandlerPtr         m_spTickHandler;
    SxL1TradeMsgScopedPtr m_tradeMsg;
    bool                  m_tick_ok;

    IEquitySecurityInfoMapCPtr m_equitySecInfo;
};

BB_DECLARE_SHARED_PTR( SxL1TickProvider );

} // namespace bb

#endif // BB_CLIENTCORE_SXL1TICKPROVIDER_H
