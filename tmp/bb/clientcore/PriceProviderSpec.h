#ifndef BB_CLIENTCORE_PRICEPROVIDERSPEC_H
#define BB_CLIENTCORE_PRICEPROVIDERSPEC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Scripting.h>
#include <bb/clientcore/BookSpec.h>

namespace bb {

// Forward declarations
class IPxProviderSpec;
class IPriceProvider;
BB_FWD_DECLARE_SHARED_PTR( IPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( PriceProviderBuilder );
BB_FWD_DECLARE_SHARED_PTR( IPxProviderSpec );

/// An IPxProviderSpec is a description of an IPriceProvider to be built.
/// Having these classes as data means we can pass them around the network.
/// A PriceProviderBuilder is responsible for taking them and actually creating the IPriceProvider.
class IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~IPxProviderSpec() {}

    virtual instrument_t getInstrument() const = 0;

    /// Instantiates a concrete PriceProvider according to the Spec
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const = 0;

    /// Deep copys this IPxProviderSpec. If instrSubst is non-null, all copies of fromInstr in the BookSpec
    /// will be replaced with toInstr.
    virtual IPxProviderSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const = 0;
    inline static IPxProviderSpec* clone(const IPxProviderSpec *e, const boost::optional<InstrSubst> &instrSubst = boost::none)
        { return e ? e->clone(instrSubst) : NULL; }
    inline static IPxProviderSpec* clone(const IPxProviderSpecCPtr &e, const boost::optional<InstrSubst> &instrSubst = boost::none)
        { return clone(e.get(), instrSubst); }

    /// Combines this IPxProviderSpec into the given hash value.
    virtual void hashCombine(size_t &result) const = 0;

    /// Compares two PxProvider specs.
    virtual bool compare(const IPxProviderSpec *other) const = 0;

    /// Prints the lua representation of this IPxProviderSpec.
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const = 0;

    /// ensures sure that all fields have been set correctly, or throws runtime_error
    virtual void checkValid() const = 0;

    /// Propagates all the required data (instr/src pairs) to the IDataRequirements.
    virtual void getDataRequirements(IDataRequirements *rqs) const = 0;
};
BB_DECLARE_SHARED_PTR( IPxProviderSpec );

/// Helper class for converting a string into a SignalSpec.
class PriceProviderSpecReader : public bb::SpecReader
{
public:
    PriceProviderSpecReader();

    // parses the given lua code (which should be lua code for a signal spec),
    // and returns the signal spec
    IPxProviderSpecPtr getPriceProviderSpec(std::string txt);
};


bool operator==(const IPxProviderSpec &a, const IPxProviderSpec &b);
inline bool operator!=(const IPxProviderSpec &a, const IPxProviderSpec &b) { return !(a == b); }

std::ostream &operator<<(std::ostream &out, const IPxProviderSpec &spec);
void luaprint(std::ostream &out, IPxProviderSpec const &spec , LuaPrintSettings const &ps);

std::size_t hash_value(const IPxProviderSpec &a);

// helpers for hash_map
struct PxPSpecHasher : public std::unary_function<size_t, IPxProviderSpecCPtr>
    { size_t operator()(const IPxProviderSpecCPtr &a) const { return hash_value(*a); } };
struct PxPSpecComparator : public std::binary_function<bool, IPxProviderSpecCPtr, IPxProviderSpecCPtr>
    { bool operator()(const IPxProviderSpecCPtr &a, const IPxProviderSpecCPtr &b) const { return *a == *b; } };

typedef IPxProviderSpec IPriceProviderSpec;
BB_DECLARE_SHARED_PTR( IPriceProviderSpec );

} // namespace bb


#endif // BB_CLIENTCORE_PRICEPROVIDERSPEC_H
