#ifndef BB_CLIENTCORE_DAYTIMERANGES_H
#define BB_CLIENTCORE_DAYTIMERANGES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>
#include <bb/core/Scripting.h>

namespace bb
{
BB_FWD_DECLARE_SHARED_PTR(Calendar);
/*
 * DayTimeRange
 * represents a pair of times
 * Used for multiday simulator runs, modelling, etc
 */
class DayTimeRange
{
public:
    //start and end must be the same day, and start<end
    DayTimeRange(bb::timeval_t const& start,bb::timeval_t const& end);
    bb::timeval_t        getStartTimeval()const;
    bb::timeval_t        getEndTimeval()const;
    bb::date_t           getDate()const;
    friend std::ostream& operator<<(std::ostream&, DayTimeRange const&);
    bool operator<(DayTimeRange const&)const;
 private:
    bb::timeval_t m_starttv;
    bb::timeval_t m_endtv;
};
/*
 * a collection of DayTimeRanges
 */
class DayTimeRanges
{
public:
    BB_DECLARE_SCRIPTING();
    typedef DayTimeRange value_type;
    typedef std::vector<value_type> cont_type;
    typedef cont_type::const_iterator const_iterator;
    typedef const_iterator iterator;
    typedef value_type& reference;
    typedef value_type const& const_reference;
    const_iterator  begin()const;
    const_iterator  end()const;
    void push_back(const_reference);
    CalendarPtr getCalendar()const;
    friend std::ostream& operator<<(std::ostream&, DayTimeRanges const&);
private:
    cont_type m_vals;
};
BB_DECLARE_SHARED_PTR(DayTimeRanges);
/*
 * a factory to create DayTimeRanges
 * Example Lua script that will create a series of
 * start/end time at 9:30/16:00 for trading days specified in a range

function at_or_next(cal,dt)
   if  not cal:exists(dt) then
     return cal:next(dt)
   end
   return dt
end
function at_or_previous(cal,dt)
   if not cal:exists(dt)then
      return cal:previous(dt)
   end
   return dt
end


class 'EquityCalendar' (DayTimeRangesFactory)
function EquityCalendar:__init(_dts,dte)
    super( )
    self.begin_day=Timeval(9*60*60+30*60)
    self.end_day  =Timeval(16*60*60)
    local calf=TradingCalendarFactoryDB(CalendarType.USEquities,cmdline_params.dbprofile)
    local cal_all=calf:create();--get all possible dates
    self.cal=calf:create(at_or_previous(cal_all,Date(_dts)),at_or_next(cal_all,Date(dte)))
end

function EquityCalendar:create()
    local res=DayTimeRanges()
    for i in cal.dates do
        res:push_back(DayTimeRange(i.timeval+self.begin_day,i.timeval+self.end_day))
    end
    return res
end
 *
 *
 */
class IDayTimeRangesFactory
{
public:
    virtual ~IDayTimeRangesFactory(){}
    virtual DayTimeRanges create()=0;
};
BB_DECLARE_SHARED_PTR(IDayTimeRangesFactory);

/*
 *
 * execute factorycmd on Environment (environment must already have core and clientcore loaded, which is very likely)
 * for example, if the above script were loaded into the env, then factorycmd = "EquityCalendar(20100701,20100702)" works
 * */
BB_FWD_DECLARE_SHARED_PTR( Environment );
DayTimeRanges  createTimeRangesLua(EnvironmentPtr const&,std::string  const&factorycmd);


} // end namespace bb
#endif // BB_CLIENTCORE_DAYTIMERANGES_H
