#ifndef BB_CLIENTCORE_TICKSTATSSENDER_H
#define BB_CLIENTCORE_TICKSTATSSENDER_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <vector>

#include <bb/core/hash_map.h>
#include <bb/core/instrument.h>
#include <bb/core/messages.h>
#include <bb/core/Subscription.h>

#include <bb/clientcore/MultiTickProvider.h>
#include <bb/clientcore/TickStats.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( ISendTransport );

class TickStatsSender
{
public:
    TickStatsSender( ClientContextPtr cc, ISendTransportPtr out,
                     double min_msg_interval, source_t broadcast_source );
private:
    unsigned int seq_num;
    MultiTickProvider m_multi_tick_provider;
    Subscription sub;
    ISendTransportPtr m_send_transport;
    TickStatisticsMsg m_tickmsg;
    double m_min_msg_interval;
    source_t m_broadcastSource;
    bbext::hash_map<instrument_t, timeval_t> times_map;
    typedef bbext::hash_map<instrument_t, TickStats> tickstats_map_t;
    tickstats_map_t tickstats_map;

public:
    void addSource( const source_t& src ) { m_multi_tick_provider.addSource( src, true ); }
    void addSourceHisto( source_t src, const std::vector<instrument_t>& instrs );

    void onTickUpdate( ITickProviderCPtr tickp );
    void doBroadcast( const TickStats& tickstats );
    void onSyncReq( const Msg& msg );
};

BB_FWD_DECLARE_SHARED_PTR( TickStatsSender );

} // namespace bb

#endif // BB_CLIENTCORE_TICKSTATSSENDER_H
