#ifndef BB_CLIENTCORE_SESSIONPARAMS_H
#define BB_CLIENTCORE_SESSIONPARAMS_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>
#include <bb/core/LuaState.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( LuaState );

class SessionParams
{
public:
    BB_DECLARE_SCRIPTING();

public:
    SessionParams();
    virtual ~SessionParams();

    static bool registerScripting( const LuaStatePtr& s );

    bool cme_24_hr;
};

BB_DECLARE_SHARED_PTR( SessionParams );

} // namespace bb

#endif // BB_CLIENTCORE_SESSIONPARAMS_H
