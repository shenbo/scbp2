#ifndef BB_CLIENTCORE_INCREMENTALRECOVERYMGR_H
#define BB_CLIENTCORE_INCREMENTALRECOVERYMGR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */
#include <boost/optional.hpp>

#include <bb/core/source.h>
#include <bb/core/messages.h>
#include <bb/core/instrument.h>
#include <bb/core/MessageMacros.h>

#include <bb/clientcore/Book.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/IEventDistListener.h>

namespace bb{
BB_FWD_DECLARE_SHARED_PTR( ClientContext );

template<typename MgrTraits>
class IncrementalRecoveryMgr
    : public bb::IEventDistListener
{
    typedef shared_ptr<typename MgrTraits::BaseMsg>     BaseMsgPtr;
public:
    typedef boost::function<void(const bb::Msg&)>    MessageCallback;
    typedef boost::function<void()>                  RecoveryCompleteCallback;

    IncrementalRecoveryMgr( ClientContextPtr cc, IBook& book, MessageCallback msgCB, RecoveryCompleteCallback compCB);
    virtual ~IncrementalRecoveryMgr();

    virtual void onEvent( const bb::Msg& msg );

private:
    void flushVector( uint32_t recoverdSeqnum );

    void cleanup();

    MessageCallback                                m_msgCB;
    RecoveryCompleteCallback                       m_completeCB;
    uint32_t                                       m_lastRecvSeqnum;
    bb::instrument_t                               m_instr;
    bb::Subscription                               m_eventSub;
    std::list<BaseMsgPtr>                          m_msgList;

    static const uint32_t                          s_MaxListSize=50;
};

template<typename MgrTraits>
IncrementalRecoveryMgr<MgrTraits>::IncrementalRecoveryMgr( ClientContextPtr cc , IBook& book, MessageCallback msgCB, RecoveryCompleteCallback compCB )
    : m_msgCB( msgCB )
    , m_completeCB( compCB )
    , m_lastRecvSeqnum( 0 )
    , m_instr( book.getInstrument() )
{
    std::vector<mtype_t> mtypes = MgrTraits::getSubscribeMType();
    // add the mtype for the snapshot msg
    mtype_t  snapshotMtype = MgrTraits::SnapshotMsg::kMType;
    mtypes.push_back( snapshotMtype );
    cc->getEventDistributor()->subscribeEvents(m_eventSub, this, book.getSource(), mtypes, m_instr, PRIORITY_CC_Book );
}

template<typename MgrTraits>
IncrementalRecoveryMgr<MgrTraits>::~IncrementalRecoveryMgr()
{
    cleanup();
}

template<typename MgrTraits>
void IncrementalRecoveryMgr<MgrTraits>::cleanup()
{
    // Clear the subscription to un-subscribe to future events
    m_eventSub.reset();
    // Clear the vector to get rid of any messages allocated
    m_msgList.clear();
}

template<typename MgrTraits>
void IncrementalRecoveryMgr<MgrTraits>::flushVector( uint32_t recoveredSeqnum)
{
    // Pass messages in the vector to the associated book
    // if the message is already included in the snapshot
    // skip it.
    BOOST_FOREACH( const BaseMsgPtr & msgPtr, m_msgList)
    {
        if( msgPtr->getExchangeInstrSeqnum() > recoveredSeqnum )
        {
            m_msgCB( static_cast<Msg&>( *msgPtr ) );
        }
    }
    m_msgList.clear();
}

template<typename MgrTraits>
void IncrementalRecoveryMgr<MgrTraits>::onEvent( const bb::Msg& msg )
{
    uint32_t instrSeqnum;
    boost::optional<bb::instrument_t>  instr = msg.instrument();
    // Check the instrument type to make sure it matches
    if( !bb::instrument_t::equals_no_mkt_no_currency()( instr.get(), m_instr) ){
        return;
    }

    BB_MSG_IF( &msg, const typename MgrTraits::SnapshotMsg, snapshotMsg){
        // We got a new snapshot message. If the fastDepth message vector
        // has messages which are more than 1 newer than the snapshot then
        // we are guaranteed to have a GAP, so just wait for the next one.
        // Otherwise process the snapshot and flush the vector
        if( ( 0 == m_msgList.size() ) ||
            ( m_msgList.front()->getExchangeInstrSeqnum() <= (snapshotMsg->getLastProcessedInstrSeqnum() + 1) ))
        {
            // call the recover callback
            m_msgCB(*snapshotMsg);
            // flush the Vector
            flushVector( snapshotMsg->getExchangeInstrSeqnum() );
            // now call the recovery complete callback. The object
            // may have been destroyed in the call, so member variables
            // cannot be accessed after the callback returns.
            m_completeCB();
        }
        return;
    }
    // fall through and process all other recovery book messages
    BB_MSG_IF( &msg, const typename MgrTraits::BaseMsg, baseMsg)
    {
        instrSeqnum = baseMsg->getExchangeInstrSeqnum();
        // if the list is not empty and there is a gap in sequence
        // numbers, then clear the list.  Otherwise the book will
        // complain when processing the messages
        if( (0 != m_msgList.size() ) &&
            ( instrSeqnum != ( m_lastRecvSeqnum + 1 ) ) )
        {
            m_msgList.clear();
        }
        m_msgList.push_back( BaseMsgPtr( static_cast<typename MgrTraits::BaseMsg*>( baseMsg->clone() ) ) );

        // If the list is getting too large, then pop
        // the front of the list
        if( m_msgList.size() > s_MaxListSize )
        {
            m_msgList.pop_front();
        }

        //limit the size of the vector
        m_lastRecvSeqnum = instrSeqnum;
    }
}

} // namespace bb

#endif // BB_CLIENTCORE_INCREMENTALRECOVERYMGR_H
