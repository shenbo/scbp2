#ifndef BB_CLIENTCORE_SPREADDIFFL1BOOK_H
#define BB_CLIENTCORE_SPREADDIFFL1BOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/Book.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/BookSpec.h>
#include <bb/clientcore/Market.h>

namespace bb
{

class SpreadDiffL1Book : public bb::BookImpl, public bb::IBookListener
{
public:
    // diff = mb*b - ma*a
    SpreadDiffL1Book( bb::IBookPtr term_b, bb::IBookPtr term_a );
    SpreadDiffL1Book( double mb, bb::IBookPtr term_b, double ma, bb::IBookPtr term_a );
    virtual ~SpreadDiffL1Book();

    Market getBestMarket() const { return m_market; }

    virtual bb::PriceSize getNthSide( size_t depth, bb::side_t side ) const
    {
        if( depth == 0 )
        {
            const boost::optional<bb::PriceSize>& v = m_market.getSide( side );
            return v ? *v : bb::PriceSize();
        }
        else
            return bb::PriceSize();
    }

    virtual void flushBook()
    {
        m_market.clear();
        notifyBookFlushed( 0 );
    }

    virtual bool isOK() const
    {
        return m_spTermB->isOK() && m_spTermA->isOK();
    }

    virtual double getMidPrice() const
    {
        if( isOK() && m_market.getBid() && m_market.getAsk() )
            return m_market.getMidPrice();
        else
            return 0.0;
    }

    virtual size_t getNumLevels( bb::side_t s ) const
    {
        return ( m_market.getSide( s ) ? 1 : 0 );
    }

    // make one that doesn't work.
    class InvalidBookLevelCIter : public bb::IBookLevelCIter
    {
    public:
        virtual bool hasNext() const { return false; }
        virtual bb::BookLevelCPtr next() { return bb::BookLevelCPtr(); }
    };

    virtual bb::IBookLevelCIterPtr getBookLevelIter( bb::side_t side ) const
    {
        return bb::IBookLevelCIterPtr( new InvalidBookLevelCIter() );
    }

    std::ostream& print( std::ostream& os, int optional_nlvls = 1 ) const
    {
        os << (m_market.getBid() ? m_market.getBid()->px() : 0) << "x"
           << (m_market.getAsk() ? m_market.getAsk()->px() : 0) << " "
           << (m_market.getBid() ? m_market.getBid()->sz() : 0) << "x"
           << (m_market.getAsk() ? m_market.getAsk()->sz() : 0) ;
        return os;
    }

    bb::IBookPtr getBook0() const { return m_spTermA; }
    bb::IBookPtr getBook1() const { return m_spTermB; }

protected:
    virtual void onBookChanged( const bb::IBook* pBook, const bb::Msg* pMsg
        , int32_t bidLevelChanged, int32_t askLevelChanged );
    void updateMarket();

protected:
    Market m_market;

    double m_multB;
    bb::IBookPtr m_spTermB;
    double m_multA;
    bb::IBookPtr m_spTermA;
};

BB_DECLARE_SHARED_PTR( SpreadDiffL1Book );

/// BookSpec corresponding to an Shfe book
class SpreadDiffL1BookSpec : public IBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SpreadDiffL1BookSpec()
        : m_mult_a( -1 )
        , m_mult_b( 1 )
    {}
    SpreadDiffL1BookSpec(const SpreadDiffL1BookSpec &a, const boost::optional<InstrSubst> &instrSubst);
    virtual IBookPtr build(BookBuilder *builder) const;
    virtual IBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual void checkValid() const;
    virtual const instrument_t &getInstrument() const { return m_book_b->getInstrument(); }


    double m_mult_a;
    double m_mult_b;
    IBookSpecPtr m_book_a;
    IBookSpecPtr m_book_b;

};
BB_DECLARE_SHARED_PTR(SpreadDiffL1BookSpec);

} // namespace bb

#endif // BB_CLIENTCORE_SPREADDIFFL1BOOK_H
