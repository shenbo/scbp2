#ifndef BB_CLIENTCORE_LIVEMSTREAMMANAGER_H
#define BB_CLIENTCORE_LIVEMSTREAMMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <string>

#include <boost/function.hpp>
#include <boost/optional.hpp>

#include <bb/core/hash_map.h>

#include <bb/io/RecvTransport.h>

#include <bb/clientcore/MStreamManager.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( IRecvTransport );
BB_FWD_DECLARE_SHARED_PTR( TransMplexStream );
BB_FWD_DECLARE_SHARED_PTR( ICancelableDNSResolver );
BB_FWD_DECLARE_SHARED_PTR( PublisherSubscriptionsManager );

namespace mmap_control {
BB_FWD_DECLARE_SHARED_PTR( File );
}

/// Implementation of MStreamManager for live contexts.
class LiveMStreamManager
    : public IMStreamManager
{
public:
    LiveMStreamManager( const std::string& id );
    LiveMStreamManager( const std::string& id, const FDSetPtr& );
    virtual ~LiveMStreamManager() {}

    /// Adds the passed IRecvTransport to this MStreamManager.
    /// The manager assumes ownership of the transport.
    void addTransport( IRecvTransportPtr spTrans,
                       const IRecvTransport::EndOfStreamCB &endOfStreamCB = IRecvTransport::EndOfStreamCB(),
                       const IRecvTransport::ErrorCB &errorCB = IRecvTransport::ErrorCB() );

    /// Removes the passed IRecvTransport from this MStreamManager.
    void removeTransport( IRecvTransportPtr spTrans );

    /// Returns the TransMplexStream used internally by the LiveMStreamManager.
    /// With great power comes great responsibility.
    TransMplexStreamPtr getTransMplexStream();

    // IMStreamManager
    virtual void run( IMStreamCallback *cb );
    virtual void exit();
    virtual void sigAction( int sig, const UnixSigCallback &cb );

    virtual void addFeed( const source_t& source );

    virtual void addDataByMTypeInstr( const source_t&, mtype_t, const instrument_t& );
    virtual void addDataByMType( const source_t&, mtype_t );

    virtual void startDispatch(IMStreamCallback *c);
    virtual void endDispatch();

    /// LiveMStreamManager offers an async DNS resolver.
    const ICancelableDNSResolverPtr& getDNSResolver();

    /// Add a callback for message loss from the specified source if
    /// possible. If the specified source does not exist in the
    /// LiveMStreamManager, or if the underlying IRecvTransport for
    /// the source does not export a mechanism for detecting message
    /// loss, then this method will return false.
    bool addMessageLossCallback(
        Subscription& outSub,
        const source_t& source,
        const boost::function<void()>& callback );

    void setLogAddFeeds( bool enable );

    /// Return the NIC time for the last message receveid
    virtual const timeval_t& getLastMsgReceiveNicTimestamp() const;

    /// Returns the source address ( typically fqdn of the qd host ) of the last received message.
    virtual const bb::sockaddr_ipv4_t & getLastMsgSourceAddress() const;

    /// Request a removal of market data for the given source/mtype/instr.
    /// Note that a call to removeDataByMTypeInstr should be made for each
    /// call to addDataByMTypeInstr.
    virtual void removeDataByMTypeInstr( const source_t& source, const mtype_t mtype, const instrument_t& instr );

    /// Remove market data for the given source/mtype.
    /// Note that a call to removeDataByMType should be made for each
    /// call to addDataByMType.
    virtual void removeDataByMType( const source_t& source, const mtype_t mtype );

    /// Sets the timeout that will be used on the next blocking operation
    /// (and all subsequent ones until the next setNextTimeout). If no data
    /// is received in this interval.
    virtual void setNextTimeout(const ptime_duration_t &timeout);

private:
    void init( );

    /// Return the PublisherSubscription Manager.  This function will
    /// create the manager if one does not exist, so the underlying
    /// class member variable should not be used.
    PublisherSubscriptionsManagerPtr getPubSubManager();

    typedef std::vector<IRecvTransportWeakPtr> IRecvTransportWeakPtrVec;
    IRecvTransportVec addFeedMMap( const source_t& );

    typedef bbext::hash_map<source_t, IRecvTransportWeakPtrVec> SourceMap;

    TransMplexStreamPtr             m_spTMplexStream;
    SourceMap                       m_vSources;
    bool                            m_logFeeds;

    typedef std::map<std::string, mmap_control::FilePtr> mmap_control_map_t;
    mmap_control_map_t m_mmaps;
    boost::optional<std::string>    m_feedInterface;

    PublisherSubscriptionsManagerPtr m_spPublisherSubscriptions;
    ICancelableDNSResolverPtr m_spResolver;
    std::string               m_id;
};
BB_DECLARE_SHARED_PTR( LiveMStreamManager );

inline TransMplexStreamPtr LiveMStreamManager::getTransMplexStream() { return m_spTMplexStream; }

} // namespace bb

#endif // BB_CLIENTCORE_LIVEMSTREAMMANAGER_H
