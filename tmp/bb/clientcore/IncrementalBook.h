#ifndef BB_CLIENTCORE_INCREMENTALBOOK_H
#define BB_CLIENTCORE_INCREMENTALBOOK_H

#include <bb/clientcore/Book.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/SourceMonitor.h>
#include <bb/clientcore/IncrementalRecoveryMgr.h>

#include <boost/foreach.hpp>
#include <boost/iterator/filter_iterator.hpp>

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */
namespace bb {

// Tack on an implied flag so we can break apart the merged book if our view
// says we should.
class IncrementalLevel
    : public BookLevel
{
public:
    IncrementalLevel()
        : BookLevel()
        , is_implied( false )
    {

    }

    IncrementalLevel( source_t _src, side_t _side, double _px, int32_t _sz, int32_t numOrders = -1, bool p_is_implied = false  )
        : BookLevel( _src, _side, _px, _sz, numOrders ), is_implied( p_is_implied )
    {

    }

    IncrementalLevel( source_t _src, side_t _side, double _px, bool p_is_implied = false )
        : BookLevel( _src, _side, _px ), is_implied( p_is_implied )
    {

    }

    bool is_implied;
};
BB_DECLARE_SHARED_PTR( IncrementalLevel );

class LevelChange;

template<bb::EFeedType>  struct IncrementalBookTraits;

template<typename Traits>
class IncrementalBook
    : public BookImpl
    , public IEventDistListener
    , public SourceMonitorListener
{
protected:
    typedef IncrementalRecoveryMgr<Traits>        RecoveryMgr;
    typedef boost::shared_ptr< RecoveryMgr >      RecoveryMgrPtr;
    typedef std::vector<IncrementalLevelPtr>      BookLevelVec;

    enum EView { MERGED = 0, OUTRIGHT = 1, IMPLIED = 2 };

    /// Do not instantiate this class directly, you muist inherit from it.
    /// Default view is merged.
    IncrementalBook( const instrument_t& instr, ClientContextPtr spCC, source_t src,
                     const std::string& strDesc, SourceMonitorPtr spSMon, EView view = MERGED, size_t holeCheckDepth = 2);


    /// Returns the current book view (either MERGED, OUTRIGHT, or IMPLIED).
    EView getView() const { return m_view; }
    /// Sets the current book view (either MERGED, OUTRIGHT, or IMPLIED).
    void setView( EView view );


public:
    typedef boost::function<bool(bb::IncrementalBook<Traits>&, bb::timeval_t, uint32_t)> AllowRecoveryCallback;
    typedef boost::function<void(bb::IncrementalBook<Traits>&)>                          RecoveryCompleteCallback;

    enum ENotify { NOTIFY, NO_NOTIFY};

    virtual ~IncrementalBook();

    /// Returns true if the implied book is OK.
    /// Note that a completely empty implied book is OK.
    bool isImpliedOK() const;

    //
    // IBook interface
    //

    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, CmeBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( side_t side ) const;

    size_t getExpectedLevels( side_t side, bool bImplied ) const;

    void setHoleCheckDepth( size_t depth );
    size_t getHoleCheckDepth() const;

    boost::optional<uint32_t> getLastProcessedInstrSeqnum() const;

    void subscribeToMarketData();
    void unsubscribeToMarketData();

    void setAllowRecoveryCallback( AllowRecoveryCallback cb);

    void setRecoveryCompleteCallback( RecoveryCompleteCallback cb);

    /// EventListener interface
    virtual void onEvent( const Msg& msg );

    void onRecoveryEvent( const Msg& msg );

protected:

    template<typename UpdateMsgT>
    void onUpdate( const UpdateMsgT& msg, ENotify notify );

    template<typename UpdateMsgT>
    void modifyAction( BookLevelVec& levels, uint32_t action, const UpdateMsgT& msg, bool priceCheck);

    virtual bool handleOtherActions( BookLevelVec& levels, uint32_t action, const Msg& msg) =0;
    virtual bool handleOtherActionNotify( BookLevel* level, uint32_t action) = 0;

    /// Finds the specified order in the book and updates its value.
    /// If size <= 0, then it is removed from the book.
    /// If it is not already in the book, then it is added.
    /// If seqnum < 0, then this is from an aggregate update; a seqnum is automatically generated.
    /// Returns true if the book is modified, false otherwise.
    bool applyOrderToBook( uint32_t level, side_t side, const timeval_t& tv_msg, const timeval_t& tv_exch,
                           int32_t sz, double px, int32_t numOrders, bool implied );

    /// Compares the current viewed book to m_prevLevels, generating the appropriate
    /// BookLevel notifications. If only does this if there are visitors.
    /// Updates m_prevLevels to be the same as the current view.
    void dispatchLevelNotifications();

    // Children should call this when they want to notify their listeners that they have changed.
    virtual void notifyBookChanged_View( EView view, const Msg* pMsg, int32_t bidLevelChanged,
                                         int32_t askLevelChanged ) const;

    void updateMergedView( side_t side );

protected:
    static const int NO_SEQNUM = -1;
    static const size_t NUM_LEVELS;
    static const size_t NUM_IMPLIED_LEVELS;


    // initialize recovery process
    void initRecovery();
    void startRecovery( uint32_t msg_dropped );
    void onRecoveryComplete();

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
                                     const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );

    // Children should implement this function to handle messages.
    // If the message was proeprly handled return true
    virtual bool handleMessage( const Msg& msg) = 0;

    /// Flushes one side of the book.
    /// Does NOT call listeners.
    void flushSide( EView view, side_t side );

    /// Generates the LevelChanges for the given side.  Helper for generateNotifications.
    void generateChangesForSide( side_t side, std::vector<LevelChange>& vChanges ) const;

    /// Checks for holes in a side of the passed BookLevelVec (book half)
    /// Returns true if there are holes, false otherwise.
    /// If the whole half is invalid, false will be returned.
    bool hasHoles( const BookLevelVec& blvec ) const;

    // Children should call this when they want to notify their listeners that they have flushed.
    virtual void notifyBookFlushed_View( EView view, const Msg* pMsg ) const;

    bool fixBooks( side_t trusted_side, double trusted_px, bool implied );

protected:

    ClientContextWeakPtr m_wpCC;
    SourceMonitorPtr m_spSMon;
    uint32_t m_verbose;

    EView m_view;
    BookLevelVec* m_viewedLevels[2];           // BID = 0, ASK = 1
    BookLevelVec m_prevViewedLevels[2];        // BID = 0, ASK = 1

    uint32_t m_next_bo_seqnum;

    mutable bool m_ok, m_ok_dirty;
    bool m_source_ok;

    size_t m_holeCheckDepth;

    // Book levels are kept in a std::vector.  Most CME books are only 5 levels
    // deep.  An empty level has a null BookLevelPtr.  Level 0 is top of book.

    /// The merged levels is the book that CME uses as their canonical
    /// representation of the market.  It will include both implied and non-
    /// implied quotes.
    BookLevelVec m_mergedLevels[2];

    /// Only contains outright quotes.  Expected to have holes.  Each level is
    /// mutually exclusive with the corresponding implied level.
    BookLevelVec m_outrightLevels[2];

    /// Only contains implied quotes.  Expected to have holes.  Each level is
    /// mutually exclusive with the corresponding outright level.
    BookLevelVec m_impliedLevels[2];

    EventSubPtr m_eventSub;

    uint32_t                                  m_lastInstrSeqnum;
    RecoveryMgrPtr                            m_recoveryMgr;
    AllowRecoveryCallback                     m_allowRecoveryCB;
    RecoveryCompleteCallback                  m_recoveryCompleteCB;
    uint32_t                                  m_msgRecvCnt;
    uint32_t                                  m_updatesMissed;
    uint32_t                                  m_updatesMissedSinceLast;
    uint32_t                                  m_recoveryNum;
    uint32_t                                  m_recoverySkipped;
    uint32_t                                  m_bookUpdateMsgCnt;
    timeval_t                                 m_lastRecoveryTime;
    bool                                      m_outOfSync;
    bool                                      m_isAdd;

};

namespace detail {

// IBookLevelCIter implementation
//
// Due to the way Incremental update works (absolute specification of depth via partial book
// painting), it is possible for there to be empty slots in the BookLevel vector.
// So, this must be handled by iterating to the next non-empty slot.
class IncrementalBookLevelCIter
    : public IBookLevelCIter
{
public:
    virtual ~IncrementalBookLevelCIter()
    {

    }

    /// Returns true if there is another item in the sequence.
    /// IBookLevelCIter::next will return a valid value if this is true.
    /// IBookLevelCIter::next will return an invalid value if this is false.
    virtual bool hasNext() const
    {
        // we can't be past the end of the vector, and the shared_ptr must be valid.
        for( std::vector<IncrementalLevelPtr>::const_iterator iter = m_curr_iter; iter != m_end_iter; ++iter )
            if( *iter )
                return true;
        return false;
    }

    /// Returns the next BookLevel from the iterator and increments the iterator.
    /// Returns an empty smart-pointer if the iteration is finished.
    virtual BookLevelCPtr next()
    {
        // iterate until we find the next valid level,
        // or return an invalid shared pointer at the end
        while( m_curr_iter != m_end_iter )
        {
            if( BookLevelCPtr spBL = *m_curr_iter++ )
                return spBL;
        }
        return BookLevelCPtr();
    }

//protected:
    // friend class bb::IncrementalBook;

    IncrementalBookLevelCIter( const std::vector<IncrementalLevelPtr>& v )
        : m_curr_iter( v.begin() )
        , m_end_iter( v.end() )
    {

    }

private:
    std::vector<IncrementalLevelPtr>::const_iterator m_curr_iter;
    std::vector<IncrementalLevelPtr>::const_iterator m_end_iter;
};

} // namespace detail

namespace{
    const double CME_EPSILON = .0001; //1.0e-4
}

template<typename Traits>
IncrementalBook<Traits>::IncrementalBook( const instrument_t& instr, ClientContextPtr spCC, source_t src,
                                          const std::string& strDesc, SourceMonitorPtr spSMon, EView view,
                                          size_t holeCheckDepth)
    : BookImpl( instr, strDesc, src, true ) // true => isAggregate
    , m_wpCC( spCC )
    , m_spSMon( spSMon )
    , m_next_bo_seqnum( 0 )
    , m_ok( false )
    , m_ok_dirty( true )
    , m_source_ok( true )
    , m_holeCheckDepth( holeCheckDepth )
    , m_lastInstrSeqnum( 0 )
    , m_allowRecoveryCB( AllowRecoveryCallback() )
    , m_recoveryCompleteCB( RecoveryCompleteCallback() )
    , m_msgRecvCnt( 0 )
    , m_updatesMissed( 0 )
    , m_updatesMissedSinceLast( 0 )
    , m_recoveryNum( 0 )
    , m_recoverySkipped( 0 )
    , m_bookUpdateMsgCnt( 0)
    , m_lastRecoveryTime( timeval_t::earliest )
{
    if( !spCC )
    {
        throw std::invalid_argument( "ClientContextPtr spCC" );
    }
    if( !spCC->getEventDistributor() )
    {
        throw std::invalid_argument( "EventDistributorPtr: spCC->getED" );
    }

    m_verbose = spCC->getVerbose();

    // If not recovering from server , then subscribe to market data
    subscribeToMarketData();

    // connect with SourceMonitor
    if( m_spSMon )
    {
        m_spSMon->addSMonListener( this );
    }

    setView( view );

    // start recovery
    initRecovery();

    if( m_verbose )
        std::cout << "Constructed IncrementalBook for " << m_instr << std::endl;
}

template<typename Traits>
void IncrementalBook<Traits>::subscribeToMarketData( )
{
    // connect with ED
    ClientContextPtr cc = m_wpCC.lock();
    if( !cc ){
        BB_THROW_ERROR( "ClientContextPtr is bad" );
    }

    cc->getEventDistributor()->subscribeEvents( m_eventSub, this, getSource(), Traits::getSubscribeMType(), m_instr, PRIORITY_CC_Book );

 }

template<typename Traits>
void IncrementalBook<Traits>::unsubscribeToMarketData()
{
    m_eventSub.reset();
}

template<typename Traits>
IncrementalBook<Traits>::~IncrementalBook()
{
    // disconnect from SourceMonitor
    if( m_spSMon )
        m_spSMon->removeSMonListener( this );

    LOG_INFO << "instr:"                 << getInstrument()
             << " msg_received:"         << m_msgRecvCnt
             << " book_updates:"         << m_bookUpdateMsgCnt
             << " msg_dropped:"          << m_updatesMissed
             << " recoveries_performed:" << m_recoveryNum
             << " recoveries_skipped:"   << m_recoverySkipped
             << bb::endl;

}


/// Returns true if the book is in a "valid" state.
/// The meaning of this is book-dependent, however clients typically use it
/// to test whether they should "trust" the book's contents.
template<typename Traits>
bool IncrementalBook<Traits>::isOK() const
{
    if( m_ok_dirty )
    {
        if( !m_source_ok )
            m_ok = false;
        // if recovery is active, or we have detected a missing update, then the book is not ok
        else if( m_recoveryMgr || (0 != m_updatesMissedSinceLast ) )
        {
            m_ok = false;
        }
        else
        {
            m_ok = true;
            if( m_view == IMPLIED || m_view == MERGED )
            {
                m_ok = m_ok && !( hasHoles( m_impliedLevels[BID] ) || hasHoles ( m_impliedLevels[ASK] ) );
            }

            if( m_view == OUTRIGHT || m_view == MERGED )
            {
                m_ok = m_ok && !( hasHoles( m_outrightLevels[BID] ) || hasHoles ( m_outrightLevels[ASK] ) );
            }

            m_ok = m_ok && checkBookStructure( *this, -1, false );
        }
        m_ok_dirty = false;
    }
    return m_ok;
}


/// Returns true if the IMPLIED book is OK.
/// Note that a completely empty implied book is OK.
template<typename Traits>
bool IncrementalBook<Traits>::isImpliedOK() const
{
    // A side can be empty in implied books
    if( hasHoles( m_impliedLevels[BID] ) || hasHoles( m_impliedLevels[ASK] ) )
        return false;

    BOOST_FOREACH( const IncrementalLevelPtr& spBL, m_impliedLevels[BID] )
    {
        if( spBL && !spBL->detailedIsOK() )
            return false;
    }

    BOOST_FOREACH( const IncrementalLevelPtr& spBL, m_impliedLevels[ASK] )
    {
        if( spBL && !spBL->detailedIsOK() )
            return false;
    }

    return true;
}

/// Flushes the book.
template<typename Traits>
void IncrementalBook<Traits>::flushBook()
{
    if( m_verbose >= 2 )
        std::cout << "CMEBook: " << m_desc << " flushBook" << std::endl;

    flushSide( MERGED, BID );
    flushSide( MERGED, ASK );

    // clear the sequence number to trigger a recovery on the next message
    m_lastInstrSeqnum = 0;

    // flushSide sets m_ok_dirty
    notifyBookFlushed_View( MERGED, NULL );
}


template<typename Traits>
void IncrementalBook<Traits>::setView( EView view )
{
    m_view = view;
    m_ok_dirty = true;
    m_prevViewedLevels[BID].clear();
    m_prevViewedLevels[ASK].clear();

    // updated the "viewed levels"
    switch( m_view )
    {
    case MERGED:
        m_viewedLevels[BID] = &m_mergedLevels[BID];
        m_viewedLevels[ASK] = &m_mergedLevels[ASK];
        break;
    case OUTRIGHT:
        m_viewedLevels[BID] = &m_outrightLevels[BID];
        m_viewedLevels[ASK] = &m_outrightLevels[ASK];
        break;
    case IMPLIED:
        m_viewedLevels[BID] = &m_impliedLevels[BID];
        m_viewedLevels[ASK] = &m_impliedLevels[ASK];
        break;
    }
}


/// Returns the mid-price of a book, or 0.0 if the book is not valid.
/// If one side of the book is empty, IncrementalBook's getMidPrice will
/// return the price of the existing side.
template<typename Traits>
double IncrementalBook<Traits>::getMidPrice() const
{
    bool bid_empty = m_viewedLevels[BID]->empty() || !( *m_viewedLevels[BID] )[0];
    bool ask_empty = m_viewedLevels[ASK]->empty() || !( *m_viewedLevels[ASK] )[0];
    if( bid_empty && !ask_empty )
        return ( *m_viewedLevels[ASK] )[0]->getPrice();
    else if( !bid_empty && ask_empty )
        return ( *m_viewedLevels[BID] )[0]->getPrice();
    else if( bid_empty && ask_empty )
        return 0.0;

    double bidpx = ( *m_viewedLevels[BID] )[0]->getPrice();
    double askpx = ( *m_viewedLevels[ASK] )[0]->getPrice();
    return ( bidpx + askpx ) / 2.0;
}


/// Returns the PriceSize at a specified depth and side.
/// Returns an empty PriceSize if nothing exists at that depth or side.
template<typename Traits>
PriceSize IncrementalBook<Traits>::getNthSide( size_t depth, side_t side ) const
{
    if( m_viewedLevels[side]->size() > depth )
        if( const IncrementalLevelPtr& spBookLevel = ( *m_viewedLevels[side] )[depth] )
            return spBookLevel->getPriceSize();
    return PriceSize();
}


// Returns an object for iterating over BookLevels of a particular side.
// Postcondition: The returned iterator will iterate getNumLevels objects.
// This abstracts the underlying containers used by a book.
template<typename Traits>
IBookLevelCIterPtr IncrementalBook<Traits>::getBookLevelIter( side_t side ) const
{
    return IBookLevelCIterPtr( new detail::IncrementalBookLevelCIter( *m_viewedLevels[side] ) );
}


/// Returns the number of levels of a particular side of this book.
template<typename Traits>
size_t IncrementalBook<Traits>::getNumLevels( side_t side ) const
{
    const BookLevelVec& blvec = *m_viewedLevels[side];
    int32_t num_levels = blvec.size();

    // subtract away the empties
    for( BookLevelVec::const_reverse_iterator iter = blvec.rbegin(); iter != blvec.rend(); ++iter )
    {
        if( ! *iter )
            --num_levels;
    }
    return num_levels;
}

template<typename Traits>
size_t IncrementalBook<Traits>::getExpectedLevels( side_t side, bool bImplied ) const
{
    const BookLevelVec& vLevels = ( bImplied ? m_impliedLevels[side] : m_outrightLevels[side] );
    return vLevels.size();
}

/// Flushes one side of the book.
/// Does NOT call listeners.
template<typename Traits>
void IncrementalBook<Traits>::flushSide( EView view, side_t side )
{
    m_ok_dirty = true;
    m_mergedLevels[side].clear();
    m_prevViewedLevels[side].clear();
    switch( view )
    {
    case MERGED:
        m_outrightLevels[side].clear();
        m_impliedLevels[side].clear();
        break;
    case OUTRIGHT:
        m_outrightLevels[side].clear();
    case IMPLIED:
        m_impliedLevels[side].clear();
        break;
    }
}


/// Finds the specified order in the book and updates its value.
/// If size <= 0, then it is removed from the book.
/// If it is not already in the book, then it is added.
/// Returns true if the book is modified, false otherwise.
template<typename Traits>
bool IncrementalBook<Traits>::applyOrderToBook( uint32_t level, side_t side, const timeval_t& tv_msg, const timeval_t& tv_exch,
                                int sz, double px, int32_t numOrders, bool implied )
{
    // find and update an existing BookLevel
    std::vector<IncrementalLevelPtr>& vLevels = ( implied ? m_impliedLevels[side] : m_outrightLevels[side] );

    // make sure there are enough levels (due partial paint?)
    // missing levels will simply be invalid pointers
    // isOK will detect this "error" condition
    vLevels.resize( std::max( vLevels.size(), static_cast<size_t>( level + 1 ) ) );
    IncrementalLevelPtr& spBL = vLevels[level];

    // size == 0 means we are clearing the level
    if( sz == 0 )
    {
        // if invalid, there's nothing to drop
        if( !spBL )
            return false;
        vLevels.erase( vLevels.begin() + level );
    }
    // if there isn't a valid level, insert one
    else if( !spBL )
    {
        // make a new BookLevel
        spBL.reset( new IncrementalLevel( m_src, side, px, 0, numOrders ) );
        spBL->is_implied = implied;
        spBL->pushbackOrder(
            new BookOrder( m_instr, tv_msg, tv_exch, ++m_next_bo_seqnum, side, px, sz, MKT_CME, m_src ) );
    }
    else // otherwise, modify existing level
    {
        spBL->setPrice( px );
        spBL->setSize( sz );
        spBL->setNumOrders( numOrders );
        spBL->is_implied = implied;
        BookOrder* pOrder = spBL->getOrders().front();
        pOrder->px = px;
        pOrder->sz = sz;
        pOrder->tv_exch = tv_exch;
        pOrder->tv_msg = tv_msg;
        pOrder->idnum = ++m_next_bo_seqnum;
    }

    return true;
}


/// EventListener interface
template<typename Traits>
void IncrementalBook<Traits>::onEvent( const Msg& msg )
{
    // all messages that we are expecting should support the instrument function
    boost::optional<instrument_t>  instr = msg.instrument();

    // we get callbacks for all instruments with the same symid
    // so check to make sure this message is for the symbol of
    // interest
    if( !bb::instrument_t::equals_no_mkt_no_currency()(instr.get(), m_instr) ){
        return;
    }

    bool handled = false;
    // handle messages of new type
    BB_MSG_IF(&msg, const typename Traits::BaseMsg, pBase)
    {
        uint32_t    msgInstrSeqnum = pBase->getExchangeInstrSeqnum();

        // ignore any messages that are too old
        if( unlikely( msgInstrSeqnum <= m_lastInstrSeqnum ) ){
            // ignore old messages
            return;
        }
        // make sure the sequence number matches if not, then start recovery
        else if( unlikely( msgInstrSeqnum != ( m_lastInstrSeqnum + 1 ) ) )
        {
            uint32_t  msg_dropped = msgInstrSeqnum - m_lastInstrSeqnum - 1;
            if( likely( m_lastInstrSeqnum != 0 ) )
            {
                m_updatesMissed += msg_dropped;
            }
            LOG_WARN << " Missing packet for instr:" << m_instr
                     << " expected_seqnum:"         << m_lastInstrSeqnum +1
                     << " received_seqnum:"         << msgInstrSeqnum
                     << bb::endl;

            // Set dirty flah when we detect a missing packet.
            m_ok_dirty = true;

            startRecovery( msg_dropped );
        }
        ++m_msgRecvCnt;
        m_lastInstrSeqnum = msgInstrSeqnum;
        BB_MSG_IF(&msg, const typename Traits::BookUpdateMsg, pUpdate )
        {
            ++m_bookUpdateMsgCnt;
            onUpdate( *pUpdate, NOTIFY );
            handled = true;
        }
        else
        {
            handled = handleMessage( msg );
        }
    }
    else {
        handled = handleMessage( msg );
    }
    if( !handled )
    {
        LOG_WARN<<"Unhandled Message with mtype: "<<msg.hdr->mtype << bb::endl;
    }
}

/// EventListener interface
template<typename Traits>
void IncrementalBook<Traits>::onRecoveryEvent( const Msg& msg )
{

    BB_MSG_IF(&msg, const typename Traits::SnapshotMsg, pSnapshot)
    {
        //flush the book
        flushSide( MERGED, BID );
        flushSide( MERGED, ASK );
        // The message contains the current state of the book.
        // iterate over the levels...
        for( size_t level = 0; level < pSnapshot->bidLevels().size(); ++level )
        {
            // apply all the levels. if the size is zero the level will just be cleared
            if( level < pSnapshot->getNumBidLevels() )
            {
                const incremental::book_entry& orderB = pSnapshot->bidLevels()[ level ];
                applyOrderToBook( orderB.level, BID, pSnapshot->hdr->time_sent, pSnapshot->getTimestamp(),
                                  orderB.sz, orderB.px, orderB.num_orders, orderB.type == incremental::LEVEL_IMPLIED );
            }

            if( level < pSnapshot->getNumAskLevels() )
            {
                const incremental::book_entry& orderS = pSnapshot->askLevels()[level];
                applyOrderToBook( orderS.level, ASK, pSnapshot->hdr->time_sent, pSnapshot->getTimestamp(),
                                  orderS.sz, orderS.px, orderS.num_orders, orderS.type == incremental::LEVEL_IMPLIED );
            }

        }

        // reset the last instrument sequence_num to be that of the snapshot
        m_lastInstrSeqnum = pSnapshot->getLastProcessedInstrSeqnum();

        updateMergedView( BID );
        updateMergedView( ASK );
        m_ok_dirty = true;
        m_lastChangeTv = pSnapshot->hdr->time_sent;
        notifyBookChanged( pSnapshot, 0, 0 );
    }
    else
    {
        onEvent( msg );
    }
}


template<typename Traits>
template<typename UpdateMsgT>
void IncrementalBook<Traits>::modifyAction( BookLevelVec& vLevels, uint32_t action, const UpdateMsgT& msg, bool priceCheck)
{
    IncrementalLevelPtr& spBL = vLevels[msg.getLevel()];
    if( !spBL )  // if there isn't a valid level, insert one
    {
        m_isAdd = true;

        // make a new BookLevel
        spBL.reset( new IncrementalLevel( m_src, msg.getSide(), msg.getPx(), 0, msg.getNumOrders() ) );
        spBL->is_implied = msg.getImplied();
        spBL->pushbackOrder( new BookOrder( m_instr, msg.hdr->time_sent, msg.getTimestamp(), ++m_next_bo_seqnum,
                                            msg.getSide(), msg.getPx(), msg.getSz(), MKT_CME, m_src ) );
    }
    else // otherwise, modify existing level
    {
        if( priceCheck && NE( spBL->getPrice(), msg.getPx(), CME_EPSILON ) )
        {
            if( m_verbose >= 1 )
            {
                LOG_INFO << "Incremental Book " << m_desc << "(" << m_instr << ") got a level to modify which does not match the level in book."
                         << " Book Data::Price: " << spBL->getPrice() << " Book Data::Size: " << spBL->getSize()
                         << " Market Data::Price: " << msg.getPx() << " Market Data::Size: " << msg.getSz()
                         << "msg: "<<msg<<bb::endl;
            }
            m_outOfSync = true;
        }

        spBL->setPrice( msg.getPx() );
        spBL->setSize( msg.getSz() );
        spBL->setNumOrders( msg.getNumOrders() );
        spBL->is_implied = msg.getImplied();
        BookOrder* pOrder = spBL->getOrders().front();
        pOrder->px = msg.getPx();
        pOrder->sz = msg.getSz();
        pOrder->tv_exch = msg.getTimestamp();
        pOrder->tv_msg = msg.hdr->time_sent;
        pOrder->idnum = ++m_next_bo_seqnum;
    }

}


template<typename Traits>
template<typename UpdateMsgT>
void IncrementalBook<Traits>::onUpdate( const UpdateMsgT& msg, ENotify notify )
{

    side_t side = msg.getSide();
    BookLevelVec& vLevels = ( msg.getImplied() ? m_impliedLevels[side] : m_outrightLevels[side] );

    BookLevelCPtr victimBL;
    m_isAdd      = false;
    m_outOfSync  = false;
    bool handled     = false;

    //make sure there are enough levels
    if( msg.getImplied() )
        vLevels.resize( 2 );
    else if( msg.getNumLevels() > vLevels.size() )
        vLevels.resize( msg.getNumLevels() );

    if( msg.getLevel() + 1u > vLevels.size() )
        vLevels.resize( msg.getLevel() + 1u );

    uint32_t action = msg.getAction();

    switch( action )
    {
    case Traits::DeleteAction:
    {
        BookLevelVec::iterator victim = vLevels.begin() + msg.getLevel(); // * == spBL
        victimBL = *victim;

        if( victimBL && NE( victimBL->getPrice(),  msg.getPx(), CME_EPSILON ) )
        {
            LOG_INFO << "Incremental Book " << m_desc << "(" << m_instr << ") got a level to remove which does not match the level in book."
                     << " Book Data::Price: " << victimBL->getPrice() << " Book Data::Size: " << victimBL->getSize()
                     << " Market Data::Price: " << msg.getPx() << " Market Data::Size: " << msg.getSz()
                     << bb::endl;

            m_outOfSync = true;
        }

        vLevels.erase( victim );
    }
    break;
    case Traits::AddAction:
    {
        m_isAdd = true;

        size_t num_levels = msg.getNumLevels();


        // make a new BookLevel
        BookLevelVec::iterator victim = vLevels.begin() + msg.getLevel(); // * == spBL
        BookLevelVec::iterator it = vLevels.insert( victim, IncrementalLevelPtr() );

        if( vLevels.size() > num_levels )  // implies deletion of outermost level
        {
            victimBL = vLevels.back();
            vLevels.pop_back();
        }
        if( vLevels.size() > num_levels )
        {
            LOG_WARN << "have " << vLevels.size() << " levels; expected at most " << num_levels << bb::endl;
        }

        it->reset( new IncrementalLevel( m_src, msg.getSide(), msg.getPx(), 0, msg.getNumOrders() ) );
        (*it)->is_implied = msg.getImplied();
        ( *it )->pushbackOrder( new BookOrder( m_instr, msg.hdr->time_sent, msg.getTimestamp(), ++m_next_bo_seqnum,
                                               msg.getSide(), msg.getPx(), msg.getSz(), MKT_CME, m_src ) );
    }
    break;
    case Traits::ModifyAction:
    {
        modifyAction( vLevels, action, msg, true);
    }
    break;
    default:
        handled = handleOtherActions(vLevels, action, msg);
        if( !handled )
        {
            LOG_WARN<<"Unhandled action " << msg.getAction()<<bb::endl;
        }
        break;
    } // end switch(action)

    int32_t bidLevelChanged = msg.getSide() == BID ? msg.getLevel() : -1;
    int32_t askLevelChanged = msg.getSide() == ASK ? msg.getLevel() : -1;

    if( m_outOfSync && msg.getAction() != Traits::DeleteAction )
    {
        fixBooks( msg.getSide(), msg.getPx(), msg.getImplied() );

        // refresh both sides
        updateMergedView( BID );
        updateMergedView( ASK );

        bidLevelChanged = askLevelChanged = 0;
    }
    else
    {
        updateMergedView( side );
    }

    // dirty flag is set to true because we detected a gap based on sequence number.
    if ( m_outOfSync )
    {
        m_ok_dirty = true;
    }

    m_lastChangeTv = msg.hdr->time_sent;

    // If NO_NOTIFY was specified, then skip all book notifications.
    // This is used during recovery to avoid double notifications
    if( NO_NOTIFY == notify ){
        return;
    }

    if( victimBL )
    {
        notifyBookLevelDropped( victimBL.get() );
    }

    switch( action )
    {
    case Traits::AddAction:
        notifyBookLevelAdded( vLevels[msg.getLevel()].get() );
        break;
    case Traits::ModifyAction:
        if( m_isAdd )
            notifyBookLevelAdded( vLevels[msg.getLevel()].get() );
        else
            notifyBookLevelModified( vLevels[msg.getLevel()].get() );
        break;
    case Traits::DeleteAction:
        break;
    default:
        handled = handleOtherActionNotify( vLevels[msg.getLevel()].get(), action);
        if( !handled )
        {
            LOG_WARN << "unsupported book action" << bb::endl;
        }
    }
    notifyBookChanged_View( msg.getImplied() ? IMPLIED : OUTRIGHT, &msg,
                            bidLevelChanged, askLevelChanged );
}

namespace {

struct ValidSharedPtrPredicate
{
    bool operator() ( const IncrementalLevelCPtr & t ) { return t.get() != NULL; }
};

}

template<typename Traits>
bool IncrementalBook<Traits>::fixBooks( side_t trusted_side, double trusted_px, bool implied )
{
    side_t opp_side = ~trusted_side;
    bool error = false;

    typedef boost::filter_iterator
        < ValidSharedPtrPredicate
         , BookLevelVec::iterator>  FilteredIter;

    // On the same side of the book the levels before the trusted book levels should have more aggressive prices
    // If they don't, they are stale and we shall drop them.
    if( !implied )              // outright book
    {
        FilteredIter
            iOutrightCurr( m_outrightLevels[trusted_side].begin(), m_outrightLevels[trusted_side].end() ),
            iOutrightEnd( m_outrightLevels[trusted_side].end(), m_outrightLevels[trusted_side].end() );

        while( iOutrightCurr != iOutrightEnd )
        {
            if( is_less_aggressive( trusted_side, iOutrightCurr->get()->getPrice(), trusted_px ) )
            {
                if( m_verbose >= 1 )
                {
                    LOG_WARN << "dropping outright book level: " << **iOutrightCurr
                             << bb::endl;
                }

                ++iOutrightCurr;
                error = true;
            }
            else
            {
                break;
            }
        }

        m_outrightLevels[trusted_side].erase( m_outrightLevels[trusted_side].begin(), iOutrightCurr.base() );
    }
    else                        // implied book
    {
        FilteredIter
            iImpliedCurr( m_impliedLevels[trusted_side].begin(), m_impliedLevels[trusted_side].end() ),
            iImpliedEnd( m_impliedLevels[trusted_side].end(), m_impliedLevels[trusted_side].end() );

        while( iImpliedCurr != iImpliedEnd )
        {
            if( is_less_aggressive( trusted_side, iImpliedCurr->get()->getPrice(), trusted_px ) )
            {
                if( m_verbose >= 1 )
                {
                    LOG_WARN << "dropping implied book level: " << **iImpliedCurr
                             << bb::endl;
                }

                ++iImpliedCurr;
                error = true;
            }
            else
            {
                break;
            }
        }

        m_impliedLevels[trusted_side].erase( m_impliedLevels[trusted_side].begin(), iImpliedCurr.base() );
    }

    // the book levels on the other side of the implied book and outright book shall not cross with
    // the latest trusted price. If they do drop them.
    // Note: when CME halts the trading the best bid and best ask could be equal.
    FilteredIter
        iOppOutrightCurr( m_outrightLevels[opp_side].begin(), m_outrightLevels[opp_side].end() )
        , iOppOutrightEnd( m_outrightLevels[opp_side].end(), m_outrightLevels[opp_side].end() )
        , iOppImpliedCurr( m_impliedLevels[opp_side].begin(), m_impliedLevels[opp_side].end() )
        , iOppImpliedEnd( m_impliedLevels[opp_side].end(), m_impliedLevels[opp_side].end() );

    while( iOppOutrightCurr != iOppOutrightEnd )
    {
        if( inside( opp_side, iOppOutrightCurr->get()->getPrice(), trusted_px ) )
        {
            if( m_verbose >= 1 )
            {
                LOG_WARN << "dropping outright book level: " << **iOppOutrightCurr
                         << bb::endl;
            }

            ++iOppOutrightCurr;
            error = true;
        }
        else
        {
            break;
        }
    }

    m_outrightLevels[opp_side].erase( m_outrightLevels[opp_side].begin(), iOppOutrightCurr.base() );

    while( iOppImpliedCurr != iOppImpliedEnd )
    {
        if( inside( opp_side, iOppImpliedCurr->get()->getPrice(), trusted_px ) )
        {
            if( m_verbose >= 1 )
            {
                LOG_WARN << "dropping implied book level:" << **iOppImpliedCurr
                         << bb::endl;
            }

            ++iOppImpliedCurr;
            error = true;
        }
        else
        {
            break;
        }
    }

    m_impliedLevels[opp_side].erase( m_impliedLevels[opp_side].begin(), iOppImpliedCurr.base() );

    return error;
}

template<typename Traits>
void IncrementalBook<Traits>::updateMergedView(side_t side)
{
    BookLevelVec & mergedView = m_mergedLevels[side];

    mergedView.clear();
    // We are only concerned with non-NULL levels
    typedef boost::filter_iterator
        < ValidSharedPtrPredicate
         , BookLevelVec::const_iterator>  FilteredIter;

    FilteredIter
        iOutrightCurr( m_outrightLevels[side].begin(), m_outrightLevels[side].end() )
        , iOutrightEnd( m_outrightLevels[side].end(), m_outrightLevels[side].end() )
        , iImpliedCurr( m_impliedLevels[side].begin(), m_impliedLevels[side].end() )
        , iImpliedEnd( m_impliedLevels[side].end(), m_impliedLevels[side].end() );

    while( iOutrightCurr != iOutrightEnd && iImpliedCurr != iImpliedEnd)
    {
        // outright closer to inside market
        if( BookLevelCmp()( iOutrightCurr->get(), iImpliedCurr->get() ) )
        {
            mergedView.push_back( *iOutrightCurr );
            ++iOutrightCurr;
        }
        // implied closer to inside market
        else if( BookLevelCmp()( iImpliedCurr->get(), iOutrightCurr->get() ) )
        {
            mergedView.push_back( *iImpliedCurr );
            ++iImpliedCurr;
        }
        // equal price...oh no
        else
        {

            IncrementalLevelPtr pNewLevel =
                boost::make_shared <IncrementalLevel> ( m_src, side
                                                        , (*iOutrightCurr)->getPrice()
                                                        , (*iOutrightCurr)->getSize() + (*iImpliedCurr)->getSize()
                                                        , -1, true );
            pNewLevel->setMaintainOrderList(false);

            mergedView.push_back ( pNewLevel );
            ++iOutrightCurr;
            ++iImpliedCurr;

        }

    }

    std::copy( iOutrightCurr, iOutrightEnd, std::back_inserter ( mergedView ) );
    std::copy( iImpliedCurr, iImpliedEnd, std::back_inserter ( mergedView ) );

}

// Children should call this when they want to notify their listeners that they have changed.
template<typename Traits>
void IncrementalBook<Traits>::notifyBookChanged_View( EView view, const Msg* pMsg, int32_t bidLevelChanged,
                                      int32_t askLevelChanged ) const
{
    if( m_view == MERGED || m_view == view )
        BookImpl::notifyBookChanged( pMsg, bidLevelChanged, askLevelChanged );
}


// Children should call this when they want to notify their listeners that they have flushed.
template<typename Traits>
void IncrementalBook<Traits>::notifyBookFlushed_View( EView view, const Msg* pMsg ) const
{
    if( m_view == MERGED || m_view == view )
        BookImpl::notifyBookFlushed( pMsg );
}


// invoked when our SourceMonitor status changes
template<typename Traits>
void IncrementalBook<Traits>::onSMonStatusChange(
    source_t src, smon_status_t status, smon_status_t old_status,
    const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv )
{
    m_ok_dirty = true;
    if( status == Stat_SMon_OK )
    {
        m_source_ok = true;
        if( m_verbose >= 1 )
            std::cout << m_desc << ": source is OK again, book will be marked ready" << std::endl;
    }
    else // !Stat_SMon_OK
    {
        m_source_ok = false;
        if( status == Stat_SMon_SourceDown )
        {
            if( m_verbose >= 1 )
                std::cout << m_desc << ": Source is down, flushing book and marking as invalid" << std::endl;
            flushBook();
        }
        else
        {
            if( m_verbose >= 1 )
                std::cout << m_desc << ": Source is having problems, book will be marked invalid until source is ok" <<
                std::endl;
        }
    }
}




/// Checks for holes in a side of the passed BookLevelVec (book half)
/// Returns true if there are holes, false otherwise.
/// If the whole half is invalid, false will be returned.

/// Clarification on what holes imply:
/// Since the Cme sends us messages that specify which level a given update(or add or delete)
/// happened in, having a level that was never defined by a CME message indicates that some portion
/// of the message stream was missed.

/// It is important to remember that the book levels tracked by the book correspond to levels with
/// existing quotes not price increment levels
template<typename Traits>
bool IncrementalBook<Traits>::hasHoles( const IncrementalBook<Traits>::BookLevelVec& blvec ) const
{
    // walk backward through the list...
    // if we've previously found a valid level and come upon an invalid one, return false.
    bool found_valid_level = false;
    BookLevelVec::const_iterator depthIter( blvec.begin() + std::min( blvec.size(), m_holeCheckDepth ) );
    for( BookLevelVec::const_reverse_iterator iter( depthIter ); iter != blvec.rend(); ++iter )
    {
        if( *iter )
            found_valid_level = true;
        else if( found_valid_level )
            return true;
    }
    return false;
}



template<typename Traits>
void  IncrementalBook<Traits>::setHoleCheckDepth( size_t depth )
{
    m_ok_dirty = true;
    m_holeCheckDepth = depth;
}

template<typename Traits>
size_t IncrementalBook<Traits>::getHoleCheckDepth() const
{
    return m_holeCheckDepth;
}


/*************************************************************************************************/
// Book Level Notification Code
/*************************************************************************************************/

class LevelChange
{
public:
    typedef enum { NONE, DROP, ADD, MODIFY } EChange;

    LevelChange( EChange change, BookLevelCPtr spBL )
        : m_change( change )
        , m_spBL( spBL )
        , m_creation_val( ms_next_creation_val++ )
    {}

    // sort by removes, adds, then modifies
    // tie breaker is creation val
    bool operator<( const LevelChange& rhs ) const
    {
        return m_change < rhs.m_change ||
               ( m_change == rhs.m_change && m_creation_val < rhs.m_creation_val );
    }

    EChange getChangeType() const { return m_change; }
    BookLevelCPtr getBookLevel() const { return m_spBL; }
    size_t getCreationVal() const { return m_creation_val; }

private:
    EChange m_change;
    BookLevelCPtr m_spBL;
    size_t m_creation_val;
    static size_t ms_next_creation_val;
};



/// Compares the current viewed book to m_prevViewedLevels, generating the appropriate
/// BookLevel notifications. If only does this if there are visitors.
/// Deeply updates m_prevViewedLevels to be the same as the current view.
template<typename Traits>
void IncrementalBook<Traits>::dispatchLevelNotifications()
{
    if( !m_bookLevelListeners.empty() )
    {
        std::vector<LevelChange> vChanges;
        generateChangesForSide( BID, vChanges );
        generateChangesForSide( ASK, vChanges );

        // sort by removes, adds, then modifies
        std::sort( vChanges.begin(), vChanges.end() );
        // apply the changes
        BOOST_FOREACH( const LevelChange& change, vChanges )
        {
            switch( change.getChangeType() )
            {
            case LevelChange::ADD:    notifyBookLevelAdded( change.getBookLevel().get() ); break;
            case LevelChange::DROP:   notifyBookLevelDropped( change.getBookLevel().get() ); break;
            case LevelChange::MODIFY: notifyBookLevelModified( change.getBookLevel().get() ); break;
            default: break;
            }
        }
    }

    // Deeply updates m_prevLevels to be the same as the current view.
    for( size_t side = 0; side < 2; ++side )
    {
        m_prevViewedLevels[side].clear();
        for( BookLevelVec::const_iterator iter = m_viewedLevels[side]->begin(); iter != m_viewedLevels[side]->end();
             ++iter )
            m_prevViewedLevels[side].push_back( *iter ? IncrementalLevelPtr( new IncrementalLevel( **iter ) ) : IncrementalLevelPtr() );
    }
}


/// Generates the LevelChanges for the given side.  Helper for generateNotifications.
template<typename Traits>
void IncrementalBook<Traits>::generateChangesForSide( side_t side, std::vector<LevelChange>& vChanges ) const
{
    size_t col_olds = 0, col_news = 0;    // indices into the arrays
    const BookLevelVec& oldLevels = m_prevViewedLevels[side];
    const BookLevelVec& newLevels = *( m_viewedLevels[side] );
    const size_t count_olds = oldLevels.size();
    const size_t count_news = newLevels.size();

    // Keep iterating until we are done going over both the old and new arrays
    while( ( col_olds < count_olds ) || ( col_news < count_news ) )
    {
        // if we are done iterating newVals, that means that the rest of the oldVals are CANCELs
        if( col_news >= count_news )
        {
            const BookLevelCPtr& spOldBL = oldLevels[col_olds];
            if( !spOldBL )
            {
                col_olds++;
                continue;
            }
            vChanges.push_back( LevelChange( LevelChange::DROP, spOldBL ) );
            col_olds++;
            continue;   // loop again
        }

        // fetch the new price and size
        const BookLevelCPtr& spNewBL = newLevels[col_news];
        if( !spNewBL )
        {
            col_news++;
            continue;
        }

        // if we are done iterating oldVals, that means that the rest of the newVals are adds
        if( col_olds >= count_olds || !oldLevels[col_olds] )
        {
            vChanges.push_back( LevelChange( LevelChange::ADD, spNewBL ) );
            col_news++;
            continue;   // loop again
        }

        const BookLevelCPtr& spOldBL = oldLevels[col_olds];
        // fetch the old price and size
        double new_price = spNewBL->getPrice();
        int32_t new_size = spNewBL->getSize();
        double old_price = spOldBL->getPrice();
        int32_t old_size = spOldBL->getSize();

        // If current new price is the current price, then we will either modify (if size is different)
        // or be unchanged.  Either way, we move to the next column for both
        if( bb::EQ( new_price, old_price ) )
        {
            if( new_size != old_size )
                vChanges.push_back( LevelChange( LevelChange::MODIFY, spNewBL ) );
            col_news++;
            col_olds++;
            continue;   // loop again
        }

        // if the prices are uneven, then what we do depends on whether
        // we are dealing with bids or asks
        //      asks are ordered lowest  to highest  (best = lowest)
        //      bids are ordered highest to lowest   (best = highest)
        // if the old_price is "better" than the new_price, then we need to CANCEL the old
        // otherwise we need to ADD the new
        // if if the old price is less than the new price, then the old price is stale!
        if( ( side == bb::BID && bb::GT( old_price, new_price ) ) ||
            ( side == bb::ASK && bb::LT( old_price, new_price ) ) )
        {
            vChanges.push_back( LevelChange( LevelChange::DROP, spOldBL ) );
            col_olds++;
        }
        else
        {
            vChanges.push_back( LevelChange( LevelChange::ADD, spNewBL ) );
            col_news++;
        }
    }
}


template<typename Traits>
boost::optional<uint32_t> IncrementalBook<Traits>::getLastProcessedInstrSeqnum() const
{
    return m_lastInstrSeqnum;
}

template<typename Traits>
void IncrementalBook<Traits>::startRecovery( uint32_t msg_dropped )
{
    // marked ok as dirty and store the number of messages dropped since last recovery
    m_ok_dirty = true;
    m_updatesMissedSinceLast += msg_dropped;

    // If recovery is disabled, or recovery is already being performed
    // return
    if( m_recoveryMgr ){
        return;
    }
    // call the user callback if set
    if( m_allowRecoveryCB ){
        // The callback takes as input the number of seconds since the last recovery
        // and the number of messages dropped since last recovery.
        // It returns a boolean value. If false, then
        // recovery is skipped.
        if( !m_allowRecoveryCB( *this, m_lastRecoveryTime, m_updatesMissedSinceLast ) )
        {
            ++m_recoverySkipped;
           return;
        }
    }
    initRecovery();
}

template<typename Traits>
void IncrementalBook<Traits>::setAllowRecoveryCallback( AllowRecoveryCallback cb )
{
    m_allowRecoveryCB = cb;
}

template<typename Traits>
void IncrementalBook<Traits>::setRecoveryCompleteCallback( RecoveryCompleteCallback cb )
{
    m_recoveryCompleteCB = cb;
}

template<typename Traits>
void IncrementalBook<Traits>::initRecovery()
{
    ++m_recoveryNum;

    ClientContextPtr cc = m_wpCC.lock();
    if( !cc ){
        BB_THROW_ERROR( "ClientContextPtr is bad" );
    }

    // Create a new recovery manager
    m_recoveryMgr.reset( new RecoveryMgr( cc, *this,
                                          boost::bind( &IncrementalBook<Traits>::onRecoveryEvent, this, _1),
                                          boost::bind( &IncrementalBook<Traits>::onRecoveryComplete, this) ) );

    // clear counters
    m_updatesMissedSinceLast = 0;
    m_lastRecoveryTime = timeval_t::now;
}

template<typename Traits>
void IncrementalBook<Traits>::onRecoveryComplete()
{
    // recovery is complete delete the recovery manager
    m_recoveryMgr.reset();

    // clear the counters and mark ok as dirty
    m_ok_dirty = true;
    m_updatesMissedSinceLast = 0;

    // call the user callback if set
    if( m_recoveryCompleteCB )
    {
        m_recoveryCompleteCB(*this);
    }
    LOG_WARN << "Recovery Complete for "<< m_instr << bb::endl;;
}


} // namespace bb
#endif // BB_CLIENTCORE_INCREMENTALBOOK_H
