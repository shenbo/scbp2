#ifndef BB_CLIENTCORE_CMEBOOKRECOVERYMANAGER_H
#define BB_CLIENTCORE_CMEBOOKRECOVERYMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */
#include <boost/optional.hpp>
#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/clientcore/IEventDistListener.h>

class CmeBookRecoveryManagerFixture;
namespace bb
{
BB_FWD_DECLARE_SHARED_PTR( CmeFastDepthMsg );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( CmeRecoveryBookBaseMsg );
class CmeBook;
class Msg;
class CmeSnapshotRecoveryBook;
class instrument_t;
}

namespace cme
{
class RecoveryManager : public bb::IEventDistListener
{
public:
    enum State
    {
        RECOVERING,
        SUCCESS,
        ERROR,
    };

    typedef boost::function<void()> SuccessCallback;
    typedef boost::function<void()> FailureCallback;

    RecoveryManager( bb::CmeBook& target,
                     SuccessCallback success = SuccessCallback(),
                     FailureCallback fail = FailureCallback() );
    virtual ~RecoveryManager();

    State getState() const;
    void setState( State s );

    virtual void onEvent( const bb::Msg& msg );
    void onRecoveryEvent( const bb::Msg& msg );

    bb::instrument_t getInstr() const;
    bb::source_t getSource() const;
    bb::CmeBook& getTarget();

protected:
    void queueMessage(const bb::CmeFastDepthMsg * bookMsg);
    void processMessage(const bb::CmeFastDepthMsg * bookMsg);
private:
    void handleErrorState();
    void flushQueue();
    void reset();
private:
    bb::CmeBook& m_target;
    State m_state;

    std::vector<bb::CmeFastDepthMsgPtr> m_queue;
    boost::optional<uint32_t> m_currentRecoveredSeqNum;

    SuccessCallback m_successCB;
    FailureCallback m_failCB;

    uint32_t m_start_seqNum;

    struct Stats
    {
        Stats() { reset(); }
        void reset() { m_msgsDiscarded = m_msgsQueued = m_msgsRecovery = m_numSuccess = m_numFail =  0; }

        uint32_t m_msgsRecovery, m_msgsQueued, m_msgsDiscarded, m_numSuccess, m_numFail;
    } m_stats;

    friend class ::CmeBookRecoveryManagerFixture;
};


class CmeSnapshotRecoveryManager:
        public bb::IEventDistListener
{
public:
    typedef boost::function<void()> RecoveryCompleteCallback;

    CmeSnapshotRecoveryManager( bb::ClientContextPtr cc, bb::CmeBook& target,
                                RecoveryCompleteCallback completeCB);
    virtual ~CmeSnapshotRecoveryManager();

    virtual void onEvent( const bb::Msg& msg );

private:
    void flushVector( uint32_t recoverdSeqnum );

    void cleanup();

    RecoveryCompleteCallback                       m_completeCallback;
    bb::CmeBook&                                   m_book;
    uint32_t                                       m_lastRecvSeqnum;
    bb::instrument_t                               m_instr;
    bb::Subscription                               m_eventSub;
    std::vector<bb::CmeRecoveryBookBaseMsgPtr>     m_msgVec;
};
} // namespace cme

#endif // BB_CLIENTCORE_CMEBOOKRECOVERYMANAGER_H
