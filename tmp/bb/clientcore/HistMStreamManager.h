#ifndef BB_CLIENTCORE_HISTMSTREAMMANAGER_H
#define BB_CLIENTCORE_HISTMSTREAMMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <set>
#include <vector>
#include <boost/optional.hpp>
#include <bb/core/gdate.h>
#include <bb/core/ptime.h>
#include <bb/core/MStream.h>
#include <bb/core/InstrSource.h>
#include <bb/clientcore/MStreamManager.h>
#include <bb/io/ByteSourceFactory.h>
#include <bb/threading/ThreadPool.h>

namespace bb {

class Msg;
BB_FWD_DECLARE_SHARED_PTR(DFStreamMplex);
BB_FWD_DECLARE_SHARED_PTR(IHistMStream);
BB_FWD_DECLARE_SHARED_PTR(SourceTradingDate);

/// Implementation of MStreamManager for historical contexts.
///
/// Does not implement addDataByMType correctly at the moment.
class HistMStreamManager
    : public IMStreamManager
{
public:
    HistMStreamManager( const timeval_t &startTv, const timeval_t &endTv );

    virtual ~HistMStreamManager() {}

    /// Adds the data file stored at strPathname to the HistMStreamManager.
    virtual void addFile( const std::string& strPathname );
    /// Adds data file (IHistMStream overload). The IHistMStream is owned by the HistMStreamManager.
    virtual void addFile( IHistMStream *stream );

    /// Injects a single message into the HistMStreamManager's MStream,
    /// in the proper time-sorted position.
    /// This Msg is cloned, so there are no issues concerning ownership.
    virtual void inject( const Msg& msg );

    /// Returns the DFStreamMplex used internally by the HistMStreamManager.
    /// With great power comes great responsibility.
    DFStreamMplexPtr getDFStreamMplex();

    /// Returns the specified start timeval of this Stream.
    timeval_t getStartTimeval() const;
    /// Returns the specified end timeval of this Stream.
    timeval_t getEndTimeval() const;
    /// Add date to skip when retrieving new sym files
    void addExcludeDate( const gdate_t& d );


    /// Returns which network this run should be simulated to take place in.
    static boost::optional<EFeedDest> getRuntimeFeedDestOpt();
    static EFeedDest getRuntimeFeedDest(); // returns a default value if not set.

    // DEPRECATED! use simulator::enableTunnelDelays or HistMStreamManager::enableSimulatedTunnelDelays
    static void setRuntimeFeedDest( EFeedDest dest );

    static void enableSimulatedTunnelDelays();

    void setByteSourceFactory( ByteSourceFactoryPtr byteSourceFactory );

    /// Returns a simulated delay needed to transport data for the given feed to
    /// the RuntimeFeedDest.
    virtual boost::optional<double> getFeedDelaySecsFromScript( const source_t& feed, mktdest_t mkt );

    /// Returns a list of sources with origin wildcard expanded
    static std::vector<source_t> expandOriginWildcardFromScript( const source_t& source );

    /// Override the value in core_config
    void setLogMissingFiles( bool value );
    bool getLogMissingFiles() const;
    void setIgnoreMissingFiles( bool value );
    bool getIgnoreMissingFiles() const;
    void setLogAddDataByMType( bool value );
    bool getLogAddDataByMType() const;
    void setEnableAddDataByMType( bool value );
    bool getEnableAddDataByMType() const;

    bool getLogAddFeeds() const { return m_logFeeds; }

    /// convenience method for use when the stream is not the main loop
    /// this allows us to get the next queued message from the stream
    const Msg* next();

    // IMStreamManager
    virtual void run( IMStreamCallback *c );
    virtual void exit();
    virtual void sigAction( int sig, const UnixSigCallback &cb );

    virtual void addDataByMTypeInstr( const source_t&, mtype_t, const instrument_t& );
    virtual void addDataByMType( const source_t&, mtype_t );

    void setNextTimeout(const ptime_duration_t &timeout);

protected:
    bool isSubscribed( const source_t&, const instrument_t& );
    void addSubscription( const source_t&, const instrument_t& );

    timeval_t                       m_startTv, m_endTv;
    boost::optional<EFeedDest>      m_runtimeFeedDest;
    bool                            m_logFeeds,
                                    m_ignoreMissingFiles,
                                    m_logAddDataByMType,
                                    m_enableAddDataByMType,
                                    m_logMissingFiles;

private:
    HistMStreamPtr getSourceWrappedDFStream( HistMStreamPtr baseMStream, const source_t source, date_t runDate );

    typedef std::pair<symbol_t, source_t> SymbolSourcePair;

    std::vector<SymbolSourcePair>   m_vSymbolSources;
    DFStreamMplexPtr                m_spDFStreamMplex;
    std::set<source_t>              m_sources;
    SourceTradingDatePtr            m_spSourceTradingDate;
    ByteSourceFactoryPtr            m_byteSourceFactory;
};

BB_DECLARE_SHARED_PTR( HistMStreamManager );

// Variant of MStreamManager which loads unsplit files from /nfs/datafiles.logdir.
class UnsplitMStreamManager : public HistMStreamManager
{
public:
    UnsplitMStreamManager( const timeval_t &startTv, const timeval_t &endTv )
        : HistMStreamManager( startTv, endTv ) {}

    virtual void addDataByMTypeInstr( const source_t&, mtype_t, const instrument_t& );
    virtual void addDataByMType( const source_t&, mtype_t );

private:
    std::set<source_t> m_sources;
};

// Variant of MStreamManager which allows the user to specify which sources are loaded from split
// files and which are loaded from unsplits.
class ConfigurableMStreamManager : public UnsplitMStreamManager
{
    typedef boost::function<bool(source_t)> UseSplitCallback;

public:
    ConfigurableMStreamManager( const timeval_t &startTv, const timeval_t &endTv, const UseSplitCallback& callback )
        : UnsplitMStreamManager( startTv, endTv )
        , m_useSplitCallback( callback )
    {}

    virtual void addDataByMTypeInstr( const source_t&, mtype_t, const instrument_t& );
    virtual void addDataByMType( const source_t&, mtype_t );

private:
    const UseSplitCallback              m_useSplitCallback;
};

// Variant of MStreamManager which allow user to specify a list of data files to load.
// All the files to be used should be added to a set<string> and pass it to the constructor.
// This class is useful when user want to load data from local disk, instead of loading from /nfs or /cache.
class FileSetMStreamManager : public HistMStreamManager
{
public:
    FileSetMStreamManager( const timeval_t &startTv, const timeval_t &endTv, const std::set<std::string>& fileset );
    virtual void addDataByMTypeInstr( const source_t& src, mtype_t type, const instrument_t&  instr);
    virtual void addDataByMType( const source_t& src, mtype_t type );

};
BB_DECLARE_SHARED_PTR( FileSetMStreamManager );


///////////////////////////////////////////////////////////////////////////////////////////////////
//
// Inline implementation
//
///////////////////////////////////////////////////////////////////////////////////////////////////

inline DFStreamMplexPtr HistMStreamManager::getDFStreamMplex() { return m_spDFStreamMplex; }

} // namespace bb

#endif // BB_CLIENTCORE_HISTMSTREAMMANAGER_H
