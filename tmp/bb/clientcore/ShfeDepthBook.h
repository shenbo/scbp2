#ifndef BB_CLIENTCORE_SHFEDEPTHBOOK_H
#define BB_CLIENTCORE_SHFEDEPTHBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/compat.h>
#include <bb/core/messages.h>

#include <bb/clientcore/BookSpec.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/IClientTimer.h>
#include <bb/clientcore/L2Book.h>

namespace bb {

class ClientContext;

/// The ShfeL2DepthBook uses our per-level messages to construct its own view of the book.
/// The messages themselves are simple PriceSize level updates, where a 0-size
/// implies removal.
class ShfeDepthBook
    : public L2BookBase
{
public:
    ShfeDepthBook( const ClientContextPtr& context, const instrument_t& instrument, source_t source,
        const SourceMonitorPtr& monitor, const char* description, int verbose = 0 );

    virtual ~ShfeDepthBook();

    void onEvent( const Msg& msg ) override;

    std::pair<long, long> getBookCheckOffset();

private:
    void onEvent( const ShfeDepthRefreshCompleteMsg & msg );
    void onEvent( const ShfeDepthUpdateMsg & msg );
    void onEvent( const ShfeDepthClearMsg & msg );
    void onBatchTimeout();

    enum level_activity
    {
        ADD,
        MODIFY,
        DELETE,
        NO_CHANGE
    };
    typedef std::pair<int32_t, level_activity> update_result_t;
    update_result_t onUpdate( const ShfeDepthUpdateMsg & update );

    // We will first receive all bid updates for all our instruments, then all ask updates for all instruments.
    // This will mean that for a couple of milliseconds the book will be inconsistent and we don't like that.
    //
    // We solve this by invalidating all ask levels that can't exist anymore given our latest best_bid price.
    // That is, ask levels that have a price that's lower than the current highest bid price. We will notify the
    // book level listener that these levels have been dropped, we will dispose the levels themselves and remove
    // them from the book. We will also claim that the first ask level to change is at index 0.
    //
    // It's also possible that we need to do this for the bid, and the logic follows the same reasoning.
    template<bb::side_t side>
    bool tryInvalidateSide ()
    {
        if ( unlikely ( bhalf[~side].empty() ) ) return false;
        const double best_price_other_side ( (*bhalf[~side].begin())->getPrice() );

        // if there's at least one level to be removed, we'll return true to signal that the top level of this
        // side has changed. We'll also remove all levels that aren't applicable anymore.
        if ( unlikely ( !bhalf[side].empty() &&
                    ( side == bb::ASK ?
                      bb::LE ( (bhalf[side].front())->getPrice(), best_price_other_side ) :
                      bb::GE ( (bhalf[side].front())->getPrice(), best_price_other_side ) )
                    ) )
        {
            bookhalf_t::iterator range_begin = bhalf[side].begin();
            bookhalf_t::iterator iter ( range_begin );
            while ( iter != bhalf[side].end() &&
                    ( side == bb::ASK ?
                    bb::LE ( (*iter)->getPrice(), best_price_other_side ) :
                    bb::GE ( (*iter)->getPrice(), best_price_other_side ) ) )
            {
                BookLevel * lvl ( *iter );
                notifyBookLevelDropped( lvl );
                delete ( lvl );
                iter++;
            }
            BB_ASSERT ( std::distance ( range_begin, iter ) < 1000 );
            BB_ASSERT ( iter != range_begin );
            bhalf[side].erase( range_begin, iter );
            m_updateBatchStats.topLevelsChanged[side] = 0;
            return true;
        }
        return false;
    }

protected:
    // Subscription token for EventDistributor
    std::list<EventSubPtr> m_subscriptions;

    // The - updates batch has started and we are still waiting for the shfe depth refresh complete message - timeout subscription
    std::vector<EventSubPtr> m_timeoutSubscription;

    // We will use the client timer to make sure we'll still update the book, even if
    // we somehow miss the 'shfe depth refresh complete' multicast message informing us that the update
    // is complete.
    IClientTimerPtr m_clientTimer;

    // Instrument the book is tracking
    const instrument_t& m_instrument;

    // Verbosity level
    const int m_verbose;

    // If we've started receiving data and after 'this timeout' we still haven't received
    // the shfe_depth_refresh_complete message, we'll start trigger the 'notifyBookChanged' callback
    // anyway.
    const ptime_duration_t m_batchTimeout;

    struct update_batch_stats_t
    {
        update_batch_stats_t ( )
        {
            reset();
        }

        void reset()
        {
            topLevelsChanged[0] = topLevelsChanged[1] = -1;
            processingBatch = false;
        }

        bool amProcessing() const
        {
            return processingBatch;
        }

        // if this is the start of a new processing batch, set the boolean flag accordingly.
        bool setProcessing()
        {
            if ( unlikely ( !processingBatch ) )
            {
                processingBatch = true;
                return true;
            }
            else
            {
                return false;
            }
        }
        int topLevelsChanged[2];
        bool processingBatch;
    };
    update_batch_stats_t m_updateBatchStats;

};

/// BookSpec corresponding to an Shfe book
class ShfeDepthBookSpec : public BookSpecCommon
{
public:
    BB_DECLARE_SCRIPTING();

    ShfeDepthBookSpec() : m_source(SRC_UNKNOWN) {}
    ShfeDepthBookSpec(const bb::ShfeDepthBookSpec&, const boost::optional<bb::InstrSubst>&);
    ShfeDepthBookSpec(const instrument_t &instr, source_t src)
        :  BookSpecCommon(instr), m_source(src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual ShfeDepthBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    source_t m_source;
};
BB_DECLARE_SHARED_PTR(ShfeDepthBookSpec);


} // namespace bb

#endif // BB_CLIENTCORE_SHFEDEPTHBOOK_H
