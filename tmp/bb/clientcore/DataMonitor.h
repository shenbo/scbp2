#ifndef BB_CLIENTCORE_DATAMONITOR_H
#define BB_CLIENTCORE_DATAMONITOR_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <bb/core/ptime.h>
#include <bb/core/messages.h>
#include <bb/core/EventPublisher.h>
#include <boost/optional.hpp>


namespace bb {


BB_FWD_DECLARE_SHARED_PTR( EventDistributor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class IDataMonitorListener : public bb::IEventSubscriber
{
public:
    virtual bool onSourceOutage( const source_t& source, float msgInterval, float msgIntervalThreshold ){ return true; }
    virtual bool onSourceRecover( const source_t& source, float msgInterval, float srcRecoverThreshold ) {return true;}
};

typedef EventPublisher<IDataMonitorListener> DataMonitorPublisher;

struct SourceOutageNotifier : public DataMonitorPublisher::INotifier
{
    SourceOutageNotifier( IDataMonitorListener* callback )
        : DataMonitorPublisher::INotifier ( callback ) { }

    bool notify( const source_t& source, float msgInterval, float msgIntervalThreshold )
    {
        return !m_callback->onSourceOutage( source, msgInterval, msgIntervalThreshold );
    }
};


struct SourceRecoverNotifier : public DataMonitorPublisher::INotifier
{
    SourceRecoverNotifier( IDataMonitorListener* callback )
        : DataMonitorPublisher::INotifier ( callback )
    {
    }

    bool notify( const source_t& source, float msgInterval, float msgIntervalThreshold)
    {
        return !m_callback->onSourceRecover( source, msgInterval, msgIntervalThreshold);
    }
};



struct DataMonitorID
{
    DataMonitorID(EFeedType feed_,  boost::optional<mktdest_t> mkt_ = boost::optional<mktdest_t>())
        : feed( feed_ ), mkt( mkt_ )
    {
    }

    bool operator< ( const DataMonitorID& rhs) const
    {
        if( likely( feed != rhs.feed ) ) {
            return feed < rhs.feed;
        }
        else {
            const mktdest_t lhsMkt = mkt ? mkt.get() : MKT_UNKNOWN;
            const mktdest_t rhsMkt = rhs.mkt? rhs.mkt.get() : MKT_UNKNOWN;
            return lhsMkt < rhsMkt;
        }
    }

    const EFeedType feed;
    const boost::optional<mktdest_t> mkt;
};

std::ostream& operator << (std::ostream &out, const DataMonitorID& id);

class DataMonitor
{
public:

    typedef enum {
        SOURCE_STAT_UP,
        SOURCE_STAT_DOWN
    }SourceStat_t;

    typedef enum
    {
        MONITOR_ALWAYS = 0,
        MONITOR_DURING_TRADING_HOURS_ONLY = 1
    }MonitorScope_t;

    DataMonitor(const EventDistributorPtr& eventDist, EFeedType feed, boost::optional<mktdest_t> mkt, MonitorScope_t scope);

    virtual ~DataMonitor(){}

    void addDataMonitorListener( IDataMonitorListener* );
    FeedStatsMsg& getSourceOutageThreshold() { return m_sourceOutageThreshold; } // gives access to setters
    FeedStatsMsg& getSourceRecoverThreshold() { return m_sourceRecoverThreshold; } // gives access to setters

    //timeWindow will change the way how we detect the source recover
    //by default, timeWindow size is 0 and we think source is recovered once latest msg interval back to normal
    //if time window is set, we think the source is recovered only if the msg intervals are consistently good
    //during this time window
    void setSourceRecoverWindowSize( const bb::ptime_duration_t timeWindow ) {
        m_recoverWindowSize = timeWindow;
    };

    const SourceStat_t getSourceState() const { return m_sourceState; }

protected:
    void onFeedStatMsg( const FeedStatsMsg& msg );
    void checkMsgInterval( const FeedStatsMsg& msg );

private:
    const MonitorScope_t     m_monitorScope;
    const bb::EFeedType      m_feed;
    const boost::optional<mktdest_t> m_marketOpt;
    MsgHandlerPtr             m_spDataMonitorMsgHandler;
    DataMonitorPublisher      m_publisher;
    FeedStatsMsg              m_sourceOutageThreshold;
    FeedStatsMsg              m_sourceRecoverThreshold;

    bb::ptime_duration_t      m_recoverWindowSize;
    boost::optional<bb::timeval_t> m_recoverWindowStart;
    SourceStat_t              m_sourceState;

    friend class TestDataMonitor;
};
BB_DECLARE_SHARED_PTR(DataMonitor);
}

#endif // BB_CLIENTCORE_DATAMONITOR_H
