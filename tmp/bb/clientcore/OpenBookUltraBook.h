#ifndef BB_CLIENTCORE_OPENBOOKULTRABOOK_H
#define BB_CLIENTCORE_OPENBOOKULTRABOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/L2Book.h>
#include <bb/clientcore/SourceBooks.h>
#include <bb/clientcore/MsgHandler.h>

namespace bb {

class NyseOpenbookUltraAddMsg;
class NyseOpenbookUltraRemMsg;
class NyseOpenbookUltraExeMsg;
class NyseOpenbookUltraUpdateMsg;

class OpenBookUltraBook : public L2Book
{
public:
    OpenBookUltraBook( const ClientContextPtr& context, const instrument_t& instr, source_t src
        , SourceMonitorPtr sm, const std::string& desc, int _vbose=0, bool cross_allowed=false);
    virtual ~OpenBookUltraBook() {}

    bool isCrossAllowed() const { return m_cross_allowed; }

protected:
    virtual std::pair<long, long> getBookCheckOffset();

protected:
    void onNyseOpenbookUltraAddMsg( const NyseOpenbookUltraAddMsg& msg );
    void onNyseOpenbookUltraRemMsg( const NyseOpenbookUltraRemMsg& msg );
    void onNyseOpenbookUltraExeMsg( const NyseOpenbookUltraExeMsg& msg );
    void onNyseOpenbookUltraUpdateMsg( const NyseOpenbookUltraUpdateMsg& msg );

    void updateBookLevel( const Msg* msg, const timeval_t& exch_time
        , side_t s, double price, uint32_t size, uint32_t num_orders );
    void handleCross( side_t side, double price );

protected:
    MsgHandlerPtr m_subAdd;
    MsgHandlerPtr m_subExe;
    MsgHandlerPtr m_subRem;
    MsgHandlerPtr m_subUpdate;
    bool          m_cross_allowed;
};


/// BookSpec corresponding to an OpenBookUltra book
class OpenBookUltraBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    OpenBookUltraBookSpec() : SourceBookSpec(), m_cross_allowed(false) {}
    OpenBookUltraBookSpec(const bb::OpenBookUltraBookSpec&, const boost::optional<bb::InstrSubst>&);
    OpenBookUltraBookSpec(const instrument_t &instr, source_t src, bool cross_allowed = false)
        : SourceBookSpec(instr, src)
        , m_cross_allowed(cross_allowed)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    //virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual OpenBookUltraBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    //virtual void getDataRequirements(IDataRequirements *rqs) const;

    bool m_cross_allowed;
};
BB_DECLARE_SHARED_PTR(OpenBookUltraBookSpec);


} // namespace bb


#endif // BB_CLIENTCORE_OPENBOOKULTRABOOK_H
