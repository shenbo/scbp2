#ifndef BB_CLIENTCORE_MULTIPATHL1BOOK_H
#define BB_CLIENTCORE_MULTIPATHL1BOOK_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/optional.hpp>

#include <bb/core/hash_map.h>
#include <bb/core/sourceset.h>
#include <bb/clientcore/L1Book.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
BB_FWD_DECLARE_SHARED_PTR( RandomSource );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );

/// A MultipathL1Book presents the first arrival of its component subbooks
/// The subbooks are essentially the same. They are just being transferred via different paths
///
/// MultipathL1Book doesn't directly listen to any data feeds. Instead, it gets
/// notified whenever one of it's component books changes.
class MultipathL1Book
    : public L1Book
    , protected IBookListener
{
public:
    MultipathL1Book( const ClientContextPtr& cc, const instrument_t& instr, const char* desc, int _vbose );

    virtual ~MultipathL1Book();

    /// Adds spSubBook to MB's list as the book for spSubBook->getSource()
    /// and stores spSMon as the SourceMonitor for spSubBook->getSource()
    void addSubBook( IBookPtr spSubBook, SourceMonitorPtr spSMon );

    /// Removes spSubBook from MB's list as the book for spSubBook->getSource()
    /// and remove spSMon as the SourceMonitor for spSubBook->getSource().
    /// Removes all BookOrders derived from that Book and invokes listener's onBookChanged.
    void removeSubBook( IBookPtr spSubBook, SourceMonitorPtr spSMon );

    /// Returns the the subbook associated with src, or an invalid shared_ptr if none exists
    IBookPtr getSubBook( source_t src );
    /// Returns the the subbook associated with src, or an invalid shared_ptr if none exists
    IBookCPtr getSubBook( source_t src ) const;

    /// returns the sources of the books monitored by this MultipathL1Book.
    const std::vector<source_t>& getSources() const;

    boost::optional<double> getLastTradePrice() const { return m_opLastTradePrice; }
    boost::optional<double> getTurnover() const { return m_opTurnover; }
    boost::optional<int64_t> getTotalVolume() const { return m_opTotalVolume; }
    boost::optional<double> getOpenInterest() const { return m_opOpenInterest; }

protected:
    /// Invoked when the subscribed Book is flushed.
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

    /// Invoked when the subscribed Book changes.
    /// The levelChanged entries are negative if there is no change, or a 0-based depth.
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );

    virtual bool isOK() const;

    virtual double getMidPrice() const
    { return getMoreUpdatedBook()->getMidPrice(); }

    virtual timeval_t getLastChangeTime() const
    { return getMoreUpdatedBook()->getLastChangeTime(); }

    virtual PriceSize getNthSide( size_t depth, side_t side ) const
    { return getMoreUpdatedBook()->getNthSide( depth, side ); }

    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const
    { return getMoreUpdatedBook()->getBookLevelIter( side ); }

    virtual size_t getNumLevels( side_t side ) const
    { return getMoreUpdatedBook()->getNumLevels( side ); }

private:
    IBookPtr getMoreUpdatedBook() const;

    void pegSourceCounter( const bb::source_t& source );
    void printSourceCounter() const;

    void recordTickInfo( const Msg *pMsg );

protected:
    MultipathL1Book();

    timeval_t m_lastExchangeTv;

    // indexed by src
    typedef bbext::hash_map<source_t, uint32_t> SourceCounterMap;
    SourceCounterMap m_sourceCounter;

    std::map<source_t, IBookPtr> subbooks;
    std::map<source_t, SourceMonitorPtr> smons;
    std::vector<source_t>   m_sources;

    boost::optional<double> m_opLastTradePrice;
    boost::optional<double> m_opTurnover;
    boost::optional<int64_t> m_opTotalVolume;
    boost::optional<double> m_opOpenInterest;
};

BB_DECLARE_SHARED_PTR( MultipathL1Book );

/// spec for building a master book
class MultipathL1BookSpec : public BookSpecCommon
{
public:
    BB_DECLARE_SCRIPTING();

    MultipathL1BookSpec() {}
    MultipathL1BookSpec(const instrument_t &instr, const sourceset_t &sources)
        : BookSpecCommon(instr), m_sources(sources) {}
    MultipathL1BookSpec(const MultipathL1BookSpec &a, const boost::optional<InstrSubst> &instrSubst);

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual MultipathL1BookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    sourceset_t m_sources;
};
BB_DECLARE_SHARED_PTR(MultipathL1BookSpec);


} // namespace bb

#endif // BB_CLIENTCORE_MULTIPATHL1BOOK_H
