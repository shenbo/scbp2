#ifndef BB_CLIENTCORE_MSGPRINTER_H
#define BB_CLIENTCORE_MSGPRINTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/MsgPrintFilter.h>
#include <bb/clientcore/IEventDistListener.h>

namespace bb {

/// MsgPrinter is a service which can hook into an EventDistributor
/// and cout the BB messages which are coming through a program.
///
/// The core_config var bb.msgPrint determines what is logged.
/// Possible values:
///
/// bb.msgPrint = printNoMessages
/// bb.msgPrint = printAllMessages
/// bb.msgPrint = printMTypes{MSG.ISLD_EXE}

class MsgPrinter : public IEventDistListener, public boost::static_visitor<void>
{
public:
    static MsgPrintFilter loadFilter(); // returns the filter defined in core_config.

    MsgPrinter(EventDistributorPtr ed, const MsgPrintFilter &filter);

    virtual void onEvent( const Msg& msg );

    // visitor for MsgPrintFilter
    void operator()(const msg_print_filters::NoMessages &filt);
    void operator()(const msg_print_filters::AllMessages &filt);
    void operator()(const msg_print_filters::MTypes &filt);

protected:
    EventDistributorPtr m_eventDist;
    std::vector<EventSubPtr> m_eventSubs;
};
BB_DECLARE_SHARED_PTR(MsgPrinter);

} // namespace bb

#endif // BB_CLIENTCORE_MSGPRINTER_H
