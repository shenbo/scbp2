#ifndef BB_TRADING_TRADINGSETUP_H
#define BB_TRADING_TRADINGSETUP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <bb/core/acct.h>
#include <bb/core/env.h>
#include <bb/clientcore/ClientCoreSetup.h>
#include <bb/trading/trading.h>
#include <bb/trading/TradingContextFactory.h>

namespace bb {
namespace trading {

class Limits;
BB_FWD_DECLARE_SHARED_PTR(TradingContext);

class DefaultTradingContextFactory : public ITradingContextFactory
{
public:
    virtual TradingContextPtr create( const TradingSetup* setup );
};

class TradingSetup : public virtual ClientCoreSetup
{
public:
    typedef bbext::hash_map<instrument_t, std::string> TradeConfigMap;

public:
    TradingSetup( int argc, char** argv, const ITradingContextFactoryPtr& f = ITradingContextFactoryPtr() );
    virtual ~TradingSetup();

    /// Creates TradingContext, either Hist or Live depending on runlive.
    virtual void setup();
    /// Call this instead of setup() if you have already called ClientCoreSetup::setup().
    void setupAfterClientCore();

    /// Does not call any parent class version of setupSymbols.
    /// Uses the addDataSymbol() method of parent,
    /// which ensures most of this compatibility matching.
    /// Does not create any Trader objects at this time. Instead,
    /// remembers syms to trade, then later when setupTrading() is called
    /// they will be created and configured.
    /// Throws std::runtime_error if any part fails.
    /// Returns true if no throw.
    virtual bool setupSymbols();

    /// Creates Trader. Passes IDstring to objects that want it.
    bool setupTrading();

    /// Start the event pump
    virtual void run();

    virtual void onRunFinished() {}

    /// Return pointer to my TradingContext.
    TradingContextPtr getTradingContext() const;

    /// Returns the configured account
    const acct_t getAccount() const { return bb::acct_t( m_acctstr.c_str() ); }

    /// Returns the limits for a particular instrument
    Limits* getLimits( const instrument_t& instr ) { return m_limits[instr.sym]; }

    /// Returns the configured trade server name
    const std::string getTradeServer() const { return m_trdserver; }

    static std::string makeIdentifyString( const std::string& base
        , const acct_t acct, const std::vector<symbol_t>& symbols );

protected:
    boost::shared_ptr<SetupOptions> getTradingSetupOptions();
    /// Handles 'a', assigns arg to m_acctstr, returns true.
    /// Handles 'T', assigns arg to m_trdserver, returns true.
    void checkTradingSetupOptions( const boost::program_options::variables_map& vm, ProblemList* problems );

    virtual void addOptions( boost::program_options::options_description &allOptions );


    /// Determines trdserver name if not otherwise set. Sets the default trdserver name
    /// based on acct, but only if no trdserver name was passed in on command line or
    /// by env variables.
    virtual void determineTradeServer();

    virtual Limits* readLimits( instrument_t instr, const luabind::table<>& limits_config );

    virtual void printSettings(bool endl_fg=true);

protected:
    std::string m_acctstr;
    std::string m_trdserver;
    boost::optional<std::string> m_simMessageDump;

    TradingContextPtr m_spTradingContext;
    boost::shared_ptr<ITradingContextFactory> m_spTradingContextFactory;

    auto_vector<Limits*> m_limits;

    TradeConfigMap m_TradeConfigMap;
    virtual void initSimulator(TradingContextPtr const&)=0;
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_TRADINGSETUP_H
