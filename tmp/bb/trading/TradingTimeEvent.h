#ifndef BB_TRADING_TRADINGTIMEEVENT_H
#define BB_TRADING_TRADINGTIMEEVENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



#include <map>
#include <boost/utility.hpp>
#include <boost/optional.hpp>
#include <boost/variant/variant_fwd.hpp>
#include <boost/variant/recursive_wrapper_fwd.hpp>
#include <bb/core/hash_map.h>
#include <bb/core/Scripting.h>
#include <bb/clientcore/Random.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/SessionParams.h>
#include <memory>
namespace bb {
namespace trading {

// TODO document

class TradingTimeInterval
{
public:
    struct RandomInterval
    {
        RandomInterval(int i) : m_maxOffsetMillis(i) {}
        int m_maxOffsetMillis;
    };
    struct Add;
    struct Negate;

    typedef boost::variant<
        timeval_t,
        RandomInterval,
        boost::recursive_wrapper<Add>,
        boost::recursive_wrapper<Negate> > Impl;
    BB_DECLARE_SHARED_PTR(Impl);

    TradingTimeInterval() {}
    explicit TradingTimeInterval(const Impl *impl);
    TradingTimeInterval(const TradingTimeInterval &e);
    TradingTimeInterval &operator=(const TradingTimeInterval &e);

    bool isValid() const { return (bool) m_intervalSpec; }
    ImplCPtr getImpl() const { return m_intervalSpec; }
    bool operator !()const{return !isValid();}
private:
    typedef bool (TradingTimeInterval::*unspecified_bool_type)() const;
public:
    operator unspecified_bool_type() const
    {
        return isValid() ? &TradingTimeInterval::operator! : 0;
    }
protected:
    ImplCPtr m_intervalSpec;
};

class TradingTimeEvent
{
public:
    BB_DECLARE_SCRIPTING();

    struct TimeOfDay
    {
        TimeOfDay(const timeval_t &tod) : m_timeOfDay(tod) {}
        timeval_t m_timeOfDay; // offset from midnight
    };
    struct Combine; // implements a min/max on times
    struct Offset;

    typedef boost::variant<
        cm::reason_t,
        std::string, // special time
        TimeOfDay,
        boost::recursive_wrapper<Combine>,
        boost::recursive_wrapper<Offset>
        > Impl;
    BB_DECLARE_SHARED_PTR(Impl);

    TradingTimeEvent() {}
    explicit TradingTimeEvent(const Impl *impl);
    TradingTimeEvent(const TradingTimeEvent &e);
    TradingTimeEvent &operator=(const TradingTimeEvent &e);

    bool isValid() const { return (bool) m_timeSpec; }
    ImplCPtr getImpl() const { return m_timeSpec; }
    bool operator !()const{return !isValid();}
private:
    typedef bool (TradingTimeEvent::*unspecified_bool_type)() const;

public:
    operator unspecified_bool_type() const
    {
        return isValid() ? &TradingTimeEvent::operator! : 0;
    }
protected:
    ImplCPtr m_timeSpec;
};

std::ostream &operator<<(std::ostream &out, const TradingTimeEvent &e);

class TimeEventEval : public boost::noncopyable
{
public:
    /// Initializes an evaluator with the given evaluation parameters.
    /// rng must remaine live throughout TimeEventEval's lifetime.
    /// TimeEventEval should be destroyed when no longer needed.
    /// instrument can be none, in which case evaluating a clock notice-based
    /// timeval will throw.
    TimeEventEval(
            boost::optional<bb::instrument_t> instrument,
            int ymdDate,
            const SessionParams& sparams,
            const timeval_t &midnightTv,
            RandomSource::RandomGenerator_t *rng);
    ~TimeEventEval();

    void defineTime(std::string const & name, timeval_t tv);

    timeval_t eval(TradingTimeEvent const & spec) { return evalImpl(spec.getImpl()); }
    timeval_t eval(TradingTimeInterval const & spec) { return evalImpl(spec.getImpl()); }

private:
    timeval_t evalImpl(TradingTimeEvent::ImplCPtr const& spec);
    timeval_t evalImpl(TradingTimeInterval::ImplCPtr const& spec);
    struct EventVisitor;
    struct IntervalVisitor;

    typedef std::map<TradingTimeEvent::ImplWeakCPtr, timeval_t> MemoCache_t;
    MemoCache_t m_memoCache;
    std::auto_ptr<EventVisitor>     m_visitor;
    std::auto_ptr<IntervalVisitor> m_intervalVisitor;

    boost::optional<bb::instrument_t> m_instrument;
    boost::optional<std::vector<cm::clock_notice> > m_clockNotices;
    int m_ymdDate;
    SessionParams m_sessionParams;
    timeval_t m_midnightTv;
    typedef bbext::hash_map<std::string, timeval_t> Str2TvMap;
    Str2TvMap m_namedTimes;
    RandomSource::RandomGenerator_t *m_rng;
};
} // namespace bb
} // namespace trading

#endif // BB_TRADING_TRADINGTIMEEVENT_H
