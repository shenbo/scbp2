#ifndef BB_TRADING_LIMITS_H
#define BB_TRADING_LIMITS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/clientcore/PriceProvider.h>
#include <bb/trading/trading.h>
#include <bb/trading/IPositionProvider.h>
#include <bb/trading/IOrderValidator.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_SHARED_PTR(IssuedOrderTracker);

struct BadLimitsException : public std::runtime_error {
    BadLimitsException(std::string s) : std::runtime_error(s) {}
};

/// Keeps limits restricting trading and provides methods for validating orders
/// before issuing. Limits kept and checked include position size and numbers of
/// orders, among others.
///
/// The orders passed to Limits must have AddLiquidityInfo attached to them,
/// for remove liquidity checks!
///
/// @todo: tests_Limits needs to be updated, both existing tests and new methods.
class Limits : public IOrderValidator, public boost::noncopyable
{
public:
    /// Provide an interface to allow any object to be notified when any limits get
    /// changed, for any reason. (user messages, time of day, extreme volatility,
    /// anything)
    class Listener {
    public:
        virtual ~Listener() {}
        virtual void onLimitsChange(const Limits& limits) = 0;
    };


    /// Forward declaration of an inner class that handles all order validation
    /// functionality.
    class OrderValidator;

    static double DEFAULT_MAX_PRICE;

public:
    /// Returns pointer to Limits. Caller owns this pointer. Underlying type of
    /// object depends on instrument. Default values based on instrument will be
    /// initialized. These default values will prevent trading.
    /// Throws invalid_argument if product isn't supported.
    static Limits* create(const instrument_t &instr);

    virtual ~Limits();

    /// Check parameters against both absolute limits and relative relationships to
    /// each other. Ensures that parameter settings are not insane.
    /// Throws a BadLimitsException if there are any errors.
    virtual void checkSanity() const =0;

    /// Add a listener. Throws exception std::invalid_argument if duplicate listener
    /// or otherwise adding the listener fails.
    void addListener(Listener* l);

    /// Remove listener. Does nothing if listener isn't in listeners.
    void removeListener(Listener* l);

    /// Prerequisite to order validation.
    /// Takes in addl objects which are needed solely for order validation.
    /// If any validateX methods are called before this method, an
    /// exception will be thrown.
    /// Throws std::runtime_error if method is called while OrderValidation is active.
    virtual void prepareOrderValidation(
        IssuedOrderTrackerCPtr spIOT,
        IPositionProviderCPtr spPosProvider,
        IPriceProviderCPtr spPriceProvider
        );

    /// Resets limits ability to do order validation. Allows prepareOrderValidation
    /// to be called again. Does nothing if prepareOV hasn't been called yet.
    virtual void clearOrderValidation();


    /// Returns true if desired_order_list as a whole is a valid order set under limits.
    /// Returns false if not.
    virtual bool validateDesiredOrderList(side_t side, const OrderList* desired_order_list) const;


    /// Returns true if, given the actual issued orders we have out
    /// (from oissued_tracker), issuing this desired_order is allowed
    /// under limits.
    /// Returns false otherwise.
    virtual bool validateOrderBeforeIssue(side_t side, OrderCPtr desired_order) const;


public: // getters
    /// returns Position Step Size. guaranteed to be > 0.
    int32_t getPosStepsz() const;
    int32_t getSoftMaxPos() const;
    int32_t getHardMaxPos() const;
    int32_t getSoftMaxPosSteps() const;
    int32_t getHardMaxPosSteps() const;
    unsigned int getMaxSideOrders() const;
    int32_t getMaxOrderSz() const;
    double getMaxPx() const;
    double getMinPx() const;
    double getMaxMargin() const;
    virtual const std::string& getLastReason() const;

public: // setters

    /// Sets position stepsz and the softmax and hardmax in units of that stepsz.
    /// This method must be called before any getters are called.
    /// This helps prevent a Limits object being used improperly when it
    /// has not been prepped for active duty in validations.
    /// Calls checkSanity with new values.
    /// Requires: posstepsz > 0
    /// Throws: BadLimitsException if checkSanity call fails or posstepsz <= 0.
    /// If checkSanity fails, values are rolled back to their original values before throw.
    void setPosLimitsAsSteps( uint32_t posstepsz, int softmaxpos_steps, int hardmaxpos_steps);

    /// sets m_soft_max_pos_steps. no error checking
    void setSoftMaxPosSteps(int _soft_max_pos_steps);

    /// sets m_hard_max_pos_steps. no error checking
    void setHardMaxPosSteps(int _hard_max_pos_steps);

    /// sets m_soft_max_pos_steps to be gte to ceiling(_soft_max_pos / m_pos_stepsz).
    /// no error checking
    void setSoftMaxPos( uint32_t _soft_max_pos );

    /// sets m_max_side_orders. no error checking
    void setMaxSideOrders( uint32_t _max_side_orders);

    /// sets m_max_px. no error checking
    void setMaxPx(double _max_px);

    /// sets m_min_px. no error checking
    void setMinPx(double _min_px);

    /// sets max margin. no error checking
    void setMaxMargin(double _max_margin);

    /// print limits
    void print(std::ostream &out) const;

protected:
    Limits();

    virtual void orderValidatorPrepError() const;
    virtual void setLimitsError() const;

    virtual void _notifyListeners();

    /// Throws BadLimitsException if stepsz <= 0.
    void setPosStepsz(int stepsz);

    boost::scoped_ptr<OrderValidator> m_order_validator;

    typedef std::vector<Listener*> listeners_t;
    listeners_t listeners;

    bool m_change_in_progress;

    uint32_t m_pos_stepsz;
    int32_t m_soft_max_pos_steps;
    int32_t m_hard_max_pos_steps;

    unsigned int m_max_side_orders;
    uint32_t m_max_order_sz;
    double m_max_px;
    double m_min_px;
    double m_max_margin;

    bool m_limits_set;
};
BB_DECLARE_SHARED_PTR( Limits );


class EquityLimits : public Limits
{
public:
    EquityLimits();

    /// checks sanity of limits for a equity instrument
    virtual void checkSanity() const;
};

class FuturesLimits : public Limits
{
public:
    FuturesLimits();

    /// checks sanity of limits for a futures instrument
    virtual void checkSanity() const;
};


std::ostream &operator <<(std::ostream &out, const Limits &limits);


} // namespace trading
} // namespace bb


#endif // BB_TRADING_LIMITS_H
