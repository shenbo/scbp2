#ifndef BB_TRADING_ADD_LIQUIDITY_H
#define BB_TRADING_ADD_LIQUIDITY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {
namespace trading {

///
/// Describes an order's liquidity disposition
///

/// NOTE(acm): These values are intended to reflect the intent of the
/// order, rather than the actual liquidity event at the
/// exchange. This is probably not what you are looking for. Try
/// liquidity_t.

typedef enum {
    CrossRemLiq=0,
    NarrowAddLiq=1,
    BestMktAddLiq=2,
    BelowBestMktAddLiq=3
} add_liquidity_t;

} // namespace trading
} // namespace bb

#endif // BB_TRADING_ADD_LIQUIDITY_H
