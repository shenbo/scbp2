#ifndef BB_TRADING_UNICASTPOSITIONTRACKERFACTORY_H
#define BB_TRADING_UNICASTPOSITIONTRACKERFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/IPositionProviderFactory.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_SHARED_PTR( UnicastPositionTrackerFactory );

// IPositionProviderFactory which builds a UnicastPositionTracker.
class UnicastPositionTrackerFactory : public IPositionProviderFactory
{
public:
    static UnicastPositionTrackerFactoryPtr create();

    virtual IOTPositionProviderPtr createInternal( ITraderPtr baseTrader,
            TradingContext *tradingContext, const bb::instrument_t& instr );
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_UNICASTPOSITIONTRACKERFACTORY_H
