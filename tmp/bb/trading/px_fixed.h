#ifndef BB_TRADING_PX_FIXED_H
#define BB_TRADING_PX_FIXED_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/operators.hpp>
#include <bb/core/side.h>
#include <bb/core/LuabindScripting.h>

namespace bb {
namespace trading {

class px_fixed_common_t
{
public:
    typedef int32_t base_t;
    // Mode argument for converting from double.
    enum rounding_mode_t
    {
        /// Checks that there was no precision loss converting from double,
        /// and throws otherwise
        THROW_ON_INEXACT,

        /// LOG_WARNs on precision loss
        WARN_ON_INEXACT,

        /// Rounds silently
        ROUND,
    };
};

///////////////////////////////////////////////////////////////////////////////
///
/// Fixed-point price class.
///
/// This is intended as a less trouble-prone implementation of price than
/// double as an intermediate representation. Internal storage is a 32-bit
/// signed integer.
///
/// Conversion to/from double is determined by a Context, which must determine
/// the scaling factor & rounding. As of now, the Context must be compile-time
/// bound (i.e. runtime choice of scaling factor for e.g. futures is not
/// possible). Talk to BMB if you need this.
///
/// Most methods attempt to validate their arguments or throw. The errPrefix
/// can be used to get better SRC_LOCATIONs or error messages in this case.
///
///////////////////////////////////////////////////////////////////////////////
template<typename Context>
class px_fixed_t : public px_fixed_common_t, boost::totally_ordered< px_fixed_t<Context> >
{
public:
    static px_fixed_t min();
    static px_fixed_t zero();
    static px_fixed_t max();
    static px_fixed_t innermost(side_t sd);
    static px_fixed_t outermost(side_t sd);
    px_fixed_t(); // initializes to min()

    /// Conversion from double. Throws if:
    /// 1) The price can't be represented exactly; pass mode=ROUND
    ///    to disable this behavior.
    /// 2) The price is beyond the bounds of min/max (inclusive).
    explicit px_fixed_t(double px,
            rounding_mode_t mode = THROW_ON_INEXACT,
            const char *errPrefix = NULL);

    bool operator<(px_fixed_t a) const; // strict weak ordering, yay
    bool operator==(px_fixed_t a) const;
    px_fixed_t<Context> operator-() const;

    /// False when beyond the bounds of min/max (inclusive).
    bool isValid() const;
    /// Throws if !isValid(). Otherwise returns the price unchanged (for chaining).
    const px_fixed_t<Context> &checkValid(const char *errPrefix = NULL) const;
    /// Throws an exception if the price is <= 0
    const px_fixed_t<Context> &checkPositive(const char *errPrefix = NULL) const;
    /// Throws an exception if the price is < 0
    const px_fixed_t<Context> &checkNonnegative(const char *errPrefix = NULL) const;

    /// Converts to double. Throws only if the price is invalid;
    /// you may want to checkPositive() if you're using it as an order price.
    double toDouble(const char *errPrefix = NULL) const;

    /// Converts to string. Never throws.
    std::string toString() const;

    /// Returns the distance between two prices.
    px_fixed_t diff(px_fixed_t b) const;
    // Returns (this - b) / b
    double ratio_from(px_fixed_t b) const;

    ///
    /// Arithmetic on prices.
    ///
    /// Each of these methods returns a new price and does not modify this.
    /// The operations are saturating, such that the sign of the price is
    /// never altered.  Put another way, if x is in the range (0, max), then
    /// so is x.increased(); same goes decreased/faded/improved.

    /// Returns this + amt.
    px_fixed_t increased(px_fixed_t amt) const;
    px_fixed_t decreased(px_fixed_t amt) const { return increased(-amt); }

    /// Returns a price that is less aggressive in
    /// the given direction by the given amount.
    px_fixed_t faded(side_t sd, px_fixed_t amt) const { return improved(sd, -amt); }
    /// Returns a more aggressive price (compare: faded)
    px_fixed_t improved(side_t sd, px_fixed_t amt) const;

    base_t get_fixed() const { return m_base; }
protected:
    explicit px_fixed_t(base_t i) : m_base(i) {}
    base_t m_base;
};

// Overloads of the functions from core/side.h
template<typename C> bool at(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b);
template<typename C> bool inside(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b);
template<typename C> bool inside_or_at(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b);
template<typename C> bool outside(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b);
template<typename C> bool outside_or_at(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b);

template<typename C> inline px_fixed_t<C> inner_price( side_t sd, px_fixed_t<C> a, px_fixed_t<C> b );
template<typename C> inline px_fixed_t<C> outer_price( side_t sd, px_fixed_t<C> a, px_fixed_t<C> b );

template<typename C> std::ostream &operator<<(std::ostream &os, px_fixed_t<C> a);

///////////////////////////////////////////////////////////////////////////////
///
/// px_isld_t: px_fixed_t<> Context for NASDAQ pricing.
///
/// Stores prices with scaling factor 0.0001, rounding prices to a penny if
/// they exceed 1.00.
///
///////////////////////////////////////////////////////////////////////////////
struct IsldPxContext;
typedef px_fixed_t<IsldPxContext> px_isld_t;

struct IsldPxContext
{
    static const int MULT = 10000;
    typedef px_isld_t::base_t base_t;

    base_t doubleToFixed(double p, px_isld_t::rounding_mode_t mode, const char *errPrefix);
    double fixedToDouble(base_t p) const;
    std::string toString(base_t a) const;
};




///////////////////////////////////////////////////////////////////////////////
/// inline impl
///////////////////////////////////////////////////////////////////////////////

namespace fixed_point_detail
{
    typedef px_fixed_common_t::base_t base_t;
    base_t sign_preserving_add(base_t base, base_t offset);
    base_t saturating_add(base_t a, base_t b);
}

template<typename C>
inline px_fixed_t<C> px_fixed_t<C>::min()
{
    return px_fixed_t<C>();
}

template<typename C>
inline px_fixed_t<C> px_fixed_t<C>::zero()
{
    return px_fixed_t<C>(0);
}

template<typename C>
inline px_fixed_t<C> px_fixed_t<C>::max()
{
    return px_fixed_t<C>(std::numeric_limits<base_t>::max());
}

template<typename C>
inline px_fixed_t<C> px_fixed_t<C>::innermost(side_t sd)
{
    return sd == BID ? max() : min();
}

template<typename C>
inline px_fixed_t<C> px_fixed_t<C>::outermost(side_t sd) { return innermost(~sd); }

template<typename C>
inline px_fixed_t<C>::px_fixed_t() : m_base(-std::numeric_limits<base_t>::max())
{
    // We use -max for the minimum value, not min; this allows people
    // to negate any price without fear of overflow.
}

template<typename C>
px_fixed_t<C>::px_fixed_t(double px, rounding_mode_t mode, const char *errPrefix)
    : m_base(C().doubleToFixed(px, mode, errPrefix))
{
}

template<typename C>
inline bool px_fixed_t<C>::operator<(px_fixed_t<C> a) const
{
#ifndef NDEBUG
    checkValid(SRC_LOCATION_SEP);
#endif
    return m_base < a.m_base;
}

template<typename C>
inline px_fixed_t<C> px_fixed_t<C>::operator-() const
{
#ifndef NDEBUG
    checkValid(SRC_LOCATION_SEP);
#endif
    return px_fixed_t<C>(-m_base);
}

template<typename C>
inline bool px_fixed_t<C>::operator==(px_fixed_t<C> a) const
{
#ifndef NDEBUG
    checkValid(SRC_LOCATION_SEP);
#endif
    return m_base == a.m_base;
}

template<typename C>
inline bool px_fixed_t<C>::isValid() const
{
    return (m_base > -std::numeric_limits<base_t>::max()) &&
           (m_base < std::numeric_limits<base_t>::max());
}

template<typename C>
inline const px_fixed_t<C> &px_fixed_t<C>::checkValid(const char *errPrefix) const
{
    BB_THROW_EXASSERT_SS(m_base > -std::numeric_limits<base_t>::max(),
            (errPrefix ? errPrefix : SRC_LOCATION_SEP) << "price is invalid (== min)");
    BB_THROW_EXASSERT_SS(m_base < std::numeric_limits<base_t>::max(),
            (errPrefix ? errPrefix : SRC_LOCATION_SEP) << "price is invalid (== max)");
    return *this;
}

template<typename C>
inline const px_fixed_t<C> &px_fixed_t<C>::checkNonnegative(const char *errPrefix) const
{
    BB_THROW_EXASSERT_SS(m_base >= 0,
            (errPrefix ? errPrefix : SRC_LOCATION_SEP) << "price is negative");
    return checkValid(errPrefix);
}


template<typename C>
inline const px_fixed_t<C> &px_fixed_t<C>::checkPositive(const char *errPrefix) const
{
    BB_THROW_EXASSERT_SS(m_base != 0,
            (errPrefix ? errPrefix : SRC_LOCATION_SEP) << "price is zero");
    return checkNonnegative(errPrefix);
}


template<typename C>
inline double px_fixed_t<C>::toDouble(const char *errPrefix) const
{
    checkValid(errPrefix);
    return C().fixedToDouble(m_base);
}

template<typename C>
inline px_fixed_t<C> px_fixed_t<C>::improved(side_t side, px_fixed_t<C> amt) const
{
#ifndef NDEBUG
    checkValid(SRC_LOCATION_SEP);
    amt.checkValid(SRC_LOCATION_SEP);
#endif
    return px_fixed_t(fixed_point_detail::sign_preserving_add(m_base, side2sign(side, amt.m_base)));
}

template<typename C>
inline px_fixed_t<C> px_fixed_t<C>::increased(px_fixed_t<C> amt) const
{
#ifndef NDEBUG
    checkValid(SRC_LOCATION_SEP);
    amt.checkValid(SRC_LOCATION_SEP);
#endif
    return px_fixed_t(fixed_point_detail::sign_preserving_add(m_base, amt.m_base));
}

template<typename C>
inline px_fixed_t<C> px_fixed_t<C>::diff(px_fixed_t<C> b) const
{
#ifndef NDEBUG
    checkValid(SRC_LOCATION_SEP);
    b.checkValid(SRC_LOCATION_SEP);
#endif
    return px_fixed_t(fixed_point_detail::saturating_add(m_base, -b.m_base));
}

template<typename C>
inline double px_fixed_t<C>::ratio_from(px_fixed_t<C> b) const
{
#ifndef NDEBUG
    checkValid(SRC_LOCATION_SEP);
    b.checkValid(SRC_LOCATION_SEP);
#endif
    return double(fixed_point_detail::saturating_add(m_base, -b.m_base)) / b.m_base;
}

template<typename C>
inline std::string px_fixed_t<C>::toString() const
{
    return C().toString(m_base);
}

template<typename C>
inline bool at(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b) { return a == b; }

template<typename C>
inline bool inside(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b)
{
    BB_ASSERT(sd == BID || sd == ASK);
    return side2sign(sd, a.get_fixed()) > side2sign(sd, b.get_fixed());
}

template<typename C>
inline bool inside_or_at(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b)
{
    BB_ASSERT(sd == BID || sd == ASK);
    return side2sign(sd, a.get_fixed()) >= side2sign(sd, b.get_fixed());
}

template<typename C>
inline bool outside(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b) { return !inside_or_at(sd, a, b); }
template<typename C>
inline bool outside_or_at(side_t sd, px_fixed_t<C> a, px_fixed_t<C> b) { return !inside(sd, a, b); }

template<typename C>
inline px_fixed_t<C> inner_price( side_t sd, px_fixed_t<C> a, px_fixed_t<C> b ) { return inside( sd, a, b ) ? a : b; } 
template<typename C>
inline px_fixed_t<C> outer_price( side_t sd, px_fixed_t<C> a, px_fixed_t<C> b ) { return inside( sd, a, b ) ? b : a; }

template<typename C>
std::ostream &operator<<(std::ostream &os, px_fixed_t<C> a)
{
    return (os << a.toString());
}

} // namespace trading
} // namespace bb

/// Allow setting/getting a px_fixed_t as double from Lua.
namespace luabind
{
    template<typename Context>
    struct default_converter<bb::trading::px_fixed_t<Context> >
        : native_converter_base<bb::trading::px_fixed_t<Context> >
    {
        static int compute_score(lua_State* L, int index)
        {
            return lua_type(L, index) == LUA_TNUMBER ? 0 : -1;
        }

        bb::trading::px_fixed_t<Context> from(lua_State* L, int index)
        {
            return bb::trading::px_fixed_t<Context>(
                    lua_tonumber(L, index),
                    bb::trading::px_isld_t::THROW_ON_INEXACT,
                    SRC_LOCATION_SEP "converting px_fixed_t from lua:");
        }

        void to(lua_State* L, bb::trading::px_fixed_t<Context> const& p)
        {
            lua_pushnumber(L, p.toDouble(SRC_LOCATION_SEP "converting px_fixed_t to lua:"));
        }
    };

    template<typename Context>
    struct default_converter<bb::trading::px_fixed_t<Context> const&>
        : default_converter<bb::trading::px_fixed_t<Context> >
    {};
}

#endif // BB_TRADING_PX_FIXED_H
