#ifndef BB_TRADING_IFILLLEDGER_H
#define BB_TRADING_IFILLLEDGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/circular_buffer.hpp>
#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/trading/FillRecord.h>
#include <bb/core/EfficientDeque.h>

namespace bb {
namespace trading {

/// A PnLProvider implementation is composed with a PnLLedger implementation. To abstract different
/// ways a ledger can be created (and pnl booked) this IFillLedger interface is defined.
/// A ledger is a pair of collections of FillRecords, one ledger for LONG and the other for SHORT trades.
class IFillLedger
{
public:

    typedef bb::EfficientDeque<FillRecord> FillRecords;
    typedef FillRecords::iterator iFillRecord;
    typedef FillRecords::const_iterator c_iFillRecord;

    virtual instrument_t getInstrument() const                      = 0;

    /// A FillRecord can be pushed to the front or back of the collection
    virtual void push_front(const FillRecord& fillrecord)           = 0;
    virtual void push_back(const FillRecord& fillrecord)            = 0;

    /// A FillRecord can be popped off the front or back of the collection.
    /// Note: This removes the element from the collection.
    virtual void pop_front(const side_t lside)                      = 0;
    virtual void pop_back(const side_t lside)                       = 0;

    virtual FillRecord& front(const side_t side)                    = 0;
    virtual FillRecord& back(const side_t side)                     = 0;
    virtual FillRecords const& getRecords(const side_t lside) const = 0;

    /// Queries and simple actions on a ledger
    virtual void clear(const side_t lside)                          = 0;
    virtual bool empty(const side_t lside) const                    = 0;
    virtual size_t size(const side_t lside) const                   = 0;

    /// Returns the ledger side a FillRecord will be added
    virtual side_t getSide(const FillRecord& fillrecord) const      = 0;

    virtual std::ostream& print(std::ostream& out) const            = 0;

    virtual ~IFillLedger() {}
};

BB_DECLARE_SHARED_PTR( IFillLedger );

/// Overload << oper for pretty-print
inline std::ostream &operator <<(std::ostream& out, const IFillLedger& ifillledger) {
    return (ifillledger.print(out));

}    // trading
}    // bb

}

#endif // BB_TRADING_IFILLLEDGER_H
