#ifndef BB_TRADING_GENERICUSERMESSAGEHANDLER_H
#define BB_TRADING_GENERICUSERMESSAGEHANDLER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/MsgHandler.h>
#include <bb/core/usermsg.h>
#include <bb/trading/trading.h>
#include <bb/trading/TradingContext.h>

namespace bb {

class UserMessageMsg;
BB_FWD_DECLARE_SHARED_PTR(IBook);

namespace trading {

class GenericUserMessageHandler
{
public:
    // Pass umsgSource == boost::none for histo,
    //                    source_t::make_auto( SRC_UMSG ) for live.
    GenericUserMessageHandler( const TradingContextPtr& tc, const boost::optional<source_t>& umsgSource,
                               const instrument_t& instr, IBookPtr refBook );

    virtual ~GenericUserMessageHandler();

    void handleUserMessage( const UserMessageMsg& user_msg );

    /// return false to prevent further processing.
    virtual bool onUserMessage( int32_t cmd, const UserMessageMsg& msg )
    {
        return true;
    }

protected:
    virtual void onUserMessageShutdown( int code, const UserMessageMsg& msg );
    virtual void onUserMessageCancelAll( int code, const UserMessageMsg& msg );
    virtual void onUserMessageImmediateGetFlat( int code, const UserMessageMsg& msg );

protected:
    TradingContextPtr m_spTradingContext;
    MsgHandlerPtr m_OurUserMessage, m_AllUserMessage;
    instrument_t m_instr;
    IBookPtr m_userMessageHandlerRefBook;
};

} // trading
} // bb

#endif // BB_TRADING_GENERICUSERMESSAGEHANDLER_H
