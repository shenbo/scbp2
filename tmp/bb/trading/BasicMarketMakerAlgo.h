#ifndef BB_TRADING_BASICMARKETMAKERALGO_H
#define BB_TRADING_BASICMARKETMAKERALGO_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <deque>
#include <boost/functional.hpp>
#include <boost/intrusive/list.hpp>
#include <bb/core/hash_map.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/IClockListener.h>
#include <bb/trading/IThresholdAdjuster.h>
#include <bb/trading/IBiasAdjuster.h>
#include <bb/trading/Algo.h>
#include <bb/trading/Limits.h>
#include <bb/trading/FillInfo.h>
#include <bb/trading/OrderHarmonizer.h>
#include <bb/trading/DesiredOrdersCollector.h>

namespace bb {
namespace trading {

template<typename T> class CompareVal
{
public:
    CompareVal( const T init, T diff_v ) : m_value( init ), m_diff( diff_v ) {}
    bool diff( const T& v1 ) const { return (std::abs( m_value - v1 ) > m_diff ); }
    bool diff( const CompareVal<T>& v ) const { return diff( v.m_value ); }
    CompareVal& operator=( const T& v ) { m_value = v; return *this; }
    const T& get() const { return m_value; }
    void update( const CompareVal<T>& v ) { m_value = v.get(); }

    void setTolerance( const T& diff ) { m_diff = diff; }

private:
    T m_value;
    T m_diff;
};

template<> class CompareVal<timeval_t>
{
public:
    CompareVal( const timeval_t init, ptime_duration_t diff_v ) : m_value( init ), m_diff( diff_v ) {}
    bool diff( const timeval_t& v1 ) const { return ptime_duration_abs( timeval_diff( m_value, v1 ) ) > m_diff; }
    bool diff( const CompareVal<timeval_t>& v ) const { return diff( v.m_value ); }
    CompareVal& operator=( const timeval_t& v ) { m_value = v; return *this; }
    const timeval_t& get() const { return m_value; }
    void update( const CompareVal<timeval_t>& v ) { m_value = v.get(); }

    void setTolerance( const ptime_duration_t& diff ) { m_diff = diff; }

private:
    timeval_t m_value;
    ptime_duration_t m_diff;
};

class Gauge
{
public:
    Gauge( uint32_t _max ) : m_curAmount( 0 ), m_maxAmount( _max ) {}
    Gauge() : m_curAmount( 0 ), m_maxAmount( 0 ) {}
    Gauge& operator+= ( uint32_t update )
    {
        m_curAmount += update;
        return *this;
    }
    uint32_t getCurrent() const { return m_curAmount; }
    uint32_t getRemainder() const
    {
        return std::max( (int32_t) (m_maxAmount - m_curAmount), (int32_t) 0 );
    }
    uint32_t getMaxAmount() const { return m_maxAmount; }
    Gauge& setMaxAmount( uint32_t m ) { m_maxAmount = m; return *this; }

private:
    uint32_t m_curAmount;
    uint32_t m_maxAmount;
};

/** Returns MM request that will maintain a constant market around the BBO midpoint.
 * Each MM takes a ThresholdAdjuster and a BiasAdjuster that provides information
 * about how far away to place the orders and a bias to the market midpoint.
 *
 * Depending on the limits, the MM may overfill the target shares.  To guarantee
 * exact target fills, make sure the the orders per side limit is set to 1.
 *
 * max-side-orders, if set, will determine how many orders the order harmonizer will
 * allow to be in the market at one time.  An input <= 0 will disable the
 * max side orders check.  This functionality is disabled by default.
 *
 * Example:
 * @code
 * main()
 * {
 *     BasicMarketMakerAlgo algo;
 *     IThresholdAdjusterPtr thresh( new ManualThresholdAdjuster() );
 *     IBiasAdjusterPtr bias( new ManualBiasAdjuster() );
 *     uint32_t bid_shares = 200;
 *     uint32_t ask_shares = 200;
 *     IAlgoRequestListenerPtr listener( new MyAlgoRequestListener() );
 *
 *     BasicMarketMakerAlgo::MarketMakerRequestPtr ref = algo.requestMarketMaker(
 *         thresh, bias, bid_shares, ask_shares, listener );
 *
 *     algo.updateOrders(); // this will place the actual orders
 *
 *     // ... wait for something to happen
 * }
 * @endcode
 */
class BasicMarketMakerAlgo
    : public IClockListener
{
public:
    /// The actual order placement engine.  We can make multiple variants of this
    /// for use with the BasicMarketMakerAlgo.
    class MarketMakerRequest
        : public Algo::Request
        , public IOrderStatusListener
        , public IBookListener
        , public IThresholdAdjusterListener
        , public IBiasAdjusterListener
        , public boost::intrusive::list_base_hook< boost::intrusive::link_mode<boost::intrusive::auto_unlink> >
    {
        friend class BasicMarketMakerAlgo;
    public:
        /// We attach this Info to each order that we issue.
        struct MMOrderInfo : public OrderInfoSlot
        {
            BB_DECLARE_ORDER_INFO();
            double m_threshold, m_midpx, m_bias;
            OrderListenerInfo::SubPtr m_listenerSub;
        };

        MarketMakerRequest( TradingContextPtr tc
            , mktdest_t mkt
            , const IBookPtr& book
            , IThresholdAdjusterPtr thresh
            , IBiasAdjusterPtr bias
            , uint32_t shares_bid
            , uint32_t shares_ask
            , uint32_t maxOrderSize
            , BasicMarketMakerAlgo* parent );
        ~MarketMakerRequest();

        void start();
        void stop();
        void addShares( side_t side, uint32_t shares );
        void setMaxOrderSize( uint32_t sz ) { m_maxOrderSize = sz; }
        void updateDesiredOrderSize( uint32_t numShares );
        uint32_t getDesiredOrderSize() const { return m_desiredOrderSize; }

        uint32_t getMaxOrderSize() const { return m_maxOrderSize; }
        mktdest_t getMktDest() const { return m_mktDest; }
        uint32_t getFilledShares( side_t side ) const { return m_fillGauge[side].getCurrent(); }
        uint32_t getTargetShares( side_t side ) const { return m_fillGauge[side].getMaxAmount(); }
        virtual uint32_t getFilledShares() const { return getFilledShares( BID ) + getFilledShares( ASK ); }
        virtual uint32_t getTargetShares() const { return getTargetShares( BID ) + getTargetShares( ASK ); }
        virtual uint32_t getOutstandingShares() const { return getTargetShares() - getFilledShares(); }
        virtual bool isActive() const { return m_bActive; }

    protected:
        OrderPtr createOrder(side_t side);
        bool isUpdateOrdersRequired() const { return m_bUpdateOrdersRequired; }
        void addOrdersToCollector( DesiredOrdersCollector* collector );

        virtual void manageOrders(bool force = false);
        virtual bool isManageOrdersNeeded();
        virtual uint32_t getOrderSize( bb::side_t side );
        virtual void checkCompletion();

        /// IOrderStatusListener impl
        virtual void onFill( const bb::trading::FillInfo& info );
        virtual void onOrderStatusChange( const OrderPtr& io, const ChangeFlags &flags );

        virtual void onThresholdUpdate( const IThresholdAdjuster* thresh ) { manageOrders(); }
        virtual void onBiasUpdate( const IBiasAdjuster* adjuster ) { manageOrders(); }
        virtual void onBookChanged( const IBook*, const Msg*, int32_t, int32_t ) { manageOrders(); }

    protected:
        struct State
        {
            State() : thresh_bid( 0, 0.0005 )
                    , thresh_ask( 0, 0.0005 )
                    , bias( 0, 0.0005 )
                    , midprice( 0, FP_EPSILON )
                    , time( 0, boost::posix_time::seconds( 1 ) )
                    , remainder_bid( 0, 0 )
                    , remainder_ask( 0, 0 )
            {}
            bool diff( const State& s ) const
            {
                if( thresh_bid.diff( s.thresh_bid ) ) return true;
                if( thresh_ask.diff( s.thresh_ask ) ) return true;
                if( midprice.diff( s.midprice ) ) return true;
                if( bias.diff( s.bias ) ) return true;
                if( time.diff( s.time ) ) return true;
                if( remainder_bid.diff( s.remainder_bid ) ) return true;
                if( remainder_ask.diff( s.remainder_ask ) ) return true;
                return false;
            }
            void update( const State& s )
            {
                thresh_bid.update( s.thresh_bid );
                thresh_ask.update( s.thresh_ask );
                bias.update( s.bias );
                midprice.update( s.midprice );
                time.update( s.time );
                remainder_bid.update( s.remainder_bid );
                remainder_ask.update( s.remainder_ask );
            }

            CompareVal<double> thresh_bid;
            CompareVal<double> thresh_ask;
            CompareVal<double> bias;
            CompareVal<double> midprice;

            CompareVal<timeval_t> time;
            CompareVal<int32_t> remainder_bid;
            CompareVal<int32_t> remainder_ask;
        };
        State m_curStateVector;

        bbext::hash_map<unsigned int, OrderListenerInfo::SubPtr> m_issuedOrderListeners;
        mktdest_t m_mktDest;
        IBookPtr m_spBook;
        instrument_t m_instr;
        Gauge m_fillGauge[2];
        IThresholdAdjusterPtr m_spThresholdAdjuster;
        IBiasAdjusterPtr m_spBiasAdjuster;
        ITimeProviderCPtr m_spTimeProvider;

        IThresholdAdjuster::Subscription  m_adjThresholdSub;
        IBiasAdjuster::Subscription m_adjBiasSub;
        bool m_bUpdateOrdersRequired;

        OrderPtr m_pOrder[2];
        OrderListenerInfo::SubPtr m_pOrderListeners;
        bool m_bActive;
        uint32_t m_maxOrderSize, m_desiredOrderSize;
        BasicMarketMakerAlgo *m_parent;
        IAlgoRequestListener* m_addedListener;
    };
    BB_DECLARE_SHARED_PTR(MarketMakerRequest);

public:
    BasicMarketMakerAlgo( TradingContextPtr tc
        , IBookPtr refbook
        , mktdest_t dest
        , IOrderValidatorCPtr limits
        );
    virtual ~BasicMarketMakerAlgo() {}

    MarketMakerRequestPtr requestMarketMaker( IThresholdAdjusterPtr thresh
        , IBiasAdjusterPtr bias
        , uint32_t bid_shares
        , uint32_t ask_shares
        , uint32_t max_order_size
        , IAlgoRequestListener* listener = 0 );

    uint32_t getNumRequests() const { return m_requestList.size(); }

    void setMktDest( mktdest_t mkt ) { m_mktDest = mkt; }
    void setMaxSideOrders( int32_t );

    void startAll();
    void stopAll();

    /// This needs to be called at the end of an evaluation cycle
    void updateOrders();

protected:
    friend class MarketMakerRequest;

protected:
    void registerListeners();
    void onRequestFinished( MarketMakerRequest& );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int32_t reason, void* pdata );

protected:
    typedef boost::intrusive::list<MarketMakerRequest, boost::intrusive::constant_time_size<false> > RequestList;

    RequestList m_requestList;
    instrument_t m_instr;
    mktdest_t m_mktDest;
    TradingContextPtr m_spTradingContext;
    IBookPtr m_spRefBook;
    OrderHarmonizerPtr m_spOrderHarmonizer;
    DesiredOrdersCollector m_desiredOrdersCollector;
    bool m_bUpdateOrders;
    IOrderValidatorCPtr m_spLimits;
    std::string m_lastReason;
};

BB_DECLARE_SHARED_PTR( BasicMarketMakerAlgo );

} // namespace bb
} // namespace trading

#endif // BB_TRADING_BASICMARKETMAKERALGO_H
