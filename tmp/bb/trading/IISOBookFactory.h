#ifndef BB_TRADING_IISOBOOKFACTORY_H
#define BB_TRADING_IISOBOOKFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/network.h>

#include <bb/io/feeds.h>

namespace bb {

class instrument_t;
BB_FWD_DECLARE_SHARED_PTR( ISOBook );

namespace trading {

class TradingContext;

class IISOBookFactory
{
public:
    virtual ~IISOBookFactory();
    virtual ISOBookPtr create( TradingContext* tradingContext, const bb::instrument_t& instr ) = 0;
};
BB_DECLARE_SHARED_PTR( IISOBookFactory );

class DefaultISOBookFactory : public IISOBookFactory
{
public:
    // A default constructed DefaultISOBookFactory will have gap
    // detection enabled, and will use the spin server information
    // from looking up subcription info on SRC_SPIN_SERVER. Use
    // setSpinServer and setGapDetection below to override these
    // defaults.
    DefaultISOBookFactory();

    virtual ~DefaultISOBookFactory();

    // You do not need to explicitly set the spin server info unless
    // you wish to override the spin server defined in the spinserver
    // configuration file. Passing an empty list of servers here will
    // disable spin servers for ISOBooks.
    void setSpinServers( const HostPortPairList& servers );

    // Set the state of gap detection. By default, gap detection is enabled.
    inline void setGapDetection( bool gapDetection )
    {
        m_gapDetection = gapDetection;
    }

    inline bool getGapDetection() const
    {
        return m_gapDetection;
    }

    virtual ISOBookPtr create( TradingContext* tradingContext, const bb::instrument_t& instr );

private:
    boost::optional<HostPortPairList> m_spinInfos;
    bool                              m_gapDetection;
};
BB_DECLARE_SHARED_PTR( DefaultISOBookFactory );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_IISOBOOKFACTORY_H
