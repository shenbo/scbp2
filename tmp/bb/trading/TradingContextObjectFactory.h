#ifndef BB_TRADING_TRADINGCONTEXTOBJECTFACTORY_H
#define BB_TRADING_TRADINGCONTEXTOBJECTFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/mktdest.h>
#include <bb/core/ListenerRelay.h>
#include <boost/function.hpp>

namespace bb {
class instrument_t;
BB_FWD_DECLARE_SHARED_PTR( ICommoditiesSpecificationsMap );

namespace trading {

BB_FWD_DECLARE_SHARED_PTR(IRefBookFactory);
BB_FWD_DECLARE_SHARED_PTR(RefData);
BB_FWD_DECLARE_SHARED_PTR(IRefDataFactory);
BB_FWD_DECLARE_SHARED_PTR(InstrumentContext);
BB_FWD_DECLARE_SHARED_PTR(IISOBookFactory);
BB_FWD_DECLARE_SHARED_PTR(IOTPositionProvider);
BB_FWD_DECLARE_SHARED_PTR(IssuedOrderTracker);
BB_FWD_DECLARE_SHARED_PTR(IPositionProvider);
BB_FWD_DECLARE_SHARED_PTR(IPositionProviderFactory);
BB_FWD_DECLARE_SHARED_PTR(IFeeProvider);
BB_FWD_DECLARE_SHARED_PTR(IFeeProviderFactory);
BB_FWD_DECLARE_SHARED_PTR(IOrderFilter);



namespace detail {
class object_factory
{
    typedef ListenerRelay<IOrderFilterPtr> OrderFilterList;
public:
    virtual ~object_factory() {}

    virtual RefDataPtr createReferenceData( const instrument_t& instr );
    virtual IOTPositionProviderPtr createPositionProvider( const instrument_t& instr );
    virtual ISOBookPtr createISOBook( const instrument_t& instr );
    virtual IssuedOrderTrackerPtr createIssuedOrderTracker( const instrument_t& instr );
    virtual PnLProviderPtr createPnLProvider( const instrument_t& instr );
    virtual IFeeProviderPtr createFeeProvider( const instrument_t& instr );

    mktdest_t getPrimaryMktDest( const instrument_t& instr ){ return m_bfPrimaryMktDest( instr ); }
    const ICommoditiesSpecificationsMapCPtr getCommoditiesSpecificationsMap(){ return m_bfCommoditiesSpecificationsMap(); }

protected:
    friend class bb::trading::TradingContext;
    object_factory( TradingContext* );
    void invalidate() { m_pTC = 0; }
    OrderFilterList& getOrderFilters() { return m_globalOrderFilters; }

    TradingContext* m_pTC;
    IPositionProviderFactoryPtr m_spPositionProviderFactory;
    IISOBookFactoryPtr m_spISOBookFactory;
    IRefDataFactoryPtr m_spReferenceDataFactory;
    IFeeProviderFactoryPtr m_spFeeProviderFactory;
    ListenerRelay<IOrderFilterPtr> m_globalOrderFilters;

    typedef boost::function<mktdest_t( const instrument_t& )> PrimaryMktDestFunction;
    PrimaryMktDestFunction m_bfPrimaryMktDest;

    typedef boost::function<const ICommoditiesSpecificationsMapCPtr()> CommoditiesSpecificationsMap;
    CommoditiesSpecificationsMap m_bfCommoditiesSpecificationsMap;
};
} // namespace detail

} // namespace trading
} // namespace bb

#endif // BB_TRADING_TRADINGCONTEXTOBJECTFACTORY_H
