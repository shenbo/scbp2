#ifndef BB_TRADING_DESIREDORDERSCOLLECTOR_H
#define BB_TRADING_DESIREDORDERSCOLLECTOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/Order.h>

namespace bb {
namespace trading {

/** Utility class for collecting a bunch of desired orders in DesiredOrderLists
 *  on both sides.
 */
class DesiredOrdersCollector
{
public:
    DesiredOrdersCollector()
    {
        m_desiredOrderList[BID].reset( new OrderList(BID) );
        m_desiredOrderList[ASK].reset( new OrderList(ASK) );
    }

    void addOrder( const OrderPtr &order )
    {
        side_t side = order->orderInfo().getSide();
        m_desiredOrderList[side]->insert( order );
    }

    void clear()
    {
        m_desiredOrderList[BID]->clear();
        m_desiredOrderList[ASK]->clear();
    }

    /// Drops orders on dropSide until the two sides do not cross.
    void dropCrossedOrders(side_t dropSide)
    {
        while(!m_desiredOrderList[BID]->empty()
                && !m_desiredOrderList[ASK]->empty()
                && bb::outside_or_at(BID, m_desiredOrderList[ASK]->front()->orderInfo().getPrice(),
                                          m_desiredOrderList[BID]->front()->orderInfo().getPrice()))
        {
            m_desiredOrderList[dropSide]->pop_front();
        }
    }

    OrderList* getDesiredOrderList( side_t s ) { return m_desiredOrderList[s].get(); }
    const OrderList* getDesiredOrderList( side_t s ) const { return m_desiredOrderList[s].get(); }

protected:
    boost::scoped_ptr<OrderList> m_desiredOrderList[2];
};

/// Combines desired orders and desired cancels on two sides.
class MarketCommand : protected DesiredOrdersCollector
{
public:
    MarketCommand()
    {
        m_cancelledOrderList[BID].reset(new OrderList(BID));
        m_cancelledOrderList[ASK].reset(new OrderList(ASK));
    }

    void issue(const OrderPtr &o) { addOrder(o); }

    void cancel(const OrderPtr &o, bool force = false)
    {
        if(!o->issuedInfo().cancelSent() || force)
            m_cancelledOrderList[o->orderInfo().getSide()]->insert(o);
    }

    template<typename Container>
    void cancelAll(const Container &c, bool force=false)
    {
        for(typename Container::const_iterator i = c.begin(), end = c.end(); i != end; ++i)
            cancel(*i, force);
    }

    using DesiredOrdersCollector::getDesiredOrderList;
    using DesiredOrdersCollector::dropCrossedOrders;
    void clearDesired() { DesiredOrdersCollector::clear(); }

    OrderList* getCancelledOrderList( side_t s ) { return m_cancelledOrderList[s].get(); }
    const OrderList* getCancelledOrderList( side_t s ) const { return m_cancelledOrderList[s].get(); }

protected:
    boost::scoped_ptr<OrderList> m_cancelledOrderList[2];
};

} // namespace bb
} // namespace trading

#endif // BB_TRADING_DESIREDORDERSCOLLECTOR_H
