#ifndef BB_TRADING_ISSUEDORDERTRACKER_H
#define BB_TRADING_ISSUEDORDERTRACKER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/range.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/utility.hpp>
#include <boost/function.hpp>

#include <bb/core/Dispose.h>
#include <bb/core/hash_set.h>
#include <bb/core/instrument.h>
#include <bb/core/ListenerRelay.h>
#include <bb/core/tid.h>
#include <bb/core/smart_ptr.h>

#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/ClockMonitor.h>

#include <bb/trading/trading.h> //PRIORITY_TRADING_DEFAULT
#include <bb/trading/trading.h>
#include <bb/trading/Order.h>
#include <bb/trading/IFillListener.h>
#include <bb/trading/IOrderStatusListener.h>


namespace bb {

class TdRepriceMsg;
class TdStatusChangeMsg;
class TdModifyStatusChangeMsg;
BB_FWD_DECLARE_SHARED_PTR(IAlert);
BB_FWD_DECLARE_SHARED_PTR(BsonLog);

namespace trading {

class TradingContext;
BB_FWD_DECLARE_SHARED_PTR(IOTPositionProvider);
BB_FWD_DECLARE_SHARED_PTR(IssuedOrderTracker);
BB_FWD_DECLARE_SHARED_PTR(ITrader);
BB_FWD_DECLARE_SHARED_PTR(IOrderFilter);

/// IssuedOrderTracker keeps track of all actual orders that have been
/// created until they are no longer needed. It tracks orders that are DONE
/// as well.
///
/// Known shortcomings:
///   orders are never removed from the DONE list.
///   searches in the active & do ne lists are linear.
///       The done list is only searched in corner cases, but it could be huge.
class IssuedOrderTracker
    : public boost::noncopyable
    , public IEventDistListener
{
public:
    /// Returns a new IssuedOrderTracker for the instr. Requires that the base trader
    /// has been created on the TradingContext! The base trader will get wrapped in an
    /// IOTTrader.
    static IssuedOrderTrackerPtr create(
        const instrument_t& instr,
        TradingContext* trading_context,
        bool bsubscribe_events = true );
    typedef PrioritizedListenerList<IOrderFilterPtr> OrderFilterList;

public:
    virtual ~IssuedOrderTracker();

    typedef bbext::hash_map<unsigned int, OrderPtr> OrderMap;

    /* const methods */
    const acct_t getAcct() const { return m_acct; }
    const std::string& getTdTransportType() const;
    const std::string& getServerName() const;
    const std::string& getServerHostName() const;
    const instrument_t& getInstrument() const { return m_instrument; }
    const symbol_t getSym() const { return m_instrument.sym; }
    const symbol_t getSymbol() const { return m_instrument.sym; }
    const uint32_t getEventCount() const { return m_eventCount; }

    /// Getters for sums of attributes of all issued orders
    bool empty() const;
    bool empty( side_t side ) const;

    // Various aggregate quantities of all our active orders; see getActiveOrdersBegin
    // for a definition of active orders.
    int32_t getNumberOfOrdersBySide(side_t side) const;
    // Returns the total quantity on this side. The sign of the result is always positive,
    // regardless of side.
    int32_t getTotalQtyBySide(side_t side) const;
    uint32_t getNumActiveOrders() const;

    /// Returns a shared pointer to a Trader that is synchronized with this IOT.
    /// Internally, IssuedOrderTracker creates that wraps the wrapped_trader, allowing automatic syncing
    /// with this IOT. The internal IOTTrader wraps calls to wrapped_trader, and on
    /// sendOrder() and sendCancel() calls takes care of synchronization with this.
    ITraderPtr getTrader() const;

    /// finds order with orderid, or NULL if not found
    /// include_done_orders controls whether to search through list of done orders.
    const OrderPtr& findOrder(unsigned int orderid, bool include_done_orders=false) const;

    /// finds the first order on which the matcher returns true, or NULL if not found
    /// include_done_orders controls whether to search through list of done orders.
    const OrderPtr& findOrder(const boost::function<bool (const OrderPtr&)>& matcher,
                       bool include_done_orders=false) const;

    /// Returns a const iterator to the beginning of the
    /// active orders list for side.
    ///
    /// Active orders are all orders which have not been confirmed as done by the
    /// trade daemon. This includes orders we've cancelled but not heard back, and
    /// completely filled orders for which we haven't gotten the STAT_DONE yet.
    OrderMap::const_iterator getActiveOrdersBegin(side_t side) const;

    /// Returns a const iterator to the end of the active orders list
    /// for side.
    OrderMap::const_iterator getActiveOrdersEnd(side_t side) const;

    /// Returns a const iterator to the beginning of the done orders list.
    OrderMap::const_iterator getDoneOrdersBegin() const;

    /// Returns a const iterator to the end of the done orders list.
    OrderMap::const_iterator getDoneOrdersEnd() const;

    // Clear map of done orders
    void clearDoneOrders() { m_doneOrders.clear(); }

    /// Sends a cancel for all orders which are in the active list.
    void cancelAll();

    /// Sends a cancel for all orders on the given side.
    void cancelAllOnSide(side_t side);

    /// subscribe to Fills.
    void subscribeFillListener( Subscription& outSub, IFillListener* listener, const Priority& p = PRIORITY_TRADING_DEFAULT ) const;
    /// adds listener to Fills.
    /// throws std::invalid_argument if listener has already been added
    void addFillListener( IFillListener* listener, const Priority& p = PRIORITY_TRADING_DEFAULT ) const;
    /// remove listener to Fills.
    void removeFillListener( IFillListener* listener ) const;

    /// subscribe to to Status Changes.
    void subscribeStatusChangeListener( Subscription &outSub, IOrderStatusListener* l
        , const Priority& p = PRIORITY_TRADING_DEFAULT ) const;

    /// adds listener to Status Changes.
    /// throws std::invalid_argument if listener has already been added
    void addStatusChangeListener( IOrderStatusListener* l, const Priority& p = PRIORITY_TRADING_DEFAULT ) const;
    /// remove listener to Status Changes.
    void removeStatusChangeListener(IOrderStatusListener* l) const;

    /* non-const methods */

    /// Inserts order into internal data structures. IssuedOrderTracker takes
    /// ownership of the order. After the order is inserted, the OrderInfo
    /// may not be modified. Doing so will result in undefined behavior.
    /// After insertion, order will be available through findOrder and will be
    /// tracked when StatusChange and/or Fill messages are received.
    /// Will be available until moveActiveOrderToDone is called.
    /// This should only be used by IOTTrader; it's public for unit testing.
    void insertOrder(const OrderPtr &order);

    void setVerbose( int verbose_lvl ) { m_vbose = verbose_lvl; }

    void setAdjustPosForDoneWithPendingFill( bool b ) { m_adjustPosForDoneWithPendingFill = b; }

    ///
    /// Clear out and release all resources (i.e. orders)
    ///
    void reset();

    ///
    /// Reset statistics
    ///
    void resetStats() { m_eventCount = 0; }

    ///
    /// Returns a reference to the log object used by the IssuedOrderTracker to log status.
    ///
    BsonLog& log() const { return *m_log; }

    void addOrderFilter( const IOrderFilterPtr& filter );
    void addOrderFilter( Subscription& s, const IOrderFilterPtr& filter, const Priority& p );
    OrderFilterList& getOrderFilters();

    // The IssuedOrderTracker provides fill notifications to the given PositionProvider.
    // Used by TradingContext, don't use directly
    void setPositionProvider(IOTPositionProviderPtr const&pp);

public:
    // Unit testing methods. Don't use directly

    /// Private constructor. Use IssuedOrderTracker::create instead.
    /// TODO trading context is used to get order trackers for legs of a spread instrument
    /// a less "hackish" was should be found moving forward
    IssuedOrderTracker( const instrument_t& instr,
            const ITimeProviderPtr &spTimeProv,
            const IAlertPtr &spAlert,
            const std::string & idString,
            const acct_t acct,
            const int vbose,
            const BsonLogPtr &log,
            const ITraderPtr &spBaseTrader,
            const EventDistributorPtr &spEventDist,
            TradingContext* tradingContext = NULL );

    IssuedOrderTracker ( const instrument_t& instr,
            TradingContext* tc,
            const EventDistributorPtr&  );
    // IEventDistListener impl
    void onEvent( const Msg& msg );

protected:
    void init(const ITraderPtr &spBaseTrader,
        const EventDistributorPtr &spEventDist);
    /// changes status of order to CXLSENT. if already CXLSENT, does nothing
    /// throws bb::Error if order is NULL. Called internally by IOTTrader.
    void markOrderCancelSent( const OrderPtr &order, const timeval_t& send_time ) const;

    /// Drops order from active list, and moves it onto the done orders list.
    /// Note: does not change the status of order, even if it's not marked STAT_DONE
    /// Throws std::invalid_argument if order can't be found
    void moveActiveOrderToDone(OrderPtr const&o);

    /// adds a startup message to the logs if it doesn't exist. inline for efficiency
    inline void initBsonLog() { if(unlikely(!m_logStarted)) logStartup(); }

    /// finds a live order referenced by orderid and updates it's status to newstatus.
    /// if status is STAT_DONE, calls checkStatDoneReason and moveActiveOrderToDone
    /// if status is not greater than order's current status, prints a message and does nothing
    /// if a live order with orderid can't be found, tries looking in done orders. If order found
    /// in done orders, will update status if newstatus higher than order status, check some
    /// warning and error conditions and print any problems, and return.
    void handleStatusChange( const TdStatusChangeMsg* status_change, const timeval_t& ts );

    /// finds live orders referenced by orderid and orig_orderid and update their status to newstatus.
    void handleModifyStatusChange( const TdModifyStatusChangeMsg* status_change, const timeval_t& ts );

    /// if status is STAT_DONE, calls checkStatDoneReason and moveActiveOrderToDone
    /// if status is not greater than order's current status, prints a message and does nothing
    /// if a live order with orderid can't be found, tries looking in done orders. If order found
    /// in done orders, will update status if newstatus higher than order status, check some
    /// warning and error conditions and print any problems, and return.
    /// Returns true if order status or cancel status has changed and notify should be called
    template< class MsgT >
    bool checkIfStatusHasChangedAndLog( const MsgT* status_change, bool orderWasDone,  OrderPtr order, const timeval_t& ts
                                        , IOrderStatusListener::ChangeFlags& flags, const ostatus_t new_orderstatus
                                        , const bb::oreason_t reason, const bb::tid_t& tradeid, const cxlstatus_t new_cxlstatus
                                        , const ostatus_t msg_old_orderstatus, const uint64_t& exch_ref);

    void handleFill( const bb::Msg& msg, unsigned int orderid, dir_t dir, double fill_px, uint32_t fill_qty,
        uint32_t left, mktdest_t contraBroker, liquidity_t liquidity, const timeval_t &exch_time,
        const timeval_t &acrTimestamp, const tid_t& tradeid,
        fillflag_t fill_flags,
        const boost::optional<int32_t> &position,
        const timeval_t& nicTime
        );

    void handleReprice( const TdRepriceMsg *reprice);

    void handleOrderNotFoundFill( const bb::Msg& msg, unsigned int orderid, dir_t dir, double fill_px, uint32_t fill_qty,
        uint32_t left, mktdest_t contraBroker, liquidity_t liquidity, const timeval_t &exch_time,
        const timeval_t &acrTimestamp, const tid_t &tradeid,
        fillflag_t fill_flags,
        const boost::optional<int32_t> &position );

    // called once the order state is consistent, to notify PositionTracker, IOT listeners & Order listeners
    void notifyFill( const bb::Msg& msg,
        const OrderPtr &o, IssuedOrderInfo &ioInfo,
        const timeval_t &tv, double fill_px, uint32_t fill_qty,
        const boost::optional<int32_t> &position,
        boost::optional<IFillListener::FeeInfo>& fees);

    // notify position tracker of the position change
    void updatePosition( const timeval_t& tv, const bb::side_t side,
        const double fillPx, const uint32_t fillQty, const boost::optional<int32_t>& position );

    void checkStatDoneReason(bb::oreason_t reason, const OrderCPtr &o) const;

    /// internal findOrder. returns NULL if order not found.
    /// include_done_orders controls whether to search through list of done orders.
    const OrderPtr& _findOrder(unsigned int orderid, bool include_done_orders) const;

    const OrderPtr& _findOrderInDoneList( unsigned int orderid ) const;

    void logStartup();

protected:
    // returns true if the instrument in a message matches our m_instrument.
    // different from instrument_t::operator==
    bool matchInstrument(const instrument_t &instr) const
        { return instrument_t::equals_no_mkt_no_currency()(instr, m_instrument); }
    bool matchInstrumentMsg(acct_t acct, const instrument_t &instr) const;
    bool matchFutureMsg(acct_t acct, symbol_t sym, expiry_t expiry) const;
    bool matchStockMsg(acct_t acct, symbol_t sym) const;

    // When we want pass a reference to a null OrderPtr, we can use this variable
    const OrderPtr     m_nullptr;
    const instrument_t m_instrument;
    const ITimeProviderPtr m_spTimeProv;
    const IAlertPtr m_spAlert;
    const std::string m_idString;
    IOTPositionProviderPtr m_spPositionProvider;

    const acct_t m_acct;

    int m_vbose;

    bool m_adjustPosForDoneWithPendingFill;

    typedef bb::PrioritizedListenerList<IFillListener*> fill_listeners_t;
    mutable fill_listeners_t m_fillListeners;

    typedef bb::ListenerRelay<IOrderStatusListener*> stat_chg_listeners_t;
    mutable stat_chg_listeners_t m_statChgListeners;

    OrderMap m_orders[2];
    OrderMap m_doneOrders;

    const BsonLogPtr m_log;
    bool m_logStarted;
    uint32_t m_eventCount;
    EventSubPtr m_eventSub;
    const std::string m_symbolstring;
    const std::string m_instrumentstring;
    const std::string m_accountstring;

    bb::IntrusiveWeakPtr<trading::TradingContext> m_tradingContext;

    BB_FWD_DECLARE_SHARED_PTR( IOTTrader );
    IOTTraderPtr m_spIOTTrader;

private:
    OrderMap const & getActiveOrders(side_t side) const;
    OrderMap const & getDoneOrders() const;
};


} // namespace trading
} // namespace bb

namespace {

    // These helper classes make it possible to use our 'FOR_EACH_ORDER_SORTED' macro.
    // The point is to shuffle our OrderPtrs in order of:
    // - price first: most aggressive orders first
    // - volume next: highest volume first
    // - orderid last: lowest orderid first
    // We do this without copying the shared pointers along; instead, we'll keep references to them.
    // Because we're keeping references, this object is not meant to be exposed for general use as
    // it's too easy to mess things up.
    struct OrderMapItemComp
        : public std::binary_function<bool, bb::trading::OrderPtr, bb::trading::OrderPtr >
    {
        bool operator()(bb::trading::OrderPtr const& a, bb::trading::OrderPtr const& b) const
        {
            // sort by price first, volume next, order id last
            return ( !bb::EQ(a->orderInfo().getPrice(), b->orderInfo().getPrice()) ?
                        bb::is_more_aggressive(a->orderInfo().getSide(), a->orderInfo().getPrice(), b->orderInfo().getPrice()) :
                    a->issuedInfo().getOrderid() < b->issuedInfo().getOrderid() );
        }
    };

    struct OrderSortingContainer
    {
        OrderSortingContainer (
              bb::trading::IssuedOrderTracker::OrderMap::const_iterator begin,
              bb::trading::IssuedOrderTracker::OrderMap::const_iterator end )
        {
            BOOST_FOREACH ( bb::trading::OrderPtr const & order, boost::make_iterator_range ( begin, end ) | boost::adaptors::map_values )
            {
                m_orders.push_back ( boost::ref(order) );
            }
            std::sort(m_orders.begin(), m_orders.end(), OrderMapItemComp());
        }

        // Notice how we're storing these orders by reference. Because of this, we can't keep this
        // container around for a long time, as we'd risk invalidating these references.
        typedef std::vector< boost::reference_wrapper<const bb::trading::OrderPtr> > OrderVct;
        typedef OrderVct::iterator iterator;
        typedef OrderVct::const_iterator const_iterator;

        iterator begin() { return m_orders.begin(); }
        iterator end() { return m_orders.end(); }

        const_iterator begin() const { return m_orders.begin(); }
        const_iterator end() const { return m_orders.end(); }

        private:
        OrderVct m_orders;
    };
}

namespace boost {
    template<>
        struct range_const_iterator< ::OrderSortingContainer >
        {
            typedef ::OrderSortingContainer::const_iterator type;
        };
}

#define FOR_EACH_ORDER(order,iot,sd) \
    BOOST_FOREACH( bb::trading::OrderPtr const & order, \
        boost::make_iterator_range ( iot->getActiveOrdersBegin( sd ), iot->getActiveOrdersEnd ( sd ) ) | boost::adaptors::map_values )

#define FOR_EACH_ORDER_SORTED(order,iot,sd) \
    BOOST_FOREACH( bb::trading::OrderPtr const & order, \
        OrderSortingContainer ( iot->getActiveOrdersBegin( sd ), iot->getActiveOrdersEnd ( sd ) ) )

#endif // BB_TRADING_ISSUEDORDERTRACKER_H
