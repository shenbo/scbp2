#ifndef BB_TRADING_SIDEDARRAY_H
#define BB_TRADING_SIDEDARRAY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/scoped_array.hpp>
#include <bb/trading/Order.h>

namespace bb {
namespace trading {

/// A length-2 array, indexed by side (BID/ASK).
template<typename T>
class SidedArray
{
public:
    typedef T value_type;
    T &operator[](side_t side) { BB_ASSERT(side < SIDE_INVALID); return m_array[side]; }
    const T &operator[](side_t side) const { BB_ASSERT(side < SIDE_INVALID); return m_array[side]; }
protected:
    T m_array[2];
};



// helper for SidedArray specialization (for types that don't have default constructors).
template<typename T>
class ScopedSidedArray
{
public:
    T &operator[](side_t side) { BB_ASSERT(side < SIDE_INVALID); return *m_array[side].get(); }
    const T &operator[](side_t side) const { BB_ASSERT(side < SIDE_INVALID); return *m_array[side].get(); }
protected:
    ScopedSidedArray() {}
    boost::scoped_ptr<T> m_array[2];
};

// Specialize for OrderList
template<>
class SidedArray<OrderList> : public ScopedSidedArray<OrderList>
{
public:
    SidedArray() { m_array[BID].reset(new OrderList(BID)); m_array[ASK].reset(new OrderList(ASK)); }
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_SIDEDARRAY_H
