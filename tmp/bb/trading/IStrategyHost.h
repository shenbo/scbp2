#ifndef BB_TRADING_ISTRATEGYHOST_H
#define BB_TRADING_ISTRATEGYHOST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



#include <bb/trading/trading.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>
namespace bb {



namespace trading {
struct IStrategyHost{
    virtual ~IStrategyHost() {}
    virtual void exit() =0;
    virtual void startup(const timeval_t &ctv) = 0;


};
BB_DECLARE_SHARED_PTR( IStrategyHost );
} // namespace trading
} // namespace bb

#endif // BB_TRADING_ISTRATEGYHOST_H
