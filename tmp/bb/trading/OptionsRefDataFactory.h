#ifndef BB_TRADING_OPTIONSREFDATAFACTORY_H
#define BB_TRADING_OPTIONSREFDATAFACTORY_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/IRefDataFactory.h>

namespace bb {
namespace trading {

// The OptionsRefDataFactory builds reference data for MKT_CFFEX, MKT_TAIFEX and MKT_SHSE for now.

class OptionsRefDataFactory : public IRefDataFactory
{
public:

//    enum DataMode
//    {
//        kUseTicksAndQuotes,
//        kUseTicksOnly,
//        kUseQuotesOnly,
//    };

    // By default we will produce RefData for each instrument that
    // includes both tick and quote sources. To suppress one of these
    // types of sources, provide a DataMode argument other than
    // kTicksAndQuotes.
    OptionsRefDataFactory();

    virtual ~OptionsRefDataFactory();


    virtual RefDataPtr createRefData(
        TradingContext* tradingContext,
        const instrument_t& instr );

};

BB_DECLARE_SHARED_PTR(OptionsRefDataFactory);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_OPTIONSREFDATAFACTORY_H
