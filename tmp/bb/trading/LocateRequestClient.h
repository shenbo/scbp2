#ifndef BB_TRADING_LOCATEREQUESTCLIENT_H
#define BB_TRADING_LOCATEREQUESTCLIENT_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/acct.h>
#include <bb/core/messages.h>
#include <bb/core/EventPublisher.h>


#include <bb/clientcore/ClientContext.h>


namespace bb {

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( ISpinServerConnection );

namespace trading {

class ILocateServerListener : public bb::IEventSubscriber
{
public:
    virtual ~ILocateServerListener(){};
    virtual bool onLocateRequestResponse( const uint32_t serverRefNum, const uint32_t requestID, const acct_t acct, const symbol_t symbol, uint32_t requestSize, float bidRate, uint32_t sharesReceived, float borrowRate ){ return true; }
    virtual bool onLocateReturnResponse( const uint32_t serverRefNum, const uint32_t requestID, const acct_t acct, const symbol_t symbol, uint32_t returnSize, uint32_t sharesReturned ){ return true; }
};

typedef EventPublisher<ILocateServerListener> LocateServerPublisher;

struct LocateRequestResponseNotifier : public LocateServerPublisher::INotifier
{
    LocateRequestResponseNotifier( ILocateServerListener* callback )
        : LocateServerPublisher::INotifier ( callback ) { }

    bool notify( const uint32_t serverRefNum, const uint32_t requestID, const acct_t acct, const symbol_t symbol, uint32_t requestSize, float bidRate, uint32_t sharesReceived, float borrowRate )
    {
        return !m_callback->onLocateRequestResponse( serverRefNum, requestID, acct, symbol, requestSize, bidRate, sharesReceived, borrowRate );
    }
};

struct LocateReturnResponseNotifier : public LocateServerPublisher::INotifier
{
    LocateReturnResponseNotifier( ILocateServerListener* callback )
        : LocateServerPublisher::INotifier ( callback ) { }

    bool notify( const uint32_t serverRefNum, const uint32_t requestID, acct_t acct, const symbol_t symbol, uint32_t returnSize, uint32_t sharesReturned )
    {
        return !m_callback->onLocateReturnResponse( serverRefNum, requestID, acct, symbol, returnSize, sharesReturned );
    }
};

class LocateRequestClient
{
public:
    LocateRequestClient( acct_t account, const ClientContextPtr& cc );
    LocateRequestClient( acct_t account, const HostPortPair& hpp, const ClientContextPtr& cc );
    virtual ~LocateRequestClient();

    void addLocateServerListener( ILocateServerListener* listener );

    uint32_t sendLocateRequest( const symbol_t symbol, uint32_t requestSize, float bidRate, bool processPartialRequest );
    uint32_t sendLocateReturn( const symbol_t symbol, uint32_t returnSize );

    void onLocateRequestResponseMsg( const LocateRequestResponseMsg& locReqResp );
    void onLocateReturnResponseMsg( const LocateReturnResponseMsg& locRetResp );

protected:
    void initMsgHandlers();

    acct_t m_account;
    ClientContextPtr m_clientContext;
    ISpinServerConnectionPtr m_pConnection;

    MsgHandlerPtr  m_spLocateRequestResponseMsgHandler;
    MsgHandlerPtr  m_spLocateReturnResponseMsgHandler;
    LocateServerPublisher    m_publisher;

    uint32_t m_requestID;
};

BB_DECLARE_SHARED_PTR( LocateRequestClient );

} // namespace trading
} // namespace bb
#endif // BB_TRADING_LOCATEREQUESTCLIENT_H
