#ifndef BB_TRADING_IBIASADJUSTER_H
#define BB_TRADING_IBIASADJUSTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/bind.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/ListenerList.h>

namespace bb {
namespace trading {

class IBiasAdjuster;

class IBiasAdjusterListener
{
public:
    virtual ~IBiasAdjusterListener() {}

    virtual void onBiasUpdate( const IBiasAdjuster* adjuster ) = 0;
};

class IBiasAdjuster : public ListenerList<IBiasAdjusterListener*>
{
public:
    virtual ~IBiasAdjuster() {}
   /// Returns an adjustment factor typically applied to a midpoint
    virtual double getBiasAdjustment() const = 0;

protected:
    void notifyChange()
    {
        notify( boost::bind( &IBiasAdjusterListener::onBiasUpdate, _1, this ) );
    }
};
BB_DECLARE_SHARED_PTR( IBiasAdjuster );

} // trading
} // bb

#endif // BB_TRADING_IBIASADJUSTER_H
