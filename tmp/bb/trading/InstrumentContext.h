#ifndef BB_TRADING_INSTRUMENTCONTEXT_H
#define BB_TRADING_INSTRUMENTCONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/trading/trading.h>
#include <bb/trading/MarkerToMarket.h>
#include <bb/core/InfoObjectHolder.h>
#include <bb/core/Scripting.h>

namespace bb {
class CommoditySpecification;
BB_FWD_DECLARE_SHARED_PTR( IBook );
BB_FWD_DECLARE_SHARED_PTR( ITickProvider );
BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( IPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( ISOBook );
BB_FWD_DECLARE_SHARED_PTR( ITickProviderFactory );

namespace trading {

BB_FWD_DECLARE_SHARED_PTR( ITrader );
BB_FWD_DECLARE_SHARED_PTR( IPositionProvider );
BB_FWD_DECLARE_SHARED_PTR( RefData );
BB_FWD_DECLARE_SHARED_PTR( IFeeProvider );
BB_FWD_DECLARE_SHARED_PTR( IOrderFilter );

namespace detail {
class object_factory;
}

/** Represents context about an instrument
 * TradingContext holds a collection of these.
 */
class InstrumentContext : public InfoObjectHolder
{
public:
    BB_DECLARE_SCRIPTING();
    InstrumentContext( const bb::trading::TradingContextPtr& tc
        , const bb::instrument_t& instr
        , const bb::IBookPtr& book
        , double tick_size
        , double lot_size = 1 );

    InstrumentContext( const bb::trading::TradingContextPtr& tc
        , const bb::instrument_t& instr
        , double tick_size
        , double lot_size = 1 );

    virtual ~InstrumentContext() {}
    bool operator<(InstrumentContext const&)const;
    bool operator<(bb::instrument_t const&)const;
    friend bool operator<(bb::instrument_t const&,InstrumentContext const&);
    bool operator==(InstrumentContext const&)const;
    bool operator==(bb::instrument_t const&)const;
    friend bool operator==(bb::instrument_t const&,InstrumentContext const&);
    const bb::IBookPtr& getBook() const;
    const ISOBookPtr& getISOBook() const;
    double getTickSize() const { return m_tickSize; }
    double getLotSize() const { return m_lotSize; }
    double getContractSize() const { return m_lotSize; }
    bb::instrument_t getInstrument() const { return m_instr; }
    const ITickProviderPtr& getTickProvider() const;
    const ITickProviderCPtr getTickProvider( const source_t& source ) const;
    const ITraderPtr getTrader() const;
    const IPositionProviderPtr getPositionProvider() const;
    const IssuedOrderTrackerPtr& getIssuedOrderTracker() const;
    const PnLProviderPtr& getPnLProvider() const;
    const ClockMonitorPtr& getClockMonitor() const { return m_spClockMonitor; }
    const IClientTimerPtr& getClientTimer() const { return m_spClientTimer; }
    const RefDataPtr& getReferenceData() const;
    const IPriceProviderCPtr& getPriceProvider() const;
    const IPriceProviderCPtr& getLimitPriceFriendlyPriceProvider() const;
    const MarkerToMarketPtr& getMarkerToMarket() const;
    const IFeeProviderPtr& getFeeProvider() const;
    void addOrderFilter( const IOrderFilterPtr& filter );

    void setTickSize( double ticksz ) { m_tickSize = ticksz; }
    void setLotSize( double lotsize ) { m_lotSize = lotsize; }
    void setContractSize( double size ) { m_lotSize = size; }

    mktdest_t getPrimaryMktDest() const;

    boost::optional<CommoditySpecification> getCommoditySpecification() const;

protected:
    friend class TradingContext;
    InstrumentContext( const TradingContext* tc
        , const instrument_t& instr );
    void setReferenceData( const RefDataPtr& rd ) { m_spReferenceData = rd; }
    const IOTPositionProviderPtr& getIOTPositionProvider() const;

private:
    bb::instrument_t m_instr;
    double m_tickSize;
    double m_lotSize;
    bb::ClockMonitorPtr m_spClockMonitor;
    bb::IClientTimerPtr m_spClientTimer;
    boost::shared_ptr<detail::object_factory> m_spObjectFactory;

    mutable bb::IBookPtr m_spBook;
    mutable bb::ITickProviderPtr m_spTickProvider;
    mutable IOTPositionProviderPtr m_spPositionProvider;
    mutable IssuedOrderTrackerPtr m_spIssuedOrderTracker;
    mutable RefDataPtr m_spReferenceData;
    mutable IPriceProviderCPtr m_spPriceProvider;
    mutable IPriceProviderCPtr m_spLPFPriceProvider;
    mutable ISOBookPtr m_spISOBook;
    mutable MarkerToMarketPtr m_spMarkerToMarket;
    mutable IFeeProviderPtr m_spFeeProvider;
    mutable PnLProviderPtr m_spPnLProvider;

    bb::ITickProviderFactoryPtr m_spSourceTickFactory;
};
BB_DECLARE_SHARED_PTR( InstrumentContext );

}

} // namespace bb

#endif // BB_TRADING_INSTRUMENTCONTEXT_H
