#ifndef BB_TRADING_IPOSITIONPROVIDERFACTORY_H
#define BB_TRADING_IPOSITIONPROVIDERFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/TradingContext.h>
#include <bb/trading/IPositionProvider.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_SHARED_PTR(ITrader);

/// Factory for the IPositionProvider.
class IPositionProviderFactory
{
public:
    virtual ~IPositionProviderFactory() {}

    /// Obtains a IPositionProvider for instr. 
    /// Please do not call directly, TradingContext will handle this!
    /// Will not be called twice for the same instr.
    virtual IOTPositionProviderPtr
        createInternal( ITraderPtr baseTrader, TradingContext *tradingContext,
            const bb::instrument_t& instr ) = 0;
};
BB_DECLARE_SHARED_PTR( IPositionProviderFactory );

} // namespace bb
} // namespace trading

#endif // BB_TRADING_IPOSITIONPROVIDERFACTORY_H
