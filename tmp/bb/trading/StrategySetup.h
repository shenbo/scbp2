#ifndef BB_TRADING_STRATEGYSETUP_H
#define BB_TRADING_STRATEGYSETUP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Scripting.h>
#include <bb/core/smart_ptr.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR(Environment);
class DayTimeRange;

namespace trading {
BB_FWD_DECLARE_SHARED_PTR(IStrategyControl);
BB_FWD_DECLARE_SHARED_PTR(TradingContext);

class IStrategyControl
{
public:
    BB_DECLARE_SCRIPTING();
    virtual ~IStrategyControl() {}
    virtual void start()=0;
    //create a IStrategyControl using the Lua environment , and command
    static IStrategyControlPtr createLua(TradingContextPtr const&_tradingcontext,EnvironmentPtr const&_env,std::string const& _cmd);
};
BB_DECLARE_SHARED_PTR(IStrategyControl);

class IStrategyRunner
{
public:
    virtual ~IStrategyRunner() {}
    virtual void exit() =0;
    virtual void run() = 0;
};
BB_DECLARE_SHARED_PTR( IStrategyRunner );

///Creates a Single Trading Context
class ITradingContextFactoryS
{
public:
    BB_DECLARE_SCRIPTING();
    virtual ~ITradingContextFactoryS(){}
    virtual trading::TradingContextPtr create()=0;
    virtual trading::TradingContextPtr createM(DayTimeRange const&)=0;
    static TradingContextPtr createLua(EnvironmentPtr const&_env,std::string const& _cmd,DayTimeRange const&_dtr);
    static TradingContextPtr createLua(EnvironmentPtr const&_env,std::string const& _cmd);
};
BB_DECLARE_SHARED_PTR(ITradingContextFactoryS);

///Creates a Strategy from a Single Trading Context
class IStrategyFactory
{
public:
    BB_DECLARE_SCRIPTING();
    typedef IStrategyControlPtr ret_type;
    virtual ~IStrategyFactory(){}
    virtual ret_type create(trading::TradingContextPtr  const&)=0;
};
BB_DECLARE_SHARED_PTR(IStrategyFactory);

} // trading
} // bb

#endif // BB_TRADING_STRATEGYSETUP_H
