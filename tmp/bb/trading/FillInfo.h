#ifndef BB_TRADING_FILLINFO_H
#define BB_TRADING_FILLINFO_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <inttypes.h>

#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <boost/noncopyable.hpp>

#include <bb/core/compat.h>
#include <bb/core/CompactMsg.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/messages.h>

namespace bb {
namespace trading {

    BB_FWD_DECLARE_INTRUSIVE_PTR(Order);
    BB_FWD_DECLARE_SHARED_PTR(IFeeProvider);

    struct FeeInfo
    {
        inline FeeInfo()
            : exchange( 0.0 )
            , regulatory( 0.0 )
            , clearing( 0.0 ) {}

        inline double total() const
        {
            return exchange + regulatory + clearing;
        }

        double exchange,
            regulatory,
            clearing;
    };
    typedef boost::optional<FeeInfo> OptionalFees;

    struct FillInfo : private boost::noncopyable
    {
        enum FeeCalculations
        {
            RETURN_FROM_CACHE = 0,
            CALCULATE_IF_NEEDED = 1
        };
        FillInfo( const OrderPtr & order,
            double fill_px,
            uint32_t fill_qty,
            bb::Msg const & fill_msg,
            const IFeeProviderPtr & fee_provider,
            const OptionalFees & fees = OptionalFees() );

        double getPx() const;
        uint32_t getQty() const;
        OrderPtr const & getOrder() const;
        const bb::Msg & getFillMsg() const;
        const IFeeProviderPtr & getFeeProvider() const;
        // Getting the fees can be very expensive ( if this entails a LUA call for instance ).
        // Only call it when you really need them, and you're not in the critical path, say before
        // sending a hedge order.
        //
        // Setting 'calculate' to RETURN_FROM_CACHE will retrieve the fees if we've looked them up
        // before already. Setting them to CALCULATE_IF_NEEDED ( default ) will call the FeeProvider
        // ( if available ) and get it to calculate the fees.
        OptionalFees const & getFees( FeeCalculations calculate = CALCULATE_IF_NEEDED ) const;
    private:
        const double            m_fillPx;
        const uint32_t          m_fillQty;
        const bb::Msg &         m_fillMsg;
        const OrderPtr &        m_order;
        const IFeeProviderPtr & m_feeProvider;
        mutable OptionalFees    m_fees;
    };
} }

#endif // BB_TRADING_FILLINFO_H
