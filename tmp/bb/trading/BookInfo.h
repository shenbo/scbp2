#ifndef BB_TRADING_BOOKINFO_H
#define BB_TRADING_BOOKINFO_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/PriceSize.h>
#include <bb/trading/Order.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( IBook );
namespace trading {

class L1Info : public OrderInfoSlot
{
public:
    L1Info( const IBookPtr& );
    BB_DECLARE_ORDER_INFO();
    virtual void logIssue(BsonLog::Record&, Order const *) const;

protected:
    PriceSize m_bid, m_ask;
};

} // namespace trading
} // namespace bb


#endif // BB_TRADING_BOOKINFO_H
