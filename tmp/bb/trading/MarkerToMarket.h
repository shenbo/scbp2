#ifndef BB_TRADING_MARKERTOMARKET_H
#define BB_TRADING_MARKERTOMARKET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/trading/IPositionProvider.h>
#include <bb/trading/IOrderStatusListener.h>
#include <bb/trading/TradingContext.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( IBook );

namespace trading {

BB_FWD_DECLARE_SHARED_PTR(IPositionProvider);
BB_FWD_DECLARE_SHARED_PTR(IssuedOrderTracker);

class MarkerToMarket
    : public IOrderStatusListener
{

public:
    MarkerToMarket( const instrument_t& instr
                    , const IBookPtr& ref_book
                    , const IPositionProviderPtr& position_provider
                    , const IssuedOrderTrackerPtr& issued_order_tracker );
    virtual ~MarkerToMarket() {};

    virtual void onFill( const FillInfo & info );
    virtual void onOrderStatusChange( const OrderPtr& order, CHANGEFLAGS flags ){};
    double markPositionToMarket();

protected:
    instrument_t m_instrument;
    IBookPtr m_refBook;
    IPositionProviderPtr m_positionProvider;

    double m_weightedEntryPriceSum;
    double m_lastPosition;
};

BB_DECLARE_SHARED_PTR( MarkerToMarket );

}
}
#endif // BB_TRADING_MARKERTOMARKET_H
