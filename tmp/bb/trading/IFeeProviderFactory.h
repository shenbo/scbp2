#ifndef BB_TRADING_IFEEPROVIDERFACTORY_H
#define BB_TRADING_IFEEPROVIDERFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>

namespace bb {

class instrument_t;

namespace trading {

BB_FWD_DECLARE_SHARED_PTR( IFeeProvider );
BB_FWD_DECLARE_SHARED_PTR( TradingContext );

class IFeeProviderFactory
{
public:
    virtual ~IFeeProviderFactory();

    virtual IFeeProviderPtr create(
        TradingContext* tradingContext,
        const instrument_t& instr ) = 0;
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_IFEEPROVIDERFACTORY_H
