#ifndef BB_TRADING_IFILLLISTENER_H
#define BB_TRADING_IFILLLISTENER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>

#include <bb/core/ListenerList.h>
#include <bb/trading/trading.h>
#include <bb/trading/FillInfo.h>
#include <bb/core/smart_ptr.h>
namespace bb {
namespace trading {

BB_FWD_DECLARE_INTRUSIVE_PTR(Order);

class IFillListener
{
public:
    typedef bb::trading::FeeInfo FeeInfo;
    typedef bb::trading::OptionalFees OptionalFees;
    virtual ~IFillListener() {}

    virtual void onFill ( const FillInfo & info ) = 0;
};
BB_DECLARE_SHARED_PTR( IFillListener );

} // trading
} // bb

#endif // BB_TRADING_IFILLLISTENER_H
