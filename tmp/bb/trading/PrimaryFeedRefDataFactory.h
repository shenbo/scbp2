#ifndef BB_TRADING_PRIMARYFEEDREFDATAFACTORY_H
#define BB_TRADING_PRIMARYFEEDREFDATAFACTORY_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include "IRefDataFactory.h"

namespace bb {
namespace trading {

class PrimaryFeedRefDataFactory : public IRefDataFactory {
public:
    PrimaryFeedRefDataFactory();
    virtual ~PrimaryFeedRefDataFactory();

    //TradingContext Can be NULL if in live data
    virtual RefDataPtr createRefData(
            TradingContext* tradingContext,
            const instrument_t& instr );

};

BB_DECLARE_SHARED_PTR(PrimaryFeedRefDataFactory);

} /* namespace trading */
} /* namespace bb */

#endif // BB_TRADING_PRIMARYFEEDREFDATAFACTORY_H
