#ifndef BB_TRADING_TRADERSTATE_H
#define BB_TRADING_TRADERSTATE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <stdexcept>

#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/MMap.h>

namespace bb {
namespace trading {

/// forward decl
class TraderState;
BB_DECLARE_SHARED_PTR( TraderState );

class TraderState
{
public:
    TraderState(const std::string filename);
    virtual ~TraderState() {}

    /// Underlying stateinfo struct, which is written to disk via mmap
    /// POD types are initialized to all-bytes-zero by MMap
    struct StateInfo
    {
        timeval_t tv;
        uint32_t Nissues;
        uint32_t Ncancels;
        uint32_t Nfills;
        uint32_t Nbadfills;
        double Hysteresis;
        int    Pos;
        double PnL;
    };

    /// Returns points to stateinfo struct
    StateInfo* getStateInfo() const { return m_mmap.get(); }

    /// For pretty-print
    virtual std::ostream& print(std::ostream& out) const;

private:
    boost::shared_ptr<StateInfo> m_mmap;
};

/// Overload << oper for pretty-print
inline std::ostream &operator <<(std::ostream &out, const TraderState &o) { return o.print( out ); }

}    // trading
}    // bb

#endif // BB_TRADING_TRADERSTATE_H
