#ifndef BB_TRADING_MANUALTHRESHOLDADJUSTER_H
#define BB_TRADING_MANUALTHRESHOLDADJUSTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/IThresholdAdjuster.h>

namespace bb {
namespace trading {

class ManualThresholdAdjuster : public IThresholdAdjuster
{
public:
    ManualThresholdAdjuster()
    {
        m_value[BID] = 0;
        m_value[ASK] = 0;
    }
    void set( bb::side_t side, double v )
    {
        if( m_value[side] != v )
        {
            m_value[side] = v;
            notifyChange();
        }
    }
    void set( double bid_threshold, double ask_threshold )
    {
        if( (m_value[BID] != bid_threshold) || (m_value[ASK] != ask_threshold) )
        {
            m_value[BID] = bid_threshold;
            m_value[ASK] = ask_threshold;
            notifyChange();
        }
    }
    virtual double getThresholdAdjustment( side_t side ) const
    {
        return ( side == BID ) ? m_value[BID] : m_value[ASK];
    }

private:
    double m_value[2];
};
BB_DECLARE_SHARED_PTR(ManualThresholdAdjuster);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_MANUALTHRESHOLDADJUSTER_H
