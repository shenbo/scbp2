#ifndef BB_TRADING_TRADINGCONTEXT_H
#define BB_TRADING_TRADINGCONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <boost/optional.hpp>

#include <bb/core/Dispose.h>
#include <bb/core/hash_map.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/ListenerRelay.h>
#include <bb/core/PreallocatedObjectFactory.h>

#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/PriceProvider.h>

#include <bb/trading/ITrader.h>
#include <bb/trading/trading.h>
#include <bb/trading/Order.h>

namespace bb {
class CommoditySpecification;
BB_FWD_DECLARE_SHARED_PTR(IBookBuilder);
BB_FWD_DECLARE_SHARED_PTR(IClientTimer);
BB_FWD_DECLARE_SHARED_PTR(BookBuilder);
BB_FWD_DECLARE_SHARED_PTR(SourceTickFactory);
BB_FWD_DECLARE_SHARED_PTR(MasterTickFactory);
BB_FWD_DECLARE_SHARED_PTR(MultipathTickFactory);
BB_FWD_DECLARE_SHARED_PTR(ITradeDemonClient);
BB_FWD_DECLARE_SHARED_PTR(ISOBook);
BB_FWD_DECLARE_SHARED_PTR(BsonLog);
BB_FWD_DECLARE_SHARED_PTR(XmlLog);

namespace trading {

BB_FWD_DECLARE_SHARED_PTR(BaseTrader);
BB_FWD_DECLARE_SHARED_PTR(IRefBookFactory);
BB_FWD_DECLARE_SHARED_PTR(RefData);
BB_FWD_DECLARE_SHARED_PTR(IRefDataFactory);
BB_FWD_DECLARE_SHARED_PTR(InstrumentContext);
BB_FWD_DECLARE_SHARED_PTR(IISOBookFactory);
BB_FWD_DECLARE_SHARED_PTR(IOTPositionProvider);
BB_FWD_DECLARE_SHARED_PTR(IssuedOrderTracker);
BB_FWD_DECLARE_SHARED_PTR(IPositionProvider);
BB_FWD_DECLARE_SHARED_PTR(IPositionProviderFactory);
BB_FWD_DECLARE_SHARED_PTR(IFeeProvider);
BB_FWD_DECLARE_SHARED_PTR(IFeeProviderFactory);
BB_FWD_DECLARE_SHARED_PTR(PnLProvider);
BB_FWD_DECLARE_SHARED_PTR(IOrderFilter);
BB_FWD_DECLARE_SHARED_PTR(LocateRequestClient);

class TradingContext;

namespace detail {
class object_factory;
} // namespace detail

/** Contains contextual information for trading
 *
 * The TradingContext gives access to all the facilities required for building and running a
 * trading agent.  A TradingContext is build on top of a ClientContext which in turn provides
 * access to the timebase and event processing features of the system.  A trading agent
 * also obtains handles to the order submission facility through this object, using the
 * ITrader interface.  Note the distinction between the getTrader() and getBaseTrader() methods,
 * the former returns an object that is implemented to keep track of incoming and out going
 * orders.  The BaseTrader is a raw interface to the trade daemon.
 *
 * dynamic properties:
 *   SourceTickFactory
 *   MasterTickFactory
 *   MultipathTickFactory
 */
class TradingContext
    : public IntrusiveWeakPtrHookable
{
public:
    /// Initializes the TradingContext.
    /// The BookBuilder will be used by anything trading-related that needs to create books.
    TradingContext( const ClientContextPtr& spCC, const IBookBuilderPtr& bookBuilder, acct_t acct );

    virtual ~TradingContext();

    // Initialization
    /// You must set a position provider factory before anything calls getPositionProvider().
    void setPositionProviderFactory( const IPositionProviderFactoryPtr& prov );

    /// You must set an ISOBook provider factory before anything calls getISOBook().
    void setISOBookFactory( const IISOBookFactoryPtr& factory );

    /// This function is called for every new instrument created to help init the InstrumentContext
    typedef boost::function< void ( const TradingContext& tc, InstrumentContext* ) > InstrumentInitializer;
    void setInstrumentInitializer( const InstrumentInitializer& init );

    virtual ITimeProviderCPtr getTimeProvider() const { return m_spCC->getClockMonitor(); }
    virtual ClockMonitorPtr getClockMonitor() const { return m_spCC->getClockMonitor(); }
    virtual IClientTimerPtr getClientTimer() const { return m_spCC->getClientTimer(); }

    /// Returns a PositionProvider for instr. This PosProvider keeps track of
    /// the position for instr.
    virtual IPositionProviderPtr getPositionProvider( const instrument_t& instr );
    /// Returns a issued order tracker, creating it if it doesn't exist.
    /// Will create the trader as well!
    virtual IssuedOrderTrackerPtr getIssuedOrderTracker( const instrument_t& instr );

    // Returns an ISOBook for the given instrument.
    virtual ISOBookPtr getISOBook( const instrument_t& instr );

    // Returns PnLProvider for the given instrument.
    virtual PnLProviderPtr getPnLProvider( const instrument_t& instr );

    virtual ITraderPtr getTrader( const instrument_t& instr );

    /// Returns the base trader that accepts, and attempts to place,
    /// issue order and cancel order requests.
    /// Returns a NULL BaseTraderPtr if child class hasn't created Trader yet.
    BaseTraderPtr getBaseTrader() const { return m_spBaseTrader; }

    // Returns the base trader, creating it if it hasn't been created already.
    BaseTraderPtr createTrader();

    LocateRequestClientPtr getLocateRequestClient();

    /// close the trading context
    virtual void close();

    /// Exit the simulation
    void exit();

    /// Returns the ClientContext that this TradingContext was built on.
    ClientContextPtr getClientContext() const { return m_spCC; }
    template<typename T> boost::shared_ptr<T> get( const std::string& name ) const { return m_spCC->get<T>( name ); }
    template<typename T> void set( const std::string& name, boost::shared_ptr<T> p ) { m_spCC->set( name, p ); }

    EventDistributorPtr const & getEventDistributor() const { return m_spCC->getEventDistributor(); }
    IBookBuilderPtr getBookBuilder() const { return m_spBookBuilder; }
    PriceProviderBuilderPtr getPriceProviderBuilder() const ;
    SourceTickFactoryPtr getSourceTickFactory() const;
    MasterTickFactoryPtr getMasterTickFactory();
    MultipathTickFactoryPtr getMultipathTickFactory();

    virtual const InstrumentContextPtr& getInstrumentContext( const instrument_t& instr );
    virtual const InstrumentContextCPtr getInstrumentContext( const instrument_t& instr ) const;

    /// Returns a PriceProvider
    virtual IPriceProviderCPtr getPriceProvider( const instrument_t& instr );

    acct_t getAccount() const { return m_acct; }
    int32_t getVerbose() const { return m_spCC->getVerbose(); }
    const std::string& getIDString() const { return m_spCC->getIDString(); }

    /// Returns the Xml log with the given ID suffix, creating if necessary. Overridden for unit testing
    virtual XmlLogPtr getLogPtr( const std::string& log_id );

    /// This can be dangerous if the TradingContext gets destroyed first; prefer getLogPtr().
    XmlLog& getLog( const std::string& log_id ) { return *getLogPtr(log_id); }

    /// Returns the BSON log with the given ID, creating if
    /// necessary. Like getLog above, getBsonLog can be dangerous;
    /// prefer getBsonLogPtr if possible.
    virtual BsonLogPtr getBsonLogPtr( const std::string& log_id );
    BsonLog& getBsonLog( const std::string& log_id ) { return *getBsonLogPtr(log_id); }
    void enableBufferedBsonLogging();
    void enableBufferedBsonLogging( ssize_t bufferSize );
    void setDelayBsonLogging( bool delay ){ m_bDelayBsonLogging = delay; }
    bool getDelayBsonLogging() const { return m_bDelayBsonLogging; }


    // Explicitly set the reference data for the instrument. This will
    // override any previously set reference data. If you set the
    // reference data here, then the reference data factory will not
    // be used to create reference data for this instrument.
    void setReferenceData( const instrument_t& instr, const RefDataPtr& rd );

    /// returns the reference data object, either the one set above by
    /// setReferenceData, or by invoking the reference data factory.
    const RefDataPtr& getReferenceData( const instrument_t& instr );

    // This method is deprecated, and simply calls
    // setReferenceDataFactory with a ConstantRefDataFactory
    // instantiated on the provided RefData. New code should not call
    // this method, and should use the setReferenceDataFactory method
    // below.
    void setDefaultReferenceData( const RefDataCPtr& rd );

    // Set the reference data factory for this trading context. The
    // factory will be used to construct reference data on demand,
    // unless explicit reference data for an instrument has been
    // provided with setReferenceData above.
    void setReferenceDataFactory( const IRefDataFactoryPtr& refDataFactory );

    // Returns the current reference data factory.
    const IRefDataFactoryPtr& getReferenceDataFactory() const;

    void setFeeProviderFactory( const IFeeProviderFactoryPtr& factory );
    const IFeeProviderPtr& getFeeProvider( const instrument_t& instr );

    void addOrderFilter( const IOrderFilterPtr& filter );

    typedef boost::function<mktdest_t( const instrument_t& )> PrimaryMktDestFunction;
    void setPrimaryMktDestFunction( const PrimaryMktDestFunction& func );
    mktdest_t getPrimaryMktDest( const instrument_t& instr ) const;

    virtual boost::optional<CommoditySpecification> getCommoditySpecification( const instrument_t& ) const;

    // create a new order
    Order* createOrder() { return getOrderFactory()->getObject(); }
    void setOrderFactoryConfig( ObjectFactoryConfig cfg );

protected:
    void setBaseTrader( const BaseTraderPtr& );
    friend class InstrumentContext;

    const IOTPositionProviderPtr& getIOTPositionProvider( const instrument_t& instr ) const;

    /// Creates a concrete child of BaseTrader and returns a shared pointer to it.
    virtual BaseTraderPtr createBaseTraderObj() = 0;
    const boost::shared_ptr<detail::object_factory>& getObjectFactory() const
    { return m_spObjectFactory; }

    OrderFactoryPtr  getOrderFactory();

protected:
    // objects external to libtrading
    ClientContextPtr m_spCC;
    IBookBuilderPtr m_spBookBuilder;

    mutable PriceProviderBuilderPtr m_spPxPBuilder;

    mutable SourceTickFactoryPtr m_spSourceTickFactory;
    MasterTickFactoryPtr m_spMasterTickFactory;
    MultipathTickFactoryPtr m_spMultipathTickFactory;

    // vars that need to be set by TradingSetup or equivalent
    acct_t m_acct;

    // vars created internally
    BaseTraderPtr m_spBaseTrader;

    LocateRequestClientPtr m_spLocateRequestClient;

public:
    typedef bbext::hash_map<instrument_t, InstrumentContextPtr> InstrumentContextMap;

    //read-only. lets the user see which instruments have been initialized so far
    const InstrumentContextMap& getInstrumentContextMap() const { return m_instrumentContexts; }

protected:
    mutable InstrumentContextMap m_instrumentContexts;

    typedef bbext::hash_map<std::string, XmlLogPtr> XmlLogMap;
    XmlLogMap m_xmlLogTable;

    typedef bbext::hash_map<std::string, BsonLogPtr> BsonLogMap;
    BsonLogMap m_bsonLogTable;

    boost::shared_ptr<detail::object_factory> m_spObjectFactory;
    InstrumentInitializer m_initializer;

    ssize_t m_bsonBufferSize;
    bool m_bDelayBsonLogging;
    OrderFactoryPtr       m_spOrderFactory;
};
BB_DECLARE_SHARED_PTR( TradingContext );

class HistTradingContext
    : public TradingContext
{
public:
    HistTradingContext( const HistClientContextPtr& spHistCC, const IBookBuilderPtr& bookBuilder, acct_t acct,
            const BaseTraderPtr& spDefaultTrader = BaseTraderPtr() );

    virtual ~HistTradingContext();

    virtual HistMStreamManagerPtr getHistMStreamManager();
    void initTrader( const BaseTraderPtr& );
    HistClientContextPtr getHistClientContext();

protected:
    HistTradingContext( const ClientContextPtr& spHistCC, const IBookBuilderPtr& bookBuilder, acct_t acct );

private:
    virtual BaseTraderPtr createBaseTraderObj();
};
BB_DECLARE_SHARED_PTR( HistTradingContext );


class LiveTradingContext
    : public TradingContext
{
public:
    LiveTradingContext( const LiveClientContextPtr& spLiveCC, const IBookBuilderPtr& bookBuilder, acct_t acct );

    virtual ~LiveTradingContext();

    LiveMStreamManagerPtr getLiveMStreamManager()
    {
        return boost::dynamic_pointer_cast<LiveMStreamManager>( getClientContext()->getMStreamManager() );
    }

private:
    /// Creates a LiveTrader, and attempts to connect it to trdserver.
    /// The LiveTrader is created whether or not connection succeeds,
    /// so to check whether connection succeeded you have to explicitly
    /// check whether the returned Trader isConnected().
    /// Resets m_spBaseTrader to own the created LiveTrader.
    /// Returns m_spBaseTrader.
    virtual BaseTraderPtr createBaseTraderObj();
};
BB_DECLARE_SHARED_PTR( LiveTradingContext );


/// TradingContext which runs live but sends no order to the market, useful for testing.
class StubTradingContext
    : public LiveTradingContext
{
public:
    StubTradingContext( const LiveClientContextPtr& spLiveCC, const IBookBuilderPtr& spBookBuilder, acct_t acct );

private:
    virtual BaseTraderPtr createBaseTraderObj();
};
BB_DECLARE_SHARED_PTR( StubTradingContext );

/// TradingContext which runs histo but sends no orders to the simulator, useful for profiling.
class StubHistTradingContext
    : public HistTradingContext
{
public:
    StubHistTradingContext( const HistClientContextPtr& spHistCC, const IBookBuilderPtr& spBookBuilder, acct_t acct );

private:
    virtual BaseTraderPtr createBaseTraderObj();
};
BB_DECLARE_SHARED_PTR( StubHistTradingContext );

class TradingContextFactory {
public:

    enum OrderRoutingMode {
        kRoute,
        kDoNotRoute,
    };

    class Config : public LuaConfig<Config> {
    public:

        Config();

        Config& setClientContextFactory( const ClientContextPtr& );
        Config& setClientContextFactoryConfig( const ClientContextFactory::Config& );
        Config& createBookBuilderFromClientContext();
        Config& setBookBuilder( IBookBuilderPtr& );
        Config& setAccount( const acct_t& );
        Config& setPositionProviderFactory( const IPositionProviderFactoryPtr& );
        Config& setOrderRoutingMode( OrderRoutingMode );
        Config& setOrderFactoryConfig( ObjectFactoryConfig cfg );


        ClientContextPtr m_clientContext;
        IBookBuilderPtr m_bookBuilder;
        acct_t m_acct;
        IPositionProviderFactoryPtr m_positionProviderFactory;
        OrderRoutingMode m_orderRoutingMode;
        ObjectFactoryConfig  m_orderFactoryConfig;
        static void describe();
    };

    static TradingContextPtr create( const Config& );
    static TradingContextPtr create( const luabind::object& );

    static TradingContextPtr create(
        const ClientContextPtr& spCC,
        const IBookBuilderPtr& spBookBuilder,
        acct_t acct,
        const IPositionProviderFactoryPtr& positionProviderFactory,
        OrderRoutingMode routingMode = kRoute,
        const ObjectFactoryConfig orderFactoryConfig = ObjectFactoryConfig() );

    static TradingContextPtr create(
        const ClientContextPtr& spCC,
        const IBookBuilderPtr& spBookBuilder,
        acct_t acct,
        OrderRoutingMode routingMode = kRoute,
        const ObjectFactoryConfig orderFactoryConfig = ObjectFactoryConfig() );
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_TRADINGCONTEXT_H
