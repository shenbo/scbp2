#ifndef BB_TRADING_IPNLLISTENER_H
#define BB_TRADING_IPNLLISTENER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include "PnLTracker.h"

#include <iostream>
#include <set>
#include <sstream>

#include <boost/foreach.hpp>

#include <bb/trading/TradingContext.h>
#include <bb/trading/IssuedOrderTracker.h>
#include <bb/trading/IFillListener.h>
#include <bb/trading/PnLTracker.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/db/CurrencyInfo.h>


namespace bb {
namespace trading {

class IPnLListener
{
    public:
        virtual ~IPnLListener() {}

        /// Called when a Fill occurs. fill_qty is absolute value of size of fill.
        virtual void onRealizedPnLChange( const PnLProvider* pnl_provider, const OrderPtr& order ) = 0;
};
BB_DECLARE_SHARED_PTR( IPnLListener );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_IPNLLISTENER_H
