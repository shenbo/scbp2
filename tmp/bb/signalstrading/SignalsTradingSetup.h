#ifndef BB_SIGNALSTRADING_SIGNALSTRADINGSETUP_H
#define BB_SIGNALSTRADING_SIGNALSTRADINGSETUP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/signals/SignalsSetup.h>
#include <bb/trading/TradingSetup.h>

namespace bb {
namespace signalstrading {

// SignalsTradingSetup joins SignalsSetup and TradingSetup, each of which derives from ClientCoreSetup.
// Using this class gets you the combined functionality of all the base classes in a coordinated way.
class SignalsTradingSetup : public signals::SignalsSetup, public trading::TradingSetup
{
public:
    SignalsTradingSetup( int argc, char** argv
        , const trading::ITradingContextFactoryPtr& f = trading::ITradingContextFactoryPtr() );

    /// Creates TradingContext, either Hist or Live depending on runlive.
    virtual void setup();

    /// Delegates to TradingSetup::setupSymbols.
    virtual bool setupSymbols();

    /// Start the event pump
    virtual void run();

protected:
    virtual void addOptions( boost::program_options::options_description& allOptions );

    virtual void printSettings( bool endl_fg = true );
};

} // namespace signalstrading
} // namespace bb

#endif // BB_SIGNALSTRADING_SIGNALSTRADINGSETUP_H
