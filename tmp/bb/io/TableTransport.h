#ifndef BB_IO_TABLETRANSPORT_H
#define BB_IO_TABLETRANSPORT_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/io/SendTransport.h>

#include <boost/unordered_map.hpp>

namespace bb {

class TableTransport : public ISendTransport {
        // This Transport acts just like a regular lookup table with SYM_ALL
        // support.
        //
        // Usage: Simple usage of `TableTransport`.
        //- - - - - - - - - - - - - - - - - - - - -
        // Lets say you have a transport that you want to act only on `XLE` symbol,
        // you can bind this symbol to your transport by using `TableTransport`.
        //
        // First, create your transport.
        //..
        //  ISendTransportPtr yourTransportPtr = yourLovelyFactoryFunciton();
        //..
        // Next, create an instance of `TableTransport`.
        //..
        //  bb::TableTransportPtr tableTransportPtr =
        //      boost::make_shared<bb::TableTransport>();
        //..
        // Then, call `bind` member function on `tableTransportPtr`.
        //..
        //  tableTransportPtr->bind(symbol_t("XLE"), yourTransportPtr);
        //..
        // Now, you can call a `send` member function on `TableTransport`,
        // it will redirect it appropriatelly.
        //..
        //

    public:
        // To make desing more configurable, we cannot assume one-to-one mapping
        typedef boost::unordered_map<bb::symbol_t,
                                     std::vector<ISendTransportPtr> > SymToTransportTable;

        // CONSTRUCTORS
        TableTransport();

        explicit TableTransport(SymToTransportTable const& symbolToTransportTable);

        virtual ~TableTransport();

        // MODIFIERS

            // Send message to underlying transports.
        virtual void send( Msg const* msg );

            // Bind `transport` to `symbol`.
        void bind(bb::symbol_t const& symbol, ISendTransportPtr const& transport);

            // Add every element of specified `table` to underlying lookup
            // table.
        void bind(SymToTransportTable const& table);

            // Bind specified `transport` to every symbol in `symbols`.
        void bind(std::vector<bb::symbol_t> const& symbols, ISendTransportPtr const& transport);

            // Bind every transport in `transports` to a specified `symbol`.
        void bind(symbol_t const& symbol, std::vector<ISendTransportPtr> const& transports);

        // unbind? or yagne?

    private:
        SymToTransportTable m_symbolToTransportTable;
        std::vector<ISendTransportPtr> m_allSymbolTransports;
};
BB_DECLARE_SHARED_PTR( TableTransport );


} // close namespace bb

#endif // BB_IO_TABLETRANSPORT_H
