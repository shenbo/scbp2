#ifndef BB_IO_DFSEARCHREADER_H
#define BB_IO_DFSEARCHREADER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/scoped_array.hpp>
#include <boost/tuple/tuple.hpp>

#include <bb/core/source.h>

#include <bb/io/ByteSource.h>
#include <bb/io/SingleDFStream.h>
#include <bb/io/NonBlockingByteSource.h>


namespace bb {


BB_FWD_DECLARE_SHARED_PTR( CFile );
BB_FWD_DECLARE_SHARED_PTR( ZByteSource );
BB_FWD_DECLARE_SHARED_PTR( ByteSource );

namespace detail {
// allows stuffing bytes in front of a wrapped bytesource; useful for DFSearchReader
class PrependedByteSource : public ByteSource
{
public:
    PrependedByteSource( ByteSourcePtr source );
    virtual size_t ByteSource_read( void* buf, size_t size );
    void setBuf( const char* buf_ptr, size_t used );

private:
    ByteSourcePtr m_source;
    const char* m_buf_ptr;
    size_t m_buf_used;
};
BB_DECLARE_SHARED_PTR( PrependedByteSource );
}

/// Converts a stream of bytes to a stream of messages.
class DFSearchReader
    : public SingleDFStream
{
public:
    typedef boost::tuple<CFilePtr, bool, source_t> FileStreamInfo;

public:
    /// Takes in a valid input stream.  Can throw an HistMStream::Exception.
    DFSearchReader( const FileStreamInfo& info, const ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

    /// Returns the first message with time_sent at or after the given time.
    /// assumes a new file
    void search( timeval_t time );

    virtual const Msg* next();

    source_t getSourceUsed() const;

    void initSearchContext( const FileStreamInfo& info );

    class SearchContext
    {
    public:
        SearchContext( const FileStreamInfo& info );
        void reset();
        bool isGZipped() const;
        bool syncZSource();

        size_t m_scanSize;
        size_t m_msgAlign; // minimum message alignment
        CFilePtr m_file;

        // gzip support
        ZByteSourcePtr m_zSource;
        ByteSourcePtr m_nonBlockingByteSource;
        detail::PrependedByteSourcePtr m_prependedSource;

        // we need this to adjust our binary bounds better for gzip
        size_t m_bytesSkipped;

        // source used to locate the file
        source_t m_sourceUsed;
    };
    BB_DECLARE_SHARED_PTR( SearchContext );

protected:
    boost::scoped_array<char> m_buf;

    const Msg* m_repeat_msg;

    SearchContextPtr m_spSearchContext;
    bool m_bFirstMsgFound;
    bool m_bInSearch;

private:
    /// Returns the next message by iterating through m_buf, m_msgAlign bytes at a time, using a heuristic.
    const Msg* scan();
    static const char* findHeader( const boost::scoped_array<char>& buf
        , int32_t msg_algn, size_t got );
    void search_imp( timeval_t time );

    /// do binary search on current search context
    /// assumes context is initialized at 0
    /// returns the next position to begin retrieving the next message
    /// throws Error on "unsupported format", i.e. older gzip format -- correct
    /// action is to call "next()" instead of "scan()"
    size_t binarySearch( timeval_t search_time, timeval_t& begin_time, timeval_t& end_time );

    /// We will want to see if the first timeval we encounter in the data file is larger already
    /// than the one we're searching for. If so, we can skip searching completely and can start to
    /// read from the start of the file. Apparently this is not typically the case in the US, but
    /// it is in Asia.
    bool searchNeeded( const timeval_t search_time );

    static const size_t s_syncSize;
    friend class SearchContext;
};
BB_DECLARE_SHARED_PTR( DFSearchReader );


} // namespace bb

#endif // BB_IO_DFSEARCHREADER_H
