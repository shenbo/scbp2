#ifndef BB_IO_MQRECVTRANSPORT_H
#define BB_IO_MQRECVTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/MsgBuffer.h>
#include <bb/core/Msg.h>
#include <bb/core/FDSetFwd.h>
#include <bb/core/MStreamCallback.h>
#include <bb/core/Subscription.h>

#include <bb/io/IRecvTransport.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( MQByteSource );

/// Receives BB messages from an MQ.
class MQRecvTransport : public IRecvTransport
{
public:
    MQRecvTransport(MQByteSourcePtr s);

    /// IRecvTransport impl
    virtual void beginDispatch(FDSet &d, IMStreamCallback *messageCB,
            const EndOfStreamCB &endOfStreamCB = EndOfStreamCB(),
            const ErrorCB &errorCB = ErrorCB());
    virtual void endDispatch();

protected:
    /// Returns a Msg. NULL indicates an end of stream.
    /// Throws a CError exception if an error occurred.
    const Msg *recv();

    class ReadyCallback;
    void onSelectCallback();

    MQByteSourcePtr m_source;
    MsgBuffer m_msgBuf;

    Subscription m_readSub;
    IMStreamCallback *m_messageCB;
    EndOfStreamCB m_endOfStreamCB;

};
BB_DECLARE_SHARED_PTR( MQRecvTransport );


} // namespace bb

#endif // BB_IO_MQRECVTRANSPORT_H
