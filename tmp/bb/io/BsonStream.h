#ifndef BB_IO_BSONSTREAM_H
#define BB_IO_BSONSTREAM_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <iostream>
#include <mongo/bson/bson.h>

#include <bb/io/ZByteSource.h>

#include <boost/shared_array.hpp>
#include <boost/format.hpp>

namespace mongo {
BB_DECLARE_SHARED_PTR( BSONObj );
} // namespace mongo

namespace bb {

class BsonStream { // does not inherit from Mstream since the bson message is not a bb message
public:

    struct Exception : virtual public std::exception
    {
        Exception() {}
        Exception(std::string const &err) : m_fullErrBuf(err) {}
        ~Exception() throw() {} //all exceptions call throw std::unexpected here

        virtual const char *what() const throw() { return m_fullErrBuf.c_str(); }

        std::string m_fullErrBuf;
    };

    BsonStream( std::string filename );
    BsonStream( std::istream& cin );
    mongo::BSONObjPtr next();

    virtual ~BsonStream();

protected:
    virtual void onError( boost::format& f );

    ByteSourcePtr m_bs;

private:
    std::string m_filename;
    boost::shared_array<char> m_buffer;

};

BB_DECLARE_SHARED_PTR( BsonStream );


}

#endif // BB_IO_BSONSTREAM_H
