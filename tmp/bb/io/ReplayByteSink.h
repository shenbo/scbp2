#ifndef BB_IO_REPLAYBYTESINK_H
#define BB_IO_REPLAYBYTESINK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <deque>
#include <string>

#include <bb/core/bbassert.h>
#include <bb/core/smart_ptr.h>

#include <bb/io/ByteSink.h>

namespace bb {

// A ReplayByteSink records all writes as strings in an internal
// queue. Those recorded strings can be dequeued via the provided
// queue interface. This is a useful gadget when you are writing a
// unit test for a component that will write to a ByteSink and you
// want to verify what was written.

class ReplayByteSink : public ByteSink
{
    typedef std::deque<std::string> string_queue_t;

public:
    inline ReplayByteSink()
        : m_queue() {}

    virtual ~ReplayByteSink() {}

    // A no-op, since we don't do any IO.
    virtual void flush() {}

    // Queue interface:

    // Return the front of the queue, which must not be empty.
    inline const std::string& front() const
    {
        BB_ASSERT( !empty() );
        return *begin();
    }

    // Pop the front element from the queue, which must not be empty.
    inline void pop()
    {
        BB_ASSERT( !empty() );
        m_queue.pop_front();
    }

    // Return the size of the queue.
    size_t size() const
    {
        return m_queue.size();
    }

    // Returns true iff the queue is empty.
    inline bool empty() const
    {
        return size() == 0;
    }

    // Iterator interface:
    inline string_queue_t::const_iterator begin() const
    {
        return m_queue.begin();
    }

    inline string_queue_t::const_iterator end() const
    {
        return m_queue.end();
    }

    inline void clear()
    {
        m_queue.clear();
    }

protected:
    virtual size_t ByteSink_write(const void* buf, size_t size)
    {
        std::string record;
        record.append( reinterpret_cast<const char*>(buf), size );
        m_queue.push_back( record );
        return size;
    }

private:
    string_queue_t m_queue;
};
BB_DECLARE_SHARED_PTR(ReplayByteSink);

} // namespace bb

#endif // BB_IO_REPLAYBYTESINK_H
