#ifndef BB_IO_STEPLOGGER_H
#define BB_IO_STEPLOGGER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <memory>

#include <boost/container/vector.hpp>
#include <boost/foreach.hpp>
#include <boost/noncopyable.hpp>

#include <bb/core/timeval.h>
#include <bb/io/BsonLog.h>

// StepLogger:
// A custom class that will let us log 'per order stage' timings.
//
// You'll typically use this class when determining how much time each step of a strategy
// process is taking - for instance between the time a msg gets passed through the event processor,
// all the way down to it being sent out there could be dozens of steps in between. This class lets
// us log times at pre-defined ( custom labeled ) checkpoints.
//
// By default ( as this is sloooww ) this will be disabled,
//
//             ** #define STEP_LOGGING_ENABLED **
//
// to turn this functionality on. Afterwards, sample usage:
//
//  STARTLOGSTEP("MarginRemoveOrderMsg")
//  LOGSTEP("hello")
//  LOGSTEP("world")
//  LOGSTEP("foo")
//  LOGSTEP("bar")
//  ENDLOGSTEP()
//
//  Will create a log entry:
//  { "MarginRemoveOrderMsg" : { "hello" : <time>, ... }
//
//  Better yet, SCOPED logging:
//  SCOPEDSTARTLOGSTEP("SomeOtherMessage");
//  LOGSTEP("Some Step")
//  LOGSTEP("Some other Step")
//
//  This will add ENDLOGSTEP() automatically when we leave the current scope

namespace bb {

    // We create a new 'log step', that is a:
    // - human readable name, say 'signal begin'
    // - timeval_t when this step was 'hit'
    class LogStep : private boost::noncopyable
    {
        public:
            template<size_t keySize>
            LogStep( const char (&key)[keySize] )
                : m_value ( bb::detail::BsonKeyValue<bb::timeval_t>(&key[0], keySize -1, bb::timeval_t::now ) )
            {
            }

            LogStep(BOOST_RV_REF(LogStep) other)            // Move ctor
            : m_value ( other.m_value )
            {
            }

            LogStep& operator=(BOOST_RV_REF(LogStep) other) // Move assign
            {
                const_cast<const char*&>(m_value.m_key) = other.m_value.m_key;
                const_cast<size_t&>(m_value.m_keyStrLen) = other.m_value.m_keyStrLen;
                const_cast<bb::timeval_t&>(m_value.m_val) = other.m_value.m_val;
                return *this;
            }

            void write ( bb::BsonLog::Record & record ) const
            {
                record << m_value;
            }
        private:
            BOOST_MOVABLE_BUT_NOT_COPYABLE(LogStep)
            bb::detail::BsonKeyValue<bb::timeval_t> m_value;
    };
    typedef boost::container::vector<LogStep> LogStepVct;

    class StepLogger;
    // A LogLine is a new entry in the bson file. It contains all intermediate 'steps' that make
    // up an order trail.
    class LogLine : private boost::noncopyable
    {
        public:
            LogLine ( StepLogger & parent, const char * name, bb::BsonLog & bson );
            ~LogLine();

            const char * name() const;

            // We add a 'step' to the current 'line':
            // - A step is a combination of human readable name, say 'signal begin' and a bb::timeval_t
            // - It is appended to the current 'line', the collection of all steps for this 'order trail'.
            template<size_t keySize>
            LogLine & addStep ( const char (&key)[keySize] )
            {
                m_steps.emplace_back(key);
                return *this;
            }

            void endLine();
        private:
            StepLogger & m_parent;
            const char * m_name;
            bb::BsonLog & m_bson;
            LogStepVct m_steps;
    };

    // This is the 'main' StepLogger class that will:
    // - open a BsonLog
    // - return a new/existing BSON record per 'order trail',
    //   this record will be appended to the currently opened log.
    // - close subh BSON records on request
    // The instance() method will:
    // - ( the first time )open a BsonLog: 'step_timing..%H%M%S.%Y%m%d.bson'
    // - return a reference to this StepLogger.
    class StepLogger : private boost::noncopyable
    {
        public:
            friend class LogLine;
            static StepLogger & instance();
            StepLogger( std::ostream& stream,
                    bb::BsonLog::OutputMode outputMode = bb::BsonLog::kOutputModeBSON );
            StepLogger ( const char * fn = "step_timing" );
            ~StepLogger();

            LogLine & getLine( const char * name = "steps" );
            void endLine();
            std::string const & getFileName() const;
            void close();
        private:
            std::auto_ptr<LogLine> m_currentLine;
            std::string m_fileName;
            std::auto_ptr<std::fstream> m_fStream;
            bb::BsonLog m_bson;
    };
}

#ifdef STEP_LOGGING_ENABLED
    #define SCOPEDSTARTLOGSTEP(NAME) \
        struct on_scope_exit_t { \
            on_scope_exit_t() { \
                bb::StepLogger::instance().getLine(NAME); \
            } \
            ~on_scope_exit_t() { \
                bb::StepLogger::instance().endLine(); \
            } \
        } on_scope_exit;

    #define STARTLOGSTEP(NAME) bb::StepLogger::instance().getLine(NAME);
    #define LOGSTEP(DESCRIPTION) bb::StepLogger::instance().getLine().addStep(DESCRIPTION);
    #define ENDLOGSTEP() bb::StepLogger::instance().endLine();
#else
    #define SCOPEDSTARTLOGSTEP(NAME)
    #define STARTLOGSTEP(NAME)
    #define LOGSTEP(DESCRIPTION)
    #define ENDLOGSTEP()
#endif // STEP_LOGGING_ENABLED

#endif // BB_IO_STEPLOGGER_H
