#ifndef BB_IO_QUOTESUBSCRIPTIONMGR_H
#define BB_IO_QUOTESUBSCRIPTIONMGR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <list>
#include <boost/function.hpp>
#include <boost/thread.hpp>
#include <bb/core/queue/waitable_queue.h>
#include <bb/core/queue/ThreadPipeNotifier.h>
#include <bb/core/SubscriptionTable.h>
#include <bb/core/Msg.h>
#include <bb/core/QuitMonitor.h>
#include <bb/core/network.h>
#include <bb/core/Notification.h>

namespace bb {
class FDSet;
class SelectDispatcher;
class TCPSocket;

/*
 * Server that handles quote subscriptions
 *
 * Spawns a thread to handle clients that send subscriptions for mtype/instr(sym) pairs.
 * Aggregates subscriptions to generate a single add/remove event per mtype/instr pair
 * when clients subscribe/unsubscribe to a (mtype,instr) pair.
 *
 * Currently, the resolution for instr is on the symid level.  You cannot subscribe to
 * (all-mtype,all-instr) or (all-mtype, instr), but you <em>CAN</em> subscribe to
 * (mtype, all-instr).
 *
 * Classes derived from this MUST call stop() in their destructors to prevent the pure
 * virtual methods postAddEvent/postRemoveEvent from being called in the base destructor.
 *
 * The constructor of the QuoteSubscriptionMgr object takes two callbacks: onAdd and onRemove.
 * The onAdd callback is called for each mtype/instr pair when the number of subscriptions
 * becomes non-zero.  The onRemove callback is called for each mtype.instr when the number of
 * subscriptions for the pairs is zero (all subscriptions have been canceled).
 *
 */
class QuoteSubscriptionMgr
{
public:
    typedef boost::function<void(mtype_t,instrument_t)>  SubscriptionCallback;

    QuoteSubscriptionMgr( const std::vector<int16_t>& ports, const std::string& logPrefix,
                          SubscriptionCallback onAddCb, SubscriptionCallback onRemoveCb,
                          SubscriptionCallback onSubscribeCb = SubscriptionCallback() );
    virtual ~QuoteSubscriptionMgr();

    void start();
    void stop(); // must be called between start() and ~QuoteSubscriptionMgr()

    // Get the address where this QuoteSubscriptionMgr is listening
    // for connections. If the server has not been started, or is not
    // currently running, the 'port()' value of the returned object
    // will be zero.
    inline const sockaddr_ipv4_t& getListenAddress() const
    {
        return m_listenAddress;
    }

private:
    void serverRun();
    void onConnection( TCPSocket* serversock, SelectDispatcher* );

    void onClientAdd( mtype_t mtype, instrument_t instr );
    void onClientRemove( mtype_t mtype, instrument_t instr );

    BB_FWD_DECLARE_SHARED_PTR( ConnectionHandler );
    typedef std::list<ConnectionHandlerPtr> ConnectionHandlerList;

    void removeConnection( ConnectionHandlerList::iterator it );

    std::vector<int16_t>             m_ports;
    ThreadQuitHook                   m_quitHook;
    boost::shared_ptr<boost::thread> m_spServerThread;

    SubscriptionTable                m_subscriptions;
    SubscriptionCallback             m_subscribeHook;
    ConnectionHandlerList            m_connections;
    std::string                      m_logPrefix;
    size_t                           m_connectionIndex;
    sockaddr_ipv4_t                  m_listenAddress;
    Notification                     m_started;
};
BB_DECLARE_SHARED_PTR( QuoteSubscriptionMgr );


/*
 * Some applications (but not typical QDs) may wish to use this extension of QuoteSubscriptionMgr,
 * which adds an internal queue for events to be processed in the context of a SelectDispatcher
 * (which is passed to the constructor).
 */
class QuoteSubscriptionMgrWithQueue : public QuoteSubscriptionMgr
{
public:
    QuoteSubscriptionMgrWithQueue( const std::vector<int16_t>& ports, const std::string& logPrefix, FDSet& fdSet );
    virtual ~QuoteSubscriptionMgrWithQueue();

protected:
    virtual void onAddEvent( mtype_t mtype, instrument_t instr ) = 0;
    virtual void onRemoveEvent( mtype_t mtype, instrument_t instr ) = 0;

    void postAddEvent( mtype_t mtype, instrument_t instr );
    void postRemoveEvent( mtype_t mtype, instrument_t instr );

    void onEventMsg();

    typedef boost::function<void ()> caller_t;
    bb::queue::waitable_concurrent_queue<caller_t, bb::queue::ThreadPipeNotifier> m_queue;
    bb::Subscription m_subDispatch;
};
BB_DECLARE_SHARED_PTR( QuoteSubscriptionMgr );

} // namespace bb

#endif // BB_IO_QUOTESUBSCRIPTIONMGR_H
