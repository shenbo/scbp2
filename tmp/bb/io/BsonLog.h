#ifndef BB_IO_BSONLOG_H
#define BB_IO_BSONLOG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <cassert>
#include <cstring>
#include <iosfwd>
#include <stack>
#include <string>

#include <boost/range/iterator_range.hpp>
#include <boost/interprocess/interprocess_fwd.hpp>
#include <boost/noncopyable.hpp>
#include <boost/scoped_array.hpp>
#include <boost/variant.hpp>
#include <boost/version.hpp>

#include <bb/core/tif.h>
#include <bb/core/acct.h>
#include <bb/core/cxlstatus.h>
#include <bb/core/fillflag.h>
#include <bb/core/instrument.h>
#include <bb/core/liquidity.h>
#include <bb/core/ostatus.h>
#include <bb/core/side.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/symbol.h>
#include <bb/core/tid.h>

namespace mongo {
class BSONArrayBuilder;
class BSONObj;
class BSONObjBuilder;

#if BOOST_VERSION >= 104800
class TrivialAllocator;
template<class Allocator> class _BufBuilder;
typedef _BufBuilder<TrivialAllocator> BufBuilder;
#else
class BufBuilder;
#endif

} // namespace mongo

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(ByteSink);
BB_FWD_DECLARE_SHARED_PTR(ITimeProvider);

namespace db {
BB_FWD_DECLARE_SHARED_PTR(IConnectionInfo);
} // namespace db

namespace detail {
template<typename T> struct BsonKeyValue;
struct literal_value_tag;
template<> struct BsonKeyValue<literal_value_tag>;
template<typename T, class Enable_one = void, class Enable_two = void> struct BsonValuePrinter;
} // namespace detail

class BsonLog : public boost::noncopyable
{
public:

    // The BSON log is intended to output BSON (binary). However, for
    // unit testing, it is often useful to have the BSON log output
    // human readable (and verifiable) data, or write nothing. Specify
    // an output mode of kOutputModeJson to have the BSON log convert
    // output to JSON format before writing to the underlying output.

    // Note that the BSON log does NOT flush the output stream after
    // writing each record. If it is important to you that the log be
    // flushed after each write, either disable buffering on your
    // ByteSink or ostream, or provide a wrapper which calls flush as
    // part of implementing the 'write' call.

    enum OutputMode
    {
        kOutputModeNone = 0,  // Write no data to the output.
        kOutputModeBSON = 1,  // Write binary (BSON) data to the output
        kOutputModeJSON = 2   // Write JSON to the output (slow!, testing only)
    };

    // Construct a BsonLog which logs to the provided stream. The
    // lifetime of the stream object must exceed the lifetime of the
    // BsonLog.
    BsonLog( std::ostream& stream,
             OutputMode outputMode = kOutputModeBSON );

    // Construct a BsonLog which logs to the provided sink.
    BsonLog( const ByteSinkPtr& sink,
             OutputMode outputMode = kOutputModeBSON );

    // Construct with no output set. You must call 'setOutput' after
    // calling this constructor (which you can only do as a
    // subclass). This exists to make life easier for subclasses that
    // want to compute a filename at construction time. This way they
    // don't have to cram that logic into their initializer list, and
    // can call setOutput from the body of the constructor
    // instead. Its also useful to create a dummy log that will never
    // write output.
    BsonLog();

    virtual ~BsonLog();

    // If a time provider is set, then the log will record timestamps
    // on top level records. XmlDatedFileLog requires virtual dispatch
    // from XmlLog to do this, we save that overhead here.
    void setTimeProvider( const ITimeProviderPtr& timeProvider );
    inline const ITimeProviderPtr& timeProvider() const
    {
        return m_timeProvider;
    }

    class RecordBase;
    friend class RecordBase;

    class Record;
    friend class Record;

    class ArrayRecord;
    friend class ArrayRecord;

    // Overloads for all of the types we can write. We do this so we
    // can keep the mongo headers isolated in the .cc file. We can't
    // use a template since we want the definition to not be visible.
    //
    // If you're tempted to use templates anyway, please be aware that
    // the header doesn't actually reference mongo headers *at the moment*.
    // That's a good thing we'd be giving up if we shift the implementation
    // to this header.
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, const char* );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, const char*, size_t );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, const std::string& );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, double );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, int );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, int64_t );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, unsigned int );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, size_t );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, bool );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<std::string> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<const char *> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<bool> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<double> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<float> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<int8_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<uint8_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<int16_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<uint16_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<int32_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::list<uint32_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<std::string> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<const char *> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<bool> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<double> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<float> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<int8_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<uint8_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<int16_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<uint16_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<int32_t> const & );
    static void append( mongo::BSONObjBuilder&, const char* key, size_t keyStrLen, std::vector<uint32_t> const & );

protected:
    // Set the logger's output to the given object.
    void setOutput( std::ostream& stream,
                    OutputMode outputMode = kOutputModeBSON );

    void setOutput( const ByteSinkPtr& sink,
                    OutputMode outputMode = kOutputModeBSON );

    // Set the logger to have no output.
    void dropOutput();

private:
    template<typename T, class Enable_one, class Enable_two> friend struct detail::BsonValuePrinter;

    // Deferred constructor work.
    void init();

    // We cache and re-use an ostream for the generic value printing case
    // because constructing ostreams is stupidly expensive, mostly due
    // to locale construction.
    typedef boost::interprocess::basic_obufferstream< char > obstream;

    template<typename T>
    inline void convertWithCachedStream( const detail::BsonKeyValue<T>& t, mongo::BSONObjBuilder& builder );

    mongo::BSONObjBuilder* createRootObjBuilder( mongo::BufBuilder& builder );
    static mongo::BSONObjBuilder* createObjBuilder( mongo::BufBuilder& builder );

    static mongo::BSONObjBuilder* startSubObj( mongo::BSONObjBuilder& parent, const std::string& tag );
    static mongo::BSONObjBuilder* startSubObj( mongo::BSONObjBuilder& parent, const char* tag, size_t tagStrLen );
    static mongo::BSONObjBuilder* startSubObj( mongo::BSONArrayBuilder& parent, size_t id );

    static mongo::BSONArrayBuilder* startSubArray( mongo::BSONObjBuilder& parent, const std::string& tag );
    static mongo::BSONArrayBuilder* startSubArray( mongo::BSONObjBuilder& parent, const char* tag, size_t tagStrLen );
    static mongo::BSONArrayBuilder* startSubArray( mongo::BSONArrayBuilder& parent, size_t id );

    inline void writeObject( const mongo::BSONObj& );

    mongo::BufBuilder* getCachedBuffer();
    inline void returnCachedBuffer( mongo::BufBuilder* );

    typedef boost::variant<std::ostream*, ByteSinkPtr > Output;

    struct OutputDropper : public boost::static_visitor<>
    {
        void operator()(std::ostream* os) { os = nullptr; }
        void operator()(ByteSinkPtr& ptr) { ptr.reset(); }
    };

    Output m_output;
    OutputMode m_outputMode;
    ITimeProviderPtr m_timeProvider;
    static const size_t kBufferStreamSize;
    boost::scoped_array<char> m_bufferStreamStorage;
    boost::scoped_ptr<obstream> m_bufferStream;

    typedef std::stack< mongo::BufBuilder* > BufBuilderCache;
    BufBuilderCache m_bufferCache;
};
BB_DECLARE_SHARED_PTR(BsonLog);

class BsonLog::RecordBase
{
protected:
    inline RecordBase( BsonLog& log )
        : m_log( log ) {}

    inline BsonLog& log()
    {
        return m_log;
    }

private:
    BsonLog& m_log;
};

class BsonLog::Record : public BsonLog::RecordBase
{
    friend class BsonLog::ArrayRecord;

    template<typename T>
    friend const Record& operator<<( const Record&, const detail::BsonKeyValue<T>& );

public:
    inline Record( BsonLog& parent, const char* tag )
        : RecordBase( parent )
        , m_buffer( log().getCachedBuffer() )
        , m_root( parent.createRootObjBuilder( *m_buffer ) )
        , m_builder( BsonLog::startSubObj( *m_root, tag, strlen( tag ) ) )
    {
        writeObjectHeader();
    }

    inline Record( BsonLog& parent, const std::string& tag )
        : RecordBase( parent )
        , m_buffer( log().getCachedBuffer() )
        , m_root( parent.createRootObjBuilder( *m_buffer ) )
        , m_builder( BsonLog::startSubObj( *m_root, tag ) )
    {
        writeObjectHeader();
    }

    inline Record( BsonLog::Record& parent, const char* tag )
        : RecordBase( parent.log() )
        , m_buffer( NULL )
        , m_root()
        , m_builder( BsonLog::startSubObj( parent.builder(), tag, strlen( tag ) ) ) {}

    inline Record( BsonLog::Record& parent, const std::string& tag )
        : RecordBase( parent.log() )
        , m_buffer( NULL )
        , m_root()
        , m_builder( BsonLog::startSubObj( parent.builder(), tag ) ) {}

    // Defined out-of-line below so it can see ArrayRecord.
    inline Record( BsonLog::ArrayRecord& parent );

    ~Record();

protected:
    inline mongo::BSONObjBuilder& builder() const
    {
        return *m_builder;
    }

private:
    template<typename T>
    inline Record& append( const detail::BsonKeyValue<T>& kv )
    {
        detail::BsonValuePrinter< detail::BsonKeyValue<T> >::print( log(), builder(), kv );
        return *this;
    }

    void writeObjectHeader();

    // Only used if parent is a BsonLog;
    mongo::BufBuilder* const m_buffer;
    mongo::BSONObjBuilder* const m_root;

    // Used for all records
    mongo::BSONObjBuilder* const m_builder;
};

// The Record object here is const so that you can write to a
// temporary BsonLog::Record object, but these things aren't really
// const, so its safe to cast away the constness here. You could break
// this if you somehow managed to construct a truly const Record, so
// don't do that.
template<typename T>
inline const BsonLog::Record& operator<<( const BsonLog::Record& crecord, const detail::BsonKeyValue<T>& kv )
{
    return const_cast<BsonLog::Record&>(crecord).append( kv );
}

class BsonLog::ArrayRecord : public BsonLog::RecordBase
{
    friend class BsonLog::Record;

public:
    // All Array objects are subordinate to (sub)-records or arrays.
    inline ArrayRecord( Record& parent, const char* tag )
        : RecordBase( parent.log() )
        , m_builder( BsonLog::startSubArray( parent.builder(), tag, strlen( tag ) ) )
        , m_id( 0 ) {}

    inline ArrayRecord( Record& parent, const std::string& tag )
        : RecordBase( parent.log() )
        , m_builder( BsonLog::startSubArray( parent.builder(), tag ) )
        , m_id( 0 ) {}

    inline ArrayRecord( ArrayRecord& parent )
        : RecordBase( parent.log() )
        , m_builder( BsonLog::startSubArray( parent.builder(), parent.nextId() ) )
        , m_id( 0 ) {}

    ~ArrayRecord();

private:
    inline mongo::BSONArrayBuilder& builder() const
    {
        return *m_builder;
    }

    inline size_t nextId()
    {
        return m_id++;
    }

    mongo::BSONArrayBuilder* const m_builder;
    size_t m_id;
};

inline BsonLog::Record::Record( BsonLog::ArrayRecord& parent )
    : RecordBase( parent.log() )
    , m_buffer( NULL )
    , m_root()
    , m_builder( BsonLog::startSubObj( parent.builder(), parent.nextId() ) ) {}

namespace detail {


template<typename T>
struct BsonKeyValue
{
    inline BsonKeyValue( const char* key, size_t keyStrLen, const T& val )
        : m_key( key )
        , m_keyStrLen( keyStrLen )
        , m_val( val ) {}

    const char* const m_key;
    const size_t m_keyStrLen;

    // TODO(acm): Can this also be a ref?
    const T m_val;
};

struct literal_value_tag;
template<>
struct BsonKeyValue<literal_value_tag>
{
    inline BsonKeyValue( const char* key, size_t keyStrLen, const char* val, size_t valStrLen )
        : m_key( key )
        , m_keyStrLen( keyStrLen )
        , m_val( val )
        , m_valStrLen( valStrLen ) {}

    const char* const m_key;
    const size_t m_keyStrLen;

    const char* const m_val;
    const size_t m_valStrLen;
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<std::string> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<std::string>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<char*> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<char*>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<const char*> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<const char*>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<literal_value_tag> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<literal_value_tag>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val, kv.m_valStrLen );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<double> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<double>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<int> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<int>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<unsigned int> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<unsigned int>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<size_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<size_t>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<bool> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<bool>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<tif_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<tif_t>& kv )
    {
        const char* s = tif2str( kv.m_val );
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, s );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<side_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<side_t>& kv )
    {
        const char* s = side2str( kv.m_val );
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, s );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<dir_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<dir_t>& kv )
    {
        const char* s = dir2str( kv.m_val );
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, s );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<ostatus_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<ostatus_t>& kv )
    {
        const char* s = ostatus2str( kv.m_val );
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, s );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<mktdest_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<mktdest_t>& kv )
    {
        const char* s = mktdest2str( kv.m_val );
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, s );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<acct_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<acct_t>& kv )
    {
        const char* s = acct2str( kv.m_val );
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, s );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<cxlstatus_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<cxlstatus_t>& kv )
    {
        const char* s = cxlstatus2str( kv.m_val );
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, s );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<symbol_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<symbol_t>& kv )
    {
        const char* s = sym2str( kv.m_val );
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, s );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<liquidity_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<liquidity_t>& kv )
    {
        const char* s = liquidity2str( kv.m_val );
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, s );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<timeval_t> >
{
    static inline void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<timeval_t>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val.to_microsec() );
    }
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<fillflag_t> >
{
    static void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<fillflag_t>& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, static_cast<uint32_t>( kv.m_val ) );
    }
};

// The mongo bson api that we use takes three 'array types': std::list, std::set and std::vector.
// We would prefer boost/std::array, but that's (still) not available. If we pass in something that
// isn't a std::set, std::list or std::vector, but that does have a begin() and end() iterator, we
// convert it into our most suitable data structure: std::vector ( inefficient! ) , and let the
// mongo api do its thing from here on.

// Container<T> specialization for integral types that aren't (u)uint64_t.
template<typename T> struct BsonValuePrinter < detail::BsonKeyValue< T >,
    typename boost::enable_if_c<
        boost::is_integral<typename T::value_type>::value ||
        boost::is_same<typename T::value_type,std::string>::value ||
        boost::is_same<typename T::value_type, const char *>::value >::type,
    typename boost::disable_if_c<
        boost::is_same<typename std::vector<typename T::value_type>, T>::value ||
        boost::is_same<typename std::list<typename T::value_type>, T>::value ||
        boost::is_same<typename std::set<typename T::value_type>, T>::value >::type
    >
{
    // Stupid mongo doesn't accept (u)int64_t as T in std::list<T>.
    BOOST_STATIC_ASSERT((!boost::is_same<typename T::value_type,uint64_t>::value));
    BOOST_STATIC_ASSERT((!boost::is_same<typename T::value_type,int64_t>::value));

    // The container type we passed in isn't a vector, set or list but does have a begin() and end()
    // iterator. Great! Let's convert it to a vector.
    static void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue< T >& kv )
    {
        std::vector<typename T::value_type> vct;
        vct.insert(vct.begin(), kv.m_val.begin(), kv.m_val.end());
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, vct );
    }
};

template<typename T> struct BsonValuePrinter < detail::BsonKeyValue< T >,
    typename boost::enable_if_c<
        boost::is_integral<typename T::value_type>::value ||
        boost::is_same<typename T::value_type,std::string>::value ||
        boost::is_same<typename T::value_type, const char *>::value >::type,
    typename boost::enable_if_c<
        boost::is_same<typename std::vector<typename T::value_type>, T>::value ||
        boost::is_same<typename std::list<typename T::value_type>, T>::value ||
        boost::is_same<typename std::set<typename T::value_type>, T>::value >::type
    >
{
    // Stupid mongo doesn't accept (u)int64_t as T in std::list<T>.
    BOOST_STATIC_ASSERT((!boost::is_same<typename T::value_type,uint64_t>::value));
    BOOST_STATIC_ASSERT((!boost::is_same<typename T::value_type,int64_t>::value));

    // The bson api knows how to print this type of container already, simply pass it along.
    static void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue< T >& kv )
    {
        BsonLog::append( builder, kv.m_key, kv.m_keyStrLen, kv.m_val );
    }
};

// The following specializations are implemented in BsonLog.cc because
// they use the fallback ovstream mechanism, and there is no point in
// inlining that.

template<> struct BsonValuePrinter< detail::BsonKeyValue<instrument_t> >
{
    static void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<instrument_t>& kv );
};

template<> struct BsonValuePrinter< detail::BsonKeyValue<tid_t> >
{
    static void print( BsonLog& log, mongo::BSONObjBuilder& builder, const detail::BsonKeyValue<tid_t>& kv );
};

} // namespace detail

template<typename T, size_t keySize>
inline const detail::BsonKeyValue<T> bson_pair_literal( const char (&key)[keySize], const T& val )
{
    return detail::BsonKeyValue<T>( &key[0], keySize - 1, val );
}

template<size_t keySize, size_t valSize>
inline const detail::BsonKeyValue<detail::literal_value_tag> bson_pair_literal( const char (&key)[keySize], const char (&val)[valSize] )
{
    return detail::BsonKeyValue<detail::literal_value_tag>( &key[0], keySize - 1, &val[0], valSize - 1 );
}

} // namespace bb

#endif // BB_IO_BSONLOG_H
