#ifndef BB_IO_ISENDTRANSPORT_H
#define BB_IO_ISENDTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>
#include <bb/core/smart_ptr.h>

namespace bb {

class Msg;

/// Interface class for send transports.
class ISendTransport : public boost::noncopyable
{
public:
    virtual ~ISendTransport();

    /// Sends the message over the transport.
    /// Throws an exception if an error occurred.
    virtual void send( const Msg* ) = 0;

    // Ideally, tranport should not waste time sending if no one listens.
    virtual std::size_t getNumListeners() const { return 1; }
};
BB_DECLARE_SHARED_PTR( ISendTransport );

} // namespace bb

#endif // BB_IO_ISENDTRANSPORT_H
