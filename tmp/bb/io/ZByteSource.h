#ifndef BB_IO_ZBYTESOURCE_H
#define BB_IO_ZBYTESOURCE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <zlib.h>
#include <limits>
#include <boost/noncopyable.hpp>
#include <boost/scoped_array.hpp>
#include <bb/io/ByteSource.h>
#include <bb/core/Buffer.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(ZByteSource);

// ByteSource decompression adapter
class ZByteSource : public ByteSource, boost::noncopyable
{
public:
    enum FormatType { GZ_FORMAT, ZLIB_FORMAT };

    // Takes a ByteSource and applies gzip decompression to it.  The
    // default behavior is to decompress using gzip format, not zlib
    // format.  To decompress using zlib format, pass
    // ZByteSource::ZLIB_FORMAT in as the second argument.
    ZByteSource(ByteSourcePtr bs, FormatType format = GZ_FORMAT);
    // Wraps a gzip decompression layer around a ByteSourcePtr and returns another ByteSourcePtr.
    static ByteSourcePtr create(ByteSourcePtr bs, FormatType format = GZ_FORMAT)
    {    return boost::static_pointer_cast < ByteSource > ( boost::make_shared < ZByteSource > (bs, format )); }
    virtual ~ZByteSource();

    // return the number of bytes skipped; throws on error.
    // returns 0 on end of stream.
    // if max is specified, stops after max bytes and returns size_t::max.
    size_t sync(const size_t max = std::numeric_limits<size_t>::max());

protected:
    // Reads in at least 1 byte, unless the end of the stream is reached.
    // Throws an exception on read failure.
    virtual size_t ByteSource_read(void *buf, size_t size);
    size_t real_read(void *buf, size_t size);

    ByteSourcePtr m_bs;

private:
    // Pulls in as much data as possible to fill the read buffer and
    // sets up the z_stream to use it.
    void prime();

    FormatType m_format;
    z_stream m_zs;
    static const size_t BUF_SIZE;
    boost::scoped_array<Bytef> m_buf;
    bool m_eof;
    bool m_synced;
    StaticBuffer<50000> m_buffer;

};

} // namespace bb

#endif // BB_IO_ZBYTESOURCE_H
