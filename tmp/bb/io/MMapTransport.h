#ifndef BB_IO_MMAPTRANSPORT_H
#define BB_IO_MMAPTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <tbb/atomic.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

#include <bb/io/RecvTransport.h>
#include <bb/io/SendTransport.h>
#include <bb/io/MMapConfig.h>

#include <bb/core/mmap_control_autogen.h>
#include <bb/core/MMap.h>

#include <map>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(EventFD);

namespace mmap_control
{
    BB_FWD_DECLARE_SHARED_PTR(File);
    /// Shared control data for the MMapTransport. This points into shared memory.
    class File : public file
    {
    public:
        File();

        static FilePtr create(std::string const &filename, mmap_lock_mode_t mode = MMAP_LOCK_SHARED_NB);
        static FilePtr attach(std::string const &filename, mmap_lock_mode_t mode = MMAP_LOCK_SHARED_NB);

        void clear();
        channel *getChannel(size_t i);
        notifier *getNotifier(size_t i);

    protected:
        void checkMagic(std::string filename);
    };
}

class MMapTransportCommon
{
public:
    // MMapTransport has a large buffer with a single writer and multiple readers.
    // If the buffer becomes full, the reader will drop messages.
    //
    // However, to allow fast readers to proceed while a slow reader is overflowing,
    // we use a mapping trick. We pretend that large buffer is a linear, circular
    // buffer but it's actually allocated in chunks which may come in any order
    // (similar to the way that virtual memory works). We call these chunks
    // "pages", and each page has a physical index (i.e. where it is in memory)
    // and a virtual index (where it is mapped into the virtual buffer).
    //
    // mmap_control.xml contains the lock-free datastructures for updating
    // these mappings.
    //
    // The array phys_to_virt describes, for a physical address, which
    // virtual address it's serving as. The special value INVALID_VIRT_PG
    // means it's not currently in use.
    //
    // The array virt_to_phys goes in the opposite direction: given a
    // virtual address, says which physical page is being used for it.
    // Since virt_to_phys is a finite-sized array, a single slot gets
    // reused (i.e. wraparound). For example slot 1 could be the
    // mapping for virtual page 1, 1+N, 2+N, 3+N... Here, N is the
    // size of the virt_to_phys array. A value of INVALID_PHYS_PG means
    // that that virtual offset is unavailable (usually because of overflow).
    //
    // A page cannot be recycled until no reader is pointing at that page.
    // The writer arranges for this using hazard pointers; see the paper:
    // "Hazard Pointers: Safe Memory Reclamation for Lock-Free Objects".
    // for details. The reader publishes the current virtual(!) page
    // it's using in the mmap_control::notifier->hazard variable.

    // A page index in virtual space (units of page size)
    typedef mmap_control::virt_pg_t virt_pg_t;

    // A page index in physical space (units of page size)
    typedef mmap_control::phys_pg_t phys_pg_t;

    // An offset in virtual space (units of bytes). Most things having to do
    // with the paging is done in page-sized units, this is used only when
    // the reader must know an exact location within the page.
    typedef mmap_control::voff_t voff_t;

protected:
    static int openDataFile(const std::string &file, int mode);

    MMapTransportCommon(const mmap_control::channel *channelSHM)
        : m_pageSizeShift(channelSHM->page_size_shift)
        , m_virtToPhysSizeShift(channelSHM->virt_to_phys_size_shift)
        , m_physPageCount(channelSHM->phys_page_count)
    {
    }

    voff_t getPageSize() const { return voff_t(1) << m_pageSizeShift; }
    /// Returns the number of physical pages, (this is also the size of the phys_to_vert array)
    phys_pg_t getPhysPageCount() const { return m_physPageCount; }
    /// Returns the size of the virt_to_phys array. We round up getPhysPageCount()
    /// to a power of 2 so that virtPageToSlot can mask instead of divide.
    virt_pg_t getVirtToPhysCount() const { return virt_pg_t(1) << m_virtToPhysSizeShift; }
    size_t getDataFileSize() const { return m_physPageCount << m_pageSizeShift; }

    /// Returns an index in virt_to_phys. i.e. virt_to_phys[virtPageToSlot(virt_page)]
    /// is the physical mapping of the page whose virtual index is virt_page.
    size_t virtPageToSlot(virt_pg_t virt_page) const
    {
        BB_ASSERT(virt_page != mmap_control::INVALID_VIRT_PG);
        return virt_page & (getVirtToPhysCount()-1);
    }
    /// Returns the virtual page index in which the byte that starts at virt_off lives.
    virt_pg_t virtPageStartingAt(voff_t virt_off) const
        { return virt_off >> m_pageSizeShift; }
    /// Returns the virtual page index in which the byte ending at virt_off lives.
    virt_pg_t virtPageEndingAt(voff_t virt_off) const
        { return (virt_off-1) >> m_pageSizeShift; }
    /// Returns the starting offset of the given page index.
    voff_t virtPageGetStartOffset(virt_pg_t virt_page) const
    {
        BB_ASSERT(virt_page != mmap_control::INVALID_VIRT_PG);
        return voff_t(virt_page) << m_pageSizeShift;
    }

    /// Returns a pointer into our memory space for the given physical page.
    const char *physPageToPtr(const char *dataBasePtr, phys_pg_t phys_page) const
        { BB_ASSERT(dataBasePtr); return dataBasePtr + (phys_page << m_pageSizeShift); }
    char *physPageToPtr(char *dataBasePtr, phys_pg_t phys_page) const
        { BB_ASSERT(dataBasePtr); return dataBasePtr + (phys_page << m_pageSizeShift); }

    /// Returns the memory location of the given virt_off, assuming that it's
    /// associated physical page starts at curPagePtr.
    const void *virtOffsetToPtr(const char *curPagePtr, voff_t virt_off) const
        { BB_ASSERT(curPagePtr); return curPagePtr + (virt_off & (getPageSize() - 1)); }
    void *virtOffsetToPtr(char *curPagePtr, voff_t virt_off) const
        { BB_ASSERT(curPagePtr); return curPagePtr + (virt_off & (getPageSize() - 1)); }

private:
    const uint8_t m_pageSizeShift;
    const uint8_t m_virtToPhysSizeShift;
    const phys_pg_t m_physPageCount;
};

/// Receives messages from an MMap'd mostly-circular buffer
class MMapRecvTransport : public IRecvTransport, public MMapTransportCommon
{
public:
    MMapRecvTransport(bb::LuaState const& luaState,
                      const mmap_control::channel *channelSHM,
                      mmap_control::notifier *notifierSHM,
                      EventFDPtr const &eventFD,
                      mmap_control::FileCPtr const &controlSHM,
                      UnixSocketPtr const &socket,
                      bool verbose);
    virtual ~MMapRecvTransport();

    /// IRecvTransport impl
    virtual void beginDispatch(FDSet &d, IMStreamCallback *messageCB,
            const EndOfStreamCB &endOfStreamCB = EndOfStreamCB(),
            const ErrorCB &errorCB = ErrorCB());
    virtual void endDispatch();

    typedef boost::function<void()> OverflowCallback;
    typedef shared_vector<OverflowCallback> CallbackVector;

    // Add a new callback to be called if an overflow is detected.
    void addOverflowCallback(Subscription &sub, const OverflowCallback& cb);

protected:
    // Called when we need to move onto the next page. Returns the
    // virtual offset (bytes) at the start of the page.
    voff_t trap(voff_t sender_offset);
    void callOverflowCBs();

    class ReadyCallback;
    void onSelectCallback();

    IMStreamCallback *m_messageCB;
    MsgBuffer m_msgBuf;
    const voff_t * const m_virtOffsetSHM; // the global writer offset
    uint32_t * const m_notificationEnabledSHM;
    voff_t m_readerOffset; // the virtual offset we're currently at.
    voff_t m_trapOffset; // the virtual offset at which our current page ends
    const char *m_curPagePtr; // points to the start of the current physical page

    // Pointers into shared memory:
    // our hazard ptr
    virt_pg_t * const m_hazardSHM;
    // the global mapping from virtual pages to physical pages
    const mmap_control::channel::virt_to_phys_t &m_virtToPhysSHM;
    // the global mapping from physical pages to virtual pages
    const mmap_control::channel::phys_to_virt_t &m_physToVirtSHM;
    FD m_dataFileFD;
    const char * const m_dataBaseAddr;
    EventFDPtr m_eventFD;
    size_t m_numMessages, m_numWakeups, m_numBudgetOverflows,
           m_numTraps, m_numSenderOverflows, m_numMissedBytes, m_maxBufferedPages, m_totalBufferedPages;
    const std::string m_dataPath;
    const bool m_verbose;
    UnixSocketPtr m_socket;
    Subscription m_readSub;
    mmap_control::FileCPtr m_controlSHM;
    CallbackVector m_overflowCallbacks;
    std::map<int, int> m_longWait;
    std::size_t m_max_messages;
    mmap_config::MMapSharedConfig m_config; // min,max,lag Thresholds
};
BB_DECLARE_SHARED_PTR( MMapRecvTransport );

/// Sends messages in datagram format using shared memory
class MMapSendTransport : public ISendTransport, public MMapTransportCommon
{
public:
    struct Listener
    {
        Listener() : m_notificationEnabledSHM(NULL), m_hazardSHM(NULL) {}
        uint32_t *m_notificationEnabledSHM;
        const virt_pg_t *m_hazardSHM;
        EventFDPtr m_eventFD;
    };
    typedef std::vector<Listener> ListenerArray;

    MMapSendTransport(mmap_control::channel *channelSHM,
            mmap_control::FileCPtr const &controlSHM,
            ListenerArray *listeners);
    virtual ~MMapSendTransport();

    /// Throws an exception if an error occurred.
    void send(const Msg *m);
    void updateListeners();

    std::size_t getNumListeners() const { return m_listeners->size(); }
private:
    virt_pg_t getCurPageVirt() const
        { BB_ASSERT(m_trapOffset >= getPageSize()); return virtPageEndingAt(m_trapOffset); }
    typedef std::vector<phys_pg_t> freelist_t;
    typedef std::vector<phys_pg_t> retiredlist_t;
    typedef std::vector<virt_pg_t> hazardlist_t;

    // Returns the smallest hazard offset
    // (or endPg if no hazard is earlier than endPg).
    virt_pg_t loadMinHazard(virt_pg_t endPg) const;
    void loadHazards(hazardlist_t &hazards) const;
    /// Retires all pages whose indices are < end, placing them on m_retiredPages.
    void retirePagesUntil(virt_pg_t end);
    void freePages();
    // Called when we need to move onto the next page. Returns the
    // virtual offset (bytes) at the start of the page.
    voff_t trap();

    voff_t * const m_virtOffsetSHM; // the shared writer offset
    voff_t m_writerOffset; // local copy of the writer offset
    voff_t m_trapOffset;
    virt_pg_t m_retiredUntil; // the highest-number page which we have retired
    ListenerArray *m_listeners;
    char *m_curPagePtr; // pointer to the start of the start of the current page

    // Shared memory: virt_to_phys and phys_to_virt.
    mmap_control::channel::virt_to_phys_t &m_virtToPhysSHM;
    mmap_control::channel::phys_to_virt_t &m_physToVirtSHM;

    // Storage for hazard pointers (used as a temporary in freePages).
    // This stores virtual page addresses.
    hazardlist_t m_hazards;
    // A list of physical pages which have been retired. The term
    // "retired" is rigorously defined in the Hazard Pointers paper.
    // A retired page is one that readers cannot take up (since
    // its virt_to_phys entry has been set to invalid), but that we
    // haven't yet proven is unused (since old readers may still be using
    // these pages).
    retiredlist_t m_retiredPages;
    // A list of physical pages which are free (i.e. proven to be unused
    // by any reader). We reuse these pages in LIFO order to minimize
    // cache footprint.
    freelist_t m_freePages;
    FD m_dataFileFD;
    char * const m_dataBaseAddr;
    mmap_control::FileCPtr m_controlSHM;
};
BB_DECLARE_SHARED_PTR(MMapSendTransport);

} // namespace bb

#endif // BB_IO_MMAPTRANSPORT_H
