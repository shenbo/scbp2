#ifndef BB_IO_FILTERTRANSPORTSUBSCRIPTIONMGR_H
#define BB_IO_FILTERTRANSPORTSUBSCRIPTIONMGR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/io/FilterTransport.h>
#include <bb/io/MsglogdSendTransport.h>
#include <bb/core/PortLookup.h>
#include <bb/io/QuoteSubscriptionMgr.h>

namespace bb {

class SelectDispatcherPolicy
{
public:
    static const bool multithreaded = false;
private:
    SelectDispatcherPolicy();
};

/*
 * Spawns a server to handle events for adding/remove subscriptions for FilterTransport
 *
 * Uses one of two policies, SelectDispatcherPolicy or EventPostPolicy (deprecated).
 * The policies determine whether the FilterTransport is told that the add/remove
 * subscription requests need to support concurrency or not.  If there will be multiple
 * FilterTransportSubscriptionMgr instances linked to a single FilterTransport,
 * EventPostPolicy should be used; otherwise SelectDispatcherPolicy is probably better.
 *
 * FilterTransportSubscriptionMgr<SelectDispatcherPolicy> will dispatch add and remove in
 * the registered SelectDispatcher loop.
 *
 * EventPostPolicy will inject an event into a event queue in the FilterTransport to be
 * applied when the next message is sent out.
 */

template<typename Policy>
class FilterTransportSubscriptionMgr : public QuoteSubscriptionMgr
{
public:
    // if log_qualifier is not NULL, it will be included it in the subscription log filenames
    FilterTransportSubscriptionMgr( const BaseFilterTransportPtr& filter, const source_t& source,
            const char* log_qualifier = NULL )
        : QuoteSubscriptionMgr( bb::PortLookup::getQDPorts( source ),
                                bb::MsglogdSendTransport::getSourceLogPrefix( source, log_qualifier ),
                                boost::bind( &FilterTransportSubscriptionMgr::postAddEvent, this, _1, _2),
                                boost::bind( &FilterTransportSubscriptionMgr::postRemoveEvent, this, _1, _2) )
        , m_filter( filter )
    {
        BB_ASSERT( filter->hasSubscriptionSupport() );
    }

    virtual ~FilterTransportSubscriptionMgr() { stop(); }

protected:
    virtual void postAddEvent( bb::mtype_t mtype, const bb::instrument_t instr )
    {
        if( instr.sym == bb::SYM_ALL )
        {
            m_filter->add( mtype );
        }
        else
        {
            m_filter->add( mtype, instr.sym );
        }
    }

    virtual void postRemoveEvent( bb::mtype_t mtype, const bb::instrument_t instr )
    {
        if( instr.sym == bb::SYM_ALL )
        {
            m_filter->remove( mtype );
        }
        else
        {
            m_filter->remove( mtype, instr.sym );
        }
    }

    BaseFilterTransportPtr m_filter;
};


// like FilterTransportSubscriptionMgr, but adds callback hooks to know when subscriptions start and end
// this is useful for forwarding the subscription requests on to another program, as tunnelsend does
class HookedFilterTransportSubscriptionMgr : public FilterTransportSubscriptionMgr<SelectDispatcherPolicy>
{
    typedef SelectDispatcherPolicy Policy; // don't bother with others unless we need them
    typedef boost::function<void ( mtype_t mtype, symbol_t sym )> SubscribeCB;

public:
    HookedFilterTransportSubscriptionMgr( const BaseFilterTransportPtr& filter, const source_t& source,
            const char* log_qualifier, SubscribeCB subscribe_hook, SubscribeCB unsubscribe_hook );

protected:
    virtual void postAddEvent( bb::mtype_t mtype, bb::instrument_t instr );
    virtual void postRemoveEvent( bb::mtype_t mtype, bb::instrument_t instr );

    SubscribeCB m_subscribeHook, m_unsubscribeHook;
};

} // namespace bb

#endif // BB_IO_FILTERTRANSPORTSUBSCRIPTIONMGR_H
