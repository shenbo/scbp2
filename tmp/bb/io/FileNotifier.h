#ifndef BB_IO_FILENOTIFIER_H
#define BB_IO_FILENOTIFIER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>

#include <boost/function.hpp>
#include <boost/scoped_array.hpp>

#include <bb/core/bbint.h>
#include <bb/core/FD.h>
#include <bb/core/Subscription.h>

struct inotify_event;

namespace bb {
class FDSet;

/// Notification mechanism for filesystem events.
class FileNotifier : public FD
{
public:
    typedef boost::function<void ( const inotify_event& )> Callback;

    FileNotifier( FDSet& fdSet ); // caller must pump the FDSet

    /// Sets Subscription to a token which will stop the callbacks when destroyed
    /// The Subscription must be destroyed before this FileNotifier is
    /// mask is an inotify mask e.g. IN_MODIFY | IN_CLOSE_WRITE
    void add( Subscription& sub, const char* path, uint32_t mask, Callback callback );

    /// As above but returns a watch descriptor which can be used with remove()
    /// mask is an inotify mask e.g. IN_MODIFY | IN_CLOSE_WRITE
    int add( const char* path, uint32_t mask, Callback callback );

    /// Removes a watch added without a Subscription token
    void remove( int watch_descriptor );

    /// Call the given function if the underlying event queue overflows
    /// If no callback is set, LOG_PANIC is used
    /// If the callback is called, the watch descriptor will always be -1:
    /// after an overflow there is no way to know which files had events
    void setOverflowCB( const Callback& );

private:
    /// called to check if there are new events and process the callbacks
    /// blocks if the underlying file descriptor is set to block (default)
    void read();

    boost::scoped_array<char> m_buffer;
    Subscription m_subscription;

    typedef std::map<int, std::pair<uint32_t, Callback> > CallbackMap;
    CallbackMap m_callbackMap;

    Callback m_overflowCB;
};
BB_DECLARE_SHARED_PTR( FileNotifier );

} // namespace bb

#endif // BB_IO_FILENOTIFIER_H
