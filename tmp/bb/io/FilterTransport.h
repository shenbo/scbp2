#ifndef BB_IO_FILTERTRANSPORT_H
#define BB_IO_FILTERTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/LuaConfig.h>

#include <bb/io/MMapTransportServer.h>
#include <bb/io/SendTransport.h>

#include <boost/smart_ptr.hpp>

#include <bb/core/FDSetFwd.h>
#include <bb/core/source.h>
#include <bb/core/symbol.h>
#include <bb/core/mtype.h>
#include <bb/core/MsgBuffer.h>
#include <bb/core/SubscriptionTable.h>

#include <bb/threading/Lock.h>

namespace luabind { namespace adl { class object; } using adl::object; }

namespace bb {


class sockaddr_ipv4_t;
BB_FWD_DECLARE_SHARED_PTR( AresResolver );
BB_FWD_DECLARE_SHARED_PTR( LuaState );
BB_FWD_DECLARE_SHARED_PTR( MMapServerFeedType );
BB_FWD_DECLARE_SHARED_PTR( ByteSinkFactory );
BB_FWD_DECLARE_SHARED_PTR( SubscriptionTable );

class BaseFilterTransport
{
public:
    BB_FWD_DECLARE_SHARED_PTR( FilterGroup );
    typedef std::vector<FilterGroupPtr> FilterGroupList;

    BaseFilterTransport();

    virtual ~BaseFilterTransport() {}

    /// Returns true if some part of the transport supports subscription ("admin") messages.
    virtual bool hasSubscriptionSupport() const;

    /// subscribe mtype,sym
    virtual void add( mtype_t mtype, symbol_t sym );

    /// unsubscribe mtype,sym
    virtual void remove( mtype_t mtype, symbol_t sym );

    /// subscribe for all syms
    virtual void add( mtype_t mtype );

    /// unsubscribe for all syms
    virtual void remove( mtype_t mtype );

    virtual void addGroup(FilterGroupPtr const& group) {
        m_filterGroupList.push_back(group);
    }

    void setSubscriptionCallback(SubscriptionTable::add_func const&,
                                 SubscriptionTable::remove_func const&);

protected:
    FilterGroupList m_filterGroupList;

    MsgBuffer m_buffer;

    SubscriptionTablePtr  m_subscriptionTablePtr;
};
BB_DECLARE_SHARED_PTR( BaseFilterTransport );

/// This filters all the messages against a symbol list and mtype list
/// and sends out all matching messages over multicast. In addition, it forwards
/// _all_ messages to the msglogd send transport
///
/// It loads configuration info from "filter_config" in the given lua LuaState object.
/// Here is a sample config
/// filter_config = {
///     verbose = true,
///     msglogd = {
///         unix = "/com/athenacr/msglogd/cts",
///         sink_config = {
///             type           = 'producer',    -- use producer ByteSink
///             buffer_num     = 256,           -- use 256 buffer
///             buffer_size    = 256*1024,      -- 256k bytes per buffers
///             flush_interval = 60,            -- force a buffer flush every 60 seconds
///         },
///         threadpool_config = {
///             thread_num = 1,                  -- number of threads in threadpool
///             allowed_cpu_list = { 0, 1 },     -- allowed CPUs for threads in threadpool
///             processing_mode = 'blocking',    -- by default we won't spin the CPU
///         },
///     },
///     filtered_symbols = {},
///     filtered_mtypes = {},
/// }
class FilterTransport : public ISendTransport, public BaseFilterTransport
{
public:
    FilterTransport( FDSet& fdSet, source_t qdSource, const std::string& file,
                     date_t date = date_t::today(), std::string suffix="" ); // loads config from the given file
    // the provided LuaState must outlive this FilterTransport (or else the "output" transport may be invalidated)
    FilterTransport( FDSet& fdSet, source_t qdSource, LuaState& config, date_t date = date_t::today(), std::string suffix="" );

    virtual ~FilterTransport();

    /// Propagates exceptions from its underlying transports.
    /// Note that msglogd sends occur after filter sends.
    virtual void send( const Msg *msg );

    // check if mmap server is initialized
    bool isMMapMode() const;

    // Because you can only have one instance of `MMapServerFeedType`
    MMapServerFeedTypePtr& getMmapServer() {
        return m_mmapserver;
    }

protected:
    void init( source_t qdSource, LuaState &config );
    void filterGroupInit( const source_t &groupSource, const luabind::object &groupConfig );

    bool m_verbose;

    LuaStatePtr m_configLuaState; // used when we are constructed with a filename for our config

    FDSet& m_fdSet;

    ISendTransportPtr m_msglogdTransport;

    MMapServerFeedTypePtr m_mmapserver;
    date_t                m_tradeDate;
    std::string           m_logSuffix;
};
BB_DECLARE_SHARED_PTR( FilterTransport );

class FilterTransportFactory {

public:

static FilterTransportPtr create(FDSet& fdSet, source_t g_source, LuaState& luaState,
                                 date_t date = date_t::today(),
                                 std::string const& suffix = "");
};

class MMapServerInfoConfig : public LuaConfig<MMapServerInfoConfig> {
public:
    MMapServerInfoConfig();

    static void describe();

    std::string m_feedType;
    std::string m_channel;
};


class FilterGroupConfig : public LuaConfig<FilterGroupConfig> {
public:
    FilterGroupConfig();

    static void describe();
    typedef std::map<mtype_t, std::vector<instrument_t> > FilterCollection;

    MMapServerInfoConfig m_mmap_server_info;
    bool m_noQdSubscriptions;
    bool m_overwrite_source;
    bool m_short_circuit_lookups;
    FilterCollection m_filtered; // this allows you to specify MTYPE = {SYMBOL1, SYMBOL2}
    bool m_verbose;
};

BaseFilterTransport::FilterGroupPtr
createFilterGroup(source_t const& groupSource,
                  FilterGroupConfig const& groupConfig,
                  MMapServerFeedTypePtr& mmapServer,
				  FDSet& fdSet,
                  ISendTransportPtr const& customGroupTransportPtr = ISendTransportPtr());

/// A stripped-down version of FilterTransport which is not Lua-configurable.
/// This has no msglogd output, only supports one output group, and always supports subscription.
/// Unlike FilterTransport, this does not overwrite the source in sent message headers.
class SimpleFilterTransport : public ISendTransport, public BaseFilterTransport
{
public:
    /// If seqFilename is not NULL, it will be used to overwrite sequence numbers in sent message headers.
    SimpleFilterTransport( const ISendTransportPtr& sendTransport, const char* seqFilename = NULL );

    /// Propagates exceptions from its underlying transports.
    virtual void send( const Msg *msg );
};
BB_DECLARE_SHARED_PTR( SimpleFilterTransport );

/// This transport supports the concurrent adding/removing subscriptions and sending messages in
/// different threads
class ConcurrentFilterTransport : public FilterTransport
{
public:

    ConcurrentFilterTransport( FDSet& fdSet, source_t qdSource, const std::string& file, date_t date, std::string suffix )
            : FilterTransport( fdSet, qdSource, file, date, suffix )
            , m_spLock( new threading::Spinlock() )
    {}

    ConcurrentFilterTransport( FDSet& fdSet, source_t qdSource, LuaState& config, date_t date, std::string suffix )
            : FilterTransport( fdSet, qdSource, config, date, suffix )
            , m_spLock( new threading::Spinlock() )
    {}

    ConcurrentFilterTransport( FDSet& fdSet, source_t qdSource, const std::string& file )
        : FilterTransport( fdSet, qdSource, file )
        , m_spLock( new threading::Spinlock() )
    {}

    ConcurrentFilterTransport( FDSet& fdSet, source_t qdSource, LuaState& config )
        : FilterTransport( fdSet, qdSource, config )
        , m_spLock( new threading::Spinlock() )
    {}

    // thread-safe interface for ISendTransport
    virtual void send( const Msg *msg );

    // thread-safe interfaces for BaseFilterTransport
    virtual bool hasSubscriptionSupport() const;

    virtual void add( mtype_t mtype, symbol_t sym );

    virtual void remove( mtype_t mtype, symbol_t sym );

    virtual void add( mtype_t mtype );

    virtual void remove( mtype_t mtype );

protected:
    // mutex to protect sending and add/remove in different threads
    mutable threading::LockPtr  m_spLock;
};
BB_DECLARE_SHARED_PTR( ConcurrentFilterTransport );


class CallbackSendTransport : public ISendTransport {
public:
    typedef boost::function<void( const Msg* )> MessageSentCallback;

    CallbackSendTransport( const ISendTransportPtr& send, const MessageSentCallback& cb );

    virtual void send(const Msg *);

protected:
    ISendTransportPtr m_send;
    MessageSentCallback m_callback;
};
BB_DECLARE_SHARED_PTR( CallbackSendTransport );


BaseFilterTransport::FilterGroupList getFilterGroups(
    source_t const& groupSource,
    LuaState const& config,
    MMapServerFeedTypePtr const& mmapServer);

} // namespace bb

#endif // BB_IO_FILTERTRANSPORT_H
