#ifndef BB_IO_TCPTRANSPORTS_H
#define BB_IO_TCPTRANSPORTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/io/SendTransport.h>
#include <bb/io/RecvTransport.h>

#include <bb/core/MStreamCallback.h>
#include <bb/core/Timer.h>
#include <bb/core/network.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Timer );

class TransMplexStream;

class MultiConnectionTCPRecvTransport : public StreamRecvTransport
{
public:
    typedef boost::function<void (bb::TCPSocketRecvTransportPtr)> ClientEofCallback;
    MultiConnectionTCPRecvTransport( const TCPSocketPtr& socket,
                                     const ClientEofCallback &clientEofCB = ClientEofCallback(),
                                     const ErrorCB &clientErrorCB = ErrorCB(),
                                     size_t kernelRecvBufSize = DEFAULT_RECV_BUF_SIZE,
                                     size_t processRecvBufSize = DEFAULT_MEM_BUFFER_SIZE);

    virtual void onDispatchBegin(
        TransMplexStream *mplexer, IMStreamCallback* messageCB,
        const EndOfStreamCB& endOfStreamCB = EndOfStreamCB(),
        const ErrorCB& errorCB = ErrorCB() );

    virtual void onConnection( TransMplexStream* );

protected:
    TCPSocketPtr m_socket;

    // These two callbacks are invoked for client transports.
    // The EOF callback and error callback of this server socket itself
    // (i.e., m_socket) are provided by onDispatchBegin().
    ClientEofCallback m_clientEofCallback;
    ErrorCB m_clientErrorCallback;
};
BB_DECLARE_SHARED_PTR( MultiConnectionTCPRecvTransport );


// ---------------------------------------------------

#ifndef SWIG
class ReconnectingTCPSocketSendTransport : public TCPSocketSendTransport, public IMStreamCallback
{
public:
    // Enable ping for testing/keep alive pinging every five seconds.
    // By default it's off and we'd only notice the other side has gone when we try to send a 'real' message.
    ReconnectingTCPSocketSendTransport( FDSet&, sockaddr_ipv4_t, bool ping = false );
    virtual ~ReconnectingTCPSocketSendTransport();

    virtual void send(const Msg *);
private:
    void reconnect();
    void onTimer();
    void onPing();
    void onConnectionCallback ( const TCPSocketPtr & sock, int error );
    virtual void onMessage( const Msg& );

    uint64_t m_numMsgsNotSent;
    Subscription m_connectSubscription;
    FDSet& m_fdSet;
    sockaddr_ipv4_t m_addr;
    boost::posix_time::time_duration m_reconnectInterval;
    bb::Timer m_reconnectTimer, m_pingTimer;
    bool m_connected;
};

BB_DECLARE_SHARED_PTR( ReconnectingTCPSocketSendTransport );
#endif // SWIG



}

#endif // BB_IO_TCPTRANSPORTS_H
