#ifndef BB_IO_MESSAGEPERFMON_H
#define BB_IO_MESSAGEPERFMON_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/*
 * MessagePerfMon.h
 *
 *  Created on: Jun 6, 2008
 *      Author: ewies
 */

#include <bitset>

#include <boost/variant/variant_fwd.hpp>
#include <boost/thread.hpp>

#include <bb/core/bbhash.h>
#include <bb/core/timeval.h>
#include <bb/core/hash.h>
#include <bb/core/hash_map.h>
#include <bb/core/ptime.h>
#include <bb/core/source.h>
#include <bb/core/symbol.h>
#include <bb/core/Msg.h>
#include <bb/core/Stats.h>
#include <bb/core/smart_ptr.h>

namespace bb {

class Msg;
class XmlLog;

namespace message_perf {

/// The structure which is passed between threads for each message in the system.
struct MessageRec
{
    bb::source_t m_source;
    bb::symbol_t m_symbol;
    bb::mtype_t m_mtype;
    bb::timeval_t m_timeSent; // BB timestamp on the message 
    boost::optional<bb::timeval_t> m_exchangeTimeSent; // exchange's time on the message
    boost::optional<timeval_t> m_machineTimestamp; // CLOCK_REALTIME time when we received the message
    boost::optional<ptime_duration_t> m_processingTime; // CLOCK_MONOTONIC time spend handling the message
    bool m_cmFired;
};

/// The structure which is passed between threads on exit.
struct ExitRec
{
    ExitRec()
        : m_eventDistTotalMsgCount (0)
        , m_maxQueueSizeSeen(0)
        , m_hostDest ( bb::DEST_UNKNOWN )
    {
        m_eventDistTotalMsgCount.reset();
    }
    boost::optional<int64_t> m_eventDistTotalMsgCount;
    size_t m_maxQueueSizeSeen;
    EFeedDest m_hostDest;
};

/// This class monitors the performance of message handling in the system.
/// It'll create a new thread, analyze data within that thread, and write
/// a report on exit.
/// For clientcore applications, use message_perf::EventDistMonitor instead.
class Monitor : public boost::noncopyable
{
public:
    Monitor( std::string outFile );
    ~Monitor();

    void addEvent( const Msg& msg,
            const boost::optional<timeval_t> &timestamp,
            const boost::optional<ptime_duration_t> &processingTime,
            bool cmFired );
    void shutdown( const boost::optional<int64_t> &totalMessages );

protected:
    typedef std::vector<MessageRec> MessageChunk;
    typedef boost::variant<ExitRec, MessageChunk*> QueueObj;
    typedef std::list<QueueObj> Queue;

    struct AnalysisThread;

    bool m_shutdown;

    // how many elements to queue up in our thread before sending over to the analysis thread.
    // we do this so the locking overhead doesn't get too onerous
    static const size_t kMessageChunkSize = 1000;
    // how many chunks we allow have in the queue before we wait for the other thread to catch up.
    static const size_t kMaxQueueSize = 100;
    size_t m_maxQueueSizeSeen;
    AnalysisThread *m_analysisThread;
    boost::thread m_analysisThreadObj;
    std::auto_ptr<MessageChunk> m_currentChunk;
};
BB_DECLARE_SHARED_PTR(Monitor);

/// Data analysis class for message_perf::Monitor; do not use directly.
/// This tracks:
///   How long we're taking to handle each message, broken down by mtype.
///   The time lag between the timestamps in the system.
class Analyzer : public boost::noncopyable
{
public:
    Analyzer();
    ~Analyzer();

    static std::bitset<SRC_UNKNOWN+1> getIgnoredFeeds();

    void handleMsg(const MessageRec &msg);
    void writeLog(const ExitRec &exit, XmlLog &log);

protected:
    std::bitset<SRC_UNKNOWN+1> m_lagFeedsToIgnore;

    typedef bbext::hash_map<mtype_t,  QuantileCalculator*> QuantilesByMtype;
    QuantilesByMtype m_processing_time_by_mtype;
    QuantileCalculator m_processing_time_clockmonitor_wakeups;

    typedef bbext::hash_map<source_t,  QuantileCalculator*> QuantilesBySource;
    QuantilesBySource m_exchange_lag_by_source, m_host_lag_by_source;

    static const size_t  kMessageCountLimit = 50000;
    int64_t              m_messages_handled;
    QuantileCalculator   m_handled_msg_rate_quantile;
    size_t               m_handled_msg_rate_counter;
    bb::timeval_t       m_handled_msg_counter_last_check_time;
};


} // namespace message_perf
} // namespace bb

#endif // BB_IO_MESSAGEPERFMON_H
