#ifndef BB_IO_SOCKET_H
#define BB_IO_SOCKET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <bb/core/FD.h>
#include <bb/core/network.h>
#include <bb/core/ptime.h>
#include <bb/core/timeval.h>

// This should be part of IO, but isn't due to library circularity
// issues between the bbcore and bbio libraries.
#include <bb/core/TCPKeepAliveOptions.h>

#include <bb/io/ByteSink.h>
#include <bb/io/ByteSource.h>
#include <bb/io/recvmmsg_glibc.h>

struct tcp_info; // fwd declaration; #include <netinet/tcp.h>

namespace bb {

class Socket : public ByteSource, public ByteSink, public NBFD
{
public:
    Socket(fd_t _fd) : NBFD(_fd) {}
    virtual ~Socket() {}

    virtual void flush() {}

    ssize_t send_nothrow(const void* buf, size_t size, int addlFlags = 0);
    size_t send(const void *buf, size_t size, int addlFlags = 0); // return of 0 means EWOULDBLOCK
    ssize_t sendmsg_nothrow(struct ::msghdr * hdr, int flags);

    ssize_t recv_nothrow(void *buf, size_t size, int addlFlags = 0);
    size_t recv(void *buf, size_t size, int addlFlags = 0); // return of 0 means EOF (throws on EWOULDBLOCK)

    size_t recv_msg(struct ::msghdr * hdr, int flags);
    ssize_t recvmsg_nothrow(struct ::msghdr * hdr, int flags);

    virtual void shutdown(int how);

    // timeout of 0 means never
    void setSendTimeout(const ptime_duration_t& timeout);
    ptime_duration_t getSendTimeout() const;
    void setRecvTimeout(const ptime_duration_t& timeout);
    ptime_duration_t getRecvTimeout() const;

    /// Sets the socket send buffer.
    virtual void setSendBufSize(int size);
    /// Returns the socket send buffer size.
    virtual int getSendBufSize() const;
    /// Sets the socket receive buffer.
    virtual void setRecvBufSize(int size);
    /// Returns the socket receive buffer size.
    virtual int getRecvBufSize() const;

    int getSockType() const; // getsockopt(SO_TYPE)

protected:
#if defined( WIN32 ) || defined( WIN64 )
    static char *voidptr_to_sockbuf(void* v) { return reinterpret_cast<char*>(v); }
#else
    static void *voidptr_to_sockbuf(void* v) { return v; }
#endif

    virtual size_t ByteSource_read(void *buf, size_t size);
    virtual size_t ByteSink_write(const void *buf, size_t size);

    /// Handle an error sending data to a socket.
    /// This function normally throws a CError.
    /// In some cases, we might not want to throw.
    virtual void handle_send_error(int err);
};
BB_DECLARE_SHARED_PTR(Socket);


/// Berkeley socket with family=AF_INET.
/// Notes on blocking -- though the socket can be set non-blocking,
/// any function which takes a host can potentially block on DNS resolution.
class NetSocket : public Socket
{
public:
    typedef sockaddr_ipv4_t AddrType;

    /// connect & bind return true on success
    bool connect(const sockaddr_ipv4_t &addr);

    virtual bool bind(const sockaddr_ipv4_t &addr);

    size_t recvfrom(void *buf, size_t size, sockaddr_ipv4_t *srcAddr, int addlFlags = 0);

    /// Returns the IP address & port of the local socket (see getsockname(2)).
    sockaddr_ipv4_t getSockName() const;

    /// Returns the IP address & port of the remote socket.
    sockaddr_ipv4_t getRemoteSockName() const;

    // Returns our **BEST GUESS** of what interface this connection goes out on.
    std::string getInterface() const;

    // Returns the MAC address of the interface used for this local socket.
    mac_addr_t getLocalMacAddr() const;

    // Returns the MAC address of the interface on the other side.
    mac_addr_t getRemoteMacAddr() const;

    /// Returns the interface address for the given interface (see ioctl(SIOCGIFADDR));
    ipv4_addr_t getInterfaceAddr(const char *iface) const;

    /// Enable HW timestamping of the packets
    bool          enableHwTimestamping();

protected:
    NetSocket(int fd) : Socket(fd) { }
};
BB_DECLARE_SHARED_PTR(NetSocket);

class UDPSocket : public NetSocket
{
public:
    UDPSocket();
    virtual ~UDPSocket();

#if !(defined(WIN32) || defined(WIN64))
    // Send multiple buffers at once, reducing copying.
    // Does not support flags, and does not include MSG_NOSIGNAL / SO_NOSIGPIPE,
    // so can raise SIGPIPE when used with TCP, though it seems not with UDP.
    size_t writeV(const ::iovec *bufs, size_t count);
#endif

    ssize_t recvMultiple_nothrow(struct ::mmsghdr *messages, size_t messages_capacity, int addlFlags = 0);
    size_t recvMultiple(struct ::mmsghdr *messages, size_t messages_capacity, int addlFlags = 0);

    /// Joins the specified multicast group on the specific interface addr.
    bool mcastJoin(const ipv4_addr_t &interfaceAddr, const ipv4_addr_t &mcastaddr);
    /// interfaceName can be e.g. "eth0"
    bool mcastJoin(const char *interfaceName, const ipv4_addr_t &mcastaddr);
    bool mcastJoin(const std::string &interfaceName, const ipv4_addr_t &mcastaddr)
        { return mcastJoin(interfaceName.c_str(), mcastaddr); }

    /// Joins the specified multicast group with a kernel chosen interface.
    bool mcastJoin(const ipv4_addr_t &mcastaddr) { return mcastJoin(ipv4_addr_t::inaddr_any(), mcastaddr); }

    /// Sets the interface for outgoing multicasts. Throws on error.
    void mcastIFSet(const ipv4_addr_t &interfaceAddr);
    void mcastIFSet(const char *interface_);
    void mcastIFSet(const std::string &interface_)
        { return mcastIFSet(interface_.c_str()); }

    /// Sets the Time To Live (TTL) in the IP header for outgoing multicast
    /// datagrams. By default it is set to 1. TTL of 0 are not transmitted on
    /// any sub-network. Multicast datagrams with a TTL of greater than 1 may be
    /// delivered to more than one sub-network, if there are one or more
    /// multicast routers attached to the first sub-network.
    bool setTimeToLive(uint16_t ttl);

    virtual void shutdown(int how);

    /// enable ip_recverr which will cause sends to fail with ENOBUFS when the
    /// buffer becomes full
    /// in linux 2.6.24 this causes /proc/net/snmp OutDiscards to get updated
    /// see "man 7 ip" for more information
    bool enableRecvErr(bool enable = true);

protected:
    virtual void handle_send_error(int err);

    void logErrors();

    unsigned m_countNoBufs; // possibly useful to know if you are actively dropping packets
    unsigned m_countConnectionRefused; // triggered when no receiver is listening to sent messages
    unsigned m_countHostUnreachable; // not sure why this happens (see related comments in .cc file)

    bool m_logPending;
    timeval_t m_lastLogTime;
    static const ptime_duration_t s_logInterval;
};
BB_DECLARE_SHARED_PTR(UDPSocket);

class TCPSocket : public NetSocket
{
public:
    enum NoDelayFlag_t { USE_NAGLE_ALGORITHM, NODELAY };

    TCPSocket(NoDelayFlag_t fg = USE_NAGLE_ALGORITHM);
    TCPSocket(fd_t fd, NoDelayFlag_t fg = USE_NAGLE_ALGORITHM);

    // Returns a TCPSocket* (or NULL) allocated via new, to be deleted by the caller
    TCPSocket *accept(sockaddr_ipv4_t *peerAddr=NULL);

    /// Enables the TCP_NODELAY option (disables Nagle algorithm)
    void setNoDelay(NoDelayFlag_t flag = NODELAY);
    /// Sets the TCP_QUICKACK option (enables a one-time immediate ACK, circumventing the
    /// delayed ack algorithm).
    void sendQuickAck();

    /// Enables or disables TCP keep-alive. Default keepalive Behavior
    /// (on Linux) depends on configuration of
    /// /proc/sys/net/ipv4/tcp_keepalive_{time,intvl,probes}. To tune
    /// these values for the current socket, you can provide a pointer
    /// to a TCPKeepAliveOptions as the second argument if 'enable' is
    /// true. Note that these parameters are Linux specific. On other
    /// platforms, the keep alive parameters may be ignored or only
    /// partially applied.
    void setKeepAlive(bool enable = true, const TCPKeepAliveOptions* options = NULL );

    /// Enables TCP Thin mode (useful for latency-sensitive applications, not bulk transfers)
    /// See http://www.mjmwired.net/kernel/Documentation/networking/tcp-thin.txt
    void setThin(bool thin = true);

    /// getsockopt(TCP_INFO); see tcp(7) manpage
    void getTCPInfo(struct tcp_info *info);

    void listen(int backlog = 5);

    virtual bool bind(const sockaddr_ipv4_t &addr);
    bool bind(int port) { return bind(sockaddr_ipv4_t::inaddr_any(port)); }
};
BB_DECLARE_SHARED_PTR(TCPSocket);

// A connected, sequenced packet socket.
class SCTPSocket : public NetSocket
{
public:

    ssize_t recvMultiple_nothrow(struct ::mmsghdr *messages, size_t messages_capacity, int addlFlags = 0);
    size_t recvMultiple(struct ::mmsghdr *messages, size_t messages_capacity, int addlFlags = 0);

    enum NoDelayFlag_t { USE_DEFAULT, NODELAY };

    SCTPSocket(NoDelayFlag_t fg = USE_DEFAULT);
    SCTPSocket(fd_t fd, NoDelayFlag_t fg = USE_DEFAULT);

    SCTPSocket *accept(sockaddr_ipv4_t *peerAddr = NULL);

    void setNoDelay(NoDelayFlag_t flag = NODELAY);

    void listen(int backlog = 5);

    virtual bool bind(const sockaddr_ipv4_t &addr);
    bool bind(int port) { return bind(sockaddr_ipv4_t::inaddr_any(port)); }
};
BB_DECLARE_SHARED_PTR(SCTPSocket);

BB_FWD_DECLARE_SHARED_PTR(UnixSocket);
class UnixSocket : public Socket
{
public:
    typedef const char *AddrType;

    // Creates a SOCK_STREAM Unix socket.
    UnixSocket();
    UnixSocket(fd_t fd);
    virtual ~UnixSocket();

    // Returns a UnixSocket* (or NULL) allocated via new, to be deleted by the caller
    UnixSocket *accept();

    // Returns a pair of connected UnixSocket's, where 'type' is as
    // the second argument to socketpair (like SOCK_STREAM or SOCK_DGRAM).
    static std::pair<UnixSocketPtr, UnixSocketPtr> createSocketPair(int type);

    bool connect_abstract(const char *path);
    bool connect_abstract(const std::string &path) { return connect_abstract(path.c_str()); }
    bool bind_abstract(const char *path);
    bool bind_abstract(std::string const &path) { return bind_abstract(path.c_str()); }

    /// Send and receive file descriptors via a UnixSocket (SCM_RIGHTS)
    /// Some amount of normal data must be sent at the same time (at least on Linux)
    size_t send_fd(fd_t  fd, const void *data, size_t size, int addlFlags = 0); // return of 0 means EWOULDBLOCK
    // fd will be set to -1 if no FD is present in the control data.
    size_t recv_fd(fd_t* fd,       void *data, size_t size, int addlFlags = 0);

    virtual void setRecvBufSize(int size);
    virtual int getRecvBufSize() const;

private:
    int pathToSockaddr(void *sockaddr, const char *path, bool abstract);
};

} // namespace bb

inline size_t bb::Socket::ByteSource_read(void *buf, size_t size) { return recv(buf, size); }
inline size_t bb::Socket::ByteSink_write(const void *buf, size_t size) { return send(buf, size); }

inline ssize_t bb::Socket::recvmsg_nothrow(struct ::msghdr * hdr, int flags){
    return ::recvmsg(getFD(), hdr, flags );
}

inline ssize_t bb::Socket::recv_nothrow(void *buf, size_t size, int addlFlags)
{
    return ::recv(getFD(), voidptr_to_sockbuf(buf), size, addlFlags);
}

inline ssize_t bb::Socket::send_nothrow(const void *buf, size_t size, int addlFlags)
{
    return ::send(getFD(), static_cast<const char*>(buf), size, MSG_NOSIGNAL | addlFlags);
}

inline ssize_t bb::Socket::sendmsg_nothrow(struct ::msghdr * hdr, int flags){
    return ::sendmsg(getFD(), hdr, flags );
}

inline ssize_t bb::UDPSocket::recvMultiple_nothrow(struct ::mmsghdr *messages, size_t messages_capacity, int addlFlags)
{
    return ::recvmmsg(getFD(), messages, messages_capacity, addlFlags, NULL/*timeout*/);
}

inline ssize_t bb::SCTPSocket::recvMultiple_nothrow(struct ::mmsghdr *messages, size_t messages_capacity, int addlFlags)
{
    return ::recvmmsg(getFD(), messages, messages_capacity, addlFlags, NULL/*timeout*/);
}

#endif // BB_IO_SOCKET_H
