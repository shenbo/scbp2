#ifndef BB_IO_IRECVTRANSPORT_H
#define BB_IO_IRECVTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <exception>
#include <vector>

#include <boost/function.hpp>
#include <boost/noncopyable.hpp>

#include <bb/core/timeval.h>
#include <bb/core/network.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/MStreamCallback.h>
#include <bb/core/FDSetFwd.h>

namespace bb {

class TransMplexStream;

/// Any live message stream which can be multiplexed using select.
/// To receive messages from this stream, implement IMStreamCallback, and use
/// beginDispatch/endDispatch to get callbacks when a message has been received.
class IRecvTransport : public boost::noncopyable
{
public:
    typedef boost::function<void()> EndOfStreamCB;
    typedef boost::function<bool( const std::exception& )> ErrorCB;

    IRecvTransport(  )
        : m_lastNicTimestamp( NoNicTime() )
        , m_lastCbTimestamp( bb::timeval_t::earliest )
    {}

    virtual ~IRecvTransport();

    /// onDispatchBegin sets up the recv transport so that the listener will be called
    /// any time there's a message available on the MStream.
    ///
    /// The end of stream callback is optional and will be called if a
    /// connection-oriented stream ends.
    ///
    /// The error callback will be called if there are non-fatal errors from the underlying
    /// transport. Return true if you've handled the error somehow( This will prevent a throw ).
    /// Return false if you'd like the transport to throw (the default).
    /// Not all the transports support this right now.
    virtual void onDispatchBegin(
        TransMplexStream *mplexer, IMStreamCallback* messageCB,
        const EndOfStreamCB& endOfStreamCB = EndOfStreamCB(),
        const ErrorCB& errorCB = ErrorCB() );

    /// If your program does not have a TransMplexer, this is the
    /// BeginDispatch method which will be called. The program is
    /// then responsible for managing its RecvTransports
    virtual void beginDispatch(
        FDSet &fdset, IMStreamCallback* messageCB,
        const EndOfStreamCB& endOfStreamCB = EndOfStreamCB(),
        const ErrorCB& errorCB = ErrorCB() );

    /// Cancels the subscription which beginDispatch created.
    virtual void endDispatch() = 0;

    /// Return the Nic Timestamp associated with the last pakcet received
    /// by the transport.  If timestamps are not supported on the machine
    /// this function will return the same value as NoNicTime()
    const timeval_t& getNicTimestamp() const;

    /// Return the Callback Timestamp associated with the last pakcet received
    /// by the transport.
    const timeval_t& getCallbackTimestamp() const { return m_lastCbTimestamp; }

    const bb::sockaddr_ipv4_t& getMessageSourceAddress() const;

    static const timeval_t& NoNicTime() { return timeval_t::earliest; };

protected:
    timeval_t                                               m_lastNicTimestamp;
    timeval_t                                               m_lastCbTimestamp;
    bb::sockaddr_ipv4_t                                     m_lastMsgSourceAddr;

    void setLastMsgSourceAddress ( bb::sockaddr_ipv4_t const & addr )
    {
        m_lastMsgSourceAddr = addr;
    }

    void setLastMsgSourceAddress ( ::msghdr const & msg_hdr )
    {
        const ::sockaddr_in* addr = (const ::sockaddr_in*)( msg_hdr.msg_name );
        setLastMsgSourceAddress ( bb::sockaddr_ipv4_t ( *addr ) );
    }

    void setLastMsgSourceAddress ( ::mmsghdr const & hdr )
    {
        BB_THROW_EXASSERT_SSX ( hdr.msg_hdr.msg_namelen == sizeof(struct ::sockaddr), "sockaddr size mismatch" );
        setLastMsgSourceAddress ( hdr.msg_hdr );
    }

};
BB_DECLARE_SHARED_PTR( IRecvTransport );

typedef std::vector<IRecvTransportPtr> IRecvTransportVec;

} // namespace bb

#endif // BB_IO_IRECVTRANSPORT_H
