#ifndef BB_IO_THROTTLEDSENDTRANSPORT_H
#define BB_IO_THROTTLEDSENDTRANSPORT_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include <map>
#include <boost/function.hpp>
#include <boost/unordered_map.hpp>
// #include <bb/core/hash_map.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/instrument.h>
#include <bb/io/ISendTransport.h>
#include <bb/core/Msg.h>

namespace bb {
class Msg;



class ThrottledSendTransport : public ISendTransport
{
public:
    typedef boost::function<bool (const Msg *)> Throttle;

    ThrottledSendTransport( const Throttle&, const ISendTransportPtr& send );

    void send( const Msg *m );

protected:
    ISendTransportPtr m_sendTransport;
    Throttle throttle;
};
BB_DECLARE_SHARED_PTR( ThrottledSendTransport );

//Throttles

class MsgIntervalThrottle
{
public:
    MsgIntervalThrottle( double min_msg_interval );
    bool check_throttle( const Msg* );
protected:
    const double m_min_msg_interval;

    typedef std::pair<bb::instrument_t, bb::mtype_t> ThrottleKey;
    // We're always going to look up by key, and will never iterate over these
    // nor care about the order of items. An unordered map works best for those
    // purposes.
    typedef boost::unordered_map<ThrottleKey, bb::timeval_t> TimevalMap;
    TimevalMap m_timeval_map;

};
BB_DECLARE_SHARED_PTR( MsgIntervalThrottle );

class PriceMoveThrottle
{
public:
    PriceMoveThrottle( uint32_t bips );

    bool check_price_move( const Msg* );
protected:
    uint32_t m_bips;

    typedef boost::unordered_map<instrument_t, double> PrevPriceMap;

    PrevPriceMap m_prevPrice;
};
BB_DECLARE_SHARED_PTR( PriceMoveThrottle );

} // namespace bb

#endif // BB_IO_THROTTLEDSENDTRANSPORT_H
