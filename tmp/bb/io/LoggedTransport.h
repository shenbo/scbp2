#ifndef BB_IO_LOGGEDTRANSPORT_H
#define BB_IO_LOGGEDTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Msg.h>

#include <bb/core/ptime.h>

#include <bb/io/ByteSink.h>

namespace bb {

/// This template class supports logging all input/output to a byte sink
///
/// For the shared ptrs, you can use something like LoggedTransport<T>::Ptr
///
/// Can be used like:
/// @code
///     CFilePtr outfile( new CFile( "lala.txt", "a" ) );
///     LoggedTransport<SocketSendTransport> trans( new SocketSendTransport, outfile );
///     UserMessageMsg msg;
///     trans.send( &msg );
/// @endcode

/// NOTE: LoggedTransport does not flush the ByteSink after
/// writing. When instantiating a LoggedTransport, you should either
/// use a ByteSink implementation that does not buffer (so that writes
/// go directly to the underlying), or wrap your output in a ByteSink
/// that calls 'flush' after each write.

template<typename T> class LoggedTransport : public T
{
public:
    typedef boost::shared_ptr<T> Ptr;

    LoggedTransport( boost::shared_ptr<T> _parent, ByteSinkPtr sink = ByteSinkPtr() )
    : parent( _parent )
    , logSink( sink )
    {
    }

    void send( const Msg* msg )
    {
        if( parent )
            parent->send( msg );

        if( logSink )
        {
            logSink->writeN( msg->asDatagram(), msg->datagramSize() );
        }
    }

    const Msg* recv()
    {
        const Msg* msg = parent->recv();
        if( msg )
            log( msg );
        return msg;
    }

    void setLog( ByteSinkPtr sink ) { logSink = sink; }
    ByteSinkPtr getLog() { return logSink; }

    void setTransport( Ptr parent_ ) { parent = parent_; }
    Ptr getTransport() { return parent; }

protected:
    // to be called by derived classes which don't have virtual send/recv
    void log( const Msg* msg )
    {
        if( logSink )
            logSink->writeN( msg->asDatagram(), msg->datagramSize() );
    }

    Ptr parent;

private:
    ByteSinkPtr logSink;
};

} // namespace bb

#endif // BB_IO_LOGGEDTRANSPORT_H
