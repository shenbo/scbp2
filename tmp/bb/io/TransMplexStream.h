#ifndef BB_IO_TRANSMPLEXSTREAM_H
#define BB_IO_TRANSMPLEXSTREAM_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <list>

#include <bb/core/MStream.h>
#include <bb/core/FDSet.h>

#include <bb/io/RecvTransport.h>

#include <bb/core/messages.h>
#include <bb/core/CompactMsg.h>

namespace bb {

//class OnIdleMsg;
//BB_FWD_DECLARE( OnIdleMsg );

/// A LiveStream which multiplexes multiple RecvTransports.
/// The RecvTransport must be Socket-based.
class TransMplexStream
    : public LiveMStream
{
public:
    TransMplexStream();
    TransMplexStream( const FDSetPtr& );
    virtual ~TransMplexStream();

    virtual void run(IMStreamCallback *mstreamCB );
    virtual void startDispatch(IMStreamCallback *mstreamCB );
    virtual void reset();
    virtual void stop();
    virtual void endDispatch();

    /// Adds the passed RecvTransport to the multiplexer.
    void add( IRecvTransportPtr spTrans,
              const IRecvTransport::EndOfStreamCB &endOfStreamCB = IRecvTransport::EndOfStreamCB(),
              const IRecvTransport::ErrorCB &errorCB = IRecvTransport::ErrorCB() );

    void add( const IRecvTransportVec& spTrans,
              const IRecvTransport::EndOfStreamCB &endOfStreamCB = IRecvTransport::EndOfStreamCB(),
              const IRecvTransport::ErrorCB &errorCB = IRecvTransport::ErrorCB() );

    /// Removes the passed RecvTransport from the multiplexer.
    void remove( IRecvTransportPtr spTrans );

    /// This allows to you install hooks for your own FDs in the main loop,
    /// if you need to listen to sockets which aren't related to messages.
    FDSetPtr getFDSet();
    FDSet &getDispatcher();

    // for the message we're currently processing, we can retrieve the nic timestamp and source address
    const timeval_t& getLastMsgReceiveNicTimestamp( ) const              { return likely ( (bool) m_lastMsgTransport ) ? m_lastMsgTransport->getNicTimestamp() : bb::timeval_t::earliest; };
    const bb::sockaddr_ipv4_t& getLastMsgSourceAddress() const                  { return likely ( (bool) m_lastMsgTransport ) ? m_lastMsgTransport->getMessageSourceAddress() : g_emptyAddr; };

private:
    // Class used as container for all the information related to a recvTransport.
    // Object of this class are created in the TransMplexStream::add function and added
    // to a list
    class RecvTransportInfo : public IMStreamCallback
    {
    public:
        RecvTransportInfo(
            TransMplexStream * transMplex,
            const IRecvTransportPtr& transport_,
            const IRecvTransport::EndOfStreamCB& eof_cb_,
            const IRecvTransport::ErrorCB error_cb_ )
            : m_transMplex( transMplex)
            , m_transport(transport_)
            , m_eofCB(eof_cb_)
            , m_errorCB(error_cb_)
        {};

        void doBeginDispatch( IMStreamCallback * handler, IRecvTransport::EndOfStreamCB& transMplexEofCB){
            // Set this object as the handler
            // and store the callback for use by this objects onMessage callback
            m_transport->onDispatchBegin( m_transMplex, this, transMplexEofCB, m_errorCB );
        }

        virtual void onMessage( const Msg& msg ){
            // now the transmplexer knows who sent the message and can retrieve ( optionally )
            // - nic timestamp
            // - sending hostname
            m_transMplex->setLastMsgTransport ( m_transport );
            // call the callback
            m_transMplex->m_runningCB->onMessage( msg );
        };

        IRecvTransportPtr& getTransport() {return m_transport; };
        IRecvTransport::EndOfStreamCB& getEofCB() {return m_eofCB;};
    private:

        TransMplexStream *             m_transMplex;  // this is a pointer to the parent TransMPlexStream that
                                                      // is shared by all RecvTransportInfo objects
        IRecvTransportPtr              m_transport;
        IRecvTransport::EndOfStreamCB  m_eofCB;
        IRecvTransport::ErrorCB        m_errorCB;
    };
    friend class RecvTransportInfo;
    void setLastMsgTransport ( IRecvTransportPtr & transport )
    {
        m_lastMsgTransport = transport;
    }

    typedef std::list<RecvTransportInfo> TransportList_t;
    struct StreamImpl;
    struct RunScope;

    void doBeginDispatch(TransportList_t::iterator i);
    void removeInternal(TransportList_t::iterator i);
    void eofHandler(TransportList_t::iterator i);

    boost::shared_ptr<StreamImpl> m_pimpl;
    IMStreamCallback *            m_runningCB;
    TransportList_t               m_transports;
    IRecvTransportPtr             m_lastMsgTransport;
    CompactMsg<OnIdleMsg>         m_onIdleMsg;
    static bb::sockaddr_ipv4_t    g_emptyAddr;
};

BB_DECLARE_SHARED_PTR( TransMplexStream );


} // namespace bb

#endif // BB_IO_TRANSMPLEXSTREAM_H
