#ifndef BB_IO_ASYNCBYTESINK_H
#define BB_IO_ASYNCBYTESINK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <queue>
#include <vector>

#include <boost/function.hpp>
#include <boost/noncopyable.hpp>

#include <bb/core/FDSetFwd.h>
#include <bb/core/SlidingBuffer.h>
#include <bb/core/Subscription.h>
#include <bb/core/ReactorCallback.h>

#include <bb/io/ByteSink.h>

namespace bb {

class NBFD;


/// AsyncByteSink wraps another ByteSink and buffers data to it.
/// Calls to AsyncByteSink::write() will not block, and will copy data into a buffers if needed.
/// Later, this buffer is consumed and sent out the wrapped ByteSink.
class AsyncByteSink : public ByteSink, boost::noncopyable
{
public:
    /// The AsyncByteSink can generate errors after your send() has returned.
    /// By default, we'll throw and the program will be terminated.
    ///
    /// To address this, pass a ErrorCB. Any time a non-fatal error happens
    /// outside of a call to send()/flush(), it'll get called.
    /// If your handler returns true, the error won't get thrown.
    typedef boost::function<bool(const std::exception &)> ErrorCB;

    // the FD must be the same object the ByteSinkPtr points to (awkward, but there's no FDByteSink in the class hierarchy)
    // the FDSet must remain valid until this is destroyed
    // coalesce allows fragmentation & coalescing (appropriate for TCP or files but not UDP or MQ)
    AsyncByteSink( ByteSinkPtr sink, NBFD& fd, FDSet& fdset,
                   bool coalesce, const ErrorCB &errCB = ErrorCB(), bool verbose = true );
    virtual ~AsyncByteSink();

    bool bufferEmpty() const { return m_buffer.empty(); }

    // flushes the buffer, which in most cases will block (assuming FDSet::select() has been pumping)
    virtual void flush();

    size_t send( const void* buf, size_t size );

protected:
    void flushBuffer();
    virtual size_t ByteSink_write( const void* buf, size_t size ) { return send( buf, size ); }
    void delayedSendCallback(); // called back by FDSet or directly by flush()

    ByteSinkPtr               m_sink;
    NBFD&                     m_fd;
    FDSet&                    m_dispatcher;
    const bool                m_wasNonBlocking;
    const ReactorCallback     m_cb;
    Subscription              m_sub;
    ErrorCB                   m_errorCB;
    bool                      m_verbose;
    SlidingBuffer             m_buffer;
    size_t                    m_deferredWrites;
    const bool                m_coalesce;
    std::queue<size_t>        m_chunkSizes; // to segregate writes when coalescing is disabled (e.g. for datagrams)
};
BB_DECLARE_SHARED_PTR( AsyncByteSink );


} // namespace bb

#endif // BB_IO_ASYNCBYTESINK_H
