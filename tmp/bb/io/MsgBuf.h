#ifndef BB_IO_MSGBUF_H
#define BB_IO_MSGBUF_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string.h>
#include <boost/noncopyable.hpp>

#include <bb/core/bbassert.h>
#include <bb/core/bbint.h>
#include <bb/core/conv_utils.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Error.h>


namespace bb {

/// Thrown on malformed data or packet problems.
struct BadPacketError : public bb::Error
{
    BadPacketError(const std::string &s) : Error(s) {}
};

namespace util {

BB_FWD_DECLARE_SHARED_PTR(MsgBuf);

/// MsgBuf is a utility for deserializing binary data from a bytestream.
/// You can read read through data from begin to end, unpacking various
/// datatypes as you go.
class MsgBuf : public boost::noncopyable
{
public:
    struct SubRange;

    MsgBuf(int sz = 1024); /// allocates a buffer with a fixed size
    MsgBuf(const SubRange &subrange);
    ~MsgBuf();

    // Resize this buffer to the desired size. Any data held in the
    // buffer is discarded. This operation is only valid if this
    // object owns the underlying buffer, and will throw if called on
    // a buffer that does not own it storage.
    void resize(int desired);

    /// Clears the buffer and returns a raw pointer for filling the buffer.
    /// The size of this buffer is the number you passed to the MsgBuf constructor.
    void *startWrite();
    /// Once you have written the data you want into the buffer returned by
    /// startWrite, call finishWrite with the number of bytes written.
    void finishWrite(int sz);

    /// Resets the MsgBuf to its initial state
    void clear();

    size_t remainingSize() const;
    bool empty() const;

    /// reads a 8 bit unsigned integer
    uint8_t readUInt8();
    /// reads a 16 bit unsigned big-endian (i.e. network byte order) integer.
    uint16_t readUInt16();
    /// reads a 32 bit unsigned big-endian (i.e. network byte order) integer.
    uint32_t readUInt32();
    /// reads a 64 bit unsigned big-endian (i.e. network byte order) integer.
    uint64_t readUInt64();

    /// reads an integer (ASCII formatted). Trailing spaces will be removed.
    template<int len> uint64_t readAsciiInteger();

    /// This function replaces the many functions used before to cast a byte
    /// into the enums defined in *_consts.h
    template<typename T> T readEnum();

    /// consume len bytes (for raw access to the buffer).
    const void *readRaw(size_t len) { return readRawImpl(len); }
    const char *readBytes(size_t len) { return reinterpret_cast<const char*>(readRawImpl(len)); }

    MsgBuf* clone() const;
    SubRange getSubBuffer(size_t len);
    const void* getCurrent() const { return m_buf; }

    struct SubRange
    {
        unsigned char *m_buf;
        size_t m_size;
    };

protected:
    // for debugging purposes, exposing the entire message so that we can print it out
    const void* getMsgHead() const { return m_bufStart; }
    size_t totalSize() const { return m_totalSize; }

private:
    unsigned char *readRawImpl(size_t len);

    bool m_bufOwned;
    unsigned char *m_bufStart, *m_buf;
    size_t m_remainingBuffer;
    size_t m_totalSize;
};

} // namespace util
} // namespace bb


// inline function definitions

inline bb::util::MsgBuf::MsgBuf(int sz)
    : m_bufOwned(true)
    , m_bufStart(0)
{
    resize(sz);
}

inline bb::util::MsgBuf::MsgBuf(const SubRange &subrange)
    : m_bufOwned(false)
    , m_bufStart(subrange.m_buf)
    , m_buf(subrange.m_buf)
    , m_remainingBuffer(subrange.m_size)
    , m_totalSize(subrange.m_size)
{
}

inline bb::util::MsgBuf::~MsgBuf()
{
    if(m_bufOwned)
        delete []m_bufStart;
}

inline void bb::util::MsgBuf::resize(int desired)
{
    BB_THROW_EXASSERT_SSX(
        m_bufOwned,
        "Invalid attempt to resize buffer we don't own");

    // TODO(acm): We could be smarter and only re-allocate if desired
    // > than the current total size. But presumably this isn't being
    // called often so its not clear that the additional complexity is
    // worth it.

    delete[] m_bufStart;

    m_totalSize = desired;
    m_bufStart = new unsigned char[desired];
    m_buf = m_bufStart;
    m_remainingBuffer = 0;
}

inline void *bb::util::MsgBuf::startWrite()
{
    clear();

    return m_bufStart;
}

inline void bb::util::MsgBuf::finishWrite(int sz)
{
    m_remainingBuffer = sz;
    m_buf = m_bufStart;
}

inline void bb::util::MsgBuf::clear()
{
    if(!m_bufOwned)
        throw BadPacketError("bb::util::MsgBuf: clear() called on frozen buffer");
    m_buf = m_bufStart;
    m_remainingBuffer = 0;
}

inline size_t bb::util::MsgBuf::remainingSize() const { return m_remainingBuffer; }
inline bool bb::util::MsgBuf::empty() const { return remainingSize() == 0; }

inline uint8_t bb::util::MsgBuf::readUInt8()
{
    if(m_remainingBuffer < 1)
        throw BadPacketError("bb::util::MsgBuf: short packet - tried to read uint8_t");
    uint8_t v = m_buf[0];
    m_buf += 1;
    m_remainingBuffer -= 1;
    return v;
}

inline uint16_t bb::util::MsgBuf::readUInt16()
{
    if(m_remainingBuffer < 2)
        throw BadPacketError("bb::util::MsgBuf: short packet - tried to read uint16_t");

    uint16_t v =   (((uint16_t)(m_buf[0])) << 8)
                   |   (uint16_t)(m_buf[1]);

    m_buf += 2;
    m_remainingBuffer -= 2;
    return v;
}

inline uint32_t bb::util::MsgBuf::readUInt32()
{
    if(m_remainingBuffer < 4)
        throw BadPacketError("bb::util::MsgBuf: short packet - tried to read uint32_t");

    uint32_t v =   (((uint32_t)(m_buf[0])) << 24)
                   | (((uint32_t)(m_buf[1])) << 16)
                   | (((uint32_t)(m_buf[2])) << 8)
                   |   (uint32_t)(m_buf[3]);

    m_buf += 4;
    m_remainingBuffer -= 4;
    return v;
}

inline uint64_t bb::util::MsgBuf::readUInt64()
{
    if(m_remainingBuffer < 8)
        throw BadPacketError("bb::util::MsgBuf: short packet - tried to read uint64_t");

    uint64_t v =   (((uint64_t)(m_buf[0])) << 56)
                   | (((uint64_t)(m_buf[1])) << 48)
                   | (((uint64_t)(m_buf[2])) << 40)
                   | (((uint64_t)(m_buf[3])) << 32)
                   | (((uint64_t)(m_buf[4])) << 24)
                   | (((uint64_t)(m_buf[5])) << 16)
                   | (((uint64_t)(m_buf[6])) << 8)
                   |   (uint64_t)(m_buf[7]);

    m_buf += 8;
    m_remainingBuffer -= 8;
    return v;
}

inline unsigned char *bb::util::MsgBuf::readRawImpl(size_t len)
{
    if(m_remainingBuffer < len)
        throw BadPacketError("bb::util::MsgBuf: short packet - tried to read raw");
    unsigned char *ptr = m_buf;

    m_buf += len;
    m_remainingBuffer -= len;
    return ptr;
}

inline bb::util::MsgBuf *bb::util::MsgBuf::clone() const
{
    MsgBuf* buf = new MsgBuf(m_totalSize);
    memcpy(buf->m_bufStart, m_bufStart, m_totalSize);
    buf->m_remainingBuffer = m_remainingBuffer;

    return buf;
}

inline bb::util::MsgBuf::SubRange bb::util::MsgBuf::getSubBuffer(size_t len)
{
    unsigned char *data = readRawImpl(len);
    SubRange subRange;
    subRange.m_buf = data;
    subRange.m_size = len;
    return subRange;
}

template<int len> uint64_t bb::util::MsgBuf::readAsciiInteger()
{
    const char *input = readBytes(len);
    return fstrtoul<len>(input);
}

template<typename T> T bb::util::MsgBuf::readEnum()
{
    return static_cast<T>(readUInt8());
}

#endif // BB_IO_MSGBUF_H
