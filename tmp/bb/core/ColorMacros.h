#ifndef BB_CORE_COLORMACROS_H
#define BB_CORE_COLORMACROS_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

//more colors here: https://wiki.archlinux.org/index.php/Color_Bash_Prompt
#define RED( msg ) "\033[1;31m" msg "\033[0m"
#define GREEN( msg ) "\033[1;32m" msg "\033[0m"
#define YELLOW( msg ) "\033[1;33m" msg "\033[0m"

#endif // BB_CORE_COLORMACROS_H
