#ifndef BB_CORE_CSV_H
#define BB_CORE_CSV_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

// parses csv into a vector. got code from this page:
// http://stackoverflow.com/questions/1120140/how-can-i-read-and-parse-csv-files-in-c

namespace bb
{

class CSVRow
{
public:
    static char DEFAULT_DELIMETER;

    CSVRow( char delimeter = DEFAULT_DELIMETER )
        : m_delimeter( delimeter ){}

    std::string const& operator[](std::size_t index) const
    {
        return m_data[index];
    }
    std::size_t size() const
    {
        return m_data.size();
    }
    void readNextRow(std::istream& str)
    {
        std::string         line;
        std::getline(str,line);

        std::stringstream   lineStream(line);
        std::string         cell;

        m_data.clear();
        while(std::getline(lineStream,cell, m_delimeter ))
        {
            m_data.push_back(cell);
        }
    }

private:
    std::vector<std::string>    m_data;
    char m_delimeter;
};

char CSVRow::DEFAULT_DELIMETER = ',';

class CSVIterator
{
public:
    typedef std::input_iterator_tag     iterator_category;
    typedef CSVRow                      value_type;
    typedef std::size_t                 difference_type;
    typedef CSVRow*                     pointer;
    typedef CSVRow&                     reference;

    CSVIterator(std::istream& str, char delimeter = CSVRow::DEFAULT_DELIMETER)
        : m_str(str.good()?&str:NULL)
        , m_row( delimeter )
    {
        ++(*this);
    }

    CSVIterator()
        : m_str(NULL) {}

    // Pre Increment
    CSVIterator& operator++()
    {
        if( m_str )
        {
            m_row.readNextRow( *m_str );
            m_str = m_str->good() ? m_str : NULL;
        }
        return *this;
    }
    // Post increment
    CSVIterator operator++(int)             {CSVIterator    tmp(*this);++(*this);return tmp;}
    CSVRow const& operator*()   const       {return m_row;}
    CSVRow const* operator->()  const       {return &m_row;}
    std::string const& operator[](std::size_t index) const     { return m_row[ index ]; }

    bool operator==(CSVIterator const& rhs) {return ((this == &rhs) || ((this->m_str == NULL) && (rhs.m_str == NULL)));}
    bool operator!=(CSVIterator const& rhs) {return !((*this) == rhs);}
private:
    std::istream*       m_str;
    CSVRow              m_row;
};

} // namespace bb



// #include <iostream>
// #include <bb/core/CSV.h>

// using namespace std;
// using namespace bb;

// int main(int argc, const char** argv )
// {
//     if( argc == 2 )
//     {
//         std::ifstream       file( argv[1] );

//         for(CSVIterator loop(file);loop != CSVIterator();++loop)
//         {
//             if( loop->size() > 3 )
//                 std::cout << "4th Element(" << (*loop)[3] << ")\n";
//         }

//         return 0;
//     }
//     else
//     {
//         std::cerr << "please enter file name as param" << std::endl;
//         return 1;
//     }
// }




#endif // BB_CORE_CSV_H
