#ifndef BB_CORE_PERSISTENTNUMBER_H
#define BB_CORE_PERSISTENTNUMBER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <fstream>
#include <string>

#include <boost/filesystem.hpp>

#include <tbb/atomic.h>

#include <bb/core/Log.h>
#include <bb/core/RunningDetector.h>
#include <bb/core/Environment.h>
#include <bb/core/env.h>

namespace bb {

/*
    A shared interface to perform the basic functionalities of a PersistentNumber object.
*/
template<typename T>
class PersistentNumberBase
{
public:
    virtual ~PersistentNumberBase() {};

    virtual void initialize_seq_num( const std::string& filename ) = 0;

    virtual PersistentNumberBase<T>& operator++() = 0;

    virtual operator T() const = 0;

    virtual void set( T v ) = 0;
};

/*
    A number that persists across application instances
    The  number is stored in <filename>
    ATTENTION: the number can not be trusted if the program crashes.
*/
template<typename T>
class PersistentNumber : public PersistentNumberBase<T>
{
public:
    PersistentNumber() : m_initialized( false ) {}
    PersistentNumber( const std::string& filename )
    {
        RunningDetector& runningDetector = bb::DefaultCoreContext::getEnvironment()->getRunningDetector();
        runningDetector.initialize( filename );

        if (runningDetector.isRunning())
        {
            LOG_PANIC << "Found running or previous crash detected(file found at: "
                << filename << ".running). PersistentNumber is not valid. Will initialize SeqNum = 0." << bb::endl;
            m_value = 0;
        }
        else
        {
            initialize_seq_num(filename);
        }
    }

    ~PersistentNumber()
    {
        std::ofstream seqfile( m_filename.c_str() );
        seqfile << m_value;
        seqfile.close();
    }

    virtual void initialize_seq_num( const std::string& filename )
    {
        m_filename = filename;
        m_initialized = true;
        if ( boost::filesystem::exists( filename ) )
        {
            std::ifstream fin( filename.c_str() );
            T x;
            fin >> x;
            m_value = x;
            fin.close();
        }
        else
        {
            m_value = 0;
            LOG_WARN << "No PersistentNumber file found at " << filename << ", assign 0 by default." << bb::endl;
        }
    }

    // NOT thread-safe
    // the incrementing itself is atomic. But by the time the caller tries to get the current value,
    // it could have been incremented again by other threads.
    virtual PersistentNumber<T>& operator++()
    {
        BB_ASSERT( m_initialized );
        ++m_value;
        return *this;
    }

    virtual operator T() const
    {
        BB_ASSERT( m_initialized );
        return m_value;
    }

    virtual void set( T v )
    {
        m_value = v;
    }

private:
    bool m_initialized;
    std::string m_filename;
    T m_value;
};

} // namespace bb

#endif // BB_CORE_PERSISTENTNUMBER_H
