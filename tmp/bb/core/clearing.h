#ifndef BB_CORE_CLEARING_H
#define BB_CORE_CLEARING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {

enum
{
    CLEARING_UNKNOWN = 0 // do not change; must match conf/clearing.table
};

}

#endif // BB_CORE_CLEARING_H
