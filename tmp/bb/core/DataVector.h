#ifndef BB_CORE_DATAVECTOR_H
#define BB_CORE_DATAVECTOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// Author:  Lance Diduck

#include <iosfwd>
#include <bb/core/smart_ptr.h>
#include <bb/core/safevalue.h>
#include <bb/core/source.h>
#include <bb/core/symbol.h>
#include <bb/core/timeval.h>
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/static_assert.hpp>
#include <bb/core/LuabindScripting.h>
struct lua_State;
namespace bb {

//Basic interface for storing a uniform time series
//metadata about the series is not included
//This is a common interface to 2 types of storage, a basic_string and mmap file.
//The main use is to 1) have persistent vectors and
//2) use large amount of data without swapping
namespace detail{
struct IDataVectorVoid{
    virtual ~IDataVectorVoid(){}
};
}


template <class ValueType,class Base=detail::IDataVectorVoid>
struct IDataVector:Base{
    typedef std::size_t size_type;
    typedef ValueType value_type;
    typedef IDataVector<ValueType,Base> self_type;
    typedef value_type* iterator;
    typedef value_type const* const_iterator;
    typedef value_type const* const_pointer;
    typedef value_type * pointer;
    typedef value_type & reference;
    typedef value_type const & const_reference;
    typedef Base base_type;
    virtual const_iterator begin()const=0;
    virtual const_iterator end()const=0;
    virtual const_pointer  data()const=0;
    virtual void push_back(const_reference)=0;
    virtual size_type size()const=0;
    virtual void reserve(size_type)=0;
    virtual ~IDataVector(){}

private:
    static self_type const& cref( bb::shared_ptr< self_type const >const& d)
    {
        return *d;
    }
public:
    typedef luabind::class_<self_type,bb::shared_ptr<self_type> > luaclass_type;
    static void registerScripting(luaclass_type&cls){
        cls.property("vals", &self_type::cref, luabind::return_stl_iterator)
        .property("size", &self_type::size)
        .def("push_back", &self_type::push_back)
        .def("reserve", &self_type::reserve);
    }

};

//Implementation that uses a plain vanilla vector
template <class ValueType,class Base=detail::IDataVectorVoid>
struct StandardDataVector:IDataVector<ValueType,Base>{
    typedef StandardDataVector<ValueType,Base> self_type;
    typedef IDataVector<ValueType,Base> base_type;
    typedef typename base_type::iterator iterator;
    typedef typename base_type::const_iterator const_iterator;
    typedef typename base_type::const_reference const_reference;
    typedef typename base_type::size_type size_type;
    typedef typename base_type::value_type value_type;
    typedef typename base_type::const_pointer const_pointer;
    const_iterator begin() const {
        return &*m_data.begin();
    }
    const_iterator end() const {
        return &*m_data.end();
    }
    iterator begin() {
        return &*m_data.begin();
    }
    iterator end() {
        return &*m_data.end();
    }

    void push_back(const_reference d){
        m_data.push_back(d);
    }
    size_type size()const{
        return m_data.size();
    }
    void reserve(size_type s){
        m_data.reserve(s);
    }
    typedef luabind::class_<self_type,base_type,bb::shared_ptr<base_type> > luaclass_type;
    static void registerScripting(luaclass_type&cls){
        cls.def( luabind::constructor<>() );
    }
    const_pointer  data()const{
        return m_data.data();
    }
    iterator
    insert(iterator __position, value_type const& _x)
    {
        return &* m_data.insert(m_data.begin()+(__position-begin()),_x);
    }
    template<typename InputIterator >
    void
    insert(iterator __position, InputIterator  _x,InputIterator  _y)
    {
         m_data.insert(m_data.begin()+(__position-begin()),_x,_y);
    }

private:
    std::basic_string<value_type> m_data;
};

namespace detail{
struct BinaryFileHelper{

    enum open_type{ReadOnly, ReadWrite,ReadWriteNoTruncate};
    enum error_style{Throw, Log};

    void resize_map(  unsigned bytesneeded );
    void resize_file(  unsigned bytesneeded,error_style );
    BinaryFileHelper(  unsigned bytesneeded, std::string const& fname, open_type );
    template<class T> T * get_data()const{
        return reinterpret_cast<T*>(m_data);
    }
    ~BinaryFileHelper();
    bool is_writable() const;
    friend std::ostream& operator<<(std::ostream&,BinaryFileHelper const&);
    bool getUnlink()const;
    void setUnlink(bool)const;
private:
    void * m_data;
    std::string m_fname;//for error reporting
    open_type m_opentype;
    unsigned m_bytesmapped;
    mutable bool m_unlinkonexit;
    BinaryFileHelper(BinaryFileHelper const&);
    void operator=(BinaryFileHelper const&);
};
    struct SimplePersistentVectorData;
}
//this is a uniform series that is saved to a file
//ValueType must be relocatable in memory (i.e. copyable via memcpy
template <class ValueType,class Base=detail::IDataVectorVoid>
struct SimplePersistentVector
    : IDataVector<ValueType,Base>
    , private boost::noncopyable
 {
    typedef SimplePersistentVector<ValueType,Base> self_type;
    typedef IDataVector<ValueType,Base> base_type;
    typedef typename base_type::value_type value_type;
    typedef typename base_type::const_iterator const_iterator;
    typedef typename base_type::iterator iterator;
    typedef typename base_type::const_reference const_reference;
    typedef typename base_type::size_type size_type;
    typedef typename base_type::const_pointer const_pointer;

    iterator begin(){
    //    BB_ASSERT(m_file.is_writable());
        return header().m_data;
    }
    iterator end()  {
    //    BB_ASSERT(m_file.is_writable());
        return &(header().m_data[header().m_size]);
    }
    const_iterator begin()const{
        return header().m_data;
    }
    const_pointer  data()const{
        return  header().m_data;
    }

    const_iterator end()  const{
        return &(header().m_data[header().m_size]);
    }
    void push_back(const_reference d){
        BB_THROW_EXASSERT_SSX(m_file.is_writable(),"Cannot call push_back on read only vector "<<m_file);
        if(header().m_capacity==header().m_size){
            reserve(std::max<size_type>((header().m_capacity) + ((header().m_capacity) /2) ,30));
        }
        header().m_data[(header().m_size++)]=d;
    }
    size_type size()const{
        return header().m_size;
    }
    void reserve(size_type s){
        BB_THROW_EXASSERT_SSX(m_file.is_writable(),"Cannot call reserve on read only vector "<<m_file);
        if(s<= header().m_capacity)return;//nothing to do
        size_type new_capacitybytes=header().sizeOf(s);
        m_file.resize_file(new_capacitybytes,detail::BinaryFileHelper::Throw);
        m_file.resize_map(new_capacitybytes);
        header().m_capacity=s;
    }
   //open for writing
    enum OpenForWritingStyle{
        Truncate,//overwrite existing contents
        DoNotTruncate//Do not overwrite existing contents
    };

    SimplePersistentVector(std::string const& _fname,size_type _capacity,OpenForWritingStyle s=Truncate)
    :m_file(Data::sizeOf(_capacity),_fname,s==Truncate?
                                detail::BinaryFileHelper::ReadWrite:
                                detail::BinaryFileHelper::ReadWriteNoTruncate){
        if(s==Truncate){
            header().m_capacity=_capacity;
            header().m_size=0;
        }else{
            m_file.resize_map(header().sizeOf());
        }
    }
    //open for reading
    SimplePersistentVector(std::string const& _fname)
    :m_file(Data::sizeOf(0),_fname,detail::BinaryFileHelper::ReadOnly)
    {
        m_file.resize_map(header().sizeOf());
    }
    //if writable, resize the file to size of contents
    ~SimplePersistentVector(){
        if(m_file.is_writable()){
            header().m_capacity=header().m_size;
            m_file.resize_file(header().sizeOf(),detail::BinaryFileHelper::Log);
        }
    }
    typedef luabind::class_<self_type,base_type,bb::shared_ptr<base_type> > luaclass_type;
    static void registerScripting(luaclass_type&cls){
        cls.def( luabind::constructor<std::string const& ,size_type>() )
            .def( luabind::constructor<std::string const&>() );
    }
    iterator
    insert(iterator __position, value_type const& _x)
    {
        const size_type _n = __position - begin();
        const size_type _sz=header().m_size;
        if(unlikely(header().m_capacity<(_sz+1))){
            //iterators invalidated
            reserve(std::max<size_type>((header().m_capacity) + ((header().m_capacity) /2),30));
        }

        if(_sz!= _n)//not inserting at end
            ::memmove(begin()+_n+1,begin()+_n,(_sz-_n)*sizeof(value_type));
        ++header().m_size;
        header().m_data[_n]=_x;
        return begin()+_n;
     }
     bool getUnlink()const{
    	 return m_file.getUnlink();
     }
     void setUnlink(bool d)const{
    	 m_file.setUnlink(d);
     }

private:
    class Data{
       friend struct SimplePersistentVector;
       typedef typename SimplePersistentVector::size_type size_type;
       typedef typename SimplePersistentVector::value_type value_type;
       size_type  m_capacity;
       size_type  m_size;
       value_type m_data[1];
       static size_type  sizeOf(size_type cap){
           return (cap)*sizeof(value_type)+sizeof(Data);
       }
       size_type  sizeOf(){
           return (m_capacity)*sizeof(value_type)+sizeof(Data);
       }
    };
    SimplePersistentVector(SimplePersistentVector const&);
    void operator=(SimplePersistentVector const&);
    detail::BinaryFileHelper m_file;
    Data& header(){
        return *(m_file.get_data<Data>());
    }
    Data const & header()const{
        return *(m_file.get_data<Data>());
    }
};

// adaptor for use with only one template parameter
template<class T>
class SimplePersistentVectorAdaptor
    : public bb::SimplePersistentVector<T, detail::IDataVectorVoid>
{
public:
    SimplePersistentVectorAdaptor(
        std::string const& _fname
    )
        : SimplePersistentVector<T, detail::IDataVectorVoid>( _fname )
    {}

    SimplePersistentVectorAdaptor(
        std::string const& _fname,
        typename SimplePersistentVector<T, detail::IDataVectorVoid>::size_type _capacity,
        typename SimplePersistentVector<T, detail::IDataVectorVoid>::OpenForWritingStyle s
    )
        : SimplePersistentVector<T, detail::IDataVectorVoid>( _fname, _capacity, s )
    {}
};

bool registerDataVector(lua_State &state);
//commonly used typedefs
typedef IDataVector<float>              IDataVectorSP;
typedef IDataVector<double>             IDataVectorDP;
typedef SimplePersistentVector<float>   SimplePersistentVectorSP;
typedef SimplePersistentVector<double>  SimplePersistentVectorDP;
typedef StandardDataVector<float>       StandardDataVectorSP;
typedef StandardDataVector<double>      StandardDataVectorDP;
BB_DECLARE_SHARED_PTR(IDataVectorSP);
BB_DECLARE_SHARED_PTR(IDataVectorDP);
BB_DECLARE_SHARED_PTR(IDataVectorSP);
BB_DECLARE_SHARED_PTR(SimplePersistentVectorSP);
BB_DECLARE_SHARED_PTR(SimplePersistentVectorDP);
BB_DECLARE_SHARED_PTR(StandardDataVectorSP);
BB_DECLARE_SHARED_PTR(StandardDataVectorDP);

} //namespace bb


#endif // BB_CORE_DATAVECTOR_H
