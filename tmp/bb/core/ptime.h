#ifndef BB_CORE_PTIME_H
#define BB_CORE_PTIME_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <bb/core/ptimefwd.h>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <bb/core/date.h>
#include <bb/core/timeval.h>

namespace bb {

/// Creates a timeval_t from a ptime_t.
/// Returns timeval_t() if ptime.is_not_a_date_time()
timeval_t ptime_to_timeval( const ptime_t& ptime );

/// Creates a timeval_t from a ptime_duration_t.
/// Returns timeval_t() if ptime_duration.is_not_a_date_time()
timeval_t ptime_duration_to_timeval( const ptime_duration_t& ptime_duration );

/// Returns the total number of seconds in a duration as a floating point value.
double ptime_duration_to_double( const ptime_duration_t& );

/// Converts a floating point number of seconds to a duration.
ptime_duration_t ptime_duration_from_double( double sec );

/// Converts an HMS string to a duration.
ptime_duration_t ptime_duration_from_string( const std::string& s );

/// Converts this timeval_t to a ptime_t.
/// Returns ptime_t() if this is timeval_t().
ptime_t timeval_to_ptime( const timeval_t& timeval );

/// timeval_t - timeval_t should return a ptime_duration_t, but we haven't gotten there yet
ptime_duration_t timeval_diff( const timeval_t& lhs, const timeval_t& rhs );
timeval_nanosecond timeval_diff_fast_to_nanoseconds(const timeval_t& lhs, const timeval_t& rhs);
timeval_microsecond nanosec_to_microsec(timeval_nanosecond nsec);

timeval_t operator +( const timeval_t& timeval, const ptime_duration_t& duration );
timeval_t operator -( const timeval_t& timeval, const ptime_duration_t& duration );

// boost::date_time::time_duration<> provides a member function to multiply by an int only
// we override the int version so that we can then make a double version without ambiguity
ptime_duration_t operator *( const ptime_duration_t&, int );
ptime_duration_t operator *( const ptime_duration_t&, double );

ptime_duration_t operator %( const timeval_t& timeval, const ptime_duration_t& duration );

// equivalent to (timeval - timeval % duration), this special method exists for two reasons:
// 1. it's an obscure usage that deserves a name that can be searched for
// 2. Boost's time_duration ships with microsecond precision by default, but we need nanoseconds
timeval_t timeval_snap( const timeval_t& timeval, const ptime_duration_t& duration );

// The normal timeval_snap snaps to an arbitrary boundary. For large
// durations, or for messages close to midnight, this might snap the
// timeval back in to a previous day. This function always starts the
// quantization boundary at midnight of the given timeval, so will
// never snap a timeval back to a previous day. Obviously, this
// doesn't make sense for a duration greater than 1 day, and such
// requests will be rejected.
timeval_t timeval_snap_in_day( const timeval_t& timeval, const ptime_duration_t& duration );

// Like timeval_snap_in_day above, but for the case where you already
// know what day it is. This is faster since you don't need to do the
// expensive conversion. 'timeval' must actually be a time that occurs
// during 'day'.
timeval_t timeval_snap_in_day( const date_t& day, const timeval_t& timeval, const ptime_duration_t& duration );

ptime_duration_t ptime_duration_abs( const ptime_duration_t& );

} // namespace bb;

bb::timeval_t& operator +=( bb::timeval_t& timeval, const bb::ptime_duration_t& duration );
bb::timeval_t& operator -=( bb::timeval_t& timeval, const bb::ptime_duration_t& duration );

namespace boost {
std::size_t hash_value( const boost::posix_time::time_duration& );
} // namespace boost

#endif // BB_CORE_PTIME_H
