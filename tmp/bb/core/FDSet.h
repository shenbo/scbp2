#ifndef BB_CORE_FDSET_H
#define BB_CORE_FDSET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <boost/noncopyable.hpp>
#include <bb/core/timeval.h>
#include <bb/core/FDSetFwd.h>
#include <bb/core/ReactorCallback.h>
#include <bb/core/Subscription.h>
#include <bb/core/UnixSig.h>
#include <bb/core/Error.h>

namespace bb
{

/// synchronous I/O multiplexing a/la select(). You pass an FD and a callback,
/// the callback gets called when the FD is readable or writeable.
class FDSet : public boost::noncopyable
{
public:
    FDSet();
    virtual ~FDSet();

    /// Adds a file descriptor to be watched. The readCB will be called
    /// when data is readable on the FD, writeCB will be called when data
    /// is writeable. Only one callback of a given type can be registered per FD.
    ///
    /// The outSub will be initialized with a RAII shared_ptr. When the last
    /// reference to that shared_ptr is removed, (for example, by calling
    /// mySub.reset()), the callback will be canceled.
    /// This means you must store outSub somewhere or your callback will
    /// mysteriously go away.
    ///
    /// If you close an FD you're selecting on, you should invalidate the
    /// Subscription first. Passing FDPtrs automatically ensures this will happen.
    virtual void createReadCB(Subscription &outSub, int fd, const ReactorCallback &readCB) = 0;
    virtual void createReadCB(Subscription &outSub, const FDPtr &fd, const ReactorCallback &readCB) = 0;
    virtual void createWriteCB(Subscription &outSub, int fd, const ReactorCallback &writeCB) = 0;
    virtual void createWriteCB(Subscription &outSub, const FDPtr &fd, const ReactorCallback &writeCB) = 0;

    /// Registers a function to be called any time a signal arrives.
    /// Signal handlers registered via this method cannot be unregistered.
    ///
    /// In a multi-threaded program, only 1 FDSet can receive signals!
    /// No EINTR/setThrowOnInterrupt handling should be necessary if you use this.
    void sigAction(int sig, const UnixSigCallback &callback);

    /// Registers a function to be called any time a signal arrives.
    /// Signal handlers registered via this method can be unregistered
    /// by invaliding the subscription. A callback associated with a
    /// signal via this method is unique, and precludes sival_ptr
    /// multiplexing on the same signal.
    void sigAction(Subscription &outSub, int sig, const UnixSigCallback &callback);

    /// Registers a function to be called when a signal arrives with a
    /// matching sival_ptr. The provided sival_ptr must not be NULL,
    /// and must not already have been associated with this signal.
    /// Signal handlers registered via this method can be unregistered
    /// by invaliding the subscription.
    void sigAction(Subscription &outSub, int sig, void* sival_ptr, const UnixSigCallback &callback);

    enum SelectResult { INTERRUPT, TIMEOUT, QUEUED };

    /// Get base Timing information for this reactor
    virtual const timeval_t& getSelectRdyTime()  const { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); return bb::timeval_t::earliest; }
    virtual const timeval_t& getSelectPollTime() const { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); return bb::timeval_t::earliest; }
    virtual uint32_t getFdRdyCount()             const { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); return 0; }
    virtual void setUseSelectTimestamps( bool )        { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); };

protected:
    bool m_signalsRegistered;
    std::vector<Subscription> m_permanentSignalHandlers;
};

BB_DECLARE_SHARED_PTR( FDSet );

class IPolicyDispatcher
{
public:
    IPolicyDispatcher();
    virtual ~IPolicyDispatcher();
    // Adds a reader/writer with an optimized policy.
    virtual void addReader(const SDPolicyHandle &policy, SDReader *reader, const FDPtr &fd) = 0;
    virtual void addWriter(const SDPolicyHandle &policy, SDWriter *writer, const FDPtr &fd) = 0;
};

class IDeferredWorkDispatcher
{
public:
    IDeferredWorkDispatcher();
    virtual ~IDeferredWorkDispatcher();

    // Defer the work in 'callback' until sometime after the current
    // IO callback completes. The provided callback must not block,
    // and should reset the subscription object.
    virtual void defer(Subscription &outSub, const ReactorCallback &callback) = 0;
};

BB_DECLARE_SHARED_PTR( IDeferredWorkDispatcher );

} // namespace bb

#endif // BB_CORE_FDSET_H
