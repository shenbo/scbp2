#ifndef BB_CORE_RELOCATABLECOMMONDEFS_H
#define BB_CORE_RELOCATABLECOMMONDEFS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/type_traits.hpp>
#include <boost/array.hpp>
#include <bb/core/relocatablecheck.h>
#include <cassert>//needed by offset_ptr
#include <boost/interprocess/offset_ptr.hpp>
#include <memory>
namespace bb {

template<std::size_t X,std::size_t Y>
struct declare_relocatable<boost::aligned_storage<X,Y> >{

    enum{value = 1
//      && is_relocatable<typename my_type::node_pointer >::value
    };
};

template<class X,class Y> struct declare_relocatable<std::pair<X,Y> >{
    //typedef std::pair<X,Y> my_type;
    enum{value = 1
        && is_relocatable<X >::value
        && is_relocatable<Y >::value
    };
};
template<class PointedType>
struct declare_relocatable<boost::interprocess::offset_ptr<PointedType> >{
    enum{value = 1
        && is_relocatable<std::ptrdiff_t>::value
    };


};
template<class X,std::size_t N>
struct declare_relocatable<boost::array<X,N> >{
    enum{value = 1
        && is_relocatable< X >::value
    };


};
} // namespace bb

#endif // BB_CORE_RELOCATABLECOMMONDEFS_H
