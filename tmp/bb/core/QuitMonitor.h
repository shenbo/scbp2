#ifndef BB_CORE_QUITMONITOR_H
#define BB_CORE_QUITMONITOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/EventFD.h>
#include <bb/core/FDSet.h>

namespace bb {

class FDSet;

/// Supports sending a "quit" command between threads, this is a
/// simple way to interrupt FDSet::select() and recognize
/// that it's time for a procedure to terminate.
class QuitMonitor
{
public:
    QuitMonitor() : m_bquit( false ) {}

    void registerSelect( Subscription& sub, FDSet& fdSet );

    operator bool () const { return m_bquit; }

    void signalQuit();
    void reset();

protected:
    EventFD m_event;
    bool m_bquit;
};

/// Exception for triggering a graceful exit of a thread's main loop.
class QuitException : public std::exception
{
    virtual const char *what() const throw() { return "bb::QuitException"; }
};

/// Enables sending a QuitException to an FDSet-based thread.
///
/// Reason to prefer this over QuitMonitor: our main loops are getting more
/// and more complicated (i.e. SelectDispatcher policies). It's hard to provide
/// for timely exit using a quit flag; the quit flag needs to be checked in too
/// many different places. Throwing an exception guarantees timely exit and is
/// more efficient in the common case.
class ThreadQuitHook
{
public:
    ThreadQuitHook() {}
    ThreadQuitHook( FDSet &set ) { registerSelect(set); }

    void registerSelect( FDSet& fdSet );
    /// Raises a QuitException in the thread which has registered this ThreadQuitHook.
    void throwQuitException();

protected:
    // TODO use tbb::tbb_exception to allow throwing different exception classes?
    void onReadable();
    Subscription m_sub;
    EventFD m_event;
};

} // namespace bb

#endif // BB_CORE_QUITMONITOR_H
