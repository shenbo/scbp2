#ifndef BB_CORE_DISPOSE_H
#define BB_CORE_DISPOSE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/intrusive/list.hpp>

#include <bb/core/bbassert.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Error.h>
#include <bb/core/Subscription.h>

namespace bb {

/// A mixin to make make implementing the dispose pattern idiomatic
/// http://en.wikipedia.org/wiki/Dispose_pattern

///when dispose is called, the object should stop whatever it is doing, free resources
// and wait to get destroyed
//
struct IDispose
{
    virtual void dispose() = 0; //do not throw ever
    virtual ~IDispose() {}
};
BB_DECLARE_SHARED_PTR( IDispose );

//some helpers to implement the dispose pattern
//we use the dispose pattern when we want to invalidate an object we do not own
//this can get real confusing,
//basically the idea is that we make make weak references to IDispose objects
//it is easy to create a weak reference using intrusive pointers that auto_unlink
namespace detail {
typedef  boost::intrusive::list_base_hook<
    boost::intrusive::link_mode<boost::intrusive::auto_unlink>,
    boost::intrusive::tag<IDispose> > DisposableHook;
struct ReallyLongNameForNull {};
}

//a helper for creating weak references to IDispose objects
//inherit from this to be managed by the LinkableDisposer
struct LinkableDisposable : IDispose, detail::DisposableHook {};

//LinkableDisposer
//when your object is destroyed, you will call dispose() on all remaining live disposable objects
template<class _P = detail::ReallyLongNameForNull>
struct LinkableDisposer : _P
{
    typedef boost::intrusive::list<LinkableDisposable
                                   , boost::intrusive::constant_time_size<false>
                                   , boost::intrusive::base_hook<detail::DisposableHook> >
    DisposerList;
    void addReference( LinkableDisposable& v )
    {
        m_references.push_back( v );
    }

protected:
    ~LinkableDisposer()
    {
        disposeAll();
    }
    void disposeAll()
    {
        for( DisposerList::iterator i = m_references.begin(), e = m_references.end(); i != e; )
        {
            DisposerList::iterator i_s = i;
            ++i;
            i_s->dispose();
            m_references.erase( i_s ); //slightly faster than calling clear()
        }
    }

private:
    DisposerList m_references;
};
// we use the Deadly Embrace when two objects can dispose each other
// DeadlyEmbraceManager does not do anything but remind you that you must implement
// and call invalidate_i() in the dtor of your final type
// invalidate must make the dispose() function have no side effects
// this prevents possible circular calls to each other's dispose methods (i.e. if you happen to call
// an object's dispose() from your dispose()
// an example is keeping a subscription to a timer that may time out. When the timer times out, the subscription is invalidated
// but when the subscription is unsubscribed before the timer times out, then the timer is invalidated
template<class _P = LinkableDisposable>
struct DeadlyEmbraceManager : LinkableDisposer<_P>
{
protected:
    virtual void invalidate_i() = 0; //must call from the final class dtor
};

//Subscriptions
template<class DisposerIType = IDispose>
struct DisposableSubscription : SubscriptionBase, DisposerIType
{
    typedef Subscription::Callback Callback;
    DisposableSubscription( Callback disposecb = Callback() )
        : m_valid( true )
        , m_disposed( disposecb ) {}
    bool isValid() const
    {
        return m_valid;
    }
    void dispose()
    {
        m_valid = false;
        if( m_disposed )
        {
            //this is done so that the callback can delete this object safely
            Callback tmp = m_disposed;
            m_disposed = Callback(); //call this guy only once
            tmp();
        }
    }

protected:
    void invalidate_i()
    {
        m_disposed = Callback(); //dont call if destroyed
        m_valid = false;
    }

private:
    bool m_valid;
    Callback m_disposed;
};

struct DeadlyEmbraceSubscription : DisposableSubscription<DeadlyEmbraceManager<> >
{
    typedef DisposableSubscription<DeadlyEmbraceManager<> > base_type;
    typedef  base_type::Callback DSCallback;
    DeadlyEmbraceSubscription( DSCallback timeout = DSCallback() )
        : base_type( timeout ) {}
    virtual ~DeadlyEmbraceSubscription()
    {
        invalidate_i();
    }

};

//this is used because when you get a shared_ptr for Lua, it does not play well with weak_ptr
struct IntrusiveWeakPtrHookable;
namespace detail {
typedef  boost::intrusive::list_base_hook<
    boost::intrusive::link_mode<boost::intrusive::auto_unlink>,
    boost::intrusive::tag<IntrusiveWeakPtrHookable> > WeakPointerHook;
}

template<class PointerType>
struct IntrusiveWeakPtr
    : detail::WeakPointerHook
{
    typedef bool ( IntrusiveWeakPtr::* unspecified_bool_type )() const;

    bool operator!() const { return !( is_linked() && m_p ); }
    operator unspecified_bool_type() const {
        return ( is_linked() && m_p ) ? &IntrusiveWeakPtr::expired : 0;
    }

    typedef PointerType* pointer;
    typedef PointerType& reference;
    IntrusiveWeakPtr( pointer _p = 0 );

    pointer operator->() const
    {
        BB_ASSERT( *this );
        return m_p;
    }

    reference operator*() const
    {
        BB_ASSERT( *this );
        return *m_p;
    }

    pointer get() const
    {
        return m_p;
    }

    bool expired() const
    {
        return is_linked();
    }

    void reset( pointer _p = 0 );
    IntrusiveWeakPtr( IntrusiveWeakPtr const& );
    IntrusiveWeakPtr& operator=( IntrusiveWeakPtr const& );

private:
    pointer m_p;
};

struct IntrusiveWeakPtrHookable
{
    typedef boost::intrusive::list<detail::WeakPointerHook
                                   , boost::intrusive::constant_time_size<false>
                                   , boost::intrusive::base_hook<detail::WeakPointerHook> > List;

    void addReference( detail::WeakPointerHook& v )
    {
        m_references.push_back( v );
    }

private:
    List m_references;
};

template<class P>
IntrusiveWeakPtr<P>::IntrusiveWeakPtr( pointer _p ) : m_p( _p )
{
    if( _p )
        _p->addReference( *this );
}

template<class P> void
IntrusiveWeakPtr<P>::reset( pointer p )
{
    this->unlink();
    m_p = p;
    if( m_p )
        m_p->addReference( *this );
}

template<class P>
IntrusiveWeakPtr<P>::IntrusiveWeakPtr( IntrusiveWeakPtr const& d )
    : m_p( d.m_p )
{

    if( m_p )
        m_p->addReference( *this );
}

template<class P>
IntrusiveWeakPtr<P> &IntrusiveWeakPtr<P>::operator=( IntrusiveWeakPtr const& d )
{
    this->reset( d.m_p );
    return *this;
}

} //namepsace bb
#endif // BB_CORE_DISPOSE_H
