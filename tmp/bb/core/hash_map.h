#ifndef BB_CORE_HASH_MAP_H
#define BB_CORE_HASH_MAP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <functional>
#include <boost/static_assert.hpp>


#if (defined( __INTEL_COMPILER ) && ( __INTEL_COMPILER <= 800 )) || defined( __INTEL_CXXLIB_ICC )

#include <hash_map>
#define bbext std

#elif defined(WIN32) || defined(WIN64)

#include <hash_map>
#define bbext stdext

#elif defined( __GNUC__ )
#include <tr1/unordered_map>
#include <bb/core/hash.h>
#include <google/dense_hash_map>

#define bbext std::tr1
#define hash_map unordered_map
#define hash_multimap unordered_multimap
#define dense_hash_map  google::dense_hash_map

namespace bb{

/// Base Hash Spec definition
template<typename Key, typename Value, typename HashFcn = std::tr1::hash<Key>, typename EqFcn = std::equal_to<Key> >
struct HashSpec{
    // static values for the default empty and deleted keys
    static Key DefaultEmptyKey;
    static Key DefaultDeletedKey;

    typedef Key  HashKey;
    typedef dense_hash_map<Key,Value, HashFcn, EqFcn > HashMap;
};
/// Initialize the static variables to uncommon values.  These values cannot be used
/// as keys in the hash.
template<typename Key,typename Value, typename HashFcn, typename EqFcn >
Key HashSpec<Key, Value, HashFcn, EqFcn >::DefaultEmptyKey = std::numeric_limits<Key>::max();
template<typename Key,typename Value, typename HashFcn, typename EqFcn >
Key HashSpec<Key ,Value, HashFcn , EqFcn >::DefaultDeletedKey = std::numeric_limits<Key>::max()-1;

/// Template specialize the HashSpec to define the new types for the Default keys
/// and initialize them
template<typename Value, typename HashFcn>
struct HashSpec<const char*, Value, HashFcn>{
    static const char * DefaultEmptyKey;
    static const char * DefaultDeletedKey;
    typedef const char*  HashKey;
    typedef dense_hash_map<const char *,Value, HashFcn , bb_eqstr > HashMap;
};
template<typename Value, typename HashFcn>
const char * HashSpec<const char *, Value, HashFcn>::DefaultEmptyKey = "\n";
template<typename Value, typename HashFcn>
const char * HashSpec<const char *, Value, HashFcn>::DefaultDeletedKey = "\r";


// A hashmap for a fixed size string. We supply the KeyLen in the type and
// figure out everything from there.
template<size_t KeyLength, typename Value>
struct FixedSizeStringHashSpec
{
    struct fixed_size_string
    {
        fixed_size_string( char value = 0 )
        {
            memset(contents, value, KeyLength);
        }

        char contents[KeyLength];

        static fixed_size_string DefaultDeletedKey()
        {
            static fixed_size_string deleted(1);
            return deleted;
        }

        static fixed_size_string DefaultEmptyKey()
        {
            static fixed_size_string empty(0);
            return empty;
        }
    };

    struct hash_fixed_len : public std::unary_function<const fixed_size_string &, size_t>
    {
        size_t operator()(const fixed_size_string & value) const
        {
            static const size_t num_elements = KeyLength / sizeof(size_t);
            BOOST_STATIC_ASSERT(KeyLength > 0);
            // quickly treat the bulk as size_t's and combine them
            size_t seed ( 0 );
            const size_t * st = reinterpret_cast<const size_t *> ( value.contents );
            for(size_t i = 0 ; i < num_elements; i++)
            {
                boost::hash_combine(seed, *(st+i) );
            }
            // use a generic function for the rest
            const size_t rest_hash =
            KeyLength % sizeof(size_t) == 0 ?
            0 :
            boost::hash_range( value.contents + KeyLength - ( KeyLength % sizeof(size_t) ), value.contents + KeyLength );
            boost::hash_combine(seed, rest_hash);
            return seed;
        }
    };

    struct eq_fixed_len : public std::binary_function<const fixed_size_string &, const fixed_size_string &, bool>
    {
        bool operator()(const fixed_size_string & a, const fixed_size_string & b) const
        {
            BOOST_STATIC_ASSERT(KeyLength > 0);
            const bool equal = memcmp(a.contents, b.contents, KeyLength) == 0;
            return equal;
        }
    };

    typedef fixed_size_string HashKey;
    static HashKey DefaultEmptyKey;
    static HashKey DefaultDeletedKey;
    typedef dense_hash_map<HashKey, Value, hash_fixed_len, eq_fixed_len > HashMap;
};
template<size_t KeyLength, typename Value>
typename FixedSizeStringHashSpec<KeyLength, Value>::HashKey FixedSizeStringHashSpec<KeyLength, Value>::DefaultEmptyKey = FixedSizeStringHashSpec<KeyLength, Value>::HashKey::DefaultEmptyKey();

template<size_t KeyLength, typename Value>
typename FixedSizeStringHashSpec<KeyLength, Value>::HashKey FixedSizeStringHashSpec<KeyLength, Value>::DefaultDeletedKey = FixedSizeStringHashSpec<KeyLength, Value>::HashKey::DefaultDeletedKey();

template<typename HashSpecT>
typename HashSpecT::HashMap HashMapCreate( typename HashSpecT::HashKey emptyKey   = HashSpecT::DefaultEmptyKey,
                                           typename HashSpecT::HashKey deletedKey = HashSpecT::DefaultDeletedKey ){
    typename HashSpecT::HashMap hash_map;

    hash_map.set_empty_key( emptyKey );
    hash_map.set_deleted_key( deletedKey );
    return hash_map;
}

template<typename HashSpecT>
typename HashSpecT::HashMap HashMapCreate( size_t initialSize, typename HashSpecT::Key emptyKey, typename HashSpecT::Key deletedKey ){
    typename HashSpecT::HashMap hash_map(initialSize);

    hash_map.set_empty_key( emptyKey );
    hash_map.set_deleted_key( deletedKey );
    return hash_map;
}

}

#else

#error bbext::hash_map not supported on this platform

#endif

#endif // BB_CORE_HASH_MAP_H
