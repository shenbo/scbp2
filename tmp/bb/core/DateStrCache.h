#ifndef BB_CORE_DATESTRCACHE_H
#define BB_CORE_DATESTRCACHE_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/itoa.h>
#include <bb/core/date.h>
#include <bb/core/timeval.h>

namespace bb {

class DateStrCache
{
    static const uint32_t SecPerHour = 60*60;
    static const uint32_t SecPerMin = 60;
public:
    enum Precision { kSec, kMsec, kUsec, kNsec };

    DateStrCache( date_t::Locale l, const bb::timeval_t& tv = bb::timeval_t::now);

    template<Precision TimePrecision>
    const char* getDateStr(const bb::timeval_t& tv);

private:
    void setDate( const bb::timeval_t& tv);

    date_t::Locale   m_locale;
    bb::timeval_t    m_prevMidnight;
    bb::timeval_t    m_nextMidnight;
    uint32_t         m_timeOffset;
    char             m_dateStr[32];
};

template<DateStrCache::Precision TimePrecision>
const char* DateStrCache::getDateStr(const bb::timeval_t& tv)
{
    // if the TV is not within the 2 midnights calcualte, then re-calculate
    // the date( which resets the midnight values )
    if( unlikely( (tv >= m_nextMidnight) ||  (tv < m_prevMidnight  ) ) ){
        setDate( tv );
    }
    // get the hr,min,sec
    uint32_t secThisDay = tv.sec() - m_prevMidnight.sec();
    div_t    div_hr     = div( secThisDay, SecPerHour ); // q = hrs
    div_t    div_min    = div( div_hr.rem, SecPerMin ); // q = min, rem = sec


    char * loc = bb::u32toa_sse2_padded<2>( div_hr.quot, &m_dateStr[m_timeOffset] );
    *loc = ':';
    loc = bb::u32toa_sse2_padded<2>( div_min.quot, loc+1  );
    *loc = ':';
    loc = bb::u32toa_sse2_padded<2>( div_min.rem, loc+1 );
    *loc = '.';

    ++loc;

    // add to the string based on the precision expected
    uint32_t sub_sec;
    switch( TimePrecision ){
    case kMsec:
        sub_sec = tv.usec() / 1000;
        loc = bb::u32toa_sse2_padded<3>( sub_sec, loc );
        break;
    case kUsec:
        sub_sec = tv.usec();
        loc = bb::u32toa_sse2_padded<6>( sub_sec, loc );
        break;
    case kNsec:
        sub_sec = tv.nsec();
        loc = bb::u32toa_sse2_padded<9>( sub_sec, loc );
        break;
    }
    *loc = '\0';
    return m_dateStr;
}

}
#endif // BB_CORE_DATESTRCACHE_H
