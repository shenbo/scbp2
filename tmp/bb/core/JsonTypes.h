#ifndef BB_CORE_JSONTYPES_H
#define BB_CORE_JSONTYPES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <json_spirit.h>

#include <bb/core/mktdest.h>
#include <bb/core/product.h>
#include <bb/core/side.h> //dir_t
#include <bb/core/tif.h>
#include <bb/core/source.h>
#include <bb/core/timeval.h>
#include <bb/core/ptime.h>

namespace bb {

class acct_t;
class currency_t;
class expiry_t;
class instrument_t;
class symbol_t;

template<typename T>
json_spirit::mValue toJsonValue( const T& );

template<> json_spirit::mValue toJsonValue( const bb::currency_t& );

template<> json_spirit::mValue toJsonValue( const bb::dir_t& );

template<> json_spirit::mValue toJsonValue( const bb::expiry_t& );

template<> json_spirit::mValue toJsonValue( const bb::instrument_t& );

template<> json_spirit::mValue toJsonValue( const bb::mktdest_t& );

template<> json_spirit::mValue toJsonValue( const bb::product_t& );

template<> json_spirit::mValue toJsonValue( const bb::symbol_t& );

template<> json_spirit::mValue toJsonValue( const bb::tif_t& );

template<> json_spirit::mValue toJsonValue( const bb::acct_t& );

template<> json_spirit::mValue toJsonValue( const bb::source_t& );

template<> json_spirit::mValue toJsonValue( const bb::timeval_t& );

template<> json_spirit::mValue toJsonValue( const bb::ptime_duration_t& );


template<typename T>
T fromJsonValue( const json_spirit::mValue& );

template<> currency_t fromJsonValue<currency_t>( const json_spirit::mValue& );

template<> dir_t fromJsonValue<dir_t>( const json_spirit::mValue& );

template<> expiry_t fromJsonValue<expiry_t>( const json_spirit::mValue& );

template<> instrument_t fromJsonValue<instrument_t>( const json_spirit::mValue& );

template<> mktdest_t fromJsonValue<mktdest_t>( const json_spirit::mValue& );

template<> product_t fromJsonValue<product_t>( const json_spirit::mValue& );

template<> symbol_t fromJsonValue<symbol_t>( const json_spirit::mValue& );

template<> tif_t fromJsonValue<tif_t>( const json_spirit::mValue& );

template<> acct_t fromJsonValue<acct_t>( const json_spirit::mValue& );

} // namespace bb

#endif // BB_CORE_JSONTYPES_H
