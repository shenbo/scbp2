#ifndef BB_CORE_SYMBOLCONTEXT_H
#define BB_CORE_SYMBOLCONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/date.h>
#include <bb/core/mapdb.h>
#include <bb/core/smart_ptr.h>

namespace bb {

class symbol_t;
BB_FWD_DECLARE_SHARED_PTR(Environment);
BB_FWD_DECLARE_SHARED_PTR(ISymbolContext);
BB_FWD_DECLARE_SHARED_PTR(BasicSymbolContext);

class ISymbolContext
{
public:
    typedef mapdb::const_iterator const_iterator;

    /// Destructor
    virtual ~ISymbolContext() { }

    /// symid-to-string conversion
    virtual const char* sym2str(const symbol_t& sym) const = 0;

    /// symid-to-string conversion
    virtual const char* sym2str(const symbol_t& sym, bool* ok) const = 0;

    /// string-to-symid conversion
    virtual symbol_t str2sym(const char* str) const = 0;

    /// Returns the working date of this Context
    virtual const date_t& getDate() const = 0;

    /// Returns whether the symid is valid
    virtual bool isValid(uint32_t symid) const = 0;

    /// Returns the size
    virtual unsigned int size() const = 0;

    /// Returns the max
    virtual unsigned int max() const = 0;

    virtual const_iterator begin() const = 0;
    virtual const_iterator end() const = 0;


};

class BasicSymbolContext
    : public ISymbolContext
{
public:
    /// Creates a BasicSymbolContext from the file
    static BasicSymbolContextPtr createFromFile(const EnvironmentPtr& env, const std::string& filename);

    /// Creates a BasicSymbolContext from an already-loaded mapdb for the given date
    static BasicSymbolContextPtr createFromMapDB(const mapdb& symbol_table, const date_t& date);

    /// Returns the working date of this Context
    const date_t& getDate() const;

    /// symid-to-string conversion
    const char* sym2str(const symbol_t& sym) const;

    const char* sym2str(const symbol_t& sym, bool* ok) const;

    /// string-to-symid conversion
    symbol_t str2sym(const char* str) const;

    /// Returns whether the symid is valid
    bool isValid(uint32_t symid) const;

    /// Returns the size
    unsigned int size() const;

    /// Returns the max
    unsigned int max() const;

    ISymbolContext::const_iterator begin() const;
    ISymbolContext::const_iterator end()   const;

protected:
    /// Constructor using filename
    BasicSymbolContext(const EnvironmentPtr& env, const std::string& filename);

    /// Constructor using mapdb
    BasicSymbolContext(const mapdb& symbol_table, const date_t& date);

protected:
    /// Holds our symbol table
    mapdb m_symbol_table;

    /// The working date of this Context
    date_t m_working_date;
};

} // namespace bb

#endif // BB_CORE_SYMBOLCONTEXT_H
