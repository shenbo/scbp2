#ifndef BB_CORE_MSTREAM_H
#define BB_CORE_MSTREAM_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/ptime.h>
#include <bb/core/MStreamCallback.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(Msg);

/** A stream of Msgs. */
class MStream
{
public:
    MStream();
    virtual ~MStream() {}

    /// Calls the IMStreamCallback for all messages in the stream.
    virtual void run(IMStreamCallback *c) = 0;
    /// Prematurely ends the mstream.
    virtual void stop() = 0;

    /// Sets the timeout that will be used on the next blocking operation
    /// (and all subsequent ones until the next setNextTimeout). If no data
    /// is received in this interval.
    virtual void setNextTimeout(const bb::ptime_duration_t &timeout) { m_nextTimeout = timeout; }

protected:
    ptime_duration_t m_nextTimeout;

};
BB_DECLARE_SHARED_PTR( MStream );
class IHistMStream: public MStream
{
public:
    /// Returns the next message in the stream, or NULL if done. Throws on error.
    /// The message is owned by the stream, and is only valid until the next time
    /// next() is called.

    virtual const Msg *next() = 0;

};
BB_DECLARE_SHARED_PTR( IHistMStream );
/** A HistMStream represents an MStream that can be multiplexed based on the historical
 *  time found in the timestamp. Typical example is a file of messages on disk.
 */
class HistMStream : public IHistMStream
{
public:
    /// Indicates a problem with the data (usually missing data file or broken file format).
    struct Exception : virtual public std::exception
    {
        Exception() {}
        Exception(std::string const &err) : m_fullErrBuf(err) {}
        ~Exception() throw() {}

        virtual const char *what() const throw() { return m_fullErrBuf.c_str(); }

        std::string m_fullErrBuf;
    };

    struct MissingData : virtual public Exception
    {
        MissingData(std::string const &err)
            : Exception(err) {}
    };

    HistMStream();
    virtual void run(IMStreamCallback *c);
    virtual void stop();

protected:
    bool m_exitStream;
};
BB_DECLARE_SHARED_PTR( HistMStream );


/** A LiveMStream is an MStream that can potentially block. */
class LiveMStream : public MStream
{
public:
    LiveMStream();
};
BB_DECLARE_SHARED_PTR( LiveMStream );


} // namespace bb

#endif // BB_CORE_MSTREAM_H
