#ifndef BB_CORE_CURRENCY_H
#define BB_CORE_CURRENCY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <string.h>
#include <bb/core/bbint.h>

namespace bb {

/// the currency of an instrument
/// the actual value stored is the 3-letter string, although
/// currency_t::UNKNOWN.id() is 0xFFFFFFFF, but prints as "UNK".
/// for backwards compatibility, id 0, which was CUR_USD in the previous enum, is converted to USD.
///
class currency_t
{
public:
    static const uint32_t   UNKNOWN_ID = 0xFFFFFFFF;
    static const currency_t UNKNOWN;
    static const currency_t USD;
    static const currency_t CNY;
    static const currency_t EUR;
    static const currency_t GBP;
    static const currency_t JPY;
    static const currency_t HKD;
    static const currency_t KRW;
    static const currency_t TWD;
    static const currency_t CAD;
    static const uint32_t   USD_ID;

    /// Constructs a currency_t that is USD.
    currency_t()
    {   set_id( USD_ID ); }
    /// Constructs a currency_t with the given id
    /// an id of 0 is considered "USD"
    explicit currency_t( uint32_t id )
    {   set_id( id ); }
    /// Copy constructor
    /// an id of 0 is converted to "USD"
    currency_t( const currency_t& curr )
    {   memcpy(m_curr, curr.m_curr, sizeof(m_curr) ); }
    /// Assumes sz is the standard 3-letter (null-terminated) currency code.
    /// The only validation is of the size (3) + NULL.
    explicit currency_t( const char* sz );

    bool operator ==(const currency_t& curr) const { return (this->id_normalized() == curr.id_normalized()); }
    bool operator !=(const currency_t& curr) const { return !(*this == curr); }
    bool operator < (const currency_t& curr) const { return (this->id_normalized() <  curr.id_normalized()); }

    /// Returns the integer id of this currency_t.
    uint32_t id() const
    {   return ((uint8_t)(m_curr[3]) <<  0) + ((uint8_t)(m_curr[2]) <<  8) +
               ((uint8_t)(m_curr[1]) << 16) + ((uint8_t)(m_curr[0]) << 24);
    }

    /// Returns the normalized id of this currency_t.
    /// The normalized id is the id, modified for any exceptions.
    /// For example, if the id() is 0, this returns USD_ID.
    uint32_t id_normalized() const
    {   uint32_t cid = id();  return (cid == 0) ? USD_ID : cid; }

    /// Returns the string representation of this currency_t, or "UNK" if it is invalid.
    const char* to_string() const;

    /// Returns true if the currency_t is valid.
    /// It is invalid if it is UNKNOWN, or if it is not a 3-letter string.
    bool is_valid() const;

    /// Returns true if the currency_t is considered a USD.
    /// This happens if its id() is 0 or
    bool is_usd() const
    {   uint32_t cid = id();  return (cid == 0) || (cid == USD_ID); }

private:
    /// Sets the id of this currency_t to be cid.
    /// It is possible to set it to an 'invalid' value.  See is_valid().
    void set_id( uint32_t cid )
    {
        m_curr[0] = (char)( (cid >> 24) & 0x000000FF );
        m_curr[1] = (char)( (cid >> 16) & 0x000000FF );
        m_curr[2] = (char)( (cid >>  8) & 0x000000FF );
        m_curr[3] = (char)( (cid >>  0) & 0x000000FF );
    }

private:
    char   m_curr[4];
};

/// Stream operator to convert currency_t into human readable form.
std::ostream& operator <<( std::ostream& out, const currency_t currency );

} // namespace bb

#endif // BB_CORE_CURRENCY_H
