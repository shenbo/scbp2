#ifndef BB_CORE_LUABINDCONTAINERS_H
#define BB_CORE_LUABINDCONTAINERS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <set>
#include <vector>

#include <bb/core/LuabindScripting.h>

namespace bb {

namespace detail {
    template<typename T> const T &return_this(const T &t) { return t; }
}

// utility function, binds a vector<> to lua.
// calling luaBindVector<foo>(..., "FooVec", "FooVec_t")
// will define the class std::vector<foo> with the name FooVec_t.
// It'll also define a function FooVec() which converts a table to a FooVec_t.
template<typename value_t>
bool luaBindVector(lua_State &rL, const char *luaConstrName, const char *luaClassName)
{
    BB_REGISTER_SCRIPTING_ONCE( rL, luaConstrName );

    typedef typename std::vector<value_t> container_t;
    typedef typename container_t::iterator iter_t;

    luabind::module( &rL )
    [
        luabind::class_<container_t>(luaClassName)
            .def( luabind::constructor<>() )
            .def( luabind::constructor<size_t>() )
            .def( luabind::constructor<size_t,const value_t&>() )
            .property( "isEmpty",     &container_t::empty )
            .property( "size",        &container_t::size )
            .property( "capacity",    &container_t::capacity )
            .property( "iterator",    &detail::return_this< container_t >, luabind::return_stl_iterator )
            .def( "push_back",        (void(container_t::*)(const value_t&))&container_t::push_back ) // C++0x has two push_backs
            .def( "pop_back",         &container_t::pop_back )
            .def( "at",               (const value_t&(container_t::*)(size_t) const)&container_t::at,
                    luabind::dependency(luabind::result, _1))
            .def( "at",               (value_t&(container_t::*)(size_t))&container_t::at,
                    luabind::dependency(luabind::result, _1))
            .def( "clear",            &container_t::clear )
            .def( "insert",           (iter_t(container_t::*)(iter_t, const value_t&))&container_t::insert )
            .def( "erase",            (iter_t(container_t::*)(iter_t                ))&container_t::erase )
            .def( "erase",            (iter_t(container_t::*)(iter_t, iter_t        ))&container_t::erase )
    ];
    luabind::globals(&rL)[luaClassName]["luatypename"] = std::string(luaClassName);

    std::string helperFunc = "function ";
    helperFunc += luaConstrName;
    helperFunc += "(tbl)" "\n"
          "    local vec = ";
    helperFunc += luaClassName;
    helperFunc += "()" "\n"
          "    for _,v in ipairs(tbl) do" "\n"
          "        vec:push_back(v)" "\n"
          "    end" "\n"
          "    return vec" "\n"
          "end" "\n";

    LuaExec(&rL).execute(helperFunc);
    return true;
}

template<typename Iterable>
luabind::object generateTable(lua_State* L, Iterable container)
{
    luabind::object table = luabind::newtable(L);
    int n = 0;
    for (typename Iterable::const_iterator it = container.begin(); it != container.end(); ++it)
        table[++n] = *it;
    return table;
}

// utility function, binds a set<> to lua.
// calling luaBindSet<foo>(..., "FooSet", "FooSetPair_t", "FooSet_t")
// will define the class std::set<foo> with the name FooSet_t.
// It'll also define a function FooSet() which converts a table to a FooSet_t.
template<typename value_t>
bool luaBindSet(lua_State &rL, const char *luaConstrName, const char *luaValueName, const char *luaClassName)
{
    BB_REGISTER_SCRIPTING_ONCE( rL, luaConstrName );

    typedef typename std::set<value_t> container_t;
    typedef typename container_t::iterator iter_t;
    typedef std::pair<iter_t, bool> pair_t;
    typedef typename container_t::size_type size_t;

    luabind::module( &rL )
    [
        luabind::class_<pair_t>( luaValueName )
        ,
        luabind::class_<container_t>(luaClassName)
            .def( luabind::constructor<>() )
            .property( "isEmpty",   &container_t::empty )
            .property( "size",      &container_t::size )
            .property( "iterator",  &detail::return_this< container_t >, luabind::return_stl_iterator )
            .def( "clear",          &container_t::clear )
            .def( "insert",         (pair_t (container_t::*)(        const value_t&)) &container_t::insert )
            .def( "insert",         (iter_t (container_t::*)(iter_t, const value_t&)) &container_t::insert )
            .def( "erase",          (size_t (container_t::*)(        const value_t&)) &container_t::erase )
#if defined(__GXX_EXPERIMENTAL_CXX0X__)
            .def( "erase",          (iter_t (container_t::*)(iter_t, iter_t        )) &container_t::erase )
            .def( "erase",          (iter_t (container_t::*)(iter_t                )) &container_t::erase )
#else
            .def( "erase",          (void (container_t::*)(iter_t, iter_t        )) &container_t::erase )
            .def( "erase",          (void (container_t::*)(iter_t                )) &container_t::erase )
#endif
    ];
    luabind::globals(&rL)[luaClassName]["luatypename"] = std::string(luaClassName);

    std::string helperFunc = "function ";
    helperFunc += luaConstrName;
    helperFunc += "(tbl)" "\n"
          "    local s = ";
    helperFunc += luaConstrName;
    helperFunc += "_t()" "\n"
          "    for _,v in ipairs(tbl) do" "\n"
          "        s:insert(v)" "\n"
          "    end" "\n"
          "    return s" "\n"
          "end" "\n";

    LuaExec(&rL).execute(helperFunc);
    return true;
}

// utility function, binds a map<> to lua.
// calling luaBindMap<key, value>(..., "KeyValueMap", "KeyValueMapPair_t", "KeyValueMap_t")
// will define the class std::map<key, value> with the name KeyValueMap_t.
// It'll also define a function KeyValueMap() which converts a table to a KeyValueMap_t.
// can be constructed in Lua like this:
// KeyValueMap { key1 = data1, key2 = data2 }
template<typename key_t, typename data_t>
bool luaBindMap(lua_State &rL, const char *luaConstrName, const char *luaKeyDataName, const char *luaClassName)
{
    BB_REGISTER_SCRIPTING_ONCE( rL, luaConstrName );

    typedef typename std::map<key_t, data_t> container_t;
    typedef typename container_t::iterator iter_t;

// this is a very dirty hack! c++11 ( 0x as well ) expects a const iter and c++98 does not
#if defined(__GXX_EXPERIMENTAL_CXX0X__)
    typedef typename container_t::const_iterator const_iter_t;
#else
    typedef typename container_t::iterator const_iter_t;
#endif

    typedef typename std::pair<iter_t, bool> pair_t;
    typedef typename container_t::value_type value_t;

    luabind::module( &rL )
    [
        luabind::class_<pair_t>( (std::string(luaConstrName) + "InsertResult_t").c_str() )
        ,
        luabind::class_<value_t>( luaKeyDataName )
            .def( luabind::constructor<const key_t&, const data_t&>() )
        ,
        luabind::class_<container_t>(luaClassName)
            .def( luabind::constructor<>() )
            .property( "isEmpty",   &container_t::empty )
            .property( "size",      &container_t::size )
            .property( "iterator",  &detail::return_this< container_t >, luabind::return_stl_iterator )
            .def( "clear",          &container_t::clear )
            .def( "insert",         (pair_t (container_t::*)(const value_t&)) &container_t::insert )
            .def( "erase",          (typename container_t::size_type (container_t::*)(const key_t&)) &container_t::erase )
#if defined(__GXX_EXPERIMENTAL_CXX0X__)
            .def( "erase",          (iter_t (container_t::*)(const_iter_t, const_iter_t)) &container_t::erase )
            .def( "erase",          (iter_t (container_t::*)(const_iter_t        )) &container_t::erase )
#else
            .def( "erase",          (void (container_t::*)(const_iter_t, const_iter_t)) &container_t::erase )
            .def( "erase",          (void (container_t::*)(const_iter_t        )) &container_t::erase )
#endif
    ];
    luabind::globals(&rL)[luaClassName]["luatypename"] = std::string(luaClassName);

    std::string helperFunc = "function ";
    helperFunc += luaConstrName;
    helperFunc += "(tbl)" "\n"
            "    local s = ";
    helperFunc += luaConstrName;
    helperFunc += "_t()" "\n"
            "    for k,v in pairs(tbl) do" "\n"
            "        s:insert(" + std::string(luaKeyDataName) + "(k,v))" "\n"
            "    end" "\n"
            "    return s" "\n"
            "end" "\n";

    LuaExec(&rL).execute(helperFunc);
    return true;
}

} // namespace bb

#endif // BB_CORE_LUABINDCONTAINERS_H
