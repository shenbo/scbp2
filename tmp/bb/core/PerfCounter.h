#ifndef BB_CORE_PERFCOUNTER_H
#define BB_CORE_PERFCOUNTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/*
    File:    PerfCounter.h
    Author:    Evan Wies

    Profiling tools.
    Now uses tbb's timing facilities (tbb::tick_count) instead of simply the RDTSC
*/


#include <limits>
#include <boost/cstdint.hpp>
#include <tbb/tick_count.h>

namespace bb {

//
// PerfCounter class
//
class PerfCounter
{
public:
    /// Constructs a new PerfCounter.  Stores the current timestamp in the object.
    PerfCounter();

    /// Resets the minimum, maximum, total, and current times.
    void        reset();

    /// Starts the timer.
    void        start();

    /// Stops the timer and computes the current time, updating the min and max times.
    /// NOTE:  Times are not updated unless PerfCounter::stop is called!!!
    /// Once a timer is stopped, a subsequent call to PerfCounter::stop can be made without calling PerfCounter::start.
    /// This will measure the time since the last PerfCounter::start.
    void        stop();

    /// Returns the minimum time (in seconds) this counter has recorded.
    double      getMinTime() const                  { return m_timeMin; }

    /// Returns the maximum time (in seconds) this counter has recorded.
    double      getMaxTime() const                  { return m_timeMax; }

    /// Returns the total time (in seconds) this counter has recorded over its lifetime.
    double      getTotalTime() const                { return m_timeTotal; }

    /// Returns the average time (in seconds) this counter has recorded.
    double      getAverageTime() const;

    /// Returns the time (in seconds) this counter has last recorded.
    double      getTime() const                     { return m_timeLast; }

    /// Returns the difference in cycles between the last start/stop series.
    tbb::tick_count::interval_t getLastDifference() const       { return m_interval; }

    /// Returns number of time the PerfCounter has been start/stopped.
    uint64_t    getCount() const                    { return m_numStops; }

private:
    double              m_timeLast;      // last time recorded (seconds)
    double              m_timeTotal;     // total time (seconds)
    double              m_timeMin;       // min time   (seconds)
    double              m_timeMax;       // max time   (seconds)
    uint64_t            m_numStops;      // the number of times the counter has been stopped

    tbb::tick_count::interval_t     m_interval;      // last tick interval measured
    tbb::tick_count                 m_count;
};



//
// LocalPerfCounter
//
// A helper class for PerfCounters.
// Create a local object (on the stack) to start measuring your code performance.
// The object will automatically stop the counter once it goes out of scope.
//
class LocalPerfCounter
{
public:
    // Constructs a LocalPerfCounter, starting the passed PerfCounter.
    LocalPerfCounter( PerfCounter& pc )
        : m_perfCounter( pc )
    {   m_perfCounter.start(); }

    // Destructs the LocalPerfCounter, stopping the passed PerfCounter
    ~LocalPerfCounter()
    {   m_perfCounter.stop(); }

private:
    PerfCounter& m_perfCounter;
};



/*****************************************************************************/
//
// INLINE IMPLEMENTATION
//
/*****************************************************************************/

// PerfCounter constructor
inline PerfCounter::PerfCounter()
{
    reset();
    start();    // fill current timestamp
}


// Reset
inline void PerfCounter::reset()
{
    m_timeLast = m_timeTotal = m_timeMax = 0;
    m_timeMin = std::numeric_limits<double>::max();
    m_interval = tbb::tick_count::interval_t();
    m_numStops = 0;

    m_count = tbb::tick_count();
}


inline double PerfCounter::getAverageTime() const
{
    if ( m_numStops == 0 )  return 0;
    else                    return m_timeTotal / static_cast<double>(m_numStops);
}


// Start
inline void PerfCounter::start()
{
    m_count = tbb::tick_count::now();
}


// Stop
inline void PerfCounter::stop()
{
    tbb::tick_count prev_count( m_count );
    m_count = tbb::tick_count::now();
    m_interval = m_count - prev_count;

    m_timeLast = m_interval.seconds();

    // Update the various values
    m_timeTotal += m_timeLast;
    if ( m_timeLast < m_timeMin )    m_timeMin = m_timeLast;
    if ( m_timeLast > m_timeMax )    m_timeMax = m_timeLast;
    ++m_numStops;
}


} // namespace bb


#endif // BB_CORE_PERFCOUNTER_H
