#ifndef BB_CORE_LUATABLELOADER_H
#define BB_CORE_LUATABLELOADER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sstream>

#include <boost/any.hpp>
#include <boost/function.hpp>

#include <luabind/luabind.hpp>

#include <bb/core/timeval.h>
#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/core/Error.h>
#include <bb/core/type_id.h>
#include <bb/core/ptime.h>

namespace bb {


/** Loads a lua table into a struct.
 * @code
 *
 * struct threshold_param_cfg
 * {
 *     double centering_factor;
 *     double base_threshold;
 *     double queue_adjustment;
 *     double volume_ratio;
 * };
 *
 * void main()
 * {
 *     // Get the configured values
 *     threshold_param_cfg cfg;
 *     LuaTableLoader<threshold_param_cfg> loader;
 *     loader
 *         .entry( "volume_ratio", &threshold_param_cfg::volume_ratio )
 *         .entry( "centering_factor", &threshold_param_cfg::centering_factor )
 *         .entry( "base_threshold", &threshold_param_cfg::base_threshold  )
 *         .entry( "queue_adjustment", &threshold_param_cfg::queue_adjustment );
 *     loader.load( &cfg, thresholds_config );
 * }
 *
 * @endcode
 */

// template specialization for converting special lua objects
template <typename T> void get_luaval( T* dest, const luabind::object& config )
{
    (*dest) = luabind::object_cast<T>( config );
}

template <> inline void get_luaval<ptime_duration_t>( ptime_duration_t* dest, const luabind::object& config )
{
    try {
        (*dest) = luabind::object_cast<ptime_duration_t>( config );
    }
    catch( luabind::cast_failed )
    {
        if( luabind::type(config) == LUA_TNUMBER )
            (*dest) = boost::posix_time::seconds(luabind::object_cast<double>(config));
        else
            throw;
    }
}

template <> inline void get_luaval<timeval_t>( timeval_t* dest, const luabind::object& config )
{
    double d = luabind::object_cast<double>( config );
    (*dest) = timeval_t( d );
}

template <> inline void get_luaval<instrument_t>( instrument_t* dest, const luabind::object& config )
{
    std::string s = luabind::object_cast<std::string>( config );
    (*dest) = instrument_t::fromString( s );
}

template <> inline void get_luaval<luabind::object>( luabind::object* dest, const luabind::object& config )
{
    (*dest) = config;
}

template<typename N> void get_luaval_optional( boost::optional<N>* ptr, const luabind::object& obj )
{
    (*ptr) = luabind::object_cast_nothrow<N>( obj );
}

namespace detail {
template<typename U> inline const std::string type_string() { return "unknown"; }
template<> inline const std::string type_string<int>() { return "int"; }
template<> inline const std::string type_string<double>() { return "double"; }
template<> inline const std::string type_string<float>() { return "float"; }
template<> inline const std::string type_string<std::string>() { return "string"; }
template<> inline const std::string type_string<bool>() { return "bool"; }
template<> inline const std::string type_string<uint32_t>() { return "uint"; }
template<> inline const std::string type_string<bb::sources_t>() { return "SourceVec"; }
} // namespace detail

template<typename T> class LuaTableLoader : public T
{
public:
    typedef boost::function< void (const luabind::object& o) > apply_func_t;

    class Entry
    {
    public:
        std::string key;
        apply_func_t f;
    };

    LuaTableLoader() {}
    LuaTableLoader( const Entry* e, uint32_t num_entries )
    {
        for( uint32_t i = 0; i < num_entries; ++i )
            optional_entry( e[i].key, e[i].f );
    }

    ~LuaTableLoader()
    {
        std::for_each( entries.begin(), entries.end(), boost::checked_delete<ParseEntryBase> );
    }

    void load( const luabind::table<>& config )
    {
        std::for_each( entries.begin(), entries.end()
            , boost::bind( &ParseEntryBase::apply_func, _1, boost::ref( config ) ) );
    }

    std::string help() const
    {
        typename std::vector<ParseEntryBase*>::const_iterator it;
        std::ostringstream oss;
        for( it = entries.begin(); it != entries.end(); ++it )
        {
            oss << (*it)->typeName() << " " << (*it)->key() << std::endl;
        }
        return oss.str();
    }

    // entry specifiers
public:
    LuaTableLoader& entry( const std::string& key, apply_func_t cb )
    {
        entries.push_back( new ParseEntryBase( key, cb ) );
        return *this;
    }

    static void call_nothrow( apply_func_t f, const luabind::object& o )
    {
        try { f( o ); } catch(...) {}
    }

    LuaTableLoader& optional_entry( const std::string& key, apply_func_t cb )
    {
        entries.push_back( new ParseEntryBase( key, boost::bind( &call_nothrow, cb, _1 ) ) );
        return *this;
    }

    template<typename V> LuaTableLoader& entry( const std::string& key, boost::optional<V> T::*vptr )
    {
        entries.push_back( new Evaluable<V>( key, &(this->*(vptr)) ) );
        return *this;
    }

    template<typename V> LuaTableLoader& entry( const std::string& key, V T::*vptr )
    {
        entries.push_back( new ValueEntry<V>( key, this, vptr, boost::optional<V>() ) );
        return *this;
    }

    template<typename V> LuaTableLoader& entry( const std::string& key, V T::*vptr, const V default_value )
    {
        entries.push_back( new ValueEntry<V>( key, this, vptr, default_value ) );
        return *this;
    }

    template<typename E, typename V> LuaTableLoader& entry( const char* key
        , void (E::*vptr) (const std::string&, V ) )
    {
        entries.push_back( new PropertyEntry< E, V >( key, vptr, boost::optional<V>(), false ) );
        return *this;
    }
    template<typename E, typename V> LuaTableLoader& optional_entry( const char* key
        , void (E::*vptr) (const std::string&, V ) )
    {
        entries.push_back( new PropertyEntry< E, V >( key, vptr, boost::optional<V>(), true ) );
        return *this;
    }
    template<typename E, typename V> LuaTableLoader& entry( const char* key
        , void (E::*vptr) (const std::string&, V )
        , const V& default_value )
    {
        entries.push_back( new PropertyEntry< E, V >( key, vptr, default_value, false ) );
        return *this;
    }

private:
    class ParseEntryBase
    {
    public:
        ParseEntryBase( const std::string& _key, apply_func_t _f ) : m_key( _key ), f( _f ) {}
        virtual ~ParseEntryBase() {}

        const std::string key() const { return m_key; }
        virtual const std::string typeName() const { return "unknown"; }

        virtual void apply_func( const luabind::table<>& config ) const
        {
            try {
                f( config[ m_key ] );
            }
            catch( ... )
            {
                throw std::runtime_error( std::string("Failed to read \"") + m_key +"\" from Lua table." );
            }
        }
    protected:
        std::string m_key;
        apply_func_t f;
    };

    template<typename U> class Evaluable : public ParseEntryBase
    {
    public:
        Evaluable( const std::string& _key, boost::optional<U>* _dest )
            : ParseEntryBase( _key, boost::bind( &get_luaval_optional<U>, _dest, _1 ) )
        {}

        virtual const std::string typeName() const { return detail::type_string<U>() + " [optional]"; }
    };

    template<typename E, typename U> class PropertyEntry : public ParseEntryBase
    {
    public:
        typedef void (E::* eval_func_type)( const std::string&, U );
        PropertyEntry( const std::string& _key, eval_func_type _vptr, boost::optional<U> default_value, bool _optional )
            : ParseEntryBase( _key, boost::bind( &get_luaval<U>, &cacheValue, _1 ) )
            , bassigned( false )
            , func( _vptr )
            , defaultValue( default_value )
            , boptional( _optional ) {}
        virtual const std::string typeName() const
        {
            return detail::type_string<U>() + (boptional ? " [optional]" : "");
        }
    protected:
        mutable U cacheValue;
        mutable bool bassigned;
        eval_func_type func;
        boost::optional<U> defaultValue;
        bool boptional;
    };

    template<typename U> class ValueEntry : public ParseEntryBase
    {
    public:
        typedef U T::* value_ptr_type;
        ValueEntry( const std::string& _key, T* _dest, value_ptr_type vptr, const boost::optional<U>& def_val )
            : ParseEntryBase( _key, boost::bind( &get_luaval<U>, &(_dest->*(vptr)), _1 ) )
            , dest( _dest )
            , value_dest( vptr )
            , defaultValue( def_val )
        {}

        virtual const std::string typeName() const { return detail::type_string<U>(); }
        virtual void apply_func( const luabind::table<>& config ) const
        {
            try { ParseEntryBase::apply_func( config ); }
            catch( ... )
            {
                if( defaultValue )
                    dest->*(value_dest) = defaultValue.get();
                else
                    throw;
            }
        }

    protected:
        T* dest;
        U T::* value_dest;
        boost::optional<U> defaultValue;
    };

protected:
    std::vector<ParseEntryBase*> entries;
};

namespace detail
{
class Dummy {};

template<typename T> void get_luaval_default( T* dest, const luabind::object& o, const T& default_value )
{
    try { bb::get_luaval<T>( dest, o ); }
    catch( ... ) { *dest = default_value; }
}

}

class LuaTableLoader0 : public LuaTableLoader<detail::Dummy>
{
public:
    LuaTableLoader0() {}
    LuaTableLoader0( const Entry* e, uint32_t num_entries )
        : LuaTableLoader<detail::Dummy>( e, num_entries ) {}
};

class EvaluatorLoader : public LuaTableLoader<detail::Dummy>
{
};

template<typename T> static LuaTableLoader0::apply_func_t apply_func( T* dest )
{
    return boost::bind( (void (*)( T*, const luabind::object&) ) &get_luaval<T>, dest, _1 );
}

template<typename T> static LuaTableLoader0::apply_func_t apply_func( T* dest, const T& default_value )
{
    return boost::bind( (void (*)( T*, const luabind::object&, const T&) ) &detail::get_luaval_default<T>, dest, _1
        , default_value );
}

/// converts LUA_TNUMBER to "number"
const char *luaType2str(int type);

namespace detail
{
    /// boilerplate for the Impl::get functions below, returns t[key].
    luabind::object luaTableGetBase(luabind::object &t, const char *key, const char *errPrefix);
    luabind::object luaTableGetBase(luabind::object &t, int key, const char *errPrefix);

    // get the raw C++ typename for most types, but specialize for some
    template<typename T>
    struct LuaTableCastTypeID
    {
        static const char* name() { return boost::python::type_info(typeid(T)).name(); }
    };

    template<>
    struct LuaTableCastTypeID<luabind::table<> >
    {
        static const char* name() { return "table"; }
    };

    /// Impl class for luaTableGet (see below).
    /// This is a workaround for not being able to partially specialize functions.
    template<class T>
    struct LuaTableCastImpl
    {
        static T get(luabind::object &t, const char *key, const char *errPrefix)
        {
            luabind::object obj = luaTableGetBase(t, key, errPrefix);
            if(luabind::type(obj) == LUA_TNIL)
                BB_THROW_ERROR_SS(errPrefix << ": " << key << " is nil");
            boost::optional<T> foo = luabind::object_cast_nothrow<T>(obj);
            if(!foo)
                BB_THROW_ERROR_SS(errPrefix << ": " << key << " could not be cast to a " << LuaTableCastTypeID<T>::name());
            return *foo;
        }

        static T get(luabind::object &t, int key, const char *errPrefix)
        {
            luabind::object obj = luaTableGetBase(t, key, errPrefix);
            if(luabind::type(obj) == LUA_TNIL)
                BB_THROW_ERROR_SS(errPrefix << ": object at index " << key << " is nil");
            boost::optional<T> foo = luabind::object_cast_nothrow<T>(obj);
            if(!foo)
                BB_THROW_ERROR_SS(errPrefix << ": object at index " << key << " could not be cast to a " << LuaTableCastTypeID<T>::name());
            return *foo;
        }
    };

    /// specialization for luaTableGet<boost::optional<T> >
    template<class T>
    struct LuaTableCastImpl<boost::optional<T> >
    {
        static boost::optional<T> get(luabind::object &t, const char *key, const char *errPrefix)
        {
            luabind::object obj = luaTableGetBase(t, key, errPrefix);
            if(luabind::type(obj) == LUA_TNIL)
                return boost::none;
            boost::optional<T> foo = luabind::object_cast_nothrow<T>(obj);
            if(!foo)
                BB_THROW_ERROR_SS(errPrefix << ": " << key << " could not be cast to a " << LuaTableCastTypeID<T>::name());
            return *foo;
        }

        static boost::optional<T> get(luabind::object &t, int key, const char *errPrefix)
        {
            luabind::object obj = luaTableGetBase(t, key, errPrefix);
            if(luabind::type(obj) == LUA_TNIL)
                return boost::none;
            boost::optional<T> foo = luabind::object_cast_nothrow<T>(obj);
            if(!foo)
                BB_THROW_ERROR_SS(errPrefix << ": object at index " << key << " could not be cast to a " << LuaTableCastTypeID<T>::name());
            return *foo;
        }
    };

    /// specialization for luaTableGet<luabind::object>
    template<>
    struct LuaTableCastImpl<luabind::object>
    {
        static luabind::object get(luabind::object &t, const char *key, const char *errPrefix)
        {
            luabind::object obj = luaTableGetBase(t, key, errPrefix);
            if(luabind::type(obj) == LUA_TNIL)
                BB_THROW_ERROR_SS(errPrefix << ": " << key << " is nil");
            if(luabind::type(obj) != LUA_TTABLE && luabind::type(obj) != LUA_TUSERDATA && luabind::type(obj) != LUA_TFUNCTION)
                BB_THROW_ERROR_SS(errPrefix << ": " << key << " is not table, userdata, or function");
            return obj;
        }

        static luabind::object get(luabind::object &t, int key, const char *errPrefix)
        {
            luabind::object obj = luaTableGetBase(t, key, errPrefix);
            if(luabind::type(obj) == LUA_TNIL)
                BB_THROW_ERROR_SS(errPrefix << ": object at index " << key << " is nil");
            if(luabind::type(obj) != LUA_TTABLE && luabind::type(obj) != LUA_TUSERDATA && luabind::type(obj) != LUA_TFUNCTION)
                BB_THROW_ERROR_SS(errPrefix << ": object at index " << key << " is not table, userdata, or function");
            return obj;
        }
    };
}

/// luaTableGet<int>(obj, "foo") generically converts obj["foo"] to the desired type,
/// with friendly error messages.
///
/// Specializations:
/// luaTableGet<int>(obj, "foo")                   converts obj.foo to an int
/// luaTableGet<boost::optional<int> >(obj, "foo") allows obj.foo to be nil or int, but throws on anything else.
/// luaTableGet<luabind::object>(obj, "foo")       allows obj.foo to be a lua table or userdata.
///
/// use errPrefix to name what obj is; each error message will start with this string.
template<typename T>
inline T luaTableGet(luabind::object t, const char *key, const char *errPrefix = "lua table")
    { return detail::LuaTableCastImpl<T>::get(t, key, errPrefix); }
template<typename T>
inline T luaTableGet(luabind::object t, int key, const char *errPrefix = "lua table")
    { return detail::LuaTableCastImpl<T>::get(t, key, errPrefix); }

/// luabind can't convert between vectors and lua tables, so as a workaround
/// we do it ourselves. some of the Spec classes have opaque luabind::objects
/// as members, and then this helper function is used to convert to a vector<>
template<typename T>
void luaTable2Vector(std::vector<T> &ivals, const luabind::object &o, const char *errMsg = "luaTable2Vector")
{
    ivals.clear();
    if(!o.is_valid()) // treat nil objects as empty vectors
        return;

    // convert from a lua table to a vector by hand
    int t = luabind::type(o);
    if(t == LUA_TTABLE)
    {
        for(luabind::iterator i(o), end; i != end; ++i)
        {
            size_t key;
            try
            {
                key = luabind::object_cast<size_t>(i.key());
                if(key <= 0)
                    BB_THROW_ERROR_SS(errMsg << ": the table has a negative key");
            }
            catch(luabind::cast_failed &e)
            {
                BB_THROW_ERROR_SS(errMsg << ": the table must have only integer keys, got " << luaType2str(luabind::type(i.key())));
            }

            if(ivals.size() < key)
                ivals.resize(key);

            try
            {
                ivals[key-1] = luabind::object_cast<T>(*i);
            }
            catch(luabind::cast_failed &e)
            {
                BB_THROW_ERROR_SS(errMsg << ": could not cast the object at index " << key << " to the desired type");
            }
        }
    }
    else
        BB_THROW_ERROR_SS(errMsg << ": expected a table, got " << luaType2str(t));
}

} // namespace bb

#endif // BB_CORE_LUATABLELOADER_H
