#ifndef BB_CORE_PROTOBUF_PROTOBUFMSG_H
#define BB_CORE_PROTOBUF_PROTOBUFMSG_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <string>

#include <bb/core/smart_ptr.h>
#include <bb/core/Msg.h>
#include <bb/core/messages.pb.h>

namespace bb {

class ProtoBufMsgBase : public Msg
{
public:
    static constexpr mtype_t kMType = MSG_PROTOBUF;
    virtual mtype_t mtype() const { return kMType; }

    ProtoBufMsgBase()
        : Msg( bb::MSG_PROTOBUF, bb::MSG_MAXDATA )
    {
    }

    ProtoBufMsgBase( char *block )
        : Msg( block )
    {
        unpack();
    }

    virtual bb::Msg *mkself() const
    {
        return new ProtoBufMsgBase();
    }

    void clear()
    {
        m_spMsg->Clear();
        Msg::clear();
    }

    int getMaxPayloadSize() const
    {
       return bb::MSG_MAXDATA - m_spMsg->GetTypeName().length() - 1;
    }


    virtual size_t msgSize() const { return bb::MSG_MAXDATA; }

    virtual void pack();

    virtual void unpack();

    virtual const char *name() const
    {
        return "ProtoBufMsgBase";
    }

    static void addMessageTypeFromProtoFile( const std::string& proto_filename );

    typedef shared_ptr< ::google::protobuf::Message> MessagePtr;

    MessagePtr getMsg() { return m_spMsg; }
    const MessagePtr getMsg() const { return m_spMsg; }

    virtual std::ostream &print(std::ostream &out ) const
    {
        return hdr->print( out )
            << " "
            << "msg_name: " << ( m_spMsg ? m_spMsg->GetTypeName() : "null" ) << " "
            << ( m_spMsg ? m_spMsg->ShortDebugString() : "null protobuf msg" );
    }

protected:
    ::google::protobuf::Message* createMessage(const std::string& typeName);

    MessagePtr m_spMsg;
};

template <class T>
class ProtoBufMsg : public ProtoBufMsgBase
{
public:
    ProtoBufMsg()
    { m_spMsg.reset( new T ); }

    const T* operator->() const { return dynamic_cast<const T*>( m_spMsg.get() ); }

    T* operator->() { return dynamic_cast<T*>( m_spMsg.get() ); }
};

}

#endif // BB_CORE_PROTOBUF_PROTOBUFMSG_H
