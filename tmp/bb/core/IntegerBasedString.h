#ifndef BB_CORE_INTEGERBASEDSTRING_H
#define BB_CORE_INTEGERBASEDSTRING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstddef>
#include <ostream>
#include <string>

#include <boost/static_assert.hpp>

#include <bb/core/hash.h>
#include <bb/core/Error.h>
#include <bb/core/relocatablecheck.h>
#include <bb/core/streamextractor.h>

namespace bb {
class IBSNullDiscriminator{};

namespace detail{
template<class T,std::size_t N>
union ibsbuf{
    T m_num;
    char m_data[N+1];
    BOOST_STATIC_ASSERT(sizeof(T)>=N+1);
    bool operator==(ibsbuf const & r)const{
        return m_num==r.m_num;
    }
    bool operator<(ibsbuf const & r)const{
        return m_num<r.m_num;
    }
    ibsbuf():m_num(0){
        m_data[N]=static_cast<char>(N);
    }
    ibsbuf(ibsbuf const&r):m_num(r.m_num){}

    char& operator[](unsigned i){return m_data[i];}
    char const& operator[](unsigned i)const{return m_data[i];}
};
template<std::size_t>struct ibsstorage{};
template<>struct ibsstorage<1>{typedef uint16_t type;};
template<>struct ibsstorage<2>{typedef uint32_t type;};
template<>struct ibsstorage<3>{typedef uint32_t type;};
template<>struct ibsstorage<4>{typedef uint64_t type;};
template<>struct ibsstorage<5>{typedef uint64_t type;};
template<>struct ibsstorage<6>{typedef uint64_t type;};
template<>struct ibsstorage<7>{typedef uint64_t type;};

template<class T, std::size_t N>
struct String2IntDesc{};


}//detail
/*
IntegerBasedString -- a string that is very small and fast
size   -> O(1)
copy   -> O(1)
compare-> O(1)



IntegerBasedStrings less than 3 chars max_size may be used in case statements:
typedef IntegerBasedString<3> fixmsgtype;
enum {  TradeCapture =fixmsgtype::constant_type::String2Int<'A','E'>::value,
        Order =fixmsgtype::constant_type::String2Int<'D'>::value};
fixmsgtype Myorder("D");
fixmsgtype TradeCap("AE");
switch(TradeCap.get_int()){
    case TradeCapture;
    case Order;
}
No heap usage.
May be read and written from file in binary format
fout.write(TradeCap.data(),sizeof(TradeCap));
May also be placed in shared memory


To distinguish strings from one another, do this
struct CurrencyDisc{};
struct FixDisc{};//note these are empty
typedef IntegerBasedString<3,FixDisc> fixmsgtype;
typedef IntegerBasedString<3,CurrencyDisc> currindtype;
fixmsgtype TradeCap("AE");
currindtype curr("USD");
//TradeCap==curr;//nonsensical and will not compile
void foo(fixmsgtype const&,currindtype const&);
foo(curr,TradeCap);//will not compile


*/

template <std::size_t MAX_SIZE, class Disc=IBSNullDiscriminator>
class  IntegerBasedString
{
public:
    class truncate{};

    typedef IntegerBasedString < MAX_SIZE,Disc  > my_type;
    typedef char value_type;
    typedef std::size_t size_type;
    typedef char* pointer;
    typedef char const* const_pointer;
    typedef pointer iterator;
    typedef const_pointer const_iterator;
    typedef char& reference;
    typedef char const& const_reference;
    typedef std::ptrdiff_t difference_type;
    typedef std::char_traits<char> traits_type;
    typedef typename detail::ibsstorage<MAX_SIZE>::type int_type;
    typedef detail::String2IntDesc<int_type,MAX_SIZE> constant_type;
    typedef detail::ibsbuf<int_type,MAX_SIZE> storage_type;
public:

    IntegerBasedString() { }


    IntegerBasedString(const_pointer beg,const_pointer end)
    {
        size_type const mysize=end-beg;
        resize(mysize);
        traits_type::copy(m_buf.m_data,beg,mysize);
    }
    template<class InputIterator>
    IntegerBasedString(InputIterator beg,InputIterator end)
    {
        size_type const mysize=std::distance(beg,end);
        resize(mysize);
        using std::copy;
        copy(beg,end,m_buf.m_data);
    }
    template<class traits,class Alloc>
    IntegerBasedString(std::basic_string< value_type,traits,Alloc> const&r)
    {
        size_type const mysize=r.size();
        resize(mysize);
        traits_type::copy(m_buf.m_data,r.data(),mysize);
    }
    template<class traits,class Alloc>
    IntegerBasedString(std::basic_string< value_type,traits,Alloc> const&r, size_type len)
    {

        resize(len);
        traits_type::copy(m_buf.m_data,r.data(),len);
    }
    template<class traits,class Alloc>
    IntegerBasedString(std::basic_string< value_type,traits,Alloc> const&r, truncate const&)
    {
        using std::min;
        size_type len=min(r.size(),max_size());
        traits_type::copy(m_buf.m_data,r.data(),len);
        m_buf[MAX_SIZE]=static_cast<char>(MAX_SIZE-len);
    }
    IntegerBasedString(const_pointer seq)
    {
        size_type const mysize=traits_type::length(seq);
        resize(mysize);
        traits_type::copy(m_buf.m_data,seq,mysize);
    }
    IntegerBasedString(const_pointer seq, truncate const&)
    {
        char * dest=m_buf.m_data;
        char * end=m_buf.m_data+max_size();
        while(dest!=end && *seq)*dest++=*seq++;
        m_buf[MAX_SIZE]=static_cast<char>(end-dest);
    }
    IntegerBasedString(const_pointer seq, size_type len)
    {
        resize(len);
        traits_type::copy(m_buf.m_data,seq,len);
    }

    bool operator==(IntegerBasedString const& _r) const
    {  return m_buf==_r.m_buf;
    }
    bool operator!=(IntegerBasedString const&  _r) const
    {   return !(*this==_r);   }

    friend bool operator==(const_pointer _l,my_type const& _r){
        return _r==_l;
    }
    friend bool operator!=(const_pointer _l,my_type const& _r){
        return _r!=_l;
    }
    bool operator!=(const_pointer _r) const {
        return !(*this==_r);
    }
    bool operator==(const_pointer _r) const {
        return 0==strcmp(data(), _r);
       // return 0==traits_type::compare(data(), _r, size());
    }
    bool operator<(const_pointer _r) const {
        size_type const rsize=traits_type::length(_r);
        size_type const mysize=size();
        if (mysize<rsize)
            return true;
        if (mysize>rsize)
            return false;
        return 0>traits_type::compare(data(), _r, mysize);
    }

    friend bool operator<(const_pointer _l,my_type const& _r){
        size_type const lsize=traits_type::length(_l);
        size_type const mysize=_r.size();
        if (lsize <mysize)
            return true;
        if (lsize>mysize)
            return false;
        return 0>traits_type::compare( _l,_r.data(),  mysize);
    }


    bool operator<(IntegerBasedString const&  _r) const
    {
        return m_buf<_r.m_buf;
    }
    bool empty()const{
        return size()==0;
    }
    int_type get_int()const{
        return m_buf.m_num;
    }
    const_pointer data()const{return m_buf.m_data;}
    static std::size_t capacity(){
        return max_size();
    }

    void resize(size_type sz){
        size_type const currentsize=size();
        //if(sz==currentsize)return;
        if(sz==0){
            m_buf.m_num=0;
        }else{
            BB_THROW_EXASSERT_SSX(sz<=max_size()//, std::length_error
                ,"requested size "<<sz<<" greater than max_size() "<<max_size()<< " with "<<typeid(*this).name());
            for(std::size_t i=sz;i<currentsize;++i){
                m_buf[i]=value_type();//need to keep 0 filled, for compares to work correctly
            }
        }
        m_buf[MAX_SIZE]=static_cast<char>(MAX_SIZE-sz);
    }
    size_type size()const{
        return MAX_SIZE-m_buf[MAX_SIZE];
    }
    size_type length()const{
        return size();
    }
    static size_type max_size(){
        return MAX_SIZE;
    }

    const_pointer begin()const{return m_buf.m_data;}
    const_pointer end()const{return m_buf.m_data+size();}
    pointer begin(){return m_buf.m_data;}
    pointer end(){return  m_buf.m_data+size();}
    const_pointer c_str()const{
        //buf is always 0 filled
        return m_buf.m_data;
    }
    void clear(){
        resize(0);
    }
    void assign(size_type n,value_type c)
    {
        resize(n);
        traits_type::assign(m_buf.m_data,n,c);
    }
    void assign(const_pointer seq, truncate const&)
    {
        size_type cap=capacity();
        char * dest=m_buf.m_data;
        char * end=m_buf.m_data+cap;
        while(dest!=end && *seq)*dest++=*seq++;
        resize(cap-(end-dest));

    }


    void assign(const_pointer beg,const_pointer end)
    {
        size_type const mysize=end-beg;
        resize(mysize);
        traits_type::copy(m_buf.m_data,beg,mysize);
    }
    void assign(const_pointer beg)
    {
        size_type const mysize=traits_type::length(beg);
        resize(mysize);
        traits_type::copy(m_buf.m_data,beg,mysize);
    }


    template<class InputIterator>
    void assign(InputIterator beg,InputIterator end)
    {
        size_type const mysize=std::distance(beg,end);
        resize(mysize);
        using std::copy;
        copy(beg,end,begin());
    }
    template<class traits,class Alloc>
    void assign(std::basic_string< value_type,traits,Alloc> const&r)
    {
        size_type const mysize=r.size();
        resize(mysize);
        traits_type::copy(m_buf.m_data,r.data(),mysize);
    }
    void assign(IntegerBasedString const& _r)
    {
        m_buf=_r.m_buf;
    }
    IntegerBasedString& operator=(IntegerBasedString const& _r){
        m_buf=_r.m_buf;
        return *this;
    }
    IntegerBasedString& operator=(const_pointer beg){
        assign(beg);
        return *this;
    }
    template<class traits,class Alloc>
    IntegerBasedString& operator=(std::basic_string< value_type,traits,Alloc> const& beg){
        assign(beg);
        return *this;
    }
    reference operator[](size_type i){return m_buf[i];}
    const_reference operator[](size_type i)const{return m_buf[i];}

    reference at(size_type i){
        BB_THROW_EXASSERT_SSX(i<size(), //std::out_of_range
                "requested index "<<i<<" greater than or equal to size() "<<size()<< " with "<<typeid(*this).name());
        return m_buf[i];
    }
    const_reference at(size_type i)const{
        return const_cast<IntegerBasedString*>(this)->at(i);
    }
private:
     storage_type m_buf;
} ;


namespace detail{

template< >
struct String2IntDesc< ibsstorage<2>::type ,2U>{
    template<char A,char B=0>struct String2Int{
        enum{max_size=2};
        static const typename ibsstorage<2>::type value=((max_size-(B?1:0)-(A?1:0))<<16)+(B<<8)+A;
    };
};

template<char A,char B>
const ibsstorage<2>::type  String2IntDesc< ibsstorage<2>::type ,2U>::String2Int<A,B>::value;
template< >
struct String2IntDesc< ibsstorage<3U>::type ,3U>{

    template<char A,char B=0,char C=0>struct String2Int{
        enum{max_size=3};
        static const typename ibsstorage<3>::type value=((max_size-(B?1:0)-(C?1:0)-(A?1:0))<<(8*max_size))+(C<<(8*2))+(B<<8)+A;
    };
};
template<char A,char B,char C>
const ibsstorage<3>::type  String2IntDesc< ibsstorage<3>::type ,3U>::String2Int<A,B,C>::value;
template< >
struct String2IntDesc< ibsstorage<4U>::type ,4U>{
    template<char A,char B=0,char C=0,char D=0>struct String2Int{
        enum{max_size=4};
        static const typename ibsstorage<4>::type hvalue=(max_size-(D?1:0)-(C?1:0)-(B?1:0)-(A?1:0));
        static const typename ibsstorage<4>::type value=(hvalue<<(8*max_size))+(D<<(8*3))+(C<<(8*2))+(B<<8)+A;
    };
};
template<char A,char B,char C,char D>
const ibsstorage<4>::type  String2IntDesc< ibsstorage<4>::type ,4U>::String2Int<A,B,C,D>::value;

template< >
struct String2IntDesc< ibsstorage<5U>::type ,5U>{
    template<char A,char B=0,char C=0,char D=0,char E=0>struct String2Int{
        enum{max_size=5};
        static const typename ibsstorage<5>::type hvalue=(max_size-(E?1:0)-(D?1:0)-(C?1:0)-(B?1:0)-(A?1:0));
        static const typename ibsstorage<5>::type cvalue=E;
        static const typename ibsstorage<5>::type value=(hvalue<<(8*max_size))+(cvalue<<(8*4))+(D<<(8*3))+(C<<(8*2))+(B<<8)+A;
    };
};
template<char A,char B,char C,char D,char E>
const ibsstorage<5>::type  String2IntDesc< ibsstorage<5>::type ,5U>::String2Int<A,B,C,D,E>::value;

template< >
struct String2IntDesc< ibsstorage<6U>::type ,6U>{
    template<char A,char B=0,char C=0,char D=0,char E=0,char F=0>struct String2Int{
        enum{max_size=6};
        static const typename ibsstorage<6>::type hvalue=(max_size-(F?1:0)-(E?1:0)-(D?1:0)-(C?1:0)-(B?1:0)-(A?1:0));
        static const typename ibsstorage<6>::type cvalue=(F<<8)+E;
        static const typename ibsstorage<6>::type value=(hvalue<<(8*max_size))+(cvalue<<(8*4))+(D<<(8*3))+(C<<(8*2))+(B<<8)+A;
    };
};
template<char A,char B,char C,char D,char E,char F>
const ibsstorage<6>::type  String2IntDesc< ibsstorage<6>::type ,6U>::String2Int<A,B,C,D,E,F>::value;


template< >
struct String2IntDesc< ibsstorage<7U>::type ,7U>{
    template<char A,char B=0,char C=0,char D=0,char E=0,char F=0,char G=0>struct String2Int{
        enum{max_size=7};
        static const typename ibsstorage<7>::type hvalue=(max_size-(G?1:0)-(F?1:0)-(E?1:0)-(D?1:0)-(C?1:0)-(B?1:0)-(A?1:0));
        static const typename ibsstorage<7>::type cvalue=(G<<(8*2))+(F<<8)+E;
        static const typename ibsstorage<7>::type value=(hvalue<<(8*max_size))+(cvalue<<(8*4))+(D<<(8*3))+(C<<(8*2))+(B<<8)+A;
    };
};
template<char A,char B,char C,char D,char E,char F,char G>
const ibsstorage<7>::type String2IntDesc< ibsstorage<7>::type ,7U>::String2Int<A,B,C,D,E,F,G>::value;

}

/// the stream insertion operator
template<class charT, class Traits, std::size_t MAX_SIZE, class Disc>
std::basic_ostream< charT ,Traits > &
operator<<(std::basic_ostream< charT ,Traits >&_m_stream ,
        IntegerBasedString<MAX_SIZE,Disc> const &val)
{
    return _m_stream.write(val.data(),std::streamsize(val.size()));
}


template<class T,std::size_t N>
struct declare_relocatable<detail::ibsbuf<T,N> >{
//unions are always relocatable
    enum{value = 1
    };
};

template<std::size_t N, class T>
struct declare_relocatable<IntegerBasedString<N,T> >{

    enum{value = 1
// gcc cant take a union        && is_relocatable<typename strings::IntegerBasedString<N,T>::storage_type >::value
    };

};





#if 0
template<char A,char B=0,char C=0, char D=0>
struct String2Int{
    enum {size=1+(B?1:0)+(C?1:0)+(D?1:0)};
    BOOST_STATIC_ASSERT(A!=0);
    BOOST_STATIC_ASSERT(B!=0 || (B==0 && C==0 && D==0));
    BOOST_STATIC_ASSERT(C!=0 || (C==0 && D==0));
    static typename detail::ibsstorage<size>::type const value=(D<<24)+(C<<16)+(B<<8)+A;
    friend std::ostream& operator<<(std::ostream&os,String2Int){
        os.put(A);
        if(B)os.put(B);
        if(C)os.put(C);
        if(D)os.put(D);
        return os;

    }


};
#endif


template<class T,class Disc=IBSNullDiscriminator>
struct String2IntDisc:T{};
} // namespace bb


BB_HASH_NAMESPACE_BEGIN {
template<std::size_t MAX_SIZE, class Disc >
struct hash<bb::IntegerBasedString<MAX_SIZE,Disc> >
{
    BOOST_STATIC_ASSERT(MAX_SIZE<sizeof(size_t));
    size_t operator()(  bb::IntegerBasedString<MAX_SIZE,Disc>  val ) const
    {
        return val.get_int();
    }
};

} BB_HASH_NAMESPACE_END
#endif // BB_CORE_INTEGERBASEDSTRING_H
