#ifndef BB_CORE_MKTDESTMAPPINGS_H
#define BB_CORE_MKTDESTMAPPINGS_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/currency.h>
#include <bb/core/mktdest.h>
#include <bb/core/EFeedType.h>

namespace bb {

EFeedType mktdest_to_primary_feed( const mktdest_t& );
currency_t mktdest_to_primary_currency( const mktdest_t& mkt );
}

#endif // BB_CORE_MKTDESTMAPPINGS_H
