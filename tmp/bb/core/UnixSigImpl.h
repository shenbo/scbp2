#ifndef BB_CORE_UNIXSIGIMPL_H
#define BB_CORE_UNIXSIGIMPL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <tbb/atomic.h>

#include <map>

#include <bb/core/UnixSig.h>
#include <bb/core/FDSetFwd.h>
#include <bb/core/Subscription.h>
#include <bb/core/ThreadPipe.h>

namespace bb
{

/// Implementation of sane handling of unix signals (single-threaded only).
/// See SelectDispatcher::sigAction().
///
/// Requires that only one thread will be processing the signals.
/// Uses a pipe to communicate signal reception from signal context to that main thread.
///
/// In live programs, the main SelectDispatcher will wakeup when the pipe is readable.
/// In historical programs, there is no SelectDispatcher so we need to poll, see pollPipe().
class UnixSigDispatcher
{
public:
    static UnixSigDispatcher *instance();

    /// Registers a function to be called any time a signal arrives.
    /// Handler is called from the normal program (not from interrupt
    /// ctx), from within the SelectDispatcher.  Will enable
    /// SA_RESTART for this signal (see sigaction(2)). At most one
    /// callback can be associated with a given signal via this
    /// method. Signals so associated preclude using sival_ptr
    /// multiplexing.
    void sigAction(Subscription &sub, int sig, const UnixSigCallback &callback);

    /// Registers a function to be called when the given signal
    /// arrives, if the signals sival_ptr field matches the provided
    /// sival_ptr argument. The provided sival_ptr argument must not
    /// be NULL, and must not already have been associated with this
    /// signal.
    void sigAction(Subscription &sub, int sig, void* sival_ptr, const UnixSigCallback &callback);

    /// Hooks this SelectDispatcher up to receive the registered signals.
    /// UnixSigDispatcher is global -- all handlers will be invoked within the single
    /// thread that calls beginDispatch.
    /// signals sent to a specific thread are not supported.
    void beginDispatch(FDSet *sd);
    /// Restores the signal handlers to what they were before beginDispatch().
    void endDispatch(FDSet *sd);

    /// Polls the pipe to see if signals are pending.
    /// Used in histo mode, where there is no SelectDispatcher.
    /// This is called every message -- make it cheaper by use a boolean flag rather
    /// than calling read() on the pipe.
    static void pollPipe();

    // Similar to beginDispatch/endDispatch, when the pipe will be polled instead.
    void beginPoll(void *poller) { begin(poller); }
    void endPoll(void *poller) { end(poller); }

protected:
    UnixSigDispatcher();

    bool begin(void *pipeChecker); // returns true if this is the first registration
    void end(void *pipeChecker);

    static void sigHandler(int sig, siginfo_t *info, void *);
    void checkPipe(); // O(m_handlers.size())

    void sigActionInternal(Subscription &sub, int sig, void* sival_ptr, const UnixSigCallback &callback);

    struct CallbackInfo;
    struct Handler;
    BB_DECLARE_SHARED_PTR(Handler);

    typedef std::map<int, HandlerPtr> HandlerMap_t;
    HandlerMap_t m_handlers;
    void *m_pipeChecker;
    Subscription m_pipeSub;
    ThreadPipeReader m_reader; // pipe is read from main thread
    ThreadPipeWriter m_writer; // pipe is written from the signal handler
    tbb::atomic<bool> m_pollPending;

    static UnixSigDispatcher *sm_sigHandler; // singleton
};

} // namespace bb

inline void bb::UnixSigDispatcher::pollPipe()
{
    UnixSigDispatcher *h = sm_sigHandler; // singleton
    if(h && h->m_pollPending)
        h->checkPipe();
}

#endif // BB_CORE_UNIXSIGIMPL_H
