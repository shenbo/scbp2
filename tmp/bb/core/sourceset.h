#ifndef BB_CORE_SOURCESET_H
#define BB_CORE_SOURCESET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <set>
#include <bb/core/source.h>

namespace bb {

typedef std::set<bb::source_t> sourceset_t;

}

#endif // BB_CORE_SOURCESET_H
