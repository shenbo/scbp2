#ifndef BB_CORE_IDNSRESOLVER_H
#define BB_CORE_IDNSRESOLVER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <boost/function.hpp>

#include <bb/core/Error.h>
#include <bb/core/network.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Subscription.h>

namespace bb {

// An abstract base class that defines our asynchronous DNS resolution
// API. The methods defined here are analogous to the blocking_ family
// of blocking DNS operations defined in core/network.h.
//
// The error handling model for an IDNSResolver is a bit subtle. Since
// the interface is asynchronous, it is not possible to throw an
// exception back to the caller at the point of the call in the event
// of a DNS error. Instead, exceptions will propagate after the
// operation has completed, in the context of the underlying FDSet's
// event loop. An implementation of IDNSResolver will catch these
// exceptions, and if an error callback was specified at the call
// site, then that error callback will be invoked with the current
// exception active.
//
// This gives the code that is interested in the results of a DNS
// resolution an opportunity to dispatch on the type of the active
// exception by re-throwing and catching specific types:
//
//    void myErrorCb()
//    {
//        try
//        {
//            throw;
//        }
//        catch( const IDNSResolver::ResolutionError& dnsError )
//        {
//            // deal with failed DNS resolution
//        }
//        catch( const OtherTypeIWantToHandle& y )
//        {
//            // deal with this other type of error
//        }
//
//        // other exceptions propagate up the stack
//    }
//
// A related issue is what occurs if an exception is thrown from a
// 'success' callback. In these cases, the exception will simply
// propagate, and will not be intercepted by the error callback. Note
// that the exception may propagate asynchronously, meaning that it
// will be emitted after a later call to select in the select loop, or
// it may propagate synchronously from the call site if c-ares was
// able to do the lookup without performing any IO.

class IDNSResolver
{
public:
    virtual ~IDNSResolver();

    // Success callback types for the various operations below.
    typedef boost::function<void( const std::string& )> GetFQDNCallback;
    typedef boost::function<void( const ipv4_addr_t& )> LookupHostCallback;
    typedef boost::function<void( const ipv4_addr_t&, const std::string& )> LookupHostWithFQDNCallback;
    typedef boost::function<void( const sockaddr_ipv4_t& )> LookupAddrCallback;
    typedef boost::function<void( const std::string& )> LookupNameCallback;

    // Failure callback type for the various operations below.
    typedef boost::function<void()> ErrorCallback;

    // Errors related to DNS resolution will be propagated as
    // exceptions of type IDNSResolver::ResolutionError.
    BB_DEFINE_ERROR( ResolutionError );

    // Resolve the current machine's fully qualified domain name and
    // invoke the specified callback with the results.
    virtual void getFQDN( const GetFQDNCallback& resultCb,
                          const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    // Resolve the specified hosts fully qualified domain name and
    // invoke the specified callback with the results.
    virtual void getFQDN( const char* hostname,
                          const GetFQDNCallback& resultCb,
                          const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    // Perform a forward name lookup on host in the AF_INET domain,
    // and resolve the specified hosts fully qualified domain name
    // and invoke the specified callback with the result as an
    // ipv4_addr_t and the FQDN.
    virtual void lookupHostWithFQDN( const char* hostname,
                          const LookupHostWithFQDNCallback& resultCb,
                          const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    // Perform a forward name lookup on host in the AF_INET domain,
    // and invoke the specified callback with the result as an
    // ipv4_addr_t.
    virtual void lookupHost( const char* host,
                             const LookupHostCallback& resultCb,
                             const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    // Perform a forward name lookup on host in the AF_INET domain,
    // and invoke the specified callback with a sockaddr_ipv4_t
    // synthesized from the returned IPv4 address and user provided
    // port.
    virtual void lookupAddr( const char* host, int port,
                             const LookupAddrCallback& resultCb,
                             const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    // Perform a reverse lookup on the given IPv4 address, and invoke
    // the specified callback with the results. The 'FQ'
    // variant will return the fully qualified name. Note that we do
    // not return service info here, only name info.
    virtual void lookupName( const sockaddr_ipv4_t& addr,
                             const LookupNameCallback& resultCb,
                             const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    virtual void lookupNameFQ( const sockaddr_ipv4_t& addr,
                               const LookupNameCallback& resultCb,
                               const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    // These just extract the underlying char* and call the function of the same name.
    inline void getFQDN( const std::string& hostname,
                         const GetFQDNCallback& resultCb,
                         const ErrorCallback& errorCb = ErrorCallback() );

    inline void lookupHostWithFQDN( const std::string& hostname,
                                    const LookupHostWithFQDNCallback& resultCb,
                                    const ErrorCallback& errorCb = ErrorCallback() );

    inline void lookupHost( const std::string& host,
                            const LookupHostCallback& resultCb,
                            const ErrorCallback& errorCb = ErrorCallback() );

    inline void lookupAddr( const std::string& host, int port,
                            const LookupAddrCallback& resultCb,
                            const ErrorCallback& errorCb = ErrorCallback() );
};
BB_DECLARE_SHARED_PTR(IDNSResolver);


// Much like an IDNSResolver, but pending operations may be canceled
// by invalidating the returned subscription object.
class ICancelableDNSResolver
{
public:
    virtual ~ICancelableDNSResolver();

    typedef IDNSResolver::GetFQDNCallback             GetFQDNCallback;
    typedef IDNSResolver::LookupHostWithFQDNCallback  LookupHostWithFQDNCallback;
    typedef IDNSResolver::LookupHostCallback          LookupHostCallback;
    typedef IDNSResolver::LookupAddrCallback          LookupAddrCallback;
    typedef IDNSResolver::LookupNameCallback          LookupNameCallback;
    typedef IDNSResolver::ErrorCallback               ErrorCallback;
    typedef IDNSResolver::ResolutionError             ResolutionError;

    virtual void getFQDN( Subscription& outSub,
                          const GetFQDNCallback& resultCb,
                          const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    virtual void getFQDN( Subscription& outSub,
                          const char* hostname,
                          const GetFQDNCallback& resultCb,
                          const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    virtual void lookupHostWithFQDN( Subscription& outSub,
                          const char* hostname,
                          const LookupHostWithFQDNCallback& resultCb,
                          const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    virtual void lookupHost( Subscription& outSub,
                             const char* host,
                             const LookupHostCallback& resultCb,
                             const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    virtual void lookupAddr( Subscription& outSub,
                             const char* host, int port,
                             const LookupAddrCallback& resultCb,
                             const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    virtual void lookupName( Subscription& outSub,
                             const sockaddr_ipv4_t& addr,
                             const LookupNameCallback& resultCb,
                             const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    virtual void lookupNameFQ( Subscription& outSub,
                               const sockaddr_ipv4_t& addr,
                               const LookupNameCallback& resultCb,
                               const ErrorCallback& errorCb = ErrorCallback() ) = 0;

    inline void getFQDN( Subscription& outSub,
                         const std::string& hostname,
                         const GetFQDNCallback& resultCb,
                         const ErrorCallback& errorCb = ErrorCallback() );

    inline void lookupHostWithFQDN( Subscription& outSub,
                                    const std::string& hostname,
                                    const LookupHostWithFQDNCallback& resultCb,
                                    const ErrorCallback& errorCb = ErrorCallback() );

    inline void lookupHost( Subscription& outSub,
                            const std::string& host,
                            const LookupHostCallback& resultCb,
                            const ErrorCallback& errorCb = ErrorCallback() );

    inline void lookupAddr( Subscription& outSub,
                            const std::string& host, int port,
                            const LookupAddrCallback& resultCb,
                            const ErrorCallback& errorCb = ErrorCallback() );
};
BB_DECLARE_SHARED_PTR(ICancelableDNSResolver);


inline void IDNSResolver::getFQDN( const std::string& hostname,
                                   const GetFQDNCallback& resultCb,
                                   const ErrorCallback& errorCb )
{
    getFQDN( hostname.c_str(), resultCb, errorCb );
}

inline void IDNSResolver::lookupHostWithFQDN( const std::string& hostname,
                                              const LookupHostWithFQDNCallback& resultCb,
                                              const ErrorCallback& errorCb )
{
    lookupHostWithFQDN( hostname.c_str(), resultCb, errorCb );
}

inline void IDNSResolver::lookupHost( const std::string& host,
                                      const LookupHostCallback& resultCb,
                                      const ErrorCallback& errorCb )
{
    lookupHost( host.c_str(), resultCb, errorCb );
}

inline void IDNSResolver::lookupAddr( const std::string& host, int port,
                                      const LookupAddrCallback& resultCb,
                                      const ErrorCallback& errorCb )
{
    lookupAddr( host.c_str(), port, resultCb, errorCb );
}

inline void ICancelableDNSResolver::getFQDN( Subscription& outSub,
                                             const std::string& hostname,
                                             const GetFQDNCallback& resultCb,
                                             const ErrorCallback& errorCb )
{
    getFQDN( outSub, hostname.c_str(), resultCb, errorCb );
}

inline void ICancelableDNSResolver::lookupHostWithFQDN( Subscription& outSub,
                                                        const std::string& hostname,
                                                        const LookupHostWithFQDNCallback& resultCb,
                                                        const ErrorCallback& errorCb )
{
    lookupHostWithFQDN( outSub, hostname.c_str(), resultCb, errorCb );
}

inline void ICancelableDNSResolver::lookupHost( Subscription& outSub,
                                                const std::string& host,
                                                const LookupHostCallback& resultCb,
                                                const ErrorCallback& errorCb )
{
    lookupHost( outSub, host.c_str(), resultCb, errorCb );
}

inline void ICancelableDNSResolver::lookupAddr( Subscription& outSub,
                                                const std::string& host, int port,
                                                const LookupAddrCallback& resultCb,
                                                const ErrorCallback& errorCb )
{
    lookupAddr( outSub, host.c_str(), port, resultCb, errorCb );
}

} // namespace bb

#endif // BB_CORE_IDNSRESOLVER_H
