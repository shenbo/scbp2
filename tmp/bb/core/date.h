#ifndef BB_CORE_DATE_H
#define BB_CORE_DATE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>

namespace bb {

/** date_t represents a date and is generally based off of
    timeval_t's.  It differs from timeval_t in that it keeps track
    of information about the actual year, month, dayhour, minute,
    second, and microsecond rather than just a number of
    seconds. */
class date_t {
public:
    enum Weekday {
        Sunday = 0,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday
    };

    enum Month {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    };

    enum Locale { kUTC, kLocal };
public:
    /// Returns the date of today at midnight. You can specify an
    /// integer offset in days (negative offsets go backwards in
    /// time). So, today(1) is equivalent to tomorrow.
    static date_t today(int offset = 0);

    /// Returns the date of tomorrow at midnight.
    static date_t tomorrow() { return date_t::today(1); }

    static date_t nth_day_of_week( int week_num, int wday, int month, int year );

    /// Creates a date representing the start of unix time.
    date_t();
    /// Creates a date representing this timeval_t.
    explicit date_t(const timeval_t &t, Locale locale = kLocal);
    /// Creates a date representing this day
    explicit date_t(int ymd_date, Locale locale = kLocal);

    date_t &operator =(const timeval_t &t);

    /// Returns the year.
    int year() const { return _year; }
    /// Returns the month.
    int month() const { return _month; }
    /// Returns the day.
    int day() const { return _day; }
    /// Returns the hour.
    int hour() const { return _hour; }
    /// Returns the minute.
    int min() const { return _minute; }
    /// Returns the second.
    int sec() const { return _sec; }
    /// Returns the microsecond.
    int usec() const { return tv.usec(); }
    /// Returns the nanosecond.
    int nsec() const { return tv.nsec(); }
    /// Returns the day of the week
    int wday() const { return _wday; }

    /// Returns the date in YYYYMMDD format
    int ymd_date() const { return (_year*10000 + _month*100 + _day); }

    /// Returns the timeval_t that represents this date.
    timeval_t timeval() const { return tv; }

    timeval_t getMidnight() const
    {
        return timeval_t(tv.sec() - _hour*60*60 - _minute*60 - _sec);
    }

    void setMidnight()
    {
        tv = getMidnight();
        _hour = _minute = _sec = 0;
    }

    timeval_t sinceMidnight() const
    {
        return timeval_t::from_sec_nsec(_sec+60*_minute+3600*_hour, nsec());
    }

    date_t prevDay( ) const { return date_t( timeval_t::from_sec_nsec( tv.sec() - 24 * 60 * 60, tv.nsec() ), _locale ); }
    date_t prevDay( size_t n ) const;
    date_t nextDay( ) const { return date_t( timeval_t::from_sec_nsec( tv.sec() + 24 * 60 * 60, tv.nsec() ), _locale ); }


    /// Returns date_t in "YYYYMMDD HHMMSS" format. If usec is true,
    /// returns in "YYYYMMDD HHMMSS.UUUUUU" format.
    std::string toString(bool usec = false) const;

    /// Returns date_t as string in the format specified, like C strftime(3).
    /// Default format is "%Y-%m-%d" = YYYY-MM-DD
    /// See http://www.cplusplus.com/reference/clibrary/ctime/strftime/
    std::string strftime(std::string const & format = "%Y-%m-%d") const;

private:
    uint16_t _year;
    uint8_t _month, _day, _hour, _minute, _sec, _wday;
    timeval_t tv;
    Locale    _locale;
};

inline bool operator ==(const date_t &d1, const date_t &d2) { return d1.timeval() == d2.timeval(); }
inline bool operator < (const date_t &d1, const date_t &d2) { return d1.timeval() <  d2.timeval(); }
inline bool operator !=(const date_t &d1, const date_t &d2) { return !(d1 == d2); }
inline bool operator > (const date_t &d1, const date_t &d2) { return   d2 <  d1 ; }
inline bool operator <=(const date_t &d1, const date_t &d2) { return !(d1 >  d2); }
inline bool operator >=(const date_t &d1, const date_t &d2) { return !(d1 <  d2); }

/// Like timeval_t::make_time(), but converts its result to date_t
date_t make_date( const char* sz );

/// parseDateRange takes 2 strings representing start and end dates and
/// converts them to date_t or timeval_t (caller's choice). Throws bb:Error.
///
/// defaultEnd[HH,MM,SS] should be the time of day to use if only the date is
/// specified for endDate. e.g. if defaultEnd = 23, 59, 59, then
/// parseDateRange("20091111", "") and parseDateRange("20091111", "20091111"),
/// both return [11th@midnight, 11th@235959].
void parseDateRange(date_t *startDate, date_t *endDate,
        const std::string& startStr, const std::string& endStr,
        int defaultEndHH = 23, int defaultEndMM = 59, int defaultEndSS = 59);
void parseDateRange(timeval_t *startTv, timeval_t *endTv,
        const std::string& startStr, const std::string& endStr,
        int defaultEndHH = 23, int defaultEndMM = 59, int defaultEndSS = 59);

/// Stream operator, used to print values in human readable form.
std::ostream &operator <<(std::ostream &out, const date_t &d);

/// Stream operator to create a date from a stream; works like make_date().
std::istream &operator >>(std::istream &, date_t &);

inline std::size_t hash_value( const date_t& date )
{
    return hash_value( date.timeval() );
}

} // namespace bb

#endif // BB_CORE_DATE_H
