#ifndef BB_CORE_JSONCODEGEN_H
#define BB_CORE_JSONCODEGEN_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/LuaCodeGen.h>
#include <bb/core/TransportLayer.h>
#include <bb/core/mtype.h>
// helper functions for generating JSONcode from C++
// If you want to add codegen ability for your structure MyStruct, you should
// define the function:
//   void jsonprint(std::ostream &out, MyStruct const &s, JsonPrintSettings const &ps);
//
// Then, in client code, you can output a MyStruct using:
// MyStruct foo;
// cout << jsonMode(foo);

namespace bb
{

class acct_t;
class currency_t;
class expiry_t;
class instrument_t;
class Msg;
class mac_addr_t;
class ipv4_addr_t;
class sockaddr_ipv4_t;
class tid_t;
class Uuid;


typedef LuaPrintSettings JsonPrintSettings;

/// Wrapper class for outputting a given type as JSON code.
template<typename T>
struct JsonMode
{
    JsonMode(const T &a, const JsonPrintSettings &l=JsonPrintSettings())
        : m_a(a), m_printSettings(l) {}
    friend std::ostream &operator<<(std::ostream &out, JsonMode const& obj)
    {
        // defer to ADL-based jsonprint function
        jsonprint(out, obj.m_a, obj.m_printSettings);
        return out;
    }
protected:
    const T& m_a;
    JsonPrintSettings m_printSettings;
};

/// JSON codegen helper
template<typename T>
JsonMode<T> jsonMode(const T &a, const JsonPrintSettings &l=JsonPrintSettings());

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG

void jsonprint(std::ostream &out, unsigned short i, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, unsigned int i, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, uint64_t i, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, short i, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, int i, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, long i, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, float i, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, double i, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, const char * str, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, const unsigned char * str, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, std::string const &str, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bool b, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::mtype_t mtype, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::mktdest_t dst, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::EFeedType feed_type, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::EFeedOrig feed_orig, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::EFeedDest feed_dest, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::timeval_t const &tm, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::expiry_t exp, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::maturity_t mat, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::symbol_t sym, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::product_t prod, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::currency_t curr, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::side_t side, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::dir_t dir, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::right_t rt, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::instrument_t const &instr, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::source_t src, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::Uuid const &uuid, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::acct_t acct, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::tif_t tif, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::TransportLayer const &transport, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::tid_t const &tid, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::mac_addr_t const &addr, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::ipv4_addr_t const &addr, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::sockaddr_ipv4_t const &addr, JsonPrintSettings const &ps);
void jsonprint(std::ostream &out, bb::Msg const &msg, JsonPrintSettings const &ps);
template<typename T>
void jsonprint(std::ostream &out, std::vector<T>  const &vec, JsonPrintSettings const &ps);
template<typename T>
void jsonprint(std::ostream &out, std::set<T>  const &s, JsonPrintSettings const &ps);

#endif // !SWIG

template<typename T>
inline JsonMode<T> jsonMode(const T &a, const JsonPrintSettings &l)
{
    return JsonMode<T>(a, l);
}

namespace detail {
template<typename Vec> void json_write_vec( std::ostream& out,  Vec  const &vec , JsonPrintSettings const &ps)
{
    if(vec.empty())
    {
        out << "{}";
        return;
    }

    bb::JsonPrintSettings ps1 = ps.next();

    // TODO: What we maybe should do here, but don't, is to read from a map<typeid,string> which would be
    // populated by luaBindVector() to figure out the JSON "constructor" name for our Vec type.
    // As it stands, round tripping doesn't work "out of the box" because the input JSON might look like
    // "x = MyTypeVec{ Thing1, Thing2 }" but the code generated below will give "x = { Thing1, Thing2 }".

    out << "{" << ps1.optNewline();

    for(typename Vec::const_iterator i = vec.begin(); i != vec.end(); ++i)
    {
        const typename Vec::value_type& val = *i;
        out << ps1.indent() << jsonMode(val, ps1) << "," << ps1.optNewline();
    }

    out << ps.indent() << "}";
}

}

template<typename T>
inline void jsonprint(std::ostream &out, std::vector<T>  const &vec, JsonPrintSettings const &ps)
{
    detail::json_write_vec( out, vec );
}

template<typename T>
inline void jsonprint(std::ostream &out, std::set<T>  const &vec, JsonPrintSettings const &ps)
{
    detail::json_write_vec( out, vec );
}

} // namespace bb


#endif // BB_CORE_JSONCODEGEN_H
