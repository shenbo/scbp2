#ifndef BB_CORE_CALENDAR_H
#define BB_CORE_CALENDAR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstddef>
#include <iosfwd>

#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>

namespace bb {
class date_t;

///Calendar
/// A simple list of bb::dates, with no other detail
/**
 *
 *
 * Example Lua program
 *
    function print_dates(cal)
        io.stdout:write( tostring(cal) .. " has :\n")
        for i in cal.dates do
            io.stdout:write( "\t" .. tostring(i) .. "\n")
        end
    end
    function at_or_next(cal,dt)
       if  not cal:exists(dt) then
         return cal:next(dt)
       end
       return dt
    end
    function at_or_previous(cal,dt)
       if not cal:exists(dt)then
          return cal:previous(dt)
       end
       return dt
    end

    calf=TradingCalendarFactoryDB(CalendarType.USEquities,config_params.dbprofile)
    io.stdout:write("Factory ".. tostring(calf) .. "\n")
    cal_all=calf:create();--get all possible dates
    io.stdout:write( "cal_all ".. tostring(cal_all) .. "\n")
    -- get a restricted set (Note: this is optimized)
    -- the create call required dates that exist in CalendarType.USEquities
    -- so these are adjusted to meet that spec
    cal=calf:create(at_or_previous(cal_all,Date(20090628)),at_or_next(cal_all,Date(20090712)))
    print_dates(cal)
    io.stdout:write( tostring(cal) .. "\n")
    T=Date(config_params.start_tv)--our trading date
    io.stdout:write( "Distance of ".. tostring(T) .. " to the first date " .. cal:distance(T,cal.first) .. "\n")
    io.stdout:write( "Distance of ".. tostring(T) .. " to the last date " .. cal:distance(T,cal.last) .. "\n")
    io.stdout:write( "T+3\t" .. tostring(cal:add(T,3)) .. "\n")


Will print out (using 20090702 100000 for start_tv, and dbdev for the database
Factory USEquities(20080102,20091230)
cal_all USEquities(20080102,20091230)
USEquities(20090626,20090713) has :
    20090626 000000
    20090629 000000
    20090630 000000
    20090701 000000
    20090702 000000
    20090706 000000
    20090707 000000
    20090708 000000
    20090709 000000
    20090710 000000
    20090713 000000
USEquities(20090626,20090713)
Distance of 20090702 100000 to the first date -4
Distance of 20090702 100000 to the last date 6
T+3 20090708 000000

 *
 */
class Calendar
{
public:
    typedef bb::date_t const* const_iterator;

    ///begin and end of the range in this Calendar
    /// for C++ only
    const_iterator begin()const;
    const_iterator end()const;

    ///return the first date in this calendar
    bb::date_t first()const;

    ///return the last date in this calendar
    bb::date_t last()const;

    ///return the previous date
    ///if date is before or at first(), return the argument
    ///if date is after last(), return last()
    ///date does not have to exist in the calendar
    bb::date_t previous(bb::date_t const&)const;

    ///return the next date
    ///if date is before or at first(), return first()
    ///if date is after last(), returns the argument
    ///date does not have to exist in the calendar
    bb::date_t next(bb::date_t const&)const;

    ///does this date exists in the Calendar?
    bool exists(bb::date_t const&)const;

    ///distance between two dates
    ///if dates are not in the calendar throws exception
    std::ptrdiff_t distance(bb::date_t const&, bb::date_t const&)const;

    ///add a number of days to a date
    ///if date is not in the calendar throws exception
    bb::date_t add(bb::date_t const&,int days)const;

    ///tests if this implementation is a valid Calendar
    ///a valid Calendar requires that begin()!=end(), begin()<end(), and
    /// iterator i=begin(),j=i+1, e = end();
    ///for(;j!=e;++i,++j)assert(*i<*j);

    void validate()const;
    virtual ~Calendar(){}

protected:
    virtual const_iterator begin_i()const=0;
    virtual const_iterator end_i()const=0;
    virtual void print_i(std::ostream&)const=0;

    friend std::ostream& operator<<(std::ostream&,Calendar const&);
};
BB_DECLARE_SHARED_PTR( Calendar );

///A Calendar Factory
class ICalendarFactory
{
public:
    virtual ~ICalendarFactory(){}
    ///create a calendar of all dates
    virtual CalendarCPtr create()const=0;
    ///create a calendar of a specified date range
    ///dates must be subset of dates return by  create()
    virtual CalendarCPtr create(bb::date_t const& start_dt, bb::date_t const& end_dt)const=0;
    ///create a calendar of a specified date range
    ///dates must be subset of dates return by  create()
    virtual CalendarCPtr create(bb::date_t const& start_dt, int _days)const=0;
};

BB_DECLARE_SHARED_PTR( ICalendarFactory );

} //namespace bb

#endif // BB_CORE_CALENDAR_H
