#ifndef BB_CORE_PROGRAM_OPTIONS_H
#define BB_CORE_PROGRAM_OPTIONS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/program_options.hpp>

namespace bb {

/// like Boost's version but detects terminal width for prettier usage printing
class options_description
    : public boost::program_options::options_description
{
public:
    options_description( const std::string& caption = std::string() );

protected:
    /// get the terminal window size for prettier usage printing
    /// default to the boost::program_options default line length
    unsigned int get_terminal_width();
};

/// parses the options described in desc into programOptions
void parseOptionsSimple( boost::program_options::variables_map&, int argc, char** argv,
        const boost::program_options::options_description& );

void parseOptionsPositional( boost::program_options::variables_map&, int argc, char** argv,
        const boost::program_options::options_description&,
        const boost::program_options::positional_options_description& );

} // namespace bb


// operator<< is placed in the boost::program_options namespace so it can be
// found using argument-dependent lookup.  Boost provides an ostream operator<<
// implementation for vanilla options_description but not for the positional
// variant.

namespace boost {
namespace program_options {

std::ostream& operator <<( std::ostream&, const positional_options_description& );

} // namespace program_options
} // namespace boost

#endif // BB_CORE_PROGRAM_OPTIONS_H
