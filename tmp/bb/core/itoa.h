#ifndef BB_CORE_ITOA_H
#define BB_CORE_ITOA_H

#include <stdint.h>
#include <string.h>

#include <boost/utility/enable_if.hpp>
/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */
// SSE2 implementation according to http://0x80.pl/articles/sse-itoa.html
namespace bb
{

///
/// Convert a uint32_t to ascii directly into the memory pointed
/// to by buf.  returns the address of the byte past the last character
///
char * u32toa_sse2(uint32_t value, char* buffer );

///
/// Convert an int32_t to ascii directly into the memory pointed
/// to by buf.  returns the address of the byte past the last character
///
char * i32toa_sse2(int32_t value, char* buffer );

///
/// Convert a uint64_t to ascii directly into the memory pointed
/// to by buf.  returns the address of the byte past the last character
///
char * u64toa_sse2(uint64_t value, char* buffer );

///
/// Convert an int64_t to ascii directly into the memory pointed
/// to by buf.  returns the address of the byte past the last character
///
char * i64toa_sse2(int64_t value, char* buffer );



///
/// The following four functions represent the same functionality as above
/// but offer right padding with zeros ('0')
///
template<int Digits>
typename boost::enable_if_c< Digits <= 22, char * >::type
u32toa_sse2_padded( const uint32_t value, char * buffer)
{
    char *      end;
    char        buf[24]; // lenght + sign + \0

    // convert the decimal part into a temp buffer
    end = bb::u32toa_sse2( value, buf );
    const int len = end - buf;
    const int diff = Digits-len;

    // pad the buffer with 0's if required
    if( diff > 0 )
    {
        memset(buffer, '0', diff);
        buffer += diff;
    }
    // now copy the converted decimal
    memcpy(buffer, buf, len );
    return buffer + len;
}

template<int Digits>
char * i32toa_sse2_padded( const int32_t value, char * buffer)
{
    uint32_t u = static_cast<uint32_t>(value);
    char *   end;
    if (value < 0)
    {
        *buffer = '-';
        u = ~u + 1;
        // we already consumed one character for the '-' sign
        // so reduce the required padding by one
        end = u32toa_sse2_padded<Digits-1>(u, &buffer[1]);
    }
    else
    {
        end = u32toa_sse2_padded<Digits>(u, buffer);
    }
    return end;
}

template<int Digits>
typename boost::enable_if_c< Digits <= 22, char * >::type
u64toa_sse2_padded( const uint64_t value, char * buffer)
{
    char *      end;
    char        buf[24]; // length + sign + \0

    // convert the decimal part into a temp buffer
    end = bb::u64toa_sse2( value, buf );
    const int len  = end - buf;
    const int diff = Digits-len;

    // pad the buffer with 0's if required
    if( diff > 0 )
    {
        memset(buffer, '0', diff);
        buffer += diff;
    }
    // now copy the converted decimal
    memcpy(buffer, buf, len );
    return buffer + len;
}

template<int Digits>
char * i64toa_sse2_padded( const int64_t value, char * buffer)
{
    uint64_t u = static_cast<uint64_t>(value);
    char *   end;
    if (value < 0)
    {
        *buffer++ = '-';
        u = ~u + 1;
        end = u64toa_sse2_padded<Digits-1>(u, buffer);
    }
    else
    {
        end = u64toa_sse2_padded<Digits>(u, buffer);
    }
    return end;
}

} //end namespace bb

#endif // BB_CORE_ITOA_H
