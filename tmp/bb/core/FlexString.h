#ifndef BB_CORE_FLEXSTRING_H
#define BB_CORE_FLEXSTRING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstddef>
#include <ostream>
#include <string>
#include <stdexcept>
#include <typeinfo>

#include <boost/static_assert.hpp>

#include <bb/core/hash.h>
#include <bb/core/Error.h>
#include <bb/core/relocatablecheck.h>
#include <bb/core/streamextractor.h>

namespace bb {

class NullDiscriminator {
};

/*
SmallString -- a string that is small, from 0 to 125 chars max size
size   -> O(1)
copy   -> O(n)
compare-> O(1) if sizes are not equal, O(n) otherwise

Example
typedef FlexString<SmallString<31> > symbol_key_type;

sizeof(symbol_key_type)==32;//i.e. no additional size overhead above C style string




No heap usage.
May be read and written from file in binary format
symbol_key_type symkey("MSFT");
fout.write(symkey.data(),sizeof(symkey));
May also be placed in shared memory


To distinguish string from one another, do this
struct SymbolType{};
struct MMIDtype{};//note these are empty
typedef  FlexString<SmallString<31,SymbolType> > symbol_type;
typedef  FlexString<SmallString<31,MMIDtype> > mmid_type;

symbol_type sym("AAPL");
mmid_type madf("MADF");
//sym==madf;//nonsensical and will not compile
void foo(symbol_type const&,mmid_type const&);
foo(madf,sym);//will not compile


*/


template<std::size_t N, class Disc=NullDiscriminator>
struct SmallString {
    typedef char value_type;
    typedef value_type* pointer;
    typedef value_type const* const_pointer;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef std::char_traits<value_type> traits_type;
    typedef char& reference;
    typedef char const& const_reference;


    BOOST_STATIC_ASSERT(N<126);

    SmallString() {
        m_buf[N]=N;

    }
    void resize(size_type sz) {
        BB_THROW_EXASSERT_SSX(sz<=max_size(), //std::length_error,
                "requested size "<<sz<<" greater than max_size() "<<max_size()<< " with "<<typeid(*this).name());
        m_buf[N]=static_cast<char>(N-sz);
    }
    size_type size() const {
        return N-m_buf[N];
    }
    const_pointer begin() const {
        return m_buf;
    }
    const_pointer end() const {
        return m_buf+size();
    }
    pointer begin() {
        return m_buf;
    }
    pointer end() {
        return m_buf+size();
    }
    const_pointer c_str() const {
        m_buf[size()]=0;
        return m_buf;
    }
    const_pointer data() const {
        return m_buf;
    }
    static size_type max_size() {
        return N;
    }
    static size_type capacity() {
                return N;
    }
protected:
    pointer data() {
        return m_buf;
    }
private:
    mutable char m_buf[N+1];

};

/*
FixedCapacityString -- a string that has large buffer associated with it, buffer is any size
size   -> O(1)
copy   -> O(n)
compare-> O(1) if sizes are not equal, O(n) otherwise

Example
typedef FlexString<FixedCapacityString<250> > filename_type;

there are four bytes additional size overhead above C style string




No heap usage.
May be read and written from file in binary format
filename_type symlog("blah");
fout.write(symlog.data(),sizeof(symlog));
May also be placed in shared memory


To distiguish string from one another, do this
struct LogFileType{};
struct ErrFiletype{};//note these are empty
typedef  FlexString<FixedCapacityString<250,LogFileType> > log_type;
typedef  FlexString<FixedCapacityString<250,ErrFiletype> > err_type;

log_type sym("AAPL");
err_type ubss("UBSS");
//sym==ubss;//nonsensical and will not compile
void foo(log_type const&,err_type const&);
foo(ubss,sym);//will not compile


*/






template<std::size_t N, class Disc=NullDiscriminator>
struct FixedCapacityString {
    typedef char value_type;
    typedef value_type* pointer;
    typedef value_type const* const_pointer;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef std::char_traits<value_type> traits_type;
    typedef char& reference;
    typedef char const& const_reference;

    FixedCapacityString() :
        m_len() {
    }

    void resize(size_type sz) {
        BB_THROW_EXASSERT_SSX(sz<=max_size(),// std::length_error,
                "requested size "<<sz<<" greater than max_size() "<<max_size()<< " with "<<typeid(*this).name());
        m_len=sz;
    }



    size_type size() const {
        return m_len;
    }
    const_pointer begin() const {
        return m_buf;
    }
    const_pointer end() const {
        return m_buf+size();
    }
    pointer begin() {
        return m_buf;
    }
    pointer end() {
        return m_buf+size();
    }
    const_pointer c_str() const {
        m_buf[size()]=0;
        return m_buf;
    }
    const_pointer data() const {
        return m_buf;
    }
    static size_type max_size() {
        return N;
    }
    static size_type capacity() {
            return N;
    }
protected:
    pointer data() {
        return m_buf;
    }
private:
    std::size_t m_len;
    mutable char m_buf[N+1];
};


template<class POLICY_TYPE> class FlexString : protected POLICY_TYPE {
public:
    class truncate{};

    typedef POLICY_TYPE policy_type; ///< the policy
    typedef FlexString < POLICY_TYPE > my_type;
    typedef typename policy_type::value_type value_type;
    typedef typename policy_type::size_type size_type;
    typedef typename policy_type::pointer iterator;
    typedef typename policy_type::const_pointer const_iterator;
    typedef typename policy_type::traits_type traits_type;
    typedef typename policy_type::pointer pointer;
    typedef typename policy_type::const_pointer const_pointer;
    typedef typename policy_type::reference reference;
    typedef typename policy_type::const_reference const_reference;

public:

    /// constructor,

    explicit FlexString(policy_type const&p=policy_type()) :
        policy_type(p) {
    }

    FlexString(const_pointer beg, const_pointer end,
            policy_type const&p=policy_type()) :
        policy_type(p) {
        assign(beg, end);
    }
    template<class InputIterator> FlexString(InputIterator beg,
            InputIterator end, policy_type const&p=policy_type()) :
        policy_type(p) {
        assign(beg, end);
    }
    template<class traits, class Alloc> FlexString(
            std::basic_string< value_type,traits,Alloc> const&r,
            policy_type const&p=policy_type()) :
        policy_type(p) {
        assign(r);
    }
    template<class traits, class Alloc> FlexString(
            std::basic_string< value_type,traits,Alloc> const&r,truncate const&t,
            policy_type const&p=policy_type()) :
        policy_type(p) {
        assign(r,t);
    }

    FlexString(const_pointer seq, policy_type const&p=policy_type()) :
        policy_type(p) {
        assign(seq);
    }
    FlexString(const_pointer seq, truncate const&t,policy_type const&p=policy_type()) :
            policy_type(p) {
            assign(seq,t);
        }
    bool operator==(FlexString const&_r) const {
        size_type const mysize=size();
        if (mysize==_r.size())
            return 0==traits_type::compare(data(), _r.data(), mysize);
        return false;
    }
    bool operator!=(FlexString const& _r) const {
        return !(*this==_r);
    }

    bool operator==(const_pointer _r) const {
        return strcmp(_r,c_str())==0;
    }
    friend bool operator==(const_pointer _l,my_type const& _r){
        return _r==_l;
    }
    friend bool operator!=(const_pointer _l,my_type const& _r){
        return _r!=_l;
    }
    bool operator!=(const_pointer _r) const {
            return !(*this==_r);
    }
    bool operator<(const_pointer _r) const {
        size_type const rsize=traits_type::length(_r);
        size_type const mysize=size();
        if (mysize<rsize)
            return true;
        if (mysize>rsize)
            return false;
        return 0>traits_type::compare(data(), _r, mysize);
    }
    friend bool operator<(const_pointer _l,my_type const& _r){
        size_type const lsize=traits_type::length(_l);
        size_type const mysize=_r.size();
        if (lsize <mysize)
            return true;
        if (lsize>mysize)
            return false;
        return 0>traits_type::compare( _l,_r.data(),  mysize);
    }

    bool operator<(FlexString const&_r) const {
        size_type const mysize=size();
        size_type const rsize=_r.size();
        if (mysize<rsize)
            return true;
        if (mysize>rsize)
            return false;
        return 0>traits_type::compare(data(), _r.data(), mysize);
    }
    bool empty() const {
        return size()==0;
    }
    const_pointer data() const {
        return this->policy_type::data();
    }
    void resize(size_type sz, value_type Elem) {
        size_type const mysize=size();
        this->policy_type::resize(sz);
        if (sz>mysize)
            traits_type::assign(this->policy_type::data()+mysize, sz-mysize,
                    Elem);

    }

    void resize(size_type sz) {
        this->policy_type::resize(sz);
    }
    size_type size() const {
        return this->policy_type::size();
    }
    size_type max_size() const {
        return this->policy_type::max_size();
    }
    size_type capacity() const {
        return this->policy_type::capacity();
    }

    const_pointer begin() const {
        return this->policy_type::begin();
    }
    const_pointer end() const {
        return this->policy_type::end();
    }
    pointer begin() {
        return this->policy_type::begin();
    }
    pointer end() {
        return this->policy_type::end();
    }
    const_pointer c_str() const {
        return this->policy_type::c_str();
    }
    void assign(const_pointer seq) {
        size_type const mysize=traits_type::length(seq);
        this->policy_type::resize(mysize);
        traits_type::copy(this->policy_type::data(), seq, mysize);
    }
    void assign(const_pointer seq, truncate const&)
    {
        size_type cap=capacity();
        this->policy_type::resize(cap);
        char * dest=this->policy_type::data();
        char * end=dest+cap;
        while(dest!=end && *seq)*dest++=*seq++;
        this->policy_type::resize(cap-(end-dest));

    }
    void assign(const_pointer beg, const_pointer end) {
        size_type const mysize=end-beg;
        this->policy_type::resize(mysize);
        traits_type::copy(this->policy_type::data(), beg, mysize);
    }
    template<class InputIterator> void assign(InputIterator beg,
            InputIterator end) {
        size_type const mysize=std::distance(beg, end);
        this->policy_type::resize(mysize);
        using std::copy;
        copy(beg, end, this->policy_type::begin());
    }
    template<class traits, class Alloc> void assign(
                std::basic_string< value_type,traits,Alloc> const&r,truncate const&) {
        using std::min;
        size_type mysize=min(r.size(),capacity());
        this->policy_type::resize(mysize);
        traits_type::copy(this->policy_type::data(), r.data(), mysize);
    }
    template<class traits, class Alloc> void assign(
            std::basic_string< value_type,traits,Alloc> const&r) {
        size_type const mysize=r.size();
        this->policy_type::resize(mysize);
        traits_type::copy(this->policy_type::data(), r.data(), mysize);
    }
    void assign(FlexString const& r) {
        policy_type::operator=(r);
    }
    void clear() {
        resize(0);
    }
    FlexString& operator=(FlexString const& r) {
        assign(r);
        return *this;
    }
    template<class traits, class Alloc> FlexString& operator=(
            std::basic_string< value_type,traits,Alloc> const&r) {
        assign(r);
        return *this;
    }
    FlexString& operator=(const_pointer r) {
        assign(r);
        return *this;
    }

    void append(std::size_t num, value_type val) {
        resize(size()+num, val);
    }
    FlexString& operator+=(value_type r) {
        append(1, r);
        return *this;
    }
    reference operator[](size_type i){return *(this->policy_type::data()+i);}
    const_reference operator[](size_type i)const{return *(this->policy_type::data()+i);}

    reference at(size_type i){
        BB_THROW_EXASSERT_SSX(i<size()//, std::out_of_range
                ,"requested index "<<i<<" greater than or equal to size() "<<size()<< " with "<<typeid(*this).name());

        return *(this->policy_type::data()+i);
    }
    const_reference at(size_type i)const{
        return const_cast<FlexString*>(this)->at(i);
    }

    void extract(std::basic_istream< value_type ,traits_type > & _m_stream,
                std::ios_base::iostate&_State) {
        typedef std::ctype<value_type> _Ctype;
        _Ctype const& _Ctype_fac = std::use_facet<_Ctype >(_m_stream.getloc());
        bool _Changed = false;
        this->resize(0);

        size_type _Size = 0 < _m_stream.width()
        && static_cast<size_type>(_m_stream.width()) < this->max_size()
        ? static_cast<size_type>(_m_stream.width()) : this->max_size();
        typename traits_type::int_type _Meta = _m_stream.rdbuf()->sgetc();

        for (; 0 < _Size; --_Size, _Meta = _m_stream.rdbuf()->snextc()) {
            if(traits_type::eq_int_type(traits_type::eof(), _Meta))
            { // end of file, quit
                    _State |= std::ios_base::eofbit;
                    break;
            }
            else if (_Ctype_fac.is(_Ctype::space,
                            traits_type::to_char_type(_Meta))) {
                break; // whitespace, quit

            }
            else
            { // add character to string
                this->append(1, traits_type::to_char_type(_Meta));
                _Changed = true;
            }
        }
        _m_stream.width(0);
        if (!_Changed){
            _State |= std::ios_base::failbit;
        }
    }
};

/// the stream insertion operator
template<class charT, class Traits, typename POLICY_TYPE> std::basic_ostream< charT ,Traits > &operator<<(
        std::basic_ostream< charT ,Traits >&_m_stream,
        FlexString<POLICY_TYPE> const &val) {
    return _m_stream.write(val.data(), std::streamsize(val.size()));
}
/// the stream extraction operator
template<class charT, class Traits, typename POLICY_TYPE> std::basic_istream< charT ,Traits > &operator>>(
        std::basic_istream< charT ,Traits >&_m_stream,
        FlexString<POLICY_TYPE> &val) {

    return stream_extractor(_m_stream,val);

    }



template<std::size_t N, class Disc>
struct declare_relocatable<
FixedCapacityString<N,Disc> > {
    enum {value = 1
        && is_relocatable<char >::value
        && is_relocatable<std::size_t >::value
    };

};
template<std::size_t N, class Disc>
struct declare_relocatable<
SmallString<N,Disc> > {
    enum {value = 1
        && is_relocatable<char >::value
    };

};
template<class POLICY_TYPE>
struct declare_relocatable<
FlexString<POLICY_TYPE> > {

    enum {value = 1
        && is_relocatable<typename FlexString< POLICY_TYPE>::policy_type >::value
    };

};

} // namespace bb
BB_HASH_NAMESPACE_BEGIN {
template<class Policy >
struct hash<bb::FlexString<Policy> >
{

    size_t operator()(  bb::FlexString<Policy> const& val ) const
    {
        return cstr_hash()(val.c_str());
    }
};

} BB_HASH_NAMESPACE_END
#endif // BB_CORE_FLEXSTRING_H
