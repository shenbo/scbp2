#ifndef BB_CORE_DESTRUCTORGUARD_H
#define BB_CORE_DESTRUCTORGUARD_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/bbassert.h>
#include <bb/core/compat.h>
#include <bb/core/Error.h>

namespace bb
{

// The problem: your class is making callbacks, but one of those
// callbacks might destroy the class. Boom, segfault.
//
// The solution: your class can check to make sure it hasn't
// been destroyed after each callback. Like so:
//
// class Foo
//    DestructorMarker m_dtor;
//    void doCallbacks()
//    {
//       DestructorGuard guard(&m_dtor);
//       for(...)
//           do something which might destroy this
//           if(guard.destroyed())
//              return;
//    }
//
// Implementation is tricky: we stack-allocate a flag here and leave ourselves
// a pointer to it in a member variable so that the destructor can null out
// the flag and we can safely examine it in the case where the
// destructor was called by our own message callback.

class DestructorMarker;

/// Allocated on the stack in a function which may
/// destroy this->. Can track when this-> gets destroyed.
class DestructorGuard
{
public:
    DestructorGuard(DestructorMarker *marker);
    ~DestructorGuard();

    bool destroyed() const { return m_member == NULL; }

protected:
    friend class DestructorMarker;
    // points to a member of a class, on the heap;
    // while we're active, that pointer will point
    // back to us.
    DestructorGuard **m_member;
};

/// Allocated as a member inside a class; when the class
/// is destroyed, informs the DestructorGuard.
class DestructorMarker
{
public:
    DestructorMarker() : m_guard(NULL) {}
    ~DestructorMarker() { stop(); }

    bool has_guard() const { return m_guard; }

protected:
    // notifies the guard that we're going away
    void stop()
    {
        if(m_guard)
            m_guard->m_member = NULL;
    }

    friend class DestructorGuard;
    // points to the stack-allocated DestructorGuard, or null if
    // there's not currently one
    DestructorGuard *m_guard;
};

inline DestructorGuard::DestructorGuard(DestructorMarker *marker)
{
    BB_ASSERT(marker);
    BB_THROW_EXASSERT(!marker->m_guard,
            // someone has already created a DestructorGuard on this object
            SRC_LOCATION_SEP "DestructorGuard: single object with multiple guards");
    marker->m_guard = this;
    m_member = &marker->m_guard;
}

inline DestructorGuard::~DestructorGuard()
{
    if(m_member)
        *m_member = NULL;
}

}

#endif // BB_CORE_DESTRUCTORGUARD_H
