#ifndef BB_CORE_TIMEPROVIDER_H
#define BB_CORE_TIMEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>

namespace bb {

/// Interface for code that just want to be able to ask what time it is
class ITimeProvider
{
public:
    virtual ~ITimeProvider() {}

    /// Returns the "current" time as a timeval.
    /// The notion of "current" is context sensitive.  For example, ClockMonitor's
    /// current time is the high timestamp of received messages, not the computer's time.
    virtual timeval_t getTime() const = 0;
};
BB_DECLARE_SHARED_PTR( ITimeProvider );


/// An ITimeProvider implementaion that always returns the actual time.
class RealTimeProvider
    : public ITimeProvider
{
public:
    virtual ~RealTimeProvider() {}

    /// Returns the current time, based on the computer's real-time clock.
    virtual timeval_t getTime() const { return timeval_t::now; }
};
BB_DECLARE_SHARED_PTR( RealTimeProvider );


/// An ITimeProvider that always returns the current time value of the
/// pointed-to timeval_t object.
class TestTimeProvider : public ITimeProvider
{
public:
    inline TestTimeProvider(timeval_t* time)
        : m_time(time) {}

    virtual ~TestTimeProvider() {};

    virtual timeval_t getTime() const { return *m_time; };

private:
    timeval_t* const m_time;
};
BB_DECLARE_SHARED_PTR( TestTimeProvider );

} // namespace bb

#endif // BB_CORE_TIMEPROVIDER_H
