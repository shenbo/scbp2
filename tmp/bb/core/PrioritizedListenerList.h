#ifndef BB_CORE_PRIORITIZEDLISTENERLIST_H
#define BB_CORE_PRIORITIZEDLISTENERLIST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <memory>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/function.hpp>
#include <bb/core/Subscription.h>
#include <bb/core/shared_vector.h>
#include <bb/core/Priority.h>

namespace bb {

/** Maintains a list of listeners of type T, T frequently can be a pointer type.
 *
 * @code
 * class ProviderListener
 * {
 * public:
 *     virtual ~ProviderListener() {}
 *     virtual void onUpdate( Data* d ) = 0;
 * };
 *
 * class Provider : public PrioritizedListenerList<ProviderListener*>
 * {
 * public:
 *     void update( Data* d )
 *     {
 *         notify( boost::bind( &ProviderListener::onUpdate, _1, d ) );
 *     }
 * };
 *
 * class MyListener : public ProviderListener
 * {
 *     virtual void onUpdate( Data* d ) { cout << "Hello World!" << endl; }
 * };
 * int main()
 * {
 *     Provider p;
 *     MyListener listener;
 *     Priority pty( 500 );
 *     p.add( &listener, pty );
 *
 *     Data d;
 *     p.update( d );  // prints out "Hello World!"
 * }
 *
 * @endcode
 */

template<typename T> class PrioritizedListenerList
{
public:
    typedef bb::Subscription Subscription;
    typedef std::pair<T, Priority> Entry;

protected:
    typedef std::pair<T, Priority> BaseValueType;
    typedef shared_vector<BaseValueType> BaseList;

    class ValueFinder
    {
    public:
        ValueFinder(const T& v) : m_v(v) {}
        bool operator()( const BaseValueType & a ) { return a.first == m_v; }
        const T& m_v;
    };

    class PriorityCmp
    {
    public:
        bool operator()( const BaseValueType & a, const Priority& b ) { return a.second >= b; }
        bool operator()( const Priority &a, const BaseValueType& b ) { return a >= b.second; }
        bool operator()( const BaseValueType &a, const BaseValueType& b ) { return a.second >= b.second; }
    };

public:
    class Enumerator
    {
    public:
        Enumerator( const Enumerator& c )
            : m_pit( c.m_pit->clone() ) {}
        bool hasNext() const { return m_pit->hasNext(); }
        const PrioritizedListenerList::Entry& next() { return m_pit->next(); }

    private:
        friend class PrioritizedListenerList;
        Enumerator( BaseList& base_list )
            : m_pit( new typename BaseList::safe_jterator(base_list) ) {}
        boost::scoped_ptr<typename BaseList::safe_jterator> m_pit;
    };

public:
    /// adds the listener to the end of the list if it doesn't exist
    /// returns true if the listener is added, else returns false because it already exists
    /// in the list.
    bool add( T v, const Priority& p )
    {
        if( std::find_if( m_baseList.unsafe_begin(), m_baseList.unsafe_end(), ValueFinder(v) ) == m_baseList.unsafe_end() )
        {
            typename BaseList::unsafe_iterator i =
                std::upper_bound( m_baseList.unsafe_begin(), m_baseList.unsafe_end(), p, PriorityCmp() );
            m_baseList.explicit_insert(i, std::make_pair(v, p));
            return true;
        }
        return false;
    }

    /// removes the listener from the list if it exists
    /// you can call this while the list is being iterated, but only for the _current_ listener!
    /// take a look at core/shared_vector.h and core/ListenNotify.h for a different approach.
    void remove( const T& v )
    {
        typename BaseList::unsafe_iterator i = std::find_if( m_baseList.unsafe_begin(), m_baseList.unsafe_end(), ValueFinder(v) );
        if( i != m_baseList.unsafe_end() )
            m_baseList.explicit_erase(i);
    }

    /// calls function 'f' on all listeners.
    void notify( const boost::function1<void,T> &f ) const
    {
        typename BaseList::safe_jterator iter(m_baseList);
        while(iter.hasNext())
            f(iter.next().first);
    }

    size_t size() const { return m_baseList.size(); }
    bool empty() const { return m_baseList.empty(); }

    void subscribe( Subscription& outSub, T v, const Priority& p )
    {
        typename BaseList::unsafe_iterator i =
            std::upper_bound( m_baseList.unsafe_begin(), m_baseList.unsafe_end(), p, PriorityCmp() );
        m_baseList.insert(outSub, i, std::make_pair(v, p));
    }

    template<typename C>
    void addSubscription( C* list, T v, const Priority& p )
    {
        Subscription sub;
        subscribe(sub, v, p);
        list->push_back(sub);
    }

    template<typename C>
    void addSubscription( std::insert_iterator<C> inserter, T v, const Priority& p )
    {
        Subscription sub;
        subscribe(sub, v, p);
        *inserter = sub;
    }

    Enumerator enumerate()
    { return Enumerator(m_baseList); }

protected:
    mutable BaseList m_baseList;
};

} // namespace bb

#endif // BB_CORE_PRIORITIZEDLISTENERLIST_H
