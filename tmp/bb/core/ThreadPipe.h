#ifndef BB_CORE_THREADPIPE_H
#define BB_CORE_THREADPIPE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>
#include <bb/core/bbint.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/FD.h>

namespace bb
{
    BB_FWD_DECLARE_SHARED_PTR(ThreadPipeReader);
    BB_FWD_DECLARE_SHARED_PTR(ThreadPipeWriter);

    /// This is a simple wrapper around the pipe() syscall. It's intended
    /// for doing inter-thread communication; one thread can pass a byte to the
    /// ThreadPipeWriter, and a second thread will get it at the ThreadPipeReader.
    ///
    /// ThreadPipeWriter&ThreadPipeReader have shared_ptr semantics; you can
    /// copy them as much as you want, when the last copy goes out of scope,
    /// that half of the pipe will be closed.
    ///
    /// See pipe(7) for the restictions on atomic pipe writes.
    /// The pipe will be placed in non-blocking mode.
    void makeThreadPipe(ThreadPipeReader &reader, ThreadPipeWriter &writer);
}


/// The writeable end of an inter-thread pipe.
class bb::ThreadPipeWriter
{
public:
    ThreadPipeWriter() {}
    ThreadPipeWriter(const ThreadPipeWriter &x) : m_pipeWriteFD(x.m_pipeWriteFD) {}
    ThreadPipeWriter &operator=(const ThreadPipeWriter &x)
        { m_pipeWriteFD = x.m_pipeWriteFD; return *this; }

    /// Sends len bytes over the pipe in an atomic interaction. len must be <= PIPE_BUF
    /// Returns -1 if the write failed (errno == EAGAIN means the pipe was full).
    /// Returns len if the write succeeded.
    ssize_t writeAtomic(const void *data, size_t len);

    /// sends a single byte over the pipe. If the pipe is full, the data will be
    /// thrown out and no error will be signaled. Throws on other errors.
    void sendWakeup(uint8_t cmd);

    int getFD() const { return m_pipeWriteFD->getFD(); }
    NBFDPtr getFDPtr() const { return m_pipeWriteFD; }

protected:
    friend void bb::makeThreadPipe(ThreadPipeReader &reader, ThreadPipeWriter &writer);
    ThreadPipeWriter(const NBFDPtr &fd) : m_pipeWriteFD(fd) {}

    NBFDPtr m_pipeWriteFD;
};


class bb::ThreadPipeReader
{
public:
    ThreadPipeReader() {}
    ThreadPipeReader(const ThreadPipeReader &x) : m_pipeReadFD(x.m_pipeReadFD){}
    ThreadPipeReader &operator=(const ThreadPipeReader &x) { m_pipeReadFD = x.m_pipeReadFD; return *this; }

    /// Consumes len bytes from a pipe.
    /// Returns -1 if there's no data available.
    /// Returns 0 on EOF.
    /// Returns len if the data has been consumed.
    /// Reads of less than len bytes are considered to be errors
    /// (since this is intended to be used in concert with writeAtomic).
    ssize_t read(void *data, ssize_t len);

    /// consumes a single byte from the pipe. Returns none if there's nothing waiting.
    boost::optional<uint8_t> consumeWakeup();

    int getFD() const { return m_pipeReadFD->getFD(); }
    NBFDPtr getFDPtr() const { return m_pipeReadFD; }
    void clear();//consume all bytes in pipe - for use with non-blocking pipes only

protected:
    friend void bb::makeThreadPipe(ThreadPipeReader &reader, ThreadPipeWriter &writer);
    ThreadPipeReader(const NBFDPtr &ref) : m_pipeReadFD(ref) {}

    NBFDPtr m_pipeReadFD;
};


#endif // BB_CORE_THREADPIPE_H
