#ifndef BB_CORE_TCPKEEPALIVEOPTIONS_H
#define BB_CORE_TCPKEEPALIVEOPTIONS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {

/// This should live in the IO library (ideally within TCPSocket) but
/// it can't, since we want to stuff this into the core_config's
/// tdconfig options, and we don't have IO available there. Oh well...
///
/// The semantics here are Linux specific. For platforms which don't
/// support these exact options, the implementation of
/// TCPSocket::setKeepAlive will make a best effort mapping to the
/// native platform's options, or log that it is ignoring the request
/// to configure the keepalive options.
///
/// When applied via TCPSocket::setKeepAlive, zero values mean to
/// leave the current socket setting unchanged.
struct TCPKeepAliveOptions
{
    inline TCPKeepAliveOptions()
        : time(0), interval(0), probes(0) {}

    /// On an idle connection, a TCP keepalive packet will be sent
    /// every 'time' seconds.
    int time;

    /// While probing for failure (e.g. after a regularly 'timed'
    /// keepalive fails), a TCP keepalive packet will be sent every
    /// 'interval' seconds. Typically this is much less than 'time'.
    int interval;

    /// While probing for failure, this many probes will be sent
    /// before declaring the connection dead.
    int probes;
};

} // namespace bb

#endif // BB_CORE_TCPKEEPALIVEOPTIONS_H
