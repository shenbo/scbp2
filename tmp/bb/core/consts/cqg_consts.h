#ifndef BB_CORE_CONSTS_CQG_CONSTS_H
#define BB_CORE_CONSTS_CQG_CONSTS_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */


#include <cstring>
#include <iosfwd>
#include <limits>
#include <boost/cstdint.hpp>
#include <boost/static_assert.hpp>

namespace cqg {

static char const  CalendarSpread = 'S';

enum depth_t {
    DEPTH_UNLIMITED   = 0,
    DEPTH_BBO         = 1,
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(depth_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const depth_t& depth );
#else
;
#endif // !defined(SWIG)

enum action_t{
    BA_NEW = 0,
    BA_MODIFY = 1,
    BA_DELETE = 2,
    BA_OVERLAY = 5,
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(action_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const action_t& action );
#else
;
#endif // !defined(SWIG)

enum stat_t{
    STAT_VOLUME        = 0,
    STAT_SETTL         = 1,
    STAT_OPEN_PRICE    = 2,
    STAT_OPEN_INTEREST = 3,
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(stat_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const stat_t& action );
#else
;
#endif // !defined(SWIG)



} // namespace cqg

#endif // BB_CORE_CONSTS_CQG_CONSTS_H
