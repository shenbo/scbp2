#ifndef BB_CORE_CONSTS_CME_CONSTS_H
#define BB_CORE_CONSTS_CME_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstring>
#include <iosfwd>
#include <limits>
#include <boost/cstdint.hpp>
#include <boost/static_assert.hpp>

#if !defined(SWIG)
#define CME_MAKE_WORD( h, l ) ((boost::uint16_t)(((boost::uint8_t)h<<8) | (boost::uint8_t)l))
#endif // !defined(SWIG)


namespace cme {

/*****************************************************************************/
//
// CME MDP RLC Message Types
//
/*****************************************************************************/

/// CME RLC Message Type
#if !defined(SWIG)
typedef enum
{
    MH_INVALID                  = CME_MAKE_WORD('\0', '\0'),
    // Administrative messages
    MH_OPENING_SUMMARY          = CME_MAKE_WORD('M', '9'),
    MH_INSTRUMENT_STATUS        = CME_MAKE_WORD('M', 'G'),
    MH_TRADING_DAY_TIMETABLE    = CME_MAKE_WORD('M', 'H'),
    MH_INTERMEDIATE_THRESHOLD   = CME_MAKE_WORD('M', 'I'),
    MH_GROUP_STATE_CHANGE       = CME_MAKE_WORD('M', 'J'),
    MH_START_TRADE_ENGINE       = CME_MAKE_WORD('M', 'K'),
    MH_START_INSTR_REFCAST      = CME_MAKE_WORD('M', 'L'),
    MH_END_INSTR_REFCAST        = CME_MAKE_WORD('M', 'M'),
    MH_INSTR_CREATION           = CME_MAKE_WORD('M', 'O'),
    MH_HEARTBEAT                = CME_MAKE_WORD('0', '9'),
    MH_SPI_MESSAGE              = CME_MAKE_WORD('2', '3'),
    // Book messages
    MH_REQUEST_FOR_QUOTE        = CME_MAKE_WORD('M', '4'),
    MH_FIVE_BEST_LIMITS         = CME_MAKE_WORD('M', 'A'),
    MH_DELETE_N_LINES           = CME_MAKE_WORD('M', 'C'),
    MH_MARKET_SHEET             = CME_MAKE_WORD('M', 'F'),
    MH_BEST_INDICATIVE_PRICE    = CME_MAKE_WORD('M', 'X'),
    MH_MOD_FIRST_FIVE_IMPLIED_ORDERS = CME_MAKE_WORD('M', 'Y'),
    // Pricing messages
    MH_LAST_BEST_PRICE          = CME_MAKE_WORD('M', '0'),
    MH_OPENING_TRADE            = CME_MAKE_WORD('M', '5'),
    MH_TRADE                    = CME_MAKE_WORD('M', '6'),
    MH_PRICE                    = CME_MAKE_WORD('M', '7'),
    MH_THEORETICAL_OPEN_PRICE   = CME_MAKE_WORD('M', '8'),
    MH_PRICE_OVERRIDE           = CME_MAKE_WORD('M', 'E')
} message_type_t;
#endif // !defined(SWIG)


/// Trading Status (FIX/FAST)
enum trading_status_t {
    TSF_TRADING_HALT            =  2,
    TSF_PRICE_INDICATION        =  5,
    TSF_SESSION_BEGIN           = 17,
    TSF_SESSION_END             = 18,
    TSF_UNKNOWN                 = 20,
    TSF_PRE_OPEN                = 21,
    TSF_PRE_CROSS               = 24,
    TSF_CROSS                   = 25,
    TSF_NO_CANCEL               = 26,
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(trading_status_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const trading_status_t& trading_status );
#else
;
#endif // !defined(SWIG)


/// Trading Status Flag(before Fall 2008)
typedef enum {
    TSF_SIMPLE_RESERVED         = 'P',      /// Simple Reserved
    TSF_RESERVED_UPWARDS        = 'H',      /// Reserved Upwards (Stop spike)
    TSF_RESERVED_DOWNWARD       = 'B',      /// Reserved Downward
    TSF_SUSPENDED               = 'S',      /// Suspended (Stop spike)
    TSF_RESUME                  = 'R',      /// Resume (Stop Spike
    TSF_UNCHANGED               = ' '       /// Unchanged or Open (Stop spike)
} trading_status_flag_t;


/// Instrument Reservation Order Flag
typedef enum {
    IR_AUTOMATIC                = 'A',      /// Automatic
    IR_MANUAL                   = 'M',      /// Manual
    IR_BLANK                    = ' '       /// May occur when instrument is resumed or Trading Status is Unchanged
} instr_reservation_t;


/// type of action on an instrument
typedef enum {
    IA_PROGRAMMING_DEFERRED_OPENING = 'P',
    IA_RESERVATION_BY_SURVILLANCE   = 'M',
    IA_TRADING                      = 'C',
    IA_CHANGE_TO_NORMAL_STATE       = 'O',
    IA_CANCEL_DEFFERED_OPENING      = 'D',
    IA_INSTR_AUTHORIZATION          = 'A',
    IA_INSTR_FORBIDDING             = 'I',
    IA_CANCEL_ORDERS                = 'E',
    IA_STATE_OF_INITIALIZATION      = 'N',
    IA_FAST_MARKET                  = 'F',
    IA_NORMAL_MARKET                = 'S',
    IA_FREEZE_INSTR                 = 'G'
} instr_action_t;


/// Indicates if this is the first opening for the instrument for the day.
#if !defined(SWIG)
typedef enum {
    TF_FIRST_OPENING    = CME_MAKE_WORD('0','4'),
    TF_LATER_OPENING    = CME_MAKE_WORD('0','7')
} trend_flag_t;
#endif // !defined(SWIG)


/// Instrument State
/// A = Authorized, I = Forbidden
/// R = Reserved, S = Suspended, G = Frozen, ' ' = Normal
#if !defined(SWIG)
typedef enum {
    IS_AR               = CME_MAKE_WORD('A', 'R'),
    IS_AS               = CME_MAKE_WORD('A', 'S'),
    IS_AG               = CME_MAKE_WORD('A', 'G'),
    IS_A_               = CME_MAKE_WORD('A', ' '),
    IS_IR               = CME_MAKE_WORD('I', 'R'),
    IS_IS               = CME_MAKE_WORD('I', 'S'),
    IS_IG               = CME_MAKE_WORD('I', 'G'),
    IS_I_               = CME_MAKE_WORD('I', ' ')
} instr_state_t;
#endif // !defined(SWIG)


/// Group of Instrument Status
typedef enum {
    GIS_START_CONSULTATION        = 'C', // Start of Consultation
    GIS_PREOPENING                = 'P', // Pre-opening
    GIS_OPENING                   = 'O', // Opening
    GIS_TRANSIENT                 = 'T', // Transient
    GIS_INTERVENTION_BEFORE_OPEN  = 'E', // Intervention before
    GIS_CONTINUOUS_TRADING        = 'S', // Continuous Trading
    GIS_MINIBATCH                 = 'M', // Minibatch (resetting
    GIS_INTERVENTION_AFTER_OPEN   = 'R', // Intervention after
    GIS_SERVEILLANCE_INTERVENTION = 'N', // Surveillance Intervention
    GIS_END_OF_CONSULTATION       = 'F', // End of Consultation
    GIS_FORBIDDEN                 = 'I', // Forbidden
    GIS_INTERRUPTED               = 'Z'  // Interrupted
} group_instr_status_t;


/// Quote Type Flag
typedef enum {
    QT_TRADABLE                 = 'T',      /// Tradable
    QT_INDICATIVE               = 'I',      /// Indicative Quote
    QT_CROSS_REQ                = ' '       /// Cross Trade Request
} quote_type_t;


/// Price Type Flag
typedef enum {
    PT_UNKNOWN                  = 0,
    PT_FIRST                    = 30,
    PT_HIGHEST                  = 31,
    PT_LOWEST                   = 32,
    PT_LAST                     = 33,
    PT_ADJ_CLOSE                = 34,
    PT_SETTLEMENT               = 35
} price_type_t;


/// Last Price Type Flag
typedef enum {
    LPT_UNKNOWN                 = 0,
    LPT_BEST_BUY                = 'A',
    LPT_BEST_SELL               = 'V',
    LPT_THEORETICAL_OPENING     = 'I',
    LPT_REFERENCE               = 'S',
    LPT_LAST_TRADING            = ' '
} last_price_type_t;


/// Deletion Type
typedef enum {
    DT_SPECIFIC_ORDER           = 1,
    DT_SIDE                     = 2,
    DT_ALL                      = 3
} deletion_type_t;


/// Order Type
typedef enum {
    OT_MARKET                   = 'M',
    OT_LIMIT                    = 'L',
    OT_OPEN                     = 'O'
} order_type_t;


/// Market Sheet Action Type
typedef enum {
    MSA_CREATION                = 'C',
    MSA_MODIFICATION            = 'M',
    MSA_REBROADCAST             = 'R'
} msheet_action_t;


/// Instrument Type
typedef enum {
    IT_FUTURE                   = 'F',
    IT_AMERICAN_CALL            = 'C',
    IT_AMERICAN_PUT             = 'P',
    IT_CME_SPREAD               = 'D',
    IT_MATIF_SPREAD             = 'S',
    IT_EURO_CALL                = 'X',
    IT_EURO_PUT                 = 'Y',
    IT_UNKNOWN                  = 0
} instr_type_t;


/// Extended Instrument Type
typedef enum {
    ITX_EQUITY                  = 'E',
    ITX_CURRENCY                = 'C',
    ITX_INTEREST_RATE           = 'I',
    ITX_AGRICULTURE             = 'A',
    ITX_ALTERNATIVE             = 'M',
    ITX_NYMEX                   = 'G',
    ITX_ONECHICAGO              = 'S',
    ITX_UNKNOWN                 = 0
} instr_type_ex_t;


/// A side in a CME order
struct limit_order
{
    double  px;
    int     sz;
    int     num_orders; /// number of orders that make up the volume

    void reset()
    {   px = sz = num_orders = 0;    }
};
BOOST_STATIC_ASSERT( sizeof(limit_order) == 16 );

#if !defined(SWIG)
#undef CME_MAKE_WORD
#endif // !defined(SWIG)


/// Flags for the cme_tick message
typedef enum
{
    CTF_NONE             = 0x00,
    CTF_NOT_CANCEL       = 0x01,
    CTF_VARIATION_SAME   = 0x06,
    CTF_VARIATION_PLUS   = 0x02,
    CTF_VARIATION_NEG    = 0x04,
    CTF_LAST_TRADE_SAME  = 0x08
} cme_tick_flags;


/// Flags for cme_instrument_creation (MO) eligible booleans
typedef enum
{
    MOE_NONE             = 0x00,
    MOE_ELECTRONIC_MATCH = 0x01,
    MOE_ORDER_CROSS      = 0x02,
    MOE_BLOCK_TRADE      = 0x04,
    MOE_EFP              = 0x08,
    MOE_EBF              = 0x10,
    MOE_EFS              = 0x20,
    MOE_EFR              = 0x40,
    MOE_OTC              = 0x80
} mo_eligible_t;

enum book_action_t {
    BA_UNKNOWN = 0,
    BA_ADD,
    BA_MODIFY,
    BA_DELETE,
    BA_DELETE_SIDE,
    BA_DELETE_TOP
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(book_action_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const book_action_t& book_action );
#else
;
#endif // !defined(SWIG)

enum stat_type_t {
    STAT_OPEN = 0,
    STAT_HIGH_TRADE,
    STAT_LOW_TRADE,
    STAT_HIGH_BID,
    STAT_LOW_ASK,
    STAT_CLEARED_VOL,
    STAT_OPEN_INTEREST,
    STAT_SETTL,
    STAT_FIXING
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(stat_type_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const stat_type_t& type );
#else
;
#endif // !defined(SWIG)

enum stat_flag_t {
    STAT_ACTUAL   = 0x01,
    STAT_ROUNDED  = 0x02,
    STAT_INTRADAY = 0x04,
};

enum security_trading_status_t {
    STS_HALT                   = 2,
    STS_CLOSE                  = 4,
    STS_NEW_PRICE              = 15,
    STS_READY_TO_TRADE         = 17,
    STS_NOT_AVAILABLE_TO_TRADE = 18,
    STS_UNKNOWN                = 20,
    STS_PRE_OPEN               = 21,
    STS_PRE_CORSS              = 24,
    STS_CROSS                  = 25,
    STS_POST_CLOSE             = 26,
    STS_NO_CHANGE              = 103,
    STS_NONE                   = 255,
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(security_trading_status_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const security_trading_status_t& status );
#else
;
#endif // !defined(SWIG)


enum halt_reason_t {
    REASON_GROUP_SCHEDULE            = 0,
    REASON_SURVAILLANCE_INTERVENTION = 1,
    REASON_MARKET_EVENT              = 2,
    REASON_INSTRUMENT_ACTIVATION     = 3,
    REASON_INSTRUMENT_EXPIRATION     = 4,
    REASON_UNKNOWN                   = 5,
    REASON_RECOVERY_IN_PROGRESS      = 6,
    REASON_NONE                      = 255,
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(halt_reason_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const halt_reason_t& reason );
#else
;
#endif // !defined(SWIG)


enum trading_event_t {
    EVENT_NO_EVENT             = 0,
    EVENT_NO_CANCEL            = 1,
    EVENT_RESET_STAT           = 4,
    EVENT_IMPLIED_MATHCING_ON  = 5,
    EVENT_IMPLIED_MATCHING_OFF = 6,
    EVENT_NONE                 = 255,
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(trading_event_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const trading_event_t& event );
#else
;
#endif // !defined(SWIG)

enum trade_action_t {
    TRADE_NEW          = 0,
    TRADE_UPDATE       = 1,
    TRADE_CANCEL       = 2,
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(trade_action_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const trade_action_t& action );
#else
;
#endif // !defined(SWIG)


struct isin6_t {
    isin6_t() { bzero(vals, sizeof(vals) ); }
    char vals[6];
};


struct spread_ratio_t {
    spread_ratio_t() { bzero(vals, sizeof(vals) ); }
    char vals[3];
};

} // namespace cme

#endif // BB_CORE_CONSTS_CME_CONSTS_H
