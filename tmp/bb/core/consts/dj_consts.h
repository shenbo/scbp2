#ifndef BB_CORE_CONSTS_DJ_CONSTS_H
#define BB_CORE_CONSTS_DJ_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/utils.h>

namespace bb {
namespace dj {

enum status_t
{
    STATUS_Prod,
    STATUS_Test,
    STATUS_CommTest,
    STATUS_Beta,
    STATUS_Resend,
} BB_PACK( status_t );

enum guidance_action_t
{
    GUIDANCE_ACTION_UNKNOWN,
    GUIDANCE_ACTION_AboveViews,
    GUIDANCE_ACTION_Adjusts,
    GUIDANCE_ACTION_Backs,
    GUIDANCE_ACTION_BelowViews,
    GUIDANCE_ACTION_Cuts,
    GUIDANCE_ACTION_InLineWithViews,
    GUIDANCE_ACTION_Issues,
    GUIDANCE_ACTION_Narrows,
    GUIDANCE_ACTION_Sees,
    GUIDANCE_ACTION_Withdraws,
    GUIDANCE_ACTION_Widens
} BB_PACK( guidance_action_t );

enum guidance_component_t
{
    GUIDANCE_COMPONENT_None,
    GUIDANCE_COMPONENT_Earnings,
    GUIDANCE_COMPONENT_EPS,
    GUIDANCE_COMPONENT_Rev,
    GUIDANCE_COMPONENT_Sales,
} BB_PACK( guidance_component_t );

enum merger_action_t
{
    MERGER_ACTION_UNKNOWN,
    MERGER_ACTION_CommittedBuy,
    MERGER_ACTION_CommittedMerger,
    MERGER_ACTION_CommittedSell,
    MERGER_ACTION_EndsTalks,
    MERGER_ACTION_FailedBid,
    MERGER_ACTION_ImprovesBid,
    MERGER_ACTION_InPlay,
    MERGER_ACTION_MergerActivity,
    MERGER_ACTION_PossibleBuy,
    MERGER_ACTION_PossibleMerger,
    MERGER_ACTION_PossibleSell,
} BB_PACK( merger_action_t );

enum merger_role_t
{
    MERGER_ROLE_UNKNOWN,
    MERGER_ROLE_Acquirer,
    MERGER_ROLE_Neutral,
    MERGER_ROLE_Seller,
    MERGER_ROLE_Target,
} BB_PACK( merger_role_t );

enum merger_type_t
{
    MERGER_TYPE_UNKNOWN,
    MERGER_TYPE_Stake,
    MERGER_TYPE_Unit,
} BB_PACK( merger_action_t );

enum rating_action_t
{
    RATING_ACTION_UNKNOWN,
    RATING_ACTION_Cut,
    RATING_ACTION_Raise,
    RATING_ACTION_Reiterate,
    RATING_ACTION_Resume,
    RATING_ACTION_Initiate,
} BB_PACK( rating_action_t );

enum rating_t
{
    RATING_UNKNOWN,
    RATING_StrongSell,
    RATING_Sell,
    RATING_Hold,
    RATING_Buy,
    RATING_StrongBuy,
} BB_PACK( rating_t );

enum debt_event_type_t
{
    DEBT_EVENT_TYPE_UNKNOWN,
    DEBT_EVENT_TYPE_Corp,
    DEBT_EVENT_TYPE_FinancialInstitution,
    DEBT_EVENT_TYPE_Insurance,
    DEBT_EVENT_TYPE_Utilities,
} BB_PACK( debt_event_type_t );

enum debt_rating_long_t
{
    DEBT_RATING_LONG_UNKNOWN,
    DEBT_RATING_LONG_AAA,
    DEBT_RATING_LONG_AAp,
    DEBT_RATING_LONG_AA,
    DEBT_RATING_LONG_AAm,
    DEBT_RATING_LONG_Ap,
    DEBT_RATING_LONG_A,
    DEBT_RATING_LONG_Am,
    DEBT_RATING_LONG_BBBp,
    DEBT_RATING_LONG_BBB,
    DEBT_RATING_LONG_BBBm,
    DEBT_RATING_LONG_BBp,
    DEBT_RATING_LONG_BB,
    DEBT_RATING_LONG_BBm,
    DEBT_RATING_LONG_Bp,
    DEBT_RATING_LONG_B,
    DEBT_RATING_LONG_Bm,
    DEBT_RATING_LONG_CCCp,
    DEBT_RATING_LONG_CCC,
    DEBT_RATING_LONG_CCCm,
    DEBT_RATING_LONG_CC,
    DEBT_RATING_LONG_D,
} BB_PACK( debt_rating_long_t );

enum debt_rating_short_t
{
    DEBT_RATING_SHORT_UNKNOWN,
    DEBT_RATING_SHORT_A1p,
    DEBT_RATING_SHORT_A1,
    DEBT_RATING_SHORT_A2,
    DEBT_RATING_SHORT_A3,
    DEBT_RATING_SHORT_B,
    DEBT_RATING_SHORT_C,
    DEBT_RATING_SHORT_Default
} BB_PACK( debt_rating_short_t );

// CW = Credit Watch

enum debt_rating_action_t
{
    DEBT_RATING_ACTION_UNKNOWN,
    DEBT_RATING_ACTION_Up,
    DEBT_RATING_ACTION_Down,
    DEBT_RATING_ACTION_Affirm,
    DEBT_RATING_ACTION_RemovedCW, // Removed from CreditWatch
    DEBT_RATING_ACTION_OnCWDev, // On CreditWatch, Developing
    DEBT_RATING_ACTION_OnCWNeg, // On CreditWatch, Negative
    DEBT_RATING_ACTION_OnCWPos, // On CreditWatch, Positive
} BB_PACK( debt_rating_action_t );

enum debt_rating_type_t
{
    DEBT_RATING_TYPE_UNKNOWN,
    DEBT_RATING_TYPE_LocalLong,
    DEBT_RATING_TYPE_LocalShort,
    DEBT_RATING_TYPE_LocalLongCWOutlook,
    DEBT_RATING_TYPE_ForeignLong,
    DEBT_RATING_TYPE_ForeignShort,
    DEBT_RATING_TYPE_ForeignLongCWOutlook,
} BB_PACK( debt_rating_type_t );

enum treasury_security_class_t
{
    TREASURY_CLASS_UNKNOWN,
    TREASURY_CLASS_Bill,
    TREASURY_CLASS_Bond,
    TREASURY_CLASS_Note,
    TREASURY_CLASS_Tips
} BB_PACK( treasury_security_class_t );

enum commodity_measure_t
{
    MEASURE_UNKNOWN,
    MEASURE_MillBbl,
    MEASURE_MillBblPerDay,
    MEASURE_Bbl,
    MEASURE_Gal,
    MEASURE_BillCubicFt,
    MEASURE_MillCubicFt,
    MEASURE_MillBtu,
    MEASURE_Pct,
    MEASURE_DollarPerBbl,
    MEASURE_CentsPerGallon,
} BB_PACK( commodity_measure_t );

} // namespace dj
} // namespace bb

#endif // BB_CORE_CONSTS_DJ_CONSTS_H
