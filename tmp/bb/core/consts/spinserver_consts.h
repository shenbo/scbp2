#ifndef BB_CORE_CONSTS_SPINSERVER_CONSTS_H
#define BB_CORE_CONSTS_SPINSERVER_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/utils.h>

namespace bb {
namespace spinserver {

enum request_status_t
{
    REQUEST_STATUS_OK = '\0',
    REQUEST_STATUS_Message_Type_Not_Tracked,
    REQUEST_STATUS_Not_Request_Message,
    REQUEST_STATUS_Instrument_Not_Tracked,
} BB_PACK(request_status_t);

} // namespace spinserver
} // namespace bb

#endif // BB_CORE_CONSTS_SPINSERVER_CONSTS_H
