#ifndef BB_CORE_CONSTS_INCREMENTAL_CONSTS_H
#define BB_CORE_CONSTS_INCREMENTAL_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <ostream>

#include <boost/cstdint.hpp>
#include <boost/static_assert.hpp>

namespace incremental {

enum level_type_t{
    LEVEL_OUTRIGHT = 0,
    LEVEL_IMPLIED  = 1
}
#if !defined(SWIG)
  __attribute__((__packed__));

BOOST_STATIC_ASSERT( sizeof(level_type_t) == 1 );

std::ostream& operator<< ( std::ostream& out, const level_type_t& type );
#else
;
#endif // !defined(SWIG)

// A CME book entry
struct book_entry
{
    float          px;
    uint16_t       sz;
    uint16_t       num_orders;
    uint8_t        level;
    level_type_t   type;
    uint8_t        reserved[2];
};

#if !defined(SWIG)

BOOST_STATIC_ASSERT( sizeof(book_entry) == 12 );

std::ostream& operator<< ( std::ostream& out, const book_entry& entry );

#endif // !defined(SWIG)


} // namespace incremental

#endif // BB_CORE_CONSTS_INCREMENTAL_CONSTS_H
