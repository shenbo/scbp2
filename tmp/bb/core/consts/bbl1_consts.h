#ifndef BB_CORE_CONSTS_BBL1_CONSTS_H
#define BB_CORE_CONSTS_BBL1_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/utils.h>

namespace bbl1 {

enum message_network_t
{
    NETWORK_UNKNOWN = '\0',
    NETWORK_NYSE_TR = 'A',
    NETWORK_AMEX_TR = 'B',
    NETWORK_ALL     = 'C',
    NETWORK_NYSE    = 'E',
    NETWORK_AMEX    = 'F',
    NETWORK_Not_App = ' ',
} BB_PACK(message_network_t);

enum session_id_t
{
    SESSION_ID_UNKNOWN = '\0',
    SESSION_ID_ALL     = 'A',
    SESSION_ID_US      = 'U'
} BB_PACK(session_id_t);

enum market_t
{
    MKT_UNKNOWN     = '\0',
    MKT_AMEX        = 'A',
    MKT_BX          = 'B',
    MKT_NSX         = 'C',
    MKT_FINRA_ADF   = 'D',
    MKT_INDEPENDENT = 'E',
    MKT_ISE         = 'I',
    MKT_EDGA        = 'J',
    MKT_EDGX        = 'K',
    MKT_CHX         = 'M',
    MKT_NYSE        = 'N',
    MKT_ARCA        = 'P',
    MKT_NASDAQ      = 'Q',
    MKT_CTS         = 'S',
    MKT_NASDAQ_OMX  = 'T',
    MKT_CBOE        = 'W',
    MKT_PHLX        = 'X',
    MKT_BYX         = 'Y',
    MKT_BATS        = 'Z',
    MKT_NONE        = ' ',
} BB_PACK(market_t);

enum temp_suffix_t
{
    TEMP_SUFFIX_UNKNOWN         = '\0',
    TEMP_SUFFIX_Ex_Div          = 'A',
    TEMP_SUFFIX_Ex_Dist         = 'B',
    TEMP_SUFFIX_Ex_Rights       = 'C',
    TEMP_SUFFIX_New             = 'D',
    TEMP_SUFFIX_Ex_Interest     = 'E',
    TEMP_SUFFIX_Not_App         = ' ',
} BB_PACK(temp_suffix_t);

enum quote_condition_t
{
    QUOTE_COND_UNKNOWN                               = '\0',
    QUOTE_COND_Slow_Due_To_Ask_Side                  = 'A',
    QUOTE_COND_Slow_Due_To_Bid_Side                  = 'B',
    QUOTE_COND_Closing                               = 'C',
    QUOTE_COND_News_Dissemination                    = 'D',
    QUOTE_COND_LRP_Bid_Side                          = 'E',
    QUOTE_COND_LRP_Ask_Side                          = 'F',
    QUOTE_COND_Trading_Range_Indication              = 'G',
    QUOTE_COND_Slow_Both_Sides                       = 'H',
    QUOTE_COND_Order_Imbalance                       = 'I',
    QUOTE_COND_Related_Security_News_Dissemination   = 'J',
    QUOTE_COND_Related_Security_News_Pending         = 'K',
    QUOTE_COND_Closed                                = 'L',
    QUOTE_COND_Addl_Info                             = 'M',
    QUOTE_COND_Non_Firm                              = 'N',
    QUOTE_COND_Opening                               = 'O',
    QUOTE_COND_News_Pending                          = 'P',
    QUOTE_COND_Addl_Info_Due_To_Related_Security     = 'Q',
    QUOTE_COND_Regular                               = 'R',
    QUOTE_COND_Related_Security                      = 'S',
    QUOTE_COND_Resume                                = 'T',
    QUOTE_COND_LRP_Both_Sides                        = 'U',
    QUOTE_COND_In_View_Of_Common                     = 'V',
    QUOTE_COND_Slow_Due_To_Set_Slow_List             = 'W',
    QUOTE_COND_Equipment_Changeover                  = 'X',
    QUOTE_COND_Sub_Penny_Trading                     = 'Y',
    QUOTE_COND_No_Open_No_Resume                     = 'Z',
} BB_PACK(quote_condition_t);

enum financial_status_t
{
    FIN_STATUS_UNKNOWN                      = '\0',
    FIN_STATUS_Normal                       = '0',
    FIN_STATUS_Bankrupt                     = '1',
    FIN_STATUS_Below_Standards              = '2',
    FIN_STATUS_Bankrupt_Below_Standards     = '3',
    FIN_STATUS_Late_Filing                  = '4',
    FIN_STATUS_Bankrupt_Late_Filing         = '5',
    FIN_STATUS_Below_Standards_Late_Filing  = '6',
    FIN_STATUS_Bankrupt_Below_Stdrds_Late   = '7',
} BB_PACK(financial_status_t);

enum price_indicator_t
{
    PRICE_INDICATOR_UNKNOWN    = '\0',
    PRICE_INDICATOR_Cancel     = 'A',
    PRICE_INDICATOR_Correction = 'B',
    PRICE_INDICATOR_Not_App    = ' ',
} BB_PACK(price_indicator_t);

enum held_trade_indicator_t
{
    HELD_TRADE_UNKNOWN           = '\0',
    HELD_TRADE_Valid_None        = 'A',
    HELD_TRADE_Valid_Participant = 'B',
    HELD_TRADE_Valid_Both        = 'C',
    HELD_TRADE_Valid_Not_App     = ' ',
} BB_PACK(held_trade_indicator_t);

enum settlement_condition_t
{
    SETTLE_COND_UNKNOWN         = '\0',
    SETTLE_COND_Regular_Way     = 'A',
    SETTLE_COND_Cash_Only       = 'B',
    SETTLE_COND_Next_Day        = 'C',
} BB_PACK(settlement_condition_t);

enum mkt_condition_t
{
    MKT_COND_UNKNOWN         = '\0',
    MKT_COND_Normal          = 'A',
    MKT_COND_Crossed         = 'B',
    MKT_COND_Locked          = 'C',
} BB_PACK(mkt_condition_t);

enum nat_bbo_app_ind_t
{
    NAT_BBO_APP_IND_UNKNOWN     = '\0',
    NAT_BBO_APP_IND_No_Effect   = '0',
    NAT_BBO_APP_IND_Is_Quote    = '1',
    NAT_BBO_APP_IND_No_Calc     = '2',
    NAT_BBO_APP_IND_Long_Form   = '4',
    NAT_BBO_APP_IND_Short_Form  = '6',
} BB_PACK(nat_bbo_app_ind_t);

enum finra_bbo_app_ind_t
{
    FINRA_BBO_APP_IND_UNKNOWN   = '\0',
    FINRA_BBO_APP_IND_No_Effect = '0',
    FINRA_BBO_APP_IND_Is_Quote  = '1',
    FINRA_BBO_APP_IND_No_Calc   = '2',
    FINRA_BBO_APP_IND_Appendage = '3',
    FINRA_BBO_APP_IND_Not_Appl  = ' ',
} BB_PACK(finra_bbo_app_ind_t);

enum finra_adf_app_ind_t
{
    FINRA_ADF_APP_IND_UNKNOWN   = '\0',
    FINRA_ADF_APP_IND_No_Effect = '0',
    FINRA_ADF_APP_IND_Is_Quote  = '1',
    FINRA_ADF_APP_IND_Appendage = '2',
    FINRA_ADF_APP_IND_Not_Appl  = ' ',
} BB_PACK(finra_adf_app_ind_t);

enum special_condition_t
{
    SPECIAL_CONDITION_UNKNOWN         = '\0',
    SPECIAL_CONDITION_One_Sided       = 'O',
    SPECIAL_CONDITION_Trading_Halt    = 'H',
    SPECIAL_CONDITION_No_Participants = 'M',
    SPECIAL_CONDITION_None            = ' ',
} BB_PACK(special_condition_t);

enum trading_action_t
{
    TRADING_ACTION_UNKNOWN              = '\0',
    TRADING_ACTION_Trading_Halt         = 'H',
    TRADING_ACTION_Quotation_Resumption = 'Q',
    TRADING_ACTION_Trading_Resumption   = 'T',
} BB_PACK(trading_action_t);

enum action_reason_t
{
    ACTION_REASON_UNKNOWN = '\0',
    ACTION_REASON_Halt_News_Pending,
    ACTION_REASON_Halt_News_Dissemination,
    ACTION_REASON_Reg_Halt_Extraordinary_Market_Activity,
    ACTION_REASON_Halt_ETF,
    ACTION_REASON_Halt_NASDAQ_Info_Request,
    ACTION_REASON_Halt_Non_Compliance,
    ACTION_REASON_Halt_Filings_Not_Current,
    ACTION_REASON_Halt_SEC_Trading_Suspension,
    ACTION_REASON_Halt_Regulatory_Concern,
    ACTION_REASON_Operations_Halt,
    ACTION_REASON_IPO_Not_Yet_Trading,
    ACTION_REASON_Corporate_Action,
    ACTION_REASON_Quote_Not_Available,
    ACTION_REASON_News_And_Resumption_Times,
    ACTION_REASON_Qual_Issues_Resolved_Will_Resume,
    ACTION_REASON_Filing_Req_Resolved_Will_Resume,
    ACTION_REASON_News_Not_Forthcoming_Will_Resume,
    ACTION_REASON_Qual_Halt_End_Maint_Req_Will_Resume,
    ACTION_REASON_Qual_Halt_Concl_Filings_Met_Will_Resume,
    ACTION_REASON_Trade_Halt_Concl_By_Other_Reg_Auth_Will_Resume,
    ACTION_REASON_New_Issue_Available,
    ACTION_REASON_Issue_Available,
    ACTION_REASON_IPO_Released_For_Quotation,
    ACTION_REASON_IPO_Positioning_Extension,
    ACTION_REASON_Not_Available,
} BB_PACK(action_reason_t);

enum sale_condition_indices_t
{
    SALE_CONDITION_Settlement   = '\0',
    SALE_CONDITION_TT_Exemption = 1,
    SALE_CONDITION_Extended     = 2,
    SALE_CONDITION_Detail       = 3,
} BB_PACK(sale_condition_indices_t);

enum sale_condition_settlement_t
{
    SETTLE_UNKNOWN                   = '\0',
    SETTLE_Regular_Trade             = '@',
    SETTLE_Cash_Sale                 = 'C',
    SETTLE_Next_Day                  = 'N',
    SETTLE_Seller                    = 'R',
    SETTLE_Yellow_Flag_Regular_Trade = 'Y',
    SETTLE_Regular_Settlement        = ' '
} BB_PACK(sale_condition_settlement_t);

enum sale_condition_tt_exemption_t
{
    EXEMPTION_UNKNOWN                      = '\0',
    EXEMPTION_Intermarket_Sweep            = 'F',
    EXEMPTION_Opening_Prints               = 'O',
    EXEMPTION_Derivatively_Priced          = '4',
    EXEMPTION_Reopening_Prints             = '5',
    EXEMPTION_Closing_Prints               = '6',
	EXEMPTION_Qualified_Contingent_Trade   = '7',
    EXEMPTION_Placeholder_For_611_Exempt_8 = '8',
    EXEMPTION_Corrected_Consolidated_Close = '9',  //used by both CTA and UTP
    EXEMPTION_None                         = ' ',
} BB_PACK(sale_condition_tt_exemption_t);

enum sale_condition_extended_t
{
    EXTENDED_UNKNOWN                             = '\0',
    EXTENDED_Sold_Last                           = 'L',
    EXTENDED_Extended_Hours                      = 'T',
    EXTENDED_Extended_Hours_Sold_Out_Of_Sequence = 'U',
    EXTENDED_Sold_Out_Of_Sequence                = 'Z',
    EXTENDED_None                                = ' ',
} BB_PACK(sale_condition_extended_t);

enum sale_condition_detail_t
{
    DETAIL_UNKNOWN                             = '\0',
    DETAIL_Acquisition                         = 'A',
    DETAIL_Average_Price_Trade                 = 'B', // UTP uses 'W' for Average Price Trade
    DETAIL_Distribution                        = 'D',
    DETAIL_Automatic_Execution                 = 'E',
    DETAIL_Bunched_Sold_Trade                  = 'G',
    DETAIL_Price_Variation_Trade               = 'H',
    DETAIL_CAP_Election_Trade                  = 'I',
    DETAIL_Rule_127_Or_155                     = 'K',
    DETAIL_Market_Center_Official_Close        = 'M',
    DETAIL_Prior_Reference_Price               = 'P',
    DETAIL_Market_Center_Official_Open         = 'Q',
    DETAIL_Split_Trade                         = 'S',
    DETAIL_Contingent_Trade                    = 'V',
    DETAIL_Bunched_Trade                       = 'W', // UTP uses 'B' for Bunched Trade
    DETAIL_Cross_Trade                         = 'X',
    DETAIL_Stopped_Stock_Regular_Trade         = '1',
    DETAIL_Stopped_Stock_Sold_Last             = '2',
    DETAIL_Stopped_Stock_Sold_Out_Of_Sequence  = '3',
    DETAIL_None                                = ' ',
} BB_PACK(sale_condition_detail_t);

enum consolidated_price_indicator_t
{
    CONS_PX_UNKNOWN         = '\0',
    CONS_PX_None            = 'A',
    CONS_PX_High            = 'B',
    CONS_PX_Low             = 'C',
    CONS_PX_Last            = 'D',
    CONS_PX_High_Last       = 'E',
    CONS_PX_Low_Last        = 'F',
    CONS_PX_High_Low_Last   = 'G',
    CONS_PX_High_Low        = 'H',
} BB_PACK(consolidated_price_indicator_t);

enum participant_price_indicator_t
{
    PART_PX_UNKNOWN             = '\0',
    PART_PX_None                = 'A',
    PART_PX_High                = 'B',
    PART_PX_Low                 = 'C',
    PART_PX_Last                = 'D',
    PART_PX_High_Last           = 'E',
    PART_PX_Low_Last            = 'F',
    PART_PX_Unused              = 'G',
    PART_PX_Open                = 'H',
    PART_PX_Open_High           = 'I',
    PART_PX_Open_Low            = 'J',
    PART_PX_Open_High_Low_Last  = 'K',
    PART_PX_Open_Last           = 'L',
    PART_PX_Open_High_Low       = 'M',
    PART_PX_Open_High_Last      = 'N',
    PART_PX_Open_Low_Last       = 'O',
    PART_PX_High_Low            = 'P',
    PART_PX_High_Low_Last       = 'Q',
} BB_PACK(participant_price_indicator_t);

enum cancel_action_t
{
    CANCEL_ACTION_UNKNOWN = '\0',
    CANCEL_ACTION_Cancel  = '1',
    CANCEL_ACTION_Error   = '2',
} BB_PACK(cancel_action_t);

enum security_status_t
{
    SEC_STATUS_UNKNOWN           = '\0',
    SEC_STATUS_Opening_Delay     = '1',
    SEC_STATUS_Trading_Halt      = '2',
    SEC_STATUS_Resume            = '3',
    SEC_STATUS_No_Open_No_Resume = '4',
    SEC_STATUS_Price_Ind         = '5',
    SEC_STATUS_Trading_Range_Ind = '6',
    SEC_STATUS_Market_Imba_Buy   = '7',
    SEC_STATUS_Market_Imba_Sell  = '8',
    SEC_STATUS_MOC_Imba_Buy      = '9',
    SEC_STATUS_MOC_Imba_Sell     = 'A',
    SEC_STATUS_Reserved          = 'B',
    SEC_STATUS_No_Market_Imba    = 'C',
    SEC_STATUS_No_MOC_Imba       = 'D',
    SEC_STATUS_Short_Sale_Restriction = 'E',
} BB_PACK(security_status_t);

enum halt_reason_t
{
    HALT_REASON_UNKNOWN            = '\0',
    HALT_REASON_Order_imba         = 'I',
    HALT_REASON_Equipment_Change   = 'X',
    HALT_REASON_News_Pending       = 'P',
    HALT_REASON_News_Dissemination = 'D',
    HALT_REASON_Order_Influx       = 'E',
    HALT_REASON_Addl_Information   = 'M',
    HALT_REASON_Sub_Penny_Trading  = 'Y',
} BB_PACK(halt_reason_t);

enum status_indicator_t
{
    STATUS_IND_UNKNOWN         = '\0',
    STATUS_IND_New_Price       = '1',
    STATUS_IND_Corrected_Price = '2',
    STATUS_IND_Cancelled_Price = '3',
} BB_PACK(status_indicator_t);

enum tick_t
{
    TICK_UNKNOWN = '\0',
    TICK_PLUS    = '1',
    TICK_MINUS   = '2',
    TICK_0_PLUS  = '3',
    TICK_0_MINUS = '4',
} BB_PACK(tick_t);

enum price_denominator_t
{
    PRICE_DENOM_UNKNOWN         = '\0',
    PRICE_DENOM_10              = 'A',
    PRICE_DENOM_100             = 'B',
    PRICE_DENOM_1000            = 'C',
    PRICE_DENOM_10000           = 'D',
    PRICE_DENOM_100000          = 'E',
    PRICE_DENOM_1000000         = 'F',
    PRICE_DENOM_10000000        = 'G',
    PRICE_DENOM_100000000       = 'H',
    PRICE_DENOM_1               = 'I',
    PRICE_DENOM_8               = '3',
    PRICE_DENOM_16              = '4',
    PRICE_DENOM_32              = '5',
    PRICE_DENOM_64              = '6',
    PRICE_DENOM_128             = '7',
    PRICE_DENOM_256             = '8',
    PRICE_DENOM_0               = '0',
} BB_PACK(price_denominator_t);

enum issue_type_t
{
    ISSUE_TYPE_UNKNOWN        = '\0',
    ISSUE_TYPE_ADR            = 'A',
    ISSUE_TYPE_Bond           = 'B',
    ISSUE_TYPE_Common_Shr     = 'C',
    ISSUE_TYPE_Bond_Deriv     = 'D',
    ISSUE_TYPE_Equity_Deriv   = 'E',
    ISSUE_TYPE_Dep_Receipt    = 'F',
    ISSUE_TYPE_Corp_Bond      = 'G',
    ISSUE_TYPE_Ltd_Partner    = 'L',
    ISSUE_TYPE_Misc           = 'M',
    ISSUE_TYPE_Note           = 'N',
    ISSUE_TYPE_Ordinary_Shr   = 'O',
    ISSUE_TYPE_Prfd_Shr       = 'P',
    ISSUE_TYPE_Rights         = 'R',
    ISSUE_TYPE_Shr_Benef_Int  = 'S',
    ISSUE_TYPE_Conv_Debenture = 'T',
    ISSUE_TYPE_Unit           = 'U',
    ISSUE_TYPE_Unit_Benef_Int = 'V',
    ISSUE_TYPE_Warrant        = 'W',
    ISSUE_TYPE_Index_Warrant  = 'X',
    ISSUE_TYPE_Put_Warrant    = 'Y',
    ISSUE_TYPE_Unclassified   = 'Z',
} BB_PACK(issue_type_t);

enum mkt_tier_t
{
    MKT_TIER_UNKNOWN               = '\0',
    MKT_TIER_NASDAQ_Global_Market  = 'G',
    MKT_TIER_NASDAQ_Global_Select  = 'Q',
    MKT_TIER_NASDAQ_Capital_Market = 'S',
} BB_PACK(mkt_tier_t);

enum authenticity_t
{
    AUTHENTICITY_UNKNOWN = '\0',
    AUTHENTICITY_Demo    = 'D',
    AUTHENTICITY_Live    = 'P',
    AUTHENTICITY_Test    = 'T',
    AUTHENTICITY_Deleted = 'X',
} BB_PACK(authenticity_t);

enum short_sale_threshold_t
{
    SHORT_SALE_THRESHOLD_UNKNOWN      = '\0',
    SHORT_SALE_THRESHOLD_Unrestricted = 'N',
    SHORT_SALE_THRESHOLD_Restricted   = 'Y',
    SHORT_SALE_THRESHOLD_Not_Avail    = ' '
} BB_PACK(short_sale_threshold_t);

enum reg_sho_action_t
{
    REG_SHO_ACTION_UNKNOWN              = '\0',
    REG_SHO_ACTION_Price_Test_Inactive  = '0',
    REG_SHO_ACTION_Price_Test_Active    = '1',
    REG_SHO_ACTION_Price_Test_Continued = '2',
} BB_PACK(reg_sho_action_t);

} // namespace bbl1

#endif // BB_CORE_CONSTS_BBL1_CONSTS_H
