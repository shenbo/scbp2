#ifndef BB_CORE_CONSTS_NTKN_CONSTS_H
#define BB_CORE_CONSTS_NTKN_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/static_assert.hpp>

namespace bb {
namespace ntkn {

#ifndef SWIG
#define BB_NTKN_PACK( ENUM ) \
    __attribute__((__packed__)); \
    BOOST_STATIC_ASSERT( sizeof( ENUM ) == 1 )
#else
#define BB_NTKN_PACK( ENUM )
#endif

enum message_type_t
{
    MESSAGE_TYPE_Release  = 0,
    MESSAGE_TYPE_Estimate = 1,
    MESSAGE_TYPE_System   = 2,
} BB_NTKN_PACK( message_type_t );

enum field_type_t
{
    FIELD_TYPE_Float       = 0,
    FIELD_TYPE_FloatRange  = 1,
    FIELD_TYPE_Short       = 2,
    FIELD_TYPE_Long        = 3,
    FIELD_TYPE_Double      = 4,
    FIELD_TYPE_Date        = 5,
    FIELD_TYPE_Boolean     = 6,
    FIELD_TYPE_YesNoNa     = 7,
    FIELD_TYPE_Directional = 8,
    FIELD_TYPE_Int         = 9,
} BB_NTKN_PACK( field_type_t );

#undef BB_NTKN_PACK

} // namespace ntkn
} // namespace bb

#endif // BB_CORE_CONSTS_NTKN_CONSTS_H
