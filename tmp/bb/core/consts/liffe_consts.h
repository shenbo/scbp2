#ifndef BB_CORE_CONSTS_LIFFE_CONSTS_H
#define BB_CORE_CONSTS_LIFFE_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <boost/static_assert.hpp>

namespace liffe {

typedef enum {
    BC_START = 0,   // A book transmission is starting
    BC_END   = 1,   // A book transmission has finished
    BC_UNDEFINED = 0xFFFF
} book_cmd_t;


//
// These are copied from LiffeAPI.h
// See the LIFFE CONNECT Reference Manal for their meanings
//


typedef enum {
    TT_UNDEFINED = 0x120000,
    TT_BLOCK,
    TT_BASIS,
    TT_AGAINST_ACTUAL,
    TT_EXTERNAL_MATCH,
    TT_EXCHANGE_FOR_SWAP,
    TT_EXCHANGE_FOR_PHYSICAL,
    TT_CONVENTIONAL,
    TT_GUARANTEED_CROSS,
    TT_PROFESSIONAL,
    TT_ASSET_ALLOCATION
} trade_type_t;


typedef enum {
    ST_UNKNOWN         = 0,
    ST_DAILY           = 'D',
    ST_MARKET_CLOSE    = 'C',
    ST_CONTRACT_EXPIRY = 'E',
    ST_INTRADAY        = 'I',
    ST_YDSP            = 'Y'
} settlement_type_t;


typedef enum {
    PT_UNKNOWN         = 0,
    PT_PROVISIONAL     = 'P',
    PT_OFFICIAL        = 'O'
} publish_type_t;


typedef enum {
    MM_UNDEFINED            = 0x100000,
    MM_FAST                 = MM_UNDEFINED + 0x1,
    MM_ENABLED              = MM_UNDEFINED + 0x2,
    MM_PRICE_LIMITS_ENABLED = MM_UNDEFINED + 0x4,
    MM_UNAVAILABLE          = MM_UNDEFINED + 0x8,
    MM_PRECLOSED            = MM_UNDEFINED + 0x10,
    MM_CLOSED               = MM_UNDEFINED + 0x20,
    MM_PREOPEN              = MM_UNDEFINED + 0x40,
    MM_OPEN                 = MM_UNDEFINED + 0x80,
    MM_RESTRICTED_OPEN      = MM_UNDEFINED + 0x100,
    MM_QSM_MODE1            = MM_UNDEFINED + 0x200,
    MM_QSM_MODE2            = MM_UNDEFINED + 0x400,
    MM_QSM_MODE3            = MM_UNDEFINED + 0x600,
    MM_EXPIT_BLOCK_OPEN     = MM_UNDEFINED + 0x800,
    MM_EXPIT_EXTENDED_OPEN  = MM_UNDEFINED + 0x1000,
    MM_HALTED               = MM_UNDEFINED + 0x2000
} market_mode_t;


typedef enum {
    EX_UNDEFINED            = 0xD0000,
    EX_TRADE                = EX_UNDEFINED + 0x1,
    EX_BUY                  = EX_UNDEFINED + 0x2,
    EX_SELL                 = EX_UNDEFINED + 0x4,
    EX_TOTAL_TRADE_VOLUME   = EX_UNDEFINED + 0x8,
    EX_SNAPSHOT             = EX_UNDEFINED + 0x10,
    EX_INDICATIVE_OPENING   = EX_UNDEFINED + 0x20,
    EX_TRADE_NO_VOLUME      = EX_UNDEFINED + 0x40
} extent_t;


typedef enum {
    RFQX_FLAG_UNDEFINED = 0xA0000, // LIFFE_CONSTANT_RANGE__RFQX_FLAG
    RFQX_FLAG_INTENDED,
    RFQX_FLAG_TRUE,
    RFQX_FLAG_FALSE,
    RFQX_FLAG_EXCLUSIVE_RANGE_LIMIT
} RFQXFlag;


// Stream operators.  Implemented in liffe_consts.cc
std::ostream& operator<<( std::ostream& out, trade_type_t );
std::ostream& operator<<( std::ostream& out, settlement_type_t );
std::ostream& operator<<( std::ostream& out, publish_type_t );
std::ostream& operator<<( std::ostream& out, market_mode_t );
std::string toString( extent_t );


BOOST_STATIC_ASSERT( sizeof(book_cmd_t) == 4 );
BOOST_STATIC_ASSERT( sizeof(trade_type_t) == 4 );
BOOST_STATIC_ASSERT( sizeof(settlement_type_t) == 4 );
BOOST_STATIC_ASSERT( sizeof(publish_type_t) == 4 );
BOOST_STATIC_ASSERT( sizeof(trade_type_t) == 4 );
BOOST_STATIC_ASSERT( sizeof(extent_t) == 4 );

} // namespace liffe

#endif // BB_CORE_CONSTS_LIFFE_CONSTS_H
