#ifndef BB_CORE_CONSTS_OPRA_CONSTS_H
#define BB_CORE_CONSTS_OPRA_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/static_assert.hpp>

#include <bb/core/utils.h>

namespace bb {
namespace opra {

enum session_t
{
    SESSION_UNKNOWN = '\0',
    SESSION_Morning = 'a',
    SESSION_Normal  = ' ',
} BB_PACK( session_t );

enum bbo_ind_t
{
    BBO_UNKNOWN                                 = '\0',
    BBO_No_Bid_Change_No_Offer_Change           = 'A',
    BBO_No_Bid_Change_Best_Offer_Quote          = 'B',
    BBO_No_Bid_Change_Best_Offer_Appendage      = 'C',
    BBO_No_Bid_Change_No_Best_Offer             = 'D',
    BBO_Best_Bid_Quote_No_Offer_Change          = 'E',
    BBO_Best_Bid_Quote_Best_Offer_Quote         = 'F',
    BBO_Best_Bid_Quote_Best_Offer_Appendage     = 'G',
    BBO_Best_Bid_Quote_No_Best_Offer            = 'H',
    BBO_No_Best_Bid_No_Offer_Change             = 'I',
    BBO_No_Best_Bid_Best_Offer_Quote            = 'J',
    BBO_No_Best_Bid_Best_Offer_Appendage        = 'K',
    BBO_No_Best_Bid_No_Best_Offer               = 'L',
    BBO_Best_Bid_Appendage_No_Offer_Change      = 'M',
    BBO_Best_Bid_Appendage_Best_Offer_Quote     = 'N',
    BBO_Best_Bid_Appendage_Best_Offer_Appendage = 'O',
    BBO_Best_Bid_Appendage_No_Best_Offer        = 'P',
    BBO_Not_Included                            = ' ',
} BB_PACK( bbo_ind_t );

enum trade_type_t
{
    TRADE_UNKNOWN                       = '\0',
    TRADE_Regular                       = ' ',
    TRADE_Cancel_Not_Last_or_Opening    = 'A',
    TRADE_Late_and_Out_of_Sequence      = 'B',
    TRADE_Cancel_Last_Reported          = 'C',
    TRADE_Late                          = 'D',
    TRADE_Cancel_Opening                = 'E',
    TRADE_Out_of_Sequence               = 'F',
    TRADE_Cancel_Only                   = 'G',
    TRADE_Late_Opening                  = 'H',
    TRADE_Automated                     = 'I',
    TRADE_Reopening                     = 'J',
    TRADE_Adjusted                      = 'K',
    TRADE_Spread                        = 'L',
    TRADE_Straddle                      = 'M',
    TRADE_Stopped                       = 'N',
    TRADE_Cancel_Stopped                = 'O',
    TRADE_With_Stock                    = 'P',
    TRADE_Combination                   = 'Q',
} BB_PACK( trade_type_t );

enum quote_type_t
{
    QUOTE_UNKNOWN               = '\0',
    QUOTE_Regular               = ' ',
    QUOTE_Non_Firm              = 'F',
    QUOTE_Rotation              = 'R',
    QUOTE_Trading_Halted        = 'T',
    QUOTE_Automated_Eligible    = 'A',
    QUOTE_Inactive              = 'I',
    QUOTE_Specialist_Bid        = 'B',
    QUOTE_Specialist_Offer      = 'O',
    QUOTE_Specialist_Both_Sides = 'C'

} BB_PACK( quote_type_t );

enum control_type_t
{
    CONTROL_UNKNOWN                     = '\0',
    CONTROL_Test_Start                  = 'A',
    CONTROL_Test_End                    = 'B',
    CONTROL_Day_Start                   = 'C',
    CONTROL_Good_Morning                = 'D',
    CONTROL_Summary_Start               = 'E',
    CONTROL_Summary_End                 = 'F',
    CONTROL_Early_Market_Close          = 'G',
    CONTROL_Transacting_Reporting_End   = 'H',
    CONTROL_Good_Night                  = 'I',
    CONTROL_Day_End                     = 'J',
    CONTROL_Reset_Sequence_Number       = 'K',
    CONTROL_Open_Interest_Start         = 'L',
    CONTROL_Open_Interest_End           = 'M'
} BB_PACK( control_type_t );

enum underlying_value_type_t
{
    UNDERLYING_UNKNOWN           = '\0',
    UNDERLYING_FROM_Last_Sale    = ' ',
    UNDERLYING_FROM_Bid_Offer    = 'I',
    UNDERLYING_FROM_FX_Spot      = 'F',
    UNDERLYING_FROM_Closing_Spot = 'C',
} BB_PACK( underlying_value_type_t );

} // namespace opra
} // namespace bb

#endif // BB_CORE_CONSTS_OPRA_CONSTS_H
