#ifndef BB_CORE_ERROR_H
#define BB_CORE_ERROR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <stdexcept>
#include <sstream>
#include <cstdarg>

#include <boost/throw_exception.hpp>

#include <bb/core/compat.h> // likely/unlikely


namespace bb {


/** Generic Error exception.  It allows you to pass into the
    constructor printf-like syntax to be included in the error
    message.  It also deals with copy construction and all that
    good stuff properly.  Use this class in a situation that you
    don't care about having hierarchical errors, but you do care
    that some useful information is unwound up the stack.

    It has a maximum msg length of Error::MAX_LEN.
*/
class Error : public std::exception {
public:
    static const unsigned int MAX_LEN = 1024;

    /** Construct an empty error. */
    Error();

    /** Copy constructor. */
    Error(const Error &);

    /** Constructs an Error with a message copied from string */
    Error(const std::string &);

    /** Constructs an Error with message according to the format
        specified by printf. */
    Error(const char *format, ...);

    /** Destructor **/
    virtual ~Error() throw();

    /** Returns a C-style character string describing the general cause of the current error. **/
    virtual const char* what() const throw();

protected:
    void set_error(const char *format, ...);
    void set_error(const char *format, va_list ap);

private:
    char *msg;

    void invalidate();
};

#define BB_DEFINE_ERROR_AS_SUBCLASS_OF( _base_error_type_, _new_type_ )                                \
class _new_type_ : public _base_error_type_ {                                                          \
public:                                                                                                \
    inline _new_type_() :                                                                              \
        _base_error_type_() {}                                                                         \
                                                                                                       \
    inline _new_type_(const _new_type_& other)                                                         \
        : _base_error_type_(other) {}                                                                  \
                                                                                                       \
    inline _new_type_(const std::string& str)                                                          \
        : _base_error_type_(str) {}                                                                    \
                                                                                                       \
    inline _new_type_(const char* format, ...)                                                         \
        : _base_error_type_() { va_list ap; va_start(ap, format); set_error(format, ap); va_end(ap); } \
};

#define BB_DEFINE_ERROR( _new_type_ ) \
    BB_DEFINE_ERROR_AS_SUBCLASS_OF( bb::Error, _new_type_ )

// This will catch anything thrown by QQQ, LOG_ERROR including QQQ as text and rethrow the original exception.
#define BB_TRY_CATCH_AND_RETHROW( QQQ )try { QQQ; }catch( const std::exception& e ){ LOG_ERROR << "caught exception at: " << e.what() << "[" << #QQQ "] rethrowing." << bb::endl; throw; }

// We write our own wrapper around boost::throw_exception so we can
// tag it as a non-returning function to silence gcc when it wants to
// get all whiny. We have to do the declaration and definition
// separately since gcc doesn't allow attribute declarations on
// definitions.
template<typename T>
inline void throw_exception( const T& exception ) _attr_noreturn_ _attr_cold_;

template<typename T>
inline void throw_exception( const T& exception )
{
    boost::throw_exception( exception );
    abort(); // if throwing fails, just terminate.
}


/// This is used in a few places to throw/catch when the commandline arguments are incorrect
struct UsageError : public std::runtime_error
{
    UsageError(std::string errmsg="") : std::runtime_error(errmsg) {}
};


// ostream operator for bb::Error
std::ostream& operator <<( std::ostream &out, const bb::Error &e );

} // namespace bb

/// Throws the first argument, which must be a type, with any
/// additional arguments passed to the constructor of that type.
#define BB_THROW_EXCEPTION( _exct_type_, ... ) \
    bb::throw_exception( _exct_type_( __VA_ARGS__ ) )

/// Note: the following macro is constructed as a do-while block which executes
/// exactly once so they can be used as if they were functions, including in if-else.

/// BB_THROW_ERROR_SS with a custom exception type.
#define BB_THROW_EXCEPTION_SS( _exct_type_, _error_stream_ )                 \
    do {                                                                     \
        std::ostringstream _throw_error_ss_;                                 \
        _throw_error_ss_ << _error_stream_;                                  \
        BB_THROW_EXCEPTION( _exct_type_, _throw_error_ss_.str() );           \
    } while( false )

#define BB_THROW_EXCEPTION_SSX( _exct_type_, _error_stream_ ) \
    BB_THROW_EXCEPTION_SS( _exct_type_, SRC_LOCATION_SEP << _error_stream_ )

/// Macro to make throwing bb::Errors easier.
#define BB_THROW_ERROR( _error_string_ ) \
    BB_THROW_EXCEPTION( bb::Error, _error_string_ )

/// Macro to make throwing bb::Errors easier.
/// Basically it constructs a string from a stringstream
/// and then throws that string.
#define BB_THROW_ERROR_SS( _error_stream_ ) \
    BB_THROW_EXCEPTION_SS( bb::Error, _error_stream_ )

/// Macro to make throwing bb::Errors easier.
/// Constructs a string from a stringstream
/// and then throws that string.
/// This extended version prepends line information
#define BB_THROW_ERROR_SSX( _error_stream_ ) \
    BB_THROW_ERROR_SS(SRC_LOCATION_SEP << _error_stream_ )

/// Macro to make throwing bb::Errors easier.
/// tests condition, and if false, throws
/// like an assert
#define BB_THROW_EXASSERT_SSX(_cond_, _error_stream_ )  \
    do {                                                \
        if(unlikely(!(_cond_)))                         \
            BB_THROW_ERROR_SSX(_error_stream_);         \
    } while(false)

#define BB_THROW_EXASSERT_SS(_cond_, _error_stream_ )   \
    do {                                                \
        if(unlikely(!(_cond_)))                         \
            BB_THROW_ERROR_SS(_error_stream_);          \
    } while(false)

#define BB_THROW_EXASSERT(_cond_, _error_string_ )      \
    do {                                                \
        if(unlikely(!(_cond_)))                         \
            BB_THROW_ERROR(_error_string_);             \
    } while(false)

/// Macro to make throwing bb::Errors easier.
/// tests condition, and if false, throws
/// like an assert, defined only when NDEBUG is not defined
#if !defined(NDEBUG)
#define BB_THROW_EXASSERT_SSX_DEBUG(_cond_, _error_stream_ )  BB_THROW_EXASSERT_SSX(_cond_,_error_stream_)
#define BB_THROW_EXASSERT_DEBUG(_cond_, _error_string_ )  BB_THROW_EXASSERT_SSX(_cond_,_error_string_)
#else
#define BB_THROW_EXASSERT_SSX_DEBUG(_cond_, _error_stream_ )  (void)0
#define BB_THROW_EXASSERT_DEBUG(_cond_, _error_string_ )  (void)0
#endif



#endif // BB_CORE_ERROR_H
