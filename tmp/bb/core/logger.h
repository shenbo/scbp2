#ifndef BB_CORE_LOGGER_H
#define BB_CORE_LOGGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <ostream>
#include <string>

#include <boost/thread/recursive_mutex.hpp>

#include <bb/core/bbassert.h>
#include <bb/core/loglevel.h>


namespace bb {

/** Logging class that prepends an indentifier plus filename and
    line number info before each message.  Not really used
    directly.  See Log.h for proper usage from within a C++
    program. */
class logger
{
public:
    logger(const std::string &_name);
    logger(const std::string &_name, std::ostream &_out);
    logger &operator ()(loglevel_t level, const char *srcLocation);
    ~logger();

    template<typename T>
    void print(const T &arg);

    logger &endl();

    typedef logger &(*manip)(logger &, int);

    // formatting methods to be called without actually logging anything
    std::ios_base::fmtflags setf(std::ios_base::fmtflags flags);
    std::streamsize precision(std::streamsize prec);

    void setname(const std::string &_name);
    const std::string &getname() const { return name; }
    void setupLogger(const std::string &_name, std::ostream &_out);
    void setPrintLocation(bool _print_loc) { print_loc = _print_loc; }

private:
    bool active() const;

    std::string name;
    std::ostream *out;
    loglevel_t curlevel;
    bool print_loc;
    boost::recursive_mutex mutex;
    bool locked;
};


template<typename T>
void logger::print(const T &arg)
{
    BB_ASSERT(active());

    (*out) << arg;
}

template<typename T>
logger& operator << (logger& log, const T& arg){
    log.print<T>(arg);
    return log;
}

inline logger& operator << (logger& log, logger::manip m) {
    return m(log, 0);
}


/// End-of-line stream operator.  ICC doesn't accept the std::endl
/// we've defined, so we have to have our own.  For compatibility
/// (and convenience), we still preserve the GCC supported
/// std::endl.
inline logger &endl(logger &l, int) { return l.endl(); }

} // namespace bb


namespace std {

/// End-of-line stream operator.  See bb::endl.
inline bb::logger &endl(bb::logger &l, int) { return l.endl(); }

} // namespace std


#endif // BB_CORE_LOGGER_H
