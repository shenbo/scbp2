#ifndef BB_CORE_OPTION_H
#define BB_CORE_OPTION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/**
 * @file option.h
 *
 * Functions for dealing with options.  As of May 2010, the OSI Symbology
 * Initiative is complete, so OPRA roots are deprecated.  There is no longer a
 * need for option.table.  It's still not as trivial as converting the symbol to
 * a symbol_t, though, because of special cases, like FLEX options.
 */

#include <bb/core/symbol.h>

namespace bb {

/// Takes an OPRA root and returns the underlying symbol_t
symbol_t option2sym(const std::string& str);

} // namespace bb

#endif // BB_CORE_OPTION_H
