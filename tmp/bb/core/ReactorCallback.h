#ifndef BB_CORE_REACTORCALLBACK_H
#define BB_CORE_REACTORCALLBACK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>

namespace bb {

typedef boost::function<void ()> ReactorCallback;

} // namespace bb

#endif // BB_CORE_REACTORCALLBACK_H
