#ifndef BB_CORE_SAFEVALUE_H
#define BB_CORE_SAFEVALUE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/*
 *
 * #include <bb/safevalue.h>
 * */
#include <bb/core/relocatablecheck.h>
#include <ostream>
#include <limits>
#include <bb/core/hash.h>
#include <boost/type_traits.hpp>
#include <boost/static_assert.hpp>

namespace bb{

///
/// A template that makes a type safe value storer,
///
///
template<class T>
struct ValueTypeSpecifier{ typedef T value_type;};
template<class T, class Cons=ValueTypeSpecifier<T> >
struct ExactComparators:Cons{

    static bool greater_than(T l,T r){return l>r;}
    static bool less_than(T l,T r){return l<r;}
    static bool equal_to(T l,T r){return l==r;}
};
template<bool IsExact>struct ComparatorSelectorImpl{
//nothing yet for inexact comparisions
};
template<>struct ComparatorSelectorImpl<true>{//is_exact === true
    template<class Y, class Cons>struct rebind{
        typedef ExactComparators<Y,Cons> comparator_type;
    };
};
template<class T,class Cons=ValueTypeSpecifier<T> >struct DefaultComparator
:ComparatorSelectorImpl< std::numeric_limits<T>::is_exact>::template rebind<T,Cons>::comparator_type
{
};

template<class T, T default_val, class Cons=ValueTypeSpecifier<T> >
struct IntegralInitializer:Cons{

    static T get_default(){return T(default_val);}
};

template<class T,  class Cons=ValueTypeSpecifier<T> >
struct BuiltInInitializer:Cons{

    static T get_default(){return T();}
};


template<bool IsInteger>struct InitializerSelectorImpl{
//nothing yet for noninteger initializer
//    static T get_default(){return T(default_val);}


};
template<>struct InitializerSelectorImpl<true>{//is_exact === true
    template<class Y, class Cons>struct rebind{
        typedef IntegralInitializer<Y, 0, Cons> initializer_type;
    };
};
template<class T, class Cons=ValueTypeSpecifier<T> >
struct DefaultInitializer
:InitializerSelectorImpl<std::numeric_limits<T>::is_integer>::template rebind<T,Cons>::initializer_type
{

};

template<class T, class Cons=ValueTypeSpecifier<T> >
struct DefaultTest:Cons{
        typedef typename Cons::value_type value_type;
        static value_type narrow(value_type _v){ return _v;}
};
template<class T>
struct SafeFundamentalDefaultPolicy:DefaultTest<T,DefaultInitializer<T,DefaultComparator<T> > >{};

template<>
struct SafeFundamentalDefaultPolicy<void*>
:DefaultTest<void*,BuiltInInitializer<void*,ExactComparators<void*> > >{};
/*

struct POLICY_TYPE{

        typedef STORAGE_TYPE value_type;
        static/const value_type narrow(value_type _v){ return _vdd;}//test
        static/const value_type get_default(){return default_val;}
        static/const bool less_than(T l,T r){return l<r;}
        static/const bool equal_to(T l,T r){return l==r;}
};



*/

template <class POLICY_TYPE>
class  SafeValue:protected POLICY_TYPE
{
public:
    typedef     POLICY_TYPE policy_type;                                    ///< the policy
    typedef SafeValue < POLICY_TYPE  > my_type;
//    static const bool is_exact= true;

    typedef typename policy_type::value_type value_type;
 //   typedef typename policy_type::test_type test_type;
private:
    value_type m_val;
//    struct BoolConverter {int dummy;};

public:

    /// constructor, with a value provided
    /// @param val the new value
    explicit SafeValue(value_type val, policy_type const&p=policy_type())
    :policy_type(p),m_val(this->narrow(val)){
        reset(val);

    }
    ///  default constructor
    explicit SafeValue(policy_type const&p=policy_type()) :policy_type(p),m_val(policy_type::get_default())
    {}
    /// default copy    constructor assignment operator OK
    value_type get()const{return m_val;}
    void reset(value_type _a){
             m_val=this->narrow(_a);
    }
    void reset(){
        m_val=this->get_default();
    }
    bool operator==(SafeValue _r) const
    {   return this->equal_to(m_val, _r.m_val);   }
    bool operator!=(SafeValue _r) const
    {   return this->equal_to(m_val, _r.m_val)==false;   }
    bool operator<(SafeValue _r) const
    {   return this->less_than(m_val, _r.m_val);   }
    bool operator>(SafeValue _r) const
    {   return this->greater_than(m_val, _r.m_val);   }


};




/// the stream insertion operator
template<class charT, class Traits, typename POLICY_TYPE>
std::basic_ostream< charT ,Traits > &
operator<<(std::basic_ostream< charT ,Traits >&_m_stream ,
        SafeValue<POLICY_TYPE> const &val)
{
    return _m_stream<<(val.get());
}



template<class POLICY_TYPE> struct declare_relocatable<SafeValue<POLICY_TYPE> >{

    enum{value = 1
        && is_relocatable<POLICY_TYPE>::value
        && is_relocatable<typename SafeValue< POLICY_TYPE>::value_type >::value
    };

};






}//bb

BB_HASH_NAMESPACE_BEGIN {
template<class T > struct hash<bb::SafeValue<T> >
{
private:
    typedef  bb::SafeValue<T> valtype;
    BOOST_STATIC_ASSERT(numeric_limits<typename valtype::value_type>::is_exact);

public:
    size_t operator()(  valtype  val ) const
    {
        return val.get();
    }
};
} BB_HASH_NAMESPACE_END

#endif // BB_CORE_SAFEVALUE_H
