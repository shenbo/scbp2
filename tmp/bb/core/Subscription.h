#ifndef BB_CORE_SUBSCRIPTION_H
#define BB_CORE_SUBSCRIPTION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <boost/function.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/noncopyable.hpp>

namespace bb {

class SubscriptionBase;
typedef boost::intrusive_ptr<SubscriptionBase> SubscriptionBasePtr;


/** A generic subscription in the listener pattern. A subscribable object will
 *  return its callers an instance of Subscription, which behaves like a shared_ptr.
 *
 *  When this object gets destroyed, the subscription expires.
 */
class Subscription
{
public:
    // in some implementations of Subscription a callback can be added to
    // get notified if and when this Subscription is invalidated
    // of course the callback must have a longer lifetime than object
    // managed by this Subscription
    typedef boost::function<void()> Callback;
    typedef bool (Subscription::*unspecified_bool_type)() const;

    Subscription() {}

    void reset() { m_base.reset(); }

    bool isValid() const { return static_cast<bool>( *this ); }
    bool operator !() const { return !isValid(); }
    operator unspecified_bool_type() const;

    explicit Subscription( SubscriptionBase *base ) : m_base( base ) {}
    void reset( SubscriptionBase *rhs ) { m_base.reset( rhs ); }
    SubscriptionBase *getBase() const { return m_base.get(); }
    int use_count() const; // to aid in debugging

protected:
    SubscriptionBasePtr m_base;
};


/** Base class for the implementers of the Subscription pattern.
 *
 *  Any subscribable object makes a subclass of this to implement its own subscription
 *  behavior; held & ref-counted by Subscription.
 */
class SubscriptionBase : public boost::noncopyable
{
public:
    virtual ~SubscriptionBase() {}
    virtual bool isValid() const = 0;
    friend inline void intrusive_ptr_add_ref( const SubscriptionBase *e )
        { ++e->m_refCount; }
    friend inline void intrusive_ptr_release( const SubscriptionBase *e )
        { if( --e->m_refCount == 0 ) delete e; }
    int use_count() const { return m_refCount; }

protected:
    SubscriptionBase() : m_refCount(0) {}
    mutable int m_refCount;
};

inline int Subscription::use_count() const
{
    if( m_base )
        return m_base->use_count();

    return 0;
}


/** Use this interface when an object adds a subscription on behalf of another object
 * e.g. Inside an accumulator
 *
 * struct Special:trading:IOrderStatusListener,ISubscriptionKeeper{};
 * struct MyAccumulator{
 *     //add listener and SubKeeper, dynamic_pointer_cast if SubscriptionKeeper is defaulted
 *     void registerOrderListener(IOrderStatusListenerPtr const&,ISubscriptionKeeperPtr const&=ISubscriptionKeeperPtr());
 *     void createOrder(){
 *       for_each(Subscriber)
 *           Subscription sub;
 *           order->listeners().subscribe(sub,Subscriber.getListener().get());
 *           Subscriber.getSubscriptionKeeper()->push_back(sub);
 *     }
 * }
 * MyAccumulator acc;
 * SpecialPtr sp;
 * acc.registerOrderListener(sp);
 * */

struct ISubscriptionKeeper
{
    virtual ~ISubscriptionKeeper() {}
    virtual void push_back( Subscription const& ) = 0;
};

struct SubscriptionKeeper : virtual ISubscriptionKeeper
{
    void push_back( Subscription const& s ) { m_subs.push_back( s ); }

protected:
    void clear() { m_subs.clear(); }

private:
    std::vector<Subscription> m_subs;
};

inline Subscription::operator unspecified_bool_type() const
{
    return ( m_base && m_base->isValid() ) ? &Subscription::isValid : 0;
}

} // namespace bb

#endif // BB_CORE_SUBSCRIPTION_H
