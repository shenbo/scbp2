#ifndef BB_CORE_LOG_H
#define BB_CORE_LOG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/** \file Provides some handy macros for verbose level controlled logging.
          Call setLogger() to make a logger.
*/

#include <tbb/atomic.h>
#include <bb/core/logger.h>
#include <bb/core/compat.h> // for SRC_LOCATION
#include <bb/core/loglevel.h> //for L_WARN, L_PANIC, etc

namespace bb {

/// Sets the name & output stream for the global, system-owned logger.
/// The name is printed on all LOG_PANIC/LOG_WARN-based logging.
///
/// This does not affect the IAlert-based (clientcore) alerting!
/// See Alert::setIdentity instead.
void setLogger( const char *name );
void setLogger( const char *name, std::ostream &_out );

// Don't play with details...
namespace detail
{
const std::string DEFAULT_LOGGER_NAME = "default_log";
extern tbb::atomic<logger*> g_logger;
logger *initDefaultLogger();
bool isDefaultLogger();
} // namespace detail

/// Retrieves the current logger.
inline bb::logger* getLogger()
{
    logger *l = detail::g_logger;
    if(unlikely(!l))
    {
        // double-checked locking -- memory-barriers from tbb::atomic should
        // make this  safe, see "C++ and the Perils of Double-Checked Locking"
        l = detail::initDefaultLogger();
    }
    return l;
}

inline bool isDefaultLogger()
{
    const logger *l = getLogger();
    return l->getname() == detail::DEFAULT_LOGGER_NAME;
}



} // namespace bb

/// Used to output a message at a certain verbosity level.  Like:
/// hout(LOG_PANIC) << "this is a panic message" << std::endl;
#define hout(LEVEL, _location) (*bb::getLogger())(LEVEL, _location)

/// Predefined verbosity level.
#define LOG_PANIC hout(bb::L_PANIC, SRC_LOCATION)
/// Predefined verbosity level.
#define LOG_ERROR hout(bb::L_ERROR, SRC_LOCATION)
/// Predefined verbosity level.
#define LOG_WARN hout(bb::L_WARN, SRC_LOCATION)
/// Predefined verbosity level.
#define LOG_INFO hout(bb::L_INFO, SRC_LOCATION)

/// Debug Level logging will only show in debug build, not optimal build
#ifndef NDEBUG
#define LOG_DEBUG(xline) \
        hout(bb::L_DEBUG, SRC_LOCATION) << xline << bb::endl
#define COND_LOG_DEBUG(expr, xline) \
        if(expr) LOG_DEBUG (xline)
#else
#define COND_LOG_DEBUG(expr, xline) (void)0
#define LOG_DEBUG(xline) (void)0
#endif //NDEBUG


//example:
//THROTTLED_LOG(INFO,  2, timeval_t::now().sec() << " info log 2 " );
//THROTTLED_LOG(INFO,  3, timeval_t::now().sec() << " info log 3 " );
//THROTTLED_LOG(WARN,  4, timeval_t::now().sec() << " WARN log 4 " );

#define THROTTLED_LOG( lvl, intvl, _error_stream_ )       \
    do {                                                   \
        static bb::timeval_t sLastLogTime##lvl##intvl;   \
        const bb::timeval_t now = bb::timeval_t::now;     \
        if( now.sec() >= (sLastLogTime##lvl##intvl.sec() + intvl) )   \
        {                                                 \
            sLastLogTime##lvl##intvl = now;               \
            std::ostringstream _error_ss;                 \
            _error_ss << _error_stream_;                  \
            LOG_##lvl << _error_ss.str() << bb::endl;     \
        }                                                 \
    }while( false )                                     \



#endif // BB_CORE_LOG_H
