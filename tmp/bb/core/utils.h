#ifndef BB_CORE_UTILS_H
#define BB_CORE_UTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// please do not add more "miscellaneous" things in this file, think of good names for them elsewhere

#include <boost/static_assert.hpp>

#include <bb/core/compat.h>

namespace bb {

/// given an array (not a pointer!), get its size as number of elements
// note that 0[x] is like x[0] but fails to compile for user-defined types having operator[] (this is safer)
#define DIM(x) (sizeof(x)/sizeof(0[x]))


/// Message Packing
#define BB_PACK( ENUM ) \
    _attr_packed_; \
    BOOST_STATIC_ASSERT(sizeof( ENUM ) == 1)

} // namespace bb

#endif // BB_CORE_UTILS_H
