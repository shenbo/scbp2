#ifndef BB_CORE_INSTRSOURCE_H
#define BB_CORE_INSTRSOURCE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash.h>
#include <bb/core/instrument.h>
#include <bb/core/source.h>


namespace bb {

/// Instrument-Source Pair is a commonly used type.
typedef std::pair<instrument_t, source_t>   InstrSource;

} // namespace bb


BB_HASH_NAMESPACE_BEGIN {
template<> struct hash<bb::InstrSource> : public pair_hash<bb::instrument_t, bb::source_t> { };
} BB_HASH_NAMESPACE_END

#endif // BB_CORE_INSTRSOURCE_H
