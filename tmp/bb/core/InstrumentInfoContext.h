#ifndef BB_CORE_INSTRUMENTINFOCONTEXT_H
#define BB_CORE_INSTRUMENTINFOCONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/mapdb.h>
#include <bb/core/mktdest.h>
#include <bb/core/product.h>
#include <bb/core/smart_ptr.h>

namespace bb {

class instrument_t;
BB_FWD_DECLARE_SHARED_PTR(InstrumentInfoContext);
BB_FWD_DECLARE_SHARED_PTR(ISymbolContext);

namespace detail {

/// SymbolMapPair is a simple container class for a symbol-value pair, primarily
/// used for fast lookups via symbol_t ordering using a sorted vector.
template<class ValueType>
class SymbolMapPair
{
public:
    typedef symbol_t key_type;
    typedef ValueType value_type;

    SymbolMapPair(const key_type& sym, const value_type& val)
        : m_symbol(sym)
        , m_val(val)
    {
    }

    bool operator<(const SymbolMapPair& right) const
    {
        return m_symbol < right.m_symbol;
    }

    bool operator<(const key_type& right) const
    {
        return m_symbol < right;
    }

    friend bool operator<(const key_type& sym, const SymbolMapPair& right)
    {
        return sym < right.m_symbol;
    }

    inline key_type getSymbol() const
    {
        return m_symbol;
    }

    inline value_type getValue() const
    {
        return m_val;
    }

private:
    key_type m_symbol;
    value_type m_val;
};

typedef std::vector<SymbolMapPair<product_t> > product_map_t;
typedef std::vector<SymbolMapPair<mktdest_t> > mktdest_map_t;

} // namespace detail

class InstrumentInfoContext
{
public:
    /// Constructor
    InstrumentInfoContext(const ISymbolContextPtr& symbol_context);

    /// Returns the product_t corresponding to the specified symbol, if found,
    /// otherwise, returns PROD_STOCK
    product_t sym2product(symbol_t sym) const;

    /// Returns the mktdest_t corresponding to the specified symbol, if found,
    /// otherwise, returns MKT_UNKNOWN
    mktdest_t sym2mktdest(symbol_t sym) const;

private:
    /// Parses all the symbols in the symbol context to create two tables for
    /// mapping a symbol_t to the product type and the associated market
    void initialize(const ISymbolContextPtr& symbol_context);

private:
    /// Keeps track of our symbol_t to product_t mappings, guessed from the
    /// string representation
    detail::product_map_t m_symbol_product_map;

    /// Keeps track of our symbol_t to mktdest_t mappings, guessed from the
    /// string representation
    detail::mktdest_map_t m_symbol_market_map;
};

} // namespace bb

#endif // BB_CORE_INSTRUMENTINFOCONTEXT_H
