#ifndef BB_CORE_PERSISTENTSEQUENCENUMBER_H
#define BB_CORE_PERSISTENTSEQUENCENUMBER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <tbb/atomic.h>

#include <bb/core/bbassert.h>
#include <bb/core/MMap.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/PersistentNumber.h>

namespace bb {

/// A sequence number that persists across application instances
///
/// The sequence number is stored in <filename>
class PersistentSequenceNumber : public PersistentNumberBase<uint32_t>
{
    typedef uint32_t value_type;
    typedef tbb::atomic<value_type> atomic_type;
    typedef shared_ptr<atomic_type> storage_type;

public:
    PersistentSequenceNumber() {}
    PersistentSequenceNumber( const std::string& filename, bool concurrent = false )
        : value( open( filename, concurrent ) )
    {
        BB_ASSERT( value != NULL );
    }

    virtual void initialize_seq_num( const std::string& filename )
    {
        init( filename, false );
    }

    void init( const std::string& filename, bool concurrent )
    {
        value.reset(); // close & unlock the old file before opening the new one.
        value = open( filename, concurrent );
        BB_ASSERT( value != NULL );
    }

    // NOT thread-safe
    // the incrementing itself is atomic. But by the time the caller tries to get the current value,
    // it could have been incremented again by other threads.
    virtual PersistentSequenceNumber& operator++()
    {
        BB_ASSERT( value != NULL );
        ++*value;
        return *this;
    }

    // thread-safe
    value_type fetch_and_add( const value_type& v )
    {
        BB_ASSERT( value );
        return value->fetch_and_add( v );
    }

    virtual operator value_type() const
    {
        return *value;
    }

    virtual void set( value_type v )
    {
        *value = v;
    }

    /// returns true if this sequence number has been init()ed with a persistent file.
    bool isValid() const { return (bool) value; }

private:
    storage_type open( const std::string& filename, bool concurrent )
    {
        return openMMap<atomic_type>( open_or_create, filename,
                                      concurrent ? MMAP_LOCK_SHARED_NB : MMAP_LOCK_EXCLUSIVE_NB ).first;
    }

    storage_type value;
};

BB_DECLARE_SHARED_PTR( PersistentSequenceNumber );

} // namespace bb

#endif // BB_CORE_PERSISTENTSEQUENCENUMBER_H
