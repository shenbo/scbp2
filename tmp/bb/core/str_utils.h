#ifndef BB_CORE_STR_UTILS_H
#define BB_CORE_STR_UTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstring>

#include <bb/core/cstr_array.h>

namespace bb {

/// Provides same functionality as strdup, except uses C++ new
/// operator for memory allocation.  If S is NULL, it returns
/// NULL.
inline char *strdup2(const char *s)
{
    if (s == NULL)
        return NULL;

    size_t len = strlen(s);
    char *res = new char[len + 1];
    memcpy(res, s, len);
    res[len] = '\0';
    return res;
}

/// Trims a string of trailing blanks
template<size_t len>
char* strtrim_blanks(char *s)
{
    for(int i = len - 1; i >= 0 && (s[i] == ' '); --i)
        s[i] = '\0';
    return s;
}

template<size_t len>
void strtrim_blanks(cstr_array<len> &s)
{
    strtrim_blanks<len>(s.c_array());
}

/// Trims a string of trailing spaces
///  Note: If only triming blanks " " then
///        use strtrim_blanks which is faster
template<size_t len>
char* strtrim(char *s)
{
    for(int i = len - 1; i >= 0 && isspace(s[i]); --i)
        s[i] = '\0';
    return s;
}

template<size_t len>
void strtrim(cstr_array<len> &s)
{
    strtrim<len>(s.c_array());
}

/// Trims a string of trailing spaces
template<size_t len>
char* strtrim(unsigned char *s)
{
    return strtrim<len>(reinterpret_cast<char*>(s));
}

}

#endif // BB_CORE_STR_UTILS_H
