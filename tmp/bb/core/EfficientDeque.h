#ifndef BB_CORE_EFFICIENTDEQUE_H
#define BB_CORE_EFFICIENTDEQUE_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/circular_buffer.hpp>

namespace bb {

// Strictly speaking not a deque, but if it lets you (push_)front(), (push_)back(), clear() and empty()
// any number of elements, just like a deque, it's probably a deque. I give you .. dequetyping!
//
// Be aware:
// - unlike deques, iterators will become invalid if we grow too much
// - unlike deques, we will at some point take a performance hit when we have to resize
template<typename T>
struct EfficientDeque : public boost::circular_buffer_space_optimized<T>
{
    EfficientDeque(int initial_capacity = 10)
    {
        boost::circular_buffer_space_optimized<T>::set_capacity(initial_capacity);
    }

    void push_front(T const & item)
    {
        resize();
        boost::circular_buffer_space_optimized<T>::push_front(item);
    }

    void push_back(T const & item)
    {
        resize();
        boost::circular_buffer_space_optimized<T>::push_back(item);
    }

    T & front()
    {
        return boost::circular_buffer_space_optimized<T>::front();
    }

    T & back()
    {
        return boost::circular_buffer_space_optimized<T>::back();
    }

    void clear()
    {
        boost::circular_buffer_space_optimized<T>::clear();
    }

    bool empty() const
    {
        return boost::circular_buffer_space_optimized<T>::empty();
    }

    typename boost::circular_buffer_space_optimized<T>::size_type size() const
    {
        return boost::circular_buffer_space_optimized<T>::size();
    }

    typename boost::circular_buffer_space_optimized<T>::iterator begin()
    {
        return boost::circular_buffer_space_optimized<T>::begin();
    }

    typename boost::circular_buffer_space_optimized<T>::iterator end()
    {
        return boost::circular_buffer_space_optimized<T>::end();
    }

    typename boost::circular_buffer_space_optimized<T>::const_iterator begin() const
    {
        return boost::circular_buffer_space_optimized<T>::begin();
    }

    typename boost::circular_buffer_space_optimized<T>::const_iterator end() const
    {
        return boost::circular_buffer_space_optimized<T>::end();
    }
private:

    void resize()
    {
        if ( unlikely ( size() > boost::circular_buffer_space_optimized<T>::capacity() - 1 ) )
        {
            boost::circular_buffer_space_optimized<T>::set_capacity( static_cast<typename boost::circular_buffer_space_optimized<T>::size_type> ( static_cast<double> ( boost::circular_buffer_space_optimized<T>::capacity() ) * 1.7 ) );
        }
    }
};

}

#endif // BB_CORE_EFFICIENTDEQUE_H
