#ifndef BB_CORE_TIMEZONE_H
#define BB_CORE_TIMEZONE_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <inttypes.h>
#include <boost/date_time/local_time/local_time.hpp>
#include <bb/core/timeval.h>
#include <bb/core/ptime.h>

namespace bb {

class timezone_not_found_error : public std::runtime_error
{
public:
    timezone_not_found_error(std::string const & tz);
};

class timezone_file_not_found_error : public std::runtime_error
{
public:
    timezone_file_not_found_error(std::string const & tz);
};

class timezone_file_incorrect_error : public std::runtime_error
{
public:
    timezone_file_incorrect_error(std::string const & tz);
};

class date_t;

class timezone_t
{
public:
    static const timezone_t EST;
    static const timezone_t EDT;
    static const timezone_t HKT;
    static const timezone_t UTC;

    struct current {};
    struct America
    {
        static const timezone_t NYC;
    };

    explicit timezone_t( int32_t offset );
    explicit timezone_t( const std::string& tzspec, const char * custom_zonespec = nullptr );
    timezone_t();
    timezone_t( const timezone_t::current& );

    const timezone_t operator+ ( int32_t offset ) const;
    const timezone_t operator- ( int32_t offset ) const;


// private:
    int32_t m_offset;
    boost::local_time::time_zone_ptr zone;
    boost::local_time::tz_database tz_db;
};

// returns UTC time where local_time is in the timezone given by tz
ptime_t make_utc_time( const ptime_t& local_time, const timezone_t& tz );
timeval_t make_utc_time( const timeval_t& local_time, const timezone_t& tz );

ptime_t convert_time( const timezone_t& tz_from
                      , const timezone_t& tz_to, const ptime_t& pt );
timeval_t convert_time_easy( const std::string& tz_from_str
                             , const std::string& tz_to_str, const timeval_t& from_time
                             , const date_t& date );

// raw ptime to timeval conversions - i.e. no local time effects
timeval_t timeval_from_ptime_raw( const ptime_t& pt );
ptime_t ptime_from_timeval_raw( const timeval_t& tv );
timeval_t utc_timeval_from_localdatetime( const boost::local_time::local_date_time& ldt );
timeval_t utc_timeval_from_datetime( const date_t& d, int32_t seconds, const timezone_t& tz );

}

#endif // BB_CORE_TIMEZONE_H
