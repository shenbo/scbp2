#ifndef BB_CORE_SERIALIZATIONUTILS_H
#define BB_CORE_SERIALIZATIONUTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/logic/tribool.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_free.hpp>
#include <bb/core/timeval.h>
#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/core/tid.h>

// Some common boost::serialization definitions

namespace boost {
namespace serialization {

template<class Archive>
void save(Archive & ar, const boost::logic::tribool &b, const unsigned int version)
{
    unsigned char val;
    if(b)       val = 1;
    else if(!b) val = 0;
    else        val = 2;
    ar & val;
}

template<class Archive>
void load(Archive & ar, boost::logic::tribool &b, const unsigned int version)
{
    unsigned char val;
    ar & val;
    switch(val)
    {
    case 1: b = true; break;
    case 0: b = false; break;
    default: b = boost::indeterminate; break;
    }
}

template<class Archive>
void save(Archive & ar, const bb::timeval_t &tv, const unsigned int version)
{
    int32_t sec = tv.sec(), usec = tv.usec();
    ar & sec;
    ar & usec;
}

template<class Archive>
void load(Archive & ar, bb::timeval_t &tv, const unsigned int version)
{
    int32_t sec, usec;
    ar & sec;
    ar & usec;
    tv = bb::timeval_t(sec, usec);
}

template<class Archive>
void save(Archive & ar, const bb::source_t &src, const unsigned int version)
{
    uint32_t srcId = src.id();
    ar & srcId;
}

template<class Archive>
void load(Archive & ar, bb::source_t &src, const unsigned int version)
{
    uint32_t srcId;
    ar & srcId;
    src = bb::source_t(srcId);
}

template<class Archive>
void save(Archive & ar, const bb::symbol_t &sym, const unsigned int version)
{
    uint32_t symId = sym.id();
    ar & symId;
}

template<class Archive>
void load(Archive & ar, bb::symbol_t &sym, const unsigned int version)
{
    uint32_t symId;
    ar & symId;
    sym = bb::symbol_t(symId);
}

template<class Archive>
void save(Archive & ar, const bb::acct_t &acct, const unsigned int version)
{
    uint32_t acctId = acct.id();
    ar & acctId;
}

template<class Archive>
void load(Archive & ar, bb::acct_t &acct, const unsigned int version)
{
    uint32_t acctId;
    ar & acctId;
    acct = bb::acct_t(acctId);
}

template<class Archive>
void save(Archive & ar, const bb::currency_t &curr, const unsigned int version)
{
    uint32_t currId = curr.id();
    ar & currId;
}

template<class Archive>
void load(Archive & ar, bb::currency_t &curr, const unsigned int version)
{
    uint32_t currId;
    ar & currId;
    curr = bb::currency_t(currId);
}

template<class Archive>
void save(Archive & ar, const bb::expiry_t &exp, const unsigned int version)
{
    uint32_t expId = exp.id();
    ar & expId;
}

template<class Archive>
void load(Archive & ar, bb::expiry_t &exp, const unsigned int version)
{
    uint32_t expId;
    ar & expId;
    exp = bb::expiry_t(expId);
}

template<class Archive>
void serialize(Archive & ar, bb::tid_t &tid, const unsigned int version)
{
    ar & tid.acct;
    ar & tid.orderid;
}

template<typename Archive, typename T1, typename T2>
void serialize(Archive & ar, boost::tuple<T1, T2> & t, const unsigned int)
{
    ar & boost::get<0>(t);
    ar & boost::get<1>(t);
}

template<class Archive>
void serialize(Archive & ar, bb::instrument_t &instr, const unsigned int version)
{
    ar & instr.sym;
    ar & instr.mkt;
    ar & instr.prod;
    ar & instr.exp;
    ar & instr.strike;
    ar & instr.right;
    ar & instr.currency;
    ar & instr.pad; // serialization knows how to deal with this
}


} // namespace serialization
} // namespace boost

BOOST_SERIALIZATION_SPLIT_FREE(boost::logic::tribool);
BOOST_SERIALIZATION_SPLIT_FREE(bb::symbol_t);
BOOST_SERIALIZATION_SPLIT_FREE(bb::source_t);
BOOST_SERIALIZATION_SPLIT_FREE(bb::acct_t);
BOOST_SERIALIZATION_SPLIT_FREE(bb::timeval_t);
BOOST_SERIALIZATION_SPLIT_FREE(bb::currency_t);
BOOST_SERIALIZATION_SPLIT_FREE(bb::expiry_t);

#endif // BB_CORE_SERIALIZATIONUTILS_H
