#ifndef BB_CORE_QUEUE_THREADPIPENOTIFIER_H
#define BB_CORE_QUEUE_THREADPIPENOTIFIER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/SelectDispatcher.h>
#include <bb/core/ThreadPipe.h>
#include <boost/thread/mutex.hpp>

namespace bb {
namespace queue {

class notify_pred;

/** A notifier based on a ThreadPipe object for use with bb::queue::waitable_queue
 *
 * If wait() is used, a SelectDispatcher is created which waits on a single fd to
 * simply consume the signal and return control to the calling method.
 *
 * Alternatively, the user can obtain a ThreadPipeReader and use his/her
 * own SelectDispatcher to wait on the event.  makeReaderCB() is a helper
 * function to wrap a user callback that will automatically consume the
 * notification and invoke the user callback.  Otherwise, the caller has
 * to explicitly call getReader().consumeWakeup() or the notification
 * will continue to signal
 *
 * Note: because waitable_queue is a template, you can access all methods
 * of the notifier
 *
 * @code
 * waitable_queue<long,ThreadPipeNotifier> q;
 * SelectDispatcher dispatch;
 *
 * // ... initialize SelectDispatcher
 *
 * bb::Subscription m_msgSub;
 * q.registerDispatch( dispatch, m_msgSub, boost::bind( &ThisClass::onEvent, this ) );
 * @endcode
 *
 */
class ThreadPipeNotifier
{
public:
    class AutoConsumeCallback
    {
    public:
        AutoConsumeCallback( ThreadPipeReader& _r, const boost::function0<void>& _cb )
            : r( _r ), cb( _cb ) {}
        void operator() ()
        {
            r.consumeWakeup();
            cb();
        }
    private:
        ThreadPipeReader& r;
        boost::function0<void> cb;
    };

public:
    ThreadPipeNotifier( notify_pred&, boost::mutex& )
    {
        makeThreadPipe( reader, writer );
    }

    void notify_one()
    {
        writer.sendWakeup( 'a' );
    }

    ThreadPipeReader& getReader() { return reader; }

    AutoConsumeCallback makeReaderCB( const boost::function0<void>& _cb )
    {
        return AutoConsumeCallback( reader, _cb );
    }

    void registerDispatch( FDSet& sd, Subscription& ref, const boost::function0<void>& _cb )
    {
        sd.createReadCB( ref, reader.getFDPtr(), makeReaderCB( _cb ) );
    }

protected:
    ThreadPipeWriter& getWriter() { return writer; }

private:
    void onRead()
    {
        reader.consumeWakeup();
    }

private:
    ThreadPipeReader reader;
    ThreadPipeWriter writer;
};

} // namespace bb::queue
} // namespace bb

#endif // BB_CORE_QUEUE_THREADPIPENOTIFIER_H
