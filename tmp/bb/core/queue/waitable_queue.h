#ifndef BB_CORE_QUEUE_WAITABLE_QUEUE_H
#define BB_CORE_QUEUE_WAITABLE_QUEUE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/thread/condition.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

#include <tbb/concurrent_queue.h>

#include <bb/core/queue/locked_queue.h>
#include <bb/core/timeval.h>
#include <bb/core/EventFD.h>

namespace bb {
namespace queue {

///
/// Notification predicate, returns true if there is really something in the queue
///
class notify_pred
{
public:
    virtual ~notify_pred() {}
    virtual bool operator() () = 0;
};

///
/// Notify through a condition variable and takes a predicate to determine
/// whether the wakeup indicates newly available data.
///
class condition_notifier
{
public:
    condition_notifier( notify_pred& _p, boost::mutex& _m ) : p( _p ), m( _m ) {}

    bool wait( const timeval_t& tv = timeval_t(0,0) )
    {
        using namespace boost::posix_time;
        using namespace boost;
        boost::mutex::scoped_lock l( m );
        if( tv == timeval_t(0,0) )
        {
            while( !p() ) cond.wait( l );
        }
        else
        {
            system_time abs_time = get_system_time() + seconds( tv.sec() ) + microseconds( tv.usec() );
            while( !p() )
            {
                if( !cond.timed_wait( l, abs_time ) )
                    return false;
            }
        }
        return true;
    }
    void notify_one()
    {
        cond.notify_one();
    }

private:
    notify_pred& p;
    boost::mutex& m;
    boost::condition cond;
};

///
/// Notify through an EventFD
/// The user is required to register for callbacks from an FDSet with getFD()
///
class eventfd_notifier
{
public:
    eventfd_notifier( notify_pred&, boost::mutex& ) {}

    void notify_one()
    {
        m_eventFD.write();
    }

    fd_t getFD()
    {
        return m_eventFD.getFD();
    }

    eventfd_t read()
    {
        return m_eventFD.read();
    }

private:
    EventFD m_eventFD;
};

/** A queue that can be waited on.
 *  the reason this notifier is a template is just to squeeze that little
 *  bit of runtime performance out of this thing and at the same time allow
 *  the flexibility to choose notifier mechanisms.  The default notifier uses
 * a condition variable. (see condition_notifier)
 *
 * @code
 * waitable_queue< long > ConditionNotifyQueue;
 * while( queue->wait() )
 * {
 *     long result;
 *     while( queue->pop( &result ) )
 *     {
 *         do_something( result );
 *     }
 * }
 * @endcode
 *
 */
template<typename N = node_type<long>*, typename NOTIFIER_T = condition_notifier>
class waitable_queue : protected locked_queue<N>
{
protected:
    typedef locked_queue<N> queue_type;

public:
    typedef N node_t;

    waitable_queue()
        : p( *this )
        , n( p, queue_type::m ) {}

    void push( const node_t& node )
    {
        queue_type::push( node );
        n.notify_one();
    }
    bool wait() { return n.wait(); }
    node_t pop() { return queue_type::pop(); }
    uint32_t pop( node_t* n ) { return queue_type::pop( n ); }
    NOTIFIER_T& getNotifier() { return n; }

protected:
    class queue_pred : public notify_pred
    {
    public:
        queue_pred( waitable_queue& _q ) : q( _q ) {}
        bool operator() () { return !q.empty(); }

    private:
        waitable_queue& q;
    };

    friend class queue_pred;

    queue_pred p;
    NOTIFIER_T n;
};

/** A queue that can be waited on, using TBB's concurrent_queue
 *  The underlying queue is TBB's concurrent_queue object that supports
 *  non-blocking concurrent inserts.
 *  the reason this notifier is a template is just to squeeze that little
 *  bit of runtime performance out of this thing and at the same time allow
 *  the flexibility to choose notifier mechanisms.  The default notifier uses
 *  a condition variable. (see condition_notifier)
 *
 * @code
 * waitable_concurrent_queue< long > ConditionNotifyQueue;
 * while( queue->wait() )
 * {
 *     long result;
 *     while( queue->pop( &result ) )
 *     {
 *         do_something( result );
 *     }
 * }
 * @endcode
 *
 */
template< typename N, typename NOTIFIER_T = bb::queue::condition_notifier >
class waitable_concurrent_queue : protected tbb::concurrent_bounded_queue<N>
{
protected:
    typedef tbb::concurrent_bounded_queue<N> queue_type;
public:
    typedef N node_t;
    using typename tbb::concurrent_bounded_queue<N>::value_type;

    waitable_concurrent_queue()
        : p( *this )
        , n( p, m ) {}

    void push( const node_t& node )
    {
        queue_type::push( node );
        n.notify_one();
    }

    void clear() { queue_type::clear(); }

    /// return false on timeout.
    bool wait( const timeval_t& tv = timeval_t(0,0) ) { return n.wait( tv ); }
//    node_t pop() { return queue_type::pop(); }
    bool pop( node_t* n ) { return queue_type::try_pop( *n ); }
    NOTIFIER_T& getNotifier() { return n; }
    size_t size() const { return queue_type::size(); }

protected:
    class queue_pred : public notify_pred
    {
    public:
        queue_pred( waitable_concurrent_queue& _q ) : q( _q ) {}
        bool operator() () { return !q.empty(); }

    private:
        waitable_concurrent_queue& q;
    };

    friend class queue_pred;

    boost::mutex m;
    queue_pred p;
    NOTIFIER_T n;
};

} // namespace bb::queue
} // namespace bb

#endif // BB_CORE_QUEUE_WAITABLE_QUEUE_H
