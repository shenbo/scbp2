#ifndef BB_CORE_AUTO_VECTOR_H
#define BB_CORE_AUTO_VECTOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



#include <vector>
#include <boost/optional.hpp>


namespace bb {

/// auto_vector<T> is an automatically resizing vector class.  It
/// acts like an associative map from unsigned integers to some type; you
/// don't have to perform bounds checking as insertions beyond it's
/// max automatically resize it. It calls the default constructor
/// of any new items it creates.
template<typename T>
class auto_vector {
public:
    typedef unsigned int key_type;
    typedef T value_type;

    /// Constructor
    auto_vector(unsigned int size = 0);
    /// Destructor.
    ~auto_vector();

    /// Returns a reference to the item stored at index I.
    /// If no item exists in index I, it is automatically
    /// created using the default constructor for T.
    /// If I is greater than size(), it automatically resizes the
    /// vector to a size of 2*I.
    T &operator [](unsigned int i);

    /// Returns a const reference to the item stored at index I.
    /// If I is greater than size(), it automatically resizes the
    /// vector to a size of 2*I.
    const T &operator [](unsigned int i) const;

    /// Returns the current max index of the vector.
    unsigned int size() const;

    /// Returns true if this index has been populated.
    bool exists(unsigned int i) const;

    /// Returns the element at index i, which must have been verified
    /// to exist by calling 'exists' above.
    T& get_existing(unsigned int i);
    const T& get_existing(unsigned int i) const;

    /// Clears out the autovector.
    void clear();

    void swap(auto_vector&);

    typedef std::vector<boost::optional<T> > underlying;

    /** auto_vector's iterators aren't exactly what you might expect
        -- they follow vector iterator semantics more closely than map
        semantics (ie, uses *p access, rather than p->first/second).
        This is most likely not what we want, but until somebody
        objects, I'm going to leave it as it is. */
    class iterator : public std::iterator<std::bidirectional_iterator_tag, value_type> {
    public:
        iterator(const iterator &i)
            : iter(i.iter), ubegin(i.ubegin), uend(i.uend)
        { }

        iterator(typename underlying::iterator i,
                  typename underlying::iterator _ubegin,
                  typename underlying::iterator _uend)
            : iter(i), ubegin(_ubegin), uend(_uend)
        {
            sync_forward();
        }

        iterator &operator ++() { iter++; sync_forward(); return *this; }
        iterator &operator --() { iter--; sync_reverse(); return *this; }
        iterator &operator ++(int) { ++iter; sync_forward(); return *this; }
        iterator &operator --(int) { --iter; sync_reverse(); return *this; }
        bool operator ==(const iterator &i) const { return i.iter == iter; }
        bool operator !=(const iterator &i) const { return !(i == *this); }

        value_type &operator*() const { return **iter; }
        value_type *operator->() const { return &(**iter); }
        /// returns the key with which this value is associated. this is a workaround
        /// for the fact that map-style iterators over std::pairs are unsupported.
        unsigned int key() const { return iter - ubegin; }

    private:
        mutable value_type value;
        typename underlying::iterator iter;
        typename underlying::iterator ubegin;
        typename underlying::iterator uend;

        void sync_forward()
        {
            while (iter != uend && !*iter)
                iter++;
        }

        void sync_reverse()
        {
            while (iter != ubegin && !*iter)
                iter--;
        }
    };

    /** See auto_vector::iterator comment. */
    class const_iterator : public std::iterator<std::bidirectional_iterator_tag, value_type> {
    public:
        const_iterator(const const_iterator &i)
            : iter(i.iter)
        { }

        const_iterator(const iterator &i)
            : iter(i)
        { }

        const_iterator &operator ++() { iter++; return *this; }
        const_iterator &operator --() { iter--; return *this; }
        const_iterator &operator ++(int) { ++iter; return *this; }
        const_iterator &operator --(int) { --iter; return *this; }
        bool operator ==(const const_iterator &i) const { return i.iter == iter; }
        bool operator !=(const const_iterator &i) const { return !(i == *this); }

        value_type &operator*() const { return *iter; }
        value_type *operator->() const { return &(*iter); }

        unsigned int key() const { return iter.key(); }

    private:
        iterator iter;
    };

    iterator begin() const { return iterator(v.begin(), v.begin(), v.end()); }
    iterator end() const { return iterator(v.end(), v.begin(), v.end()); }

    iterator find(unsigned int index) const;

    /// Removes excess storage at the end of the vector.
    void trim();

private:
    mutable underlying v;
};

template<typename T>
auto_vector<T>::auto_vector(unsigned int _size)
    : v(_size) { }

template<typename T>
auto_vector<T>::~auto_vector() {}

template<typename T>
T &auto_vector<T>::operator [](unsigned int i)
{
    if (i >= v.size())
        v.resize(i*2+1);

    //    return v[i];
    if (!v[i])
        v[i] = T();
    return *v[i];
}

template<typename T>
const T &auto_vector<T>::operator [](unsigned int i) const
{
    if (i >= v.size())
        v.resize(i*2+1);

    //    return v[i];
    if (!v[i])
        v[i] = T();
    return *v[i];
}

template<typename T>
unsigned int auto_vector<T>::size() const
{
    return v.size();
}

template<typename T>
bool auto_vector<T>::exists(unsigned int i) const
{
    return i < v.size() && v[i];
}

template<typename T>
T& auto_vector<T>::get_existing(unsigned int i)
{
    BB_ASSERT( exists(i) );
    return *v[i];
}

template<typename T>
const T& auto_vector<T>::get_existing(unsigned int i) const
{
    BB_ASSERT( exists(i) );
    return *v[i];
}

template<typename T>
void auto_vector<T>::clear()
{
    for (unsigned int i = 0; i < v.size(); i++)
        if (v[i]) {
            v[i] = boost::none;
        }
}

template<typename T>
void auto_vector<T>::swap(auto_vector<T>& other)
{
    v.swap(other.v);
}

template<typename T>
typename auto_vector<T>::iterator auto_vector<T>::find(unsigned int index) const
{
    if (exists(index)) {
        typename underlying::iterator p = v.begin();
        p += index;
        return iterator(p, v.begin(), v.end());
    }
    else
        return end();
}

template<typename T>
void auto_vector<T>::trim()
{
    v.resize(v.empty() ? 0 : (--end()).key() + 1);
}

} // namespace bb

#endif // BB_CORE_AUTO_VECTOR_H
