#ifndef BB_CORE_LUABIND_EXT_H
#define BB_CORE_LUABIND_EXT_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

// this file adds the shared_ptr extension to luabind such that you can
// do the automatic conversion between shared_ptr<T> and shared_ptr<T const>
//
// *** bb does this really strange thing where it overrides the
// *** include path so we pick a up a special luabind.hpp in bb/thirdparty
// *** ... that was INCREDIBLY hard to find.

// make sure we're not using the ACR version of luabind
#ifndef BB_THIRDPARTY_LUABIND_LUABIND_HPP

// The magic we really want to enforce on all includers of
// luabind/luabind.hpp is contained in this luabind file.
#include <luabind/shared_ptr_converter.hpp>

// Provide const-conversion correctness for boost::intrusive_ptr.
namespace luabind {
    template<class A>
    boost::intrusive_ptr<const A>*
    get_const_holder(boost::intrusive_ptr<A>*)
    { return 0; }
}

#endif // BB_THIRDPARTY_LUABIND_LUABIND_HPP

#endif // BB_CORE_LUABIND_EXT_H
