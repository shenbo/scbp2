#ifndef BB_CORE_FPMATH_H
#define BB_CORE_FPMATH_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cmath>
#include <functional>
#include <bb/core/bbint.h>

namespace bb {

    namespace internal {
    struct Epsilon;
    struct Epsilon
    {
        Epsilon( double value = 0.000001 /* 1.0e-6 */)
            : m_value ( value ) {}
        operator double() const
        {
            return m_value;
        }
        Epsilon& operator = (double value)
        {
            m_value = value;
            return *this;
        }
        static inline Epsilon& instance()
        {
            static Epsilon m_instance;
            return m_instance;
        }
        private:
            double m_value;
        public:
    };
    }

/// define simple comparison operators for doubles
static internal::Epsilon& FP_EPSILON ( internal::Epsilon::instance() );

inline bool EQ( double d1, double d2, double ep = FP_EPSILON )
{   return fabs( d1 - d2 ) < ep; }

inline bool GE( double d1, double d2, double ep = FP_EPSILON )
{   return d1 >= ( d2 - ep ); }

inline bool GT( double d1, double d2, double ep = FP_EPSILON )
{   return d1 > ( d2 + ep ); }

inline bool NE( double d1, double d2, double ep = FP_EPSILON )
{   return !EQ( d1, d2, ep ); }

inline bool LE( double d1, double d2, double ep = FP_EPSILON )
{   return !GT( d1, d2, ep ); }

inline bool LT( double d1, double d2, double ep = FP_EPSILON )
{   return !GE( d1, d2, ep ); }

// fuzzy compare to 0
inline bool EQZ( double d, double ep = FP_EPSILON )
{   return fabs( d ) < ep; }

inline bool GEZ( double d, double ep = FP_EPSILON )
{   return d >= -ep; }

inline bool GTZ( double d, double ep = FP_EPSILON )
{   return d > ep; }

inline bool NEZ( double d, double ep = FP_EPSILON )
{   return !EQZ( d, ep ); }

inline bool LEZ( double d, double ep = FP_EPSILON )
{   return !GTZ( d, ep ); }

inline bool LTZ( double d, double ep = FP_EPSILON )
{   return !GEZ( d, ep ); }

// check if value is between bounds, regardless of bounds' order
inline bool between( double value, double bound1, double bound2 )
{   return ( bound1 - value ) * ( value - bound2 ) > 0; }

inline bool inclusive_between( double value, double bound1, double bound2 )
{   return ( bound1 - value ) * ( value - bound2 ) >= 0; }

inline double round_to_precision( double v, double precision )
{   return round( v / precision ) * precision; }

inline double round_down_to_precision( double v, double precision )
{   return floor( v / precision ) * precision; }

inline double round_up_to_precision( double v, double precision )
{   return ceil( v / precision ) * precision; }

inline int32_t round_to_nearest_integer( double v )
{   return ( v > 0 ) ? ((int32_t) (v + 0.5)) : ((int32_t) (v - 0.5)); }

inline int64_t round_to_nearest_integer64( double v )
{   return ( v > 0 ) ? ((int64_t) (v + 0.5)) : ((int64_t) (v - 0.5)); }

struct FPComp : std::binary_function<double, double, bool>
{
    bool operator () (double a, double b) const { return LT(a, b); }
};

} // namespace bb

#endif // BB_CORE_FPMATH_H
