#ifndef BB_CORE_HEXDUMP_H
#define BB_CORE_HEXDUMP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <iosfwd>
#include <cassert>
namespace bb{

///
/// Hex Stream utilities
///
/// This is a way to get a pretty print of raw memory, for debugging.
/// The no-frills usage is
/// char b[8000];//some buffer
///std::cout<<bb::byte_out(b,400)<<std::endl; //print 400 bytes

struct byte_out_enums{
    enum {e_print_address = 0x01,e_print_byte_val=0x02,e_print_char=0x04};
    enum hex_dec {e_hexadecimal,e_decimal};
};

template<class charT,class Traits>
struct byte_out_templ : byte_out_enums
{
    typedef charT char_t;
    typedef Traits traits_t;
    byte_out_templ(const void* v, std::size_t _bufsize,unsigned long cols1,unsigned long opts)
                    throw() :b_(v)
                ,bufsize_(_bufsize),cols_(cols1)
                ,options_(opts)
                ,non_printable_character_replacement_('.')
                ,pointer_field_byte_field_sep_(" => ")
                ,space_(' ')
                ,byte_fmt_(e_hexadecimal)
    { };

    operator const char_t*()const    throw(){return reinterpret_cast<const char_t*const>(b_);};

    const void*        buf()        const {return b_;            };
    std::size_t        bufsize()    const {return bufsize_;    };
    std::size_t     cols()        const {return cols_;        };
    unsigned long    options()    const {return options_;    };
    char_t            space()        const {return space_;        };
    char_t            non_printable_character_replacement()        const throw()
                    {return non_printable_character_replacement_;};
    const char_t*    pointer_field_byte_field_sep()const throw(){
                    return pointer_field_byte_field_sep_;
                };
    hex_dec            byte_fmt()        const throw() {return byte_fmt_;};

private:
    const void* b_;
    std::size_t bufsize_;
    std::size_t cols_;
    unsigned long options_;
    char_t non_printable_character_replacement_;
    char_t const* pointer_field_byte_field_sep_;
    char_t     space_;
    hex_dec byte_fmt_;
};

//for now I am only doing characters, Later will do wide characters
typedef byte_out_templ<char,std::char_traits< char> > bout_out_char;
template<std::size_t COLS,unsigned int OPTIONS>
struct byte_out_templ_char_spec:bout_out_char
{
    byte_out_templ_char_spec(const void* v, std::size_t h)
                :byte_out_templ<char,std::char_traits< char> >(v,h,COLS,OPTIONS){}
};

typedef byte_out_templ_char_spec<
        16, //number of columns for values
        //display vals and character representations (no addresses)
        byte_out_enums::e_print_byte_val|byte_out_enums::e_print_char
        > byte_out;

typedef byte_out_templ_char_spec<
            16, //number of columns for values
            byte_out_enums::e_print_address        |
             byte_out_enums::e_print_byte_val    |
             byte_out_enums::e_print_char
        >    byte_out_addr;

 std::ostream&
operator<<(std::ostream& os, bout_out_char const&bout);

}//bb

#endif // BB_CORE_HEXDUMP_H
