#ifndef BB_CORE_EXPIRY_H
#define BB_CORE_EXPIRY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>

#include <bb/core/timeval.h>
#include <bb/core/hash.h>

#include <iostream>

namespace bb {

/// indicates the expiration month/year of an instrument
class expiry_t
{
public:
    /// ID of an explicitly UNKNOWN expiry_t
    static const uint32_t UNKNOWN_ID = 0;

    // If the ID is larger than the largest yearmonth, it contains a weekly information
    static const uint32_t LARGEST_YEARMONTH = 999999;
    static const uint32_t WEEKLY_FACTOR = 10;

    /// ID of an explicitly EARLIEST expiry_t (190001).
    static const uint32_t EARLIEST_ID = 190001;
    /// ID of an explicitly LATEST expiry_t (210012)
    static const uint32_t LATEST_ID = 210012;
    /// min and max for the month id
    static const uint8_t EARLIEST_MONTH_ID = 1;
    static const uint8_t LATEST_MONTH_ID = 12;
    /// min and max for the weekly id
    static const uint8_t EARLIEST_WEEK_ID = 1;
    static const uint8_t LATEST_WEEK_ID = 5;
    /// min and max for the day id
    static const uint8_t EARLIEST_DAY_ID = 1;
    static const uint8_t LATEST_DAY_ID = 31;
    /// Static member that represents an invalid expiry.
    static const expiry_t UNKNOWN;
    /// Static member that represents the earliest possible valid expiry (190001).
    static const expiry_t EARLIEST;
    /// Static member that represents the latest possible valid expiry (210012).
    static const expiry_t LATEST;

    /// The string value of an invalid expiry_t, which is "INVLID".
    static const char* kszInvalid;

    /// String length of different expiries
    static const int EXPIRY_YYYYMM_LENGTH = 6;
    static const int EXPIRY_YYYYMMDD_LENGTH = 8;
    static const int EXPIRY_YYYYMMWW_LENGTH = 8;  //e.g. 201409w1

    /// Returns the expiry_t for the next quarter-end after a given timeval.
    /// Example: Passing a timeval in Jan04 will return a Mar04 expiry.
    static expiry_t for_quarter( const timeval_t& timeval );
    /// Returns the expiry_t for the month of the given timeval.
    static expiry_t for_month( const timeval_t& timeval );

    /// Creates an invalid expiry_t
    expiry_t() : m_exp( UNKNOWN_ID )
    {}

    /// Creates an expiry_t from an integer of the form YYYYMM, YYYMMW or YYYYMMDD.
    /// Expirations go from January 1980 (198001) to Decemember 2010 (201012).
    /// This checks the validity of the argument, which you can verify with is_valid.
    /// An invalid expiry_t has an integer value of 0 and a string value of kszInvalid ("INVLID").
    explicit expiry_t( uint32_t exp_int )
    {   set( exp_int ); }

    /// Creates an expiry_t from an integer YYYY and integer MM.
    /// This checks the validity of the argument, which you can verify with is_valid.
    /// An invalid expiry_t has an integer value of 0 and a string value of kszInvalid ("INVLID").
    explicit expiry_t( uint32_t yyyy, uint32_t mm )
    {   set( yyyy*100 + mm ); }

    /// Creates an expiry_t from a string of the form "YYYYMM" or "YYYYMMwW"
    /// Creates an invalid expiry_t if the string is malformed.

#define EXPIRY_CTOR_STR                                                        \
        if( unlikely( !str ) ) m_exp = UNKNOWN_ID;                             \
                                                                               \
        unsigned int val = 0;                                                  \
        for( uint8_t i=0; i<EXPIRY_YYYYMM_LENGTH; i++)                         \
            val = val*10 + (*str++ - '0');                                     \
        switch( *str )                                                         \
        {                                                                      \
        case '\0':                                                             \
            set( val );                                                        \
            break;                                                             \
        case 'w':                                                              \
            set( val );                                                        \
            str++;                                                             \
            set_week( *str++ - '0' );                                          \
            if( unlikely( *str != '\0' ) )                                     \
                m_exp = UNKNOWN_ID;                                            \
            break;                                                             \
        default:                                                               \
            for( uint8_t i = 0;                                                \
                    i < EXPIRY_YYYYMMDD_LENGTH - EXPIRY_YYYYMM_LENGTH; ++i)    \
                val = val*10 + (*str++ - '0');                                 \
            set( val );                                                        \
            if( unlikely( *str != '\0' ) )                                     \
                m_exp = UNKNOWN_ID;                                            \
            break;                                                             \
        }
#if __GNUC__ == 4 && __GNUC_MINOR__ == 4
    // Move the definition to the .cc file (thus stopping make it inline).
    // We do this because we want to minimize the impact of ignoring
    // -Warray-bounds. In fact, it is very likely that GCC would refuse
    // to inline this large ctor body in the first place with the default
    // -finline-limit value.
    explicit expiry_t( const char *str );
#else
    explicit expiry_t( const char *str )
    {
        EXPIRY_CTOR_STR
#undef EXPIRY_CTOR_STR
    }
#endif

    static expiry_t weekly( uint32_t yyyymm, uint8_t ww );

    /// Returns the numeric value of the expiry, which is the form YYYYMM.
    /// An invalid expiry has a value of 0. This has the convenient property
    /// that you can say "if ( !expiry ) {}"  instead of "if ( !expiry.is_valid() )"
    operator unsigned int() const                 { return expid(); }

    /// Returns the numeric value of the expiry, which is the form YYYYMM.
    uint32_t expid() const
    {
        return is_weekly() ?
            yearmonth() * 10 + week()
            :
            m_exp;
    }

    uint32_t id() const                          { return expid(); }

    /// Returns the month portion of the expiry.
    /// Returns 0 if the expiry is invalid.
    uint32_t month() const                          { return yearmonth() % 100; }

    /// Returns the year portion of the expiry.
    /// Returns 0 if the expiry is invalid.
    uint32_t year() const                           { return yearmonth() / 100; }

    /// Returns the year portion of the expiry.
    /// Returns 0 if the expiry is invalid.
    uint32_t week() const                           { return m_parts.m_expweek; }

    /// Returns true if the expiry_t is valid
    bool is_valid() const                         { return (m_exp != 0); }

    /// Returns true if the expiry_t is valid for a quarter-end (Mar,Jun,Sep,Dec).
    bool is_valid_quarter() const;

    /// Returns true if the expiry_t represents a weekly expiry
    bool is_weekly() const;

    /// Overloaded Comparison Operators
    bool operator ==( const expiry_t exp ) const { return m_exp == exp.m_exp; }
    bool operator !=( const expiry_t exp ) const { return !(*this == exp); }
    bool operator < ( const expiry_t exp ) const;
    bool operator <= ( const expiry_t exp ) const;
    bool operator > ( const expiry_t exp ) const;
    bool operator >= ( const expiry_t exp ) const;

    /// Returns the next expiry_t from the current one.
    expiry_t& operator++ ();    // prefix  ++
    expiry_t  operator++ (int); // postfix ++

    /// Returns the previous expiry_t from the current one.
    expiry_t& operator-- ();    // prefix  --
    expiry_t  operator-- (int); // postfix --

    /// Converts the expiry_t to a string in the form "YYYYMM" or "YYYYMMwW" for a weekly.
    /// An invalid expiry_t has a value of kszInvalid ("INVLID")
    /// szDest must have room for 9 characters (including NULL terminator).
    /// Returns szDest for chaining convenience.
    /// set displayWeek to false will just return YYYYMM for a weekly
    char* to_string( char* szDest, bool displayWeek = true ) const;

    expiry_t operator + ( int );
    expiry_t& operator += ( int );

    void set_id(uint32_t exp) {
        if ( exp > LARGEST_YEARMONTH){
            m_exp = exp / WEEKLY_FACTOR;
            m_parts.m_expweek = exp % WEEKLY_FACTOR;
        }
        else {
            m_exp = exp;
        }
    }

private:
    /// Sets the id based on YYYYMM or YYYYMMDD.
    /// Values outside of [100001,999912] or [10000101, 99991231] are set to UNKNOWN_ID.
    void set( uint32_t yyyymm );
    void set_week( uint8_t ww );
    uint32_t yearmonth() const;

private:
    // uint32_t m_exp;

    //
    // squeeze the week num in the high byte, note - this is little-endian
    // so high byte is at the end
    // [ m_exp(32)                                  ]
    // [ m_expweek(8) ][ m_expmonth(24)             ]
    // [ m_expweek(8) ][ pad(8) ][ pad(8) ][ pad(8) ]
    // [      3       ][    2   ][    1   ][   0    ]
    //
    struct Parts
    {
        union {
            struct {
                uint8_t pad0;
                uint8_t pad1;
                uint8_t pad2;
                uint8_t m_expweek; // 1-5
            };
            uint32_t m_expmonth;
        };
    };
    union {
        int32_t m_exp;
        Parts m_parts;
    };
};

// hashing function
inline std::size_t hash_value( const expiry_t& exp )
{
    boost::hash<uint32_t> hasher;
    return hasher( exp.id() );
}

/// Stream operator to convert expiry_t into human readable form.
std::ostream& operator <<( std::ostream& out, const expiry_t exp );

const char* expiry2str(const expiry_t& sym);
expiry_t str2expiry(const char* str);

} // namespace bb

#endif // BB_CORE_EXPIRY_H
