#ifndef BB_CORE_EVENTFD_H
#define BB_CORE_EVENTFD_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <sys/eventfd.h>

#include <bb/core/smart_ptr.h>
#include <bb/core/FD.h>

namespace bb {

/// Non-blocking wrapper around an eventfd(2).
class EventFD : public NBFD {
public:
    EventFD();
    EventFD(fd_t fd);

    // Increments the eventfd by the given value. throws if value would overflow.
    void write(eventfd_t value = 1);

    // Reads the value of the eventfd.
    // If the counter is non-zero, it'll be reset and the value will be returned.
    // If the counter is zero, zero will be returned.
    // Throws on other errors.
    eventfd_t read();
};
BB_DECLARE_SHARED_PTR(EventFD);

} // namespace bb

#endif // BB_CORE_EVENTFD_H
