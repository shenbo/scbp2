#ifndef BB_CORE_TID_H
#define BB_CORE_TID_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/acct.h>
#include <bb/core/date.h>
#include <bb/core/hash.h>


namespace bb {


// trade id, account:unique-orderid pair.  We're depending on the
// assumption that every trade daemon will have a unique account
// name, even across all brokerages we trade through.  A tid_t is
// considered valid if its account isValid and its orderid is > 0.
class tid_t
{
public:
    typedef acct_t acct_type;
    typedef uint32_t id_type;
    static const id_type invalid_orderid;

    tid_t()
        : acct(), orderid(0) { }
    tid_t(acct_t _acct, id_type _orderid)
        : acct(_acct), orderid(_orderid) { }
    tid_t(const char *_acct, id_type _orderid)
        : acct(_acct), orderid(_orderid) { }
    tid_t(const tid_t &t)
        : acct(t.acct), orderid(t.orderid) { }

    acct_type acct;
    id_type orderid;

    bool operator ==(const tid_t &t) const
    {
        return acct == t.acct && orderid == t.orderid;
    }

    bool operator !=(const tid_t &t) const
    {
        return !(*this == t);
    }

    bool operator <(const tid_t &t) const
    {
        return acct < t.acct ||
            (acct == t.acct && orderid < t.orderid);
    }

    bool isValid() const
    {
        return acct.isValid() && orderid > invalid_orderid;
    }

    std::string toString() const;
};

///   0 - 15 ( 16 bits ) date - epoch
///  16 - 23 (  8 bits ) reserved
///  24 - 39 ( 16 bits ) acct
///  40 - 63 ( 24 bits ) orderid

// hashing function - stored so that
inline std::size_t hash_value( const tid_t& tid )
{
    std::size_t rval = hash_value( tid.acct );
    rval = rval << 24;
    rval |= tid.orderid;
    return rval;
}

// It is assumed TIDs are unique for a date, so being able to hash
// a date/tid pair is useful. I didn't use an actual pair so that
// it would play nice with SWIG as these hash values are often used
// in DB populators
inline std::size_t hash_value( const date_t& date, const tid_t& tid )
{
    size_t date_val = date.ymd_date() - 1970;
    std::size_t rval = date_val << ( 8 + 16 + 24 );
    rval |= hash_value( tid );
    return rval;
}


/// Stream operator to convert tid_t into human readable form.
std::ostream &operator <<(std::ostream &out, const tid_t &p);

} // namespace bb


BB_HASH_NAMESPACE_BEGIN {

template<> struct hash<bb::tid_t>
{
    size_t operator ()(const bb::tid_t &t) const
    {
        size_t seed = 0;
        boost::hash_combine(seed, t.acct);
        boost::hash_combine(seed, t.orderid);
        return seed;
    }
};

} BB_HASH_NAMESPACE_END


#endif // BB_CORE_TID_H
