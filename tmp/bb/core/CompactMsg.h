#ifndef BB_CORE_COMPACTMSG_H
#define BB_CORE_COMPACTMSG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstring>
#include <boost/aligned_storage.hpp>
#include <boost/type_traits.hpp>
#include <boost/utility/enable_if.hpp>
#include <bb/core/Msg.h>

namespace bb {

namespace detail {

// Don't use CompactMsgBuffer directly, use CompactMsg below. T must be a
// BB Msg type that defines MSG_SIZE.
template<typename T>
struct CompactMsgBuffer
{
    enum { kBufferSize = ( sizeof(message_hdr_t) + MSG_MAXSIZE ) };

    inline CompactMsgBuffer()
        : m_data()
    {
        // This memset should be elided by the compiler and emitted as assembly since:
        // - The pointer is aligned
        // - The value to write is constant (and particularly zero)
        // - The size is a compile time constant.
        ::memset( data(), 0, kBufferSize );
    }

    inline char* data()
    {
        return reinterpret_cast<char *>( m_data.address() );
    }

    boost::aligned_storage<kBufferSize> m_data;
};

} // namespace detail

// Efficiently create a BB Msg of type T in contiguous memory.
//
// {
//     bb::CompactMsg<bb::TdStatusChangeMsg> msg;
//     msg.setAccount( acct );
//     ...
//     m_trans.send( msg );
// }
//
// T must be a BB Msg type that defines MSG_SIZE and kMType.

template<typename T, class Enable = boost::enable_if < boost::is_base_of < bb::Msg, T > > >
class CompactMsg : private detail::CompactMsgBuffer<T>, public T
{
public:
    inline CompactMsg()
        : detail::CompactMsgBuffer<T>()
        , T( detail::CompactMsgBuffer<T>::data() )
    {
        T::hdr->mtype  = T::kMType;
        T::hdr->size   = T::MSG_SIZE;
        T::hdr->endian = message_hdr_t::kEndianSentinelValue;
    }

};

} // namespace bb

#endif // BB_CORE_COMPACTMSG_H
