#ifndef BB_CORE_TDCONNECTIONCONFIG_H
#define BB_CORE_TDCONNECTIONCONFIG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <bb/core/CoreConfig.h>

namespace bb {

/// Retrieves parameters from core_config bb.TDConnection. TDs beware: not threadsafe.
namespace TDConnectionConfig
{
    bool doAuthentication();
    bool useTcpNoDelay();
    bool useTcpQuickAck();
    bool useTcpKeepAlive();
    bool useNonBlockingSend();

    // Returns the configured sendbuf size for AF_UNIX
    // sockets. Returns zero to indicate that the system default
    // should be maintained.
    size_t unixSocketSendBufSize();

    typedef TDConnConfig::LoginTestReq TestRequests;
    typedef TDConnConfig::TCPKeepAliveOptions TCPKeepAliveOptions;

    const TCPKeepAliveOptions& getTCPKeepAliveOptions();
    const TestRequests& getLoginTestRequests();
} // namespace TDConnectionConfig

} // namespace bb

#endif // BB_CORE_TDCONNECTIONCONFIG_H
