#ifndef BB_CORE_CALL_ONCE_H
#define BB_CORE_CALL_ONCE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



/// Uses static initialization to invoke a block of code one time.
/// This is NOT threadsafe.
///
/// Example:
///    BB_CALL_ONCE( initialize_static_stuff )
///    {
///        do_stuff();
///    }
inline bool _bb_call_once_tester( bool& var ) { bool orig = var; var = true; return orig; }
#define BB_CALL_ONCE( _varname_ )                                    \
    static bool _bb_call_once_##_varname_##_called = false;          \
    if (!_bb_call_once_tester(_bb_call_once_##_varname_##_called))


#endif // BB_CORE_CALL_ONCE_H
