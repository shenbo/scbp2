#ifndef BB_CORE_USER_H
#define BB_CORE_USER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

namespace bb {

// returns the name of the current "real" (cf. effective) user
std::string get_username();

} // namespace bb

#endif // BB_CORE_USER_H
