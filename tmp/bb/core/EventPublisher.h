#ifndef BB_CORE_EVENTPUBLISHER_H
#define BB_CORE_EVENTPUBLISHER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Error.h>
#include <bb/core/Subscription.h>

#include <boost/range/join.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/foreach.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/type_traits.hpp>

#include <algorithm>
#include <vector>
#include <set>
#include <stdint.h>

namespace bb {

// The EventPublisher is a Publish/Subscribe mechanism that lets us hook several
// callbacks to the same event. An example of such a callback and event could be
// - A subscriber being notified of a book level being added/removed/modified.
// - A source monitor being notified of status changing.
// - A subscriber being notified of signal state being entered/exited
// - Or whatever you like..
//
// Typically, we would have implemented this either as a vector of interface pointers,
// where we add new pointers in the 'subscribe' function, remove them in an
// 'unsubscribe' function and iterate over them to notify changes.
// Or, we could have used boost::signals2, which uses bound functions and weak pointers,
// along with mutexes for thread safety. Boost::signals2 is slower.
//
// This solution gives us the following advantages over boost::signals2:
// - speed
// And the following over maintaining our own vector of interface-ptrs:
// - Ease of use, more code reuse, yadayada.
// - Iterator-safety: you can add/remove subscriptions while notify()-ing of changes.
// - Automatic disconnect: if an EventSubscriber goes out of scope, it will disconnect
//   from all connected EventPublishers.
// - The cost for this extra safety is paid in the connect/disconnect function as well as
//   the destructors. This sets us apart from boost::signals2.
//
// Sample usage:
//
// class BookLevelListener: public bb::IEventSubscriber
// {
//      bool onBookLevelAdded( const IBook* pBook, const BookLevel* pLevel ) { return true; }
// };
//
// typedef EventPublisher<BookLevelListener> PublisherType;
// PublisherType publisher;
// struct notifyBookLevelAddedImpl : public PublisherType::INotifier
// {
//     notifyBookLevelAddedImpl(BookLevelListener* callback) : PublisherType::INotifier ( callback ) { }
//     bool notify( const IBook* pBook, const BookLevel* pLevel )
//     {
//        return !m_callback->onBookLevelAdded( pBook, pLevel );
//     }
// };
//
// BookLevelListener listener;
// publisher.connect(&listener);
// publisher.notifyAll<notifyBookLevelAddedImpl>( pBook, pLevel );
//
// Also, please see test_EventPublisher.cc for a complete implementaton.

class IEventSubscriber;
class IEventPublisher
{
    public:
        friend class IEventSubscriber;
        virtual ~IEventPublisher() {}
    protected:
        virtual bool disconnect(IEventSubscriber* subscription) = 0;
};

class IEventSubscriber
{
    friend class IEventPublisher;

    public:
        // We will disconnect ourselves from any attached EventPublisher before
        // completely destroying the object.
        virtual ~IEventSubscriber()
        {
            stopAllSubscriptions();
        }

        // Once this IEventSubscriber has been connected to a given EventPublisher, the EP will
        // call us back to let us know the subscription is open. This lets us keep track of which
        // connections are outstanding and should be disconnected upon disposing.
        void startSubscription( IEventPublisher* tp )
        {
            BB_THROW_EXASSERT_SSX ( tp, "IEventPublisher can't be NULL" );
            m_openSubscriptions.insert(tp);
        }

        // Once this IEventSubscriber has been disconnected from a given EventPublisher, there is
        // no need to try and disconnect again later. This might even be disastrous if the EventPublisher
        // has been disposed already. Stop the connection now.
        void stopSubscription( IEventPublisher* tp )
        {
            BB_THROW_EXASSERT_SSX ( tp, "IEventPublisher can't be NULL" );
            m_openSubscriptions.erase(tp);
        }

        // At this point, we don't want any EventPublishers to call us back anymore.
        // To make sure they don't, disconnect from every EventPublisher we know about.
        void stopAllSubscriptions()
        {
            // EventPublisher::disconnect will actually call our own stopSubscription again.
            // We still don't want to invalidate iterators, so in here we actually loop over
            // a copy of our subscriptions.
            EventPublisherTypeSet copy ( m_openSubscriptions );
            BOOST_FOREACH(IEventPublisher* tp, copy)
            {
                tp->disconnect(this);
            }
            // We've told all known EventPublishers to forget about us. This will cause them
            // to call 'stopSubscription' on us - at the end of this loop and without explicly
            // doing so, 'm_openSubscriptions' should be empty because of that.
            BB_THROW_EXASSERT_SSX ( m_openSubscriptions.empty(), "We didn't get rid of all subscriptions." );
        }

    protected:
        typedef std::set< IEventPublisher* > EventPublisherTypeSet;
        EventPublisherTypeSet m_openSubscriptions;
};

template<typename EventSubscriberType,
    class Enable = typename boost::enable_if<boost::is_base_of<IEventSubscriber,EventSubscriberType> >::type>
class EventPublisher : public IEventPublisher, private boost::noncopyable
{
    // This gnarly looking code above ensures that will inherit off of IEventSubscriber when we pass an
    // EventSubscriberType in.
    public:
        typedef EventPublisher<EventSubscriberType> PublisherType;
        friend class IEventSubscriberType;
        EventPublisher() : m_iterating(false), m_disposed(false), m_curIdx(0) {}
        virtual ~EventPublisher()
        {
            m_copyVct = m_mainVct;
            m_disposed = true;

            BOOST_FOREACH( EventSubscriberType* subscription, m_mainVct )
            {
                BB_THROW_EXASSERT_SSX ( subscription, "Didn't expect subscription to be NULLptr!" );
                subscription->stopSubscription(this);
            }

            // Invalidate all bb::Subscriptions
            BOOST_FOREACH(AutoDisconnectSubscription* sub, m_subscriptionVct)
            {
                sub->invalidate();
            }
            m_subscriptionVct.clear();
        }

        struct INotifier
        {
            public:
                INotifier(EventSubscriberType * callback ) : m_callback ( callback ) {}
            protected:
                EventSubscriberType* m_callback;
        };

        size_t numConnections() const
        {
            return m_mainVct.size();
        }

        // Connect the EventSubscriberType to the collection of callbacks that will be notified.
        // Returns true if we inserted the subscription to the collection, false if
        // it existed in the collection already.
        bool connect(EventSubscriberType* subscription)
        {
            BB_THROW_EXASSERT_DEBUG(!m_disposed, "Calling function on EP that's disposed already");
            BB_THROW_EXASSERT_SSX(subscription, "Subscription can't be empty!");
            // take extra precaution when connecting/disconnecting subscriptions while iterating.
            swapVectors();

            if ( std::find(m_mainVct.begin(),
                m_mainVct.end(),
                subscription) == m_mainVct.end() )
            {
                // If this listener gets destructed before this EventPublisher does, it will disconnect itself.
                subscription->startSubscription(this);

                m_mainVct.push_back(subscription);
                return true;
            }
            else
            {
                return false;
            }
        }

        // Connect the EventSubscriberType to the collection of callbacks that will be notified.
        // Returns true if we inserted the subscription to the collection, false if
        // it existed in the collection already.
        //
        // When the 'Subscription' that's passed in goes out of scope, the subscription will be
        // automatically disconnected. This is a 'safe' function in the sense that the subscription
        // won't try to do this when the EventPublisher or EventSubscriber goes out of scope earlier
        // instead.
        //
        // Regardless, it's adviseable to just use the 'disconnect' function below instead.
        bool connect(Subscription & outSub, EventSubscriberType* subscription)
        {
            BB_THROW_EXASSERT_DEBUG(!m_disposed, "Calling function on EP that's disposed already");
            if( connect ( subscription ) )
            {
                AutoDisconnectSubscription * sub(new AutoDisconnectSubscription(*this, subscription));
                m_subscriptionVct.push_back(sub);
                outSub.reset(sub);
                return true;
            }
            else
            {
                return false;
            }
        }

        // Disconnect the EventSubscriberType from the collection of callbacks that will be notified.
        // Returns true if the subscription existed in the collection and is now removed, false otherwise.
        bool disconnect(EventSubscriberType* subscription)
        {
            BB_THROW_EXASSERT_DEBUG(!m_disposed, "Calling function on EP that's disposed already");
            BB_THROW_EXASSERT_SSX(subscription, "Subscription can't be empty!");
            // take extra precaution when connecting/disconnecting subscriptions while iterating.
            swapVectors();

            {
                // Invalidate all bb::Subscriptions connected to this EventSubscriberType*
                typename AutoDisconnectSubscriptionVct::iterator iter = m_subscriptionVct.begin();
                while(iter != m_subscriptionVct.end())
                {
                    AutoDisconnectSubscription* sub = *iter;
                    if ( sub->invalidate(subscription) )
                    {
                        iter = m_subscriptionVct.erase(iter);
                    }
                    else
                    {
                        iter++;
                    }
                }
            }

            {
                // And remove the actual subscription as well
                typename SubscriptionVct::iterator iter = std::find(m_mainVct.begin(),
                    m_mainVct.end(),
                    subscription);
                if ( iter != m_mainVct.end() )
                {
                    // The subscription doesn't need to care about this event publisher anymore.
                    subscription->stopSubscription(this);

                    m_mainVct.erase(iter);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //TODO:C++11 - replace all these notifyAll overloads with a single variadic template solution,
        //             and provide the same treatment for notifySimple
        //             .. once we move completely off C++98 that is

        // We call NotifyImpl::notify with the arguments provided and return the number of them that return true.
        //
        // To use this one specific overload below one would:
        //
        // - Set up a listener, hooked up to a  ( SomeListener that inherits off of bb::IEventSubscriberType ):
        //   bb::IEventPublisher<SomeListener> m_listeners;
        //
        // - Create a
        //   struct notifyImpl : public EventPublisher<SomeListener>::INotifier
        //   {
        //      bool notify( const bool p1, const uint64_t p2 )
        //      {
        //          m_callback->doSomething(p1, p2);
        //      }
        //   };
        //   Where 'const bool p1' and 'const uint64_t p2' can be replaced by any data type you want.
        //
        // - Call this notify function on all registered callbacks:
        //   m_listeners.notify<notifyImpl>(*some bool value*, *some uint64_t value*);

        template<typename NotifyImpl>
        size_t notifyAll()
        {
            if( m_mainVct.empty() )
            {
                return 0;
            }
            // If we decide to add/remove subscriptions during this boost_foreach, we will
            // swap the two ListerVct's around. After iterating we have no more need for the
            // copy, which we will clear.
            //
            // We've put this into its own struct to make sure the cleanup gets called,
            // even if we somehow throw an exception.
            on_exit_t on_exit(m_iterating, m_copyVct);
            size_t num_success ( 0 );
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                num_success += NotifyImpl(m_mainVct[ (idx+m_curIdx) % vSize ] ).notify();
            }
            return num_success;
        }

        template<typename NotifyImpl, typename Param1>
        size_t notifyAll(Param1 const & b)
        {
            if( m_mainVct.empty() )
            {
                return 0;
            }
            // If we decide to add/remove subscriptions during this boost_foreach, we will
            // swap the two ListerVct's around. After iterating we have no more need for the
            // copy, which we will clear.
            //
            // We've put this into its own struct to make sure the cleanup gets called,
            // even if we somehow throw an exception.
            on_exit_t on_exit(m_iterating, m_copyVct);
            size_t num_success ( 0 );
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                num_success += NotifyImpl(m_mainVct[ (idx+m_curIdx) % vSize ] ).notify(b);
            }
            return num_success;
        }

        // We call NotifyImpl:notify with the arguments provided and return the number that returned true.
        // See the comments above for a more detailed description.
        template<typename NotifyImpl, typename Param1, typename Param2>
        size_t notifyAll(Param1 const & b, Param2 const & c)
        {
            if( m_mainVct.empty() )
            {
                return 0;
            }
            // If we decide to add/remove subscriptions during this boost_foreach, we will
            // swap the two ListerVct's around. After iterating we have no more need for the
            // copy, which we will clear.
            //
            // We've put this into its own struct to make sure the cleanup gets called,
            // even if we somehow throw an exception.
            on_exit_t on_exit(m_iterating, m_copyVct);
            size_t num_success ( 0 );
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                num_success += NotifyImpl(m_mainVct[ (idx+m_curIdx) % vSize ] ).notify(b, c);
            }
            return num_success;
        }

        // We call NotifyImpl:notify with the arguments provided and return the number that returned true.
        // See the comments above for a more detailed description.
        template<typename NotifyImpl, typename Param1, typename Param2, typename Param3>
        size_t notifyAll(Param1 const & b, Param2 const & c, Param3 const & d)
        {
            if( m_mainVct.empty() )
            {
                return 0;
            }
            // If we decide to add/remove subscriptions during this boost_foreach, we will
            // swap the two ListerVct's around. After iterating we have no more need for the
            // copy, which we will clear.
            //
            // We've put this into its own struct to make sure the cleanup gets called,
            // even if we somehow throw an exception.
            on_exit_t on_exit(m_iterating, m_copyVct);
            size_t num_success ( 0 );
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                num_success += NotifyImpl(m_mainVct[ (idx+m_curIdx) % vSize ] ).notify(b, c, d);
            }
            return num_success;
        }

        // We call NotifyImpl:notify with the arguments provided and return the number that returned true.
        // See the comments above for a more detailed description.
        template<typename NotifyImpl, typename Param1, typename Param2, typename Param3, typename Param4>
        size_t notifyAll(Param1 const & b, Param2 const & c, Param3 const & d, Param4 const & e)
        {
            if( m_mainVct.empty() )
            {
                return 0;
            }
            // If we decide to add/remove subscriptions during this boost_foreach, we will
            // swap the two ListerVct's around. After iterating we have no more need for the
            // copy, which we will clear.
            //
            // We've put this into its own struct to make sure the cleanup gets called,
            // even if we somehow throw an exception.
            on_exit_t on_exit(m_iterating, m_copyVct);
            size_t num_success ( 0 );
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                num_success += NotifyImpl(m_mainVct[ (idx+m_curIdx) % vSize ] ).notify(b, c, d, e);
            }
            return num_success;
        }

        // We call NotifyImpl::notify with the arguments provided and return the number that returned true.
        // See the comments above for a more detailed description.
        template<typename NotifyImpl, typename Param1, typename Param2, typename Param3, typename Param4, typename Param5>
        size_t notifyAll(Param1 const & b, Param2 const & c, Param3 const & d, Param4 const & e, Param5 const & f)
        {
            if( m_mainVct.empty() )
            {
                return 0;
            }
            // If we decide to add/remove subscriptions during this boost_foreach, we will
            // swap the two ListerVct's around. After iterating we have no more need for the
            // copy, which we will clear.
            //
            // We've put this into its own struct to make sure the cleanup gets called,
            // even if we somehow throw an exception.
            on_exit_t on_exit(m_iterating, m_copyVct);
            size_t num_success ( 0 );
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                num_success += NotifyImpl(m_mainVct[ (idx+m_curIdx) % vSize ] ).notify(b, c, d, e, f);
            }
            return num_success;
        }

        // We call NotifyImpl::notify with the arguments provided and return the number that returned true.
        // See the comments above for a more detailed description.
        template<typename NotifyImpl, typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename Param6>
        size_t notifyAll(Param1 const & b, Param2 const & c, Param3 const & d, Param4 const & e, Param5 const & f, Param6 const & g)
        {
            if( m_mainVct.empty() )
            {
                return 0;
            }
            // If we decide to add/remove subscriptions during this boost_foreach, we will
            // swap the two ListerVct's around. After iterating we have no more need for the
            // copy, which we will clear.
            //
            // We've put this into its own struct to make sure the cleanup gets called,
            // even if we somehow throw an exception.
            on_exit_t on_exit(m_iterating, m_copyVct);
            size_t num_success ( 0 );
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                num_success += NotifyImpl(m_mainVct[ (idx+m_curIdx) % vSize ] ).notify(b, c, d, e, f, g);
            }
            return num_success;
        }

        // We call NotifyImpl::notify with the arguments provided and return the number that returned true.
        // See the comments above for a more detailed description.
        template<typename NotifyImpl, typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename Param6, typename Param7>
        size_t notifyAll(Param1 const & b, Param2 const & c, Param3 const & d, Param4 const & e, Param5 const & f, Param6 const & g, Param7 const & h)
        {
            if( m_mainVct.empty() )
            {
                return 0;
            }
            // If we decide to add/remove subscriptions during this boost_foreach, we will
            // swap the two ListerVct's around. After iterating we have no more need for the
            // copy, which we will clear.
            //
            // We've put this into its own struct to make sure the cleanup gets called,
            // even if we somehow throw an exception.
            on_exit_t on_exit(m_iterating, m_copyVct);
            size_t num_success ( 0 );
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                num_success += NotifyImpl(m_mainVct[ (idx+m_curIdx) % vSize ] ).notify(b, c, d, e, f, g, h);
            }
            return num_success;
        }

        // We call NotifyImpl::notify with the arguments provided and return the number that returned true.
        // See the comments above for a more detailed description.
        template<typename NotifyImpl, typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename Param6, typename Param7, typename Param8>
        size_t notifyAll(Param1 const & b, Param2 const & c, Param3 const & d, Param4 const & e, Param5 const & f, Param6 const & g, Param7 const & h, Param8 const & i)
        {
            // If we decide to add/remove subscriptions during this boost_foreach, we will
            // swap the two ListerVct's around. After iterating we have no more need for the
            // copy, which we will clear.
            //
            // We've put this into its own struct to make sure the cleanup gets called,
            // even if we somehow throw an exception.
            on_exit_t on_exit(m_iterating, m_copyVct);
            size_t num_success ( 0 );
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                num_success += NotifyImpl(m_mainVct[ (idx+m_curIdx) % vSize ] ).notify(b, c, d, e, f, g, h, i);
            }
            return num_success;
        }

        // We call NotifyImpl:notify with no arguments on a single listener. A counter is incremented
        // on each notify and the nth listener is notified.  This is not perfect since a modification
        // to the list may cause a listener to be called twice or not at all.  The idea here is to allow
        // the publisher to notify listeners one at a time
        template<typename NotifyImpl>
        bool notifySingle()
        {
            if( m_mainVct.empty() )
            {
                return false;
            }
            if( m_curIdx >= m_mainVct.size() )
            {
                m_curIdx = 0;
            }
            EventSubscriberType* subscription = m_mainVct[ m_curIdx ];
            const bool rc = NotifyImpl(subscription).notify();
            ++m_curIdx;
            return rc;
        }

        template<typename NotifyImpl, typename Param1>
        bool notifySingle( Param1 const & b )
        {
            if( m_mainVct.empty() )
            {
                return false;
            }
            if( m_curIdx >= m_mainVct.size() )
            {
                m_curIdx = 0;
            }
            EventSubscriberType* subscription = m_mainVct[ m_curIdx ];
            const bool rc = NotifyImpl(subscription).notify(b);
            ++m_curIdx;
            return rc;
        }

        template<typename NotifyImpl, typename Container>
        bool notifySingleForEach(Container const & container)
        {
            if( m_mainVct.empty() )
            {
                return false;
            }
            if( m_curIdx >= m_mainVct.size() )
            {
                m_curIdx = 0;
            }
            EventSubscriberType* subscription = m_mainVct[ m_curIdx ];
            bool notified = false;
            BOOST_FOREACH(typename Container::value_type const & item, container )
            {
                notified = NotifyImpl( subscription).notify(item) | notified;
            }
            ++m_curIdx;
            return notified;
        }

        template<typename NotifyImpl, typename Container>
        bool notifyAllForEach(Container const & container)
        {
            if( m_mainVct.empty() )
            {
                return false;
            }

            bool notified = false;
            const uint32_t vSize = m_mainVct.size();
            for( uint32_t idx=0; idx<vSize; ++idx )
            {
                EventSubscriberType* subscription = m_mainVct[ idx ];
                BOOST_FOREACH(typename Container::value_type const & item, container )
                {
                    notified = NotifyImpl( subscription).notify(item) | notified;
                }
            }

            return notified;
        }

        // NOTICE! There is no overloads yet for more than eight parameters.
        // This is not a hard limit ( just a sensible one ). So by all means, feel free to expand if you
        // really really need a callback that takes over eight parameters.
        // If you do so, please also extend 'test_EventPublisher' to cover the new overload.

        // Returns the number of subscribers to this event.
        size_t size() const
        {
            return m_mainVct.size();
        }

        // Returns whether or not we have subscribers attached.
        bool empty() const
        {
            return m_mainVct.empty();
        }

    protected:
        // Within IEventSubscriberType we keep a vector of IEventPublisher*'s to which the
        // listener is subscribed. Once this IEventSubscriber is disposed, it will need to
        // make sure it won't get called anymore.
        bool disconnect(IEventSubscriber* subscription)
        {
            BB_THROW_EXASSERT_DEBUG(!m_disposed, "Calling function on EP that's disposed already");
            BB_THROW_EXASSERT_SSX(subscription, "Subscription can't be empty!");
            EventSubscriberType* tp = (EventSubscriberType*)(subscription);
            BB_THROW_EXASSERT_SSX(tp, "Unable to dynamic cast to right EventSubscriberType!");
            return(tp && disconnect(tp));
        }

    private:
        // This subscription takes care of automatically disconnecting the passed in subscriber
        // from the passed in publisher. It will take care not to do this when it's 'invalid', that is
        // when either the publisher or subscriber has gone out of scope before itself does.
        struct AutoDisconnectSubscription : public SubscriptionBase
        {
            public:
                friend class EventPublisher;
                AutoDisconnectSubscription( PublisherType & publisher, EventSubscriberType* subscription)
                    : m_isValid ( true )
                    , m_publisher ( publisher )
                    , m_subscription ( subscription )
                {
                }

                ~AutoDisconnectSubscription()
                {
                    // We're not valid anymore if either the Publisher or Subscriber has gone out of scope
                    if ( likely ( m_isValid ) )
                    {
                        m_publisher.remove(this);
                        m_publisher.disconnect(m_subscription);
                    }
                }

                // We're valid when both our Publisher and Subscriber haven't gone out of scope
                virtual bool isValid() const
                {
                    return m_isValid;
                }

            protected:
                // Returns true if we were previously valid but are not anymore
                bool invalidate( EventSubscriberType* subscription )
                {
                    // Our subscriber went out of scope, we're no longer valid
                    const bool valid_before ( m_isValid );
                    m_isValid = m_isValid && ( subscription != m_subscription );
                    return valid_before && !m_isValid;
                }

                // Returns true if we were previously valid but are not anymore
                bool invalidate()
                {
                    // Our publisher went out of scope, we're no longer valid
                    const bool valid_before ( m_isValid );
                    m_isValid = false;
                    return valid_before;
                }

            private:
                bool                    m_isValid;
                PublisherType &         m_publisher;
                EventSubscriberType *   m_subscription;
        };

        void remove( AutoDisconnectSubscription * subscription )
        {
            BB_THROW_EXASSERT_DEBUG(!m_disposed, "Calling function on EP that's disposed already");
            typename AutoDisconnectSubscriptionVct::iterator iter (
                    std::find(
                        m_subscriptionVct.begin(),
                        m_subscriptionVct.end(),
                        subscription ) );
            if ( iter != m_subscriptionVct.end() )
            {
                m_subscriptionVct.erase(iter);
            }
        }

        typedef std::vector<EventSubscriberType*> SubscriptionVct;
        typedef std::vector<AutoDisconnectSubscription*> AutoDisconnectSubscriptionVct;
        SubscriptionVct m_mainVct, m_copyVct;
        AutoDisconnectSubscriptionVct m_subscriptionVct;
        // This will be set to true if we're 'BOOST_FOREACH'ing over the collection.
        // In that case we have to take extra precaution not to invalidate iterators.
        bool m_iterating;
        // We'll need to m_disposed variable to help our unit tests - we need to make sure we
        // don't call functions on an EP that's been disposed already. We don't - but this way we
        // can make sure.
        bool m_disposed;
        uint32_t m_curIdx;
        struct on_exit_t
        {
            public:
                on_exit_t(bool & iterating, SubscriptionVct & vct )
                    : m_iterating ( iterating )
                    , m_vct ( vct ) { iterating = true; }
                ~on_exit_t()
                {
                    m_iterating = false;
                    if ( unlikely ( !m_vct.empty() ) )
                    {
                        m_vct.clear();
                    }
                }
            private:
                bool & m_iterating;
                SubscriptionVct &m_vct;
        };

        void swapVectors()
        {
            // we don't want to invalidate iterators so if we notice we're already iterating we:
            // - deep copy the list of callbacks
            // - swap our vectors
            // this works because iterators stay valid when you swap vectors
            if(m_iterating && m_copyVct.empty())
            {
                m_copyVct = m_mainVct;
                m_mainVct.swap(m_copyVct);
            }
        }

};

}

#endif // BB_CORE_EVENTPUBLISHER_H
