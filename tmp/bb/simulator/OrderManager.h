#ifndef BB_SIMULATOR_ORDERMANAGER_H
#define BB_SIMULATOR_ORDERMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <bb/core/smart_ptr.h>

#include <bb/clientcore/IEventDistListener.h>

#include <bb/trading/trading.h>
#include <bb/core/Scripting.h>
#include <bb/simulator/ISimMktDest.h>
#include <bb/simulator/SimUtils.h>
#include <bb/simulator/SimTypes.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( BsonLog );
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );
BB_FWD_DECLARE_SHARED_PTR( HistMStreamManager );

namespace trading {
class IssuedOrderInfo;
BB_FWD_DECLARE_SHARED_PTR( TradingContext );
} //trading
namespace simulator {
struct MarketActionMessage;
struct TD2MktRequestMessage;
struct TDNotifier;
BB_FWD_DECLARE_SHARED_PTR( IOrderHandler );
BB_FWD_DECLARE_SHARED_PTR( IMarketTransitDelays );
BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );
BB_FWD_DECLARE_SHARED_PTR( SimTradingContext );

class OrderManager
    : public virtual ISimMktDest
    , protected IEventDistListener
    , public virtual IOrderManager
    , public MarketDestination
    , protected TimeProviderForwarder
{
public:
    friend class SimTradeDemonClient;
    BB_DECLARE_SCRIPTING();
    std::string getDesc() const;
    void setDesc( std::string const& );
    int getVerbose() const;
    void setVerbose( int );
    //impl IOrderManager
    bool notifyFill( trading::OrderPtr const& simord, double price, unsigned sz
                     , bb::fillflag_t fflag, const ptime_duration_t& mktdelay );
    void notifyDone( trading::OrderPtr const& simord, bb::oreason_t dreason
                     , const ptime_duration_t& add_delay );
    void registerOrderHandler( IOrderHandlerPtr const& );

    virtual void addInstrument( const instrument_t& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                                const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );

    OrderManager( bb::mktdest_t _dest,
                  HistMStreamManagerPtr const& _tc,
                  bb::ITimeProviderCPtr const&,
                  bb::EventDistributorPtr const&,
                  int _vbose,
                  IMarketTransitDelaysCPtr const& mktDelays );

    virtual ~OrderManager();

private:
    /// Receive messages from the SimTradeDemonClient
    void receiveOrder( const instrument_t& instr, dir_t dir, mktdest_t dest,
                       double px, unsigned int size, unsigned int orderid, int timeout,
                       bool visible, tif_t tif, uint32_t oflags );
    void receiveCancelOrder( acct_t order_acct, const instrument_t& instr, const unsigned int order_id );

    void onEvent( Msg const& msg );
    void handleMarketActionMessage( MarketActionMessage const& );
    void handleMarketRequestMessage( TD2MktRequestMessage const& );
    void OpenTheOrder( trading::OrderPtr const& order, IOrderHandlerPtr const& );
    void CancelTheOrder( trading::OrderPtr const& order, IOrderHandlerPtr const& );

    void notifyFill( bb::instrument_t const&, OrderId const& oid, double price, unsigned sz
                     , unsigned remsize
                     , bb::fillflag_t fflag
                     , const ptime_duration_t& mktdelay );
    void notifyDone( bb::instrument_t const&, OrderId const& oid
                     , bb::oreason_t dreason
                     , const ptime_duration_t& add_delay );

    void clear();

    /// Returns true if the order is done and removed from the order list.
    void checkAndHandleDones( trading::OrderPtr const& simord
                              , const ptime_duration_t& add_delay = ptime_duration_t() );

private:
    /// inject message into main stream
    void inject( const Msg& msg );

    std::string m_mydesc;
    typedef std::vector<OrderIdHolder> OrderCont;
    OrderCont m_orders;
    void cancel( OrderIdHolder const& );
    void checkAndHandleDones( OrderIdHolder&
                              , trading::IssuedOrderInfo& simInfo
                              , const ptime_duration_t& add_delay );
    typedef std::vector<IOrderHandlerPtr> OrderHandlerCont;
    OrderHandlerCont m_orderhandlers;
    HistMStreamManagerPtr m_histStreamManager;

    std::auto_ptr<TDNotifier> const m_tdnotifier;
    int m_verboseLevel;
    std::vector<EventSubPtr> m_eventSubs;
    bb::timeval_t m_startTv;
    bb::timeval_t m_endTv;
    BsonLogPtr m_nullLog;
    bool m_dtorcalled;
};
BB_DECLARE_SHARED_PTR( OrderManager );
} // namespace simulator
} // namespace bb
#endif // BB_SIMULATOR_ORDERMANAGER_H
