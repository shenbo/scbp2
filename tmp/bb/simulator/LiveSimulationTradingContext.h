#ifndef BB_SIMULATOR_LIVESIMULATIONTRADINGCONTEXT_H
#define BB_SIMULATOR_LIVESIMULATIONTRADINGCONTEXT_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/enable_shared_from_this.hpp>
#include <bb/core/acct.h>
#include <bb/trading/TradingContext.h>

namespace bb {
namespace simulator {

class LiveSimulationTradingContext
    : public bb::trading::HistTradingContext
    , public boost::enable_shared_from_this<LiveSimulationTradingContext>
{
public:
    LiveSimulationTradingContext( const LiveClientContextPtr& spLiveCC,
                                  const IBookBuilderPtr& bookBuilder,
                                  acct_t acct );

    virtual ~LiveSimulationTradingContext();

    LiveMStreamManagerPtr getLiveMStreamManager()
    {
        return boost::dynamic_pointer_cast<bb::LiveMStreamManager>( getClientContext()->getMStreamManager() );
    }
    virtual HistMStreamManagerPtr getHistMStreamManager();

    void injectMsg( const Msg& msg );

private:
    /// Creates a LiveTrader, and attempts to connect it to trdserver.
    /// The LiveTrader is created whether or not connection succeeds,
    /// so to check whether connection succeeded you have to explicitly
    /// check whether the returned Trader isConnected().
    /// Resets m_spBaseTrader to own the created LiveTrader.
    /// Returns m_spBaseTrader.
    virtual bb::trading::BaseTraderPtr createBaseTraderObj();

    class HistMStreamManagerAdapter;
    BB_DECLARE_SHARED_PTR( HistMStreamManagerAdapter );
    HistMStreamManagerAdapterPtr m_spMStreamManagerAdapter;
};
BB_DECLARE_SHARED_PTR( LiveSimulationTradingContext );

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_LIVESIMULATIONTRADINGCONTEXT_H
