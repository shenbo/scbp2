#ifndef BB_SIMULATOR_SIMTRADEDEMONCLIENT_H
#define BB_SIMULATOR_SIMTRADEDEMONCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>

#include <bb/core/hc.h>
#include <bb/core/auto_vector.h>
#include <bb/core/hash_map.h>
#include <bb/core/source.h>
#include <bb/core/oreason.h>
#include <bb/core/mktdest.h>
#include <bb/core/cxlstatus.h>
#include <bb/core/liquidity.h>
#include <bb/core/fillflag.h>
#include <bb/core/ostatus.h>
#include <bb/core/ptime.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Dispose.h>
#include <bb/core/Scripting.h>

#include <bb/clientcore/ITradeDemonClient.h>
#include <bb/clientcore/IEventDistListener.h>

#include <bb/simulator/markets/ShfeSimMktDest.h>

#include <bb/trading/trading.h>

#include <bb/simulator/SimTypes.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( BsonLog );
BB_FWD_DECLARE_SHARED_PTR( ITimeProvider );
BB_FWD_DECLARE_SHARED_PTR( HistMStreamManager );
BB_FWD_DECLARE_SHARED_PTR( IBookBuilder );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
class TdStatusChangeMsg;

namespace trading {
BB_FWD_DECLARE_SHARED_PTR( HistTradingContext );
BB_FWD_DECLARE_INTRUSIVE_PTR( Order );
BB_FWD_DECLARE_SHARED_PTR( TradingContext );
} // namespace trading

namespace tdcore {
BB_FWD_DECLARE_SHARED_PTR( ITdRiskControlExt );
BB_FWD_DECLARE_SHARED_PTR( ITdPositionStore );
class SymbolData;
}

namespace simulator {
struct MarketActionMessage;
struct Mkt2TDResponseMessage;
struct Mkt2TDFillMessage;
struct Mkt2TDFinishedMessage;
class FakeMessageGenerator;

BB_FWD_DECLARE_SHARED_PTR( OrderManager );
BB_FWD_DECLARE_SHARED_PTR( SimTradingContext );
BB_FWD_DECLARE_SHARED_PTR( ITDTransitDelays );
BB_FWD_DECLARE_SHARED_PTR( IDelaysFactory );
BB_FWD_DECLARE_SHARED_PTR( ISimMktDest );
BB_FWD_DECLARE_SHARED_PTR( SimTradeDemonClient );

/// ITradeDemonClient implementation simulating a connection to
/// a BB message-based Trade Demon (vxtd, tttd, etc.)

/// Passes orders/cancels to the Simulated Market's Order Manager
/// Receives messages back from the Order Manager, then inserts appropriate messages
/// into the message stream to the client trader.

/// Takes advantage of the 1:1:many relationship of the
/// SimTrader:SimTradeDemonClient:OrderManager, and the fact that orderID's are unique
/// per SimTrader by using the order_id internally as the unique identifier for an order.
/// This does not match the live ITradeDemonClient implementation.
class SimTradeDemonClient
    : public ITradeDemonClient
    , protected IEventDistListener
    , public IntrusiveWeakPtrHookable
{
public:
    static SimTradeDemonClientPtr create( trading::HistTradingContextPtr const& sim_trading_context);

    static timeval_t s_hkfe_baml_qh_cutoff;

    BB_DECLARE_SCRIPTING();
    virtual ~SimTradeDemonClient();
    SimTradeDemonClient( trading::TradingContextPtr const& sim_trading_context);

    void setRiskPositionControl( tdcore::ITdRiskControlExtPtr const& );
    tdcore::ITdRiskControlExtPtr getRiskPositionControl() const;

    void initSimMarketDest( mktdest_t mkt, ISimMktDestPtr const& smd );
    ISimMktDestPtr initSimMarketDestWithSource( mktdest_t mkt, const source_t& source );

    ISimMktDestPtr getSimMktDest( mktdest_t dest );
    void initDelays( const IDelaysFactoryCPtr& delaysf );
    void initInstrument( const instrument_t& instr, mktdest_t src );
    bb::source_t getSimTDSource() const;
//Implement  ITradeDemonClient
    bool is_valid() const { return true; }
    /// sends an identify message to the server
    /// returns true if successful
    bool identify( const char* id, timeval_t* send_time = NULL );

    virtual hc_return_t send_order( acct_t acct
                                    , const instrument_t& instr
                                    , dir_t dir
                                    , mktdest_t dest
                                    , double px
                                    , unsigned int size
                                    , unsigned int orderid
                                    , int timeout
                                    , bool visible
                                    , tif_t tif
                                    , uint32_t oflags
                                    , timeval_t* send_time = NULL
                                    , const ISpecialOrderPostProcessor* specialOrder = NULL );

    virtual hc_return_t modify_order( acct_t acct
                                     , unsigned int orig_orderid
                                     , const instrument_t& instr
                                     , dir_t dir
                                     , mktdest_t dest
                                     , double px
                                     , unsigned int size
                                     , unsigned int orderid
                                     , int timeout
                                     , bool visible
                                     , tif_t tif
                                     , uint32_t oflags
                                     , timeval_t* send_time = NULL
                                     , const ISpecialOrderPostProcessor* specialOrder = NULL );


    hc_return_t send_cancel( acct_t acct, const instrument_t& instr,
                             unsigned int orderid, timeval_t* send_time = NULL );

    hc_return_t send_cancels_for_side( acct_t acct, const instrument_t& instr,
                                       bb::side_t side, timeval_t* send_time = NULL ) ;

    /// the PositionTracker will call this on startup and
    /// won't trade until it gets a MarginInstrumentInfoMsg for this acct/sym.
    /// simulate this behavior by injecting the MarginInstrumentInfoMsg as soon as the
    /// broadcasts are requsted.
    virtual bool setup_position_broadcasts( acct_t acct, const instrument_t& instr );
    virtual bool margin_subscribe( acct_t acct, const instrument_t& instr );
    virtual bool margin_unsubscribe( acct_t acct, const instrument_t& instr );

    void cancel( side_t, const instrument_t & );

    void initSyntheticPosition( const instrument_t& instr, int pos );
    void setSimulateMktReject( bool b ) { m_bSimMktReject = b; }

    acct_t getAccount() const { return m_account; }

private:
    //
    void registerOM( mktdest_t mkt, IOrderManagerPtr const& smd );


    /// single point of message injection into main stream
    void inject( const Msg& msg );

    void applyFill( trading::OrderPtr const&, Mkt2TDFillMessage const& );

    /// TODO: Need to document what this does
    ISimMktDestPtr getSimMktDest( const instrument_t& instr, mktdest_t dest, bool create, timeval_t* tv = NULL );

    trading::HistTradingContextWeakPtr m_wkSimTradingContext;
    HistMStreamManagerPtr m_histStreamManager;
    IBookBuilderPtr m_spBookBuilder;

    acct_t m_account;
    std::auto_ptr<FakeMessageGenerator> const m_fmg;
    int m_vbose;
    bool m_bSimMktReject;

    // Now has an auto_vector of SimMktDests
    typedef bb::auto_vector<ISimMktDestPtr> SimMktDestVec;
    SimMktDestVec sim_markets;

    typedef bbext::hash_map<instrument_t, int,
                            instrument_t::hash_no_mkt_no_currency,
                            instrument_t::equals_no_mkt_no_currency>     InstrMap;
    InstrMap                 m_simulatedPositionMap;

    std::vector<EventSubPtr> m_eventSubs;
    void onEvent( Msg const& msg );

    void handleMarketResponse( Mkt2TDResponseMessage const& );
    typedef std::map<OrderId, trading::OrderPtr> OrdersMap_t;
    OrdersMap_t m_ordersmap;

    IDelaysFactoryCPtr m_delaysfactory;
    ITDTransitDelaysCPtr m_tdtransitdelays;

    enum MsgSource { FromClient, FromMkt, FromMktminus1, Internal };
    bb::timeval_t changeOrderStatus( trading::OrderPtr const & io,
                                     bb::ostatus_t newstatus,
                                     bb::oreason_t reason,
                                     bb::mktdest_t,
                                     MsgSource,
                                     bb::timeval_t const & exch_time );

    bb::timeval_t changeCancelStatus( trading::OrderPtr const & io,
                                      bb::cxlstatus_t cxlstatus,
                                      bb::oreason_t reason,
                                      bb::mktdest_t,
                                      MsgSource,
                                      bb::timeval_t const & exch_time,
                                      bool cancelFromModify = false);
        // `cancelFromModify` specifies if cancel is being generated by
        // Cancel/Replace message.

    bb::ClientContextPtr m_clientcontext;
    tdcore::ITdRiskControlExtPtr m_riskcontrol;
    tdcore::ITdPositionStorePtr m_positionstore;
    BsonLogPtr m_nullLog;

    /// Communications from SimMktDests
    void receiveMarketFill( const instrument_t& instr
                            ,
                            OrderId const& orderid,
                            unsigned int size_left,
                            dir_t dir
                            ,
                            double px_fill,
                            int sz_fill,
                            timeval_t fillmsg_tv
                            ,
                            mktdest_t contraBroker,
                            liquidity_t liquidity,
                            bb::timeval_t const& exch_time,
                            bb::fillflag_t fillflags );

    void broadcastPosition( const instrument_t&, const tdcore::SymbolData& );
    void broadcastPosition( const instrument_t&, double price, const int32_t potential_shares[2],
                            int32_t actual_shares, int32_t baseline_shares );


    // HELPER FUNCTIONS

    // Check if original order described by `origOrderPtr` is canceled or if
    // we cannot determine this information (i.e disconnected). In either of
    // the cases specified, new order will be rejected and reason status
    // returned. If original order is not canceled, HC_SUCCESS status wil be
    // returned.
    hc_return_t checkIfCanceledOrDisconnected(trading::OrderPtr const& origOrderPtr,
                                              OrderId const& newOrderId,
                                              instrument_t const& instr,
                                              acct_t account,
                                              timeval_t const& ltv,
                                              timeval_t* send_time);

    // Check if order satisfies TD limits.
    bool checkTDLimits(acct_t account,
                       OrderId const& orderid,
                       instrument_t const& instr,
                       dir_t dir,
                       mktdest_t dest,
                       double px,
                       unsigned int size,
                       timeval_t const& ltv,
                       tif_t tif);

    bool m_started;
};

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_SIMTRADEDEMONCLIENT_H
