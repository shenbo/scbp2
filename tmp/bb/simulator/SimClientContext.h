#ifndef BB_SIMULATOR_SIMCLIENTCONTEXT_H
#define BB_SIMULATOR_SIMCLIENTCONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/core/acct.h>

#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/MStreamManager.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Environment );

namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( SimClientContext );
BB_FWD_DECLARE_SHARED_PTR( SimStreamManager );

class SimClientContext : public bb::HistClientContext
{
public:
    /// Event distributor for market data
    /// only the mkt should be connecting to this
    EventDistributorPtr mktEventDistributor() const;

    SimStreamManagerPtr getSimStreamManager() const;

    static SimClientContextPtr create( const EnvironmentPtr& env
                                       , timeval_t starttv, timeval_t endtv, const std::string& id
                                       , int verbose = 0, const std::string& dbProfileString = "dbstage" );

protected:
    SimClientContext( const EnvironmentPtr& env
                      , timeval_t starttv, timeval_t endtv, const std::string& id
                      , SimStreamManagerPtr const& streamManager, int verbose = 0
                      , const std::string& dbProfileString = "dbstage" );

private:
    SimStreamManagerPtr m_spMStreamMgr;
};

BB_DECLARE_SHARED_PTR( SimClientContext );

} // simulator
} // bb

#endif // BB_SIMULATOR_SIMCLIENTCONTEXT_H
