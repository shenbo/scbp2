#ifndef BB_SIMULATOR_MARKETS_TICKSONLYSIMMKTDEST_H
#define BB_SIMULATOR_MARKETS_TICKSONLYSIMMKTDEST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/scoped_ptr.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <bb/core/Subscription.h>
#include <bb/core/mktdest.h>
#include <bb/core/side.h>
#include <bb/core/tif.h>
#include <bb/core/acct.h>

#include <bb/trading/trading.h>

#include <bb/simulator/SimTypes.h>
#include <bb/simulator/SimUtils.h>
#include <bb/core/Scripting.h>
#include <bb/core/sourceset.h>
namespace bb {

class MultiTickProvider;
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( ITickProvider );

namespace trading {
BB_FWD_DECLARE_SHARED_PTR( HistTradingContext );
} // namespace trading

namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );
class TicksOnlySimMktDest
    : public virtual IOrderHandler
    , protected MarketDestination
    , protected OrderManagerForwarder
{
public:
    BB_DECLARE_SCRIPTING();
    TicksOnlySimMktDest( ClientContextPtr const& hist_cc, bb::mktdest_t _dest, int _vbose, IOrderManagerPtr const& om,
                         IMarketInternalDelaysCPtr const& _internal );

    TicksOnlySimMktDest( ClientContextPtr const& hist_cc,
                         bb::mktdest_t _dest,
                         int _vbose,
                         sourceset_t const&,
                         IOrderManagerPtr const& om,
                         IMarketInternalDelaysCPtr const& _internal );

    virtual ~TicksOnlySimMktDest();

private:
    void onMultiTick( const MultiTickProvider&, ITickProviderCPtr const& );
    //implement IOrderHandler
    bool canAccept( trading::OrderCPtr const& ) const;
    bool canCancel( trading::OrderCPtr const& ) const;
    void activateOrder( trading::OrderPtr const& );
    void deactivateOrder( trading::OrderPtr const& ); //may be called more than once????
    IMarketInternalDelaysCPtr getMarketInternalDelays() const;
    bool addInstrument( bb::instrument_t const& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                        const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );
    void init( sourceset_t const& );

private:
    void cancelAll();
    boost::scoped_ptr<MultiTickProvider> const m_multiTickProv;
    Subscription m_spMultiTickSub;
    typedef std::list<trading::OrderPtr> OrderList;
    OrderList m_active_orders;
    IMarketInternalDelaysCPtr m_internaldelays;
};
BB_DECLARE_SHARED_PTR( TicksOnlySimMktDest );

/// Configures the simulator to use:
///    TicksOnlySimMktDest+IsldCrossSimulator for MKT_ISLD
///    TicksOnlySimMktDest for all other markets.
void initMachSimulator( trading::HistTradingContextPtr const& histTradingContext, int vbose, source_t isldSrc );

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MARKETS_TICKSONLYSIMMKTDEST_H
