#ifndef BB_SIMULATOR_MARKETS_ISLDCROSSSIMULATOR_H
#define BB_SIMULATOR_MARKETS_ISLDCROSSSIMULATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <bb/clientcore/ImbalanceInfo.h>
#include <bb/core/Subscription.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <list>
#include <bb/core/acct.h>
#include <bb/core/timeval.h>
#include <bb/core/ostatus.h>
#include <bb/simulator/SimTypes.h>
#include <bb/simulator/SimUtils.h>
#include <memory>
#include <bb/core/Scripting.h>
namespace bb {

class instrument_t;
class IsldCrossTradeMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( IsldImbalanceInfo );
BB_FWD_DECLARE_SHARED_PTR( HistClientContext );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );
BB_FWD_DECLARE_SHARED_PTR( IOrderManager );
class IsldCrossSimulator
    : public virtual IOrderHandler
    , protected MarketDestination
    , protected TimeProviderForwarder
    , protected OrderManagerForwarder
{
public:
    IsldCrossSimulator( ClientContextPtr const& hist_cc, bb::source_t isldSrc, int vbose
                        , IOrderManagerPtr const& om, IMarketInternalDelaysCPtr const& _internal );
    virtual ~IsldCrossSimulator();
    BB_DECLARE_SCRIPTING();

private:
    //Impl IOrderHandler
    bool canAccept( trading::OrderCPtr const& ) const;
    bool canCancel( trading::OrderCPtr const& ) const;
    void activateOrder( trading::OrderPtr const& );
    void deactivateOrder( trading::OrderPtr const& ); //may be called more than once????
    IMarketInternalDelaysCPtr getMarketInternalDelays() const;
    bool addInstrument( bb::instrument_t const& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                        const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );


    // midnight callback
    void atMidnightWakeup( const timeval_t& ctv, const timeval_t& swtv );

    // handles cross trades
    void onIsldImbalance( ImbalanceProvider const& imbalanceProv, IsldImbalanceInfoCPtr const& spIsldImb );
    void onIsldCrossTrade( IsldCrossTradeMsg const& cross_msg );

    ClientContextPtr m_spHistCC;
    Subscription m_midnightWakeupSub;

    MsgHandlerPtr m_spCrossTradeMsgHandlerSub;
    ImbalanceProvider m_imbalanceProv;
    Subscription m_spIsldImbSub;

    typedef std::map<symbol_t, IsldImbalanceInfo> SymIsldImbMap;
    SymIsldImbMap m_sym2IsldImbMap_OPEN;
    SymIsldImbMap m_sym2IsldImbMap_CLOSE;

    // reset every midnight
    timeval_t m_open_nocancel_threshold_tv;
    timeval_t m_open_reject_tv;
    timeval_t m_close_nocancel_threshold_tv;
    timeval_t m_close_reject_tv;

    static double ms_open_nocancel_threshold_hhmmss;  // after this time, opening orders cannot be cancelled
    static double ms_open_reject_hhmmss;              // after this time, uncrossed orders are rejected
    static double ms_close_nocancel_threshold_hhmmss; // after this time, closing orders cannot be cancelled
    static double ms_close_reject_hhmmss;             // after this time, uncrossed orders are rejected

    int m_verboseLevel;
    typedef std::list<trading::OrderPtr> OrderList;
    OrderList m_active_orders;
    IMarketInternalDelaysCPtr m_internaldelays;

};
BB_DECLARE_SHARED_PTR( IsldCrossSimulator );
} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MARKETS_ISLDCROSSSIMULATOR_H
