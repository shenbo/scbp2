#ifndef BB_SIMULATOR_MARKETS_INTERACTIVEDEST_H
#define BB_SIMULATOR_MARKETS_INTERACTIVEDEST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Dispose.h>
#include <bb/core/Scripting.h>
#include <bb/core/TimeProvider.h>
#include <bb/clientcore/MsgHandler.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/trading/Order.h>
#include <bb/simulator/ISimMktDest.h>
#include <bb/simulator/SimTypes.h>
#include <bb/simulator/OrderManager.h>
#include <bb/core/ValHolder.h>
#include <bb/core/smart_ptr.h>

namespace bb {
class Msg;

namespace trading {
BB_FWD_DECLARE_SHARED_PTR( HistTradingContext );
}

namespace simulator {
BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );

class InteractiveDest;

class InteractiveDestListener
{
public:
    virtual ~InteractiveDestListener(){}
    virtual bool canAccept( const InteractiveDest* dest, trading::OrderCPtr const& ) const { return true; };
    virtual bool canCancel( const InteractiveDest* dest, trading::OrderCPtr const& ) const { return true; };
    virtual void activateOrder( InteractiveDest* dest, trading::OrderPtr const& )  {};
    virtual void deactivateOrder( InteractiveDest* dest, trading::OrderPtr const& ) {};
};

///
/// This market Destination is for testing only.  It does not build books
/// or change the state of orders automatically. Use the static functions
/// to manage orders
class InteractiveDest
    : public virtual IOrderHandler
    , protected MarketDestination
    , protected TimeProviderForwarder
    , protected OrderManagerForwarder
{
public:

    static void initSimulator( const trading::HistTradingContextPtr& htc, const std::set<instrument_t>& instrs, InteractiveDestListener * l );

    InteractiveDest(
        bb::mktdest_t _dest,
        const trading::HistTradingContextPtr& htc,
        bb::instrument_t sym,
        int _vbose,
        IOrderManagerPtr const& om,
        IMarketInternalDelaysCPtr const& _internal,
        InteractiveDestListener * l);

    ~InteractiveDest();
    std::string getDesc() const;
    void setDesc( std::string const& );
    int getVerbose() const;
    void setVerbose( int );
    bb::instrument_t getInstrument() { return m_instr;}

    void fillOrder( uint32_t oid, uint32_t qty, double px );
    void cancelOrder( uint32_t oid );
    void rejectOrder( uint32_t oid );
    bool addInstrument(const bb::instrument_t&, const IBookSpecPtr&, const IOrderBookSpecPtr&)
    {
        { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    }

    static void fillOrder( bb::instrument_t instr, uint32_t oid, uint32_t qty, double px );
    static void fillOrder( bb::instrument_t instr, uint32_t oid );
    static void cancelOrder( bb::instrument_t instr, uint32_t oid );
    static void rejectOrder( bb::instrument_t instr, uint32_t oid );
    static trading::OrderPtr getOrder( bb::instrument_t instr, uint32_t oid );

private:
    bool canAccept( trading::OrderCPtr const& ) const;
    bool canCancel( trading::OrderCPtr const& ) const;
    void activateOrder( trading::OrderPtr const& );
    void deactivateOrder( trading::OrderPtr const& ); //may be called more than once????
    trading::OrderPtr getOrder( uint32_t oid );
    trading::OrderPtr getOrder( uint32_t oid, side_t side );
    IMarketInternalDelaysCPtr getMarketInternalDelays() const;
private:

    typedef bb::ValHolder<boost::scoped_ptr<trading::OrderList>, bb::side_t, 2> order_holder_t;

    std::string m_mydesc;
    int m_verboseLevel;
    IMarketInternalDelaysCPtr m_internaldelays;
    friend std::ostream& operator<<( std::ostream& out, const InteractiveDest& mds );
    order_holder_t m_simOrders;

    bb::instrument_t          m_instr;
    InteractiveDestListener*  m_listener;

    static std::map<bb::instrument_t, boost::shared_ptr<InteractiveDest> >     s_instrDestMap;

};
BB_DECLARE_SHARED_PTR( InteractiveDest );
} // namespace simulator
} // namespace bb


#endif // BB_SIMULATOR_MARKETS_INTERACTIVEDEST_H
