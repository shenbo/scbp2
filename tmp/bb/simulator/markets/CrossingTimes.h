#ifndef BB_SIMULATOR_MARKETS_CROSSINGTIMES_H
#define BB_SIMULATOR_MARKETS_CROSSINGTIMES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <bb/core/Subscription.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <list>
#include <bb/core/timeval.h>
#include <bb/core/ostatus.h>
#include <bb/simulator/SimTypes.h>
#include <bb/simulator/SimUtils.h>
#include <memory>
#include <bb/core/Scripting.h>

namespace bb {
class instrument_t;
class BbL1TradeMsg;
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );
BB_FWD_DECLARE_SHARED_PTR( IOrderManager );

class ICrossingTimes : public virtual IMarketDestination
{
public:
    virtual bb::timeval_t getOpenAccept() const = 0;
    virtual bb::timeval_t getOpenNoCancel() const = 0;
    virtual bb::timeval_t getOpenNoAccept() const = 0;
    virtual bb::timeval_t getCloseAccept() const = 0;
    virtual bb::timeval_t getCloseNoCancel() const = 0;
    virtual bb::timeval_t getCloseNoAccept() const = 0;
    virtual ~ICrossingTimes() {}

    //   static ICrossingTimesCPtr create(bb::mktdest_t);
};
BB_DECLARE_SHARED_PTR( ICrossingTimes );

class CrossingTimes : public ICrossingTimes, public MarketDestination
{
public:
    CrossingTimes( bb::mktdest_t, bb::timeval_t midnight );
    BB_DECLARE_SCRIPTING();
    bb::timeval_t getOpenAccept() const;
    bb::timeval_t getOpenNoCancel() const;
    bb::timeval_t getOpenNoAccept() const;
    bb::timeval_t getCloseAccept() const;
    bb::timeval_t getCloseNoCancel() const;
    bb::timeval_t getCloseNoAccept() const;

    void setOpenAccept( bb::timeval_t const& );
    void setOpenNoCancel( bb::timeval_t const& );
    void setOpenNoAccept( bb::timeval_t const& );
    void setCloseAccept( bb::timeval_t const& );
    void setCloseNoCancel( bb::timeval_t const& );
    void setCloseNoAccept( bb::timeval_t const& );

private:
    bb::timeval_t m_midnight;
    bb::timeval_t m_openaccept;
    bb::timeval_t m_opennocancel;
    bb::timeval_t m_opennoaccept;
    bb::timeval_t m_closeaccept;
    bb::timeval_t m_closenocancel;
    bb::timeval_t m_closenoaccept;
};

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MARKETS_CROSSINGTIMES_H
