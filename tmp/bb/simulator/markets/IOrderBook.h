#ifndef BB_SIMULATOR_MARKETS_IORDERBOOK_H
#define BB_SIMULATOR_MARKETS_IORDERBOOK_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/instrument.h>
#include <bb/core/side.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <bb/core/Dispose.h>
#include <bb/core/LuaCodeGen.h>
#include <bb/core/Scripting.h>

#include <bb/clientcore/BookSpec.h>

namespace bb {

namespace trading {

// Forward declaration
class TradingContext;
BB_FWD_DECLARE_INTRUSIVE_PTR( Order );

} // namespace trading

namespace simulator {

// Forward declaration
class IOrderManager;
BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );

class IOrderBook
{
protected:
    enum FillMode
    {
        FILL_FROM_TRADE,
        FILL_FROM_BOOKUPDATE,
        FILL_MARKET_PRICE
    };

public:
    virtual ~IOrderBook() {}

    virtual void activateOrder( const trading::OrderPtr& order ) = 0;

    virtual void deactivateOrder( const trading::OrderPtr& order ) = 0;

protected:
    virtual void checkAndApplyFills( const side_t& side, const double& price, const uint32_t& size,
                                     const FillMode& fill_mode ) = 0;

    virtual void checkBookExecution( FillMode fill_mode ) = 0;
};

BB_DECLARE_SHARED_PTR( IOrderBook );

class IOrderBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~IOrderBookSpec() {}

    /// Construct the IOrderBook.  The reason the parameters includes the
    /// instrument is because we usually construct an OrderBook per asset
    /// we're interested in trading, the other parameters will be fixed.
    virtual IOrderBookPtr construct( const instrument_t& instrument,
                                     const source_t& source,
                                     const IntrusiveWeakPtr<trading::TradingContext>& context,
                                     const IntrusiveWeakPtr<IOrderManager>& manager,
                                     const IMarketInternalDelaysCPtr& internal_delays,
                                     const IBookSpecPtr& book_spec ) = 0;

    virtual void print( std::ostream& os, const LuaPrintSettings& settings ) const = 0;
};

BB_DECLARE_SHARED_PTR( IOrderBookSpec );

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MARKETS_IORDERBOOK_H
