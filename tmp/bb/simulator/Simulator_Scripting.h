#ifndef BB_SIMULATOR_SIMULATOR_SCRIPTING_H
#define BB_SIMULATOR_SIMULATOR_SCRIPTING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_SHARED_PTR( TradingContext );
}
namespace simulator {
bool registerScripting();
struct ITDSimulatorInit
{
    virtual ~ITDSimulatorInit() {}
    virtual void init( trading::TradingContextPtr const& ) = 0;
};
BB_DECLARE_SHARED_PTR( ITDSimulatorInit );
} // namespace simulator
} // namespace bb


#endif // BB_SIMULATOR_SIMULATOR_SCRIPTING_H
