// Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved.

#include <algorithm>
#include <boost/bind.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/assign/std/vector.hpp>
#include <boost/foreach.hpp>

#include <bb/core/env.h>

#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/BookPriceProvider.h>
#include <bb/clientcore/BookSpec.h>
#include <bb/clientcore/ClientContextAdvancer.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/L2BookBase.h>
#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderBuilder.h>
#include <bb/clientcore/ShfeL2Book.h>
#include <bb/clientcore/SourceBooks.h>
#include <bb/clientcore/TickFactory.h>
#include <bb/clientcore/TickProvider.h>

#include <bb/trading/trading.h>
#include <bb/trading/TradingContext.h>

#include <bb/simulator/SimTradingContext.h>
#include <bb/simulator/SimMktDelays.h>
#include <bb/simulator/markets/AsiaQueueInstrOrderBook.h>

using namespace bb::trading;

namespace bb {
namespace simulator {

BB_DEFINE_ORDER_INFO(AsiaQueueSimulationInfo);

AsiaQueueSimulationInfo::AsiaQueueSimulationInfo()
    : m_positionInQueue(0)
    , m_lastUpdateTime(bb::timeval_t::earliest)
{}

AsiaQueueInstrOrderBook::AsiaQueueInstrOrderBook(const instrument_t& instr
                                                  , bb::IntrusiveWeakPtr<trading::TradingContext> const& tc
                                                  , source_t src
                                                  , bb::IntrusiveWeakPtr<IOrderManager> const& om
                                                  , IMarketInternalDelaysCPtr const& _internaldelays
                                                  , const IBookSpecPtr& sim_book_spec
                                                  , const bool& use_split_trades
                                                  , const AdverseFillsConfig& adverse_fills_config
                                                  , const LookAheadExchangeConfig& look_ahead_book_config)
    : AsiaInstrOrderBook(instr, tc, src, om, _internaldelays, sim_book_spec, use_split_trades)
    , m_context(tc)
    , m_adverseFills(adverse_fills_config)
    , m_lookAheadExchange(look_ahead_book_config)
{

    if(unlikely(m_lookAheadExchange.verbose))
    {
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "AsiaQueueInstrOrderBook::AsiaQueueInstrOrderBook" << bb::endl;
        }
    }
    if(m_adverseFills.enabled())
    {
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "creating SimHistClientContext for adverse fills with lookahead time: " << m_adverseFills.look_ahead_time << bb::endl;
        }
        m_adverseFillsCC = SimHistClientContext::create(bb::DefaultCoreContext::getEnvironment()
                                                         , m_context->getClientContext()->getStartTimeval()
                                                         , m_context->getClientContext()->getEndTimeval());

        ClientContextAdvancerPtr advancer(new FixedWindowClientContextAdvancer(m_adverseFillsCC, m_adverseFills.look_ahead_time));

        //hacky.  i have a circular dependancy. ideally i would pass a book_spec into the creater func
        //but i need the book that gets created.
        //perhaps I will add a getBook() method to advancer, pass book spec into cc create, and then get
        //book after that.
        m_adverseFillsCC->setAdvancer(advancer);

        BookBuilderPtr book_builder(new BookBuilder(m_adverseFillsCC));
        PriceProviderBuilderPtr price_builder(new PriceProviderBuilder(m_adverseFillsCC, false, book_builder));
        BookPxPSpecPtr source_provider_spec(new BookPxPSpec);
        source_provider_spec->m_bookSpec.reset(new SourceBookSpec(instr, src));
        m_adverseFillsPP = source_provider_spec->build(price_builder.get());
    }

    if(m_lookAheadExchange.enabled)
    {
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "creating IndeterminateHistClientContext" << bb::endl;
        }
        m_lookAheadExchangeCC = SimHistClientContext::create(bb::DefaultCoreContext::getEnvironment()
                                                              , tc->getClientContext()->getStartTimeval()
                                                              , tc->getClientContext()->getEndTimeval()
                                                              , "SimHistClientContext"
                                                              , m_lookAheadExchange.verbose);

        BookBuilderPtr book_builder(new BookBuilder(m_lookAheadExchangeCC));
        m_lookAheadBook = AsiaInstrOrderBook::build_book(book_builder, instr, src, sim_book_spec);

        ClientContextAdvancerPtr advancer;

        if(m_lookAheadExchange.use_fixed_look_ahead_time)
        {
            if(unlikely(m_lookAheadExchange.verbose))
            {
                LOG_INFO << "FixedWindowClientContextAdvancer: " << m_lookAheadExchange.max_advance_duration << bb::endl;
            }

            advancer.reset(new FixedWindowClientContextAdvancer(m_lookAheadExchangeCC, m_lookAheadExchange.fixed_look_ahead_time));
        }
        else
        {
            if(unlikely(m_lookAheadExchange.verbose))
            {
                LOG_INFO << "BookUpdateClientContextAdvancer: " << m_lookAheadExchange.max_advance_duration << bb::endl;
            }

            advancer.reset(new BookUpdateClientContextAdvancer(m_lookAheadExchangeCC, m_lookAheadBook,
                                                                 m_lookAheadExchange.max_advance_duration));
        }

        m_lookAheadExchangeCC->setAdvancer(advancer);
    }
}

AsiaQueueInstrOrderBook::~AsiaQueueInstrOrderBook()
{}

void AsiaQueueInstrOrderBook::applyAdverseFillAdjustment(trading::OrderPtr const& o)
{
    dir_t direction = o->orderInfo().getDir();
    double order_price = o->orderInfo().getPrice();

    AsiaQueueSimulationInfo& sim_info = o->get<AsiaQueueSimulationInfo>();
    int32_t old_queue_position = int32_t(sim_info.getPositionInQueue());
    int32_t adjusted_queue_position = old_queue_position;

            if(unlikely(m_lookAheadExchange.verbose))
            {
                LOG_INFO << "AsiaQueueInstrOrderBook::applyAdverseFillAdjustment"
                         << " old_queue_position: " << old_queue_position
                         << bb::endl;
            }
    if(old_queue_position > 0)
    {
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << *o << bb::endl;
        }

        m_adverseFillsCC->advance(m_context->getClockMonitor()->getTime());
        double future_price = m_adverseFillsPP->getRefPrice();
        double abs_price_diff = fabs(future_price - order_price);
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "AsiaQueueInstrOrderBook::applyAdverseFillAdjustment"
                     << " pxp_time:" << (m_adverseFillsPP->getLastChangeTime())
                     << " future_price:" << (future_price)
                     << " order_price: " << (order_price)
                     << " abs_price_diff: " << abs_price_diff
                     << bb::endl;
        }
        // Returns
        double adjustment_factor = abs_price_diff / order_price;
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "adjustment_factor calculation: " << adjustment_factor << " = " << abs_price_diff << " / " <<
                order_price << bb::endl;
        }
        // Divided my the max returns we care about.
        adjustment_factor = adjustment_factor / m_adverseFills.front_of_queue_returns;
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "adjustment_factor / " << m_adverseFills.front_of_queue_returns << " = " << adjustment_factor << bb::endl;
        }

        // Drop anything greater than the max.
        if(adjustment_factor > 1.0)
            adjustment_factor = 1.0;
        // Multiply by the current queue to get the adjustment.
        adjusted_queue_position =
            bb::round_to_nearest_integer(double(old_queue_position) * (1 - adjustment_factor));

        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "adjustment_queue_position calculation: " << adjusted_queue_position << " = "
                     << "bb::round_to_nearest_integer(double("
                     << old_queue_position << ") * (1 - " << adjustment_factor << "))" << bb::endl;
        }

        if(adjusted_queue_position < 0)
            adjusted_queue_position = 0;
        bool apply_adjustment = false;

        const bool futurePriceGreaterThanOrderPrice = future_price > order_price;

        // Only move up the queue if this will hurt you: Adverse!
        if(direction == BUY && !futurePriceGreaterThanOrderPrice)
            apply_adjustment = true;
        // Sells
        if(direction != BUY && futurePriceGreaterThanOrderPrice)
            apply_adjustment = true;

        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "apply adjustment: " << apply_adjustment << " futurePriceGreaterThanOrderPrice: " <<
                futurePriceGreaterThanOrderPrice << bb::endl;
        }

        if(apply_adjustment)
        {
            if(unlikely(m_lookAheadExchange.verbose))
            {
                LOG_INFO << "AsiaQueueInstrOrderBook::applyAdverseFillAdjustment"
                         << " order_price:" << order_price
                         << " old_queue_position:" << old_queue_position
                         << " future_price:" << future_price
                         << " adjustment_factor:" << adjustment_factor
                         << " adjusted_queue_position:" << adjusted_queue_position
                         << " look_ahead_time:" << m_adverseFills.look_ahead_time
                         << " update_time:" << m_context->getClientContext()->getTime()
                         << bb::endl;
            }
            sim_info.setPositionInQueue(adjusted_queue_position, m_context->getClientContext()->getTime());
        }
        else
        {
            // Initialize last update time
            if(timeval_diff(m_context->getClientContext()->getTime(),
                              sim_info.getLastUpdateTime()) > m_adverseFills.look_ahead_time)
                sim_info.setPositionInQueue(adjusted_queue_position, m_context->getClientContext()->getTime());
        }

    }
}

void AsiaQueueInstrOrderBook::activateOrder(trading::OrderPtr const& o)
{
    BB_ASSERT(o->hasInfo<trading::IssuedOrderInfo>());
    labelAutoOffsetOrder(o);
    trading::OrderList& list = orderList(o->orderInfo().getDir());
    list.push_back(o); //not actually at the end as this list gets sorted

    if(m_lookAheadExchange.enabled)
    {
        m_lookAheadExchangeCC->advance(m_context->getClockMonitor()->getTime());
        m_lookAheadBookOrderCheckTime.insert(std::make_pair(o->issuedInfo().getOrderid(), m_lookAheadBook->getLastChangeTime()));
        addQueuePosition(o, m_lookAheadBook);
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "setting active order: " << *o << bb::endl;
        }
        m_activeOrder = o;
        checkLookAheadBookExecution(FILL_MARKET_PRICE);

        //do not start queue processing this order until the regular boooks time is past that of the lookAhead
        //book at the time the order entered this function
        m_activeOrder.reset();
    }
    else
    {
        addQueuePosition(o, m_spBook); //this queue position is based off the current book, not lookAhead
        checkBookExecution(FILL_MARKET_PRICE);
    }
}

uint32_t AsiaQueueInstrOrderBook::getBookSizeAtPrice(const IBookPtr book, side_t side, double price)
{
    int32_t size_at_this_price = 0;

    int32_t found_depth = 0;
    BookLevelCPtr psp = getBookLevelAtPrice(*book, side, price, &found_depth);
    if(found_depth >= 0)
    {
        size_at_this_price = psp->getSize();
    }

    return size_at_this_price;
}

void AsiaQueueInstrOrderBook::addQueuePosition(bb::trading::OrderPtr const& order, const IBookPtr& book)
{
    side_t side = order->orderInfo().getSide();
    double price = order->orderInfo().getPrice();
    uint32_t default_queue_size = 0; //this could become a configurable value
    uint32_t queue_position = getBookSizeAtPrice(book, side, price);
    if(queue_position == 0)
    {
        queue_position = default_queue_size;
    }

    if(unlikely(m_lookAheadExchange.verbose))
    {
        LOG_INFO << "AsiaQueueInstrOrderBook::addQueuePosition: order: " << *order << bb::endl;
        LOG_INFO << "queue position: " << queue_position << " book: " << *book << bb::endl;
    }
    order->construct<AsiaQueueSimulationInfo>().setPositionInQueue(queue_position);

    if(m_adverseFills.enabled())
        applyAdverseFillAdjustment(order);
}

// Where/when will these be called? Deprecated?
void AsiaQueueInstrOrderBook::updateQueuePositions(const IBookPtr& book,
                                                    const side_t& side,
                                                    const double& price,
                                                    const uint32_t& size,
                                                    const IOrderBook::FillMode& m)
{
    dir_t opposite_dir = side2dir(opposite_side(side));
    OrderList& olist = orderList(opposite_dir);
    if(olist.empty())
        return;

    for(OrderList::iterator it = olist.begin(); it != olist.end(); ++it)
    {
        trading::OrderPtr o = *it;
        updateQueuePosition(o, book, side, price, size, m);
    }
}

void AsiaQueueInstrOrderBook::updateQueuePosition(const trading::OrderPtr& o,
                                                   const IBookPtr& book,
                                                   const side_t& side,
                                                   const double& price,
                                                   const uint32_t& size,
                                                   const IOrderBook::FillMode& m)
{
    trading::OrderInfo const& oi = o->orderInfo();

    AsiaQueueSimulationInfo& sim_info = o->get<AsiaQueueSimulationInfo>();

    // First apply adverse fill adjustments, if it is time for another update.
    if(m_adverseFills.enabled())
    {
        const ptime_duration_t diff = timeval_diff(m_context->getClientContext()->getTime(), sim_info.getLastUpdateTime());

        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "AsiaQueueInstrOrderBook::updateQueuePosition"
                     << " oid: " << o->issuedInfo().getOrderid()
                     << " current_time: " << m_context->getClientContext()->getTime()
                     << " last_update_time: " << sim_info.getLastUpdateTime()
                     << " diff: " << ptime_duration_to_double(diff)
                     << " lookAheadTime: " << ptime_duration_to_double(m_adverseFills.look_ahead_time)
                     << bb::endl;
        }

        if(diff > m_adverseFills.look_ahead_time)
        {
            applyAdverseFillAdjustment(o);
        }
    }

    if(m_lookAheadExchange.enabled)
    {
        LookAheadOrderTimeMap::const_iterator i = m_lookAheadBookOrderCheckTime.find(o->issuedInfo().getOrderid());
        BB_ASSERT(i != m_lookAheadBookOrderCheckTime.end());

        const timeval_t& bookChangeTime = book->getLastChangeTime();
        if(bookChangeTime <= i->second)
        {
            if(unlikely(m_lookAheadExchange.verbose))
            {
                LOG_INFO << "updateQueuePosition: book time: " << bookChangeTime << " is not past the lookAhead time of: " << i->second << " at which the original queue position was set. not updating queue position for order: " << o->issuedInfo().getOrderid() << bb::endl;
            }
            return;
        }
    }

    uint32_t old_queue_position = sim_info.getPositionInQueue();

    // Assume trades at price of trade, or inner BBBO and size of trade or BBBO occured,
    // so decrease queues. Check against latest book size.
    if(at(side, oi.getPrice(), price))
    {
        // Size at same side and level as orders.
        uint32_t size_at_order_price_level = int32_t(getBookSizeAtPrice(book, opposite_side(side), oi.getPrice()));

        if((m == IOrderBook::FILL_FROM_BOOKUPDATE) && (size_at_order_price_level == 0))
        {
            const PriceSize outerMostSide = book->getOuterMostSide(side);
            if(outside(side, price, outerMostSide.getPrice()))
            {
                // if we are here we are so far away from the top of book that we have no price
                // in this case we do not change your position if we are here on a book update.
                // in reality we should cancel the order and put it closer to the midpoint
                return;
            }
        }

        uint32_t tmp = uint32_t(std::max(int32_t(old_queue_position - size), 0));
        uint32_t new_queue_position = std::min(size_at_order_price_level, tmp);

        if(new_queue_position != old_queue_position)
        {
            if(unlikely(m_lookAheadExchange.verbose))
            {
                LOG_INFO << "AsiaQueueInstrOrderBook::updateQueuePosition: order: " << *o << bb::endl;
                LOG_INFO << "old_q_pos: " << old_queue_position << " new_q_pos: " << new_queue_position << " book: " << *book << bb::endl;
            }
            sim_info.setPositionInQueue(new_queue_position);
        }
    }
    else if(outside(side, oi.getPrice(), price))
    {
        // Assume full fills since trades are happening outside this level.
        // Set queue position to 0 for correctness
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "AsiaQueueInstrOrderBook::updateQueuePosition: order: " << *o << bb::endl;
            LOG_INFO << "price of order outside price of book. setting queue position to zero" << bb::endl;
            LOG_INFO << "old_q_pos: " << old_queue_position << " new_q_pos: " << 0 << " book: " << *book << bb::endl;
        }
        sim_info.setPositionInQueue(0);
    }
}

bool AsiaQueueInstrOrderBook::oidMatchesActiveOid(unsigned int oid)
{
    if(m_activeOrder)
    {
        unsigned int active_oid = m_activeOrder->issuedInfo().getOrderid();
        return oid == active_oid;
    }
    return false;
}

uint32_t AsiaQueueInstrOrderBook::checkAndApplyLookAheadFills(side_t book_side,
                                                               double la_book_price,
                                                               uint32_t la_book_size,
                                                               const IOrderBook::FillMode& m)
{
    dir_t opposite_dir = side2dir(opposite_side(book_side));
    OrderList& olist = orderList(opposite_dir);
    if(olist.empty())
        return 0;

    if(unlikely(m_lookAheadExchange.verbose))
    {
        LOG_INFO << "AsiaQueueInstrOrderBook::checkAndApplyLookAheadFills: side: " << side2str(book_side) << " la_book_price: " << la_book_price << " fill mode: " << m << endl;
    }

    std::vector<FillInfo> fill_list;
    for(OrderList::iterator it = olist.begin(); it != olist.end(); ++it)
    {
        trading::OrderPtr o = *it;
        trading::OrderInfo const& oi = o->orderInfo();
        // Debug info
        unsigned int oid = o->issuedInfo().getOrderid();

        if(!oidMatchesActiveOid(oid))
        {
            if(unlikely(m_lookAheadExchange.verbose))
            {
                LOG_INFO << "skipping beceause oid does not match active oid: " << *o << bb::endl;
            }
            continue;
        }

        if(outside_or_at(book_side, oi.getPrice(), la_book_price))
        {
            // *** Find Pessimistic Fill Price ***
            // We don't want the look-ahead book to give a better fill price for cases where we might have gotten run over
            // from the current book to the look ahead book.

            // we assume here that any change in the best price on book_side is the same for each level, so we can just use
            // the top of book price change on the opposite side of market to get a pessimistic fill price
            double book_level_delta = m_spBook->getNthSide(0, book_side).getPrice() - m_lookAheadBook->getNthSide(0, book_side).getPrice();
            double curr_book_level_price_estimate = la_book_price + book_level_delta;

            double pessimistic_fill_price = outer_price(book_side, curr_book_level_price_estimate, la_book_price); // run over price

            if(outside(book_side, pessimistic_fill_price, la_book_price))
                LOG_DEBUG("pessimistic_fill_price: " << pessimistic_fill_price << " is outside la_book_price: " << la_book_price << " for side: " << book_side);

            if(outside(book_side, pessimistic_fill_price, oi.getPrice())) {
                LOG_DEBUG ("The pessimistic_fill_price would have been: " << pessimistic_fill_price << " but that is outside order's price: " << oi.getPrice() << " for side: " << book_side);
                pessimistic_fill_price = oi.getPrice();
            }

            if(book_level_delta != 0)
            {
                if(unlikely(m_lookAheadExchange.verbose))
                {
                    LOG_DEBUG("order: " << *o);
                    LOG_DEBUG("The book has moved " << (const char *)(book_level_delta > 0.0 ? "down" : "up") << ". comparing the " << side2str(book_side) << " side of the market");
                    LOG_DEBUG("The worse midprice for this order between currBook: " << m_spBook->getMidPrice() << " and LookAheadBook: " <<  m_lookAheadBook->getMidPrice() << " is " << outer_price(book_side, m_spBook->getMidPrice(), m_lookAheadBook->getMidPrice()));
                    LOG_DEBUG("the outside (worse) price with side " << side2str(book_side) << " for estimate book level: " << curr_book_level_price_estimate << " and look ahead book price: " << la_book_price << " but not worse than order's price: " << oi.getPrice() << " is the pessimistic_fill_price: " << pessimistic_fill_price);
                }
            }

            //if the size on the book is smaller than the size remaining in the order
            //only fill the book amount
            uint32_t fill_size = std::min(la_book_size, o->issuedInfo().getRemainingSize());
            FillInfo fi(o, pessimistic_fill_price, fill_size);

            if(fi.fill_size > 0)
            {
                if(unlikely(m_lookAheadExchange.verbose))
                {
                    LOG_INFO << "adding fill to list. remaining size: " << o->issuedInfo().getRemainingSize() << " - " << *o << bb::endl;
                }
                fill_list.push_back(fi);
            }
        } // end if(outside_or_at)
    } // end OrderList::iter

    BOOST_FOREACH(const FillInfo &fi, fill_list)
    {
        notifyFill(fi.order, fi.fill_price, fi.fill_size
                    , getFillFlag(fi.order->orderInfo()));

        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "notifying of fill. size: " << fi.fill_size << " price: " << fi.fill_price << " remaining size: " << fi.order->issuedInfo().getRemainingSize() << " - " <<
                *fi.order << bb::endl;
        }
    }
    return fill_list.size();
}


void AsiaQueueInstrOrderBook::checkAndApplyFills(const side_t& side, const double& price
                                                      , const uint32_t& size, const IOrderBook::FillMode& m)
{
    dir_t dir = side2dir(opposite_side(side));
    OrderList& olist = orderList(dir);
    if(olist.empty())
        return;

    if(unlikely(m_lookAheadExchange.verbose))
    {
        LOG_INFO << "AsiaQueueInstrOrderBook::checkAndApplyFills: side: " << side2str(side) << " book_price: " << price << " fill mode: " << m << endl;
    }

    int32_t unfilled_size = int32_t(size);

    std::vector<FillInfo> fill_list;

    for(OrderList::iterator it = olist.begin(); it != olist.end(); ++it)
    {
        trading::OrderPtr o = *it;
        trading::OrderInfo const& oi = o->orderInfo();
        // Debug info
        unsigned int oid = o->issuedInfo().getOrderid();

        if(oidMatchesActiveOid(oid))
        {
            if(unlikely(m_lookAheadExchange.verbose))
            {
                LOG_INFO << "skipping because oid matches active oid: " << *o << bb::endl;
            }
            continue;
        }

        if(outside_or_at(side, oi.getPrice(), price))
        {
            if(unlikely(m_lookAheadExchange.verbose))
            {
                LOG_INFO << "might have a fill. remaining size: " << o->issuedInfo().getRemainingSize() << " - " << *o <<
                    bb::endl;
            }
            int32_t remaining_size = o->issuedInfo().getRemainingSize();
            FillInfo fi(o, price, remaining_size);

            //if the size on the book is smaller than the size remaining in the order
            //only fill the amount on teh book
            if(unfilled_size < remaining_size)
                fi.fill_size = unfilled_size;

            // FILL_FROM_TRADE is when you have an add-liquidity order and trade happens (onTickUpdate)
            // at your price or higher. the goal is to move that trade-size off of the queue and fill
            // where appropriate (ie: tick-size > queue-pos)
            if(m == FILL_FROM_TRADE)
            {
                fi.fill_price = oi.getPrice();

                int32_t queue_adjusted_size = unfilled_size;
                if(at(side, oi.getPrice(), price))
                {
                    AsiaQueueSimulationInfo& sim_info = o->get<AsiaQueueSimulationInfo>();
                    int32_t queue_position = int32_t(sim_info.getPositionInQueue());

                    //example. if sum of all orders in front of me is 7 (queue_position) and
                    // trade size (unfilled_size) is 10 than i get filled for min(10 - 7 = 3, remaining_size)
                    if(unfilled_size > queue_position)
                    {

                        queue_adjusted_size = unfilled_size - queue_position;
                        if(unlikely(m_lookAheadExchange.verbose))
                        {
                            LOG_INFO << "AsiaQueueInstrOrderBook::checkAndApplyFills "
                                     << "Fill due to trade, trade larger than queue size. oid:" << o->issuedInfo().getOrderid()
                                     << " trade size: " << unfilled_size
                                     << " queue:" << queue_position
                                     << " size_filled:" << unfilled_size << bb::endl;
                        }
                    }
                    else
                    {
                        if(unlikely(m_lookAheadExchange.verbose))
                        {
                            LOG_INFO << "AsiaQueueInstrOrderBook::checkAndApplyFills "
                                     << "No fill due to queue. oid:" << o->issuedInfo().getOrderid()
                                     << " queue:" << queue_position
                                     << " size_filled:" << unfilled_size << bb::endl;
                        }
                        queue_adjusted_size = 0;
                    }
                    fi.fill_size = std::min(uint32_t(queue_adjusted_size), fi.fill_size);
                }
                else
                {
                    fi.fill_size = std::min(uint32_t(unfilled_size), fi.fill_size);
                }
                if(unfilled_size > int32_t(fi.fill_size))
                    unfilled_size -= int32_t(fi.fill_size);
                else
                    unfilled_size = 0;
            }
            // FILL_FROM_BOOKUPDATE is when you have an add-liquidity order and the opposite side of the book
            // moves to you or through you so your level in the book is completely gone. Everyone at that
            // price therefore gets a fill
            else if(m == FILL_FROM_BOOKUPDATE)
            {
                fi.fill_price = oi.getPrice();
                // fill all crossed levels in entirety
            }
            if(fi.fill_size > 0)
            {
                if(unlikely(m_lookAheadExchange.verbose))
                {
                    LOG_INFO << "adding fill to list. remaining size: " << o->issuedInfo().getRemainingSize() << " - " <<
                        *o << bb::endl;
                }
                fill_list.push_back(fi);
                if(unfilled_size <= 0)
                    break;
            }
        } // end if(outside_or_at)
    } // end OrderList::iter

    updateQueuePositions(m_spBook, side, price, size, m);

    BOOST_FOREACH(const FillInfo &fi, fill_list)
    {
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "notifying of fill. remaining size: " << fi.order->issuedInfo().getRemainingSize() << " - " <<
                *fi.order << bb::endl;
            LOG_INFO << "regular Book" << *m_spBook << bb::endl;
        }
        notifyFill(fi.order, fi.fill_price, fi.fill_size
                    , getFillFlag(fi.order->orderInfo()));
    }
}

void AsiaQueueInstrOrderBook::checkBookExecution(IOrderBook::FillMode m)
{
    const IBookPtr book = m_spBook;

    FOR_EACH_SIDE(side) {
        dir_t opposite_dir = side2dir(opposite_side(side));
        if(orderList(opposite_dir).empty())
            continue;

        IBookLevelCIterPtr it = book->getBookLevelIter(side);
        while(it->hasNext())
        {
            BookLevelCPtr lvl = it->next();
            if(!isnan(lvl->getPrice()) && lvl->getSize() > 0)
            {
                checkAndApplyFills(side, lvl->getPrice(), lvl->getSize(), m);
            }
        }
    }
}


void AsiaQueueInstrOrderBook::checkLookAheadBookExecution(IOrderBook::FillMode m)
{
    if(unlikely(m_lookAheadExchange.verbose))
    {
        LOG_INFO << "curr book: " << *m_spBook << bb::endl;
        LOG_INFO << "lookahead book: " << *m_lookAheadBook << bb::endl;
    }


    if(!m_spBook->isOK())
    {
        LOG_INFO << "current book is not OK - not checking for lookahead fills" << bb::endl;

        return;
    }

    if(!m_lookAheadBook->isOK())
    {
        LOG_INFO << "lookahead book is not OK - not checking for lookahead fills" << bb::endl;

        return;
    }

    if(m_spBook->getMidPrice() != m_lookAheadBook->getMidPrice())
    {
        if(unlikely(m_lookAheadExchange.verbose))
        {
            LOG_INFO << "lookahead book different: "
                     << m_spBook->getMidPrice()
                     << " : "
                     << m_lookAheadBook->getMidPrice()
                     << bb::endl;
        }
    }

    const IBookPtr book = m_lookAheadBook;

    uint32_t fill_count = 0;

    FOR_EACH_SIDE(side) {
        dir_t opposite_dir = side2dir(opposite_side(side));
        if(orderList(opposite_dir).empty())
            continue;

        IBookLevelCIterPtr it = book->getBookLevelIter(side);
        while(it->hasNext())
        {
            BookLevelCPtr lvl = it->next();
            if(!isnan(lvl->getPrice()) && lvl->getSize() > 0)
            {
                fill_count += checkAndApplyLookAheadFills(side, lvl->getPrice(), lvl->getSize(), m);
            }
        }
    }

    if(unlikely(m_lookAheadExchange.verbose))
    {
        LOG_INFO << "done checking for lookAheadFills. fills found: " << fill_count << bb::endl;
    }
}

ptime_duration_t AdverseFillsConfig::DEFAULT_LOOK_AHEAD_TIME = boost::posix_time::minutes(10);
double AdverseFillsConfig::DEFAULT_FRONT_OF_QUEUE_RETURNS = std::numeric_limits<double>::infinity();

AsiaQueueInstrOrderBookSpec::AsiaQueueInstrOrderBookSpec()
    : m_lookAheadTime(AdverseFillsConfig::DEFAULT_LOOK_AHEAD_TIME)
    , m_frontOfQueueReturns(AdverseFillsConfig::DEFAULT_FRONT_OF_QUEUE_RETURNS)
{}

IOrderBookPtr AsiaQueueInstrOrderBookSpec::construct(const instrument_t& instrument,
                                                      const source_t& source,
                                                      const IntrusiveWeakPtr<trading::TradingContext>& context,
                                                      const IntrusiveWeakPtr<IOrderManager>& manager,
                                                      const IMarketInternalDelaysCPtr& internal_delays,
                                                      const IBookSpecPtr& book_spec)
{
    boost::optional<AdverseFillsConfig> adverseFillsConfig = AdverseFillsConfig::fromOptionalLua(m_adverseFillsConfig);
    if(!adverseFillsConfig)
    {
        adverseFillsConfig = AdverseFillsConfig();

        if((adverseFillsConfig.get().look_ahead_time != m_lookAheadTime) ||
            (adverseFillsConfig.get().front_of_queue_returns != m_frontOfQueueReturns))
        {
            LOG_WARN << "'look_ahead_time' and 'front_of_queue_returns' params are deprecated. use 'adverse_fills' param instead" << bb::endl;
        }

        //we warn above, but set the values anyway
        adverseFillsConfig.get().look_ahead_time = m_lookAheadTime;
        adverseFillsConfig.get().front_of_queue_returns = m_frontOfQueueReturns;
    }

    boost::optional<LookAheadExchangeConfig> lookAheadExchangeConfig = LookAheadExchangeConfig::fromOptionalLua(m_lookAheadExchangeConfig);
    if(!lookAheadExchangeConfig)
    {
        lookAheadExchangeConfig = LookAheadExchangeConfig();
    }
    return AsiaQueueInstrOrderBookPtr(new AsiaQueueInstrOrderBook(instrument
                                                                    , context
                                                                    , source
                                                                    , manager
                                                                    , internal_delays
                                                                    , book_spec
                                                                    , m_useSplitTrades
                                                                    , adverseFillsConfig.get()
                                                                    , lookAheadExchangeConfig.get()));
}

void AsiaQueueInstrOrderBookSpec::print(std::ostream& os, const LuaPrintSettings& ps) const
{
    // Indentation one past current
    const LuaPrintSettings one_indent = ps.next();

    os << "(function() -- AsiaQueueInstrOrderBookSpec" << std::endl
       << one_indent.indent() << "local book = AsiaQueueInstrOrderBookSpec()" << std::endl
       << one_indent.indent() << "book.look_ahead_time = " << luaMode(m_lookAheadTime, one_indent) << std::endl
       << one_indent.indent() << "book.front_of_queue_returns = " <<
            luaMode(m_frontOfQueueReturns, one_indent) << std::endl
       << one_indent.indent() << "return book" << std::endl
       << one_indent.indent() << "end)()";
}

bool AsiaQueueInstrOrderBookSpec::registerScripting(lua_State& state)
{
    luabind::module(&state)
    [
        luabind::class_ < AsiaQueueInstrOrderBookSpec, AsiaInstrOrderBookSpec, IOrderBookSpecPtr > (
            "AsiaQueueInstrOrderBookSpec")
        .def(luabind::constructor < > ())
        .def_readwrite("look_ahead_time", &AsiaQueueInstrOrderBookSpec::m_lookAheadTime)
        .def_readwrite("front_of_queue_returns", &AsiaQueueInstrOrderBookSpec::m_frontOfQueueReturns)
        .def_readwrite("adverse_fills", &AsiaQueueInstrOrderBookSpec::m_adverseFillsConfig)
        .def_readwrite("look_ahead_exchange", &AsiaQueueInstrOrderBookSpec::m_lookAheadExchangeConfig)
    ];

    return true;
}

CmeQueueInstrOrderBook::CmeQueueInstrOrderBook(const instrument_t& instr
                                                , bb::IntrusiveWeakPtr<trading::TradingContext> const& tc
                                                , source_t src
                                                , bb::IntrusiveWeakPtr<IOrderManager> const& om
                                                , IMarketInternalDelaysCPtr const& _internaldelays
                                                , const IBookSpecPtr& sim_book_spec
                                                , const bool& use_split_trades
                                                , const AdverseFillsConfig& adverse_fills_config
                                                , const LookAheadExchangeConfig& look_ahead_book_config)
: AsiaQueueInstrOrderBook(instr, tc, src, om, _internaldelays,
                           sim_book_spec, use_split_trades, adverse_fills_config, look_ahead_book_config)
{
    LOG_INFO << "CmeQueueInstrOrderBook::CmeQueueInstrOrderBook: " << instr << " src: " << src << bb::endl;
}

void CmeQueueInstrOrderBook::onBookChanged(const IBook* pBook, const Msg* pMsg,
                                            int32_t bidLevelChanged, int32_t askLevelChanged)
{
    typedef std::pair<bb::side_t, bb::PriceSize> SidePriceSize;
    AsiaQueueInstrOrderBook::onBookChanged(pBook, pMsg, bidLevelChanged, askLevelChanged);

    std::vector< SidePriceSize > sidesThatChanged;

    if(bidLevelChanged > -1)
    {
        const bb::PriceSize ps = pBook->getNthSide(bidLevelChanged, bb::BID);
        sidesThatChanged.push_back(std::make_pair(bb::BID, ps));
    }
    if(askLevelChanged > -1)
    {
        const bb::PriceSize ps = pBook->getNthSide(askLevelChanged, bb::ASK);
        sidesThatChanged.push_back(std::make_pair(bb::ASK, ps));
    }

    BOOST_FOREACH(const SidePriceSize& sps, sidesThatChanged)
    {
        bb::side_t sideThatChanged = sps.first;
        bb::PriceSize ps = sps.second;
        dir_t dir = side2dir( sideThatChanged);
        OrderList& olist = orderList(dir);

        for(OrderList::iterator it = olist.begin(); it != olist.end(); ++it)
        {
            trading::OrderPtr o = *it;
            trading::OrderInfo const& oi = o->orderInfo();
            if(EQ(oi.getPrice(),  ps.getPrice()))
            {
                // pass in a trade size of zero. we only want to update the position
                // based on the book size
                // pass in the current price of the order to guarantee we hit the code
                // that updates queue position
                // pass in opposite side that changed as it gets flipped back around again (hacky)
                if(unlikely(m_lookAheadExchange.verbose))
                {
                    LOG_INFO << "Book has changed on a level where we have an order. Updating queue pos. Level price: " << oi.getPrice() << bb::endl;
                    LOG_INFO << *pBook << bb::endl;
                }
                updateQueuePosition(o, m_spBook,  opposite_side(sideThatChanged), oi.getPrice(), 0, IOrderBook::FILL_FROM_BOOKUPDATE);
            }
        }
    }
}
} // namespace simulator
} // namespace bb
