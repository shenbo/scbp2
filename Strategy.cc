#include "Strategy.h"

#include <iostream>
#include <iomanip>
#include <string>

#include <bb/core/messages_autogen.h>
#include <bb/core/ptime.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/trading/IssuedOrderTracker.h>
#include <bb/trading/InstrumentContext.h>

#include </usr/include/python2.7/Python.h>

#include "SSETickProvider.h"
#include "SSEBook.h"

using namespace bb;
using namespace std;

namespace alphaless {

Strategy::Strategy(const InstrVector& instruments,
                    const bb::trading::TradingContextPtr& tradingContext,
                    const StrategySettings &strategySettings,
                    const bb::source_t& source,
                    std::string& csvfile,
                    bool baseline
                    ) 
    : m_instrs (instruments)
    , m_strategySettings (strategySettings)
    , m_market(bb::str2mktdest(strategySettings.market.c_str()))
    , m_tradingContext(tradingContext)
    , m_clientContext(tradingContext->getClientContext())
    , m_clockMonitor(m_clientContext->getClockMonitor())
    , m_timer(m_clientContext->getClientTimer())
    , m_baseline(baseline)
{
    BOOST_FOREACH(const InstrVector::value_type& instr, m_instrs) {
        bb::IBookPtr spBook(new SseBook(m_clientContext, instr, source, string("desc")));
        spBook->addBookListener(this);
        m_books.push_back(spBook);
        m_Books.insert(BookMap::value_type(instr, spBook));

        bb::IPriceProviderCPtr pp = m_tradingContext->getInstrumentContext(instr)->getPriceProvider();
	
        pp->addPriceListener(m_priceSub[ instr ], boost::bind(&Strategy::onPriceChanged, this, _1));

        bb::ITickProviderPtr tp(new SSETickProvider(m_clientContext, instr, source, string("desc")));
        tp->addTickListener(this);
        m_tps.push_back(tp);

        m_tradingContext->getIssuedOrderTracker(instr)->addStatusChangeListener(this);
        m_tradingContext->getPositionProvider(instr)->addPositionListener(m_posSub[ instr ], this);

    }

    // create a timer that wakes the strategy up one hour before the end of market
    {
        instrument_t ins = instrument_t::fromString(string("SHSE_600000"));
        bb::Subscription sub;
        m_timer->schedule(sub,
                           boost::bind(&Strategy::oneShotTimerCB, this, ins, bb::BUY, 100, 16.56),
                           bb::timeval_t::make_time(2016, 10, 11, 11, 1, 1));
        m_subVec.push_back(sub);
    }

    m_tickPrices.clear();
}

Strategy::~Strategy()
{
    BOOST_FOREACH(BookMap::value_type bookPair, m_Books) {
        bookPair.second->removeBookListener(this);
    }

    LOG_INFO<<"Destructing Startegy"<<bb::endl;
}

void Strategy::subscribeUserMessage()
{
    bb::MsgHandlerPtr handler = bb::MsgHandler::createMType<bb::UserMessageMsg>(
    bb::source_t::make_auto(bb::SRC_UMSG),
    m_tradingContext->getEventDistributor(),
    boost::bind(&Strategy::handleUserMessage, this, _1),
    bb::trading::PRIORITY_TRADING_DEFAULT);
    m_msgHandlers.push_back(handler);
}

void Strategy::oneShotTimerCB(instrument_t ins, dir_t dir, int size, double price) {
    bb::trading::ITraderPtr trader = m_tradingContext->getTrader(ins);

    bb::trading::OrderPtr order(new bb::trading::Order);
    order->orderInfo()
            .setInstrument(ins)
            .setPrice(price)
            .setDir(dir)
            .setTimeInForce(bb::TIF_IMMEDIATE_OR_CANCEL)
            .setDesiredSize(size)
            .setMktDest(m_market);

    unsigned int orderResult = trader->sendOrder(order);
    if (orderResult == bb::trading::ITrader::SEND_ORDER_FAILED) {
        LOG_PANIC << "PLACED ORDER FAILED: " << *order << bb::endl;
    }
}

void Strategy::onPriceChanged(const bb::IPriceProvider& priceProvider) {
}

void Strategy::onBookChanged(const bb::IBook* pBook, const bb::Msg* pMsg,
                              int32_t bidLevelChanged, int32_t askLevelChanged) {
                              return;
}

void Strategy::onBookFlushed(const bb::IBook* pBook, const bb::Msg* pMsg) { }

void Strategy::onPositionUpdated(bb::trading::IPositionProvider* pos) {
}

void Strategy::onOrderStatusChange(const bb::trading::OrderPtr& order, const bb::trading::IOrderStatusListener::ChangeFlags& flags) {
}

void Strategy::onFill(const bb::trading::FillInfo& info) {
}

void Strategy::onOpenTick(const bb::ITickProvider* tp, const bb::TradeTick& tick) {
}

void Strategy::shutdown() {
}

void Strategy::handleUserMessage(const bb::UserMessageMsg& userMsg) {
}

}// namespace alphaless
