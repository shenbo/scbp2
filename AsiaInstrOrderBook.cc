// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include <algorithm>
#include <boost/bind.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/assign/std/vector.hpp>
#include <boost/foreach.hpp>

#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/BookSpec.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/CffexL1Book.h>
#include <bb/clientcore/CffexSfitComboL1Book.h>
#include <bb/clientcore/WindBook.h>
#include <bb/clientcore/TaifexBook.h>
#include <bb/clientcore/DceL2Book.h>
#include <bb/clientcore/TwseBook.h>
#include <bb/clientcore/ShfeL2Book.h>
#include <bb/clientcore/SourceBooks.h>
#include <bb/clientcore/TickFactory.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/CzceL1Book.h>
#include <bb/clientcore/CzceL2Book.h>
//#include <bb/clientcore/ShfeTickProvider.h>

#include <bb/trading/trading.h>
#include <bb/trading/TradingContext.h>
//#include <bb/trading/Order.h>

#include <bb/simulator/SimTradingContext.h>
#include <bb/simulator/markets/AsiaInstrOrderBook.h>

using namespace bb::trading;

namespace bb {
namespace simulator {

AsiaInstrOrderBook::AsiaInstrOrderBook(const instrument_t& instr
                                        , bb::IntrusiveWeakPtr<trading::TradingContext> const& tc
                                        , source_t src
                                        , bb::IntrusiveWeakPtr<IOrderManager> const& om
                                        , IMarketInternalDelaysCPtr const& _internaldelays
                                        , const IBookSpecPtr& sim_book_spec
                                        , const bool& use_split_trades
                                       )
    : OrderManagerForwarder(om, _internaldelays) //keeps a weak reference
    , m_instr(instr)
    , m_buyOrderList(MoreAggressiveOrderComp(BID))
    , m_selOrderList(MoreAggressiveOrderComp(ASK))
    , m_useSplitTrades(use_split_trades)
    , m_cc (tc->getClientContext())
    , m_marketSpecMap (m_cc->getMarketSpecificationsMap())
    , m_volumeAdjustingTickListener (instr.mkt, (*m_marketSpecMap)[ instr.mkt ], *this)
{
    for(int i = 0; i < bb::DIR_INVALID; ++i) m_offsets[i] = 0;

    m_spBook = AsiaInstrOrderBook::build_book(tc->getBookBuilder(), instr, src, sim_book_spec);

    BB_ASSERT(m_spBook);
    m_spTickProvider = tc->getSourceTickFactory()->getTickProvider(instr, src, true);
    BB_ASSERT(m_spTickProvider);
    m_spTickProvider->addTickListener(&m_volumeAdjustingTickListener);
    m_spBook->addBookListener(this);
}

IBookPtr AsiaInstrOrderBook::build_book(const IBookBuilderPtr& book_builder, const instrument_t& instr,
                                         source_t src, const IBookSpecPtr& sim_book_spec)
{
    BB_ASSERT(book_builder);
    IBookPtr book;
    if(sim_book_spec)
    {
        book = book_builder->buildBook(sim_book_spec);
    }
    else if(src.type() == SRC_CFFEX || src.type() == SRC_ACR_CFFEX_L1 || src.type() == SRC_CFFEX_MOCK)
    {
        CffexL1BookSpecPtr sbs(new CffexL1BookSpec(instr, src));
        book = book_builder->buildBook(sbs);
    }
    else if(src.type() == SRC_CFFEX_SFIT_COMBO)
    {
        CffexSfitComboL1BookSpecPtr sbs(new CffexSfitComboL1BookSpec(instr, src));
        book = book_builder->buildBook(sbs);
    }
    else if(src.type() == SRC_ACR_CFFEX_L2_MERGED
             || src.type() == SRC_ACR_CFFEX_L2_MERGED_W_INFO
             || src.type() == SRC_ACR_CFFEX_L2
             || src.type() == SRC_WIND_STOCK)
    {
        WindBookSpecPtr sbs(new WindBookSpec(instr, src));
        book = book_builder->buildBook(sbs);
    }
    else if(src.type() == SRC_MULTIPATH)
    {
        book = book_builder->buildSourceBook(instr, src);
    }
    else if(src.type() == SRC_ACR_DCE_L2 || src.type() == SRC_DCE_L2 || src.type() == SRC_DCE_L1_L2_MERGED
            || src.type() == SRC_NH_DCE_PROXY_L2)
    {
        DceL2BookSpecPtr sbs(new DceL2BookSpec(instr, src));
        book = book_builder->buildBook(sbs);
    }
    else if(src.type() == SRC_TWSE)
    {
        TwseBookSpecPtr sbs(new TwseBookSpec(instr, src));
        book = book_builder->buildBook(sbs);
    }
    else if(src.type() == SRC_TAIFEX)
    {
        TaifexBookSpecPtr sbs(new TaifexBookSpec(instr, src));
        book = book_builder->buildBook(sbs);
    }
    else if (src.type() == SRC_SHFE || src.type() == SRC_SHFE_DIRECT)
    {
        ShfeL2BookSpecPtr sbs(new ShfeL2BookSpec(instr, src));
        book = book_builder->buildBook(sbs);
    }
    else if (src.type() == SRC_CZCE_L2
            || src.type() == SRC_CZCE_L2_MCAST
            || src.type() == SRC_CZCE_L2_MCAST_BACKUP_A
            || src.type() == SRC_CZCE_L2_MERGED)

    {
        CzceL2BookSpecPtr sbs(new CzceL2BookSpec(instr, src));
        book = book_builder->buildBook(sbs);
    }
    else if (src.type() == SRC_CZCE_L1 || src.type() == SRC_ACR_CZCE_L1)
    {
        CzceL1BookSpecPtr sbs(new CzceL1BookSpec(instr, src));
        book = book_builder->buildBook(sbs);
    }
    else {
        SourceBookSpecPtr sbook(new SourceBookSpec(instr, src));
        book = book_builder->buildBook(sbook);
    }
    return book;
}

AsiaInstrOrderBook::~AsiaInstrOrderBook()
{
    m_spBook->removeBookListener(this);
    m_spTickProvider->removeTickListener(this);
}

bb::fillflag_t AsiaInstrOrderBook::getFillFlag(trading::OrderInfo const& o)
{
    fillflag_t fillflags = FILLFLAG_NONE;
    uint32_t flags = o.getFlags();
    if(flags & OFLAG_SHFE_MANUAL_OPENCLOSE)
    {
        if(flags & OFLAG_SHFE_CLOSEPOSITIONTODAY)
            fillflags = (fillflag_t) (FILLFLAG_SHFE_CLOSE | FILLFLAG_SHFE_CLOSETODAY);
        else if(flags & OFLAG_SHFE_CLOSEPOSITION)
            fillflags = FILLFLAG_SHFE_CLOSE;
    }
    return fillflags;
}

void AsiaInstrOrderBook::activateOrder(trading::OrderPtr const& o)
{
    BB_ASSERT(o->hasInfo<trading::IssuedOrderInfo>());

    labelAutoOffsetOrder(o);
    orderList(o->orderInfo().getDir()).push_back(o);
    checkBookExecution(FILL_MARKET_PRICE);
}

void AsiaInstrOrderBook::labelAutoOffsetOrder(trading::OrderPtr const& o)
{
    // if it's specified already, honor the choice made by the trader
    if(o->orderInfo().getFlags() & OFLAG_SHFE_MANUAL_OPENCLOSE) return;

    bb::dir_t dir = o->orderInfo().getDir();
    bb::dir_t opp_dir = opposite_dir(dir);

    int32_t closing_offsets = 0;
    OrderList& olist = orderList(dir);

    for(OrderList::iterator it = olist.begin(); it != olist.end(); ++it)
    {
        if((*it)->orderInfo().getFlags() & OFLAG_SHFE_CLOSEPOSITION)
        {
            closing_offsets += (*it)->issuedInfo().getRemainingSize();
        }
    }

    uint32_t closable_offsets = std::max(0, m_offsets[opp_dir] - closing_offsets);

    if(closable_offsets >= o->orderInfo().getDesiredSize())
    {
        if(o->orderInfo().getInstrument().mkt == MKT_SHFE)
        {
            o->orderInfo().setFlags(
                OFLAG_SHFE_MANUAL_OPENCLOSE | OFLAG_SHFE_CLOSEPOSITION | OFLAG_SHFE_CLOSEPOSITIONTODAY);
        }
        else
        {
            o->orderInfo().setFlags(
                OFLAG_SHFE_MANUAL_OPENCLOSE | OFLAG_SHFE_CLOSEPOSITION);
        }
    }
    else
    {
        o->orderInfo().setFlags(OFLAG_SHFE_MANUAL_OPENCLOSE);
    }
}

void AsiaInstrOrderBook::deactivateOrder(trading::OrderPtr const& o)
{
    OrderList& mylist = orderList(o->orderInfo().getDir());
    mylist.remove(o);
}

void AsiaInstrOrderBook::checkAndApplyFills(const side_t& side, const double& price
                                             , const uint32_t& size, const IOrderBook::FillMode& m)
{
    dir_t dir = side2dir(opposite_side(side));
    OrderList& olist = orderList(dir);
    if(olist.empty())
        return;

    int32_t unfilled_size = int32_t(size);

    std::vector<FillInfo> fill_list;
    for(OrderList::iterator it = olist.begin(); it != olist.end(); ++it)
    {
        trading::OrderPtr o = *it;
        trading::OrderInfo const& oi = o->orderInfo();
        if(outside_or_at(side, oi.getPrice(), price))
        {
            FillInfo fi(o, price, o->issuedInfo().getRemainingSize());

            if(m == FILL_FROM_TRADE)
            {
                fi.fill_price = oi.getPrice();
                fi.fill_size = std::min(uint32_t(unfilled_size), fi.fill_size);
                unfilled_size -= fi.fill_size;
            }
            else if(m == FILL_FROM_BOOKUPDATE)
            {
                fi.fill_price = oi.getPrice();
                // fill all crossed levels in entirety
            }

            if(fi.fill_size > 0)
            {
                fill_list.push_back(fi);
                if(unfilled_size <= 0)
                    break;
            }
        }
    }
    BOOST_FOREACH(const FillInfo &fi, fill_list)
    {
        notifyFill(fi.order, fi.fill_price, fi.fill_size
                    , getFillFlag(fi.order->orderInfo()));
    }
}

bool AsiaInstrOrderBook::notifyFill(trading::OrderPtr const& simord,
                                     double price,
                                     unsigned sz,
                                     bb::fillflag_t fflag,
                                     const ptime_duration_t& add_delay)
{
    dir_t dir = simord->orderInfo().getDir();
    dir_t opp_dir = opposite_dir(dir);

    if(fflag & FILLFLAG_SHFE_CLOSE)
    {
        m_offsets[opp_dir] = std::max(0, m_offsets[opp_dir] - (int32_t)sz);
    }
    else
    {
        m_offsets[dir] += sz;
    }

    return OrderManagerForwarder::notifyFill(simord, price, sz, fflag, add_delay);
}

void AsiaInstrOrderBook::checkBookExecution(IOrderBook::FillMode m)
{
    FOR_EACH_SIDE(side) {
        dir_t dir = side2dir(opposite_side(side));
        if(orderList(dir).empty())
            continue;
        PriceSize lvl = m_spBook->getNthSide(0, side);
        if(lvl.getSize() > 0)
        {
            checkAndApplyFills(side, lvl.getPrice(), lvl.getSize(), m);
        }
    }
}

void AsiaInstrOrderBook::onTickReceived(const ITickProvider* tp, const TradeTick& tick)
{
    using namespace std;
    double price = tick.getPrice();
    FOR_EACH_SIDE(side)
    {
        const bb::ITradeSplitter* splitter = dynamic_cast<const bb::ITradeSplitter*>(tp);
        if(m_useSplitTrades && splitter)
        {
            const TradeTick* above_vwap = splitter->getAboveVwapTradeTick();
            const TradeTick* below_vwap = splitter->getBelowVwapTradeTick();
            if(above_vwap->getSize() != 0 && below_vwap->getSize() != 0)
            {
                // Send closest price last
                uint32_t below_size = uint32_t(below_vwap->getSize());
                double below_price = below_vwap->getPrice();
                uint32_t above_size = uint32_t(above_vwap->getSize());
                double above_price = above_vwap->getPrice();
                if(fabs(above_price - price) < fabs(price - below_price))
                {
                    checkAndApplyFills(side, below_price, below_size, FILL_FROM_TRADE);
                    checkAndApplyFills(side, above_price, above_size, FILL_FROM_TRADE);
                } else {
                    checkAndApplyFills(side, above_price, above_size, FILL_FROM_TRADE);
                    checkAndApplyFills(side, below_price, below_size, FILL_FROM_TRADE);
                }
            } else {
                // If the split trade results in 0 size (the default value), fall
                // back to the usual processing
                uint32_t fill_size = tick.getSize();
                checkAndApplyFills(side, price, fill_size, FILL_FROM_TRADE);
            }
        } else {
            uint32_t fill_size = tick.getSize();
            checkAndApplyFills(side, price, fill_size, FILL_FROM_TRADE);
        }
    }
}

const bool AsiaInstrOrderBookSpec::DEFAULT_USE_SPLIT_TRADES = false;

AsiaInstrOrderBookSpec::AsiaInstrOrderBookSpec()
    : m_useSplitTrades(DEFAULT_USE_SPLIT_TRADES)
{}

IOrderBookPtr AsiaInstrOrderBookSpec::construct(const instrument_t& instrument,
                                                 const source_t& source,
                                                 const IntrusiveWeakPtr<trading::TradingContext>& context,
                                                 const IntrusiveWeakPtr<IOrderManager>& manager,
                                                 const IMarketInternalDelaysCPtr& internal_delays,
                                                 const IBookSpecPtr& book_spec)
{
    return AsiaInstrOrderBookPtr(new AsiaInstrOrderBook(instrument, context, source, manager, internal_delays,
                                                          book_spec, m_useSplitTrades));
}

void AsiaInstrOrderBookSpec::print(std::ostream& os, const LuaPrintSettings& ps) const
{
    // Indentation one past current
    const LuaPrintSettings one_indent = ps.next();

    os << "(function() -- AsiaInstrOrderBookSpec" << std::endl
       << one_indent.indent() << "local book = AsiaInstrOrderBookSpec()" << std::endl
       << one_indent.indent() << "book.use_split_trades = " << luaMode(m_useSplitTrades, one_indent) << std::endl
       << one_indent.indent() << "return book" << std::endl
       << one_indent.indent() << "end)()";
}

bool AsiaInstrOrderBookSpec::registerScripting(lua_State& state)
{
    luabind::module(&state)
    [
        luabind::class_ < AsiaInstrOrderBookSpec, IOrderBookSpec, IOrderBookSpecPtr > ("AsiaInstrOrderBookSpec")
        .def(luabind::constructor < > ())
        .def_readwrite("use_split_trades", &AsiaInstrOrderBookSpec::m_useSplitTrades)
    ];

    return true;
}

} // namespace simulator
} // namespace bb
