#ifndef ALPHALESS_STRATEGY_H
#define ALPHALESS_STRATEGY_H

#include <set>
#include <map>
#include <vector>
#include <string>

#include <bb/core/smart_ptr.h>
#include <bb/core/Subscription.h>
#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/trading/IOrderStatusListener.h>
#include <bb/trading/IPositionProvider.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/MsgHandler.h>
#include <boost/foreach.hpp>

#include "StrategyConfig.h"


namespace bb {

BB_FWD_DECLARE_SHARED_PTR( IPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( IClientTimer );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class BbL1TradeMsg;
class UserMessageMsg;
class NyseImbalanceMsg;

namespace trading {
BB_FWD_DECLARE_SHARED_PTR( TradingContext );
BB_FWD_DECLARE_INTRUSIVE_PTR( Order );

class ChangeFlags;

}

}

namespace alphaless {

BB_FWD_DECLARE_SHARED_PTR( Pair );
BB_FWD_DECLARE_SHARED_PTR( OPS );
BB_FWD_DECLARE_SHARED_PTR( ClosedPosition );

typedef std::vector<bb::instrument_t> InstrVector;

class Strategy
    : public bb::IBookListener, public bb::trading::IOrderStatusListener, public bb::ITickListener, public bb::trading::IPositionListener
{
public:
    Strategy( const InstrVector&,
              const bb::trading::TradingContextPtr&,
              const StrategySettings&,
              const bb::source_t&,
              std::string&, 
              bool 
              );

    virtual ~Strategy();

    void onTickReceived( const bb::ITickProvider*, const bb::TradeTick& );

    void onPriceChanged( const bb::IPriceProvider& );

    /// Prints the top of book whenever the best market changes.
    /// From clientcore/IBook.h:
    /// Invoked when the subscribed Book changes.
    /// The levelChanged entries are negative if there is no change, or a 0-based depth.
    /// This depth is a minimum -- there could be multiple deeper levels that changed
    /// since the last onBookChanged.
    void onBookChanged( const bb::IBook*, const bb::Msg*,
                                int32_t bidLevelChange, int32_t askLevelChanged );

    /// Invoked when the subscribed Book is flushed.
    void onBookFlushed( const bb::IBook*, const bb::Msg* );

    void onPositionUpdated( bb::trading::IPositionProvider * );

    /// Called whenever an order's OrderStatus or CancelStatus changes.
    /// The details of what has changed can be found in the ChangeFlags.
    /// Listeners typically will only care about a small number of possible changes,
    /// so they can check the order to see whether it is something they care about.
    void onOrderStatusChange( const bb::trading::OrderPtr&, const bb::trading::IOrderStatusListener::ChangeFlags& );

    /// Called whenever there is a fill on the order. If you want fee
    /// info in your onFill callbacks, then leave this method alone in
    /// your subclass, and override IFillListener onFillWithFees
    /// instead. If you do implement onFillWithFees, then onFill WILL
    /// NOT BE CALLED automatically.
    virtual void onFill(const bb::trading::FillInfo & info);

    /// Called when the openTick is received
    void onOpenTick( const bb::ITickProvider* tp, const bb::TradeTick& tick );

    void shutdown();

    void handleUserMessage( const bb::UserMessageMsg& );
    void subscribeUserMessage();

    void updateMktPrice(std::string& instr, double price) {
        PriceInfoMap::iterator it = m_priceInfos.find(instr);
        if (m_priceInfos.end() != it) {
            PriceInfoPtr pip = it->second;
            if (price < pip->lowprice) {
                pip->lowprice = price;
            } else if (price > pip->highprice) {
                pip->highprice = price;
            }
        } else {
            PriceInfoPtr pip(new PriceInfo(price));
            m_priceInfos.insert(std::make_pair(instr, pip));
        }
    }

private:

    void oneShotTimerCB(bb::instrument_t ins, bb::dir_t dir, int size, double price);
    void hourlyTimerPeriodicCB();
    void hourlyTimerDoneCB();

    typedef std::map< bb::instrument_t, bb::IBookPtr, bb::instrument_t::less_no_mkt_no_currency > BookMap;
    InstrVector m_instrs;
    StrategySettings m_strategySettings;
    const bb::mktdest_t m_market;
    //const bb::trading::TradingContext m_tradingContext;
    // virtual ITrader getTrader( const instrument_t& instr );
    // virtual IPositionProvider getPositionProvider( const instrument_t& instr );
    const bb::trading::TradingContextPtr m_tradingContext;
    // TradingContext
    // virtual IssuedOrderTrackerPtr getIssuedOrderTracker( const instrument_t& instr );
    // issuedordertracker
    const bb::ClientContextPtr m_clientContext;

    BookMap m_Books;

    // clock
    bb::ClockMonitorPtr m_clockMonitor;

    // timer stuff
    typedef std::map< bb::instrument_t, bb::Subscription, bb::instrument_t::less_no_mkt_no_currency> SubscriptionMap;
    SubscriptionMap m_posSub;
    SubscriptionMap m_priceSub;

    const bb::IClientTimerPtr m_timer;
    bb::timeval_t m_startTime, m_endTime, m_entryTime, m_exitTime;
    bool m_trade;
    bool m_entryOrdersSent, m_exitOrdersSent;

    // keep track of my positions
    typedef std::map< bb::instrument_t, uint32_t, bb::instrument_t::less_no_mkt_no_currency> PositionsMap;
    PositionsMap   m_posMap;

    // price handler for opening prints
    bb::Subscription m_open;

    // for Shutting down (via userMsghandler)
    bb::Subscription shutdownTimerSub;

    // subscriptionVector
    typedef std::vector<bb::Subscription>  SubscriptionVector;
    SubscriptionVector      m_subVec;
    std::vector<bb::MsgHandlerPtr> m_msgHandlers;
    std::vector<bb::ITickProviderPtr> m_tps;
    std::vector<bb::IBookPtr> m_books;

    struct OpenOrderInfoByInstrument {
        OpenOrderInfoByInstrument(double p)
            : price(p)
            , openCount(0) {}
        double price;
        int openCount;
    };
    typedef bb::shared_ptr<OpenOrderInfoByInstrument> OpenOrderInfoByInstrumentPtr;

    typedef std::map<std::string, OpenOrderInfoByInstrumentPtr> PriceMap;
    PriceMap m_tickPrices;
    
    struct PriceInfo {
        PriceInfo(double price)
            : lowprice (price)
            , highprice (price) {}

        double lowprice;
        double highprice;
    };

    typedef bb::shared_ptr<PriceInfo> PriceInfoPtr;
    typedef std::map<std::string, PriceInfoPtr> PriceInfoMap;
    PriceInfoMap m_priceInfos;

    // demo commands
    std::string m_demoTag;
    bool m_switchDirection;
    bool m_baseline;
};
BB_DECLARE_SHARED_PTR( Strategy );

} // namespace alphaless

#endif // ALPHALESS_STRATEGY_H
