set title "Fsteps plot\nwith date and time as x-values"
set output "glox1.png"
set autoscale
set style data fsteps
set xlabel "Date\nTime"
set timefmt "%H:%M:%S"
set yrange [0:3]
set xdata time
set xrange [ "09:10:00":"09:10:30" ]
set ylabel "Concentration\nmg/l"
set format x "%H:%M:%S"
set grid
set key left
plot 'data.o' usi 1:2 with linespoints
