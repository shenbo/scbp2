#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>

#include <vector>
#include <map>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace boost;

typedef vector<string> VECSTR;
typedef map<string, string> OSMap;
OSMap g_osmp;

typedef map<string, double> ValueMap;
ValueMap g_vm;

int main(int argc, char* argv[]) {
    if (argc < 2) {
        cout << "help: " << argv[0] << " [filename]\n";
        exit(1);
    }
    ifstream input;
    input.open(argv[1]);

    g_osmp.clear();
    g_vm.clear();

    try {
        while (true) {
            char line[256] = {0};
            bool b = input.getline(line, 256);
            if (!b) {
                break;
            }
            VECSTR vec;
            boost::split(vec, line, boost::is_any_of(" "));
            if (vec.size() < 4) {
                continue;
            }
            
            string key = vec[1];
            OSMap::iterator it = g_osmp.find(key);
            if (it == g_osmp.end()) {
                g_osmp.insert(make_pair(key, line + string("\n")));
            } else {
                it->second += (line + string("\n"));
            }

            /// update value map
            {
                ValueMap::iterator iter = g_vm.find(vec[0]);
                double delta = boost::lexical_cast<double>(vec[3]);
                if (iter == g_vm.end()) {
                    g_vm.insert(make_pair(vec[0], delta));
                } else {
                    iter->second += delta;
                }
            }
        }
    } catch (...) {
        cout << "classify confront some errors ..." << std::endl;
    }

    input.close();

    // dump the cached sub streams
    ofstream ofs("out.o", ofstream::out);
    {
        OSMap::iterator it = g_osmp.begin();
        for (; it != g_osmp.end(); it++) {
            ofs << it->second << ">>>>>>>\n";
        }
    }
    ofs.close();

    /// dump daily value informations
    ofstream ofs_v("value.o", ofstream::out);
    {
        ValueMap::iterator it = g_vm.begin();
        for (; it != g_vm.end(); it++) {
            ofs_v << it->first << " " << it->second << std::endl;
        }
    }
    ofs_v.close();
   
    return 0;
}
