#include <algorithm>
#include <sstream>
#include <bb/core/Log.h>

#include "SSEHistMStream.h"

using namespace std;

namespace bb {

namespace {
source_t g_source;
uint32_t g_sendSequence;

template<typename T>
double convert2double(T n, int precision = 0)
{
    if (precision <= 0) {
        return (double)n;
    }

    uint32_t div = 1;
    for (int i = 0; i < precision; i++) {
        div *= 10;
    }

    return ((double)n / (double)div);
}
    
void generateTimeval(uint64_t& dt, timeval_t& o_tv)
{
    // Datetime format: 2016081209330408
    int a[7] = {0};

    uint64_t magic = 1000000000000;
    for (int i = 0; i < 7; i++)
    {
        a[i] = dt / magic;
        dt -= (a[i] * magic);

        magic /= 100;
    }

    o_tv = timeval_t::make_time(a[0], a[1], a[2], a[3], a[4], a[5], a[6]);
}

};

SSEHistMStream::SSEHistMStream(mtype_t& mtype, std::string id, std::string dayStr, std::string dataPath)
{
    boost::erase_all(dayStr, "-");

    // TODO: will fix the hardcode filename
    if (MSG_SSE_HISTORICAL_SNAP1 == mtype) {
        ostringstream os;
        os << dataPath << dayStr << "/HisSnap.dat";
        m_in.open(os.str().c_str());
    } else if (MSG_SSE_HISTORICAL_TICK == mtype) {
        ostringstream os;
        os << dataPath << dayStr << "/HisTick.dat";
        m_in.open(os.str().c_str());
    }

    m_remain = 0;
    m_initialized = false;
    m_msgCgy = SMT_TICK;
    m_offset = 0;
    m_indexPosition = 0;

    // TODO: will replace the make_auto for better performance
    g_source = source_t::make_auto(SRC_SHSE);
    g_source.setOrig(ORIG_SFIT);
    g_source.setDest(DEST_SFIT);

    g_sendSequence = 0;

    m_msgs[SMT_TICK] = &m_tick;
    m_msgs[SMT_AUCTION] = &m_auction;
    m_msgs[SMT_SNAP] = &m_snap;
    m_msgs[SMT_DAY_MIN] = &m_day_min;
    m_msgs[SMT_SNAP2] = &m_snap2Part1;
    m_desiredID = id;
}

void SSEHistMStream::compactBuffer()
{
    memmove((void*)m_buffer, (void*)(m_buffer + m_offset), m_remain);
    m_offset = 0;
}

void SSEHistMStream::updateBuffer()
{
    m_in.read(m_buffer + m_remain, MAX_BUF_LEN - m_remain);
    size_t len = m_in.gcount();
    m_remain += len;
}

int SSEHistMStream::initialize()
{
    if (m_initialized)
    {
        return 0;
    }

    updateBuffer();

    if (m_remain < SSE_FILE_HEAD_LEN)
    {
        return -1;
    }

    char* p = m_buffer;
    SSEFileHead* head = (SSEFileHead*)p;

    // TODO: will enhance later, by replace these if-else for better performance
    string cgy(head->Category);
    if (cgy == string("tick"))
    {
        m_msgCgy = SMT_TICK;
        SSE_THIS_MSG_LEN = SSE_TICK_LEN;
    }
    else if (cgy == string("auction"))
    {
        m_msgCgy = SMT_AUCTION;
        SSE_THIS_MSG_LEN = SSE_AUCTION_LEN;
    }
    else if (cgy == string("snap"))
    {
        m_msgCgy = SMT_SNAP2;
        SSE_THIS_MSG_LEN = SSE_SNAP_L2_LEN;
    }
    else if ((cgy == string("day")) || (cgy == string("minute")))
    {
        m_msgCgy = SMT_DAY_MIN;
        SSE_THIS_MSG_LEN = SSE_DAY_MIN_LEN;
    }

    p += SSE_FILE_HEAD_LEN + head->DescriptionSize;

    int indexCount = (head->IndexSize / SSE_FILE_INDEX_LEN);
    for (int i = 0; i < indexCount; i++)
    {
        SSEFileIndex* pIndex = (SSEFileIndex*)p;
        SSEFileIndexPtr indexPtr(new SSEFileIndex());

        memcpy((void*)(indexPtr->Code), (void*)(pIndex->Code), 16);
        indexPtr->CodeBeginPosition = pIndex->CodeBeginPosition;
        indexPtr->CodeEndPosition = pIndex->CodeEndPosition;

        m_indexes.push(indexPtr);
        p += SSE_FILE_INDEX_LEN;
   
        ostringstream os;
        os << "SHSE_" << indexPtr->Code;
        string id = os.str();
        if (id == m_desiredID) {
            m_CodeBeginPosition = indexPtr->CodeBeginPosition;
            m_CodeEndPosition = indexPtr->CodeEndPosition;
        }
    }

    m_in.seekg(m_CodeBeginPosition);
    m_remain = 0;
    m_offset = 0;
    
    m_initialized = true;

    return 0;
}

const Msg* SSEHistMStream::next()
{
    if (!m_initialized)
    {
        int iret = initialize();
        if (iret < 0)
        {
            return NULL;
        }
    }

    if (m_indexes.empty())
    {
        return NULL;
    }

    const SSEFileIndex& index = *(m_indexes.front());
    int total = (m_CodeEndPosition - m_CodeBeginPosition) / SSE_THIS_MSG_LEN;

    if (m_remain < SSE_THIS_MSG_LEN)
    {
        compactBuffer();
        updateBuffer();
    }

    // TODO: will enhance later
    switch(m_msgCgy)
    {
        case SMT_TICK: fillTickMsg(index); break;
        case SMT_AUCTION: fillAuctionMsg(index); break;
        case SMT_SNAP: fillSnapMsgL1(index); break;
        case SMT_DAY_MIN: fillDayMinMsg(index); break;
        case SMT_SNAP2: fillSnapMsgL2(index); break;
        default: return NULL;
    }

    m_offset += SSE_THIS_MSG_LEN;
    m_remain -= SSE_THIS_MSG_LEN;

    m_indexPosition++;
    if (m_indexPosition >= total)
    {
        return NULL;
    }

    return m_msgs[m_msgCgy];
}

void SSEHistMStream::fillTickMsg(const SSEFileIndex& index)
{
    SSETick* tick = (SSETick*)(m_buffer + m_offset);
    msg_sse_historical_tick* body = m_tick.body();
    generateTimeval(tick->DateTime, body->date_time);
    body->code = instrument_t::stock(m_desiredID.c_str(), MKT_SHSE, currency_t::CNY);
    body->price = convert2double(tick->Price, 3);
    body->amount = convert2double(tick->Amount, 5);
    body->buy_no = tick->BuyNo;
    body->sell_no = tick->SellNo;
    body->volume = convert2double(tick->Volume, 3);
    m_tick.setHeader(body->code.sym, g_source, g_sendSequence++, body->date_time);
}

void SSEHistMStream::fillAuctionMsg(const SSEFileIndex& index)
{
    SSEAuction* auction = (SSEAuction*)(m_buffer + m_offset);
    msg_sse_historical_auction* body = m_auction.body();
    body->code = instrument_t::stock(m_desiredID.c_str(), MKT_SHSE, currency_t::CNY);
    generateTimeval(auction->DateTime, body->date_time);
    body->price = convert2double(auction->Price, 3);
    body->virtual_auction_qty = convert2double(auction->VirtualAuctionQty, 3);
    body->leaves_qty = convert2double(auction->LeaveQty, 3);
    body->side.strncpy(auction->Side, 8);
    m_auction.setHeader(body->code.sym, g_source, g_sendSequence++);
}

void SSEHistMStream::fillSnapMsgL1(const SSEFileIndex& index) {
    SSESnapL1* snap = (SSESnapL1*)(m_buffer + m_offset);
    msg_sse_historical_snap1* body = m_snap.body();
    uint64_t Datetime = 100 * snap->uiDateTime; 
    generateTimeval(Datetime, body->date_time);
    body->code = instrument_t::stock(m_desiredID.c_str(), MKT_SHSE, currency_t::CNY);
    body->pre_close_px = convert2double(snap->uiPreClosePx, 3);
    body->open_px = convert2double(snap->uiOpenPx, 3);
    body->high_px = convert2double(snap->uiHighPx, 3);
    body->low_px = convert2double(snap->uiLowPx, 3);
    body->last_px = convert2double(snap->uiLastPx, 3);
    body->volume = convert2double(snap->uiVolume, 3);
    body->amount = convert2double(snap->uiAmount, 3);
    for (int i = 0; i < 5; i++)
    {
        body->bid[i].price = convert2double(snap->arrBidPx[i], 3);
        body->bid[i].size = snap->arrBidSize[i];
        body->ask[i].price = convert2double(snap->arrOfferPx[i], 3);
        body->ask[i].size = snap->arrOfferSize[i];
    }
    body->num_trades = snap->uiNumTrades;
    body->iopv = convert2double(snap->uiIOPV, 3);
    body->nav = convert2double(snap->uiNAV, 3);
    body->phasecode.strncpy(snap->strPhaseCode, 8);
    cout << "snap info:"
         << ", code=" << body->code
         << ", open_px=" << body->open_px
         << ", high_px=" << body->high_px
         << ", amount=" << body->amount
         << ", num_trades=" << body->num_trades
         << ", nav=" << body->nav
         << std::endl;
    m_snap.setHeader(body->code.sym, g_source, g_sendSequence++, body->date_time);
}

void SSEHistMStream::fillSnapMsgL2(const SSEFileIndex& index)
{
    SSESnapL2* snap = (SSESnapL2*)(m_buffer + m_offset);
    msg_sse_historical_snap2_part1* body = m_snap2Part1.body();
    uint64_t Datetime = 100 * snap->uiDateTime; 
    generateTimeval(Datetime, body->date_time);
    body->code = instrument_t::stock(m_desiredID.c_str(), MKT_SHSE, currency_t::CNY);
    body->pre_close_px = convert2double(snap->uiPreClosePx, 3);
    body->open_px = convert2double(snap->uiOpenPx, 3);
    body->high_px = convert2double(snap->uiHighPx, 3);
    body->low_px = convert2double(snap->uiLowPx, 3);
    body->last_px = convert2double(snap->uiLastPx, 3);
    body->total_volume_trade = convert2double(snap->uiTotalVolumeTrade, 3);
    body->total_amount_trade = convert2double(snap->uiTotalValueTrade, 5);
    body->total_bid_qty = convert2double(snap->uiTotalBidQty, 3);
    body->total_ask_qty = convert2double(snap->uiTotalOfferQty, 3);
    body->weighted_avg_bid_px = convert2double(snap->uiWeightedAvgBidPx, 3);
    body->weighted_avg_ask_px = convert2double(snap->uiWeightedAvgOfferPx, 3);
    body->withdraw_buy_number = snap->uiWithdrawBuyNumber;
    body->withdraw_sell_number = snap->uiWithdrawSellNumber;
    body->withdraw_buy_amount = convert2double(snap->uiWithdrawBuyAmount, 3);
    body->withdraw_buy_money = convert2double(snap->uiWithdrawBuyMoney, 5);
    body->withdraw_sell_amount = convert2double(snap->uiWithdrawSellAmount, 3);
    body->withdraw_sell_money = convert2double(snap->uiWithdrawSellMoney, 5);
    body->total_bid_number = snap->uiTotalBidNumber;
    body->total_ask_number = snap->uiTotalOfferNumber;
    body->bid_trade_max_duration = snap->uiBidTradeMaxDuration;
    body->ask_trade_max_duration = snap->uiOfferTradeMaxDuration;
    body->num_bid_orders = snap->uiNumBidOrders;
    body->num_ask_orders = snap->uiNumOfferOrders;

    // cout << "level2 info, instrument=" << body->code << ": ";
    for (int i = 0; i < MAX_LEVEL2_SIZE; i++)
    {
        body->bid[i].price = convert2double(snap->arrBidPrice[i], 3);
        body->bid[i].size = convert2double(snap->arrBidOrderQty[i], 3);
        body->ask[i].price = convert2double(snap->arrOfferPrice[i], 3);
        body->ask[i].size = convert2double(snap->arrOfferOrderQty[i], 3);
        /*
        cout << ", mkt" << i << ": "
             << "bidPrice=" << body->bid[i].price << ","
             << "bidSize=" << body->bid[i].size << ","
             << "askPrice=" << body->bid[i].price << ","
             << "askSize=" << body->bid[i].size;
             */
    }
    // cout << endl;
    body->num_trades = snap->uiNumTrades;
    /*
    cout << "snap2 info:"
         << ", code=" << body->code
         << ", open_px=" << body->open_px
         << ", high_px=" << body->high_px
         << ", num_trades=" << body->num_trades
         << std::endl;
         */
    m_snap2Part1.setHeader(body->code.sym, g_source, g_sendSequence++, body->date_time);
}

void SSEHistMStream::fillDayMinMsg(const SSEFileIndex& index)
{
    SSEDayMin* day = (SSEDayMin*)(m_buffer + m_offset);
    msg_sse_historical_day_min* body = m_day_min.body();
    body->code = instrument_t::stock(m_desiredID.c_str(), MKT_SHSE, currency_t::CNY);
    generateTimeval(day->uiDateTime, body->date_time);
    body->pre_close_px = convert2double(day->uiPreClosePx, 3);
    body->open_px = convert2double(day->uiOpenPx, 3);
    body->high_px = convert2double(day->uiHighPx, 3);
    body->low_px = convert2double(day->uiLowPx, 3);
    body->last_px = convert2double(day->uiLastPx, 3);
    body->volume = convert2double(day->uiVolume, 3);
    body->amount = convert2double(day->uiAmount, 3);
    body->qt = convert2double(day->uiIOPV, 3);
    m_day_min.setHeader(body->code.sym, g_source, g_sendSequence++);
}
}
