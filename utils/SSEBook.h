#ifndef BB_CLIENTCORE_SSEBOOK_H
#define BB_CLIENTCORE_SSEBOOK_H

#include <boost/optional.hpp>
#include <bb/core/timeval.h>
#include <bb/clientcore/L1Book.h>
#include <bb/clientcore/SourceBooks.h>

namespace bb {

class ClientContext;
class SseHistoricalSnap1Msg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
BB_FWD_DECLARE_SHARED_PTR( RandomSource );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );

class SseBook : public L1Book //L1Book
{
public:
    SseBook( const ClientContextPtr& context, const instrument_t& instr, source_t src
        , const std::string& desc, int _vbose = 0 );
    virtual ~SseBook() {}

    //////////////////////////////////////////// L1Book interfaces
    virtual PriceSize getSide( side_t side ) const
    {
        return m_bookLevels[side][0].getPriceSize();
    }

    virtual bool isOK() const;

    virtual PriceSize getNthSide( size_t depth, side_t side ) const
    {
        if ((depth < 0) || (depth > 9) || !(m_bookLevels[side][depth].isOK()))
        {
            return PriceSize();
        }
        
        return m_bookLevels[side][depth].getPriceSize();
    }

    class BookLevelCIter : public IBookLevelCIter
    {
    public:
        BookLevelCIter( const BookLevel* bl )
            : m_next( (bl->isOK()) ? bl : 0 ) {}
        virtual bool hasNext() const { return m_next != 0; }
        virtual BookLevelCPtr next()
        {
            const BookLevel* rval = m_next;
            if( rval )
            {
                m_next = 0;
                return makeNoopSharedPtr( rval );
            }
            else return BookLevelCPtr();
        }
    protected:
        const BookLevel* m_next;
    };

    // L2Book
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const
    {
        return IBookLevelCIterPtr( new BookLevelCIter(&m_bookLevels[side][0] ) );
    }

    bool isOK10(side_t side) const
    {
        bool res = true;
        for (int i = 0; i < 10; i++)
        {
            res = (res && m_bookLevels[side][i].isOK());
        }

        return res;
    }

    virtual size_t getNumLevels( side_t side ) const {
        bool ok = isOK10(side);
        return ok ? 10 : 0;
    }

    /// Flushes the book.
    virtual void flushBook()
    {
        for (int i = 0; i < 10; i++)
        {
            setLevel( BID, i, 0, 0 );
            setLevel( ASK, i, 0, 0 );
        }
    }

    bool setLevel( side_t side, int depth, double price, size_t size )
    {
        BookLevel& bl = m_bookLevels[side][depth];
        if( !bl.isOK() )
        {
            if( size > 0 )
            {
                bl.setPriceSize(price, size);
                notifyBookLevelAdded(&bl);
                return true;
            }
        }
        else
        {
            if( (size == 0) || EQZ( price ) )
            {
                notifyBookLevelDropped( &bl );
                bl.setPriceSize( std::numeric_limits<double>::signaling_NaN(), 0 );
                return true;
            }

            // assume non-zero price and size
            if( !EQ( bl.getPrice(), price ) )
            {
                // drop and add new book level
                notifyBookLevelDropped( &bl );
                bl.setPriceSize( price, size );
                notifyBookLevelAdded( &bl );
                return true;
            }

            if( !EQ( bl.getSize(), size ) )
            {
                // modify level size
                bl.setPriceSize( price, size );
                notifyBookLevelModified( &bl );
                return true;
            }
        }
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////
    // L1Book
    timeval_t getExchangeTime() const
    { return m_exchangeTv; }

    virtual double getMidPrice() const;

    double getLimitUpPrice() const
    { return m_limitUpPrice ? m_limitUpPrice.get() : 0.0; }
    double getLimitDownPrice() const
    { return m_limitDownPrice ? m_limitDownPrice.get() : 0.0; }

protected:
    void onSseQdMsg( const SseHistoricalSnap1Msg& msg );
    void onSseQdMsg2(const SseHistoricalSnap2Part1Msg& msg);

protected:
    MsgHandlerPtr m_subSseQdMsg;
    MsgHandlerPtr m_subSseQdMsg2;
    boost::optional<double> m_limitUpPrice;
    boost::optional<double> m_limitDownPrice;
    timeval_t m_exchangeTv;
    BookLevel m_bookLevels[2][10];
};

BB_DECLARE_SHARED_PTR( SseBook );

} // namespace bb

#endif // BB_CLIENTCORE_SseBook_H
